﻿<%@ Page Title="" Language="C#" MasterPageFile="~/facebook/FBContentPageMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Biore2012.facebook.CSBCCoupon.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div id="fb-root"></div>
	<script type="text/javascript">
		var extJsHost_connect_facebook_net = (("https:" == document.location.protocol) ? "https://" : "http://");
		window.fbAsyncInit = function() {
			FB.init({
				appId: '<%= System.Configuration.ConfigurationManager.AppSettings["CSBCCouponFBAppId"] %>', // App ID
				//channelURL : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
				status: true, // check login status
				cookie: true, // enable cookies to allow the server to access the session
				oauth: true, // enable OAuth 2.0
				xfbml: true  // parse XFBML
			});

			// Additional initialization code here
			FB.Canvas.setAutoGrow();
			FB.Canvas.scrollTo(0, 0);
		};

		// Load the SDK Asynchronously
		(function(d) {
			var js, id = 'facebook-jssdk'; if (d.getElementById(id)) { return; }
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = extJsHost_connect_facebook_net + "connect.facebook.net/en_US/all.js";
			d.getElementsByTagName('head')[0].appendChild(js);
		} (document));
	</script>

	<div class="csbc">
		<asp:Panel ID="panelLikeOverlay" CssClass="preLike" runat="server" Visible="true">
			<p class="ir">SAY GOODBYE TO SUMMER GRIME For Less
			   Like Us and Request Your $1.50<sup>&Dagger;</sup> Coupon for a Bior&eacute;<sup>&reg;</sup> Pore Strip Pack
			   Bior&eacute;<sup>&reg;</sup> &ndash; Face Anything&trade;</p>
			<p class="disclaimer"><sup>&Dagger;</sup>While supplies last. Offer valid on any Bior&eacute;<sup>&reg;</sup> Pore Strip Pack only. Offer valid to legal residents of all (50) US States, including DC. Coupon limited to one per person, per household. Cannot be combined with any other coupon/offer. Excludes travel and trial sizes.</p>
		</asp:Panel>
		
		<asp:Panel ID="panelUserLikes" CssClass="postLike" runat="server" Visible="false">
            <h1 class="ir">SAY GOODBYE TO SUMMER GRIME SAY HELLO TO FRESH, CLEAN SKIN With $1.50<sup>&Dagger;</sup> off Bior&eacute;<sup>&reg;</sup> Pore Strips</h1>
			
			<div class="top">
			    <h2>Summer fun can be harsh on your complexion.</h2>
			    <p>Sweat, chlorine, and pollution can all add up to clogged pores. Bior&eacute;<sup>&reg;</sup> Pore Strips</a> give your skin the deep clean it needs, by removing a week&#39;s worth of dirt and oil in just 10 minutes. Try <a href="<%= System.Configuration.ConfigurationManager.AppSettings["path"] %>breakup-with-blackheads/pore-strips" target="_blank">Deep Cleansing Pore Strips</a>, <a href="<%= System.Configuration.ConfigurationManager.AppSettings["path"] %>breakup-with-blackheads/pore-strips#ultra" target="_blank">Deep Cleansing Pore Strips Ultra</a>, or the <a href="<%= System.Configuration.ConfigurationManager.AppSettings["path"] %>breakup-with-blackheads/pore-strips#combo" target="_blank">Deep Cleansing Pore Strip Combo Pack</a>.<br /><br />
			        Say bye-bye to grimy buildup, and say hello to clean skin <br />
			        with Bior&eacute;<sup>&reg;</sup> Pore Strips</a>.</p>
    			
			    <a href="<%= System.Configuration.ConfigurationManager.AppSettings["path"] %>email-newsletter-sign-up/?promo=biore_us_201208_FBPS_Coup50835K&show=2" class="btnCoupon ir">Request Your Coupon</a>
			</div>
			
			<p class="disclaimer"><sup>&Dagger;</sup>While supplies last. Offer valid on any Bior&eacute;<sup>&reg;</sup> Pore Strip Pack only. Offer valid to legal residents of all (50) US States, including DC. Coupon limited to one per person, per household. Cannot be combined with any other coupon/offer. Excludes travel and trial sizes.</p>
			
			<div class="videoCaption">
			    <h3 class="ir">Watch it Work</h3>
			    <p><a href="<%= System.Configuration.ConfigurationManager.AppSettings["path"] %>breakup-with-blackheads/pore-strips" target="_blank">Bior&eacute;<sup>&reg;</sup> Pore Strips</a> remove a week&#39;s worth of dirt and oil that can cause blackheads, leaving skin fresh and clean. Watch the video to see them in action!</p>
			</div>
			<div class="videoHolder">
				<div class="video pie">
					<div id="video">
						<a class="videoLink" target="_blank" href="<%= System.Configuration.ConfigurationManager.AppSettings["path"] %>video/PORE_STRIPS_US_512x288_h264_768Kbps.mp4" rel="videoParams::448|252|40">
							<img src="images/LikedVideoOverlay.jpg" alt="Watch this effective facial cleanser in action." />
						</a>
					</div>		
				</div>
			</div>            
		</asp:Panel>
		
		<asp:Panel ID="panelNotFacebook" CssClass="notFacebook" runat="server" Visible="false">
			<h1><a href="http://www.biore.com/en-US/biore-skincare" target="_blank">Bior&eacute;<sup>&reg;</sup> &ndash; Face Anything&trade;</a></h1>
			<h2>Bring More Balance (to your skin) for less!</h2>
			<h3><a href="<%= System.Configuration.ConfigurationManager.AppSettings["CSBCCouponFBTabUrl"] %>">Visit our Facebook Page and Like Us</a> and Request Your $1.50 Coupon* for NEW Combination Skin Balancing Cleanser</h3>
			<p>*While supplies last. Offer valid on any Bior&eacute;<sup>&reg;</sup> Pore Strip Pack only. Offer valid to legal residents of all (50) US States, including DC. Coupon limited to one per person, per household. Cannot be combined with any other coupon/offer. Excludes travel and trial sizes.</p>
		</asp:Panel>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script>window.jQuery || document.write('<script src="//code.jquery.com/jquery-latest.min.js"><\/script>')</script>
    <script>window.jQuery || document.write('<script src="/js/jquery-1.6.4.min.js"><\/script>')</script>
        
    <%=SquishIt.Framework.Bundle.JavaScript()
        .Add("~/js/jquery.ENLvideo.js")
        .Add("~/js/swfobject.min.js")
        .Add("~/facebook/CSBCCoupon/js/main.js")
		.Render("~/facebook/js/combinedCSBCCoupon_#.js")
	%>
	
    <%--
	<script>
	    // Assign Facebook vars from AppSettings
	    //		CSBCCoupon.fbVars = {
	    //			"FBAppID": '<%= System.Configuration.ConfigurationManager.AppSettings["CSBCCouponFBAppId"] %>'
	    //			"FBShareUrl": '<%= System.Configuration.ConfigurationManager.AppSettings["porespectiveFBShareUrl"] %>',
	    //			"FBSharePic": '<%= System.Configuration.ConfigurationManager.AppSettings["porespectiveFBSharePic"] %>',
	    //			"FBShareName": '<%= System.Configuration.ConfigurationManager.AppSettings["porespectiveFBShareName"] %>',
	    //			"FBShareCaption": '<%= System.Configuration.ConfigurationManager.AppSettings["porespectiveFBShareCaption"] %>',
	    //			"FBShareDesc": '<%= System.Configuration.ConfigurationManager.AppSettings["porespectiveFBShareDesc"] %>',
	    //			"FBShareMsg": '<%= System.Configuration.ConfigurationManager.AppSettings["porespectiveFBShareMsg"] %>',
	    //			"FBShareRedirUrl": '<%= System.Configuration.ConfigurationManager.AppSettings["porespectiveFBShareRedirUrl"] %>'
	    //		}		
	</script>
	--%>
</asp:Content>