﻿<%@ Page Title="" Language="C#" MasterPageFile="~/facebook/FBContentPageMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Biore2012.facebook.SeeTheChange.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<meta property="fb:app_id" content="<%= System.Configuration.ConfigurationManager.AppSettings["SeeTheChange_FBAppId"] %>" />
	
	<script type="text/javascript">
		var FBVars = {}
	</script>
	
	<link rel="Stylesheet" href="colorbox/colorbox.css">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div id="fb-root">
	</div>

	<script type="text/javascript">
		var extJsHost_connect_facebook_net = (("https:" == document.location.protocol) ? "https://" : "http://");
		window.fbAsyncInit = function() {
		    FB.init({
		        appId: '<%= System.Configuration.ConfigurationManager.AppSettings["SeeTheChange_FBAppId"] %>', // App ID
		        //channelURL : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
		        status: true, // check login status
		        cookie: true, // enable cookies to allow the server to access the session
		        oauth: true, // enable OAuth 2.0
		        xfbml: true  // parse XFBML
		    });

		    // Additional initialization code here
		    FB.Canvas.setAutoGrow();
		    FB.Canvas.scrollTo(0, 0);
		};

		// Load the SDK Asynchronously
		(function(d) {
			var js, id = 'facebook-jssdk'; if (d.getElementById(id)) { return; }
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = extJsHost_connect_facebook_net + "connect.facebook.net/en_US/all.js";
			d.getElementsByTagName('head')[0].appendChild(js);        } (document));
		
	</script>
	
	<div class="stc">
		
		<!-- NOT LIKED -->
		<asp:Panel ID="panLikeOverlay" CssClass="stcLikeOverlay" runat="server" Visible="true">
		
		    <h1>Bior&eacute;<sup>&reg;</sup> See The Change</h1>
		    
			<p class="overlayIntro">Bior&eacute;<sup>&reg;</sup> Skincare products help you see change in your skin&mdash;but why stop there? <strong class="showUs">Show us</strong> where you'd like to see a difference <strong class="dropCoins">by dropping "coins"</strong> into the jar of the organization that's most important to you! Your daily contributions will ultimately choose which charity will receive a $5,000 donation* from Bior&eacute;<sup>&reg;</sup> Skincare.</p>

            <p class="likeOurPage">Like our page to be a part of the change!</p>
            
            <div class="overlayBG"></div>
			
		</asp:Panel>
		
		<!-- LIKED -->
		<asp:Panel ID="panUserLikes" CssClass="userLikes" runat="server" Visible="false">
		</asp:Panel>
		
		<div id="theContent">
		
		    <h1>Bior&eacute;<sup>&reg;</sup> See The Change</h1>
		    
		    <div id="introWrapper" class="introWrapper">
		        <h2>Change can happen every day</h2>
		        <p>Bior&eacute;<sup>&reg;</sup> Skincare products help you see change in your skin&mdash;but why stop there? <strong class="showUs">Show us</strong> where you'd like to see a difference <strong class="dropCoins">by dropping "coins"</strong> into the jar of the organization that's most important to you! Your daily contributions will ultimately choose which charity will receive a $5,000 donation* from Bior&eacute;<sup>&reg;</sup> Skincare. <strong class="comeBack">Come back</strong> and drop your change off <strong class="onceAday">once a day</strong> to help the charity of your choosing!</p>   
		    </div> 
		
		    <!-- Voting coins -->
		    <div id="poll" class="poll">
		    
		        <h3>I Want To See Change For:</h3>
		       
		        <div id="vitalVoicesVote" class="voteOption">
		        
		            <div id="leftArrow" class="leftArrow arrow">&larr;</div>
		        
		            <div id="coinDropVV" class="coinDropVV coinDropContainer">
		                <div id="voteVitalVoices" class="voteCoin coinVV">Vote for Vital Voices</div>
		                <input type="radio" name="poll" id="Poll1" value="0" />
		            
		                <!--img src="images/jarVV.png" alt=""-->
		            
		            </div>
		            
		        </div>
		        
		        <div id="petfinderVote" class="voteOption">
		        
		            <div id="rightArrow" class="rightArrow arrow">&rarr;</div>
		            
		            <div id="coinDropPF" class="coinDropPF coinDropContainer">
		                <div id="votePetfinder" class="voteCoin coinPF">Vote for the Petfinder Foundation</div>
		                <input type="radio" name="poll" id="Poll2" value="1" />
		            
		                <!--img src="images/jarPF.png" alt=""-->
		            </div>
		            
		        </div>
		        
		    </div> <!-- /#poll -->
		
		    <!-- Jars -->
		    <div id="vitalVoicesJar" class="vitalVoicesJar jarContents">
		    
		        <img id="vvCoins" class="coinLevel vvCoins" src="images/coins.png" alt="" />    
		        <img id="vvJarImg" class="jarImg vvJarImg" src="images/jarVV.png" alt="" />
		    
		        <div id="playVVvideo" class="playVVvideo videoHolder">
		            <a class="videoLink" href="video/VitalVoices_480x270_750kbps.mp4" rel="videoparams::480|270|90">Play Vital Voices video</a>
		        </div>
		        
		        <div id="vvLearnMoreContainer" class="vvLearnMoreContainer lmContainer">
		            <div id="vvLearnMore" class="vvLearnMore learnMoreDiv">
		                <p class="more">Since 1997, Vital Voices has honored and helped women leaders enhance their ability to transform lives and bring peace to communities. Their Rising Voices initiative invites 500 young women to the Annual Global Leadership Awards to be inspired by women leaders around the world.<br>
		                    <a onclick="window.open('http://www.vitalvoices.org/global-initiatives/rising-voices', '_blank'); return false;" href="http://www.vitalvoices.org/global-initiatives/rising-voices">Visit their website.</a></p>
		                <p>
		                    <a id="fbShareVitalVoices" class="fbShare" href="#">Share Vital Voices</a>
		                    <a id="learnMoreVV" class="learnMore" href="#">Learn More</a>
		                </p>
		            </div>
		            <img class="labelEdge" src="images/labelEdgeVV.png" alt="">
		        </div>
		    </div> <!-- /#vitalVoicesJar -->
		    
		    <div id="petfinderJar" class="petfinderJar jarContents">
		    
		        <img id="pfCoins" class="coinLevel pfCoins" src="images/coins.png" alt="" />
		        <img id="pfJarImg" class="jarImg pfJarImg" src="images/jarPF.png" alt="" />
		    
		        <div id="playPFvideo" class="playPFvideo videoHolder">
		            <a class="videoLink" href="video/PetfinderFoundation_480x270_750kbps.mp4" rel="videoparams::480|270|60">Play the Petfinder Foundation video</a>
		        </div>
		        
		        <div id="pfLearnMoreContainer" class="pfLearnMoreContainer lmContainer">
		            <div id="pfLearnMore" class="pfLearnMore learnMoreDiv">
		                <p class="more">Founded in 2003, the Petfinder Foundation has been committed to improving animal welfare by finding safe and loving families for homeless pets through adoption. Their mission is to ensure that no adoptable pet is euthanized for lack of a home.<br>
		                    <a onclick="window.open('http://www.petfinderfoundation.com', '_blank'); return false;" href="http://www.petfinderfoundation.com">Visit their website.</a></p>
		                <p>
		                    <a id="fbSharePetfinder" class="fbShare" href="#">Share Petfinder</a>
		                    <a id="learnMorePF" class="learnMore" href="#">Learn More</a>
		                </p>
		            </div>
		            <img class="labelEdge" src="images/labelEdgePF.png" alt="">
		        </div>
		    </div> <!-- /#petfinderJar -->
		    
		    <!-- FOOTER -->
		    <div id="footer" class="footer">
		        <a id="cfcLogo" href="http://www.changeforchange.org" onclick="window.open('http://www.changeforchange.org', '_blank'); return false;">Change For Change<sup>&reg;</sup></a>
		        <p>This program is possible through a partnership between <a href="http://www.biore.com/en-US/biore-skincare" onclick="window.open('http://www.biore.com/en-US/biore-skincare', '_blank'); return false;">Bior&eacute;<sup>&reg;</sup> Skincare</a> and <a href="http://www.changeforchange.org" onclick="window.open('http://www.changeforchange.org', '_blank'); return false;">Change for Change<sup>&reg;</sup></a>, a network of young adults across colleges, workplaces, and other communities. Change for Change<sup>&reg;</sup> is based on the idea that small change found in pockets or under couch cushions can actually become a catalyst for bigger change in the world. <strong>To learn how you can make a difference with Change for Change<sup>&reg;</sup>, visit their <a href="http://www.changeforchange.org" onclick="window.open('http://www.changeforchange.org', '_blank'); return false;">website</a></strong>.</p>
		        
		        <p id="disclaimer" class="disclaimer">*One contribution per day, per person. Voting begins daily at 12:01 am EDT until program ends. Bior&eacute;<sup>&reg;</sup> Skincare will donate $5,000 to the charity with the most "change" in their jar as of 08/24/2012. The runner-up charity will receive a $2,000 donation.</p>
		        <p id="disclaimerCFC" class="disclaimer disclaimerCFC">"Change for Change<sup>&reg;</sup>" is a registered trademark licensed by Change for Change, Inc. and is used under license.</p>
		    </div>
			
			<!-- Modal Dialog + Overlay -->
			<div id="entireModal">
			
			    <div id="voteModalOverlay"></div>
			    
			    <div id="voteModalContainer" class="voteModalContainer">
			        <div class="modalBGtop">
			            <a href="#" id="closeModal" title="Close">Close</a>
			        </div>
			        <div class="modalContent">
			            <div id="voteSuccess">
			                <h4>Thank you!</h4>
		                    <p id="yourSupport" class="yourSupport">
		                        You've supported <span id="yourVote">Charity Name</span><span id="yourStatus"> charity status</span>
		                    </p>
		                </div>
		                
		                <div id="tryTomorrow">
		                    <h4>We Appreciate The Enthusiasm</h4>
		                    <p>But we've already counted your contribution for the day. Come back again tomorrow to deposit "change" for the charity of your choice!</p>
		                </div>
		                    
		                    <div class="voteCount">
		                        <div class="vvTotalWrapper">
		                            <h5 class="vvTotalH">Vital Voices</h5>
		                            <span id="vvVotesTotal" class="vvVotesTotal"></span>
		                        </div>
		                        <div class="pfTotalWrapper">
		                            <h5 class="pfTotalH">Petfinder</h5>
		                            <span id="pfVotesTotal" class="pfVotesTotal"></span>
		                        </div>
		                    </div>
		                
		                <p id="daysLeft" class="daysLeft">You have <span id="numDaysLeft">[X] days</span> left to show your support!</p>
		                
		            </div> <!-- /.modalContent -->
		            <div class="modalBGbottom"></div>
		        </div> <!-- /#voteModalContainer -->
		    </div> <!-- /#entireModal -->
		    <div id="jplayer"></div>
		</div>
	</div>

    <%=SquishIt.Framework.Bundle.JavaScript()
		.Add("~/facebook/SeeTheChange/js/jquery-1.7.2.min.js")
        .Add("~/facebook/SeeTheChange/js/jquery.cookie.js")
        .Add("~/facebook/SeeTheChange/js/colorbox.min.js")
        .Add("~/facebook/SeeTheChange/js/swfobject.min.js")
        .Add("~/js/jquery.ENLvideo.js")
        .Add("~/facebook/SeeTheChange/js/jquery.crSpline.js")
        .Add("~/facebook/SeeTheChange/js/jquery.jplayer.min.js")
		.Add("~/facebook/SeeTheChange/js/SeeTheChange.js")
		.Render("~/facebook/SeeTheChange/js/combinedSeeTheChange_#.js")
	%>

	<script type="text/javascript">
	  	FBVars = {
	  		fbAppId: '<%= System.Configuration.ConfigurationManager.AppSettings["SeeTheChange_FBAppId"] %>',
	  		FBShareUrl: '<%= System.Configuration.ConfigurationManager.AppSettings["SeeTheChange_FBShareURL"] %>',
	  		baseURL: '<%= System.Configuration.ConfigurationManager.AppSettings["SeeTheChange_ServerBaseUrl"]%>'
	  	};
	  	(window.JSON && typeof JSON.stringify === 'function' && typeof JSON.parse === 'function') || document.write(unescape("%3Cscript src='" + 'js/json2.min.js' + "'%3E%3C/script%3E"));

	  	stc.init();

	  	/* Flash Tracking */
	  	function riaTrack(trackingParam) {
	  	    goToPage(trackingParam);
	  	}

	  	function goToPage(pg) {
	  	    firstTracker._trackPageview(pg);
	  	    secondTracker._trackPageview(pg);
	  	}
		
    </script>
</asp:Content>
