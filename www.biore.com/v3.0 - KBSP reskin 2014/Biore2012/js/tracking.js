﻿/*------------------------------------------*/
/* GA Event Tracking                        */
/* Mike Taylor - CG Marketing Communications*/
/*-------------------- ---------------------*/
var tpj = jQuery;
// tpj.noConflict();

tpj(document).ready(function () {
    Tagging.init();
});

tpj(function () {
    var urlPath = window.location.pathname;
    var urlPathArray = urlPath.split('/');
    var urlPathCount = urlPathArray.length - 1;
    var urlPageName = urlPathArray[urlPathCount];
    if (urlPageName == "")
        urlPageName = urlPathArray[(urlPathCount - 1)]; //if no filename found in ending path, move 1 step back in URL tree.

    var section = "";
    var subsection = "";
    var event = "";

    //Tagging for product on Product Page 
    tpj(".productListSpacer a").click(function () {
        var productName = $(this).attr("id");
        productName = productName.replace("details-", "");
        productName = productName.replace(/-/g, " ");
        Tagging.tracking("Our Products", "Click", productName);
    });

    tpj(".popup-contents a").click(function () {
        Floodlight.flood(this);
    });

    // click event for accordian on Product Detail
    tpj(".contentHolder h3").click(function () {

        str = tpj(this).attr("id");

        section = urlPageName;
        subsection = str;
        event = "Click";

        //trackEvent(section, subsection, event);
        Tagging.tracking("",event,section)
    });

    //click event for homepage theater circle nav
    /*tpj(".flex-control-nav li a").click(function () {

        var theaterSlides = [];
        var theaterCount = 0;

        tpj(".slides li a").each(function () { theaterSlides.push(tpj(this).attr("id")); theaterCount++; });

        for (i = 0; i <= theaterCount; i++) {
            if ((parseInt(tpj(this).text()) - 1) == i) {
                section = urlPageName;
                subsection = "circle-navigate-" + theaterSlides[i].replace("ctl00_ContentPlaceHolder1_", "");
                event = "click";
                trackEvent(section, subsection, event);

            }

        }

    });*/

    //click on change country link
    tpj('#footer-change-country').click(function () {
        ga('send', 'event', 'Change Country', 'Click', 'Change Country', '1', { 'nonInteraction': 1 })
    });

    //click event for buy now pop-up in product detail pages
    tpj("#theBuyNowLink").click(function () {

        temp = urlPageName;
        name = temp.replace(/-/g," ");
        section = "Buy Buttons";
        event = "Click";

        Tagging.tracking(section, event, name);

    });

    //look for a "GATrack" class.  
    //e.g. "GATrack-IdentifierCategory-IdentifierSubCategory"
    tpj('A[class*="GATrack"]').click(function () {

        var trackClass = tpj(this).attr('class');
        var ary = trackClass.split("-");

        section = "";
        subsection = "";
        event = "";
        if (ary[0] != undefined)
            section = ary[0];
        if (ary[1] != undefined)
            subsection = ary[1];
        if (ary[2] != undefined)
            event = ary[2];

        trackEvent(section, subsection, event);

    });

    Floodlight.event();

    tpj('.fma2Face').click(function () {
        //_gaq.push(['_trackEvent', 'Biore.com', 'click', 'ProveIt Facebook CTA', 1, true]);
        trackEvent('Biore.com', 'ProveIt Facebook CTA', 'click');
    });
});

function trackPage(pageName){
	//alert("Tracking: " + pageName);
    //pageTracker._trackPageview(pageName);
    //_gaq.push(['_trackPageview', pageName]);
    firstTracker._trackPageview(pageName);
    secondTracker._trackPageview(pageName);
}

/* SUBSECTION IS VALIDATING IF THIS IS ABOUT /where-to-buy-biore/ PAGE*/
function trackEvent(section, subsection, event){
    if (section == undefined) {
        section = "";
    }
    if (subsection == undefined) {
        subsection = "";
    } else {
        subsectionSplit = subsection.split("_");
        if (subsectionSplit[0] == "lvRetailer") {
            lvRetailerTracking();
        }
    }
    if (event == undefined) {
        event = "";
    }

    firstTracker._trackEvent(section, subsection, event);
    secondTracker._trackEvent(section, subsection, event);
}

function trackSpotlight(url){
	// for doublelick-related tags
	//alert("Spotlight: " + url);

	var axel = Math.random()+"";
	var a = axel * 10000000000000;
	
	var img = document.createElement('img');
	img.src = url + ';num=' + a;	
}

function getDataTracking() {
    
    var productPatch = window.location.pathname.split('/');

    var productPatchLenght = productPatch.length - 1;
    
    if(productPatch[productPatchLenght] == "default.aspx")
        var productName = productPatch[(productPatchLenght - 1)];
    else
        var productName = productPatch[productPatchLenght];

    if (!productName) {
       productName = productPatch[(productPatchLenght - 1)];
    }

    var dataTracking;
    switch (productName) {
        case "blemish-fighting-ice-cleanser":
            dataTracking = "biore530|KAO_B0";
            break;
        case "blemish-fighting-astringent-toner":
            dataTracking = "biore530|KAO_B000";
            break;
        case "acne-clearing-scrub":
            dataTracking = "biore530|KAO_B00";
            break;
        case "self-heating-one-minute-mask":
            dataTracking = "biore530|KAO_B002";
            break;
        case "combination-skin-balancing-cleanser":
            dataTracking = "biore530|KAO_B004";
            break;
        case "daily-cleansing-cloths":
            dataTracking = "biore530|KAO_B005";
            break;
        case "deep-pore-charcoal-cleanser":
            dataTracking = "biore530|KAO_B001";
            break;
        case "pore-detoxifying-foam-cleanser":
            dataTracking = "biore530|KAO_B007";
            break;
        case "make-up-removing-towelettes":
            dataTracking = "biore530|KAO_B006";
            break;
        case "pore-unclogging-scrub":
            dataTracking = "biore530|KAO_B003";
            break;
        case "email-newsletter-sign-up":
            dataTracking = "biore530|KAO_B009";
            break;
        case "where-to-buy-biore":
            dataTracking = "biore530|KAO_B008";
            break;
        case "pore-strips-combo":
            dataTracking ="biore530|biore943";
            break;
        case "pore-strips-ultra":
            dataTracking = "biore530|biore106";
            break;
        case "pore-strips":
            dataTracking = "biore530|biore437";
            break;
        case "warming-anti-blackhead-cleanser":
            dataTracking = "biore530|deepc128";
            break;
        default:
            dataTracking = null;
    }

    return dataTracking;
}

/* ACTIVITY TRACKING FOR lvRetailer #ID IN /where-to-buy-biore/ PAGE */
function lvRetailerTracking() {

    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    var iframe = ' <iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=KAO_B008;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe> ';
    $('body').append(iframe);
}

var Floodlight = {

    flood: function (element) {
        var axel = Math.random() + "";
        var a = axel * 10000000000000;
        var tagsdata = getDataTracking();
        if (tagsdata) {
            var tagsdata = tagsdata.split('|');
            var iframe =' <iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=' + tagsdata[0] + ';cat=' + tagsdata[1] + ';ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe> ';
            $('body').append(iframe);
        }
    },
    event: function () {
        $('.buyNow').click(function (e) {
            e.preventDefault();
            Floodlight.flood(this);
        });
    }
}

var Tagging = {
    tracking: function (section, eventType, name) {
        //Fire first tracking
        ga('send', 'event', section, eventType, name, '1', { 'nonInteraction': 1 });

        //Fire second tracking (accessible by name)
        ga('KAOGlobal.send', 'event', section, eventType, name, '1', { 'nonInteraction': 1 });


    },
    linkTrack: function () {

        var anchor = this;
        tpj('body').on('click', '.track', function (e) {
            e.preventDefault();

            //Section | eventType | name
            var dataTracking = tpj(this).attr("data-tracking");
            var uri = tpj(this).attr("href");
            var target = tpj(this).attr("target");
            var attributes = dataTracking.split("|");
            var element = tpj(this);

            anchor.tracking(attributes[0], attributes[1], attributes[2]);

            var isRedirect = tpj(this).hasClass('redirect');
            setTimeout(function () {
                if (uri && uri != "#" && isRedirect == false) {
                    if (target) {
                        window.open(uri, "_blank");
                    } else if (!anchor.prevent(element)) {
                        window.location = uri;
                    }
                } else {

                   
                }
            }, 500);

        });
    },
    prevent: function (element) {
        var prevent = false;
        if (element.hasClass("prevent")) {
            prevent = true;
        }
        return prevent;
    },
    init: function () {
        this.linkTrack();
    }
}