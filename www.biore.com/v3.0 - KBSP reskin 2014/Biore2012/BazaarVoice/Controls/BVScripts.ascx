﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BVScripts.ascx.cs" Inherits="Biore.BazaarVoice.Controls.BVScripts" %>

<%= BVProductScript %>
<script type="text/javascript">
    function BVRoiBeacon(product) {
        // BV ROI Beacon
        window._bvapiq = window._bvapiq || [];
        var ConversionData = {
            clientID: 'BioreUS',
            environment: 'staging',
            dc: "16616",
            host: "qas.biore.com",
            type: 'BuyNowConversion',
            label: 'Buy Now',
            value: product
        };
        _bvapiq.push(['Conversion', ConversionData]);
        $BV.SI.trackConversion({
            "type": "BuyNowConversion",
            "label": "Buy Now",
            "value": product
        });
    }
</script>