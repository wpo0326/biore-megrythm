﻿<%@ Page Title="Get a Clean Face | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Get healthy, radiant skin with products from Bioré® Skincare" name="description" />
    <meta content="skincare, clean face" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/UniversPro67.css")
        .Add("~/css/homepage.css")
        .Add("~/css/flexslider.css")
        .Render("~/CSS/combinedhome_#.css")
    %>
    <meta name="viewport" content="width=device-width">


    <script type="text/javascript">
        $(document).ready(function () {
            //$('.scroll-dots a').click(function (event) {
            //    event.preventDefault();
            //    var target = $(this).attr('id');
            //    console.log(target);
            //});
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $('#theaterItem1 a').click(
                function (e) {
                    e.preventDefault();

                    var axel = Math.random() + "";
                    var a = axel * 10000000000000;
                    var strScript = "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore551;ord='" + a + "'?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"
                               + "<noscript>"
                               + "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore551;ord=1?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"
                               + "</noscript>";

                    $('body').prepend(strScript);
                    var target = $(this).attr('target');
                    var uri = $(this).attr('href');

                    setTimeout(function () {
                        if (target) {
                            window.open(uri, '_blank');
                        } else {
                            window.location = uri;
                        }
                    }, 1000);

                });
        })
    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">


    <script type="text/javascript">
        var axel = Math.random() + "";
        var a = axel * 10000000000000;
        document.write('<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore916;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
    <noscript>
<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore916;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>

    <!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main1">
        <div class="home-content">
            <section id="section-2017-interim">
                <div class="top-baking-soda-2017">
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/top-right-bubbles.png") %>" alt="top right bubbles" /></div>
                <div class="welcome-module clearfix">
					<div style="position: absolute; margin: 70px auto 0; text-align: center; width: 100%; cursor: pointer;"><a href="/en-US/biore-facial-cleansing-products/"><img src="images/spacer.gif" height="510" width="300" /></a></div>
                     <div class="header-2017">
                        <div class="acne-header-2017">
                            <h2>
                                <span class="acnes foamparty">Kiss Your <span class="cateye">Contour-<br />Cat-Eye-Lip-Kit</span></span>
                              
                            </h2>
                            <h2 style="margin-top: 14px;">
                                <span class="acnessmall">Goodbye!</span>

                            </h2>
                        </div>
                       
                    </div>

                    <div class="left-text">
                        <div>
                           <span class="new">New </span> Bior&eacute;<sup>&reg;</sup><br />Cleansing<br />Micellar Waters

                        </div>

                        <!--<div class="allInOneCleansing">All-in-One Cleansing!</div>-->

                    </div>

                     <div class="right-text">
                         <div>
                         All-in-one cleanser<br />and makeup remover
                              <span class="waterproof"> Your Pores<br />(And Pillowcase)<br />Will Thank You</span>

                         </div>

                     </div>

                     <div class="footer-2017">
                        <div class="acne-footer-2017">
                            <!--*Garnier Skin Active Micellar Water All-In-1 Waterproof, the leading Micellar Water according to Neilsen Scanning Data, 52 Weeks Ending April 2018.-->
                        </div>
                    </div>


                    <div class="hide-mobile"></div>
                    <div class="scrollDownHldr">
                        <p class="scrollDown">SCROLL DOWN</p>
                        <div id="arrowBounce">
                            <a href="#section-2018-products">
                                <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/arrowDown.png") %>" class="section-down-arrow" /></a>
                        </div>

                    </div>
                </div>

                <div class="scroll-dots">
                    <a href="#section-2017-interim"><span class="scroll-dot enable"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                    <a href="#section-01"><span class="scroll-dot"></span></a>
                    <a href="#section-04"><span class="scroll-dot"></span></a>
                    <a href="#section-05"><span class="scroll-dot"></span></a>
                    <a href="#section-03"><span class="scroll-dot"></span></a>
                    <a href="#section-02"><span class="scroll-dot"></span></a>
                </div>
                <div class="bottom-charcoal-2017">
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/bottom-left-bubbles.png") %>" alt="bottom left bubbles" /></div>
            </section>
            <section class="home-section" id="section-2018-products">
                <h4>Removes Stubborn Makeup, Deep Cleans & Clarifies Pores<br />
<span class="waterproof">(Lookin' at you, Waterproof Mascara!)</span><br /></h4>
                <div class="container3">

                    <div class="prodList">
                        
                        <div class="videoHolder">
                          <div class="video pie">
                              <div id="video1">
                                  <a class="popup-youtube" href="https://www.youtube.com/watch?v=WpB3BFp4kHE">
                                      <img src="/en-US/images/homepage/homepage-video-still.jpg" alt="Shay Micellar Water" />
                                  </a>
                              </div>
                          </div>
                      </div>
                        <!--<div class="disclaimer">*Garnier Skin Active Micellar Water All-In-1 Waterproof, the leading Micellar Water according to Neilsen Scanning Data, 52 Weeks Ending April 2018.</div>-->
                        <!--<ul>
                            <li class="productListSpacer">
                                <a href="/en-US/take-it-all-off/baking-soda-cleansing-micellar-water"><img src="images/ourProducts/products/small/baking-soda-cleansing-micellar-water.png" alt="BAKING SODA CLEANSING MICELLAR WATER" /></a>
                                <h3>BAKING SODA CLEANSING<br />MICELLAR WATER</h3>
                                <p>Specially formulated for combination skin. Removes makeup, deep cleans pores and balances without over-drying.
  <a href="/en-US/take-it-all-off/baking-soda-cleansing-micellar-water" class="buynowbtn"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/micellar-products-button.png") %>" alt="button"/></a></p>
         
                               
                            </li>
                            <li class="productListSpacer">
                                <a href="/en-US/take-it-all-off/charcoal-cleansing-micellar-water"><img src="images/ourProducts/products/small/charcoal-cleansing-micellar-water.png" alt="CHARCOAL CLEANSING MICELLAR WATER" /></a>
                                <h3>CHARCOAL CLEANSING<br />MICELLAR WATER</h3>
                                <p>Specially formulated for oily skin. Removes makeup, deep cleans pores and removes excess oil.
 <a href="/en-US/take-it-all-off/charcoal-cleansing-micellar-water" class="buynowbtn"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/micellar-products-button.png") %>" alt="button"/></a></p>

                    
                            </li>
                        </ul>-->
                    </div>
                </div>

                <div class="scroll-dots">
                    <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot enable"></span></a>
                    <a href="#section-01"><span class="scroll-dot"></span></a>
                    <a href="#section-04"><span class="scroll-dot"></span></a>
                    <a href="#section-05"><span class="scroll-dot"></span></a>
                    <a href="#section-03"><span class="scroll-dot"></span></a>
                    <a href="#section-02"><span class="scroll-dot"></span></a>
                </div>
            </section>
            <section class="home-section iex" id="section-01">
                <div class="welcome-module">
                    <!--<video autoplay="autoplay" loop="loop" muted="muted" preload="auto" id="welcomeVideo">
                    <source src="<%= VirtualPathUtility.ToAbsolute("~/video/homepage/shayzilla.mp4") %>" type="video/mp4"/>
                    <source src="<%= VirtualPathUtility.ToAbsolute("~/video/homepage/shayzilla.ogv") %>" type="video/ogg"/>
                    <source src="<%= VirtualPathUtility.ToAbsolute("~/video/homepage/shayzilla.webm") %>" type="video/webm"/>
                </video>-->
                    <div id="welcomeImage" class="hide-mobile show" style="height: 100%">
                        <img class="ïmg-less" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/shay.png") %>" />
                    </div>
                    <div id="welcomeImage" class="hide-desktop">
                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/shayzilla.png") %>" />
                    </div>
                    <div class="welcome-module-content">
                        <h1>FIND YOUR WAY TO<br>
                            <span>COME CLEAN</span></h1>
                        <p>Dirty little secrets—we&rsquo;ve all got &rsquo;em. Dirt, oil and blackheads hiding in our pores. Let Bior&eacute;<sup>&reg;</sup> beauty and actress Shay Mitchell show you how to clean them away in&nbsp;these&nbsp;exclusive&nbsp;videos.</p>
                        <div class="welcome-video hide-desktop">
                            <a href="https://www.youtube.com/embed/TVBZqfDfAgE?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a>
                        </div>
                        <div class="animation hide-mobile overwritte">
                            <div class="animation-container">
                                <div><a href="https://www.youtube.com/embed/TVBZqfDfAgE?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                <a href="https://www.youtube.com/embed/TVBZqfDfAgE?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm  fancybox.iframe">
                                    <!--<img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/Biore_Intro.gif") %>">-->
                                    <img class="gif-holder overwritte-gif" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/Biore_Intro.gif") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/Biore_Intro.gif") %>">
                                </a>
                                <div class="animation-play-btn overwritte-btn">
                                    <a href="https://www.youtube.com/embed/TVBZqfDfAgE?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm  fancybox.iframe">
                                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>" />
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="hide-mobile">
                    </div>
                    <div class="bubbles-bg hide-mobile" style="width: 34%">
                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/bubbles-web.png") %>" style="float: right" />
                        <img class="products-responsive" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/product.png") %>" />
                    </div>
                </div>
                <div class="scroll-dots">
                    <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                    <a href="#section-01"><span class="scroll-dot enable"></span></a>
                    <a href="#section-04"><span class="scroll-dot"></span></a>
                    <a href="#section-05"><span class="scroll-dot"></span></a>
                    <a href="#section-03"><span class="scroll-dot"></span></a>
                    <a href="#section-02"><span class="scroll-dot"></span></a>
                </div>
                <div class="white-shadow hide-mobile"></div>
            </section>
            
            <section class="home-section"" id="section-04">
            <div class="bg-left"></div><div class="bg-right"></div>
            <div class="container3">
	            <div class="container2">
		            <div class="container1">
			            <div class="headline">
				            <h4>BE <br>MAG-<br>NETIC</h4>
			            </div>
			            <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/embed/e9Hw1XdJwio?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                <a href="https://www.youtube.com/embed/e9Hw1XdJwio?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/03-charcoal-strips.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/03-charcoal-strips.gif") %>">                                
                                </a>
                                <div class="animation-skin-container">
                                    <div class="animation-skin"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/03_charcoal_pore_strips.png") %>"/></div>
                                </div>
                                    <div class="animation-play-btn">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>
                                </div>
                            </div>
			            </div>
			            <div class="product">
				            <p>Bior&eacute;<sup>&reg;</sup> Deep Cleansing Charcoal Pore Strips</p>
			            </div>
		            </div>
	            </div>
            </div>

                <div class="scroll-dots">
                    <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                    <a href="#section-01"><span class="scroll-dot"></span></a>
                    <a href="#section-04"><span class="scroll-dot enable"></span></a>
                    <a href="#section-05"><span class="scroll-dot"></span></a>
                    <a href="#section-03"><span class="scroll-dot"></span></a>
                    <a href="#section-02"><span class="scroll-dot"></span></a>
                </div>
        
        </section>
        
        <section class="home-section float-right" id="section-05">
                <div class="bg-left"></div>
                <div class="bg-right"></div>
                <div class="container3">
                    <div class="container2">
                        <div class="container1">
                            <div class="headline">
                                <h4>DRAW OUT THE DIRT</h4>
                            </div>
                            <div class="animation">
                                <div class="animation-container">
                                    <div class="animation-border"><a href="https://www.youtube.com/embed/xhStsDgvRMM?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                    <a href="https://www.youtube.com/embed/xhStsDgvRMM?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                        <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/04-charcoal-pore-cleanser.gif") %>">
                                        <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/04-charcoal-pore-cleanser.gif") %>">
                                    </a>
                                    <div class="animation-skin-container">
                                        <div class="animation-skin">
                                            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/04_deep_pore_charcoal_cleanser.png") %>" /></div>
                                    </div>
                                    <div class="animation-play-btn">
                                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>" />
                                    </div>
                                </div>
                            </div>
                            <div class="product">
                                <p>Bior&eacute;<sup>&reg;</sup> Deep Pore Charcoal Cleanser</p>
                            </div>
                        </div>
                    </div>
                </div>

               <div class="scroll-dots">
                    <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                    <a href="#section-01"><span class="scroll-dot"></span></a>
                    <a href="#section-04"><span class="scroll-dot"></span></a>
                    <a href="#section-05"><span class="scroll-dot enable"></span></a>
                    <a href="#section-03"><span class="scroll-dot"></span></a>
                    <a href="#section-02"><span class="scroll-dot"></span></a>
                </div>
            </section>
            
             <section class="home-section float-right" id="section-03">
                <div class="bg-left"></div>
                <div class="bg-right"></div>
                <div class="container3">
                    <div class="container2">
                        <div class="container1">
                            <div class="headline">
                                <h4>BRING ON<br>
                                    THE BUBBLY</h4>
                            </div>
                            <div class="animation">
                                <div class="animation-container">
                                    <div class="animation-border"><a href="https://www.youtube.com/embed/_RyteX6GhNA?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                    <a href="https://www.youtube.com/embed/_RyteX6GhNA?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                        <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/02-baking-soda-cleanser.gif") %>">
                                        <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/02-baking-soda-cleanser.gif") %>">
                                    </a>
                                    <div class="animation-skin-container">
                                        <div class="animation-skin">
                                            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/02_baking_soda_pore_cleanser.png") %>" /></div>
                                    </div>
                                    <div class="animation-play-btn">
                                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>" />
                                    </div>
                                </div>
                            </div>
                            <div class="product">
                                <p>Bior&eacute;<sup>&reg;</sup> Baking Soda Pore Cleanser</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="scroll-dots">
                    <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                    <a href="#section-01"><span class="scroll-dot"></span></a>
                    <a href="#section-04"><span class="scroll-dot"></span></a>
                    <a href="#section-05"><span class="scroll-dot"></span></a>
                    <a href="#section-03"><span class="scroll-dot enable"></span></a>
                    <a href="#section-02"><span class="scroll-dot"></span></a>
                </div>
            </section>
        
            <section class="home-section" id="section-02">
                <div class="bg-left"></div>
                <div class="bg-right"></div>
                <div class="container3">
                    <div class="container2">
                        <div class="container1">
                            <div class="headline">
                                <h4>GET FIZZY WITH IT</h4>
                            </div>
                            <div class="animation">
                                <div class="animation-container">
                                    <div class="animation-border"><a href="https://www.youtube.com/embed/B8bmBAlKF14?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                    <a href="https://www.youtube.com/embed/B8bmBAlKF14?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                        <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/01-baking-soda-scrub.gif") %>">
                                        <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/01-baking-soda-scrub.gif") %>">
                                    </a>
                                    <div class="animation-skin-container">
                                        <div class="animation-skin">
                                            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/01_baking_soda_cleansing_scrub.png") %>" /></div>
                                    </div>
                                    <div class="animation-play-btn">
                                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>" />
                                    </div>
                                </div>
                            </div>
                            <div class="product">
                                <p>Bior&eacute;<sup>&reg;</sup> Baking Soda Cleansing Scrub</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="scroll-dots">
                    <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                    <a href="#section-01"><span class="scroll-dot"></span></a>
                    <a href="#section-04"><span class="scroll-dot"></span></a>
                    <a href="#section-05"><span class="scroll-dot"></span></a>
                    <a href="#section-03"><span class="scroll-dot"></span></a>
                    <a href="#section-02"><span class="scroll-dot enable"></span></a>
                </div>
            </section>
           
            
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/jquery.flexslider.min.js")
        .Render("~/js/combinedhome_#.js")
    %>

    <script type="text/javascript" src="<%= VirtualPathUtility.ToAbsolute("~/js/homepage.js") %>"></script>

    <!--Begin Mediavest DMP-->
    <script type="text/javascript" src="/en-US/js/kao-VivaKiDIL_6.4.js"></script>
    <!--End Mediavest DMP-->
</asp:Content>
