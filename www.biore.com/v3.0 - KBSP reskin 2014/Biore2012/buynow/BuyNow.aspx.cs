﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

namespace Biore2012.buy_now
{
    public partial class BuyNow : System.Web.UI.Page
    {
        public string strPageURL = HttpContext.Current.Request.ServerVariables["URL"].ToString();
        public string strQueryString;
        public string strProductSku;
        public string strProductRefName;

        protected void Page_Load(object sender, EventArgs e)
        {
            zip.Attributes.Add("onclick", "javascript:this.value='';");
            zipBtn.Attributes.Add("onclick", "trackPage('/click/" + strPageURL.Replace(".aspx", "").ToString() + "/zip_code_submit')");

            if (!IsPostBack)
            {
                strProductRefName = Request.QueryString["RefName"] != null ? Request.QueryString["RefName"].ToString() : "";

                if (strProductRefName != "")
                {
                    ShowOnlineRetailers(strProductRefName);
                }
                else if (Request.QueryString["ziprequest"] != null && Request.QueryString["ziprequest"].ToString() != "")
                {
                    ShowBrickAndMortarRetailers(Request.QueryString["productrefname"].ToString());
                }
            }
            else if (IsPostBack && zip.Text != "Enter Zip Code" && zip.Text != "")
            {
                strProductSku = hfProductSku.Value;
                strProductRefName = Server.UrlEncode(hfProductRefName.Value);

                BrickAndMortarRedirect(strProductSku, strProductRefName, zip.Text);

            }
            else
            {
                if (OnlineRetailers.Visible)
                    ShowOnlineRetailers(hfProductRefName.Value);
                else
                    ShowBrickAndMortarRetailers(hfProductRefName.Value);
            }

        }

        public void ShowOnlineRetailers(string RefName)
        {
            strProductRefName = RefName;
            hfProductRefName.Value = strProductRefName;
            GetCIData(strProductRefName);

            BrickAndMortarRetailers.Visible = false;
        }

        public void ShowBrickAndMortarRetailers(string RefName)
        {

            hfProductRefName.Value = RefName;

            OnlineRetailers.Visible = false;

            litReturnToOnlineRetailer.Text = "<a href=\"BuyNow.aspx?RefName=" + Server.UrlEncode(RefName) + "\" class=\"ci-view-online-retailers\">View Online Retailers</a>";

            if (RefName != "")
                GetCIData(RefName);

        }

        public void BrickAndMortarRedirect(string ProductSku, string RefName, string Zip)
        {
            //change RGID code from 2090 to 6382 for local call
            strQueryString = "&cii_nIID=-1&cii_sSKU=" + ProductSku + "&cii_sCT=&cii_nRID=-1&cii_nRGID=6382&cii_nVID=-1&cii_nDSID=-1&cii_nPGID=-1&cii_sCountry=US&cii_sCDATA=&cii_sSort=&cii_sSortOrder=&cii_sZip=" + Zip + "&cii_nRadius=50";
            Response.Redirect("BuyNow.aspx?ziprequest=true&productrefname=" + RefName + strQueryString);
        }

        protected void GetCIData(string ProductRefName)
        {
            try
            {
                XmlDocument navDoc = new XmlDocument();
                navDoc.Load(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["path"].ToString() + "buynow/buynow.xml"));

                string strNodeRoot = "Products/Product[ProductRefName = '" + ProductRefName.ToLower() + "']";

                string strProductName = navDoc.SelectSingleNode(strNodeRoot + "/ProductShortName").InnerText.ToString();
                string strProductSku = navDoc.SelectSingleNode(strNodeRoot + "/SKU").InnerText.ToString();
                string strProductSmallImg = navDoc.SelectSingleNode(strNodeRoot + "/ImageCollection/Item/FileName").InnerText.ToString();
                string strProductSmallImgTxt = navDoc.SelectSingleNode(strNodeRoot + "/ImageCollection/Item/AltText").InnerText.ToString();

                litProductImage.Text = "<img src=\"" + System.Configuration.ConfigurationManager.AppSettings["path"].ToString() +  "/images/ourProducts/products/large/" + strProductSmallImg + "\" alt=\"" + strProductSmallImgTxt + "\" class=\"images\" />";

                litProductName1.Text = litProductName2.Text = strProductName;
                ////litCIBuyNow.Text = "function cii_embed() { cii_EmbedProductLinks('kao','" + ProductSku + "',6370,'');} var initCII = setTimeout ( cii_embed, 0 );";
                litCIBuyNow.Text = "cii_EmbedProductLinks('kao','" + strProductSku + "',6370,'');";
                hfProductSku.Value = strProductSku;
            }
            catch (Exception e)
            {
                Response.Write("error" + e);
                //Invalid XML file.  Do nothing.
            }
        }

        public void ButtonClick_Zip(object sender, EventArgs e)
        {

        }
    }
}