﻿<%@ Page Title="Bior&eacute;&reg; Skincare | Promotion Over" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PromotionOver.aspx.cs" Inherits="Biore2012.FormConfig.PromotionOver" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Bior&eacute;&reg; Skincare | Promotion Over" name="description" />
    <meta content="" name="keywords" />
 <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedrules_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <div id="polaroid"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/forms/signUpPhoto.jpg") %>" alt="" /></div>
                <div id="content">
                    <h1>We're Sorry</h1>
                    
                    <p>Unfortunately, the promotion you are currently looking for has expired. We appreciate your interest.</p>
                    <p>You can <a href="<%= VirtualPathUtility.ToAbsolute("~/email-newsletter-sign-up") %>">sign up</a> to be notified of future Bioré® Skincare offers and promotions, or go to the <a href="<%= VirtualPathUtility.ToAbsolute("~/") %>">Bioré® Skincare homepage</a>.</p>

                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
