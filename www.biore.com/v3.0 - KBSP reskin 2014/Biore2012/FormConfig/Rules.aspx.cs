﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Biore2012.FormConfig {
    public partial class Rules : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            Site myMaster = (Site)this.Master;
            myMaster.bodyClass += " utility rules";
        }
    }
}
