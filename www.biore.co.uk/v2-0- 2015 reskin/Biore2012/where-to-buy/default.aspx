﻿<%@ Page Title="Where to Buy Bioré Products - Bioré" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.wheretobuy._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="If you're looking to buy Bioré products then read on, as the following page shows which nationwide UK retailers stock Bioré products." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
	#storeLogos ul li { width:240px; height:120px; float:left; display:block; text-align:center;}
    #storeLogos ul li a {width:180px; height:100px; background-position:center left;text-indent:-9999px; display:block;}
	#storeLogos ul li a:hover { background-position:right;}
	</style>
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1>Where To Buy</h1>
                    <div id="storeLogos" class="logos">
                        <p>Shop for Bior&eacute;<sup>&reg;</sup> products at a retailer near you:<br /><br /><br /></p>
                        <ul>
                            <li><a style="background-image: url(../images/whereToBuy/amazon.png);" href="https://www.amazon.co.uk/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=biore+uk&rh=i%3Aaps%2Ck%3Abiore+uk" target="wtb">amazon</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/boots.png);" href="http://www.boots.com/webapp/wcs/stores/servlet/SolrSearchLister?storeId=10052&langId=-1&catalogId=11051&stReq=1&searchTerm=biore#container" target="wtb">boots</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/superdrug.png);" href="http://www.superdrug.com/search?text=biore" target="wtb">superdrug</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/sainsburys.png);" href="http://www.sainsburys.co.uk/webapp/wcs/stores/servlet/SearchDisplayView?catalogId=10122&langId=44&storeId=10151&krypto=23nuGUmwWBxzrqwqVb5dNacsGIYTjTLADBeP4A%2FoX1WIbJeQnWxn32WUcCg1OKN3frH4KPn40rtQD84xKVHE1NafLVKrn8pvutn6t3hPMB7rE0FLZdq%2BjhwojxpC2cMmTCK4EawDbptzBt9Sug2yCkKsSxnYSgy7dfUeqUY6pzrh8UuxCtFZdmqu%2BCIZkOffs9w4tfmb5ONundZQ3iMKkUDTv0dw5T5q9KulwpsbZoktmIPA4BTyCW5qRTjAGcZ26jNcVXRK4nS6cSnOofJypw%3D%3D#langId=44&storeId=10151&catalogId=10122&categoryId=&parent_category_rn=12448&top_category=&pageSize=30&orderBy=RELEVANCE&searchTerm=biore&beginIndex=0&hideFilters=true&categoryFacetId1" target="wtb">sainsburys</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/asda.png);" href="http://groceries.asda.com/asda-webstore/landing/home.shtml#/search/biore" target="wtb">asda</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/waitrose.png);" href="http://www.waitrose.com/shop/HeaderSearchCmd?searchTerm=biore&defaultSearch=GR&search=/" target="wtb">waitrose</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/ocado.png);" href="https://www.ocado.com/webshop/getSearchProducts.do?clearTabs=yes&isFreshSearch=true&entry=biore" target="wtb">ocado</a></li>
                            <!--<li><a style="background-image: url(../images/whereToBuy/wilko.png);" href="http://www.wilko.com/" target="wtb">wilko</a></li>-->
                            <li><a style="background-image: url(../images/whereToBuy/tesco.png);" href="http://www.tesco.com/groceries/product/search/default.aspx?searchBox=biore&icid=tescohp_sws-1_biore/" target="wtb">tesco</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/feelunique.png);" href="http://www.feelunique.com/brands/biore?q=BIORE" target="wtb">feelunique</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/morrisons.png);" href="https://groceries.morrisons.com/webshop/getSearchProducts.do?clearTabs=yes&isFreshSearch=true&chosenSuggestionPosition=&entry=biore" target="wtb">morrisons</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/helloskin.png);" href="https://helloskinshop.co.uk/collections/biore" target="wtb">hello skin</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/chemist_direct.png);" href="https://www.chemistdirect.co.uk/search/go?w=biore" target="wtb">ChemistDirect</a></li>
                           
                        </ul>                
                    </div>
                    <div style="clear:both; height:0px;"></div>
            </div>
        </div>
    </div>
</asp:Content>