﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Facebook;

namespace Biore2012.facebook.AlloysDatingRules
{
	public partial class Default : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			// Decode the Signed Request object to check for an app_data parameter
			string signedReqJson = "";
			FacebookSignedRequest signedReqObj = new FacebookSignedRequest();
			decodeFBSignedRequest(ref signedReqJson, ref signedReqObj);

			if (signedReqObj.app_data != null && signedReqObj.app_data.Contains("topten"))
			{
				Response.Redirect("TopTenDatingRules.aspx");
			}
		}

		private static void decodeFBSignedRequest(ref string signedReqJson, ref FacebookSignedRequest signedReqObj)
		{
			signedReqJson = FBUtils.decodeSignedReq();
			signedReqObj = FBUtils.serializeSignedRequestJSON<FacebookSignedRequest>(signedReqJson);

			FBUtils.setSignedRequestSession(signedReqJson, signedReqObj);
		}
	}
}
