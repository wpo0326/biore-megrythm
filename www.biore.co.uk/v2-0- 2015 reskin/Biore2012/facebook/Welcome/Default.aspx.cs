﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Facebook;

namespace Biore2012.facebook.Welcome
{
	public partial class Default : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			string signedReqJson = "";
			FacebookSignedRequest signedReqObj = new FacebookSignedRequest();

			decodeFBSignedRequest(ref signedReqJson, ref signedReqObj);

			//if there is no signedRequest, kick to error page
			if (Session["signedRequestObj"] == null)
			{
				setDisplayState("notfacebook");
				return;
			}

			if (signedReqObj != null && signedReqObj.page.liked)
			{
				setDisplayState("pageliked");
			}
			else
			{
				setDisplayState("likeoverlay");
			}
		}

		private void setDisplayState(string state)
		{
			panLikeOverlay.Visible = false;
			panUserLikes.Visible = false;

			// For Dev purposes to force a state (must comment out the switch statement)
			//panUserLikes.Visible = true;

			switch (state)
			{
				case "likeoverlay":
					panLikeOverlay.Visible = true;
					break;
				case "pageliked":
					panUserLikes.Visible = true;
					break;
				default:
					panLikeOverlay.Visible = true;
					break;
			}
		}

		private static void decodeFBSignedRequest(ref string signedReqJson, ref FacebookSignedRequest signedReqObj)
		{
			signedReqJson = FBUtils.decodeSignedReq();
			signedReqObj = FBUtils.serializeSignedRequestJSON<FacebookSignedRequest>(signedReqJson);

			FBUtils.setSignedRequestSession(signedReqJson, signedReqObj);
		}
	}
}
