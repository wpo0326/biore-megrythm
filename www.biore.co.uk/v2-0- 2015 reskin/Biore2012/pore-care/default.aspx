﻿<%@ Page Title="Skin Care Tips for Clean Skin & Pores - Bioré" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.pore_care.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Skin Care Tips for Clean Skin & Pores - Bioré" name="title" />
    <meta content="Clogged pores are the root of many skin issues and if left untreated can lead to blemishes and inflammation, so read our skin care tips on which products to use." name="description" />
    <meta content="How to clean clogged pores, how to clean pores, clear blemishes, how to clear blemishes, blemish clearing" name="keywords" />

    <script type="text/javascript">
        var axel = Math.random() + "";
        var a = axel * 10000000000000;
        document.write('<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore0;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore0;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>


    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/poreCare.css")
        .Render("~/css/combinedprorecare_#.css")
    %>

    <link rel="stylesheet" type="text/css" href="/css/nestedAccordion.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="floodlightPixels" runat="server">
   <!-- header scripts here -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div class="centeringDiv">
                <div id="content">
                    <div class="titleh1">
                        <h1>PORE CARE <span>101</span></h1>
                        <div class="ruleBGa"></div>
                        <div class="responseRule"></div>

                    </div>

                    <div class="rowCont">
                        <div class="row1">                             
		                    <div class="whyCare"><h4>WHY CARE?</h4><h2 class="green"> Maintaining clean, healthy pores</h2><p>is important for sustaining healthy skin since the average adult has approximately 20,000 facial pores.</p></div>
                            <div class="col1 colMobile"><img src="../images/poreCare/poreCare2.jpg" alt="Pore Care 101 – Healthy Skin" id="Img4" /></div>
                            <div class="col2 colDesktop"><img src="../images/poreCare/poreCare2.jpg" alt="Pore Care 101 – Healthy Skin" id="Img2" /></div>    
		                </div>
                    </div>

                    <ul id="cbp-ntaccordion" class="cbp-ntaccordion">
                        <li>
                            <h4 class="cbp-nttrigger"><!--Q:-->What are pores?</h4>
                            <div class="cbp-ntcontent">
                                <p><span class="answer"><!--A:--></span>  Pores play a vital role in skin physiology. After all, without them we’d have nowhere for hair to grow out of, no method for skin oils to reach the surface, protecting skin from environmental dehydration.</p>
                                <p>Technically speaking, the pore is little more than the opening of the hair follicle onto the surface of the skin. The source of the hair follicle lies deep within the dermis. Midway up the dermal layer, the sebaceous oil gland empties into the hair follicle. Both skin oil (sebum) and the hair breach the surface through this tiny aperture. Wherever there is a hair, there will be a pore. </p>
                                <!--<ul class="cbp-ntsubaccordion">
                                    <li>
                                        <h4 class="cbp-nttrigger">QUESTION</h4>
                                        <div class="cbp-ntcontent">
                                            <p>ANSWER</p>
                                        </div>
                                    </li> 
                                </ul>-->
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger"><!--Q:-->Can clogged pores cause breakouts?</h4>
                            <div class="cbp-ntcontent">
                                <p><span class="answer"><!--A:--></span>  Yes. Dirt and oil collect around your pores, creating build-up that can clog the skin follicle. If bacteria make its way to the blockage and grow, this can cause inflammation and may progress into breakouts. Swelling and redness may linger in the form of whiteheads and red inflamed patches of skin. Other clogged pores like blackheads may not have the same markers of swelling, redness and inflammation but still contribute to zapping skin’s beauty potential.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger"><!--Q:-->Why do I need to use pore strips if I’m already using cleanser and scrub?</h4>
                            <div class="cbp-ntcontent">
                                <p><span class="answer"><!--A:--></span>  Our Bioré Pore Strips are for instantly removing the plugs that have already formed in the pores, which liquid cleansers can’t completely remove by themselves.  While cleansers are excellent at cleaning deep inside pores that have not yet become plugged or impacted and may help prevent some plugs from occurring, they may not prevent plugs completely. You will still need the pore strips to remove the plugs that do occur.</p>
                            </div>
                        </li>

                        <li>
                            <h4 class="cbp-nttrigger"><!--Q:-->How do Bioré Pore Strips work?</h4>
                            <div class="cbp-ntcontent">
                                <p><span class="answer"><!--A:--></span>  Bioré Pore Strips feature proprietary Cationic Bond Technology that has a positive charge while the blackheads are negatively charged. The two bind tightly and the plug can then be pulled out effectively when the strip is pulled gently off and away from the skin. </p>
                            </div>
                        </li>
                          <li>
                            <h4 class="cbp-nttrigger"><!--Q:-->What are the benefits of using Bioré Pore Strips?</h4>
                            <div class="cbp-ntcontent">
                                <p><span class="answer"><!--A:--></span>  When used once a week, Bioré Pore Strips are proven to minimise the appearance of pores and minimise the re-occurrence of blackheads.</p>
                            </div>
                        </li>
                          <li>
                            <h4 class="cbp-nttrigger"><!--Q:-->What makes Bioré cleansers, washes and scrubs different from competitors?</h4>
                            <div class="cbp-ntcontent">
                               <p><span class="answer"><!--A:--></span>  Traditional cleansers trap and wash dirt away, while also leaving behind individual molecules, which can penetrate skin and disrupt your skin’s natural moisture barrier, leading to: dry patches, oilier skin and clogged pores.</p>
                                  <div class="videoHolder">
                                      <div class="video pie">
                                          <div id="video1">
                                              <a class="popup-youtube" target="_blank" href="../video/video-iframe-faq-general.html">
                                                  <img src="../images/videoLanding/videoStillFAQ1.jpg" alt="" />
                                              </a>
                                          </div>
                                      </div>
                              
                                  </div>
                                <p>Bioré Cleansers feature Skin Purifying Technology that allows for selective cleansing – cleansing molecules target dirt, not your skin’s natural moisture. Like a magnet it locks onto problem-causing dirt, oil, and impurities.  When the job is done, individual molecules are attracted to each other and are swept away, leaving fewer molecules behind.  This leads to: high cleansibility, low potential for damage and irritation with skin’s natural moisture remaining intact, which means it lessens the chance of vulnerability to more issues.</p>
                                  <div class="videoHolder">
                                      <div class="video pie">
                                          <div id="video2">
                                              <a class="popup-youtube" target="_blank" href="../video/video-iframe-faq-spt-final.html">
                                                  <img src="../images/videoLanding/videoStillFAQ2.jpg" alt="" />
                                              </a>
                                          </div>
                                      </div>
                                  </div>
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger"><!--Q:-->Which Bioré range is suited to my skin type?</h4>
                            <div class="cbp-ntcontent">
                               <p><span class="answer"><!--A:--></span>  Bioré Charcoal Range is ideal for normal to oily skin. You have this skin type if your skin on the entire face gets shiny / greasy two hours after your wash your face.
Bioré Blue Agave + Baking Soda Range is ideal for combination skin. You have this skin type if your T-zone (your forehead and nose) is oily and your cheeks are dry. </p>
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger"><!--Q:-->What Charcoal and Blue Agave + Baking Soda do?</h4>
                            <div class="cbp-ntcontent">
                               <p><span class="answer"><!--A:--></span> Charcoal is a powerful natural ingredient known for its ability to absorb oil and impurities. Bioré charcoal products, with charcoal and Skin Purifying Technology work like a magnet to draw out pore-clogging dirt and give oily skin a deep clean.</p> 

<p>Blue Agave is a plant found in Mexico. Back in the days, its nectar was used by the Aztecs for hundreds of years as a folk remedy for many types of skin conditions. It's known for its properties to condition and soothe dry skin.
Baking Soda is a powerful exfoliant found in nature. On its own, natural baking soda is an alkaline that can dry the skin.  
Bioré Blue Agave +  Baking Soda products take all the natural powerful soothing properties of Blue Agave and cleansing and exfoliating properties of baking soda but they have been formulated precisely for use on your skin, so you get all the same benefits without damaging your skin. 
It will keep your T-zone clear of enlarged pores and breakthrough shine without encouraging dryness and flakiness elsewhere on the face. 
</p>
                            </div>
                        </li>
                    </ul>

                    <div id="Tips10">
                            <p class="largeNumber">10</p>
                            <p class="tipsFor green">TIPS FOR</p>
                            <p class="clearSkin">CLEAN SKIN</p>
                            <div class="ruleBG"></div>
                        </div>
		                <div class="bullets">	
                            <h3 class="green">• Keep a pore care regimen.</h3>
	                        <h3>• Clean skin gently with a mild cleanser.</h3>
	                        <h3 class="green">• Remove all dirt and makeup. </h3>
	                        <h3>• Wash daily up to twice per day, especially after exercising.</h3>
	                        <h3 class="green">• Avoid over-scrubbing or excessive skin washing.</h3>
	                        <h3>• Don’t squeeze, scratch, pick, or rub pimples. It can lead to infections and scarring. </h3>
	                        <h3 class="green">• Avoid touching your face with hands. </h3>
	                        <h3>• Oily cosmetics and creams may contain pore clogging ingredients.  </h3>
	                        <h3 class="green">• Water-based or noncomedogenic formulas may help to minimise pore clogs.</h3>
	                        <h3>• Be smart about sun exposure and be sure to use a skin caring sun protection product daily. </h3>
		                </div>
	                </div>
                    <!--<div class="rowCont">
                        <div class="row1">                             
		                    <div class="whyCare"><h4>WHY CARE?</h4><h2 class="green"> Maintaining clean, healthy pores</h2><p>is important for sustaining healthy skin since the average adult has approximately 200,000 facial pores.</p></div>
                            <div class="col1 colMobile"><img src="../images/poreCare/poreCare2.jpg" alt="Pore Care 101 – Healthy Skin" id="Img4" /></div>    
		                </div>
                       <div class="row2">
                            <div class="col1"><h4>THE QUESTION:</h4><h2>What are pores?</h2><p>Pores are the opening to an essential skin part called a sebaceous gland. Through the pore opening or follicle the skin is able to exchange skin moisture and discharge sebum, our skin’s natural oil. Healthy skin is able to strike a balance maintaining enough moisture while secreting enough oils to keep skin supple, flexible and in good condition.  <br/>Clogged pores are the root of many skin issues. Pore clogs impede sebaceous function which can diminish skin appearance. Clean pores are smaller and overall have an improved, more refined skin appearance. </p>
						<br /><h4>THE QUESTION:</h4><h2>Can clogged pores lead to blemishes?</h2><p><b>Yes. </b>Dirt and oil collect around your pores, creating build-up that can clog the skin follicle. If bacteria make its way to the blockage and grow, this can cause inflammation and may progress into blemishes. Swelling and redness may linger in the form of whiteheads and red inflamed patches of skin. Other clogged pores like blackheads may not have the same markers of swelling, redness and inflammation but still contribute to zapping skin’s beauty potential.</p></div>
		                    <div class="col2 colDesktop"><img src="../images/poreCare/poreCare2.jpg" alt="Pore Care 101 – Healthy Skin" id="Img2" /></div>
                           <div class="col3 colMobile"><img src="../images/poreCare/poreCare3.png" alt="facial care regime for healthy skin" id="Img5" /></div>
		                </div>
                        <div style="clear:both;"></div>
                        <div class="row2">
		                    <div class="col1"><h4>THE SOLUTION:</h4><h2>Cleanse, Scrub, Remove.</h2><p>Our Bioré Regimen works wonders.  <br />Just choose a cleanser, scrub, and pore strip for clean skin.  <br /><br />If you have blemishes, salicylic acid is one of the best blemish fighting ingredients. It helps reduce sebum and limits bacteria growth. By causing the uppermost part of the pore clog to exfoliate away, Salicylic acid can help to keep pores clean and with continued use can help to regulate skin cell turnover. For milder blemishes, it helps unclog pores to prevent reduce future lesions.</div> 
                            <div class="col2 colDesktop"><img src="../images/poreCare/poreCare3.png" alt="facial care regime for healthy skin" id="Img3" /></div>                            
		                </div>
                        <div style="clear:both;"></div>
                        <div id="Tips10">
                            <p class="largeNumber">10</p>
                            <p class="tipsFor green">TIPS FOR</p>
                            <p class="clearSkin">CLEAN SKIN</p>
                            <div class="ruleBG"></div>
                        </div>
		                <div class="bullets">	
                            <h3 class="green">• Keep a pore care regimen.</h3>
	                        <h3>• Clean skin gently with a mild cleanser.</h3>
	                        <h3 class="green">• Remove all dirt and makeup. </h3>
	                        <h3>• Wash daily up to twice per day, especially after exercising.</h3>
	                        <h3 class="green">• Avoid over-scrubbing or excessive skin washing.</h3>
	                        <h3>• Don’t squeeze, scratch, pick, or rub pimples. It can lead to infections and scarring. </h3>
	                        <h3 class="green">• Avoid touching your face with hands. </h3>
	                        <h3>• Oily cosmetics and creams may contain pore clogging ingredients.  </h3>
	                        <h3 class="green">• Water-based or noncomedogenic formulas may help to minimise pore clogs.</h3>
	                        <h3>• Be smart about sun exposure and be sure to use a skin caring sun protection product daily. </h3>
		                </div>
	                </div>-->
                </div>
            </div>
        </div>

        
    </div>

    <script>
        $(function () {
            $('#cbp-ntaccordion').cbpNTAccordion();

        });
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
    <%--<h1>pore care - scripts</h1>--%>
    <script src="/js/jquery.cbpNTAccordion.min.js" type="text/javascript"></script>
</asp:Content>
