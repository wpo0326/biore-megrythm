﻿<%@ Page Title="Sitemap | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.sitemap._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1>Sitemap</h1>
                 <div id="linkList">
                     <div id="prodLinks">
                         <ul class="col" id="col1">
                            <li><a href="../biore-facial-cleansing-products">Our products <span class="arrow">&rsaquo;</span></a></li>
                            <li>
			     <h2 class="pie roundedCorners"><span class="subNavHeadlines"><span class="greenBold">dirty</span> & oily skin no more</span></h2> 
                                <ul>
                                    <li><a href="../dirty-and-oily-skin-no-more/baking-soda-cleansing-scrub">BAKING SODA CLEANSING SCRUB</a></li>
                                    <li><a href="../dirty-and-oily-skin-no-more/baking-soda-pore-cleanser">BAKING SODA PORE CLEANSER</a></li>
                                    <li><a href="../dirty-and-oily-skin-no-more/deep-pore-charcoal-cleanser">DEEP PORE CHARCOAL CLEANSER</a></li>
                                    <li><a href="../dirty-and-oily-skin-no-more/self-heating-one-minute-mask">SELF HEATING ONE MINUTE MASK</a></li>
                                    <li><a href="../dirty-and-oily-skin-no-more/pore-penetrating-charcoal-bar">PORE PENETRATING CHARCOAL BAR</a></li>                               
                                </ul>
                            </li>
                            <li>
                                <h2 class="pie roundedCorners"><span class="subNavHeadlines">hello visibly <span class="tealBold">smaller pores</span></span></h2>
                                <ul>
                                    <li><a href="../hello-visibily-smaller-pores/charcoal-pore-minimizer">CHARCOAL PORE MINIMIZER</a></li>                             
                                </ul>
                            </li>
                        </ul>
                        <ul class="col" id="col2">
                            <li>
                                <h2 class="pie roundedCorners"><span class="subNavHeadlines"><span class="orangeBold">blemishes</span> are out of here</span></h2>
                                <ul>
                                    <li><a href="../blemishes-are-out-of-here/acne-clearing-scrub">BLEMISH CLEARING SCRUB</a></li>                             
                                </ul>
                            </li>
                            <li>
                                <h2 class="pie roundedCorners"><span class="subNavHeadlines">Break up With <span class="redBold">Blackheads</span></span></h2>
                                <ul>
                                    <li><a href="../breakup-with-blackheads/pore-strips#regular">DEEP CLEANSING PORE STRIPS</a></li>
                                    <li><a href="../breakup-with-blackheads/pore-strips#ultra">ULTRA DEEP CLEANSING PORE STRIPS</a></li>
                                    <li><a href="../breakup-with-blackheads/pore-strips#combo">Combo Deep Cleansing Pore Strips</a></li>
                                    <li><a href="../breakup-with-blackheads/charcoal-pore-strips#regular">Deep Cleansing Charcoal<br /> Pore Strips</a></li>
                                </ul>
                                <ul>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <ul class="col noPadding" id="col3">
                        <li><a href="../pore-care/">Pore Care <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../biore-facial-cleansing-products/Baking-Soda.aspx">What's New <span class="arrow">&rsaquo;</span></a></li>
                        <!--<li><a href="../email-newsletter-sign-up">Sign Me Up <span class="arrow">&rsaquo;</span></a></li>-->
                        <li><a href="../where-to-buy">Where To Buy <span class="arrow">&rsaquo;</span></a></li>
                        <!--<li><a href="../biore-videos">Watch It Work <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../about-us">About Bior&eacute;<sup>&reg;</sup> Skincare <span class="arrow">&rsaquo;</span></a></li>-->
                        <li><a href="http://www.kaobrands.com/" target="_blank">Kao Brands Company <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../contact-us">Contact Us <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaobrands.com/legal.asp" target="_blank">Legal <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">Privacy Policy <span class="arrow">&rsaquo;</span></a></li>
                     </ul>
                 </div>
                 <div id="photo"></div>
            </div>
        </div>
    </div>
</asp:Content>