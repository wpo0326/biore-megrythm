﻿<%@ Page Title="Sitemap - Bioré" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.sitemap._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Our sitemap contains an overview of all pages you can find within our website, allowing you to find what you want and quickly navigate to it." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1>Sitemap</h1>
                 <div id="linkList">
                     <div id="prodLinks">
                         <ul class="col" id="col1">
                            <li><a href="../biore-facial-cleansing-products">Our products <span class="arrow">&rsaquo;</span></a></li>
                            <li>
			     <h2 class="pie roundedCorners"><span class="subNavHeadlines"><span class="greenBold">charcoal</span> for oily to normal skin</span></h2> 
                                <ul>
                                    <li><a href="../charcoal-for-oily-to-normal-skin/charcoal-cleansing-micellar-water">CHARCOAL CLEANSING MICELLAR WATER</a></li>
                                    <li><a href="../charcoal-for-oily-to-normal-skin/deep-pore-charcoal-cleanser">CHARCOAL ANTI-BLEMISH CLEANSER</a></li>
                                    <li><a href="../charcoal-for-oily-to-normal-skin/deep-pore-charcoal-cleanser">DEEP PORE CHARCOAL CLEANSER</a></li>
                                    <li><a href="../charcoal-for-oily-to-normal-skin/pore-penetrating-charcoal-bar">PORE PENETRATING CHARCOAL BAR</a></li>  
                                    <li><a href="../charcoal-for-oily-to-normal-skin/charcoal-pore-minimizer">CHARCOAL PORE MINIMIZER</a></li> 
                                    <li><a href="../charcoal-for-oily-to-normal-skin/charcoal-anti-blemish-scrub">CHARCOAL ANTI-BLEMISH SCRUB</a></li>   
                                    <li><a href="../charcoal-for-oily-to-normal-skin/self-heating-one-minute-mask">SELF HEATING ONE MINUTE MASK</a></li>                     
                                </ul>
                            </li>
                            <li>
                                <h2 class="pie roundedCorners"><span class="subNavHeadlines"><span class="tealBold">baking soda</span>for dry / oily combination skin</span></h2>
                                <ul>
                                    <li><a href="../baking-soda-for-dry-oily-combination-skin/baking-soda-cleansing-micellar-water">BAKING SODA CLEANSING MICELLAR WATER </a></li>
                                    <li><a href="../baking-soda-for-dry-oily-combination-skin/baking-soda-anti-blemish-cleansing-foam">BAKING SODA ANTI-BLEMISH CLEANSING FOAM</a></li>
                                    <li><a href="../baking-soda-for-dry-oily-combination-skin/baking-soda-cleansing-scrub">BAKING SODA CLEANSING SCRUB</a></li>
                                    <li><a href="../baking-soda-for-dry-oily-combination-skin/baking-soda-pore-cleanser">BAKING SODA PORE CLEANSER</a></li>                          
                                </ul>
                            </li>
                        </ul>
                        <ul class="col" id="col2">
                            <li>
                                <h2 class="pie roundedCorners"><span class="subNavHeadlines"><span class="redBold">pore strips </span>for blackhead removal </span></h2>
                                <ul>
                                    <li><a href="../pore-strips-for-blackhead-removal/deep-cleansing-pore-strips">DEEP CLEANSING PORE STRIPS</a></li>
                                    <li><a href="../pore-strips-for-blackhead-removal/deep-cleansing-pore-strips-ultra">ULTRA DEEP CLEANSING PORE STRIPS</a></li>
                                    <li><a href="../pore-strips-for-blackhead-removal/deep-cleansing-pore-strips-combo">Combo Deep Cleansing Pore Strips</a></li>
                                    <li><a href="../pore-strips-for-blackhead-removal/charcoal-pore-strips#regular">Deep Cleansing Charcoal<br /> Pore Strips</a></li>
                                </ul>
                                <ul>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <ul class="col noPadding" id="col3">
                        <li><a href="../pore-care/">Pore Care <span class="arrow">&rsaquo;</span></a></li>
                        <!--<li><a href="../biore-facial-cleansing-products/Baking-Soda.aspx">What's New <span class="arrow">&rsaquo;</span></a></li>-->
                        <!--<li><a href="../email-newsletter-sign-up">Sign Me Up <span class="arrow">&rsaquo;</span></a></li>-->
                        <li><a href="../where-to-buy">Where To Buy <span class="arrow">&rsaquo;</span></a></li>
                        <!--<li><a href="../biore-videos">Watch It Work <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../about-us">About Bior&eacute;<sup>&reg;</sup> Skincare <span class="arrow">&rsaquo;</span></a></li>-->
                        <li><a href="http://www.kaobrands.com/" target="_blank">Kao Brands Company <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../contact-us">Contact Us <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaobrands.com/legal.asp" target="_blank">Legal <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">Privacy Policy <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../imprint/" target="_blank">Imprint <span class="arrow">&rsaquo;</span></a></li>
                     </ul>
                 </div>
                 <div id="photo"></div>
            </div>
        </div>
    </div>
</asp:Content>