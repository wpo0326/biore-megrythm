﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Biore2012.DAL;
using System.Xml;
using System.IO;
using System.Text;
using System.Xml.Schema;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace Biore2012.BLL
{
    public class BazaarVoiceProductFeed
    {
        private Dictionary<string, string> uniqueProdCategory;
        public BazaarVoiceProductFeed()
        {
            uniqueProdCategory = new Dictionary<string, string>();
        }


        public void generateProductFeed()
        {
            String xmlResult = generateXML();
            //if (validateXML(xmlResult))
            //{
            //    HttpContext.Current.Response.Write("Good");
            //}
            //else
            //{
            //    HttpContext.Current.Response.Write("Bad");
            //}
        }

        private static bool validateXML(string xmlResult)
        {
            bool isValid = true;
            XmlReader rdr=null;
            try
            {
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.Schemas.Add(null, System.Configuration.ConfigurationManager.AppSettings["BV_xmlns"]);
                settings.ValidationType = ValidationType.Schema;
                XmlDocument document = new XmlDocument();
                document.Load(new XmlTextReader(System.Configuration.ConfigurationManager.AppSettings["BV_ProductFeedPath"]+System.Configuration.ConfigurationManager.AppSettings["BV_ProductFeedFile"]));
                rdr = XmlReader.Create(new StringReader(document.InnerXml), settings);
                while (rdr.Read()) { }
               
            }
            catch (Exception ex)
            {
                isValid = false;
                HttpContext.Current.Response.Write(ex.Message);
            }
            finally 
            {
                if (rdr != null)
                {
                    rdr.Close();
                }
               
            }
            return isValid;

        }


        private static void XSDValidationEventHandler(object sender, ValidationEventArgs e)
        {
            HttpContext.Current.Response.Write(e.Message);
        }

        private static void ValidationHandler(object sender, ValidationEventArgs e)
        {
            HttpContext.Current.Response.Write(e.Message);
        }

        private string generateXML()
        {

            string results = string.Empty;
            StringWriter strWriter = new StringWriter();
            StringBuilder categoryBlock = new StringBuilder();
            StringBuilder productBlock = new StringBuilder();

            //XmlTextWriter writer = new XmlTextWriter(strWriter);
            XmlTextWriter writer = new XmlTextWriter(System.Configuration.ConfigurationManager.AppSettings["BV_ProductFeedPath"] + System.Configuration.ConfigurationManager.AppSettings["BV_ProductFeedFile"], null);


            writer.WriteStartDocument();


            writer.WriteStartElement("Feed");
            writer.WriteAttributeString("xmlns", System.Configuration.ConfigurationManager.AppSettings["BV_xmlns"]);
            writer.WriteAttributeString("name", System.Configuration.ConfigurationManager.AppSettings["BV_FeedName"]);
            writer.WriteAttributeString("incremental", "false");
            writer.WriteAttributeString("extractDate", string.Format("{0:s}", DateTime.Now));

            writer.WriteStartElement("Brands");
            writer.WriteStartElement("Brand");
            writer.WriteElementString("ExternalId", System.Configuration.ConfigurationManager.AppSettings["BV_BrandExtID"]);
            writer.WriteElementString("Name", cleanEntities(System.Configuration.ConfigurationManager.AppSettings["BV_BrandName"]));
            writer.WriteEndElement(); //close Brand
            writer.WriteEndElement(); //close Brands

            Trace.Write("Written to: " + System.Configuration.ConfigurationManager.AppSettings["BV_ProductFeedPath"] + System.Configuration.ConfigurationManager.AppSettings["BV_ProductFeedFile"]);

            /*
             To avoid looping products list twice, one for category chunk (product types) 
             and the other for product data, using stringbuilder for string concats
            */
            foreach (Product item in ProductDAO.GetProductsForBazaarVoice())
            {
                categoryBlock.Append(processCategoryBlock(item));
                productBlock.Append(processProductBlock(item));
            }

            //Start Categories Block
            writer.WriteStartElement("Categories");
            //Let's start with all top level(1st level) categories
            foreach (ProductFamily item in FamilyDAO.GetAllProductFamily())
            {
                writer.WriteStartElement("Category");
                writer.WriteElementString("ExternalId", item.FamilyRefName);
                writer.WriteElementString("Name", cleanEntities(item.ProductFamilyName.Replace("<sup>", "").Replace("</sup>", "")));
                //writer.WriteStartElement("Name");
                //writer.WriteCData(cleanEntities(item.ProductFamilyName));
                //writer.WriteEndElement();
                string link = System.Configuration.ConfigurationManager.AppSettings["productfamily"].Replace("{productfamily}", item.FamilyUrlName);
                writer.WriteElementString("CategoryPageUrl", HttpUtility.UrlEncode(System.Configuration.ConfigurationManager.AppSettings["BV_SiteRefPrefix"] + link));
                writer.WriteEndElement(); //close Category

            }
            //Inject the categories based on Product Types derived from products
            writer.WriteRaw(categoryBlock.ToString());
            writer.WriteEndElement(); //close Categories
            //End Categories Block



            //Start Products Block
            writer.WriteStartElement("Products");
            writer.WriteRaw(productBlock.ToString());
            writer.WriteEndElement(); //close Products
            //End Products Block

            writer.WriteEndElement(); //close Feed
            // Ends the document.

            writer.WriteEndDocument();

            // close writer

            writer.Close();


            return strWriter.ToString();
        }

        private string processProductBlock(Product item)
        {
            StringBuilder sb2 = new StringBuilder();
            /*

               <Product>
               <ExternalId>ra-strength-restoring-shampoo</ExternalId>
               <Name>Strength Restoring&amp;trade; Shampoo</Name>
               <Description>&lt;ul&gt;&lt;li&gt;&lt;strong&gt;Invigorates and feeds from root to tip while fortifying to reduce breakage&lt;/strong&gt; for greater manageability and stronger hair that’s noticeably more resilient with every use&lt;/li&gt;&lt;li&gt;&lt;strong&gt;With natural Eucalyptus and peppermint oil&lt;/strong&gt;, this formula stimulates the scalp with a cool, tingling sensation. Massaging your scalp encourages circulation, which will &amp;quot;wake-up&amp;quot; healthier-looking new growth.&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Containing Panthenol, a vital strengthening ingredient&lt;/strong&gt;, the formula reinforces the hair structure and helps prevent breakage and split ends.&lt;/li&gt;&lt;/ul&gt;</Description>
               <BrandExternalId>JF-EN_US</BrandExternalId>
               <CategoryExternalId>root-awakening-shampoo</CategoryExternalId>
               <ProductPageUrl>http://jfqa.enlighten.com/en-US/ProductDetail/Hair-Care/Root-Awakening/Strength-Restoring-Shampoo</ProductPageUrl>
               <ImageUrl>http://jfqa.enlighten.com/en-US/images/products/small/ra-strength-restoring-shampoo.jpg</ImageUrl>
               <UPCs>
                   <UPC>382157229380</UPC>
               </UPCs>
               </Product>

               */

            string familyPrefix = item.ProductForceFamily;
            if (familyPrefix.Length < 1)
            {
                familyPrefix = item.ProductDefaultFamily;
            }

            string description = string.Empty;
            if (item.ContentDataCollection != null)
            {
                foreach (ContentBase content in item.ContentDataCollection)
                {
                    if (content.Purpose == "howitworks")
                    {
                        description = content.ValueData.Replace("<p>", "").Replace("</p>", "");
                        break;
                    }
                }
            }

            string link = System.Configuration.ConfigurationManager.AppSettings["productdetail"].Replace("{productfamily}", ProductFamily.GetProductFamilyByProductFamilyRefName(familyPrefix).FamilyUrlName).Replace("{productdetail}", item.ProductUrlName);

            ImageBase imgData = PresentationUtils.GetImage(item.ImageCollection, "large");

            string imgLink = HttpUtility.UrlEncode(System.Configuration.ConfigurationManager.AppSettings["imageprefix"] + imgData.Location + imgData.FileName);

            sb2.Append("<Product>");
            sb2.Append("<ExternalId>");
            sb2.Append(item.ProductRefName);
            sb2.Append("</ExternalId>");
            sb2.Append("<Name>");
            sb2.Append(cleanEntities(item.ProductShortName.Replace("<sup>", "").Replace("</sup>", "") + " " + item.ProductSuffixName.Replace("<sup>", "").Replace("</sup>", "")));
            sb2.Append("</Name>");
            sb2.Append("<Description>");
            sb2.Append(cleanEntities(description));
            sb2.Append("</Description>");
            sb2.Append("<BrandExternalId>");
            sb2.Append(System.Configuration.ConfigurationManager.AppSettings["BV_BrandExtID"]);
            sb2.Append("</BrandExternalId>");
            sb2.Append("<CategoryExternalId>");
            sb2.Append(familyPrefix + "-" + item.ProductType);
            sb2.Append("</CategoryExternalId>");
            sb2.Append("<ProductPageUrl>");
            sb2.Append(HttpUtility.UrlEncode(System.Configuration.ConfigurationManager.AppSettings["BV_SiteRefPrefix"] + link));
            sb2.Append("</ProductPageUrl>");
            sb2.Append("<ImageUrl>");
            sb2.Append(HttpUtility.UrlEncode(System.Configuration.ConfigurationManager.AppSettings["BV_SiteRefPrefix"].Replace("/en-CA/","") +  imgLink));
            sb2.Append("</ImageUrl>");
            sb2.Append("</Product>");

            return sb2.ToString();
        }





        private string processCategoryBlock(Product item)
        {
            // Remember we can only show the product types that actually have products 
            // and using dictionary to prevent duplicate categories

            StringBuilder sb = new StringBuilder();
            string familyPrefix = item.ProductForceFamily;
            if (familyPrefix.Length < 1)
            {
                familyPrefix = item.ProductDefaultFamily;
            }

            if (!uniqueProdCategory.ContainsKey(familyPrefix + "-" + item.ProductType))
            {
                string link = System.Configuration.ConfigurationManager.AppSettings["productfamilyfilter"].Replace("{productfamily}", ProductFamily.GetProductFamilyByProductFamilyRefName(familyPrefix).FamilyUrlName).Replace("{producttypesfilter}", ProductTypesFilter.GetProductTypesFilterByProductType(item.ProductType).ProductTypesFilterUrlName);

                sb.Append("<Category>");
                sb.Append("<ExternalId>");
                sb.Append(familyPrefix + "-" + item.ProductType);
                sb.Append("</ExternalId>");
                sb.Append("<ParentExternalId>");
                sb.Append(familyPrefix);
                sb.Append("</ParentExternalId>");
                sb.Append("<Name>");
                sb.Append(cleanEntities(ProductTypesFilter.GetProductTypesFilterByProductType(item.ProductType).ProductTypesFilterDisplayName.Replace("<sup>", "").Replace("</sup>", "")));
                sb.Append("</Name>");
                sb.Append("<CategoryPageUrl>");
                sb.Append(HttpUtility.UrlEncode(System.Configuration.ConfigurationManager.AppSettings["BV_SiteRefPrefix"] + link));
                sb.Append("</CategoryPageUrl>");
                sb.Append("</Category>");
                uniqueProdCategory.Add(familyPrefix + "-" + item.ProductType, "");
            }
            return sb.ToString();
        }


        public string cleanEntities(string source)
        {

            String rstr = source;
            // rstr = rstr.Replace("\n"," ");
            // rstr = rstr.Replace("<br */>","\n");
            // rstr = rstr.Replace("<br>","\n");
            rstr = rstr.Replace("&#8209;", "-");
            rstr = rstr.Replace("<[^>]*>", "");
            rstr = rstr.Replace("<", "&lt;");
            rstr = rstr.Replace(">", "&gt;");
            rstr = rstr.Replace(" & ", " &amp; ");
            rstr = rstr.Replace("&cent;", "\u00A2");
            rstr = rstr.Replace("&euro;", "\u20AC");
            rstr = rstr.Replace("&pound;", "\u20AC");
            rstr = rstr.Replace("&yen;", "\u20AC");
            rstr = rstr.Replace("&nbsp;", "\u00a0");
            //rstr = rstr.Replace("&amp;", "\u0026");
            rstr = rstr.Replace("&quot;", "\"");
            // rstr=rstr.Replace("&quot;", "\u0022");
            rstr = rstr.Replace("&cent;", "\u00a2");
            rstr = rstr.Replace("&euro;", "\u20ac");
            rstr = rstr.Replace("&pound;", "\u00a3");
            rstr = rstr.Replace("&yen;", "\u00a5");
            rstr = rstr.Replace("&copy;", "\u00a9");
            //rstr = rstr.Replace("&reg;", "\u00AE");
            rstr = rstr.Replace("&trade;", "\u2122");
            rstr = rstr.Replace("&permil;", "\u2030");
            rstr = rstr.Replace("&micro;", "\u00b5");
            rstr = rstr.Replace("&middot;", "\u00b7");
            rstr = rstr.Replace("&bull;", "\u2022");
            rstr = rstr.Replace("&hellip;", "\u2026");
            rstr = rstr.Replace("&prime;", "\u2032");
            rstr = rstr.Replace("&Prime;", "\u2033");
            rstr = rstr.Replace("&sect;", "\u00a7");
            rstr = rstr.Replace("&para;", "\u00b6");
            rstr = rstr.Replace("&szlig;", "\u00df");
            rstr = rstr.Replace("&lsaquo;", "\u2039");
            rstr = rstr.Replace("&rsaquo;", "\u203a");
            rstr = rstr.Replace("&laquo;", "\u00ab");
            rstr = rstr.Replace("&raquo;", "\u00bb");
            rstr = rstr.Replace("&lsquo;", "\u2018");
            rstr = rstr.Replace("&rsquo;", "\u2019");
            rstr = rstr.Replace("&ldquo;", "\u201c");
            rstr = rstr.Replace("&rdquo;", "\u201d");
            rstr = rstr.Replace("&sbquo;", "\u201a");
            rstr = rstr.Replace("&bdquo;", "\u201e");
            //rstr = rstr.Replace("&lt;", "\u003c");
            //rstr = rstr.Replace("&gt;", "\u003e");
            rstr = rstr.Replace("&le;", "\u2264");
            rstr = rstr.Replace("&ge;", "\u2265");
            rstr = rstr.Replace("&ndash;", "\u2013");
            rstr = rstr.Replace("&mdash;", "\u2014");
            rstr = rstr.Replace("&macr;", "\u00af");
            rstr = rstr.Replace("&oline;", "\u203e");
            rstr = rstr.Replace("&curren;", "\u00a4");
            rstr = rstr.Replace("&brvbar;", "\u00a6");
            rstr = rstr.Replace("&uml;", "\u00a8");
            rstr = rstr.Replace("&iexcl;", "\u00a1");
            rstr = rstr.Replace("&iquest;", "\u00bf");
            rstr = rstr.Replace("&circ;", "\u02c6");
            rstr = rstr.Replace("&tilde;", "\u02dc");
            rstr = rstr.Replace("&deg;", "\u00b0");
            rstr = rstr.Replace("&minus;", "\u2212");
            rstr = rstr.Replace("&plusmn;", "\u00b1");
            rstr = rstr.Replace("&divide;", "\u00f7");
            rstr = rstr.Replace("&frasl;", "\u2044");
            rstr = rstr.Replace("&times;", "\u00d7");
            rstr = rstr.Replace("&sup1;", "\u00b9");
            rstr = rstr.Replace("&sup2;", "\u00b2");
            rstr = rstr.Replace("&sup3;", "\u00b3");
            rstr = rstr.Replace("&frac14;", "\u00bc");
            rstr = rstr.Replace("&frac12;", "\u00bd");
            rstr = rstr.Replace("&frac34;", "\u00be");
            rstr = rstr.Replace("&fnof;", "\u0192");
            rstr = rstr.Replace("&int;", "\u222b");
            rstr = rstr.Replace("&sum;", "\u2211");
            rstr = rstr.Replace("&infin;", "\u221e");
            rstr = rstr.Replace("&radic;", "\u221a");
            rstr = rstr.Replace("&sim;", "\u223c");
            rstr = rstr.Replace("&cong;", "\u2245");
            rstr = rstr.Replace("&asymp;", "\u2248");
            rstr = rstr.Replace("&ne;", "\u2260");
            rstr = rstr.Replace("&equiv;", "\u2261");
            rstr = rstr.Replace("&isin;", "\u2208");
            rstr = rstr.Replace("&notin;", "\u2209");
            rstr = rstr.Replace("&ni;", "\u220b");
            rstr = rstr.Replace("&prod;", "\u220f");
            rstr = rstr.Replace("&and;", "\u2227");
            rstr = rstr.Replace("&or;", "\u2228");
            rstr = rstr.Replace("&not;", "\u00ac");
            rstr = rstr.Replace("&cap;", "\u2229");
            rstr = rstr.Replace("&cup;", "\u222a");
            rstr = rstr.Replace("&part;", "\u2202");
            rstr = rstr.Replace("&forall;", "\u2200");
            rstr = rstr.Replace("&exist;", "\u2203");
            rstr = rstr.Replace("&empty;", "\u2205");
            rstr = rstr.Replace("&nabla;", "\u2207");
            rstr = rstr.Replace("&lowast;", "\u2217");
            rstr = rstr.Replace("&prop;", "\u221d");
            rstr = rstr.Replace("&ang;", "\u2220");
            rstr = rstr.Replace("&acute;", "\u00b4");
            rstr = rstr.Replace("&cedil;", "\u00b8");
            rstr = rstr.Replace("&ordf;", "\u00aa");
            rstr = rstr.Replace("&ordm;", "\u00ba");
            rstr = rstr.Replace("&dagger;", "\u2020");
            rstr = rstr.Replace("&Dagger;", "\u2021");
            rstr = rstr.Replace("&Agrave;", "\u00c0");
            rstr = rstr.Replace("&Aacute;", "\u00c1");
            rstr = rstr.Replace("&Acirc;", "\u00c2");
            rstr = rstr.Replace("&Atilde;", "\u00c3");
            rstr = rstr.Replace("&Auml;", "\u00c4");
            rstr = rstr.Replace("&Aring;", "\u00c5");
            rstr = rstr.Replace("&AElig;", "\u00c6");
            rstr = rstr.Replace("&Ccedil;", "\u00c7");
            rstr = rstr.Replace("&Egrave;", "\u00c8");
            rstr = rstr.Replace("&Eacute;", "\u00c9");
            rstr = rstr.Replace("&Ecirc;", "\u00ca");
            rstr = rstr.Replace("&Euml;", "\u00cb");
            rstr = rstr.Replace("&Igrave;", "\u00cc");
            rstr = rstr.Replace("&Iacute;", "\u00cd");
            rstr = rstr.Replace("&Icirc;", "\u00ce");
            rstr = rstr.Replace("&Iuml;", "\u00cf");
            rstr = rstr.Replace("&ETH;", "\u00d0");
            rstr = rstr.Replace("&Ntilde;", "\u00d1");
            rstr = rstr.Replace("&Ograve;", "\u00d2");
            rstr = rstr.Replace("&Oacute;", "\u00d3");
            rstr = rstr.Replace("&Ocirc;", "\u00d4");
            rstr = rstr.Replace("&Otilde;", "\u00d5");
            rstr = rstr.Replace("&Ouml;", "\u00d6");
            rstr = rstr.Replace("&Oslash;", "\u00d8");
            rstr = rstr.Replace("&OElig;", "\u0152");
            rstr = rstr.Replace("&Scaron;", "\u0160");
            rstr = rstr.Replace("&Ugrave;", "\u00d9");
            rstr = rstr.Replace("&Uacute;", "\u00da");
            rstr = rstr.Replace("&Ucirc;", "\u00db");
            rstr = rstr.Replace("&Uuml;", "\u00dc");
            rstr = rstr.Replace("&Yacute;", "\u00dd");
            rstr = rstr.Replace("&Yuml;", "\u0178");
            rstr = rstr.Replace("&THORN;", "\u00de");
            rstr = rstr.Replace("&agrave;", "\u00e0");
            rstr = rstr.Replace("&aacute;", "\u00e1");
            rstr = rstr.Replace("&acirc;", "\u00e2");
            rstr = rstr.Replace("&atilde;", "\u00e3");
            rstr = rstr.Replace("&auml;", "\u00e4");
            rstr = rstr.Replace("&aring;", "\u00e5");
            rstr = rstr.Replace("&aelig;", "\u00e6");
            rstr = rstr.Replace("&ccedil;", "\u00e7");
            rstr = rstr.Replace("&egrave;", "\u00e8");
            rstr = rstr.Replace("&eacute;", "\u00e9");
            rstr = rstr.Replace("&ecirc;", "\u00ea");
            rstr = rstr.Replace("&euml;", "\u00eb");
            rstr = rstr.Replace("&igrave;", "\u00ec");
            rstr = rstr.Replace("&iacute;", "\u00ed");
            rstr = rstr.Replace("&icirc;", "\u00ee");
            rstr = rstr.Replace("&iuml;", "\u00ef");
            rstr = rstr.Replace("&eth;", "\u00f0");
            rstr = rstr.Replace("&ntilde;", "\u00f1");
            rstr = rstr.Replace("&ograve;", "\u00f2");
            rstr = rstr.Replace("&oacute;", "\u00f3");
            rstr = rstr.Replace("&ocirc;", "\u00f4");
            rstr = rstr.Replace("&otilde;", "\u00f5");
            rstr = rstr.Replace("&ouml;", "\u00f6");
            rstr = rstr.Replace("&oslash;", "\u00f8");
            rstr = rstr.Replace("&oelig;", "\u0153");
            rstr = rstr.Replace("&scaron;", "\u0161");
            rstr = rstr.Replace("&ugrave;", "\u00f9");
            rstr = rstr.Replace("&uacute;", "\u00fa");
            rstr = rstr.Replace("&ucirc;", "\u00fb");
            rstr = rstr.Replace("&uuml;", "\u00fc");
            rstr = rstr.Replace("&yacute;", "\u00fd");
            rstr = rstr.Replace("&thorn;", "\u00fe");
            rstr = rstr.Replace("&yuml;", "\u00ff");
            rstr = rstr.Replace("&Alpha;", "\u0391");
            rstr = rstr.Replace("&Beta;", "\u0392");
            rstr = rstr.Replace("&Gamma;", "\u0393");
            rstr = rstr.Replace("&Delta;", "\u0394");
            rstr = rstr.Replace("&Epsilon;", "\u0395");
            rstr = rstr.Replace("&Zeta;", "\u0396");
            rstr = rstr.Replace("&Eta;", "\u0397");
            rstr = rstr.Replace("&Theta;", "\u0398");
            rstr = rstr.Replace("&Iota;", "\u0399");
            rstr = rstr.Replace("&Kappa;", "\u039a");
            rstr = rstr.Replace("&Lambda;", "\u039b");
            rstr = rstr.Replace("&Mu;", "\u039c");
            rstr = rstr.Replace("&Nu;", "\u039d");
            rstr = rstr.Replace("&Xi;", "\u039e");
            rstr = rstr.Replace("&Omicron;", "\u039f");
            rstr = rstr.Replace("&Pi;", "\u03a0");
            rstr = rstr.Replace("&Rho;", "\u03a1");
            rstr = rstr.Replace("&Sigma;", "\u03a3");
            rstr = rstr.Replace("&Tau;", "\u03a4");
            rstr = rstr.Replace("&Upsilon;", "\u03a5");
            rstr = rstr.Replace("&Phi;", "\u03a6");
            rstr = rstr.Replace("&Chi;", "\u03a7");
            rstr = rstr.Replace("&Psi;", "\u03a8");
            rstr = rstr.Replace("&Omega;", "\u03a9");
            rstr = rstr.Replace("&alpha;", "\u03b1");
            rstr = rstr.Replace("&beta;", "\u03b2");
            rstr = rstr.Replace("&gamma;", "\u03b3");
            rstr = rstr.Replace("&delta;", "\u03b4");
            rstr = rstr.Replace("&epsilon;", "\u03b5");
            rstr = rstr.Replace("&zeta;", "\u03b6");
            rstr = rstr.Replace("&eta;", "\u03b7");
            rstr = rstr.Replace("&theta;", "\u03b8");
            rstr = rstr.Replace("&iota;", "\u03b9");
            rstr = rstr.Replace("&kappa;", "\u03ba");
            rstr = rstr.Replace("&lambda;", "\u03bb");
            rstr = rstr.Replace("&mu;", "\u03bc");
            rstr = rstr.Replace("&nu;", "\u03bd");
            rstr = rstr.Replace("&xi;", "\u03be");
            rstr = rstr.Replace("&omicron;", "\u03bf");
            rstr = rstr.Replace("&pi;", "\u03c0");
            rstr = rstr.Replace("&rho;", "\u03c1");
            rstr = rstr.Replace("&sigmaf;", "\u03c2");
            rstr = rstr.Replace("&sigma;", "\u03c3");
            rstr = rstr.Replace("&tau;", "\u03c4");
            rstr = rstr.Replace("&upsilon;", "\u03c5");
            rstr = rstr.Replace("&phi;", "\u03c6");
            rstr = rstr.Replace("&chi;", "\u03c7");
            rstr = rstr.Replace("&psi;", "\u03c8");
            rstr = rstr.Replace("&omega;", "\u03c9");
            rstr = rstr.Replace("&alefsym;", "\u2135");
            rstr = rstr.Replace("&piv;", "\u03d6");
            rstr = rstr.Replace("&real;", "\u211c");
            rstr = rstr.Replace("&thetasym;", "\u03d1");
            rstr = rstr.Replace("&upsih;", "\u03d2");
            rstr = rstr.Replace("&weierp;", "\u2118");
            rstr = rstr.Replace("&image;", "\u2111");
            rstr = rstr.Replace("&larr;", "\u2190");
            rstr = rstr.Replace("&uarr;", "\u2191");
            rstr = rstr.Replace("&rarr;", "\u2192");
            rstr = rstr.Replace("&darr;", "\u2193");
            rstr = rstr.Replace("&harr;", "\u2194");
            rstr = rstr.Replace("&crarr;", "\u21b5");
            rstr = rstr.Replace("&lArr;", "\u21d0");
            rstr = rstr.Replace("&uArr;", "\u21d1");
            rstr = rstr.Replace("&rArr;", "\u21d2");
            rstr = rstr.Replace("&dArr;", "\u21d3");
            rstr = rstr.Replace("&hArr;", "\u21d4");
            rstr = rstr.Replace("&there4;", "\u2234");
            rstr = rstr.Replace("&sub;", "\u2282");
            rstr = rstr.Replace("&sup;", "\u2283");
            rstr = rstr.Replace("&nsub;", "\u2284");
            rstr = rstr.Replace("&sube;", "\u2286");
            rstr = rstr.Replace("&supe;", "\u2287");
            rstr = rstr.Replace("&oplus;", "\u2295");
            rstr = rstr.Replace("&otimes;", "\u2297");
            rstr = rstr.Replace("&perp;", "\u22a5");
            rstr = rstr.Replace("&sdot;", "\u22c5");
            rstr = rstr.Replace("&lceil;", "\u2308");
            rstr = rstr.Replace("&rceil;", "\u2309");
            rstr = rstr.Replace("&lfloor;", "\u230a");
            rstr = rstr.Replace("&rfloor;", "\u230b");
            rstr = rstr.Replace("&lang;", "\u2329");
            rstr = rstr.Replace("&rang;", "\u232a");
            rstr = rstr.Replace("&loz;", "\u25ca");
            rstr = rstr.Replace("&spades;", "\u2660");
            rstr = rstr.Replace("&clubs;", "\u2663");
            rstr = rstr.Replace("&hearts;", "\u2665");
            rstr = rstr.Replace("&diams;", "\u2666");
            rstr = rstr.Replace("&ensp;", "\u2002");
            rstr = rstr.Replace("&emsp;", "\u2003");
            rstr = rstr.Replace("&thinsp;", "\u2009");
            rstr = rstr.Replace("&zwnj;", "\u200c");
            rstr = rstr.Replace("&zwj;", "\u200d");
            rstr = rstr.Replace("&lrm;", "\u200e");
            rstr = rstr.Replace("&rlm;", "\u200f");
            rstr = rstr.Replace("&shy;", "\u00ad");
            rstr = rstr.Replace("&#174;", "&reg;");
            // rstr = rstr.Replace("&reg;","\u00AE");
            // rstr = rstr.Replace("&copy;","\u00A9");
            // rstr = rstr.Replace("&trade;", "\u2122");
            rstr = rstr.Replace("&#153;", "\u2122");
            rstr = rstr.Replace("&#8482;", "\u2122");
            rstr = rstr.Replace("&#x2122;", "\u2122");
            // rstr = rstr.Replace("&nbsp;", "\u00A0");
            rstr = rstr.Replace("&#160;", "\u00A0");
            rstr = rstr.Replace("&160;", "\u00A0");
            rstr = rstr.Replace("&#xe9;", "\u00E9");
            rstr = rstr.Replace("&#233;", "\u00E9");
            rstr = rstr.Replace("&#xe8;", "\u00E8");
            rstr = rstr.Replace("&#232;", "\u00E8");
            // rstr = rstr.Replace("&ndash;", "-");
            rstr = rstr.Replace("&#8211;", "-");
            rstr = rstr.Replace("&quot;", "\"");
            rstr = rstr.Replace("&#34;", "\"");
            rstr = rstr.Replace("&#034;", "\"");
            // rstr = rstr.Replace("&amp;", "&");
            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex(@"\s+", options);
            rstr = regex.Replace(rstr, @" ");
            return rstr;


        }
    }
}
