﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using Biore2012.DAL;

namespace Biore2012.BLL
{
    [System.Web.Script.Services.ScriptService]
    [Serializable]
    [XmlRoot("DataApiResponse")]
    public class CGBazaarvoiceFeed
    {
        /// <summary>
        /// Results 
        /// </summary>
        [XmlElement("Results")]
        public string Results { get; set; }


        /// <summary>
        /// Includes 
        /// </summary>
        [XmlElement("Includes")]
        public string Includes { get; set; }


        /// <summary>
        /// A collection of BV products
        /// </summary>
        [System.Xml.Serialization.XmlArray("Products")]
        [System.Xml.Serialization.XmlArrayItem("Product", typeof(String))]
        public List<string> ProductCollection;

        [XmlAttribute("id")]
        public string id { get; set; }

        /// <summary>
        /// TotalReviewCount is the total reviews for a product.
        /// </summary>
        [XmlElement("TotalReviewCount")]
        public string TotalReviewCount { get; set; }

        /// <summary>
        /// CategoryId is product category ID.
        /// </summary>
        [XmlElement("CategoryId")]
        public string CategoryId { get; set; }

        /// <summary>
        /// ProductPageUrl is the path to the product detail page.
        /// </summary>
        [XmlElement("ProductPageUrl")]
        public string ProductPageUrl { get; set; }

        /// <summary>
        /// AverageOverallRating is the overall rating of the product.
        /// </summary>
        [XmlElement("AverageOverallRating")]
        public string AverageOverallRating { get; set; }

        public CGBazaarvoiceFeed()
        {
            ProductCollection = new List<string>();
        }

    }
}