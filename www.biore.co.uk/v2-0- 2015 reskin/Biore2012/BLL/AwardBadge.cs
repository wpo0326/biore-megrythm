﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace Biore2012.BLL
{
    public class AwardBadge
    {
        /// <summary>
		/// The Award URL
		/// </summary>
		[XmlElement("AwardImageURL")]
        public string AwardImageURL;
        
        /// <summary>
		/// The Award Badge vertical offset
		/// </summary>
		[XmlElement("VerticalOffset")]
        public string VerticalOffset;

        /// <summary>
		/// The Award Badge Horizontal Offset
		/// </summary>
		[XmlElement("HorizontalOffset")]
        public string HorizontalOffset;

        public AwardBadge()
        {

        }

        public AwardBadge(string awardImageURL, string verticalOffset, string horizontalOffset)
        {
            AwardImageURL = awardImageURL;
            VerticalOffset = verticalOffset;
            HorizontalOffset = horizontalOffset;

        }
    }
}
