﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Xml;
using System.Text.RegularExpressions;

namespace Biore2012.BLL
{
    public class BioreUtils
    {
        public bool checkIsMobile(HttpRequest httpRequest)
        {
            bool isMobile = false;
            if (httpRequest.Browser.IsMobileDevice.ToString().ToLower() == "true" && httpRequest.Browser["is_tablet"].ToLower() == "false")
            {
                //isMobile = true;
                isMobile = false; //doesn't seem relevant for Biore Canada.
            }
            return isMobile;


            // Old Detect code from: http://detectmobilebrowsers.com/
            //string u = Request.ServerVariables["HTTP_USER_AGENT"];
            //Regex b = new Regex(@"android.+mobile|avantgo|bada\\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\/|plucker|pocket|psp|symbian|treo|up\\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            //Regex v = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\/|capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|iac( |\\-|\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\\/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\/(k|l|u)|50|54|e\\-|e\\/|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50\\/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\/|se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\\-|2|g)|yas\\-|your|zeto|zte\\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            //if ((b.IsMatch(u) || v.IsMatch(u.Substring(0, 4)))){ // mobile{
            //    bodyClass = "mobile";
            //}

        }

        public void configureProveItLinks(Boolean isMobile, HyperLink proveItAnchor) {
            if (isMobile == true) {
                proveItAnchor.Target = "";
                proveItAnchor.NavigateUrl = "http://www.facebook.com/bioreskin?sk=app_205787372796203";// Point global header prove it link to landing page
            }
        }

        public static XmlCDataSection genCData(string textIn)
        {
            XmlDocument doc = new XmlDocument();
            return doc.CreateCDataSection(textIn);
        }
        public static string remCData(XmlCDataSection textIn)
        {

            return textIn.InnerText.ToString();
        }

        public static List<string> parseAppSettings(string keyPattern)
        {
            //Definitely want to cache this
            //We can use arrayList because text and value are the same
            List<string> arr = new List<string>();
            foreach (string appSetting in ConfigurationManager.AppSettings.AllKeys)
            {
                if (appSetting.Contains(keyPattern) == true)
                    arr.Add(ConfigurationManager.AppSettings.Get(appSetting));
            }
            return arr;
        }

        bool TryParseXML(string xml)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(xml);
                xd = null;
                return true;
            }
            catch (XmlException)
            {
                return false;
            }
            catch (Exception)
            {

                return false;
            }

        }

        public void queueNumber(int min, int max, string defaultVal, DropDownList DDL)
        {
            /// <summary>
            /// Method to bind ranged numbers to DropDownLists
            /// </summary>

            bindDDList(defaultVal, "", DDL); //Create default value

            if (DDL.ID.ToString() != "yyyy") //Days/Month increment to max value
            {
                for (int i = min; i < max; i++)
                {
                    bindDDList(i.ToString(), i.ToString(), DDL);
                }
            }
            else
            {
                for (int i = max; i > min; i--) //Year decrements to min value
                {
                    bindDDList(i.ToString(), i.ToString(), DDL);
                }
            }
        }

        public void bindDDList(string text, string value, DropDownList DDL)
        {
            /// <summary>
            /// Method to bind values/numbers to DropDownLists
            /// </summary>
            /// 
            ListItem li = new ListItem();
            li.Text = text;
            li.Value = value;
            DDL.Items.Add(li);
            li = null;
        }


        public bool inBounds(int val, int min, int max)
        {
            /// <summary>
            /// Method to return whether a value to determine bounds
            /// </summary>
            /// 
            bool valid = false;

            if ((val <= max) && (val >= min))
            {
                valid = true;
            }

            return valid;
        }

        public static string InitCap(string textToformat)
        {
            string pattern = @"\w+|\W+";
            string result = "";
            Boolean capitalizeNext = true;
            foreach (Match m in Regex.Matches(textToformat, pattern))
            {
                // get the matched string
                string x = m.ToString().ToLower();

                // if the first char is lower case
                if (char.IsLower(x[0]) && capitalizeNext)
                {
                    // capitalize it
                    x = char.ToUpper(x[0]) + x.Substring(1, x.Length - 1);
                }

                // Check if the word starts with Mc
                if (x[0] == 'M' && x[1] == 'c' && !String.IsNullOrEmpty(x[2].ToString()))
                {
                    // Capitalize the letter after Mc
                    x = "Mc" + char.ToUpper(x[2]) + x.Substring(3, x.Length - 3);

                }

                if (capitalizeNext == false)
                    capitalizeNext = true;

                // if the apostrophe is at the end i.e. Andrew's
                // then do not capitalize the next letter
                if (x[0].ToString() == "'" && m.NextMatch().ToString().Length == 1)
                {
                    capitalizeNext = false;
                }

                // collect all text
                result += x;
            }
            return result;
        }
    }
}
