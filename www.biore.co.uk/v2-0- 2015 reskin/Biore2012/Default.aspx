﻿ <%@ Page Title="Free Your Pores | Bioré® Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Get healthy-looking, radiant skin with products from Bioré® Skincare" name="description" />
    <meta content="skincare, clean face" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/homepage.css")
        .Add("~/css/flexslider.css")
        .Render("~/CSS/combinedhome_#.css")
    %>
    <meta name="viewport" content="width=device-width">

    <script type="text/javascript" src="<%= VirtualPathUtility.ToAbsolute("~/js/homepage.js") %>"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            //$('.scroll-dots a').click(function (event) {
            //    event.preventDefault();
            //    var target = $(this).attr('id');
            //    console.log(target);
            //});
        });
    </script>

    <script type="text/javascript">
         $(function() {
             $('#theaterItem1 a').click(
                 function (e) {
                     e.preventDefault();

                     var axel = Math.random() + "";
                     var a = axel * 10000000000000;
                     var strScript = "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore551;ord='" + a + "'?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"                     
                                + "<noscript>"
                                + "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore551;ord=1?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"
                                + "</noscript>";

                     //$('body').prepend(strScript);
                     var target = $(this).attr('target');
                     var uri = $(this).attr('href');

                     setTimeout(function () {
                         if (target) {
                             window.open(uri, '_blank');
                         } else {
                             window.location = uri;
                         }
                     }, 1000);
                     
                 });
         })
    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main1">
    <div class="home-content">        
        <section id="section-2017-interim">
                <div class="top-baking-soda-2017">
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/top-right-bubbles.png") %>" alt="top right bubbles" /></div>
                <div class="welcome-module clearfix">
					<div style="position: absolute; margin: 70px auto 0; text-align: center; width: 100%; cursor: pointer;"><a href="/biore-facial-cleansing-products/"><img src="images/spacer.gif" height="510" width="300" /></a></div>
                     <div class="header-2017">
                        <div class="acne-header-2017">
                            <h2>
                                <span class="acnes foamparty">Just Take It</span>
                              
                            </h2>
                            <h2 style="margin-top: 14px;">
                                <span class="acnessmall">All Off!</span>

                            </h2>
                        </div>
                       
                    </div>

                    <div class="left-text">
                        <div>
                           <span class="new">New </span> Bior&eacute;<sup>&reg;</sup><br />Cleansing<br />Micellar Waters<br /><br />from <span class="nbr1">#1</span> Cleansing Brand<br />in Japan<sup>*</sup>

                        </div>

                      

                    </div>

                     <div class="right-text">

                         <div class="allInOneCleansing">All-in-One<br />Cleansing!</div>

                         <div>
                            Remove dirt,<br />oil and makeup<br /><span class="waterproof">(even waterproof mascara!)</span>  

                         </div>

                           

                     </div>

                     <div class="footer-2017">
                        <div class="acne-footer-2017">
                            *Intage SRI, Facial Wash Market, Jun 2017 ～ May 2018, Accumulated total sales value of Biore Facial Cleanser Series
                        </div>
                    </div>


                    <div class="hide-mobile"></div>
                    <div class="scrollDownHldr">
                        <p class="scrollDown">SCROLL DOWN</p>
                        <div id="arrowBounce">
                            <a href="#section-2018-products">
                                <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/arrowDown.png") %>" class="section-down-arrow" /></a>
                        </div>

                    </div>
                </div>

                <div class="scroll-dots">
                    <a href="#section-2017-interim"><span class="scroll-dot enable"></span></a>
                <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                <a href="#section-06"><span class="scroll-dot"></span></a>
                <a href="#section-07"><span class="scroll-dot"></span></a>
                <a href="#section-08"><span class="scroll-dot"></span></a>
                
                <a href="#section-03"><span class="scroll-dot"></span></a>
                
                <a href="#section-05"><span class="scroll-dot"></span></a>
                </div>
               
            </section>
            <section class="home-section" id="section-2018-products">
                <h4>NO RUB, NO RINSE, NO RESIDUE!</h4>
                <div class="container3">

                    <div class="prodList">

                         <div class="videoHolder">
                          <div class="video pie">
                              <div id="video1">
                                  <a class="popup-youtube" href="https://www.youtube.com/watch?v=yS3YsTz1TtY">
                                      <img src="/images/homepage/homepage-video-still.jpg" alt="Shay Micellar Water" />
                                  </a>
                              </div>
                          </div>
                      </div>
                        
                        <ul>
                            <li class="productListSpacer">
                                <a href="/baking-soda-for-dry-oily-combination-skin/baking-soda-cleansing-micellar-water"><img src="images/ourProducts/products/small/baking-soda-cleansing-micellar-water.png" alt="BAKING SODA CLEANSING MICELLAR WATER" /></a>
                                <h3>BAKING SODA CLEANSING<br />MICELLAR WATER</h3>
                                <p>Specially formulated for combination skin. Removes makeup, deep cleans pores and balances without over-drying.
  <a href="/baking-soda-for-dry-oily-combination-skin/baking-soda-cleansing-micellar-water" class="buynowbtn"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/micellar-products-button.png") %>" alt="button"/></a></p>

                                
                               
                            </li>
                            <li class="productListSpacer">
                                <a href="/charcoal-for-oily-to-normal-skin/charcoal-cleansing-micellar-water"><img src="images/ourProducts/products/small/charcoal-cleansing-micellar-water.png" alt="CHARCOAL CLEANSING MICELLAR WATER" /></a>
                                <h3>CHARCOAL CLEANSING<br />MICELLAR WATER</h3>
                                <p>Specially formulated for oily skin. Removes makeup, deep cleans pores and removes excess oil.
 <a href="/charcoal-for-oily-to-normal-skin/charcoal-cleansing-micellar-water" class="buynowbtn"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/micellar-products-button.png") %>" alt="button"/></a></p>

                    
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="scroll-dots">
                    <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                <a href="#section-2018-products"><span class="scroll-dot enable"></span></a>
                <a href="#section-06"><span class="scroll-dot"></span></a>
                <a href="#section-07"><span class="scroll-dot"></span></a>
                <a href="#section-08"><span class="scroll-dot"></span></a>
                
                <a href="#section-03"><span class="scroll-dot"></span></a>
                
                <a href="#section-05"><span class="scroll-dot"></span></a>
                </div>
            </section> 
        <section class="home-section" id="section-06">
            <div class="bg-left"></div>
            <div class="bg-right"></div>
            <div class="container3">
                <div class="container2">
                    <div class="container1">
                        <div class="headline">
                            <h4>SO LONG SPOTS WITH THE BLACK FACIAL GEL!</h4>
                        </div>
                        <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/embed/csEft4NwP3g?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                <a href="https://www.youtube.com/embed/ffRpOxRF6Wg?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2-2018.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2-2018.gif") %>">
                                </a>
                                <div class="animation-skin-container">
                                    <div class="animation-skin">
                                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/06_anti_pickel-charcoal.png") %>" /></div>
                                </div>
                                <div class="animation-play-btn">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>" />
                                </div>
                            </div>
                        </div>
                        <div class="product">
                            <p>CHECK OUT HOW TO GET CLEARER SKIN IN JUST 2 DAYS WITH THE CHARCOAL ANTI-BLEMISH CLEARING CLEANSER</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="scroll-dots">
                <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                <a href="#section-06"><span class="scroll-dot enable"></span></a>
                <a href="#section-07"><span class="scroll-dot"></span></a>
                <a href="#section-08"><span class="scroll-dot"></span></a>
                
                <a href="#section-03"><span class="scroll-dot"></span></a>
                
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section" id="section-07">
            <div class="bg-left"></div>
            <div class="bg-right"></div>
            <div class="container3">
                <div class="container2">
                    <div class="container1">
                        <div class="headline">
                            <h4>SAY FAREWELL TO SPOTS WITH A BIORÉ FOAM PARTY!</h4>
                        </div>
                        <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/embed/Y3z8cMdZ6T4?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                <a href="https://www.youtube.com/embed/ffRpOxRF6Wg?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section3-2018.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section3-2018.gif") %>">
                                </a>
                                <div class="animation-skin-container">
                                    <div class="animation-skin">
                                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/07_baking_soda_anti-pickel.png") %>" /></div>
                                </div>
                                <div class="animation-play-btn">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>" />
                                </div>
                            </div>
                        </div>
                        <div class="product">
                            <p>LEARN HOW TO CLEAR BREAKOUTS WITH OUR FLUFFY BAKING SODA ANTI-BLEMISH CLEANSING FOAM</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="scroll-dots">
               <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                <a href="#section-06"><span class="scroll-dot"></span></a>
                <a href="#section-07"><span class="scroll-dot enable"></span></a>
                <a href="#section-08"><span class="scroll-dot"></span></a>
                
                <a href="#section-03"><span class="scroll-dot"></span></a>
                
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section" id="section-08">
           
                        <div class="skinapp">
                            <div class="skinapp-container">
                                <div class="start-skin-plan"><a href="http://cleanskinplan.biore.co.uk" target="skinapp"><img src="/images/homepage/start-skin-plan.png" alt="Start Here to Create Your Skin Plan" /></a></div>
                                <div class="skin-plan"><a href="http://cleanskinplan.biore.co.uk" target="skinapp"><img src="/images/homepage/skin-plan.png" alt="Create Your Skin Plan" /></a></div>
                                
                            </div>
                        </div>

            <div class="scroll-dots">
               <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                <a href="#section-06"><span class="scroll-dot"></span></a>
                <a href="#section-07"><span class="scroll-dot"></span></a>
                <a href="#section-08"><span class="scroll-dot enable"></span></a>
                
                <a href="#section-03"><span class="scroll-dot"></span></a>
                
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section" id="section-03">
            <div class="bg-left"></div><div class="bg-right"></div>
            <div class="container3">
	            <div class="container2">
		            <div class="container1">
			            <div class="headline">
				            <h4>Act like a super-magnet and remove grease, dirt and impurities</h4>
			            </div>
			            <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/embed/OOlpgPzHQzQ?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                <a href="https://www.youtube.com/embed/OOlpgPzHQzQ?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2.gif") %>">                                
                                </a>
                                <div class="animation-skin-container">
                                    <div class="animation-skin"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/03_charcoal_pore_strips.png") %>"/></div>
                                </div>
                                    <div class="animation-play-btn">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>
                                </div>
                            </div>
			            </div>
			            <div class="product">
				            <p>See how to use Deep Cleansing Charcoal Pore Strips</p>
			            </div>
		            </div>
	            </div>
            </div>

            <div class="scroll-dots">
                <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                <a href="#section-06"><span class="scroll-dot"></span></a>
                <a href="#section-07"><span class="scroll-dot"></span></a>
                <a href="#section-08"><span class="scroll-dot"></span></a>
                
                <a href="#section-03"><span class="scroll-dot enable"></span></a>
                
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section" id="section-05">
            <div class="bg-left"></div><div class="bg-right"></div>
            <div class="container3">
	            <div class="container2">
		            <div class="container1">
	
			            <div class="product">
				            <p>...and follow us on</p>
				            <div class="social-container">
				                <div class="facebook"><a href="https://www.facebook.com/BioreUK" target="_blank"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/facebook.png") %>" alt="facebook" /></a></div>
				                <div class="instagram"><a href="https://www.instagram.com/bioreuk" target="_blank"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/instagram.png") %>" alt="instagram" /></a></div>
				                <div class="youtube"><a href="https://www.youtube.com/channel/UCHKypMCBRZEgDbp-LKz3fnQ" target="_blank"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/youtube.png") %>" alt="youtube" /></a></div>
				            </div>
				            <p>#freeyourpores</p>
			            </div>
		            </div>
	            </div>
            </div>

            <div class="scroll-dots">
               <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                <a href="#section-06"><span class="scroll-dot"></span></a>
                <a href="#section-07"><span class="scroll-dot"></span></a>
                <a href="#section-08"><span class="scroll-dot"></span></a>
                <a href="#section-03"><span class="scroll-dot"></span></a>
                
                <a href="#section-05"><span class="scroll-dot enable"></span></a>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">

    $(function() {


        var viewport = {
            width: $(window).width(),
            height: $(window).height()
        };

        var loaded = false;

        if (viewport.width > 800 && loaded == false) {
            var section5img = new Image();
            section5img.src = '/images/homepage/gif/output.gif';
            section5img.onload = function() {
                document.getElementById('imganim').src = section5img.src;
            }
            loaded = true;
        }
    });

</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/jquery.flexslider.min.js")
        .Render("~/js/combinedhome_#.js")
    %>
</asp:Content>