﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Biore2012.BLL;
using Biore2012.DAL;
using System.Xml;

namespace Biore2012.our_products {
    public partial class Default : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e)
        {
            Site myMaster = (Site)this.Master;
            myMaster.bodyClass += " ourProducts";

            //check to see if ratings/reviews are active before proceeding.
            if (System.Configuration.ConfigurationManager.AppSettings["RatingsAndReviews"].ToLower() == "true")
            {
                //update the XML feed if necessary.
                XmlDocument xmlDoc = BazaarvoiceDAO.GetBVAPIFeed();

                //now build the ratings for each hard-coded product.

                
                //2018 additions
                litCharcoalAntiBlemishCleanser.Text = BazaarvoiceDAO.GetRatingsByProductID("charcoal-anti-blemish-cleanser");
                litBakingSodaAntiBlemishFoam.Text = BazaarvoiceDAO.GetRatingsByProductID("baking-soda-anti-blemish-cleansing-foam");
                litCharcoalOilControlScrub.Text = BazaarvoiceDAO.GetRatingsByProductID("charcoal-anti-blemish-scrub");
                litCharcoalMicellarWater.Text = BazaarvoiceDAO.GetRatingsByProductID("charcoal-cleansing-micellar-water");
                litBSMicellarWater.Text = BazaarvoiceDAO.GetRatingsByProductID("baking-soda-cleansing-micellar-water");
                litBSWhippedMask.Text = BazaarvoiceDAO.GetRatingsByProductID("blue-agave-baking-soda-whipped-mask");
                litCharcoalWhippedMask.Text = BazaarvoiceDAO.GetRatingsByProductID("charcoal-whipped-mask");

                //dirty & oily skin no more
                litBakingSodaScrub.Text = BazaarvoiceDAO.GetRatingsByProductID("baking-soda-cleansing-scrub");
                litBakingSodaPoreCleanser.Text = BazaarvoiceDAO.GetRatingsByProductID("baking-soda-pore-cleanser");
                litDeepPoreCharcoalCleanser.Text = BazaarvoiceDAO.GetRatingsByProductID("deep-pore-charcoal-cleanser");
                litSelfHeatingOneMinuteMask.Text = BazaarvoiceDAO.GetRatingsByProductID("self-heating-one-minute-mask");
                litCharcoalBar.Text = BazaarvoiceDAO.GetRatingsByProductID("charcoal-bar");

                //hello visibly smaller pores
                litCharcoalPoreMinimizer.Text = BazaarvoiceDAO.GetRatingsByProductID("charcoal-pore-minimizer");

                //blemish
                litAcneClearingScrub.Text = BazaarvoiceDAO.GetRatingsByProductID("acne-clearing-scrub");

                //breakup with blackheads
                litPoreStrips.Text = BazaarvoiceDAO.GetRatingsByProductID("deep-cleansing-pore-strips");
                litPoreStripsUltra.Text = BazaarvoiceDAO.GetRatingsByProductID("deep-cleansing-pore-strips-ultra");
                litPoreStripsCombo.Text = BazaarvoiceDAO.GetRatingsByProductID("deep-cleansing-pore-strips-combo");
                litCharcoalStrips.Text = BazaarvoiceDAO.GetRatingsByProductID("charcoal-pore-strips");
                

               
            }
        }
    }
}
