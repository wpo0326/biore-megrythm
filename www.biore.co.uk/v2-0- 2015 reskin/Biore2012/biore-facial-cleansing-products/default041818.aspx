﻿<%@ Page Title="Bioré Facial Pore Cleansing Products for Blackheads & Blemishes" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.our_products.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Bioré Skincare products cleanse and gently exfoliate, giving you healthy, clean skin! See our product range of liquid, foam, scrub, and strips…" name="description" />
    <meta content="Clear Skin, Biore&reg; Products" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>

    <meta name="viewport" content="width=device-width">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="intro">
                <a href="https://cleanskinplan.biore.co.uk" class="skinPlanProd" target="_blank"><img src="http://www.biore.co.uk/images/header/skin-plan-nav.png"></a>
                <h1>Our Products</h1>
                <div id="responseRule"></div>
                <p>Our highly effective Bioré products clean your pores from dirt, oil and blackheads to help you to effectively fight against impurities. Whether used as washes, scrubs, masks or pore strips – Bioré deep cleans your pores at the source. For clean skin and free pores. Because when you clean the pore, you clear the problem.</p>
            </div>


            <div id="deepCleansingProds" class="prodList">
                <h2 class="pie roundedCorners deepCleansing"><span>charcoal</span> for oily and normal skin
                    <span class="arrow"></span>
                </h2>
                <ul>
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-anti-blemish-cleanser_100X235.png" alt="" />
                        <asp:Literal ID="litCharcoalAntiBlemishCleanser" runat="server" />
                         
                        <h3>Charcoal Anti-Blemish<br />Cleanser</h3>
                        <p>Daily anti-spots cleanser with natural charcoal to absorb excess oil and clear breakouts.</p>
                        <a href="../charcoal-for-oily-to-normal-skin/charcoal-anti-blemish-cleanser" id="A1">details ></a>
                    </li>
                    
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CHARCOAL_100X235.png" alt="" />
                         <asp:Literal ID="litDeepPoreCharcoalCleanser" runat="server" />
                        <h3>Deep Pore<br />Charcoal Cleanser</h3>
                        <p>A pore-cleansing facial wash that deep cleans 2x better* & naturally purifies.</p>
                        <a href="../charcoal-for-oily-to-normal-skin/deep-pore-charcoal-cleanser" id="A1">details ></a>
                    </li>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalBar.png" alt="" />
                         <asp:Literal ID="litCharcoalBar" runat="server" />
                        <h3>Pore Penetrating <br />Charcoal Bar</h3>
                        <p>A facial cleansing bar that deep cleans & gently exfoliates for 2.5x cleaner pores*.</p>
                        <a href="../charcoal-for-oily-to-normal-skin/pore-penetrating-charcoal-bar" id="A1">details ></a>
                    </li> 
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreMinimizer.png" alt="" />
                        <asp:Literal ID="litCharcoalPoreMinimizer" runat="server"></asp:Literal>
                        <h3>Charcoal<br />
                      Pore Minimizer</h3>
                        <p>A facial treatment that gently exfoliates & deep cleans to instantly reduce the appearance of pores.	</p>
                        <a href="../baking-soda-for-dry-oily-combination-skin/charcoal-pore-minimizer" id="details-charcoal-pore-minimizer">details ></a>
                    </li>
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CharcoalOilControlScrub.png" alt="" />
                        <asp:Literal ID="litCharcoalOilControlScrub" runat="server" />
                        <h3>CHARCOAL OIL <br />CONTROL SCRUB</h3>
                        <p>A facial scrub to deep clean, exfoliate, and absorb excess oil. With natural charcoal and the anti-blemish fighting power of salicylic acid.</p>
                        <a href="../blemishes-are-out-of-here/charcoal-oil-control-scrub" id="details-charcoal-oil-control-scrub">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/SHM_100X235.png" alt="" />
                        <asp:Literal ID="litSelfHeatingOneMinuteMask" runat="server" />
                        <h3>Self Heating<br />One Minute Mask</h3>
                        <p>Unclog pores with this soothing facial mask. Purifies pores 2.5x better*.</p>
                        <a href="../charcoal-for-oily-to-normal-skin/self-heating-one-minute-mask" id="A2">details ></a>
                    </li>           
                </ul>
            </div>

            <div id="bigPoresProds" class="prodList">
                <h2 class="pie roundedCorners bigPores"><span>baking soda </span>for dry /<br />oily combination skin
                    <span class="arrow"></span>
                </h2>
                <ul>
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-anti-blemish-cleansing-foam_100X235.png" alt="" />
                        <asp:Literal ID="litBakingSodaAntiBlemishFoam" runat="server" />
                        <h3>Baking Soda Anti-Blemish<br />Cleansing Foam</h3>
                        <p>Deep clean with unique foam to help clear and prevent breakouts without over-drying. It's surprisingly strong yet gentle, and oh-so-fun!</p>
                        <a href="../baking-soda-for-dry-oily-combination-skin/baking-soda-anti-blemish-cleansing-foam" id="A4">details ></a>
                    </li>
                    
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-pore-cleanser.png" alt="" />
                         <asp:Literal ID="litBakingSodaPoreCleanser" runat="server" />
                        <h3>Baking Soda<br />Pore Cleanser</h3>
                        <p>A facial cleanser that deep cleans pores & gently exfoliates.</p>
                        <a href="../charcoal-for-oily-to-normal-skin/baking-soda-pore-cleanser" id="A4">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-cleansing-scrub.png" alt="" />
                            <asp:Literal ID="litBakingSodaScrub" runat="server" />
                        <h3>Baking Soda<br />Cleansing Scrub</h3>
                        <p>A facial scrub that activates with water for a deep clean and gentle exfoliation.</p>
                        <a href="../charcoal-for-oily-to-normal-skin/baking-soda-cleansing-scrub" id="A5">details ></a>
                    </li>
                </ul>
            </div>

            <div id="murtProds" class="prodList">
                <h2 class="pie roundedCorners murt"><span>pore strips </span>for blackhead removal
                    <span></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/acneClearingScrub.png" alt="" />
                        <asp:Literal ID="litAcneClearingScrub" runat="server" />
                        <h3>BLEMISH CLEARING<br />SCRUB</h3>
                        <p>Defeat deep-down dirt and oil in 2 days.</p>
                        <a href="../blemishes-are-out-of-here/acne-clearing-scrub" id="details-acne-clearing-scrub">details ></a>
                    </li>-->
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStrips.png" alt="" />
                        <asp:Literal ID="litPoreStrips" runat="server" />
                        <h3>DEEP CLEANSING<br />PORE STRIPS</h3>
                        <p>Facial pore strips to unclog pores & achieves the deepest clean. </p>
                        <a href="../pore-strips-for-blackhead-removal/deep-cleansing-pore-strips" id="details-deep-cleansing-pore-strips">details ></a>
                    </li>
                    
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltra.png" alt="" />
                        <asp:Literal ID="litPoreStripsUltra" runat="server" />
                        <h3>Ultra Deep Cleansing Pore Strips</h3> 
                        <p>Facial pore strips to unclog pores for the deepest clean with Witch Hazel & <br />Tea Tree Oil.</p>
                        <a href="../pore-strips-for-blackhead-removal/deep-cleansing-pore-strips-ultra" id="details-deep-cleansing-pore-strips-ultra">details ></a>
                    </li>                 
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.png" alt="" />
                        <asp:Literal ID="litPoreStripsCombo" runat="server" />
                        <h3>Combo Deep Cleansing<br /> Pore Strips</h3>
                        <p>A mixed pack of our facial pore strips: 7 nose strips & 7 face strips</p>
                        <a href="../pore-strips-for-blackhead-removal/deep-cleansing-pore-strips-combo" id="details-deep-cleansing-pore-strips-combo">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" />
                        <asp:Literal ID="litCharcoalStrips" runat="server" />
                        <h3>Deep Cleansing <br /> Charcoal Pore Strips</h3>
                        <p>Pore strips to unclog pores & draw out excess oil for the deepest clean.</p>
                        <a href="../pore-strips-for-blackhead-removal/charcoal-pore-strips" id="details-deep-cleansing-pore-strips-face">details ></a>
                    </li>
                </ul>
            </div>
            
             
            
            <div style="font-size:9px; line-height:10px; margin:20px 0;">* Bioré<sup>&reg;</sup> Ultra Deep Cleansing Pore Strips remove 2x more deep pore clogs than original Bioré®  Deep Cleansing Pore Strips</div>
        

        
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
