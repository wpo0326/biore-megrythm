﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Charcoal.aspx.cs" Inherits="Biore2012.biore_facial_cleansing_products.Charcoal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Purify pores with our Charcoal Mask and Charcoal Face Wash. Try Deep Pore Charcoal Cleanser and Self Heating One Minute Mask from Bioré® Skincare." />
    <meta content="Charcoal Mask, Charcoal Face Wash, charcoal" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/charcoalLanding.css")
        .Render("~/css/combinednew_#.css")
    %>
    <meta name="viewport" content="width=device-width">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">
<script type="text/javascript" src="http://o2.eyereturn.com/?site=2687&amp;page=biore_charcoal_EN"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main" class="charcoalLanding">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
                <div class="charcoalHeaderSpacer"></div>
                <h1><img src="<%= VirtualPathUtility.ToAbsolute("~/images/charcoalLanding/charcoalHeader.png") %>" border="0" alt="" /></h1>
                
            <div id="charcoalProdWrap">
                <div id="charDescript">
                    <!--<div id="charPaint"></div>-->
                    <!--<h2 class="archer-book">Really? <span class="charGrey archer-bold">Charcoal?</span></h2>-->
                    <p>Yes, CHARCOAL! Like a magnet, it draws out and traps impurities. And now, Bioré® Skincare brings you this revolutionary ingredient in 2 new products:  <br /><br />
                        <b>NEW <span class="charGreeen">DEEP CLEANSING CHARCOAL PORE STRIPS</span></b> and <b><span class="charGreeen">CHARCOAL PORE MINIMISER.</span></b><br />   Both products draw out deep down dirt, oil, and impurities from pores. They purify skin and remove 2x more dirt and oil than a basic cleanser. Use together for a deep clean you can see.</p>
                </div>
            <!--    <div id="charProducts"></div>
            </div>
            
            <div class="charList archer-bold">
                <span class="charGreeen">Charcoal Pore Strips and Charcoal Pore Minimizer</span><br /><br />

                <span class="charbullet"></span>Deep cleans 40% more dirt & impurities than a basic cleanser.<br />
                <span class="charbullet"></span>Draws out and traps 2.5x more surface impurities. <br />
                <span class="charbullet"></span>Leaves skin tingly-smooth.<br />
                <span class="charbullet"></span>Proven to purify pores instantly.<br />
                <span class="charbullet"></span>Deep cleans all 200,000 of your pores.<br />
            </div>-->

            <div id="charTryNowWrap">
                <div id="charTryProdOne">
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/deep-cleansers/charcoal-pore-minimizer") %>"><div class="charTry">Try it Now</div></a>
                </div>
                <div id="charTryProdTwo">
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/deep-cleansers/charcoal-pore-strips#regular") %>"><div class="charTry">Try it Now</div></a>
                </div>
            </div>
           
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
