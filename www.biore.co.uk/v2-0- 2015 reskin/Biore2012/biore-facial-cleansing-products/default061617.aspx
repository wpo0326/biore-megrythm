﻿<%@ Page Title="Free Your Pores – Get Clean, Healthy Skin – Browse All Products  | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.our_products.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Bior&eacute;&reg; Skincare products cleanse and gently exfoliate, giving you healthy, clean skin! See the complete Bior&eacute;&reg; Skincare product line." name="description" />
    <meta content="Clear Skin, Biore&reg; Products" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>
    <meta name="viewport" content="width=device-width">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="intro">
                <h1>Our Products</h1>
                <div id="responseRule"></div>
                <p>Targets the cause of all skin problems &mdash; the clogged pore. The powerful, pore–cleansing Bioré<sup>&reg;</sup> products come in liquid, foam, scrub, wipes and strip forms so you can remove pore–clogging debris! Go from school to the library, home to beach, and from work to working out, with skin that's clear and healthy-looking. Because when you clean the pore, you clear the problem.</p>
            </div>


            <div id="deepCleansingProds" class="prodList">
                <h2 class="pie roundedCorners deepCleansing"><span>dirty</span> & oily skin no more
                    <span class="arrow"></span>
                </h2>
                <ul>
                       <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-cleansing-scrub.png" alt="" />
                            <asp:Literal ID="litBakingSodaScrub" runat="server" />
                        <h3>Baking Soda<br />Cleansing Scrub</h3>
                        <p>Activates with water for a deep clean and gentle exfoliation.</p>
                        <a href="../charcoal-for-oily-to-normal-skin/baking-soda-cleansing-scrub" id="A5">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-pore-cleanser.png" alt="" />
                         <asp:Literal ID="litBakingSodaPoreCleanser" runat="server" />
                        <h3>Baking Soda<br />Pore Cleanser</h3>
                        <p>Deep cleans pores & gently exfoliates.</p>
                        <a href="../charcoal-for-oily-to-normal-skin/baking-soda-pore-cleanser" id="A4">details ></a>
                    </li>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CHARCOAL_100X235.png" alt="" />
                         <asp:Literal ID="litDeepPoreCharcoalCleanser" runat="server" />
                        <h3>Deep Pore<br />Charcoal Cleanser</h3>
                        <p>Deep cleans 2x better* & naturally purifies.</p>
                        <a href="../charcoal-for-oily-to-normal-skin/deep-pore-charcoal-cleanser" id="A1">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/SHM_100X235.png" alt="" />
                        <asp:Literal ID="litSelfHeatingOneMinuteMask" runat="server" />
                        <h3>Self Heating<br />One Minute Mask</h3>
                        <p>Purifies pores 2.5x better*.</p>
                        <a href="../charcoal-for-oily-to-normal-skin/self-heating-one-minute-mask" id="A2">details ></a>
                    </li>           
                    
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalBar.png" alt="" />
                         <asp:Literal ID="litCharcoalBar" runat="server" />
                        <h3>Pore Penetrating <br />Charcoal Bar</h3>
                        <p>Deep cleans & gently exfoliates for 2.5x cleaner pores*.</p>
                        <a href="../charcoal-for-oily-to-normal-skin/pore-penetrating-charcoal-bar" id="A1">details ></a>
                    </li> 
                </ul>
            </div>

            <div id="bigPoresProds" class="prodList">
                <h2 class="pie roundedCorners bigPores">hello visibly <span>smaller pores</span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreMinimizer.png" alt="" />
                        <asp:Literal ID="litCharcoalPoreMinimizer" runat="server"></asp:Literal>
                        <h3>Charcoal<br />
                      Pore Minimizer</h3>
                        <p>Gently exfoliates & deep cleans to instantly reduce the appearance of pores.	</p>
                        <a href="../baking-soda-for-dry-oily-combination-skin/charcoal-pore-minimizer" id="details-charcoal-pore-minimizer">details ></a>
                    </li>
                    
                </ul>
            </div>

            <div id="complexionClearingProds" class="prodList">
                <h2 class="pie roundedCorners complexionClearing">blemishes <span>are out of here</span>
                    <span></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CharcoalOilControlScrub.png" alt="" />
                        <asp:Literal ID="litCharcoalOilControlScrub" runat="server" />
                        <h3>CHARCOAL OIL <br />CONTROL SCRUB</h3>
                        <p>Deep clean, exfoliate, and absorb excess oil. With natural charcoal and the anti-blemish fighting power of salicylic acid.</p>
                        <a href="../blemishes-are-out-of-here/charcoal-oil-control-scrub" id="details-charcoal-oil-control-scrub">details ></a>
                    </li>
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/acneClearingScrub.png" alt="" />
                        <asp:Literal ID="litAcneClearingScrub" runat="server" />
                        <h3>BLEMISH CLEARING<br />SCRUB</h3>
                        <p>Defeat deep-down dirt and oil in 2 days.</p>
                        <a href="../blemishes-are-out-of-here/acne-clearing-scrub" id="details-acne-clearing-scrub">details ></a>
                    </li>-->
                  
                </ul>
            </div>

            <div id="murtProds" class="prodList">
                <h2 class="pie roundedCorners murt">break up with <span>blackheads</span>
                    <span class="arrow"></span>
                </h2>


                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStrips.png" alt="" />
                        <asp:Literal ID="litPoreStrips" runat="server" />
                        <h3>DEEP CLEANSING<br />PORE STRIPS</h3>
                        <p>Unclogs pores & achieves the deepest clean. </p>
                        <a href="../pore-strips-for-blackhead-removal/pore-strips#regular" id="details-deep-cleansing-pore-strips">details ></a>
                    </li>
                    
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltra.png" alt="" />
                        <asp:Literal ID="litPoreStripsUltra" runat="server" />
                        <h3>Ultra Deep Cleansing Pore Strips</h3>
                        <p>With Witch Hazel & <br />Tea Tree Oil.</p>
                        <a href="../pore-strips-for-blackhead-removal/pore-strips#ultra" id="details-deep-cleansing-pore-strips-ultra">details ></a>
                    </li>                 
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.png" alt="" />
                        <asp:Literal ID="litPoreStripsCombo" runat="server" />
                        <h3>Combo Deep Cleansing<br /> Pore Strips</h3>
                        <p>7 nose strips & 7 face strips</p>
                        <a href="../pore-strips-for-blackhead-removal/pore-strips#combo" id="details-deep-cleansing-pore-strips-combo">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" />
                        <asp:Literal ID="litCharcoalStrips" runat="server" />
                        <h3>Deep Cleansing <br /> Charcoal Pore Strips</h3>
                        <p>Unclogs pores & draws out excess oil for the deepest clean.</p>
                        <a href="../pore-strips-for-blackhead-removal/charcoal-pore-strips" id="details-deep-cleansing-pore-strips-face">details ></a>
                    </li>
                </ul>
            </div>
            
             
            
            <div style="font-size:9px; line-height:10px; margin:20px 0;">* Bioré<sup>&reg;</sup> Ultra Deep Cleansing Pore Strips remove 2x more deep pore clogs than original Bioré®  Deep Cleansing Pore Strips</div>
        

        
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
