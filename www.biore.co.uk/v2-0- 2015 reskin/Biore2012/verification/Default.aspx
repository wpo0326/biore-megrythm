﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Biore2012.verification.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <meta content="UK Number One Claim" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>

    <style type="text/css">
        a {
            text-decoration: underline;
        }
        h2 {
            margin: auto;
            font-size: 2em;
            text-align:center
        }

        .verificationImages {
            text-align:center;
                width:90%;
        }

            .verificationImages img {
                width: 100%;
                max-width: 1050px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="floodlightPixels" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="main">
        <div id="mainContent" style="min-height: 730px">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <br />
                <br /><br />
                  <h1>Bioré Deep Cleansing<br />Pore Strips</h1>
                   <h2>UK Number One Claim </h2>
                <div class="verificationImages">
                <img src="/images/verification/1.jpg" alt="Verification Page 1" />
                <img src="/images/verification/2.jpg" alt="Verification Page 2" />
                    </div>
                </div>
            </div>
        </div>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
