﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Biore2012.facebook.permalinks.deep_cleansing_pore_strips_ultra
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            ogImage.Content = "http://" + Request.Url.Host + VirtualPathUtility.ToAbsolute("~/images/ourProducts/products/social/deepCleansingPoreStripsUltra.png");
            metaRefresh.Content = "0;url=" + VirtualPathUtility.ToAbsolute("~/pore-strips-for-blackhead-removal/deep-cleansing-pore-strips-ultra");
            //Response.Status = "301 Moved Permanently";
            // Response.AddHeader("Location", VirtualPathUtility.ToAbsolute("~/pore-strips-for-blackhead-removal/deep-cleansing-pore-strips-ultra"));
        }
    }
}
