﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using Biore2012.BLL;
using System.Text;

namespace Biore2012 {
    public partial class Site : System.Web.UI.MasterPage {

        public string localVar;
        private string _bodyClass;
        public string bodyClass {
            get { return _bodyClass; }
            set { _bodyClass = value; }
        }

        private bool _isMobile;
        public bool isMobile
        {
            get { return _isMobile; }
            set { _isMobile = value; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            // Is this a mobile device?
            BioreUtils bu = new BioreUtils();
            HttpRequest httpRequest = HttpContext.Current.Request;
            isMobile = bu.checkIsMobile(httpRequest);

            if (isMobile == true) {
                bodyClass = "mobile";
            }
            else { 
                bodyClass = "desktop";
                fbScript.Visible = true;
                likeBtn.Visible = true;
                panel_Question_Pro.Visible = true;
            }

            //bu.configureProveItLinks(isMobile, proveItLink);

            string fbImagepath = "http://" + Request.Url.Host + VirtualPathUtility.ToAbsolute("~/images/facebook_share_image.jpg");
            fbImage.Attributes.Add("content", fbImagepath);

            // assign current year
            litDate.Text = DateTime.Now.Year.ToString();
        }

        protected void Page_Load(object sender, EventArgs e) {


            // Kao Brands name change;
            DateTime EndDate = new DateTime(2012, 1, 1);
            if (DateTime.Now.Date < EndDate.Date)
            {
                kaoBrandsLinkText.Text = "Kao Brands Company";
                kaoBrandsCopyright.Text = "Kao Brands Company.";
            }
            else
            {
                kaoBrandsLinkText.Text = "KAO";
                kaoBrandsCopyright.Text = "Kao (UK) Limited";
            }

            if (Request.Cookies["Survey_1352391"] != null) {
                panel_Question_Pro.Visible = false;
            }

            //Add Logout and Change Password links if logged in.
            if (System.Configuration.ConfigurationManager.AppSettings["RatingsAndReviews"] == "true")
            {
                AuthUtils au = new AuthUtils();
                if (au.CheckIfAuthenticated() == true)
                {
                    hlLogout.Visible = true;
                    hlLogout.Text = System.Configuration.ConfigurationManager.AppSettings["logoutText"];
                    hlLogout.NavigateUrl = System.Configuration.ConfigurationManager.AppSettings["path"] + "FormConfig/Ratings-And-Reviews/Logout.aspx?return=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri);
                    hlLogout.CssClass = "logout";

                    hlChangePassword.Visible = true;
                    hlChangePassword.Text = System.Configuration.ConfigurationManager.AppSettings["changePasswordText"];
                    hlChangePassword.NavigateUrl = System.Configuration.ConfigurationManager.AppSettings["path"] + "FormConfig/Ratings-And-Reviews/ChangePassword.aspx?return=" + Server.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri);
                    hlChangePassword.CssClass = "changePassword";
                }
            }
        }

    }
}
