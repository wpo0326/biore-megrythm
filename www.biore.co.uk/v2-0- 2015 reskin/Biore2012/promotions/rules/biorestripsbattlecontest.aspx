﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="5-days-of-swag.aspx.cs" Inherits="Biore2012.__days" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedrules_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <div id="polaroid"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/forms/signUpPhoto.jpg") %>" alt="" /></div>
                <div id="content">
                    <h1>Terms and Conditions for Elle Biore Competition</h1>

                     <p>1.	The Promotion is open to residents of the United Kingdom and Ireland aged 18 and over, excluding employees and immediate relatives of employees of Kao (UK) Limited (the “Promoter”) or their respective parents, subsidiaries or affiliated companies or any other person professionally connected with the competition.</p>
                      
                    <p>2.	To enter the competition, participants must post a selfie on Instagram of themselves wearing their chosen Biore Pore Strip (either Deep Cleansing Charcoal Pore Strips or Ultra Deep Cleansing Pore Strips) telling us why they love it with the tags #biorestripsbattle @bioreuk (the “Entry Content”).</p>
                      
                    <p>3.	The closing date for entries is 11.59pm (GMT) on 31st December 2015 (the “Closing Date”).</p>
                      
                    <p>4.	One (1) entry per person. For the avoidance of doubt, this also applies where the same entrant uses multiple social media accounts. Multiple entries will be void and the entrant disqualified.</p>
                      
                    <p>5.	Three (3) winners will be selected at random by the Promoter from all valid entries received containing the correct Entry Content after the Closing Date.  </p>
                      
                    <p>6.	Each winner will receive a year’s worth of Biore products.</p>
                      
                    <p>7.	The selected winner will be notified by the Promoter by email advising them of their prize and the steps required to claim this.</p>
                      
                    <p>8.	In the event that the Promoter cannot for any reason contact a prize winner within twenty four (24) hours of first attempting to do so, the Promoter reserves the right to select another winner from all valid entries received. </p>
                      
                    <p>9.	For the avoidance of doubt, the prize does not consist of anything other than expressly set out in Term 6 above and no additional prizes are included. </p>
                      
                    <p>10.	Winners are responsible for all applicable taxes and expenses not specified in the prize description. </p>
                      
                    <p>11.	No purchase necessary.</p>
                      
                    <p>12.	The photo submitted must not contain any matter that is obscene, offensive, derogatory, or otherwise inappropriate. The photo must be an original work created for this competition and must not have been previously published. The content of the photo must not violate or infringe any other person’s rights, including but not limited to copyright. </p>
                      
                    <p>13.	Responsibility will not be accepted for loss, delay and corruption of any data in transit or in the incomplete nature of any entries to the prize draw.</p>
                      
                    <p>14.	The prize is non-transferable and there is no cash alternative. The Promoter reserves the right, at its sole discretion, to award prizes of equal or greater value, should the advertised prize become unavailable for any reason. </p>
                      
                    <p>15.	The winners agree to take part in publicity relating to the competition including but not limited to; photo-shoots and interviews, only if requested to do so by the Promoter. </p>
                      
                    <p>16.	By entering this competition all entrants hereby grant to the Promoter and their respective parents, subsidiaries and affiliated companies a perpetual, worldwide, royalty-free, irrevocable, exclusive licence to use, reproduce, copy, adapt, transmit, broadcast, publish or delete any information or other material including without limitation, the entrant’s name, entry, photograph, likeness, biographical information, voice, text, images, designs, audio and/or video clips submitted in entering or any statement made by them concerning the competition ("Submissions") in any and all media as they, in their discretion, see fit.</p>
                      
                    <p>17.	All entrants hereby irrevocably and unconditionally assign to the Promoter with full title guarantee (and by way of present assignment of present and future rights) all intellectual property rights in any of the Submissions throughout the world for the full unexpired period of such rights and all renewals, reversions and extensions of such period as may be provided under any applicable law throughout the world.</p>
                      
                    <p>18.	The Promoter reserves the right to select an alternative winner in the event that the Promoter has reasonable grounds for believing that a selected winner has contravened any of these terms.  Any alternative winner will be selected applying the same criteria as that used to select the original winner.</p>
                      
                    <p>19.	The Promoter reserves the right to select an alternative winner in the event that the Promoter has reasonable grounds for believing that a selected winner has contravened Instagram’s Terms of Use. </p>
                      
                    <p>20.	Instagram takes on no responsibilities in respect of this competition. This competition is in no way sponsored, endorsed or administered by, or associated with Instagram.   </p>
                      
                    <p>21.	Entrants will be deemed to have accepted these rules and agree to be bound by them.</p>
                      
                    <p>22.	We will not pass on your personal details to any other organisation without your permission.</p>
                      
                    <p>23.	All entries must be made directly by the person entering the promotion. Bulk, third-party or entries made online using methods such as a macro, a script or the use of automated devices or processes are not allowed, and all such entries shall be disqualified.</p>
                      
                    <p>24.	The judge’s decision is final.  No correspondence will be entered into. </p>
                      
                    <p>25.	The promoter of the competition is Kao (UK) Limited, 130 Shaftesbury Avenue, London W1D 5EU. </p>
                      
                    <p>26.	The name of the winners may be obtained by sending a stamped, self-addressed envelope to Kao (UK) Limited, 130 Shaftesbury Avenue, London, W1D 5EU (specifying the name of the competition) for ten (10) days after the competition closes. </p>
                      
                    <p>27.	The Promotor reserves the right to amend and/or withdraw the competition at any time without prior notice.</p>

                </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
