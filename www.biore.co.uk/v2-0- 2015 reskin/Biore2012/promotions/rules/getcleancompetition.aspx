﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="5-days-of-swag.aspx.cs" Inherits="Biore2012.__days" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedrules_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <!--<div id="polaroid"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/forms/signUpPhoto.jpg") %>" alt="" /></div>-->
                <div id="content">
                    <h1>Terms and Conditions for Biore UK #GetClean Competition</h1>

                     <p>1.	Eligibility: This promotion is only open to UK residents aged 16 years and over. Employees (and their immediate families) of KAO (UK) LIMITED (the “Promoter”), or their respective subsidiaries or affiliated companies or any other persons professionally connected with the competition are not eligible to enter.</p>
                      
                    <p>2.	To enter the competition, participants must follow @BioreUK on Instagram and post a #nomakeup selfie with the Biore Baking Soda Scrub in view, hashtagging #GetClean #WinBiore, and mentioning @BioreUK (“Entry Content”).</p>
                      
                    <p>3.	No purchase is necessary, however internet access is required.</p>
                      
                    <p>4.	Entries are accepted from 8am (BST) Monday, 14th November 2016 until 12:00am (BST) on Sunday, 27th November 2016.</p>
                      
                    <p>5.	One (1) entry per person. </p>
                      
                    <p>6.	Five (5) winners shall be selected at random by the Promoter from all valid entries received containing the correct Entry Content after 5pm (BST) on Monday, 28th November 2016. </p>
                      
                    <p>7.	Each winner shall receive one (1) of #GetClean prize, including: 
				•	Four (4) Bioré Ultra Deep Cleansing Pore Strips (box of 6) 
				•	Four (4) Bioré Deep Cleansing Charcoal Pore Strips (box of 6) 
				•	Two (2) Bioré Deep Pore Charcoal Cleanser 200ml 
				•	Two (2) Bioré Baking Soda Pore Cleanser 200ml 
				•	One (1) Bioré Charcoal Pore Minimizer 92ml
				•	One (1) Bioré Baking Soda Cleansing Scrub 125g
				•	One (1) £100 Love2shop voucher
</p>
                      
                    <p>8.	Selected winners shall be notified by the Promoter via a direct message to their Instagram, advising them of their prize and the steps required to claim this.</p>
                      
                    <p>9.	In the event that the Promoter cannot for any reason contact a prize winner within five (5) days of first attempting to do so, the Promoter reserves the right to select another winner from all valid entries received. </p>
                      
                    <p>10.	For the avoidance of doubt, the prize does not consist of anything other than expressly set out in Term 7 above and no additional prizes are included.</p>
                      
                    <p>11.	Winners are responsible for all applicable taxes and expenses not specified in the prize description. </p>
                      
                    <p>12.	The photo submitted must not contain any matter that is obscene, offensive, derogatory, or otherwise inappropriate. The photo must be an original work created for this competition and must not have been previously published. The content of the photo must not violate or infringe any other person’s rights, including but not limited to copyright.</p>
                      
                    <p>13.	The Promoter will not be liable for entries not received, incomplete, delayed or damaged</p>
                      
                    <p>14.	The prize is non-transferable and there is no cash alternative. The Promoter reserves the right, at its sole discretion, to award prizes of equal or greater value, should the advertised prizes (or any part of them) become unavailable for any reason. The prize is subject to the additional terms and conditions of the manufacturer or supplier. </p>
                      
                    <p>15.	All entrants hereby irrevocably and unconditionally assign to the Promoter with full title guarantee (and by way of present assignment of present and future rights) all intellectual property rights in any information or other material including without limitation, the entrant’s name, entry, photograph, likeness, biographical information, voice, text, images, designs, audio and/or video clips submitted in entering or any statement made by them concerning the competition throughout the world for the full unexpired period of such rights and all renewals, reversions and extensions of such period as may be provided under any applicable law throughout the world.</p>
                      
                    <p>16.	The Promoter reserves the right to verify all entries including but not limited to asking for address and identity details (which they must provide within 5 days) and to refuse to award a prize or withdraw prize entitlement and/or refuse further participation in the promotion and disqualify the participant where there are reasonable grounds to believe there has been a breach of these terms and conditions or any instructions forming part of this promotions entry requirements or otherwise where a participant has gained unfair advantage in participating in the promotion or won using fraudulent means. The Promoter will be the final arbiter in any decisions and these will be binding and no correspondence will be entered into.</p>
                      
                    <p>17.	In the event of circumstances outside the reasonable control of the Promoter, or otherwise where fraud, abuse, and/or an error (human or computer) affects or could affect the proper operation of this promotion, and only where circumstances make this unavoidable, the Promoter reserves the right to cancel or amend the promotion or these terms and conditions, at any stage, but will always endeavour to minimize the effect to participants in order to avoid undue disappointment. </p>
                      
                    <p>18.	This promotion is in no way sponsored, endorsed or administered by, nor associated with Instagram. You understand that you are providing information to the Promoter and not to Instagram. The information you provide will only be used for fulfilment of this promotion unless you have agreed to its use for any other purpose. By entering the promotion, all participants agree to a complete liability release for Instagram. The Promoter accepts no responsibility for late, incomplete, incorrectly submitted, corrupted or misdirected entries, nor for network failure or delay.</p>
                      
                    <p>19.	The Promoter does not guarantee continuous uninterrupted or secure access to Instagram. Numerous factors outside the control of the Promoter may interfere with the operation of Instagram. No responsibility will be accepted for any difficulties in entering or any entries delayed or corrupted.</p>
                      
                    <p>20.	The Promoter and its associated agencies and companies will not be liable to any prize winner for any loss (including, without limitation, indirect, special or consequential loss, or loss of profits), expense or damage which is suffered or sustained (whether or not arising from any person's negligence) in connection with this promotion, or accepting or using a prize.</p>
                      
                    <p>21.	Prizes are not transferable or exchangeable unless otherwise stated and cannot be redeemed for cash or any other form of compensation. In the event, for any reason, a prize winner does not take any element of their prize at the time stipulated by the Promoter, then that element of the prize will be forfeited by the winner and neither cash nor any other form of compensation will be supplied in lieu of that element of the prize, unless otherwise agreed. If due to unforeseen circumstances a prize is not available, the Promoter reserves the right to substitute another prize for it, in its sole discretion, of equal or higher value.</p>
                      
                    <p>22.	In all aspects of this promotion, the Promoter's decision is final.</p>
                      
                    <p>23.	The names of the winners may be obtained at the end of the promotion for a period of three months by sending a stamped SAE to: John Frieda Promotion, Brave, 7 Heathmans Road, Parsons Green, London SW6 4TJ.</p>
                      
                    <p>24.	All entries must be made directly by the person entering the promotion. Bulk, third-party or entries made online using methods such as a macro, a script or the use of automated devices or processes are not allowed, and all such entries shall be disqualified.</p>
                      
                    <p>25.	Data Protection: Your personal details will be kept confidential at all times and in accordance with the Data Protection Act 1998. The Promoter will only use the personal details supplied for the administration of the prize draw and for no other purpose, unless we have your consent. The Promoter is bound by the Data Protection Acts 1998.</p>
                      
                    <p>26.	PROMOTER: KAO (UK) LIMITED, 130 Shaftesbury Ave, London W1D 5EU</p>
                      
                </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
