﻿<%@ Page Title="About Cookies - Bioré" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Biore2012.cookies.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="This website uses cookies, to find out how to enable them for popular browsers - including Chrome, Internet Explorer and Firefox - then read this page." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent" style="min-height: 730px">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <br />
                <br />
                <h1>How to Enable Cookies</h1>
                <br />
                <p>
                    <b>To enable cookies</b><br />
                    If you're not sure of the type and version of web browser you use to access the Internet:
                    <br />
                    <br />
                    <b>For PCs:</b> click on 'Help' at the top of your browser window and select the 'About' option<br />
                    <b>For Macs:</b> with the browser window open, click on the Apple menu and select the 'About' option
                    <br />
                    <br />
                    <b>Google Chrome</b><br />
                    1. Click on 'Tools' at the top of your browser window and select Options<br />
                    2. Click the 'Under the Hood' tab, locate the 'Privacy' section, and select the 'Content settings' button<br />
                    3. Now select 'Allow local data to be set'
                    <br />
                    <br />
                    <b>Microsoft Internet Explorer 6.0, 7.0, 8.0, 9.0</b><br />
                    1. Click on 'Tools' at the top of your browser window and select 'Internet options' , then click on the 'Privacy' tab<br />
                    2. Ensure that your Privacy level is set to Medium or below, which will enable cookies in your browser<br />
                    3. Settings above Medium will disable cookies
                    <br />
                    <br />
                    <b>Mozilla Firefox</b><br />
                    1. Click on 'Tools' at the top of your browser window and select Options<br />
                    2. Then select the Privacy icon<br />
                    3. Click on Cookies, then select 'allow sites to set cookies'
                    <br />
                    <br />
                    <b>Safari</b><br />
                    1. Click Click on the Cog icon at the top of your browser window and select the 'Preferences' option<br />
                    2. Click on 'Security', check the option that says 'Block third-party and advertising cookies'<br />
                    3. Click 'Save'
                    <br />
                    <br />
                    <b>How to check cookies are enabled for Macs</b><br />
                    <br />
                    <b>Microsoft Internet Explorer 5.0 on OSX</b><br />
                    1. Click on 'Explorer' at the top of your browser window and select 'Preferences' options<br />
                    2. Scroll down until you see 'Cookies' under Receiving Files<br />
                    3. Select the 'Never Ask' option
                    <br />
                    <br />
                    <b>Safari on OSX</b><br />
                    1. Click on 'Safari' at the top of your browser window and select the 'Preferences' op<br />
                    2. Click on 'Security' then 'Accept cookies'<br />
                    3. Select the 'Only from site you navigate to'
                    <br />
                    <br />
                    <b>Mozilla and Netscape on OSX</b><br />
                    1. Click on 'Mozilla' or 'Netscape' at the top of your browser window and select the 'Preferences' option<br />
                    2. Scroll down until you see cookies under 'Privacy & Security'<br />
                    3. Select 'Enable cookies for the originating web site only'
                    <br />
                    <br />
                    <b>Opera</b><br />
                    1. Click on 'Menu' at the top of your browser window and select 'Settings'<br />
                    2. Then select 'Preferences', select the 'Advanced' tab<br />
                    3. Then select 'Accept cookies' option
                    <br />
                    <br />
                    <b>All other browsers</b><br />
                    Please consult your documentation or online help files.
                </p>
                <br /><br />
            </div>
        </div>
    </div>
</asp:Content>
