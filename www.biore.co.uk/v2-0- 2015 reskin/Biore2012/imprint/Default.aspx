﻿<%@ Page Title="Imprint - Bioré" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Biore2012.imprint.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="By using this website you understand and agree the to the information that is presented on this page." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        #mainContent {
            min-height: 750px;

        }

        #mainContent .centeringDiv {
            margin-top: 3em;
        }

            #mainContent .centeringDiv p {
                margin: 1em 0;
            }

            #mainContent .centeringDiv div {
                margin: 2em 0;
            }

            #mainContent .centeringDiv {
                font-size: 16px;
            }

    </style>
    <div id="main">
        <div id="mainContent" style="min-height: 730px">
            <div id="shadow"></div>
            <div class="centeringDiv">
       
                 <h1 style="text-align:center">Imprint</h1>
                   <div id="responseRule"></div>
             
                <br />
                <h2>KAO UK LTD</h2>

                <div>
                    <p>
                        <b>ADDRESS:</b><br />
                        21 Holborn Viaduct,<br />
                        London, EC1A 2DY<br />
                        
                    </p>
                </div>

                <div>
                    <p>
                        <b>TELEPHONE / FAX:</b><br />
                        Phone: 0800 1070 853.<br />
                        Fax: 020 7851 9888.

                    </p>
                </div>

                <div>
                    <p>
                        <b>E-MAIL:</b><br />
                        customer.service@kao.com
                    </p>
                </div>

                <div>
                    <p>
                        <b>MANAGING DIRECTOR:</b><br />
                        Debbie Rix<br />
                        Company Number<br />
                        4471658<br />
                        VAT NUMBER<br />
                        798 9240 59

                    </p>
                </div>

               


                <br />
                <br />
            </div>
        </div>
    </div>
</asp:Content>
