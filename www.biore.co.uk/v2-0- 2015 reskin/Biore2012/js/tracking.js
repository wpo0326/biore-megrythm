﻿/*------------------------------------------*/
/* GA Event Tracking                        */
/* Mike Taylor - CG Marketing Communications*/
/*-------------------- ---------------------*/
var tpj = jQuery;
//tpj.noConflict();

tpj(document).ready(function() {
    Tagging.init();
});

tpj(function () {
    var urlPath = window.location.pathname;
    var urlPathArray = urlPath.split('/');
    var urlPathCount = urlPathArray.length - 1;
    var urlPageName = urlPathArray[urlPathCount];
    if (urlPageName == "")
        urlPageName = urlPathArray[(urlPathCount - 1)]; //if no filename found in ending path, move 1 step back in URL tree.

    var section = "";
    var subsection = "";
    var event = "";

    //Tagging for product on Product Page 
    tpj(".productListSpacer a").click(function () {
        var productName = $(this).attr("id");
        productName = productName.replace("details-", "");
        productName = productName.replace(/-/g, " ");
        Tagging.tracking("Our Products", "Click", productName);
    });

    tpj(".popup-contents a").click(function () {
        Floodlight.flood(this);
    });

    // click event for accordian on Product Detail
    tpj(".contentHolder h3").click(function () {

        str = tpj(this).attr("id");

        section = urlPageName;
        subsection = str;
        event = "Click";

        //trackEvent(section, subsection, event);
        Tagging.tracking("", event, section)
    });


   /* tpj('A').click(function () {

        if (tpj(this).attr("id")) {
            var str = tpj(this).attr("id");
            str = str.replace("ctl00_ContentPlaceHolder1_", "");

            section = urlPageName;
            subsection = str;
            event = "click";

            trackEvent(section, subsection, event);
        }

    });*/

    // click event for accordian on Product Detail
    tpj(".contentHolder h3").click(function () {

        str = tpj(this).attr("id");

        section = urlPageName;
        subsection = str;
        event = "click";

        //trackEvent(section, subsection, event);
        Tagging.tracking("", event, section)
    });

    //click event for homepage theater circle nav
    tpj(".flex-control-nav li a").click(function () {

        var theaterSlides = [];
        var theaterCount = 0;

        tpj(".slides li a").each(function () { theaterSlides.push(tpj(this).attr("id")); theaterCount++; });

        for (i = 0; i <= theaterCount; i++) {
            if ((parseInt(tpj(this).text()) - 1) == i) {
                section = urlPageName;
                subsection = "circle-navigate-" + theaterSlides[i].replace("ctl00_ContentPlaceHolder1_", "");
                event = "click";
                //trackEvent(section, subsection, event);
                Tagging.tracking(section, event, subsection);

            }

        }

    });

    //click event for buy now pop-up in product detail pages
    tpj("#productRetailerContainer a").click(function () {

        str = tpj(this).attr("title") + "-link";
        if (tpj(this).attr("id").indexOf("Logo") >= 0)
            str = tpj(this).attr("title") + "-logo";

        section = urlPageName;
        subsection = str;
        event = "click";

        trackEvent(section, subsection, event);

    });

    //look for a "GATrack" class.  
    //e.g. "GATrack-IdentifierCategory-IdentifierSubCategory"
    tpj('A[class*="GATrack"]').click(function () {

        var trackClass = tpj(this).attr('class');
        var ary = trackClass.split("-");

        section = "";
        subsection = "";
        event = "";
        if (ary[0] != undefined)
            section = ary[0];
        if (ary[1] != undefined)
            subsection = ary[1];
        if (ary[2] != undefined)
            event = ary[2];

        //trackEvent(section, subsection, event);

    });

});

function trackPage(pageName){
	//alert("Tracking: " + pageName);
    //pageTracker._trackPageview(pageName);
    //_gaq.push(['_trackPageview', pageName]);
    firstTracker._trackPageview(pageName);
    secondTracker._trackPageview(pageName);
}

function trackEvent(section, subsection, event){
	if(section == undefined)
		section = "";
	if(subsection == undefined)
		subsection = "";
	if(event == undefined)
		event = "";
	//alert("Event: " + section + " : " + subsection + " : " + event);
	//pageTracker._trackEvent(section, subsection, event);
//_gaq.push(['_trackEvent', section, subsection, event]);
    firstTracker._trackEvent(section, subsection, event);
    secondTracker._trackEvent(section, subsection, event);
}

function trackSpotlight(url){
	// for doublelick-related tags
	//alert("Spotlight: " + url);

	var axel = Math.random()+"";
	var a = axel * 10000000000000;
	
	var img = document.createElement('img');
	img.src = url + ';num=' + a;	
}

var Tagging = {
    tracking: function(section, eventType, name) {
        //Fire first tracking
        ga('send', 'event', section, eventType, name, '1', { 'nonInteraction': 1 });

        //Fire second tracking (accessible by name)
        ga('KAOGlobal.send', 'event', section, eventType, name, '1', { 'nonInteraction': 1 });


    },
    linkTrack: function() {

        var anchor = this;

        try {
            tpj("body").on("click", ".track", function(e) {
                e.preventDefault();

                //Section | eventType | name
                var dataTracking = tpj(this).attr("data-tracking");
                var uri = tpj(this).attr("href");
                var target = tpj(this).attr("target");
                var attributes = dataTracking.split("|");
                var element = tpj(this);

                anchor.tracking(attributes[0], attributes[1], attributes[2]);

                var isRedirect = tpj(this).hasClass('redirect');
                setTimeout(function() {
                    if (uri && uri != "#" && isRedirect == false) {
                        if (target) {
                            window.open(uri, "_blank");
                        } else if (!anchor.prevent(element)) {
                            window.location = uri;
                        }
                    } else {


                    }
                }, 500);

            });
        }
        catch(err) {
            //alert(err.message);
        }
    },
    prevent: function(element) {
        var prevent = false;
        if (element.hasClass("prevent")) {
            prevent = true;
        }
        return prevent;
    },
    init: function() {
        this.linkTrack();
    }
}