﻿<%@ Page Title="Privacy - Bioré" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.privacy._default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="At KAO, we respect your privacy and take reasonable measures to protect your privacy." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>

    <style type="text/css">
        #main a {
            text-decoration: underline;
        }

        #main p {
            padding: 1em 0 0 0;
        }

   
        #main ul {
            margin: auto auto auto 2em;
        }
        #main ul li {
            list-style: square;
        }

          #main ul li p {
                margin-left: 0;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent" style="min-height: 730px">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <br />
                <br />
                <h1>Privacy Policy</h1>

           
<p>
    <strong>Last revised August 9<sup>th</sup>, 2018</strong>
</p>

            
<p>Koa (UK) Ltd, 130 Shaftesbury Avenue, London, W1D 5EU ("<strong>Kao Company</strong>" or "<strong>we</strong>" or "&nbsp;<strong>our</strong>") and each of its affiliates and subsidiaries in the EMEA region collectively, the "<strong>Kao Group</strong>") takes data privacy seriously. This Privacy Policy informs the users of&nbsp;<a href="http://www.biore.co.uk/biore-skincare">http://www.biore.co.uk/biore-skincare</a> &nbsp;and any other Kao Company-owned websites or mobile applications on which this Privacy Policy is displayed ("<strong>Website</strong>") how we, as controller within the meaning of the General Data Protection Regulation ("<strong>GDPR</strong>") collect and process the personal data and other information of such users in connection with their usage of the Website.</p>
<p>Note that other Kao Group websites or mobile apps may be governed by other privacy policies.</p>
<p>&nbsp;</p>
<ol>
<li><strong>Categories of Personal Data and Processing Purposes -&nbsp;</strong><em><strong>What personal data do we process about you and why?</strong></em></li>
</ol>
<p><strong>1.1 Metadata</strong></p>
<p>You may use the Website without providing any personal data about you. In this case, we will collect only the following metadata that result from your usage of the Website: browser type and version, operating system and interface, website from which you are visiting us (referrer URL), webpage(s) you are visiting on our Website, date and time of accessing our Website, and internet protocol (IP) address.</p>
<p>Your IP address will be used to enable your access to our Website. The metadata, including the shortened IP address, will be used to improve the quality and services of our Website and services by analysing the usage behaviour of our users.</p>
<p><strong>1.2 Competitions</strong></p>
<p>If you participate in a competition, we collect and process the following personal data about you: name, email address and postal address (if you are selected as a winner). We process such personal data for purposes of carrying out the competition, informing the winner, delivering the prize to the winner, carrying out the event, and providing you with marketing materials where you have provided us consent to do so, to the extent permitted by applicable law, and analysing your interests for marketing purposes.</p>
<p><strong>1.3 Contact Us</strong></p>
<p>On our website, we offer you the opportunity to contact us via a contact form. For this we need the following personal data from you: email name, telephone number (optional), address (optional), date of birth (optional) and gender (optional). The personal data that you provide us in the context of this contact request will only be used to answer your inquiry and for the technical administration thereof. The transfer to third parties does not take place. Your personal data will be deleted as soon as we have processed your request or you revoke the consent you have given.</p>
<p>1.4 Third Party Services</p>
<p>On our website we use the rating tool of the third party service provider Bazaarvoice. This tool is used to allow our customers to provide feedback on our goods and services. For this tool, the privacy policy of this services provider applies, and is available at <a href="https://www.bazaarvoice.com/legal/privacy-policy/">https://www.bazaarvoice.com/legal/privacy-policy/</a></p>
<p>&nbsp;</p>
<p><strong>2. Processing Basis and Consequences -&nbsp;</strong><em><strong>What is the legal justification for processing your personal data and what happens if you choose not to provide it?</strong></em></p>
<p>We rely on the following legal grounds for the collection, processing, and use of your personal data:</p>
<ul>
<li>your consent to the processing of your data for one or more specific purposes (as detailed in Section 1) ; or if</li>
</ul>
<ul>
<li>we have a legitimate interest in doing so (including but not limited to) a legitimate interest in performing marketing activities, research activities, data analytics, internal administration functions, processing and enforcing legal claims and conducting our business in compliance with all applicable laws, relevant industry standards and our policies).</li>
</ul>
<p>&nbsp;</p>
<p>The provision of your personal data is not required by a statutory or contractual obligation. The provision of your personal data is not necessary to enter into a contract with us or to receive our services/products as requested by you. The provision of your personal data is voluntary for you.</p>
<p>Not providing your personal data may result in disadvantages for you, for example, you may not be able to receive certain products and services. However, unless otherwise specified, not providing your personal data will not result in legal consequences for you.</p>
<p>&nbsp;</p>
<p><strong> 3. Categories of Recipients and International Transfers -&nbsp;</strong><em><strong>Who do we transfer your personal data to and where are they located?</strong></em></p>
<p>We may transfer your personal data to third parties for the processing purposes described above as follows:</p>
<ul>
<li><strong>Within the Kao Company:&nbsp;</strong>Our parent entity, the Kao Corporation, in Japan and each of its affiliates and subsidiaries (each affiliate or subsidiary including us referred to as "<strong>Kao Company</strong>"; collectively, the "<strong>Kao Group</strong>") within the global&nbsp;<a href="http://www.kao.com/global/en/about/outline/group-companies/">Kao Group</a> may receive your personal data as necessary for the processing purposes described above. Depending on the categories of personal data and the purposes for which the personal data has been collected, different internal departments within the Kao Company may receive your personal data. For example, our IT department may have access to your account data, and our eCommerce and sales departments may have access to your account data or data relating to product orders. Moreover, other departments within the Kao Company may have access to certain personal data about you on a need to know basis, such as the legal department, the finance department or internal auditing.</li>
<li><strong>With data processors:</strong>Certain third parties, whether affiliated or unaffiliated, may receive your personal data to process such data under appropriate instructions ("<strong>Processors</strong>") as necessary for the processing purposes described above, such as website service providers, order fulfilment providers, customer care providers, marketing service providers, IT support service providers, and other service providers who support us in maintaining our commercial relationship with you. The Processors will be subject to contractual obligations to implement appropriate technical and organizational security measures to safeguard the personal data, and to process the personal data only as instructed. See section 1.4 for more details.</li>
<li><strong>Other recipients:</strong>We may transfer - in compliance with applicable data protection law - personal data to law enforcement agencies, governmental authorities, judicial authorities, legal counsel, external consultants, or business partners. In case of a corporate merger or acquisition, personal data may be transferred to the third parties involved in the merger or acquisition. We will not disclose your personal data to third parties for advertising or marketing purposes or for any other purposes without your permission.</li>
</ul>
<p>Any access to your personal data is restricted to those individuals that have a need-to-know in order to fulfill their job responsibilities.</p>
<p><strong>International transfers. </strong></p>
<p>The personal data that we collect or receive about you may be transferred to and processed by recipients that are located inside or outside the European Economic Area ("<strong>EEA</strong>").</p>
<ul>
<li>For recipients located outside of the EEA, some are certified under the EU-U.S. Privacy Shield and others are located in countries with adequacy decisions (in particular, <em>[Andorra, Argentina, Canada (for non-public organizations subject to the Canadian Personal Information Protection and Electronic Documents Act), Switzerland, Faeroe Islands, Guernsey, Israel, Isle of Man, Jersey, and New Zealand]</em>), and, in each case, the transfer is thereby recognized as providing an adequate level of data protection from a European data protection law perspective.</li>
<li>Other recipients might be located in countries which do not adduce an adequate level of protection from a European data protection law perspective. We will take all necessary measures to ensure that transfers out of the EEA are adequately protected as required by applicable data protection law.</li>
<li>With respect to transfers to countries not providing an adequate level of data protection, we will base the transfer on appropriate safeguards, such as standard data protection clauses adopted by the European Commission or by a supervisory authority, approved codes of conduct together with binding and enforceable commitments of the recipient, or approved certification mechanisms together with binding and enforceable commitments of the recipient.</li>
</ul>
<p>You can ask for a copy of such appropriate safeguards by contacting us as set out in Section 7 below</p>
<p>&nbsp;</p>
<p><strong> 4. Retention Period -&nbsp;</strong><em><strong>How long do we keep your personal data?</strong></em></p>
<p>Your personal data will be retained as long as necessary to provide you with the services and products requested. Once you have terminated the contractual relationship with us or the competition has been completed or otherwise ended your relationship with us, we will remove your personal data from our systems and records and/or take steps to properly anonymize it so that you can no longer be identified from it (unless we need to keep your information to comply with legal or regulatory obligations to which the Kao Company is subject--e.g., taxation purposes).</p>
<p>We may retain your contact details and interests in our products or services for a longer period of time if the Kao Company is allowed to send you marketing materials. Also, we may be required by applicable law to retain certain of your personal data for a period of 10 years after the relevant taxation year. We may also retain your personal data after the termination of the contractual relationship if your personal data are necessary to comply with other applicable laws or if we need your personal data to establish, exercise or defend a legal claim, on a need to know basis only. To the extent possible, we will restrict the processing of your personal data for such limited purposes after the termination of the contractual relationship.</p>
<p><strong> 5. Your Rights -&nbsp;</strong><em><strong>What rights do you have and how can you assert your rights?</strong></em></p>
<p><strong>Right to withdraw your consent:</strong>&nbsp;If you have declared your consent regarding certain collecting, processing and use of your personal data (in particular, regarding the receipt of direct marketing communication via email, telephone/SMS and postal), you can withdraw this consent at any time with immediate effect. Such a withdrawal will not affect the lawfulness of the processing prior to the consent withdrawal. Please contact us as stated in Section 7 below to withdraw your consent. Further, you can object to the use of your personal data for the purposes of marketing without incurring any costs other than the transmission costs in accordance with the basic tariffs.</p>
<p><strong>Additional data privacy rights:</strong>&nbsp;Pursuant to applicable data protection law, you may have the right to: (i) request access to your personal data; (ii) request rectification of your personal data; (iii) request erasure of your personal data; (iv) request restriction of processing of your personal data; (v) request data portability; and/or (vi) object to the processing of your personal data (including objection to profiling).</p>
<p>Please note that these aforementioned rights might be limited under the applicable local data protection law. Below please find further information on your rights to the extent that the GDPR applies:</p>
<ul>
<li><strong>Right to request access to your personal data:&nbsp;</strong>You may have the right to obtain from us confirmation as to whether or not personal data concerning you is being processed, and, where that is the case, to request access to the personal data. This access information includes &ndash; inter alia &ndash; the purposes of the processing, the categories of personal data concerned, and the recipients or categories of recipient to whom the personal data have been or will be disclosed. However, this is not an absolute right and the interests of other individuals may restrict your right of access.</li>
</ul>
<p>You may have the right to obtain a copy of the personal data undergoing processing free of charge. For further copies requested by you, we may charge a reasonable fee based on administrative costs.</p>
<ul>
<li><strong>Right to request rectification:&nbsp;</strong>You may have the right to obtain from us the rectification of inaccurate personal data concerning you. Depending on the purposes of the processing, you may have the right to have incomplete personal data completed, including by means of providing a supplementary statement.</li>
<li><strong>Right to request erasure (right to be forgotten):&nbsp;</strong>Under certain circumstances, you may have the right to obtain from us the erasure of personal data concerning you and we may be obliged to erase such personal data.</li>
<li><strong>Right to request restriction of processing:&nbsp;</strong>Under certain circumstances, you may have the right to obtain from us restriction of processing your personal data. In such case, the respective data will be marked and may only be processed by us for certain purposes.</li>
<li><strong>Right to request data portability:&nbsp;</strong>Under certain circumstances, you may have the right to receive the personal data concerning you, which you have provided to us, in a structured, commonly used and machine-readable format and you may have the right to transmit those data to another entity without hindrance from us.</li>
<li><strong>Right to object:&nbsp;</strong></li>
</ul>
<p>&nbsp;</p>
<table>
<tbody>
<tr>
<td style="width: 638px; max-width: 638px;background-color: #CCC;padding: .5em">
<p>Under certain circumstances, you may have the right to object, on grounds relating to your particular situation, at any time to the processing of your personal data by us and we can be required to no longer process your personal data. Such right to object may especially apply if we collect and process your personal data for profiling purposes in order to better understand your interests in our products and services or for direct marketing. If you have a right to object and you exercise this right, your personal data will no longer be processed for such purposes by us. You may exercise this right by contacting us as stated in Section 7 below. Such a right to object may, in particular, not exist if the processing of your personal data is necessary to take steps prior to entering into a contract or to perform a contract already concluded. If you no longer want to receive direct marketing via email, telephone/SMS, and postal, you need to withdraw your consent as explained at the start of Section 5.</p>
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>

<p>To exercise your rights, please contact us as stated under Section 7 below. You also have the right to lodge a complaint with the competent data protection supervisory authority.</p>

<p><strong> 6. Cookies and other tracking technologies</strong></p>
<p><strong>What is a cookie?</strong></p>
<p>A cookie is a small text file a web portal installs on your computer, tablet or smartphone when you visit the portal. Cookies can help us in many ways, they make it for example possible for the portal to remember certain entries and settings (e.g. login info, language, font size and other display preferences) for a specific period of time so you do not have to re-enter this information every time you visit and navigate the portal., for example, by allowing us to tailor a Web site to better match your interests or to store your password to save you having to re-enter it each time. Furthermore, when you visit our website you may notice some cookies that aren't related to us. If you go on to a web page that contains embedded content, you may be sent cookies from these websites. We don't control the setting of these cookies, so we suggest you check the third-party websites for more information about their cookies and how to manage them. If you do not wish to receive cookies, please configure your Internet browser to erase all cookies from your computer&rsquo;s hard drive, block all cookies or to receive a warning before a cookie is stored.</p>
<p>&nbsp;<strong>How we use cookies and for which purpose:</strong></p>
<p>Here's a list of the cookies we use and what we use them for.</p>
<p><strong>Cookies hosted by Kao</strong></p>
<p>These cookies are used to protect the visitor&rsquo;s session state across page requests: <strong>Cookie</strong> <strong>Acceptance, JSESSIONID and ASP.NET_Sessionid</strong></p>
<p><strong>Cookies hosted by Third parties</strong></p>
<p>When you visit our website, you may notice some cookies that aren't related to Biore. If you go on to a web page that contains embedded content, you may be sent cookies from these websites. We don't control the setting of these cookies, so we suggest you check the third-party websites for more information about their cookies and how to manage them.</p>
<p><strong>Google</strong></p>
<p>Google uses Cookies to distinguish users and events on the website. Google Analytics tracks unique visitors. Stored in this cookie is a unique visitor ID, the date and time of their first visit, the time their current visit started and the total number of visits they have made. Google uses Cookies to decide whether a visit has timed out and also how deep a visit has gotten.&nbsp; The Data is used to analyze the user interaction on the website. The tracked data is anonymous. It also uses the data to measure conversions and helps us to improve our website.</p>
<p><strong>Opt-Out Options for Google Analytics</strong></p>
<p>You can opt-out of Google Analytics for Display Advertising and customize Google Display Network ads by using the Ads Settings, or by using the Google Analytics opt-out Browser add-on. You can find more information about Google Analytics at: <a href="https://tools.google.com/dlpage/gaoptout">tools.google.com/dlpage/gaoptout</a>.</p>
<p>You may stop or restrict the placement of cookies on your computer or flush them from your browser by adjusting your web browser preferences and browser plug-in settings, in which case you may still use our web site, but it may interfere with some of its functionality.</p>
<p>As an alternative to the browser add-on and especially for mobile browsers, please click on the following link to set an opt-out cookie. This opt-out cookie prevents detection by Google Analytics within this website. <a href="http://www.biore.co.uk/privacy/?google-analytics-opt-out=true">http://www.biore.co.uk/privacy/?google-analytics-opt-out=true</a></p>
<p><strong>Bazaarvoice</strong></p>
<p>We work with Bazaarvoice to provide a system for customers to review our products online. Bazaarvoice uses cookies to collect information from consumers such as the IP address of your computer, the pages you visit within the Bazaarvoice Network, the date and time of your visit, browser type and version and other click-stream data (collectively "Analytics and Tracking Data"). Bazaarvoice use this Analytics and Tracking Data to operate the Services, enable the submission of User Generated Content, to enable anonymous analytics and measurement reporting, and to display online advertisements to you and other users.</p>
<p><strong>Instagram</strong></p>
<p>These cookies are used by Instagram as soon as the Instagram plugin has been activated on our website. For more information on the type, use and purpose of these cookies, please click the following link: <a href="https://www.instagram.com/about/legal/privacy/">https://www.instagram.com/about/legal/privacy/</a></p>
<p><strong>Pinterest</strong></p>
<p>These cookies are used by Pinterest as soon as the Pinterest plugin has been activated on our website. For more information on the type, use and purpose of these cookies, please click the following link: <a href="https://about.pinterest.com/en/privacy-policy">https://about.pinterest.com/en/privacy-policy</a></p>
<p><strong>Youtube</strong></p>
<p>These cookies are used by YouTube and/or Google as soon as a video has been integrated on a site and/or has been played. For more information on the type, use and purpose of the cookies, please click on the following link: <a href="https://www.google.com/intl/en/policies/privacy/%20">https://www.google.com/intl/en/policies/privacy/</a></p>
<p><strong>Facebook</strong></p>
<p>These cookies are used by Facebook as soon as the Facebook plugin has been activated on our website. For more information on the type, use and purpose of these cookies, please click the following link: <a href="https://www.facebook.com/policies/cookies/">https://www.facebook.com/policies/cookies/</a></p>
<p><strong>Twitter</strong></p>
<p>These cookies are used by Twitter as soon as the Twitter plugin has been activated on our website. For more information on the type, use and purpose of these cookies, please click the following link: <a href="https://www.twitter.com/privacy">https://www.twitter.com/privacy</a></p>
<p><strong>Controlling cookies</strong></p>
<p>You can randomly manage and/or delete these cookies. You can delete all of the cookies stored on your computer and you can set up most browsers in such a manner that the archiving of cookies is prevented in the first place.</p>
<p>However, if you do this, you may have to manually adjust some settings every time you visit and live with the impairment of some of the functions.</p>
<p><strong> 7. Questions and Contact Information</strong></p>
<p>For further information and to exercise your statutory rights according to section 5, please go to <a href="https://www.kao.com/global/en/EU-Data-Subject-Request/">&nbsp;https://www.kao.com/global/en/EU-Data-Subject-Request/</a>. </p>
<p><strong> 8. Changes to this Privacy Policy</strong></p>
<p>We may update this Privacy Policy from time to time in response to changing legal, regulatory or operational requirements. We will notify you of any such changes, including when they will take effect, by updating the "Last revised" date above or as otherwise required by applicable law. Your continued use of our Website after any such updates take effect will constitute acceptance of those changes. If you do not accept updates to this Privacy Policy, you should stop using our Website.</p>
    
<br /><br />




 
            </div>
        </div>
    </div>
</asp:Content>
