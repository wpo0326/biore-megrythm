﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Biore2012.BLL;
using System.Configuration;
using System.Web.UI.HtmlControls;

namespace Biore2012.FormConfig
{
    public partial class form : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Request.QueryString["show"] != null)
            {
                if (Int32.Parse(Request.QueryString["show"]) == 2)
                {
                    this.MasterPageFile = "~/SiteNoHeader.Master";
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.DataBind();

            // Grab masterpage, set appropriate body classes
            MasterPage myMasterTemp = (MasterPage)this.Master;

            if (Request.QueryString["promo"] != null && Request.QueryString["promo"] == "biore_us_201208_FBPS_Coup50835K")
            {
                Panel panelFbScript = (Panel)myMasterTemp.FindControl("fbScript");
                panelFbScript.Visible = true;

                PlaceHolder phAsync = (PlaceHolder)panelFbScript.FindControl("fbAsyncInitScript");
                phAsync.Visible = true;

                Literal litAppId = (Literal)phAsync.FindControl("asyncAppID");
                litAppId.Text = System.Configuration.ConfigurationManager.AppSettings["CSBCCouponFBAppId"];
			}

			// Author Partnership custom styles
			if (Request.QueryString["promo"] != null && 
				Request.QueryString["promo"] == "biore_us_201210_AthrPrtnPreLaunchSweeps63T2UY2" ||
				Request.QueryString["promo"] == "biore_us_201210_AthrPrtnLaunchDayAGiveaway9KWNM52" ||
				Request.QueryString["promo"] == "biore_us_201210_AthrPrtnLaunchDayBSweeps27SZG93" ||
				Request.QueryString["promo"] == "biore_us_201210_AthrPrtnLaunchDayCFinalSweeps5M2DBX67"
				)
			{
				Panel panelFbPageTabScript = (Panel)myMasterTemp.FindControl("fbPageTabScript");
				panelFbPageTabScript.Visible = true;

				Literal litFBPageAppId = (Literal)panelFbPageTabScript.FindControl("fbPageTabAppID");
				litFBPageAppId.Text = System.Configuration.ConfigurationManager.AppSettings["AuthorPartnershipFBAppId"];

				updateMasterBodyClass(" stpoFBForm " + Request.QueryString["promo"]);
			}

            updateMasterBodyClass(" forms");

            // hide floodlight pixels for all form pages
            myMasterTemp.FindControl("floodlightPixels").Visible = false;

            if (Request.Url.ToString().Contains("email-newsletter-sign-up")) {
                
                // BWR-291 ATAM
                Response.Redirect("~/");

                updateMasterBodyClass(" signUp");
                // show floodlight pixels for sign me up page
                myMasterTemp.FindControl("floodlightPixels").Visible = true;
            }
            else if (Request.Url.ToString().Contains("contact-us")) 
                updateMasterBodyClass(" contactUs");


            // This turns on and off the Question Pro Include for this page...
            ((Panel)Page.Master.FindControl("panel_Question_Pro")).Visible = false;

            if (Request.QueryString["promo"] != null)
            {
                string promoID = Request.QueryString["promo"].ToString().ToLower(); // MAKE SURE TO CHECK LOWERCASE
                bool promoMatch = false;
                if (promoID.Contains("biore_us_201108_walmartcoup")) { promoMatch = true; } // MAKE SURE TO CHECK LOWERCASE
                if (promoID.Contains("biore_us_201108_newlooktargetendcap")) { promoMatch = true; } // MAKE SURE TO CHECK LOWERCASE
                if (promoID.Contains("biore_us_201111_freshfanporespectives")) { promoMatch = true; } // MAKE SURE TO CHECK LOWERCASE
                if (promoID.Contains("biore_us_201203_tripackette_fb_samp")) { promoMatch = true; } // MAKE SURE TO CHECK LOWERCASE
                if (promoID.Contains("biore_us_201203_tripackette_imedia1_samp")) { promoMatch = true; } // MAKE SURE TO CHECK LOWERCASE
                
                if (promoMatch)
                {
                    Response.Redirect("~/404/");
                }
            }
        }

        private void updateMasterBodyClass(string val)
        {
            if ((MasterPage)this.Master is Site){
                Site myMaster = (Site)this.Master;
                myMaster.bodyClass += val;
            }
            else {
                SiteNoHeader myMaster = (SiteNoHeader)this.Master;
                myMaster.bodyClass += val;
            }
        }
    }
}
