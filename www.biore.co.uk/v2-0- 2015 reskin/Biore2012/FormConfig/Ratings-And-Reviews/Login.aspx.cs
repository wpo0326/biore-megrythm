﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Biore2012.BLL;
using System.Web.Security;

namespace Biore2012.Forms.RatingsAndReviews
{
    public partial class Login : System.Web.UI.Page
    {
        private string reqUsername1 = string.Empty;
        private string reqUsername2 = string.Empty;
        private string reqUsername3 = string.Empty;
        private string reqUsername4 = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            initErrorMessages();

            if (Request.QueryString["return"] != null && Request.QueryString["return"] != "")
            {
                literalRegisterButton.Text = "<a class=\"buttonLink\" href=\"register.aspx?return=Default.aspx?" + Server.UrlEncode(Request.QueryString["return"]) + "\">FIRST REVIEW? REGISTER NOW</a>";
                literalForgotPasswordButton.Text = "<a class=\"buttonLink\" href=\"resetPassword.aspx?return=Login.aspx?" + Server.UrlEncode(Request.QueryString["return"]) + "\">Forgot your password?</a>";
            }
            else
            {
                literalRegisterButton.Text = "<a class=\"buttonLink\" href=\"register.aspx\">FIRST REVIEW? REGISTER NOW</a>";
                literalForgotPasswordButton.Text = "<a class=\"buttonLink\" href=\"resetPassword.aspx\">Forgot your password?</a>";
            }
        }

        private void initErrorMessages()
        {
            reqEmail.Text = "<p>Please enter your Email Address.</p>";
            regEmail.Text = "<p>Please enter a valid Email Address.</p>";
            reqPassword.Text = "<p>Please enter your password.</p>";
            

            //Forced Server Side messages
            reqUsername1 = "<p>Username or password are incorrect.</p>";
            reqUsername2 = "<p>Your account has been locked out for too many failed login attempts in a row. Try again in 30 minutes.</p>";
            reqUsername3 = "<p>Username or password are incorrect.</p>";
            reqUsername4 = "<p>Username or password are incorrect.</p>";
        }

        protected void login_Click(object sender, EventArgs e)
        {
      
            if (Page.IsValid)

            {
                AuthUtils au = new AuthUtils();
                MembershipUser u = au.FetchUser(loginUsername.Text);
                if (u != null)
                {
                    if (u.IsLockedOut == false)
                    { // User is not locked out
                        if (System.Web.Security.Membership.ValidateUser(loginUsername.Text, loginPassword.Text))
                        { // User supplied good info!
                            FormsAuthentication.SetAuthCookie(loginUsername.Text, true);

                            if (Request.QueryString["return"] != null && Request.QueryString["return"] != "")
                            {
                               // Response.Redirect(Server.UrlDecode(Request.QueryString["return"]));
                                Response.Redirect((Request.QueryString["return"]));
                            }
                            else
                            {
                                Response.Redirect("Default.aspx");
                            }
                        }
                        else
                        { // User supplied bad info
                            reqUsername.Text = reqUsername1;
                            username.Text = "";  // will trigger error for the reqUsername validator
                            Page.Validate();
                            username.Text = "username";
        
                        }
                    }
                    else
                    { // User is locked out

                        TimeSpan theLockoutDuration = new TimeSpan(0, 30, 0);
                        if (DateTime.Now - u.LastLockoutDate < theLockoutDuration)
                        { // Half hour hasn't passed yet
                            username.Text = "";
                            Page.Validate();
                            username.Text = "username";
                            reqUsername.Text = reqUsername2;
                        }
                        else
                        {
                            //user is due to be unlocked.
                            u.UnlockUser();
                            if (System.Web.Security.Membership.ValidateUser(loginUsername.Text, loginPassword.Text))
                            { // User supplied good info!
                                FormsAuthentication.SetAuthCookie(loginUsername.Text, true);

                                if (Request.QueryString["return"] != null && Request.QueryString["return"] != "")
                                {
                                    Response.Redirect(Request.QueryString["return"]);
                                }
                                else
                                {
                                    Response.Redirect("Default.aspx");
                                }
                            }
                            else
                            { // User supplied bad info
                                username.Text = "";
                                Page.Validate();
                                username.Text = "username";
                                reqUsername.Text = reqUsername3;
                            }
                        }
                    }
                }
                else
                { // User supplied bad info
                    username.Text = "";
                    Page.Validate();
                    username.Text = "username";
                    reqUsername.Text = reqUsername4;
                }
            }
        }
    }
}
