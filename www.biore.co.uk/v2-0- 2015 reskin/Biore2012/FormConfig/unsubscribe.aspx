﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="unsubscribe.aspx.cs" Inherits="Biore2012.FormConfig.unsubscribe" %>
<%@ Register TagPrefix="uc1" TagName="ucFormUnsubscribe" Src="~/Controls/ucFormUnsubscribe.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combined_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv responsive">
                <div id="BrandImageContainer">
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/utilityPages/polaroid.png") %>">	
                </div>
                <%--Include the Usubscribe Control Form--%>
                <uc1:ucFormUnsubscribe id="ucFormUnsubscribe" runat="server"></uc1:ucFormUnsubscribe>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>