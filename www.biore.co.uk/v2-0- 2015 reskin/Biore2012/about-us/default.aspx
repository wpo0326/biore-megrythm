﻿<%@ Page Title="Bioré Skin Care Products to Free Your Pores" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.about_us._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Get healthy-looking, radiant skin with products from Bioré Skincare - we have everything from pore strips to facial pore cleansers to help with blackheads. " name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent" class="aboutBlueBG">
            <div class="centeringDiv">
                <div id="content" class="aboutBlueTxt">
                    <div class="titleh1">
                        <h1>DISCOVER Bior&eacute;<sup>&reg;</sup></h1>
                        <div id="responseRule"></div>
                    </div>
                    <img src="../../images/about/1brandaboutusUK.png" /></p>
                    <div class="about-info">     
                        
                        <p><span>Discover Bioré – the expert in clean pores! </span></p>

                        <p>Did you know that the Bioré brand’s parent company is Kao Corporation (pronounced like ‘cow’) and is headquartered in Tokyo, Japan?</p>
<p>The story behind Kao begins with a small face-care company in Tokyo founded by Mr. Tomiro Nagase in 1887. Tomiro was on a mission to make affordable, high-quality, facial soap—which he called “Kao” – pronounced the same way as the Japanese word for “face” (makes sense, right!?).</p>

<p>Today, Bioré is a pore focused--or more like a pore obsessed-- face care brand in UK that continues the mission to make high quality, affordable skincare products that provide a variety of skincare benefits. Even though we create our own line of products in the North America and Europe, we approach all of our product development with a Japanese Beauty Philosophy and are able to leverage our parent company’s amazing Japanese technology. These technologies are backed by years of research and development (hello skincare jackpot!).</p>

<p>Our Japanese beauty roots inspire us to improve and innovate in everything we do—innovation is in our DNA. Bottom line, our products perform. With technology rooted in science, we take a no-nonsense approach to fight smarter, not harder--while having a little bit of fun along the way! Our products focus on those 20,000 pesky pores, providing deep cleansing products for the skin of your dreams.</p>

                        <p><span>Bioré – Free Your Pores</span></p>
                                           
                       
                    </div>
                    <img src="../images/about/aboutBG.png" alt="" id="Img1" />                   
                </div>
                        <p>*Intage SRI, Facial Wash Market, Jun 2017 ～ May 2018, Accumulated total sales value of Biore Facial Cleanser Series</p> 
            </div>
        </div>
    </div>
</asp:Content>

