function getCookie(c_name) {
            if (document.cookie.length > 0) {
                c_start = document.cookie.indexOf(c_name + "=");
                if (c_start != -1) {
                    c_start = c_start + c_name.length + 1;
                    c_end = document.cookie.indexOf(";", c_start);
                    if (c_end == -1) c_end = document.cookie.length;
                    return unescape(document.cookie.substring(c_start, c_end));
                }
            }
            return "";
        }
        function getQuerystring(key, default_) {
            if (default_ == null) default_ = "";
            key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
            var qs = regex.exec(window.location.href);
            if (qs == null)
                return default_;
            else
                return qs[1];
        }

        var showPostSurvey = false;
        var wt_cookie_id_val = '';
        var tmp_wt_cookie_val = getCookie('WT_FPC');
        var tmp_wt_cookie_val_split_val = tmp_wt_cookie_val.split(':');
        for (var i = 0; i < tmp_wt_cookie_val_split_val.length; i++) {
            tmpnamevalpair = tmp_wt_cookie_val_split_val[i];
            if (tmpnamevalpair.substring(0, 3) == 'id=') {
                wt_cookie_id_val = tmpnamevalpair.substring(3, tmpnamevalpair.length);
                tmplastnum = wt_cookie_id_val.substring(wt_cookie_id_val.lastIndexOf('.') - 1, wt_cookie_id_val.lastIndexOf('.'));
                // half should get pre-survey invitation and the other have get the post-survey invitation
                if (tmplastnum != Number.NaN && tmplastnum < 5) showPostSurvey = true;
            }
        }

        var config = new Object();

        if (showPostSurvey) {
            config.surveyID = XXXXXXXX; //(Biore-POST) 
            config.popupMessage = '<br />Please click start survey, then minimize window. Once you are through with your visit to the Bior&eacute;&reg; site please complete the survey.';
        } else {
            config.surveyID = XXXXXXXX; //(Biore-PRE)
            config.popupMessage = '<br />Please take the survey <strong>BEFORE</strong> you visit the site';
        }
        config.sCustom = '&custom1=' + wt_cookie_id_val + '&custom2=' + getQuerystring('enl_medium', '') + '&custom3=' + getQuerystring('enl_source', '');
        config.popupInvitationHeader = 'Bior&eacute;&reg; Survey';

        config.animationMode = 2;
        config.takeSurveyURL = 'http://www.questionpro.com/akira/TakeSurvey';
        config.windowPositionLeft = 36;
        config.windowPositionTop = 45;
        config.home = 'http://www.questionpro.com/';
        config.isRightToLeft = false;
        config.surveyStartMessage = 'Start Survey';
        config.popupInvitationLaterMessage = 'Later';
        config.showFooter = false;
        config.invitationDelay = 0;
        config.skipCount = 0;
        config.popupMode = 2;