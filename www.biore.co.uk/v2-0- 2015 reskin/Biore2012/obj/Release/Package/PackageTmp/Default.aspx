﻿<%@ Page Title="Get a Clean Face | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Get healthy, radiant skin with products from Bioré® Skincare" name="description" />
    <meta content="skincare, clean face" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/homepage.css")
        .Add("~/css/flexslider.css")
        .Render("~/CSS/combinedhome_#.css")
    %>
    <meta name="viewport" content="width=device-width">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">
<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: Biore Skincare
URL of the webpage where the tag is expected to be placed: http://www.biore.com/en-US/biore-skincare
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 05/31/2013
-->
<script type="text/javascript">
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    document.write('<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore932;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore932;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div class="flexslider"  id="theater" >
            <ul class="slides">
              <li id="theaterItem1" class="theaterHolder">
                <div class="fma1Product"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/fmas/fma1Product.png") %>" border="0" alt="" /></div>
                <div class="fma1headline">
                    <span class="fma1Intro">Meet <span class="fma1Char">Charcoal</span></span><br />
                    <span class="fma1New">The New Secret Weapon</span><br />
                    <span class="fma1From">From The Bior&eacute;&reg; Skincare Experts</span><br />
                    <div class="fma1FindWrapper">
                        <div class="fma1Find">
                            <a href="<%= VirtualPathUtility.ToAbsolute("~/biore-facial-cleansing-products/charcoal.aspx") %>">Find Out More</a>
                        </div>
                    </div>
                    </div>
               </li>
                
               <!-- <li id="theaterItem2" class="theaterHolder">        
                   
                               <div class="fma2Headline">Start Earning Your</div><br />

                                   <div class="fma2HeadlineTwo">Way to <span class="fma2Great">Great Rewards!</span></div>

                              <div class="fma2Bubbles"><div class="fma2Prove"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/fmas/fma2Prove.png") %>" border="0" alt="" /></div></div>
                                <div class="fma2HowWrap">
                                  <span id="fma2How">How?</span>
                                  <span id="fma2List">
                                      <ul>
                                          <li>&#8226; Earn <b>Prove It!</b> Reward Points.</li>
                                          <span class="fmaListOneLine">
                                              <li class="fmaListOneLineSpacer">&#8226; Redeem.</li>
                                              <li>&#8226; Celebrate.</li>
                                          </span>
                                      </ul>
                                  </span>
                              </div>

                             
                             <a href="https://apps.facebook.com/proveitrewards/"><div class="fma2Face">Start Earning On Facebook</div></a>
                               
                 </li> -->
            </ul>
            <div class="centeringDiv" id="pagerWrapper"><div id="pager"></div></div>
        </div>
        
    </div>
  
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/jquery.flexslider.min.js")
        .Render("~/js/combinedhome_#.js")
    %>
</asp:Content>