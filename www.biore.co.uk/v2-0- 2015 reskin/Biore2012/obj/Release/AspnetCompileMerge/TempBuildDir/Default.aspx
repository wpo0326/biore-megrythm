﻿<%@ Page Title="Get a Clean Face | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Get healthy, radiant skin with products from Bioré® Skincare" name="description" />
    <meta content="skincare, clean face" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/homepage.css")
        .Add("~/css/flexslider.css")  
        .Render("~/CSS/combinedhome_#.css")
    %>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">
<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: Biore Skincare
URL of the webpage where the tag is expected to be placed: http://www.biore.com/en-US/biore-skincare
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 05/31/2013
-->
<script type="text/javascript">
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    document.write('<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore932;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore932;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>       
        <div class="flexslider" id="theater">
            <ul class="slides">
    	        <!--<li id="theaterItem6" class="theaterHolder">
    		        <div class="theaterBox">
    		            <h1>Goodbye Hello Contest</h1>
    		            <p>
    	                    <asp:HyperLink ID="helloContest" NavigateUrl="https://apps.facebook.com/proveitrewards/?tab=gh" runat="server" Target="_blank">
    	                        <img class="normal" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/voteNowBtn.png") %>" alt="Vote Now" />
    	                        <img class="over" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/voteNowBtnOver.png") %>" alt="Vote Now" />
    	                    </asp:HyperLink>
    		            </p>
    		            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/theater6Bg.jpg") %>" alt="" />
    		        </div>
    	        </li>-->
    	        <li id="theaterItem4" class="theaterHolder">
    		        <div class="theaterBox">
    		            <h1>Goodbye Harsh Scrubs</h1>
    		            <p>
    		                <span>Hello, Clearer Healthier Skin - in <strong>Just Two Days</strong>.</span>
    	                    <asp:HyperLink ID="acneScrub" NavigateUrl="complexion-clearing-products/acne-clearing-scrub" runat="server">
    	                        <img class="normal" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/findOutHowBtn.png") %>" alt="Find Out How" />
    	                        <img class="over" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/findOutHowBtnOver.png") %>" alt="Find Out How" />
    	                    </asp:HyperLink>
    		            </p>
    		            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/theater4Bg.jpg") %>" alt="" />
    		        </div>
    	        </li>
    	        <li id="theaterItem5" class="theaterHolder">
    		        <div class="theaterBox">
    		            <h1>Hello Blemish-Fighting Team Goodbye All Kinds of Acne</h1>
    		            <p>
    		                <!--<span>Hello, Clearer Healthier Skin - in <strong>Just Two Days</strong>.</span>-->
    	                    <asp:HyperLink ID="blemishFighting" NavigateUrl="biore-facial-cleansing-products/blemish-fighting-team.aspx" runat="server">
    	                        <img class="normal" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/findOutHowBtn.png") %>" alt="Find Out How" />
    	                        <img class="over" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/findOutHowBtnOver.png") %>" alt="Find Out How" />
    	                    </asp:HyperLink>
    		            </p>
    		            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/theater5Bg.jpg") %>" alt="" />
    		        </div>
    	        </li>
    	        <li id="theaterItem2" class="theaterHolder">
    		        <div class="theaterBox">
    		            <h1>Prove It!&trade; Reward Points</h1>
    		            <p>
    		                <span>Get rewards for your routine on Facebook.</span>
    	                    <asp:HyperLink ID="proveItTheaterLink" NavigateUrl="http://www.facebook.com/bioreskin?sk=app_205787372796203" runat="server" Target="_blank">
    	                        <img class="normal" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/startEarningTodayBtn.png") %>" alt="Start Earning Today!" />
    	                        <img class="over" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/startEarningTodayBtnOver.png") %>" alt="Start Earning Today!" />
    	                    </asp:HyperLink>
    		            </p>
    		            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/theater2Bg.jpg") %>" alt="" />
    		        </div>
    	        </li>
            </ul>
        </div>
        <div class="centeringDiv" id="pagerWrapper"><div id="pager"></div></div>
    </div>
    <div id="promoHolder">
        <div id="promo1" class="promo">
            <div class="promoImage"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/promo3Img.jpg") %>" alt="" /></div>
            <div class="promoContent">
                <div class="promoHeader">Join the movement</div>
                <div class="promoBody">Sign up to hear about offers and promos from Bior&eacute;<sup>&reg;</sup> Skincare.</div>
                <div class="promoButton"><a href="email-newsletter-sign-up">Check it Out <span class="arrow">&rsaquo;</span></a></div>
            </div>
        </div>
        
        <div id="promo3" class="promo">
            <div class="promoImage"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/promo1Img.jpg") %>" alt="" /></div>
            <div class="promoContent">
                <div class="promoHeader">2 Steps to a<br />Complete Clean</div>
                <div class="promoBody">1. Cleanse daily<br /> 2. Pore Strip weekly</div>
                <div class="promoButton"><a href="biore-facial-cleansing-products">Learn More <span class="arrow">&rsaquo;</span></a></div>
            </div>
        </div>
    </div>
  
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/jquery.flexslider.min.js")
        .Render("~/js/combinedhome_#.js")
    %>
</asp:Content>
