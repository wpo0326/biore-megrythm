﻿<%@ Page Title="Watch it Work | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.watch_it_work._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="See Bioré® Skincare cleansers and Pore Strips work first-hand." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/videoLanding.css")
        .Add("~/css/ghindaVideoPlayer.css")
        .Render("~/css/combinedvideo_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <div class="intro">
                    <h1>Watch It Work</h1>
                    <p>See our Pore Strips and cleansers in action.</p>
                </div>
                <div id="content">
                    <ul>
                        <li id="video_porestrips">
                            <div class="videoHolder">
                                <div class="video pie">
                                    <div id="video1">
                                        <a class="videoLink" target="_blank" href="../video/PORE_STRIPS_US_512x288_h264_768Kbps.mp4" rel="videoParams::512|288|40">
                                            <img src="../images/videoLanding/videoStill1.jpg" alt="" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="copyAndImg">
                                <img src="../images/videoLanding/poreStrips.jpg" alt="" />
                                <h3>Bior&eacute;<sup>&reg;</sup> Pore Strips</h3>
                                <p>Our Deep Cleansing Pore Strips clean a week's worth of buildup off your face. The proof is on the Pore Strip.</p>
                            </div>   
                            <div class="clear"></div>                      
                        </li>
                        <li id="video_murt">
                            <div class="videoHolder">
                                <div class="video pie">
                                    <div id="video2">
                                        <a class="videoLink" target="_blank" href="../video/TOWELETTES_US_512x288_h264_768Kbps.mp4" rel="videoParams::512|288|34">
                                            <img src="../images/videoLanding/videoStill2.jpg" alt="" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="copyAndImg">
                                <img src="../images/videoLanding/murt.jpg" alt="" />
                                <h3>Bior&eacute;<sup>&reg;</sup> Make-up Removing Towelettes</h3>
                                <p>Make-up Removing Towelettes clean all types of make-up off&mdash;even waterproof mascara&mdash;with no leftover residue.</p>
                            </div>  
                            <div class="clear"></div> 
                        </li>
                        <li id="video_csbc">
                            <div class="videoHolder">
                                <div class="video pie">
                                    <div id="video3">
                                        <a class="videoLink" target="_blank" href="../video/CLEANSER_US_512x288_h264_768Kbps.mp4" rel="videoParams::512|288|40">
                                            <img src="../images/videoLanding/videoStill3.jpg" alt="" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="copyAndImg">
                                <img src="../images/videoLanding/csbc.jpg" alt="" />
                                <h3><span class="new">NEW</span> Combination Skin Balancing Cleanser</h3>
                                <p>Get an all-over clean without over-drying.</p>
                            </div>
                            <div class="clear"></div>    
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    
<%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/swfobject.min.js")
        .Add("~/js/jquery-ui-1.8.2.custom.min.js")
        .Add("~/js/jquery.ghindaVideoPlayer.js")
        .Render("~/js/combinedvideo_#.js")
%>
</asp:Content>
