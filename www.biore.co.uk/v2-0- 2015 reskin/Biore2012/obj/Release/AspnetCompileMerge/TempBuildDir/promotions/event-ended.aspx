﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="event-ended.aspx.cs" Inherits="Biore2012.event_ended" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedrules_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <div id="polaroid"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/utilityPages/polaroid.jpg") %>" alt="" /></div>
                <div id="content">
                    <h1>Bior&eacute;<sup>&reg;</sup> Skincare</h1>
                    <h2>Event Ended</h2>
					<p>We're sorry. The program you are trying to access has ended.</p>
					<p>To learn about our latest news and offers, please <a href="../email-newsletter-sign-up">sign up to receive our newsletter</a>.</p>
                </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
