﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Drawing;
using System.IO;
using KAOForms;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Biore2012.BLL;
using System.Web.UI.HtmlControls;

namespace Biore2012.facebook.StoryToPoreOver
{
	public partial class PostLaunchForm : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Page.Header.DataBind();

			// Grab masterpage, set appropriate body classes
			MasterPage myMasterTemp = (MasterPage)this.Master;

			// Author Partnership custom styles
			if (Request.QueryString["promo"] != null &&
				Request.QueryString["promo"] == "biore_us_201210_AthrPrtnLaunchDayCFinalSweeps5M2DBX67")
			{
				Panel panelFbPageTabScript = (Panel)myMasterTemp.FindControl("fbPageTabScript");
				panelFbPageTabScript.Visible = false;

				Literal litFBPageAppId = (Literal)panelFbPageTabScript.FindControl("fbPageTabAppID");
				litFBPageAppId.Text = System.Configuration.ConfigurationManager.AppSettings["AuthorPartnershipFBAppId"];

				Panel panelFbScript = (Panel)myMasterTemp.FindControl("fbScript");
				panelFbScript.Visible = false;				

				updateMasterBodyClass(" stpoFBForm " + Request.QueryString["promo"]);
			}

			updateMasterBodyClass(" forms");

			// hide floodlight pixels for all form pages
			myMasterTemp.FindControl("floodlightPixels").Visible = false;

			// This turns on and off the Question Pro Include for this page...
			((Panel)Page.Master.FindControl("panel_Question_Pro")).Visible = false;

			if (Request.QueryString["promo"] != null)
			{
				string promoID = Request.QueryString["promo"].ToString().ToLower(); // MAKE SURE TO CHECK LOWERCASE
				bool promoMatch = false;
				if (promoID.Contains("biore_us_201108_walmartcoup")) { promoMatch = true; } // MAKE SURE TO CHECK LOWERCASE
				if (promoID.Contains("biore_us_201108_newlooktargetendcap")) { promoMatch = true; } // MAKE SURE TO CHECK LOWERCASE
				if (promoID.Contains("biore_us_201111_freshfanporespectives")) { promoMatch = true; } // MAKE SURE TO CHECK LOWERCASE
				if (promoID.Contains("biore_us_201203_tripackette_fb_samp")) { promoMatch = true; } // MAKE SURE TO CHECK LOWERCASE
				if (promoID.Contains("biore_us_201203_tripackette_imedia1_samp")) { promoMatch = true; } // MAKE SURE TO CHECK LOWERCASE

				if (promoMatch)
				{
					Response.Redirect("~/404/");
				}
			}
			if (IsPostBack)
			{
				CheckPhotoAndSave();
			}

		}

		private void updateMasterBodyClass(string val)
		{
			if ((MasterPage)this.Master is Site)
			{
				Site myMaster = (Site)this.Master;
				myMaster.bodyClass += val;
			}
			else
			{
				SiteNoHeader myMaster = (SiteNoHeader)this.Master;
				myMaster.bodyClass += val;
			}
		}

		#region CustomFormQuestions


		private class ImgUploadStatus
		{
			public bool status { get; set; }
			public string msg { get; set; }
		}

		// SHOW/HIDE FileUpload based off of PanelForm visibility status
		protected void Page_PreRender(object sender, EventArgs e)
		{
			Panel pf = (Panel)ucForm.FindControl("PanelForm");
			
			if (pf.Visible == true)
			{
				showFileUpload();
			}
			else
			{
				hideFileUpload();
			}
		}

		private void CheckPhotoAndSave()
		{	
			//Reset validation field
			valCheck.Text = "checkString";
			Page.Validate();
			if(Page.IsValid)
			{
				validateAndSaveFBImg();
			}
		}

		private void validateAndSaveFBImg()
		{
			Control ucFormMemberInfo = ucForm.FindControl("ucFormMemberInfo") as Control;
			TextBox email = ucFormMemberInfo.FindControl("Email") as TextBox;
			int iMemID = ConsumerInfoUtils.GetMemId(email.Text);
			string memID = iMemID.ToString();
			ImgUploadStatus iuStatus = UploadPhoto(hidAddPhoto.Value, memID);

			if (!iuStatus.status)
			{
				reqValCheck.ErrorMessage = iuStatus.msg;
				reqValCheck.Visible = true;
			}
			else // File Saved Successfully
			{
				AuthorPartnershipAdditionalFieldsDAO iuDAO = new AuthorPartnershipAdditionalFieldsDAO();

				if (!string.IsNullOrEmpty(memID))
				{
					int iStatus = iuDAO.AddAuthPartnershipFields(
							iMemID, iuStatus.msg, email.Text, ddBooksRead.Text, ddBookClub.Text, ddAvailable.Text, txtWhyYou.Text);

					if (iStatus != -1)
					{
						PageUtils pUtils = new PageUtils();
						Control HeaderControl = pUtils.getHeaderControl(Page);
						pUtils.createMetaDataTag("DCSext.imgFilename", iuStatus.msg, "metaImgUploadMsg", HeaderControl);
					}
					else
					{
						processError("There was an error saving the image, please try again.");
					}
				}
			}
		}
		

		private void processError(string err)
		{
			reqValCheck.ErrorMessage = err;
			reqValCheck.Visible = true;

			//Force validation error
			valCheck.Text = "";
			Page.Validate();
		}

		private void hideFileUpload()
		{
			panAddFormFields.Visible = false;
			MoveFileUpload.Visible = false;
		}

		private void showFileUpload()
		{
			panAddFormFields.Visible = true;
			MoveFileUpload.Visible = true;
		}

		private ImgUploadStatus UploadPhoto(string url, string memidStr)
		{
			ImgUploadStatus iuStatus = new ImgUploadStatus();
			iuStatus.status = false;  // set to false by default, only set true if successful
			iuStatus.msg = "";

			//Reset validation field
			valCheck.Text = "checkString";
			Page.Validate();

			TimeSpan t = (DateTime.UtcNow - new DateTime(1970, 1, 1));
			double timestamp = (double)t.TotalMilliseconds; // TODO: is this random enough to get rid of original filename?

			if (this.Page.IsValid)
			{
				String contentType = String.Empty;
				String fileName = String.Empty;
				int width = 0;
				int height = 0;

				if (!string.IsNullOrEmpty(url))
				{
					String FileExtension = url.Substring(url.LastIndexOf('.')).ToLower();
					if (FileExtension.Contains(".jpg") || FileExtension.Contains(".jpeg") || FileExtension.Contains(".gif") || FileExtension.Contains(".png"))
					{
						try
						{
							HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(new Uri(url));
							webRequest.Credentials = CredentialCache.DefaultCredentials;
							HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
							Int64 fileSize = webResponse.ContentLength;
							
							if (fileSize > 6000000)
							{
								iuStatus.msg = "The file you chose is too large. Please keep filesize under 5MB.";
							}
							else
							{
								Stream imageStream = new WebClient().OpenRead(url);
								fileName = url.Substring(url.LastIndexOf('/') + 1);
								string tmpFileName = url.Substring(url.LastIndexOf('/') + 1);
								string newFileName = ConfigurationManager.AppSettings["201211AuthorPartnership_ImgPrefix"] + 
													 "_" + memidStr + 
													 "_" + timestamp.ToString().Replace(".", "-") + 
													 "_" + fileName;

								System.Drawing.Image myImg = System.Drawing.Image.FromStream(imageStream);
								width = myImg.Width;
								height = myImg.Height;

								if (myImg.Width > 0 && myImg.Height > 0)
								{
									iuStatus.status = true;
									iuStatus.msg = newFileName;
									string fullPath = System.Configuration.ConfigurationManager.AppSettings["201211AuthorPartnership_ImgPath"] + "\\" + iuStatus.msg; // iuStatus.msg set to newFilename above if  iuStatus.status == true
									myImg.Save(fullPath);
								}
								else
								{
									iuStatus.msg = "Error with image file.  Image was not uploaded.";
								}
							}
						}
						catch (Exception)
						{
							iuStatus.msg = "Error with file.  Image was not uploaded.";
						}
					}
					else
					{
						iuStatus.msg = "Cannot accept files of this type. Please use .jpg, .gif or .png.";
					}
				}
				else
				{
					iuStatus.msg = "No file provided. Please select an image.";
				}

				if (!iuStatus.status)
				{
					//Force validation error
					valCheck.Text = "";
					Page.Validate();
				}
			}
			return iuStatus;
		}

		#endregion

	}

	public class AuthorPartnershipAdditionalFieldsDAO
	{
		public int AddAuthPartnershipFields(int memberID, 
											string Filename, 
											string email, 
											string booksRead, 
											string bookClubMember, 
											string available, 
											string whyPerfect)
		{
			int validEntry = -1;

			SqlConnection myConn = new SqlConnection();
			myConn = new SqlConnection(ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString);
			myConn.Open();
			SqlCommand myCom = new SqlCommand("sp201211AuthorPartnershipAddQuestions", myConn);
			myCom.CommandType = CommandType.StoredProcedure;

			SqlParameter myP1 = myCom.Parameters.Add("@CIMemID", SqlDbType.Int);
			myP1.Value = memberID.ToString();

			SqlParameter myP2 = myCom.Parameters.Add("@ImageFileName", SqlDbType.VarChar, 250);
			myP2.Value = Filename;

			SqlParameter myP3 = myCom.Parameters.Add("@Email", SqlDbType.VarChar, 250);
			myP3.Value = email;

			SqlParameter myP4 = myCom.Parameters.Add("@BooksRead", SqlDbType.VarChar, 10);
			myP4.Value = booksRead;

			SqlParameter myP5 = myCom.Parameters.Add("@BookClubMember", SqlDbType.Bit);
			myP5.Value = bookClubMember == "Yes";

			SqlParameter myP6 = myCom.Parameters.Add("@Available", SqlDbType.Bit);
			myP6.Value = available == "Yes";

			SqlParameter myP7 = myCom.Parameters.Add("@WhyPerfect", SqlDbType.VarChar, 1000);
			myP7.Value = whyPerfect;

			SqlParameter retval = myCom.Parameters.Add("@return_value", SqlDbType.Int);
			retval.Direction = ParameterDirection.ReturnValue;

			try
			{
				myCom.ExecuteNonQuery();
				validEntry = (Int32)myCom.Parameters["@return_value"].Value;
			}
			catch (Exception ex100)
			{
				logAddAuthPartnershipFieldsError(100, ex100.ToString(), memberID, Filename);
				validEntry = -1;

			}
			finally
			{
				if (myCom != null)
				{
					myCom.Dispose();
					myCom = null;
				}
				if (myConn != null)
				{
					myConn.Close();
					myConn = null;
				}
			}

			if (validEntry != 0)
			{
				logAddAuthPartnershipFieldsError(200, validEntry.ToString(), memberID, Filename);
				validEntry = -1;
			}
			return validEntry;
		}

		private void logAddAuthPartnershipFieldsError(int EventID, string message, int memberID, string Filename)
		{
			StringBuilder myString = new StringBuilder();
			myString.Append("Error executing stored procedure in Add_FBImgUpload in spAuthorPartnershipFBImgUpload201210: \r\n");
			myString.Append("Error Message: " + message + "\r\n");
			myString.Append("memberID: " + memberID + "\r\n");
			myString.Append("FName: " + Filename + "\r\n");

			LogEntry myLog = new LogEntry();
			myLog.EventId = EventID;
			myLog.Message = myString.ToString();
			myLog.Priority = 1;
			Logger.Write(myLog);
			myLog = null;
		}

	}
}
