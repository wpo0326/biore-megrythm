﻿<%@ Page Title="Free Your Pores | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Get healthy-looking, radiant skin with products from Bioré® Skincare" name="description" />
    <meta content="skincare, clean face" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/UniversPro67.css")
        .Add("~/css/homepage.css")
        .Add("~/css/flexslider.css")
        .Render("~/CSS/combinedhome_#.css")
    %>
    <meta name="viewport" content="width=device-width">

    <script type="text/javascript" src="<%= VirtualPathUtility.ToAbsolute("~/js/homepage.js") %>"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            //$('.scroll-dots a').click(function (event) {
            //    event.preventDefault();
            //    var target = $(this).attr('id');
            //    console.log(target);
            //});

            var windowSize = $(window).width();
            var mobileSize = 768;
              $('a[href*=#]:not([href=#])').click(function (event) {
                event.preventDefault();
                console.log('Alvaro');
                if (windowSize > mobileSize) {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if (target.length) {
                            $('html,body').animate({
                                scrollTop: target.offset().top
                            }, 1000);
                            return false;
                        }
                    }
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $('#theaterItem1 a').click(
                function (e) {
                    e.preventDefault();

                    var axel = Math.random() + "";
                    var a = axel * 10000000000000;
                    var strScript = "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore551;ord='" + a + "'?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"
                               + "<noscript>"
                               + "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore551;ord=1?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"
                               + "</noscript>";

                    //$('body').prepend(strScript);
                    var target = $(this).attr('target');
                    var uri = $(this).attr('href');

                    setTimeout(function () {
                        if (target) {
                            window.open(uri, '_blank');
                        } else {
                            window.location = uri;
                        }
                    }, 1000);

                });
        })
    </script>
  
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main1">
    <div class="home-content">
        <section id="section-2017-interim">
                <div class="top-baking-soda-2017">
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/top-right-bubbles.png") %>" alt="top right bubbles" /></div>
                <div class="welcome-module clearfix">
					<div style="position: absolute; margin: 70px auto 0; text-align: center; width: 100%; cursor: pointer;"><a href="/biore-facial-cleansing-products/"><img src="images/spacer.gif" height="510" width="300" /></a></div>
                     <div class="header-2017">
                        <div class="acne-header-2017">
                            <h2>
                                <span class="acnes foamparty">Kiss Your <span class="cateye">Contour-<br />Cat-Eye-Lip-Kit</span></span>
                              
                            </h2>
                            <h2 style="margin-top: 14px;">
                                <span class="acnessmall">Goodbye!</span>

                            </h2>
                        </div>
                       
                    </div>

                    <div class="left-text">
                        <div>
                           <span class="new">New </span> Bior&eacute;<sup>&reg;</sup><br />Cleansing<br />Micellar Waters

                        </div>

                        <!--<div class="allInOneCleansing">All-in-One Cleansing!</div>-->

                    </div>

                     <div class="right-text">
                         <div>
                         All-in-one cleanser,<br />makeup remover<br />and refresher
                              <span class="waterproof"> Your Pores<br />(And Pillowcase)<br />Will Thank You</span>

                         </div>

                     </div>

                     <div class="footer-2017">
                        <div class="acne-footer-2017">
                            <!--*Garnier Skin Active Micellar Water All-In-1 Waterproof, the leading Micellar Water according to Neilsen Scanning Data, 52 Weeks Ending April 2018.-->
                        </div>
                    </div>


                    <div class="hide-mobile"></div>
                    <div class="scrollDownHldr">
                        <p class="scrollDown">SCROLL DOWN</p>
                        <div id="arrowBounce">
                            <a href="#section-2018-products">
                                <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/arrowDown.png") %>" class="section-down-arrow" /></a>
                        </div>

                    </div>
                </div>

                <div class="scroll-dots">
                    <a href="#section-2017-interim"><span class="scroll-dot enable"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                    <a href="#section-micellar"><span class="scroll-dot"></span></a>
                    <a href="#section-02"><span class="scroll-dot"></span></a>
                    <a href="#section-06"><span class="scroll-dot"></span></a>
                    <a href="#section-03"><span class="scroll-dot"></span></a>
                    <a href="#section-05"><span class="scroll-dot"></span></a>

                </div>
                <div class="bottom-charcoal-2017">
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/bottom-left-bubbles.png") %>" alt="bottom left bubbles" /></div>
            </section>
        <section class="home-section" id="section-2018-products">
                <h4>Removes Stubborn Makeup, Deep Cleans & Clarifies Pores<br />
<span class="waterproof">(Lookin' at you, Waterproof Mascara!)</span><br /></h4>
                <div class="container3">

                    <div class="prodList">
                        
                        <div class="videoHolder">
                          <div class="video pie">
                              <div id="video1">
                                  <a class="popup-youtube" href="https://www.youtube.com/watch?v=WpB3BFp4kHE">
                                      <img src="/images/homepage/homepage-video-still.jpg" alt="Shay Micellar Water" />
                                  </a>
                              </div>
                          </div>
                      </div>
                        <!--<div class="disclaimer">*Garnier Skin Active Micellar Water All-In-1 Waterproof, the leading Micellar Water according to Neilsen Scanning Data, 52 Weeks Ending April 2018.</div>-->
                        <!--<ul>
                            <li class="productListSpacer">
                                <a href="/take-it-all-off/baking-soda-cleansing-micellar-water"><img src="images/ourProducts/products/small/baking-soda-cleansing-micellar-water.png" alt="BAKING SODA CLEANSING MICELLAR WATER" /></a>
                                <h3>BAKING SODA CLEANSING<br />MICELLAR WATER</h3>
                                <p>Specially formulated for combination skin. Removes makeup, deep cleans pores and balances without over-drying.
  <a href="/take-it-all-off/baking-soda-cleansing-micellar-water" class="buynowbtn"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/micellar-products-button.png") %>" alt="button"/></a></p>
         
                               
                            </li>
                            <li class="productListSpacer">
                                <a href="/take-it-all-off/charcoal-cleansing-micellar-water"><img src="images/ourProducts/products/small/charcoal-cleansing-micellar-water.png" alt="CHARCOAL CLEANSING MICELLAR WATER" /></a>
                                <h3>CHARCOAL CLEANSING<br />MICELLAR WATER</h3>
                                <p>Specially formulated for oily skin. Removes makeup, deep cleans pores and removes excess oil.
 <a href="/take-it-all-off/charcoal-cleansing-micellar-water" class="buynowbtn"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/micellar-products-button.png") %>" alt="button"/></a></p>

                    
                            </li>
                        </ul>-->
                    </div>
                </div>

                <div class="scroll-dots">
                    <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot enable"></span></a>
                    <a href="#section-micellar"><span class="scroll-dot"></span></a>
                    <a href="#section-02"><span class="scroll-dot"></span></a>
                    <a href="#section-06"><span class="scroll-dot"></span></a>
                    <a href="#section-03"><span class="scroll-dot"></span></a>
                    <a href="#section-05"><span class="scroll-dot"></span></a>
                </div>
            </section>
        <section class="home-section"" id="section-micellar">
            <div class="bg-left"></div><div class="bg-right"></div>
            <div class="headline-center">
				    <h4>Makeup Remove</h4>
			    </div> 
            <div class="container3">
	         
                <div class="prodList">

                        <ul>
                            <li class="productListSpacer">
                                <div class="combo-skin">Combo Skin?</div>
                                <div class="animation">
                                   <div class="animation-container">
                                        <div class="animation-border"><a href="/take-it-all-off/baking-soda-cleansing-micellar-water"></a></div>
                                       <div class="animation-skin-container">
                                            <div class="animation-skin"><a href="/take-it-all-off/baking-soda-cleansing-micellar-water"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/ourProducts/products/large/baking-soda-cleansing-micellar-water.png") %>"/></a></div>
                                        </div>
                                        <a href="#" class="fancybox-media fbm">
                                            <!--<img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section3.gif") %>">-->
                                            <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" />
                                        </a>
                                        
                                        <div class="animation-play-btn">                                    
                                            <!--<img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="text-blurb">
                                    <h3 class="purple">TAKE IT ALL OFF</h3>
                                    <span>Refresh and balance skin without over-drying</span>
                                </div>
                                <div class="learn-more purple"><a href="/take-it-all-off/baking-soda-cleansing-micellar-water">Find Out More</a></div>
                            </li>
                            <li class="productListSpacer">
                                <div class="oily-skin">Oily Skin?</div>
                                <div class="animation">
                                    <div class="animation-container">
                                        <div class="animation-border"><a href="/deep-porefection-combination-skin/charcoal-cleansing-scrub"></a></div>
                                        <div class="animation-skin-container">
                                            <div class="animation-skin"><a href="/take-it-all-off/charcoal-cleansing-micellar-water"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/ourProducts/products/large/charcoal-cleansing-micellar-water.png") %>"/></a></div>
                                        </div>
                                        <a href="#" class="fancybox-media fbm">
                                            <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section3.gif") %>">
                                            <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section3.gif") %>">                                
                                        </a>
                                        <div class="animation-play-btn">                                    
                                            <!--<img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="text-blurb">
                                    <h3 class="purple">SHOW'S OVER LIP KIT</h3>
                                    <span>Refresh pores, removing excess oil</span>
                                </div>
                                <div class="learn-more purple"><a href="/take-it-all-off/charcoal-cleansing-micellar-water">Find Out More</a></div>
                            </li>
                        </ul>
    

                </div>

		           
            </div>

            <div class="scroll-dots">
                <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                <a href="#section-micellar"><span class="scroll-dot enable"></span></a>
                <a href="#section-02"><span class="scroll-dot"></span></a>
                <a href="#section-06"><span class="scroll-dot"></span></a>
                <a href="#section-03"><span class="scroll-dot"></span></a>
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section"" id="section-02">
            <div class="bg-left"></div><div class="bg-right"></div>
            <div class="container3">

                
                <div class="headline-center">
				    <h4>Cleanse</h4>
			    </div> 

                <div class="prodList">
	            
			            <ul>
                            <li class="productListSpacer">
                                <div class="combo-skin">Combo Skin?</div>
                                 <div class="animation">
                                    <div class="animation-container">
                                        <div class="animation-border"><a href="/deep-porefection-combination-skin/baking-soda-pore-cleanser"></a></div>
                                        <a href="#" class="fancybox-media fbm">
                                            <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section3.gif") %>">
                                            <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section3.gif") %>">                                
                                        </a>
                                        <div class="animation-skin-container">
                                            <div class="animation-skin"><a href="/deep-porefection-combination-skin/baking-soda-pore-cleanser"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/ourProducts/products/large/baking-soda-pore-cleanser.png") %>"/></a></div>
                                        </div>
                                        <div class="animation-play-btn">                                    
                                            <!--<img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>-->
                                        </div>
                                    </div>
			                    </div>
                                <div class="text-blurb">
                                    <h3 class="green">BRING ON THE BUBBLY</h3>
                                    <span>Deep clean and naturally exfoliate</span>
                                </div>
                                <div class="learn-more green"><a href="/deep-porefection-combination-skin/baking-soda-pore-cleanser">Find Out More</a></div>
                            </li>
                            <li class="productListSpacer">
                                <div class="oily-skin">Oily Skin?</div>
                                <div class="animation">
                                    <div class="animation-container">
                                        <div class="animation-border"><a href="/deep-porefection-oily-skin/deep-pore-charcoal-cleanser"></a></div>
                                        <a href="#" class="fancybox-media fbm">
                                            <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section3.gif") %>">
                                            <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section3.gif") %>">                                
                                        </a>
                                        <div class="animation-skin-container">
                                            <div class="animation-skin"><a href="deep-porefection-oily-skin/deep-pore-charcoal-cleanser"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/ourProducts/products/large/CHARCOAL_390X418.png") %>"/></a></div>
                                        </div>
                                        <div class="animation-play-btn">                                    
                                            <!--<img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>-->
                                        </div>
                                    </div>
			                    </div>
                                <div class="text-blurb">
                                    <h3 class="green">DRAW OUT THE DIRT</h3>
                                    <span>Naturally draw out and trap impurities</span>
                                </div>
                                <div class="learn-more green"><a href="/deep-porefection-oily-skin/deep-pore-charcoal-cleanser">Find Out More</a></div>
                            </li>
                        </ul> 

                </div>
		          
            </div>

            <div class="scroll-dots">
                <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                    <a href="#section-micellar"><span class="scroll-dot"></span></a>
                    <a href="#section-02"><span class="scroll-dot enable"></span></a>
                    <a href="#section-06"><span class="scroll-dot"></span></a>
                    <a href="#section-03"><span class="scroll-dot"></span></a>
                    <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section" id="section-06">
            <div class="bg-left"></div>
            <div class="bg-right"></div>
            <div class="container3">
                
                 <div class="headline-center">
				    <h4>Scrub</h4>
			    </div> 

                <div class="prodList">
	            
			            <ul>
                            <li class="productListSpacer">
                                <div class="combo-skin">Acne Prone Combo Skin?</div>
                                  <div class="animation">
                                    <div class="animation-container"><!--https://www.youtube.com/embed/csEft4NwP3g?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"-->
                                        <div class="animation-border"><a href="/acnes-outta-here/baking-soda-acne-scrub"></a></div>
                                        <a href="#" class="fancybox-media fbm">
                                            <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2-2018.gif") %>">
                                            <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2-2018.gif") %>">
                                        </a>
                                        <div class="animation-skin-container">
                                            <div class="animation-skin">
                                                <a href="acnes-outta-here/baking-soda-acne-scrub"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/ourProducts/products/large/baking-soda-acne-scrub.png") %>" /></a></div>
                                        </div>
                                        <!--<div class="animation-play-btn">
                                            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>" />
                                        </div>-->
                                    </div>
                                </div>
                                 <div class="text-blurb">
                                    <h3 class="orange">GET FIZZY WITH IT</h3>
                                    <span>Unclog pores and rebalance skin to help reduce acne</span>
                                </div>
                                <div class="learn-more orange"><a href="/acnes-outta-here/baking-soda-acne-scrub">Find Out More</a></div>

                            </li>
                            <li class="productListSpacer">
                                <div class="oily-skin">ACNE PRONE OILY SKIN?</div>
                                 <div class="animation">
                                    <div class="animation-container"><!--https://www.youtube.com/embed/csEft4NwP3g?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"-->
                                        <div class="animation-border"><a href="/acnes-outta-here/charcoal-acne-clearing-cleanser"></a></div>
                                        <a href="#" class="fancybox-media fbm">
                                            <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2-2018.gif") %>">
                                            <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2-2018.gif") %>">
                                        </a>
                                        <div class="animation-skin-container">
                                            <div class="animation-skin">
                                                <a href="/acnes-outta-here/charcoal-acne-clearing-cleanser"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/ourProducts/products/large/charcoal-acne-clearing-cleanser-scrub-combo.png") %>" /></a></div>
                                        </div>
                                        <!--<div class="animation-play-btn">
                                            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>" />
                                        </div>-->
                                    </div>
                                </div>

                                <div class="text-blurb">
                                    <h3 class="orange">ACNE BE GONE</h3>
                                    <span>Wash away acne-causing dirt and absorb excess oil</span>
                                </div>
                                <div class="learn-more orange"><a href="/acnes-outta-here/charcoal-acne-clearing-cleanser">Find Out More</a></div>
                            </li>
                        </ul> 
                </div>
   
            </div>

            <div class="scroll-dots">
                <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                    <a href="#section-micellar"><span class="scroll-dot"></span></a>
                    <a href="#section-02"><span class="scroll-dot"></span></a>
                    <a href="#section-06"><span class="scroll-dot enable"></span></a>
                    <a href="#section-03"><span class="scroll-dot"></span></a>
                    <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section" id="section-03">
            <div class="bg-left"></div><div class="bg-right"></div>
            <div class="container3">

                <div class="headline-center">
				    <h4>Treat</h4>
			    </div> 

                <div class="prodList">
	            
			            <ul>
                            <li class="productListSpacer">
                                <div class="combo-skin">Combo Skin?</div>
                                  <div class="animation">
                                    <div class="animation-container">
                                        <div class="animation-border"><a href="/breakup-with-blackheads/deep-cleansing-pore-strips"></a></div>
                                        <a href="https://www.youtube.com/embed/wHHtq8HL3q4?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                            <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2.gif") %>">
                                            <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2.gif") %>">                                
                                        </a>
                                        <div class="animation-skin-container">
                                            <div class="animation-skin"><a href="/breakup-with-blackheads/deep-cleansing-pore-strips"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/ourProducts/products/large/deepCleansingPoreStrips.png") %>"/></a></div>
                                        </div>
                                            <div class="animation-play-btn">
                                            <!--<img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>-->
                                        </div>
                                    </div>
			                    </div>
                                <div class="text-blurb">
                                    <h3 class="red">FREE YOUR PORES</h3>
                                    <span>Unclog pores for a deep clean</span>
                                </div>
                                <div class="learn-more red"><a href="/breakup-with-blackheads/deep-cleansing-pore-strips">Find Out More</a></div>

                            </li>
                            <li class="productListSpacer">
                                <div class="oily-skin">Oily Skin?</div>
                                 <div class="animation">
                                    <div class="animation-container">
                                        <div class="animation-border"><a href="/breakup-with-blackheads/charcoal-pore-strips"></a></div>
                                        <a href="https://www.youtube.com/embed/wHHtq8HL3q4?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                            <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2.gif") %>">
                                            <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2.gif") %>">                                
                                        </a>
                                        <div class="animation-skin-container">
                                            <div class="animation-skin"><a href="/breakup-with-blackheads/charcoal-pore-strips"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/ourProducts/products/large/CharcoalPoreStrips.png") %>"/></a></div>
                                        </div>
                                            <div class="animation-play-btn">
                                            <!--<img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>-->
                                        </div>
                                    </div>
			                    </div>
                                <div class="text-blurb">
                                    <h3 class="red">BE MAGNETIC</h3>
                                    <span>Draw out excess oil and deep down dirt</span>
                                </div>
                                <div class="learn-more red"><a href="/breakup-with-blackheads/charcoal-pore-strips">Find Out More</a></div>
                            </li>
                        </ul> 
                </div>
            </div>

            <div class="scroll-dots">
                <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                    <a href="#section-micellar"><span class="scroll-dot"></span></a>
                    <a href="#section-02"><span class="scroll-dot"></span></a>
                    <a href="#section-06"><span class="scroll-dot"></span></a>
                    <a href="#section-03"><span class="scroll-dot enable"></span></a>
                    <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section" id="section-05">
            <div class="bg-left"></div><div class="bg-right"></div>
            <div class="container3">
	            <div class="container2">
		            <div class="container1">
			            <div class="headline">
				            <h4>Bye Bye Masquerade<br />Free your pores and show your true face</h4>
			            </div> 
			            
			            <div class="animation">
			                <a href="https://www.youtube.com/embed/SJlgQXb1Qw4?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe">
			                    <span style="width:100%; height:650px;position:absolute;top:0;left:0;z-index:300;display:block;"></span>
			                 </a>
			            
                            <div class="animation-container">
                                    <img id="imganim" class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section5.gif") %>" alt="Verstop je niet meer achter je masker!" />                                
                                
                                <!--<div class="animation-skin-container">
                                    <div class="animation-skin"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/01_baking_soda_cleansing_scrub.png") %>"/></div>
                                </div>-->
                                <!--<div class="animation-play-btn">                                    
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>
                                </div>-->
                            </div>
			            </div>
			            <div class="product">
				            <p>...and follow us on</p>
				            <div class="social-container">
				                <div class="facebook"><a href="https://www.facebook.com/bioreaus" target="_blank"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/facebook.png") %>" alt="facebook" /></a></div>
				                <div class="instagram"><a href="https://www.instagram.com/bioreaus" target="_blank"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/instagram.png") %>" alt="instagram" /></a></div>
				                <div class="youtube"><a href="https://www.youtube.com/channel/UCfw8nLL6pqo68Bi6z0_ZbMw" target="_blank"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/youtube.png") %>" alt="youtube" /></a></div>
				            </div>
				            <p>#freeyourpores</p>
			            </div>
		            </div>
	            </div>
            </div>

            <div class="scroll-dots">
                <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                    <a href="#section-micellar"><span class="scroll-dot"></span></a>
                    <a href="#section-02"><span class="scroll-dot"></span></a>
                    <a href="#section-06"><span class="scroll-dot"></span></a>
                    <a href="#section-03"><span class="scroll-dot"></span></a>
                    <a href="#section-05"><span class="scroll-dot enable"></span></a>
            </div>
        </section>
    </div>
  
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/jquery.flexslider.min.js")
        .Render("~/js/combinedhome_#.js")
    %>
</asp:Content>