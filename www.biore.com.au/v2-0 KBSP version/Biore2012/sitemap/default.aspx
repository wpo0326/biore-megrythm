﻿<%@ Page Title="Sitemap | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.sitemap._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1>Sitemap</h1>
                 <div id="linkList" class="utility">
                     <div id="prodLinks">
                         <ul class="col" id="col1">
                            <li><a href="../biore-facial-cleansing-products">Our products <span class="arrow">&rsaquo;</span></a></li>
                            <li>
                                <h2 class="pie deepCleansing"><span class="subNavHeadlines"><span class="greenBold">DeepPOREfection:</span><br />Oily Skin </span></h2>
                                <ul>
                                    <li class="greenHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/deep-porefection-oily-skin/deep-pore-charcoal-cleanser") %>" id="A3">Deep Pore Charcoal Cleanser</a></li>
                                                <li class="greenHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/deep-porefection-oily-skin/pore-penetrating-charcoal-bar") %>" id="topnav-pore-penetrating-charcoal-bar">Pore Penetrating Charcoal Bar</a></li>
                                                
                                                <li class="greenHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/deep-porefection-oily-skin/charcoal-pore-minimizer") %>" id="topnav-charcoal-pore-minimizer">Charcoal Pore Minimiser</a></li>

                                                <li class="greenHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/deep-porefection-oily-skin/pore-unclogging-scrub") %>" id="topnav-pore-unclogging-scrub">Pore Unclogging Scrub</a></li>
                                             
                                                <li class="greenHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/deep-porefection-oily-skin/self-heating-one-minute-mask") %>" id="A4">Self-Heating One Minute Mask</a></li>
                                                 <li class="greenHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/deep-porefection-oily-skin/daily-deep-pore-cleansing-wipes") %>" id="topnav-daily-cleansing-cloths">Daily Deep Pore Cleansing Wipes</a></li>
                                </ul>

                            <h2 class="pie smallerPores"><span class="subNavHeadlines"><span class="greenBold">DeepPOREfection:</span> <span class="tealBold">Combination Skin</span> </span></h2>
                                <ul>
                                     <li class="tealHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/deep-porefection-combination-skin/baking-soda-pore-cleanser") %>">Baking Soda Pore Cleanser</a></li>
                                        <li class="tealHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/deep-porefection-combination-skin/baking-soda-cleansing-scrub") %>">Baking Soda Cleansing Scrub</a></li>
                                </ul>

                                <h2 class="pie justTakeItOff"><span class="subNavHeadlines">Just take it <span class="purpleBold">all off!</span></span></h2>
                                <ul>
                                    <li class="purpleHover"><a href="../take-it-all-off/baking-soda-cleansing-micellar-water">Baking Soda Cleansing Micellar Water</a></li>
                                    <li class="purpleHover"><a href="../take-it-all-off/charcoal-cleansing-micellar-water">Charcoal Cleansing Micellar Water</a></li>
                                                
                                </ul>
                            </li>
                        </ul>
                        <ul class="col" id="col2">
                            <li>
                                <h2 class="pie complexionClearing"><span class="subNavHeadlines"><span class="orangeBold">Acne's Outta Here</span></span></h2>
                                <ul>
                                    <li class="orangeHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/acnes-outta-here/charcoal-acne-clearing-cleanser") %>" id="A6">Charcoal Acne Clearing Cleanser - For Oily Skin</a></li>
                                    <li class="orangeHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/acnes-outta-here/charcoal-acne-scrub") %>" id="A1">Charcoal Acne Scrub - For Oily Skin</a></li>
                                    <li class="orangeHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/acnes-outta-here/baking-soda-acne-scrub") %>" id="topnav-blemish-treating-astringent">Baking Soda Acne Scrub - For Combination Skin</a></li>
                                               
                                    <li class="orangeHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/acnes-outta-here/triple-action-toner") %>" id="A2">Triple Action Toner</a></li>
                                </ul>
                                <h2 class="pie murt"><span class="subNavHeadlines"><span class="redBold">Break up With Blackheads</span></span></h2>
                                <ul>
                                    <li class="redHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/breakup-with-blackheads/warming-anti-blackhead-cleanser") %>" id="topnav-warming-anti-blackhead-cleanser">Warming Anti-Blackhead Cleanser</a></li>
 									<li class="redHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/breakup-with-blackheads/pore-strips#regular") %>" id="topnav-pore-strips-regular">Deep Cleansing Pore Strips 6ct</a></li>
                                    <li class="redHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/breakup-with-blackheads/charcoal-pore-strips#regular") %>" id="A5">Deep Cleansing Charcoal Pore Strips 6ct</a></li>
									<li class="redHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/breakup-with-blackheads/pore-strips#ultra") %>" id="topnav-pore-strips-ultra">Ultra Deep Cleansing Pore Strips 6ct</a></li>
									<li class="redHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/breakup-with-blackheads/pore-strips#combo") %>" id="topnav-pore-strips-combo">Combo Deep Cleansing Pore Strips 14ct</a></li>
									<li class="redHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/breakup-with-blackheads/pore-strips#ultracombo") %>" id="topnav-pore-strips-ultra-combo">Combo Deep Cleansing Pore Strips<br /><span class="topPadding">with Ultra Nose Strips</span></a></li>
                                </ul>
                                <ul>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <ul class="col noPadding" id="col3">
                        <li>
                            <a href="../pore-care/">Pore Care <span class="arrow">&rsaquo;</span></a>
                  
                        </li>
                     
                        <li><a href="http://www.kaobrands.com/" target="_blank">Kao Brands Company <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../contact-us">Contact Us <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../legal">Legal <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../privacy_policy">Privacy Policy <span class="arrow">&rsaquo;</span></a></li>
                     </ul>
                 </div>
                 <div id="photo"></div>
            </div>
        </div>
    </div>
</asp:Content>