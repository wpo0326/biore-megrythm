﻿<%@ Page Title="Free Your Pores – Get Clean, Healthy Skin – Browse All Products  | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.our_products.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Bior&eacute;&reg; Skincare products cleanse and gently exfoliate, giving you healthy, clean skin! See the complete Bior&eacute;&reg; Skincare product line." name="description" />
    <meta content="Clear Skin, Biore&reg; Products" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>
    <meta name="viewport" content="width=device-width">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="intro">
                <h1>Our Products</h1>
                <div id="responseRule"></div>
                <p>Targets the cause of all skin problems &mdash; the clogged pore. The powerful, pore–cleansing Bioré<sup>&reg;</sup> products come in liquid, foam, scrub, wipes and strip forms so you can remove pore–clogging debris! Go from school to the library, home to beach, and from work to working out, with skin that's clear and healthy-looking. Because when you clean the pore, you clear the problem.</p>
            </div>

             <div id="takeitalloffProds" class="prodList">
                <h2 class="pie roundedCorners takeitalloff">Just take it <span>all off!</span>
                    <span></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
 
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-cleansing-micellar-water.png" alt="" />
                        <asp:Literal ID="litBSMicellarWater" runat="server" />
                        <h3>BIORÉ® BAKING SODA<br />CLEANSING MICELLAR WATER</h3>
                        <p>Removes makeup, deep cleans pores and balances without over-drying.</p>
                        <a href="../take-it-all-off/baking-soda-cleansing-micellar-water" id="details-baking-soda-cleansing-micellar-water">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-cleansing-micellar-water.png" alt="" />
                        <asp:Literal ID="litCharcoalMicellarWater" runat="server" />
                        <h3>BIORÉ® CHARCOAL CLEANSING<br />MICELLAR WATER</h3>
                        <p>Removes makeup, deep cleans pores and removes excess oil. </p>
                        <a href="../take-it-all-off/charcoal-cleansing-micellar-water" id="details-charcoal-cleansing-micellar-water">details ></a>
                    </li>
                </ul>
            </div>

            <div id="deepCleansingProds" class="prodList">
                <h2 class="pie roundedCorners deepCleansing"><span>DeepPOREfection:</span> <span class="break">Oily Skin</span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CHARCOAL_100X235.png" alt="" />
                         <asp:Literal ID="litDeepPoreCharcoalCleanser" runat="server" />
                        <h3>Deep Pore<br />Charcoal Cleanser</h3>
                        <p>Deep cleans 2x better* & naturally purifies.</p>
                        <a href="../deep-porefection-oily-skin/deep-pore-charcoal-cleanser" id="A1">details ></a>
                    </li>

                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalBar.png" alt="" />
                         <asp:Literal ID="litCharcoalBar" runat="server" />
                        <h3>Pore Penetrating <br />Charcoal Bar</h3>
                        <p>Deep cleans & gently exfoliates for 2.5x cleaner pores*.</p>
                        <a href="../deep-porefection-oily-skin/pore-penetrating-charcoal-bar" id="A6">details ></a>
                    </li> 

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreMinimizer.png" alt="" />
                        <asp:Literal ID="litCharcoalPoreMinimizer" runat="server"></asp:Literal>
                        <h3>Charcoal<br />Pore Minimizer</h3>
                        <p>Gently exfoliates & deep cleans to instantly reduce the appearance of pores.	</p>
                        <a href="../deep-porefection-oily-skin/charcoal-pore-minimizer" id="details-charcoal-pore-minimizer">details ></a>
                    </li>
                    
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/poreUncloggingScrub.png" alt="" />
                        <asp:Literal ID="litPoreUncloggingScrub" runat="server" />
                        <h3>PORE UNCLOGGING <br />SCRUB</h3>
                        <p>Deep cleans pores & smoothes skin.</p>
                        <a href="../deep-porefection-oily-skin/pore-unclogging-scrub" id="A3">details ></a>
                    </li>      

                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/SHM_100X235.png" alt="" />
                        <asp:Literal ID="litSelfHeatingOneMinuteMask" runat="server" />
                        <h3>Self Heating<br />One Minute Mask</h3>
                        <p>Purifies pores 2.5x better*.</p>
                        <a href="../deep-porefection-oily-skin/self-heating-one-minute-mask" id="A2">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/dailyCleansingCloths.png" alt="" />
                        <asp:Literal ID="litDailyCleansingCloths" runat="server" />
                        <h3>DAILY DEEP PORE CLEANSING WIPES</h3>
                        <p>Deep cleans pores & exfoliates skin in one wipe.</p>
                        <a href="../deep-porefection-oily-skin/daily-deep-pore-cleansing-wipes" id="details-daily-cleansing-cloths">details ></a>
                    </li> 
                    
                </ul>
            </div>

            <div id="bigPoresProds" class="prodList">
                <h2 class="pie roundedCorners bigPores">DeepPOREfection: <span>Combination Skin</span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-pore-cleanser.png" alt="" />
                         <asp:Literal ID="litBakingSodaPoreCleanser" runat="server" />
                        <h3>Baking Soda<br />Pore Cleanser</h3>
                        <p>Deep cleans pores & gently exfoliates.</p>
                        <a href="../deep-porefection-combination-skin/baking-soda-pore-cleanser" id="A4">details ></a>
                    </li>
		            <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-cleansing-scrub.png" alt="" />
                            <asp:Literal ID="litBakingSodaCleansingScrub" runat="server" />
                        <h3>Baking Soda<br />Cleansing Scrub</h3>
                        <p>Activates with water for a deep clean and gentle exfoliation.</p>
                        <a href="../deep-porefection-combination-skin/baking-soda-cleansing-scrub" id="A5">details ></a>
                    </li>
                </ul>
            </div>

            <div id="complexionClearingProds" class="prodList">
                <h2 class="pie roundedCorners complexionClearing">Acne's Outta Here
                    <span></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                      <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-acne-clearing-cleanser.png" alt="" style="bottom:155px;" />
                        <asp:Literal ID="litCharcoalAcneClearingCleanser" runat="server" /> 
                        <h3>CHARCOAL ACNE<br />CLEARING CLEANSER</h3>
                        <p>Penetrates pores and absorbs excess oil to fight spots and prevent future breakouts.</p>                       
                        <a href="../acnes-outta-here/charcoal-acne-clearing-cleanser" id="A7">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-acne-scrub.png" alt="" style="bottom:155px;" />
                        <asp:Literal ID="litCharcoalAcneScrub" runat="server" /> 
                        <h3>CHARCOAL<br />ACNE SCRUB</h3>
                        <p>Smooths away acne causing dirt and absorbs excess oil to help eliminate breakouts.</p>              
                        <a href="../acnes-outta-here/charcoal-acne-scrub" id="details-charcoal-acne-scrub">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-acne-scrub.png" alt="" style="bottom:155px;" />
                        <asp:Literal ID="litBakingSodaAcneScrub" runat="server" /> 
                        <h3>BAKING SODA<br />ACNE SCRUB</h3>
                        <p>For Combination Skin. Unclogs pores and balances skin to reduce breakouts.</p>                  
                        <a href="../acnes-outta-here/baking-soda-acne-scrub" id="details-baking-soda-acne-scrub">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishTreatingAstringentToner.png" alt="" />
                        <asp:Literal ID="litBlemishAstringent" runat="server" />
                        <h3>TRIPLE ACTION<br />TONER</h3>
                        <p>Help prevent breakouts with an oil-free toner.</p>
                        <a href="../acnes-outta-here/triple-action-toner" id="details-blemish-fighting-astringent">details ></a>
                    </li>

                  
                </ul>
            </div>




            <div id="murtProds" class="prodList">
                <h2 class="pie roundedCorners murt"><span>Breakup with Blackheads</span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/warmingAntiBlackheadCleanser.png" alt="" />
                        <asp:Literal ID="litBlackheadCleanser" runat="server" />
                        <h3>Warming Anti-<br />Blackhead Cleanser</h3>
                        <p>Heats up to open pores & target blackheads. </p>
                        <a href="../breakup-with-blackheads/warming-anti-blackhead-cleanser" id="details-deep-cleansing-blackhead-cleanser">details ></a>
                    </li>
                    
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStrips.png" alt="" />
                        <asp:Literal ID="litPoreStrips" runat="server" />
                        <h3>DEEP CLEANSING<br />PORE STRIPS</h3>
                        <p>Unclogs pores & achieves the deepest clean. </p>
                        <a href="../breakup-with-blackheads/deep-cleansing-pore-strips" id="details-deep-cleansing-pore-strips">details ></a>
                    </li>
                    
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltra.png" alt="" />
                        <asp:Literal ID="litPoreStripsUltra" runat="server" />
                        <h3>Ultra Deep Cleansing Pore Strips</h3>
                        <p>With Witch Hazel & <br />Tea Tree Oil.</p>
                        <a href="../breakup-with-blackheads/deep-cleansing-pore-strips-ultra" id="details-deep-cleansing-pore-strips-ultra">details ></a>
                    </li>                 
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.png" alt="" />
                        <asp:Literal ID="litPoreStripsCombo" runat="server" />
                        <h3>Combo Deep Cleansing<br /> Pore Strips</h3>
                        <p>7 nose strips & 7 face strips</p>
                        <a href="../breakup-with-blackheads/deep-cleansing-pore-strips-combo" id="details-deep-cleansing-pore-strips-combo">details ></a>
                    </li>

                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" />
                        <asp:Literal ID="litCharcoalStrips" runat="server" />
                        <h3>Charcoal Deep <br /> Cleansing Pore Strips</h3>
                        <p>Unclogs pores & draws out excess oil for the deepest clean.</p>
                        <a href="../breakup-with-blackheads/charcoal-pore-strips" id="details-deep-cleansing-pore-strips-face">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltraCombo.png" alt="" />
                        <asp:Literal ID="litPoreStripsFace" runat="server" />
                        <h3 class="combo">COMBO DEEP CLEANSING PORE STRIPS WITH ULTRA NOSE STRIPS</h3>
                        <p>Removes 2x more deep-down clogged pores.*</p>
                        <a href="../breakup-with-blackheads/deep-cleansing-pore-strips-ultra-combo" id="details-deep-cleansing-pore-ultra-combo">details ></a>
                    </li>
                  
                   
                </ul>
            </div>
            
             
            
            <div style="font-size:9px; line-height:10px; margin:20px 0;">* Bioré<sup>&reg;</sup> Ultra Deep Cleansing Pore Strips remove 2x more deep pore clogs than original Bioré®  Deep Cleansing Pore Strips</div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
