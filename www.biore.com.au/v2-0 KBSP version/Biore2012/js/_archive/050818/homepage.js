﻿$(document).ready(function () {

    var windowSize = $(window).width();
    var mobileSize = 768;
    var playBtn = $('.animation-play-btn');
    var playButtonSize = playBtn.css('width');
    $(window).resize(function () {
        playBtn.attr('style', '');
        playButtonSize = playBtn.css('width');
    });
    function isMobile() {
        if (navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)
        ) {
            return true;
        }
        else {
            return false;
        }
    }
    
    $(window).resize(function () {
        var newSize = $(window).width();
        windowSize = newSize;

        removeLightBox();
                
    });

    function productAnimation(eventName, target) {

        var imageElement = target.find('.gif-holder');
        var playBtn = target.find('.animation-play-btn');
        var imgGif = imageElement.attr('data-animate');
        var imgCover = imageElement.attr('data-origin');
        var skin = target.find('.animation-skin');

        //var currentWidthPlayBtn = playBtn.css('width');
        var newWidthPlayBtn = parseInt(playButtonSize) + 10;
        var originalWidthPlayBtn = parseInt(newWidthPlayBtn) - 10;
        if (eventName == 'mouseenter') {            
            skin.slideUp();
            if (!playBtn.hasClass('overwritte-btn')) {
                playBtn.fadeOut('slow');
            } else {
                playBtn.css('width', newWidthPlayBtn);
            }
            imageElement.fadeOut(0, function () {
                imageElement.attr('src', imgGif);
                imageElement.fadeIn(0);
            });
        } else {
            skin.slideDown();
            if (!playBtn.hasClass('overwritte-btn')) {
                playBtn.fadeIn('slow');
            } else {
                playBtn.css('width', originalWidthPlayBtn);
            }
            imageElement.fadeOut(0, function () {
                imageElement.attr('src', imgCover);
                imageElement.fadeIn(0);
            });
        }
    }

    function removeLightBox() {

        if (windowSize > mobileSize) {
            if (!$('.fbm').hasClass('fancybox-media')) {
                $('.fbm').addClass('fancybox-media');
            }            
        } else {
            if ($('.fbm').hasClass('fancybox-media')) {
                $('.fbm').removeClass('fancybox-media');
            }            
        }                
    }


    /*$('a[href*=#]:not([href=#])').click(function (event) {
        alert("ahh!");
        event.preventDefault();
        console.log('Alvaro');
        if (windowSize > mobileSize) {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        }
    });*/

    removeLightBox();

    $('.animation').on('mouseenter mouseleave', '.animation-container', function (event) {
        if (windowSize > mobileSize) {
            productAnimation(event.type, $(this));
        }
    });
    
    $('.fancybox-media')
		.attr('rel', 'media-gallery')
		.fancybox({
		    transitionIn: 'fade',
		    transitionOut: 'fade',
		    margin: 100,

		    arrows: false,
		    helpers: {
		        media: {},
		        buttons: {}
		    }
		});

    //$.fancybox({      
    //    'margin': 100,
    //    'transitionIn': 'fade',
    //    'transitionOut': 'fade',
    //    'type': 'iframe',
    //    'href': 'https://www.youtube.com/embed/TVBZqfDfAgE?rel=0&autoplay=1'
    //});

    if (isMobile()) {
        $('#welcomeVideo').css('display', 'none');
        $('#welcomeImage').css('display', 'block');
    }

    $('body').find('[name="google_conversion_frame"]').css('display', 'none');

    $(window).load(function () {
        //$('#arrowBounce').effect('bounce', { times: 3 }, 6000);
    });

 var viewport = {
            width: $(window).width(),
            height: $(window).height()
        };

        var loaded = false;

        if (viewport.width > 800 && loaded == false) {
            var section5img = new Image();
            section5img.src = '/images/homepage/gif/output.gif';
            section5img.onload = function() {
                document.getElementById('imganim').src = section5img.src;
            }
            loaded = true;
        }

});