﻿
// Run on document ready

$(function() {
    $('a.videoLink').ENLvideo({
        flashPrefix: '../../flash/',                    /* Flash player relative to this file */
        flashVideoPrefix: '',  /* relative from the player URL to the video link URL on the page */
        lightbox: false,
        hideVideoLink: true,
        lightboxOverlayOpacity: 0
    });
});

/* Flash Tracking */
function riaTrack(trackingParam) {
    goToPage(trackingParam);
}

function goToPage(pg) {
    firstTracker._trackPageview(pg);
    secondTracker._trackPageview(pg);
}

