﻿<%@ Page Title="About Bior&eacute;&reg; Skincare | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.about_us._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Learn more about Bioré® Skincare" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
                .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <div id="polaroids">
                    <img src="../images/about/productGrouping.jpg" alt="" />
                </div>
                <div id="content">
                    <h1>About Bior&eacute;<sup>&reg;</sup> Skincare</h1>
                    <p>
                        <img src="../images/about/productGroupingSmall.jpg" alt="" id="productGroupingSmall" />
                        From deep cleansing to complexion clearing and even make-up removing, Bior&eacute;<sup>&reg;</sup> products help keep your skin clean and beautiful. Go from work to working out, then out for some weekend fun with skin that's always ready.
                    </p>                    
                    <p class="imageText"><img src="../images/about/availableAtStores.png" alt="Bior&eacute;&reg; Skincare products are available at select food, drug and mass merchant stores." /></p>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

