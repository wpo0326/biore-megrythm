﻿<%@ Page Title="" Language="C#" MasterPageFile="~/facebook/FBContentPageMaster.Master"
	AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Biore2012.facebook.porespectives2.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div id="fb-root">
	</div>

	<script type="text/javascript">
		var extJsHost_connect_facebook_net = (("https:" == document.location.protocol) ? "https://" : "http://");
		window.fbAsyncInit = function() {
			FB.init({
				appId: '<%= System.Configuration.ConfigurationManager.AppSettings["porespectiveFBAppId"] %>', // App ID
				//channelURL : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
				status: true, // check login status
				cookie: true, // enable cookies to allow the server to access the session
				oauth: true, // enable OAuth 2.0
				xfbml: true  // parse XFBML
			});

			// Additional initialization code here
			FB.Canvas.setAutoGrow();
			FB.Canvas.scrollTo(0, 0);
		};

		// Load the SDK Asynchronously
		(function(d) {
			var js, id = 'facebook-jssdk'; if (d.getElementById(id)) { return; }
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = extJsHost_connect_facebook_net + "connect.facebook.net/en_US/all.js";
			d.getElementsByTagName('head')[0].appendChild(js);
		} (document));
	</script>

	<div class="pores">
		<asp:Panel ID="panLikeOverlay" CssClass="likeOverlay" runat="server" Visible="true">
			<h1>Like our page</h1>
			<p>to see what others are saying about our fresh, new packaging design&mdash;and to
				add your own "pore-spective" to the conversation.</p>
		</asp:Panel>
		<asp:Panel ID="panUserLikes" CssClass="userLikes" runat="server" Visible="false">
			<h1>See our new look and share your pore-spective</h1>
			<p><strong>Say "goodbye" to green&mdash;</strong>we're stepping up the style of Bior&eacute;<sup>&reg;</sup>
				skincare products with fresh white and blue hues! Same effective invigorating formulas
				inside, a strikingly fresh makeover outside.</p>
			<p>Scroll down to see what some of our selected fans are saying about the sneak peek
				they receive of our new packaging, then add your own <span class="no-wrap">pore-spective</span>
				to the conversation.</p>
			<h2>Pore-spectives</h2>
			<dl id="userComments">
				<dd>"I love the new packaging, nice blue color, clean and clear to read. Love love love
					the new look!"</dd>
				<dt class="user">Mary Ann Z., Bay City, MI</dt>
				<dt>Sneak Peak Reviewer and Product Recipient</dt>
				<dd>"I like the clean (pun intended) look of the box. It's simple, yet different, and
					makes me want to pick it up from the shelf."</dd>
				<dt class="user">Amanda A., Hanahan, SC</dt>
				<dt>Sneak Peak Reviewer and Product Recipient</dt>
				<dd>"I have always associated the color green but this new look is fresh and clean...I
					like it!"</dd>
				<dt class="user">Rose P., Anaheim, CA</dt>
				<dt>Sneak Peak Reviewer and Product Recipient</dt>
				<dd>"It's clean, simple, and pretty...Easy to read and highlights the important bits."</dd>
				<dt class="user">Shelly R., Mansfield, IL</dt>
				<dt>Sneak Peak Reviewer and Product Recipient</dt>
				<dd>"Nice clean design. Familiar enough that I can still find it on the shelf, but new
					enough to be refreshing. I APPROVE!"</dd>
				<dt class="user">Sarah G., Goshen, NY</dt>
				<dt>Sneak Peak Reviewer and Product Recipient</dt>
			</dl>
			<div id="shareNewComment">
				<label for="tbComment">Join the conversation: Tell us what you think of our fresh, new
					look!</label>
				<asp:TextBox ID="tbComment" CssClass="tbComment" TextMode="MultiLine" runat="server" />
				<asp:LinkButton ID="submitBtn" runat="server" CssClass="submitBtn">Submit</asp:LinkButton>
				<asp:RequiredFieldValidator ID="reqComment" Display="Dynamic" CssClass="tbError"
					SetFocusOnError="True" EnableClientScript="true" ControlToValidate="tbComment"
					Text="You must enter a Pore-spective to submit." runat="server" />
				<asp:RegularExpressionValidator ID="reqCommentLength" runat="server" CssClass="tbError"
					SetFocusOnError="True" ErrorMessage="Your Pore-spective must be less than 400 characters."
					Display="Dynamic" EnableClientScript="true" ControlToValidate="tbComment" ValidationExpression="^[\s\S]{1,400}$"></asp:RegularExpressionValidator>
				<asp:RegularExpressionValidator ID="reqCommentSpecChars" runat="server" Display="Dynamic"
					CssClass="tbError" ValidationExpression="^[^<>]+$" ControlToValidate="tbComment"
					EnableClientScript="true" SetFocusOnError="true" ErrorMessage="The message cannot contain the < or > symbols."></asp:RegularExpressionValidator>
			</div>
			<h3 id="tagline">Bior&eacute;<sup>&reg;</sup>. Get your best clean.</h3>
		</asp:Panel>
		<asp:Panel ID="panNotFacebook" CssClass="notFacebook" runat="server" Visible="false">
			<h1>See our new look and share your pore-spective</h1>
			<p><strong>Say "goodbye" to green&mdash;</strong>we're stepping up the style of Bior&eacute;<sup>&reg;</sup>
				skincare products with fresh white and blue hues! Same effective invigorating formulas
				inside, a strikingly fresh makeover outside.</p>
			<p>Please go to the <a href="<%=System.Configuration.ConfigurationManager.AppSettings["porespectiveFBTabUrl"] %>">
				Bior&eacute;<sup>&reg;</sup> Facebook page</a> to see what others are saying about
				our fresh, new packaging design&mdash;and to add your own <span class="no-wrap">"pore-spective"</span>
				to the conversation.</p>
		</asp:Panel>
	</div>
	<%=SquishIt.Framework.Bundle .JavaScript()
		.Add("js/main.js")
		.Render("js/combinedpores_#.js")
	%>
	<script>
		// Assign Facebook vars from AppSettings
		ps2.fbVars = {
			"FBAppID": '<%= System.Configuration.ConfigurationManager.AppSettings["porespectiveFBAppId"] %>',
			"FBShareUrl": '<%= System.Configuration.ConfigurationManager.AppSettings["porespectiveFBShareUrl"] %>',
			"FBSharePic": '<%= System.Configuration.ConfigurationManager.AppSettings["porespectiveFBSharePic"] %>',
			"FBShareName": '<%= System.Configuration.ConfigurationManager.AppSettings["porespectiveFBShareName"] %>',
			"FBShareCaption": '<%= System.Configuration.ConfigurationManager.AppSettings["porespectiveFBShareCaption"] %>',
			"FBShareDesc": '<%= System.Configuration.ConfigurationManager.AppSettings["porespectiveFBShareDesc"] %>',
			"FBShareMsg": '<%= System.Configuration.ConfigurationManager.AppSettings["porespectiveFBShareMsg"] %>',
			"FBShareRedirUrl": '<%= System.Configuration.ConfigurationManager.AppSettings["porespectiveFBShareRedirUrl"] %>'
		}		
	</script>
</asp:Content>
