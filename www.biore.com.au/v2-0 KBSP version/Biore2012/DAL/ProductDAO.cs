﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Biore2012.BLL;
using System.Xml.Serialization;
using System.IO;
using System.Web.Caching;

namespace Biore2012.DAL
{
    public class ProductDAO
    {
        public static List<Product> GetAllProducts()
        {
            List<Product> fileContent = (List<Product>)HttpContext.Current.Cache["ProductFile"];
            if (fileContent == null)
            {
                string src = System.Configuration.ConfigurationManager.AppSettings["XMLDataPath"] + System.Configuration.ConfigurationManager.AppSettings["ProductXMLFile"];
                
                XmlSerializer mySerializer = new XmlSerializer(typeof(List<Product>), new XmlRootAttribute("Products"));
                FileStream myFileStream = new FileStream(src, FileMode.Open, FileAccess.Read);
                try
                {
                    fileContent = (List<Product>)mySerializer.Deserialize(myFileStream);
                }
                catch (Exception)
                {

                    throw;
                }
                finally {
                    myFileStream.Close();
                }

                HttpContext.Current.Cache.Insert("ProductFile", fileContent, new CacheDependency(src));
                //HttpContext.Current.Response.Write("updated" + DateTime.Now.ToString() + "<BR/>");
            }
            return fileContent;
        }
        
        public static Product GetValidProductByProductRefName(string ProductRefName)
        {
            return GetProductByProductRefName(ProductRefName, false);
        }
		
		public static Product GetDiscontinuedProductByProductRefName(string ProductRefName)
		{
            return GetProductByProductRefName(ProductRefName, true);
        }

        public static Product GetProductByProductRefName(string ProductRefName, bool isDiscontinued) {
			Product p = new Product();
			foreach (Product item in GetAllProducts())
			{
                if (isDiscontinued)
                {
                    if (item.ProductRefName.Equals(ProductRefName))
                    {
                        p = item;
                        break;
                    }
                }
                else
                {
                    if (item.ProductRefName.Equals(ProductRefName))
                    {
                        p = item;
                        break;
                    }
                }
				
			}
			return p;
		}

        public static Product GetProductByURLName(string productID)
        {
            Product p = new Product();
            foreach (Product item in GetAllProducts())
            {
                // Make check case insensitive
                if (item.Name.ToLower().Equals(productID.ToLower()))
                {
                    p = item;
                    break;
                }
            }
            return p;
        }
      
        public static List<Product> GetProductListByRefName(List<string> ProductsList)
        {
            List<Product> prodList = new List<Product>();
            foreach (string prodRefName in ProductsList)
            {
                Product p = ProductDAO.GetValidProductByProductRefName(prodRefName);
                if (p.ProductRefName != null)
                {
                    prodList.Add(p);
                }
            }
            return prodList;
        }

        public static List<Product> GetProductsForBazaarVoice()
        {
            List<Product> validProducts = new List<Product>();
			foreach (Product item in GetAllProducts())
			{
                    validProducts.Add(item);
			}
            return validProducts;
        }

    }
}
