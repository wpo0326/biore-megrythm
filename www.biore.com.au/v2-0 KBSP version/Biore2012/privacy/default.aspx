﻿<%@ Page Title="Privacy | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.privacy._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id=“mainContent” style=“min-height:730px”>
            <div id="shadow"></div>
            <div class="centeringDiv">
            <br /><br />
                 <h1>Privacy Policy</h1>
                 <h2>Kao Australia Pty. Limited</h2><br />
                 <p>At Kao, we respect your privacy and take reasonable measures to protect your privacy. This policy applies only to the web site on which it appears, and does not apply to any other Kao web sites or services. By accessing or using this web site, you agree and consent to our use of your information as described in this policy. </p><br />
                 <h2>Effective Date </h2><br />
                 <p>This privacy policy is effective and was last updated on 17/03/2011 </p><br />
                 <h2>What information we collect about you</h2><br />
                 <p><strong>Personally Identifiable Information</strong></p>

<p>This web site is structured so that, in general, you can visit without revealing any personally identifiable information. Once you choose to provide us with such information, you may be assured that it will only be used in accordance with the principles set forth in this document.  </p><br />

<p>On some Kao web pages, you can interact with entry forms for the purposes of contacting us, subscribing to communications, and other promotional events. The types of personally identifiable information collected at these pages may include name, mailing address and email address. We may also ask you to voluntarily provide us with information regarding your personal or professional interests, demographics, experience with our products, and contact preferences. </p><br />

<p>We do not collect any personally identifiable information about you unless you provide it to us. If you email us or answer questions we have placed on this web site, you are voluntarily releasing that information to us for use as set forth in this policy. Please do not submit personally identifiable information to us if you do not want us to have this information in our database. </p><br />

<p><strong>Why we collect your personally identifiable information</strong></p>
<p>Kao collects your personally identifiable information for the primary purpose of responding to your queries and requests for information about us. We may also collect personally identifiable information for purposes related or ancillary to the main reasons we collect it, including to:<br />
<br />
• provide services to you, such as to send you communications that you have subscribed to;<br />
• gain an understanding of your needs, in order for us to provide you with better, more personalized services;<br />
• improve and develop our services, including to conduct market research and surveys;<br />
• to assess customer satisfaction; and<br />
• to otherwise communicate with you, including to send you marketing material or invitations to promotional events.</p><br />

<p><strong>Performance Information</strong></p>
<p>Kao may record your interactions with our advertisements, our web sites, emails or other applications we provide using Clickstream Data and Cookies. "Clickstream Data" is a recording of what you click on while browsing the internet. This data can tell us the type of computer and browsing software you use and the address of the web site from which you linked to the Site. This information may be collected and stored by a web site's server such as ours.</p> <br />

<p>"Cookies" are small text files that are placed on your computer by a web site for the purpose of facilitating and enhancing your communication and interaction with that web site and collecting aggregate information. Many web sites, including ours, use cookies for these purposes. You may stop or restrict the placement of cookies on your computer or flush them from your browser by adjusting your web browser preferences and browser plug-in settings, in which case you may still use our web site, but it may interfere with some of its functionality. </p><br />

<p>Kao does not attempt to collect sensitive data such as social security or credit card numbers. Furthermore, Kao has no intention of collecting personally identifiable information from individuals under the age of 18. Kao will maintain procedures to assure that personally identifiable information about individuals under the age of 18 is only collected with explicit consent of such child's parent or guardian. If an individual under the age of 18 has provided Kao with personally identifiable information without such consent, Kao asks that a parent or guardian of the individual contact Kao using our Contact Us page to inform us of that fact. Kao will use reasonable efforts to delete such information from our database.</p><br />

<h2>How we use your information</h2><br />
<p>Kao uses and discloses your information for the primary purpose for which it was collected stated above, and for purposes related or ancillary to the main reasons we collect it, including to understand your needs and provide better products and services. Kao may combine your personal and behavioral data to personalize your web site experience, tailor future communications, and send you targeted offers. You consent to Kao using your information to contact you on an ongoing basis for market research or to provide you with marketing information we think would be of particular interest, including by mail, email, SMS and telephone. We will always give you the opportunity to opt out of receiving such contacts.</p><br />
<p>You agree that Kao may share the personally identifiable information you provide online with other Kao divisions or affiliates. </p>
<br />
<p>At times, personally identifiable information may be collected from you on behalf of KBa and a third party who is identified at the time of collection. In such instances, you agree that your personally identifiable information may be provided to both Kao and such third party. While Kao will use your personally identifiable information as set forth in this policy, such third party will use your personally identifiable information as set forth in their own privacy policy. Therefore, you should review such policies prior to providing your personally identifiable information. Kao is not responsible for the actions of such third parties.  </p><br />
<p>
Kao may permit its vendors, subcontractors and service providers to access your personally identifiable information, but they are only permitted to do so in connection with services they are performing for Kao. They are not authorized by Kao to use the information for their own benefit.  </p><br />
<p>Kao may disclose personally identifiable information as required by law or legal process. Kao may disclose personally identifiable information to investigate suspected fraud, harassment or other violations of any law, rule or regulation, or the terms or policies for the web site and where we are otherwise required, authorized or permitted by law to do so. </p><br />
<p>In the event of a sale, merger, liquidation, dissolution, reorganization or acquisition of Kao, or a Kao business unit, information Kao has collected about you may be sold or otherwise transferred. You agree that the party acquiring the information may use and disclose personally identifiable information about you, provided that they agree to do so in accordance with this policy. </p><br />
<p>You consent to Kao disclosing your personally identifiable information to entities in jurisdictions outside the country in which you reside, on the basis that Kao is not required to ensure that such recipients comply with the privacy laws applicable in your country.</p><br />


<h2>Targeted Content and Messaging</h2><br />
<p>We believe that content, messages and advertising are more relevant and valuable to you when they are based upon your interests, needs and demographics. Therefore, we may combine your personally identifiable information with aggregate information collected about you to deliver content, messages and advertising specifically to you that are based upon your prior activities on the web or information provided. For example, if you have previously expressed an interest in hair care products through your activities on our web site, we may deliver more advertisements to you about hair care products than other products for which you have not expressed an interest or interacted with on the web site. While we may use these profiles to tailor what we deliver to you, we will still handle and secure your personally identifiable information as set forth in this policy.
 </p><br />
<h2>Your choice</h2><br />
<p>Kao will not use or share the information provided to us online in ways unrelated to the ones described above without letting you know and offering you a choice. </p><br />
<h2>Security</h2><br />
<p>Kao is committed to taking reasonable steps to ensure the security of the information we collect. To prevent unauthorized access, maintain data accuracy, and ensure the appropriate use of personally identifiable information, we have put in place appropriate physical, electronic, and managerial procedures to safeguard and secure the personally identifiable information we collect online. If we provide you a direct means for maintaining your information in our database, such as through a "profile center" application, you are responsible for taking reasonable precautions to protect your login data, such ID and password. Kao will not be responsible for breach that results from a lost or stolen login. </p><br />
<h2>Non-confidential</h2>
<br />
<p>Any communication or material you transmit to us by email or otherwise, including any data, questions, comments, suggestions, or the like is, and will be treated as, non-confidential and nonproprietary. Except to the extent expressly covered by this policy, you agree that anything you transmit or post may be used by us for any purpose, including but not limited to, reproduction, disclosure, transmission, publication, broadcast and posting. You also agree that we are free to use any ideas, concepts, know-how, or techniques contained in any communication you send to us without compensation and for any purpose whatsoever, including but not limited to, developing, manufacturing and marketing products and services using such information. Furthermore, you confirm that any media files you submit to us (eg. pictures, videos, etc) are your property and you have permission of anyone depicted in these files to provide them to Kao. </p><br />
<h2>Access to and Correction of Information / Opt-Out</h2><br />
<p>Kao will maintain reasonable procedures for individuals to gain access to their personally identifiable information and preferences. Kao will correct any information that is inaccurate or incomplete or allow you to change your individual consent level. You may do so by contacting us with the relevant details for your request.  </p><br />
<p>If you wish for us to stop using your personally identifiable information and delete it from our list of active users, please contact us with the details necessary to complete your request. We will process and honor such requests within a reasonable period of time after receipt. However, even though we may remove your personally identifiable information from our list of active users, we are not responsible for removing your personally identifiable information from the lists of any third party who has been provided your information in accordance with this policy, such as a business partner. </p><br />
<h2>Comment and Questions</h2><br />
<p>If you have comments or questions about our privacy policy or your personally identifiable information, please use our Contact Us page. </p><br />
<h2>Australia</h2><br />
<p>This web site and our related databases are maintained in Australia. By using the web site, you freely and specifically give us your consent to collect and store, your information in Australia and to use your information as specified within this policy.</p><br />
<h2>Changes to Privacy Policy</h2><br />
                 <p>Kao will, and reserves the right to, modify and update this policy or its related business practices at any time at our discretion. Any such material changes will be posted here for reference. However, Kao will not make material changes to this policy, such as to whom it discloses personally identifiable information, without giving you a chance to opt-out of such differing uses or disclosures. </p><br /><br />
                 
            </div>
        </div>
    </div>
</asp:Content>