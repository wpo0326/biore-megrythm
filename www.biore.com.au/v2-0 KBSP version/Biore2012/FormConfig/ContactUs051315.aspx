﻿<%@ Page Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="Biore2012.FormConfig.ContactUs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <%#SquishIt.Framework.Bundle .Css()
        .Add("~/css/formConfig.css")
        .Render("~/css/combinedform_#.css")
    %>

</asp:Content>

<asp:Content ID="floodlightPixelsContent" ContentPlaceHolderID="floodlightPixels" runat="server" Visible="false">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <div class="responsive contact">
                    <div id="contactFormWrap">
                        <div id="BrandImageContainer" class="png-fix">
                            <img class="png-fix" src="images/forms/contactUsPhoto.jpg" style="border-width: 0px;" />
                        </div>
                        <div id="ContactFormContainer">
                            <asp:Panel ID="ContactDetails" runat="server">
                                <!-- Header -->
                                <div id="formHeader">
                                    <h1 id="ctl00_ContentPlaceHolder1_ucForm_PageHeader">
                                        Contact Us
                                    </h1>
                                </div>
                                <!-- Description -->
                                <div class="DescriptionContainer png-fix">
                                    Do you have a specific question or a comment?<br />
                                    We'd love to hear from you!
                                    <h2>
                                        By Mail</h2>
                                    <p>
                                        Consumer Care Dept<br />
                                        Kao Australia<br />
                                        Level 1, 19 Prospect St<br />
                                        Box Hill, Victoria 3128<br />
                                        </p>
                                    <h2>
                                        By Phone</h2>
                                    <p>
                                        <span>
                                        If you're calling from Australia, you can call us toll free at 1-800-468-318 between 9am - 5pm (AEST).<br />
                                        If you're calling from New Zealand, you can call us at +61 3 9897 1444 between 9am - 5pm (AEST).
                                        </span></p>
                                    <h2>
                                        Online</h2>
                                    <p>
                                        Fill out the form below and someone will get back to you as soon as possible.</p>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="ContactForm" runat="server">
                                <!-- From Fields -->
                                <div class="FormContainer">
                                    <p class="req png-fix">
                                        <em>Required*</em></p>
                                    <div class="MemberInfoContainer png-fix">
                                        <div class="NameWrapper">
                                            <div class="FirstNameContainer Question">
                                                <asp:Label ID="FNameLbl" runat="server" Text="First Name*" AssociatedControlID="FName"></asp:Label>
                                                <asp:TextBox ID="FName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="FNameValidator1" runat="server" ErrorMessage="Please enter your First Name."
                                                        ControlToValidate="FName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="FNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your First Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="FName" EnableClientScript="true"
                                                        SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="LastNameContainer Question">
                                                <asp:Label ID="LNameLbl" runat="server" Text="Last Name*" AssociatedControlID="LName"></asp:Label>
                                                <asp:TextBox ID="LName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="LNameValidator1" runat="server" ErrorMessage="Please enter your Last Name."
                                                        ControlToValidate="LName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="LNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Last Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="LName" SetFocusOnError="true"
                                                        Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="EmailWrapper">
                                            <div class="EmailContainer Question">
                                                <asp:Label ID="EmailLbl" runat="server" Text="Email*" AssociatedControlID="Email"></asp:Label>
                                                <asp:TextBox ID="Email" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="EmailValidator1" runat="server" ErrorMessage="Please enter your Email Address."
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="EmailValidator2" runat="server" ErrorMessage="Please enter a valid Email Address."
                                                        ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="ConfirmEmailContainer Question">
                                                <asp:Label ID="RetypeEmailLbl" runat="server" Text="Re-type Email*" AssociatedControlID="RetypeEmail"></asp:Label>
                                                <asp:TextBox ID="RetypeEmail" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="RetypeValidator1" runat="server" ErrorMessage="Please re-type your Email Address."
                                                        ControlToValidate="RetypeEmail" EnableClientScript="true" SetFocusOnError="true"
                                                        Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="RetypeValidator2" Text="Make sure you retyped the Email correctly."
                                                        ControlToValidate="RetypeEmail" ControlToCompare="Email" EnableClientScript="true"
                                                        SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelAddress" class="AddressContainer">
                                            <div class="AddressWrapper">
                                                <div class="Address1Container Question">
                                                    <asp:Label ID="Address1Lbl" runat="server" Text="Address 1*" AssociatedControlID="Address1"></asp:Label>
                                                    <asp:TextBox ID="Address1" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="Address1Validator1" runat="server" ErrorMessage="Please enter your Address."
                                                            ControlToValidate="Address1" EnableClientScript="true" SetFocusOnError="true"
                                                            Display="Dynamic" CssClass="errormsg" />
                                                        <asp:RegularExpressionValidator ID="Address1Validator2" runat="server" ErrorMessage="Please do not use special characters in the Address 1 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address1" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="Address2Container Question">
                                                    <asp:Label ID="Address2Lbl" runat="server" Text="Address 2" AssociatedControlID="Address2"></asp:Label>
                                                    <asp:TextBox ID="Address2" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="Address2Validator1" runat="server" ErrorMessage="Please do not use special characters in the Address 2 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address2" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="Address3Container Question">
                                                    <asp:Label ID="Address3Lbl" runat="server" Text="Address 3" AssociatedControlID="Address3"></asp:Label>
                                                    <asp:TextBox ID="Address3" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="Address3Validator1" runat="server" ErrorMessage="Please do not use special characters in the Address 3 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address3" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="CityContainer Question">
                                                    <asp:Label ID="CityLbl" runat="server" Text="Suburb*" AssociatedControlID="City"></asp:Label>
                                                    <asp:TextBox ID="City" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="CityValidator1" runat="server" ErrorMessage="Please enter your Suburb."
                                                            ControlToValidate="City" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                            CssClass="errormsg" />
                                                        <asp:RegularExpressionValidator ID="CityValidator2" runat="server" ErrorMessage="Please do not use special characters in the Suburb field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="City" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                         <div class="StateContainer Question">
                                                <asp:Label ID="StateLbl" runat="server" Text="State*" AssociatedControlID="State"></asp:Label>
                                                <asp:DropDownList ID="State" runat="server">
                                                    <asp:ListItem Value="" Selected="true">Select State</asp:ListItem>
                                                    <asp:ListItem Value="ACT">Australian Capital Territory</asp:ListItem>
                                                    <asp:ListItem Value="NSW">New South Wales</asp:ListItem>
                                                    <asp:ListItem Value="NT">Northern Territory</asp:ListItem>
                                                    <asp:ListItem Value="QLD">Queensland</asp:ListItem>
                                                    <asp:ListItem Value="SA">South Australia</asp:ListItem>
                                                    <asp:ListItem Value="TAS">Tasmania</asp:ListItem>
                                                    <asp:ListItem Value="VIC">Victoria</asp:ListItem>
                                                    <asp:ListItem Value="W.A">Western Australia</asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="StateValidator" runat="server" ErrorMessage="Please select your State."
                                                        ControlToValidate="State" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg" />
                                                </div>
                                            </div>

                                        <div class="PostalCodeContainer Question">
                                            <asp:Label ID="PostalCodeLbl" runat="server" Text="Post Code*" AssociatedControlID="PostalCode"></asp:Label>
                                            <asp:TextBox ID="PostalCode" runat="server" MaxLength="20" CssClass="inputTextBox"></asp:TextBox>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="PostalCodeValidator1" runat="server" ErrorMessage="Please enter your Post Code"
                                                    ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true"
                                                    Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                
                                            </div>
                                        </div>
                                        <div class="CountryContainer Question">
                                            <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_CountryLbl">Country
                                            </span><span class="fake_input">Australia</span>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelGender" class="GenderContainer Question">
                                            <asp:Label ID="GenderLbl" runat="server" Text="Gender*" AssociatedControlID="Gender"></asp:Label>
                                            <asp:DropDownList ID="Gender" runat="server">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="F">Female</asp:ListItem>
                                                <asp:ListItem Value="M">Male</asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="GenderValidator" runat="server" ErrorMessage="Please enter your Gender."
                                                    ControlToValidate="Gender" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="BirthdayContainer Question">
                                            <asp:Label ID="DOBLbl" runat="server" Text="Birthdate*" AssociatedControlID="yyyy"></asp:Label>
                                           <asp:DropDownList ID="dd" runat="server" CssClass="inputSelectBDay day">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="mm" runat="server" CssClass="inputSelectBDay month">
                                            </asp:DropDownList>
                                             <asp:DropDownList ID="yyyy" runat="server" CssClass="inputSelectBDay year">
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:CustomValidator ID="DOBValidator" runat="server" ErrorMessage="Please enter a valid date."
                                                    OnServerValidate="DOBValidate" EnableClientScript="false" Display="Dynamic" CssClass="errormsg validdate"></asp:CustomValidator>                            
                                                <asp:RequiredFieldValidator ID="ddValidator" runat="server" ErrorMessage="Please enter your Birth Day."
                                                    ControlToValidate="dd" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg day"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="mmValidator" runat="server" ErrorMessage="Please enter your Birth Month."
                                                    ControlToValidate="mm" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg month"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="yyyyValidator" runat="server" ErrorMessage="Please enter your Birth Year."
                                                    ControlToValidate="yyyy" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg year"></asp:RequiredFieldValidator>
                                                
                                                
                                            </div>
                                        </div>
                                        
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelComment" class="CommentContainer Question">
                                            <asp:Label ID="QuestionCommentLbl" runat="server" Text="Question / Comment*" AssociatedControlID="QuestionComment"></asp:Label>
                                            <asp:TextBox ID="QuestionComment" runat="server" TextMode="MultiLine" />
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="QuestionCommentValidator" runat="server" ErrorMessage="Please complete: Question / Comment."
                                                    ControlToValidate="QuestionComment" EnableClientScript="true" SetFocusOnError="true"
                                                    Display="Dynamic" CssClass="errormsg questioncomment" />
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                    </div>
                                    <div id="ctl00_ContentPlaceHolder1_ucForm_QuestionPanel" class="QuestionContainer">
                                    </div>
                                    <div id="submit-container" class="SubmitContainer png-fix">
                                        <asp:Button ID="Button1" runat="server" CssClass="submit buttonLink png-fix" Text="SUBMIT ›" />
                                    </div>
                                    <asp:HiddenField ID="hfHookID" runat="server" />
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="ContactFormSuccess" runat="server">
                                <div id="contactSuccess">
                                    <h2>
                                        Thank You!</h2>
                                    <p>
                                        Thank you for your contact. A member of our Consumer Relations team will be in contact
                                        as soon as possible. If you would like to sign up to receive information about Biore<sup>&reg;</sup> products, <a href="email-newsletter-sign-up">click here</a>.</p>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="ContactFormFailure" runat="server">
                                <div id="contactFailure">
                                    <h2>
                                        We're Sorry...</h2>
                                    <p>
                                        There has been a problem with your form submission. Please <a href="javascript: back();">
                                            try again</a>.</p>
                                    <asp:Literal ID="litError" runat="server"></asp:Literal>
                                </div>
                            </asp:Panel>
                            <div class="FormBottom png-fix">
                            </div>
                            <!-- Disclaimer -->
                            <div id="DisclaimerContainer" class="png-fix">
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">

                    </script>

                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
<script type="text/javascript">
    $(function () {
        if ($(".signUp").length) {
            toggle("noneToggle1");
            toggle("noneToggle2");
            toggle("noneToggle3");
            toggle("noneToggle4");
        }
    });
    function toggle(className) {
        var checkToggle = $("." + className + " input:last");
        checkToggle.click(function () {
            if ($("#" + checkToggle.attr('id') + ":checked").val()) {
                $("." + className + " input").each(function () {
                    if ($(this).attr('id') != checkToggle.attr('id')) {
                        $(this).attr("checked", false);
                        $(this).attr('disabled', 'disabled');
                    }
                });
            } else {
                $("." + className + "  input").each(function () {
                    if ($(this).attr('id') != checkToggle.attr('id')) {
                        $(this).removeAttr('disabled');
                    }
                });
            }
        });
    }
</script>
</asp:Content>
