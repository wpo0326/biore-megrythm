﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KAOForms;
using System.Web.UI.HtmlControls;
using Biore2012.BLL;

namespace Biore2012.Forms.RatingsAndReviews
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            initErrorMessages();
        }

        private void initErrorMessages()
        {
            reqEmail.Text = "Please enter your Email Address.";
            regEmail.Text = "Please enter a valid Email Address.";
            reqPassword.Text = "<p>Please enter your password.</p>";
            regLengthPassword.Text = "<p>The password must be at least six (6) characters long.</p>";
            regPassword.Text = "<p>Re-enter a password using letters, numbers or these characters !@#$%^*-=+?,_.</p>";
            reqPasswordConfirm.Text = "<p>Please re-enter your password for confirmation.</p>";
            cmpPasswordConfirm.Text = "<p>Password fields must match.</p>";

            //Forced ServSide message 
            if (Request.QueryString["return"] != null && Request.QueryString["return"] != "")
            {
                reqEmailCheck.Text = "<p>This email address has already been registered.<br/>Click <a href=\"Login.aspx?return=" + Server.UrlEncode(Request.QueryString["return"]) + "\">here</a> to login or reset your password.</p>";
            }else{
                reqEmailCheck.Text = "<p>This email address has already been registered.<br/>Click <a href=\"Login.aspx\">here</a> to login or reset your password.</p>";
            }
        }

        protected void register_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                AuthUtils au = new AuthUtils();
                if (au.testUniqueEmail(txtEmail.Text) == false)
                {
                    //email is already registered.
                    username.Text = "";  //force error in reqEmail validator
                    Page.Validate();
                   
                }
                else
                {
                    au.CreateUser(txtEmail.Text, txtPW1.Text, txtEmail.Text);
                    au.LogInUser(txtEmail.Text, txtPW1.Text);

                    if (Request.QueryString["return"] != null && Request.QueryString["return"] != "")
                    {
                        Response.Redirect(Request.QueryString["return"]);
                    }
                    else
                    {
                        Response.Redirect("Default.aspx");
                    }
                }
            }
        }       
    }
}
