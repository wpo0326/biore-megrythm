﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ResetPassword.aspx.cs" Inherits="Biore2012.Forms.RatingsAndReviews.ResetPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/login.css")
        .Render("~/CSS/combinedlogin_#.css")
    %>
    <!--<link href="<%= System.Configuration.ConfigurationManager.AppSettings["cssprefix"] %>login.min.css" rel="stylesheet" type="text/css" media="screen,projection" />  -->   

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="contactFormWrap">
        <div id="BrandImageContainer">
            <!--<img style="border-width: 0px;" src="/images/forms/ratingsReviewModel.jpg" id="ctl00_ContentPlaceHolder1_ucFormConfig_Image1" />-->
        </div>
        <div id="ContactFormContainer">
            <!-- Header -->
            <div id="formHeader">
                <h1 id="PageHeader" runat="server"> RESET PASSWORD </h1>
            </div>
            <!-- Description -->
            <asp:Panel ID="formPanel" runat="server" Visible="true">
                <div class="DescriptionContainer">
                    <h2 class="first">
                        To reset your password, please enter the email you used to sign up, and we'll send
                        you a new password.</h2>
                </div>
                <p class="req">
                    <em>Required*</em></p>
                <div class="Question">
                    <asp:Label ID="Label6" runat="server" AssociatedControlID="forgotPasswordUsername">Email* </asp:Label>
                    <asp:TextBox runat="server" MaxLength="70" CssClass="text" ID="forgotPasswordUsername"></asp:TextBox>
                </div>
                <%--<div class="ErrorContainer" id="EmailError">
                    <asp:Literal runat="server" ID="literalEmailError" />
                </div>--%>
                <div class="ErrorContainer">
                    <asp:RequiredFieldValidator ID="reqEmail" runat="server" Display="Dynamic" ErrorMessage="Please enter your Email Address."
                        ControlToValidate="forgotPasswordUsername" EnableClientScript="true" SetFocusOnError="true"
                        CssClass="errormsg"></asp:RequiredFieldValidator>
                </div>
                <div class="ErrorContainer">
                    <asp:RegularExpressionValidator ID="regEmail" runat="server" Display="Dynamic" ErrorMessage="Please enter a valid Email Address."
                        ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                        ControlToValidate="forgotPasswordUsername" EnableClientScript="true" SetFocusOnError="true"
                        CssClass="errormsg"></asp:RegularExpressionValidator>
                </div>
                <div class="ErrorContainer">
                    <asp:TextBox runat="server" ID="username" Visible="false" Text="username" />
                    <asp:RequiredFieldValidator ID="reqUsername" runat="server" Display="Dynamic" ErrorMessage=""
                        ControlToValidate="username" EnableClientScript="true" SetFocusOnError="true"
                        CssClass="errormsg"></asp:RequiredFieldValidator>
                </div>
                <div class="Buttons">
                    <asp:Button ID="Button" runat="server" OnClientClick="return passwordValidate()"
                        OnClick="reset_Click" CssClass="buttonLink submit" Text="Reset My Password">
                    </asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="confirmationPanel" runat="server" Visible="false">
                <div class="DescriptionContainer">
                    <h2 class="first">
                        Thank you! We've sent an email containing a new password to the address associated
                        with your account.</h2>
                    <p>
                        <br />
                        <asp:Literal ID="literalLoginButton" runat="server" />
                        <!-- <a href="Login.aspx" class="buttonLink">Login</a> -->
                        <br />
                    </p>
                </div>
            </asp:Panel>
            <asp:Panel ID="lockedOutPanel" runat="server" Visible="false">
                <div class="DescriptionContainer">
                    <h2 class="first">
                        Your account has been locked out for too many failed login attempts in a row. Try
                        again in 30 minutes.</h2>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
