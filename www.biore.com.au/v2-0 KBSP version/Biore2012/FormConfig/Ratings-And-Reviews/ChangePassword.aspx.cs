﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Biore2012.BLL;

namespace Biore2012.Forms.RatingsAndReviews
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            initErrorMessages();
            AuthUtils au = new AuthUtils();
            if (au.CheckIfAuthenticated() == false)
            {
                Response.Redirect("Login.aspx");
            }

            literalBackButton.Text = "<a href=\"" + Request.QueryString["return"] + "\" class=\"buttonLink\">Back</a><br />";


        }

        private void initErrorMessages()
        {
            reqCurrPassword.Text = "<p>Please enter your current password.</p>";
            regCurrLengthPassword.Text = "<p>The current password must be at least six (6) characters long.</p>";
            regCurrPassword.Text = "<p>Re-enter a password using letters, numbers or these characters !@#$%^*-=+?,_.</p>";
            reqNewPass.Text = "<p>Please enter your new password.</p>";
            regLenNewPass.Text = "<p>The new password must be at least six (6) characters long.</p>";
            regNewPass.Text = "<p>Re-enter a password using letters, numbers or these characters !@#$%^*-=+?,_.</p>";
            reqPasswordConfirm.Text = "<p>Please re-enter your password for confirmation.</p>";
            cmpPasswordConfirm.Text = "<p>New Password fields must match.</p>";
            //Forced ServSide message for wrong current password
            reqPassCheck.Text = "<p>The current password entered is not correct.</p>";
        }

        protected void change_Click(object sender, EventArgs e)
        {
            //is current password right?
            if (Membership.ValidateUser(HttpContext.Current.User.Identity.Name, textCurrent.Text))
            {
                if (textNew.Text != textNew2.Text)
                {
                    // new password fields must match
                    Page.Validate();

                }
                else if (textNew.Text.Length < 6)
                {
                    // new password must be at least 6 characters long.
                    Page.Validate();
                }
                else
                {
                    // Okay, let's change it.
                    //change password
                    MembershipUser u;
                    u = Membership.GetUser(HttpContext.Current.User.Identity.Name, false);
                    u.ChangePassword(textCurrent.Text, textNew.Text);
                    //show confirmation
                    formPanel.Visible = false;
                    confirmationPanel.Visible = true;
                }
            }
            else
            {
                // Hey wait the current PW is wrong...
                userpass.Text = "";
                Page.Validate();
                userpass.Text = "userpass";
            }
        }
    }
}
