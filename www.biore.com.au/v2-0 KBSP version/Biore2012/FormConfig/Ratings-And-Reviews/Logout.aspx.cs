﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Biore2012.BLL;

namespace Biore2012.Forms.Ratings_And_Reviews
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AuthUtils au = new AuthUtils();
            au.LogOutUser();



            String siteName = System.Configuration.ConfigurationManager.AppSettings.Get("siteName");
            String fromEmail = System.Configuration.ConfigurationManager.AppSettings.Get("resetPasswordFromEmail");
            String fromName = System.Configuration.ConfigurationManager.AppSettings.Get("resetPasswordFromName");
            String SMTPServer = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPServer");
            String theLink = Request.Url.Host + System.Configuration.ConfigurationManager.AppSettings["path"];
            theLink = "http://" + theLink.Replace("//", "/");



            if (Request.QueryString["return"] != null && Request.QueryString["return"] != "")
            {
                if (Request.QueryString["return"].ToLower().IndexOf("ChangePassword.aspx") > -1)
                {
                    Response.Redirect(theLink);
                }
                else
                {
                    Response.Redirect(Request.QueryString["return"]);
                }
            }
            else
            {
                Response.Redirect(theLink);
            }


        }
    }
}
