﻿<%@ Page Title="About Bior&eacute;&reg; Skincare | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.about_us._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Learn more about Bioré® Skincare" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
                .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent" class="aboutBlueBG">
            <div class="centeringDiv">
                <div id="content" class="aboutBlueTxt">
                    <div class="titleh1">
                        <h1>DISCOVER Bior&eacute;<sup>&reg;</sup></h1>
                        <div id="responseRule"></div>
                    </div>
                    <img src="../../images/about/1brandaboutusUK.png" /></p>
                    <div class="about-info">     
                        
                        <p><span>Discover Bioré – the expert in clean pores! </span></p>

                        <p>Clogged pores are the main cause of skin imperfections. The highly effective Bioré products with baking soda and charcoal clean your pores from dirt, oil and blackheads to help you effectively fight against impurities. Whether used as washes, scrubs, masks or pore strips – Bioré deep cleans your pores at the source. For clean skin and free pores.  Because when you clean the pore, you clear the problem.</p>

                        <p><span>Bioré – Free Your Pores</span></p>
                                           
                       
                    </div>
                    <img src="../images/about/aboutBG.png" alt="" id="Img1" />                   
                </div>
                        <p>* Source: IRI Scan Data, Total market, Pore Strip Dollars (000s) MAT 25/03/18</p> 
            </div>
        </div>
    </div>
</asp:Content>
