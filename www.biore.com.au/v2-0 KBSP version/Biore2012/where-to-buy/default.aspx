﻿<%@ Page Title="Where To Buy | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.wheretobuy._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
	#storeLogos ul li { width:240px; height:120px; float:left; display:block; text-align:center;}
    #storeLogos ul li a {width:180px; height:100px; background-position:center left;text-indent:-9999px; display:block;}
	#storeLogos ul li a:hover { background-position:right;}
	</style>
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1>Where To Buy</h1>
                    <div id="storeLogos" class="logos">
                        <p>Bior&eacute;<sup>&reg;</sup> skincare products are available to buy in stores at:<br /><br /><br /></p>
                        <ul>
                            <li><a style="background-image: url(../images/whereToBuy/Priceline.png);" href="http://www.priceline.com.au/" target="wtb">Priceline</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/Chemist-Warehouse.png);" href="http://www.chemistwarehouse.com.au" target="wtb">Chemist Warehouse</a></li>
							<li><a style="background-image: url(../images/whereToBuy/target.png);" href="">Target</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/My-Chemist.png);" href="" target="wtb">My Chemist</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/bigw.png);" href="" target="wtb">Big W</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/Woolworths.png);" href="" target="wtb">Woolworths</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/Coles.png);" href="" target="wtb">Coles</a></li>
                        </ul>                
                    </div>
                    <div style="clear:both; height:0px;"></div>
            </div>
        </div>
    </div>
</asp:Content>