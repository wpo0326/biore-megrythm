﻿<%@ Page Title="Over Bioré® huidverzorging | Bioré® huidverzorging" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.about_us._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Leer meer over Bioré® Huidverzorging " name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent" class="aboutBlueBG">
            <div class="centeringDiv">
                <div id="content" class="aboutBlueTxt">
                    <div class="titleh1">
                        <h1>oVER Bior&eacute;<sup>&reg;</sup></h1>
                        <div id="responseRule"></div>
                    </div>
                    <div class="about-info">     
                        
                        <p><span>Ontdek Bioré – de expert voor het reinigen van je poriën!</span></p>

                        <p>Bioré huidverzorging richt zich op de oorzaak van veel huidproblemen– verstopte poriën.</p> 
<p>De zeer effectieve producten van Bioré met houtskool en zuiverings-soda bevrijden poriën van vuil en talg en bestrijden effectief mee-eters en onzuiverheden.</p>  
<p>Bioré reinigt slimmer, niet harder. Onze gels, scrubs en porie strips reinigen tot diep in de poriën en bestrijden onzuiverheden bij de kern. Voor een stralende huid en schone poriën.</p>

                        <p><span>Bioré – Free Your Pores</span></p>
                                           
                       
                    </div>
                    <img src="../images/about/aboutBG.png" alt="" id="Img1" />                    
                </div>
            </div>
        </div>
    </div>
</asp:Content>

