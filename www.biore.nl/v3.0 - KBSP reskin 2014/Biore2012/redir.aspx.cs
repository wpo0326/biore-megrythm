﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Biore2012
{
    public partial class redir : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string redirectURL = "/";
            string dest = Request.QueryString["dest"];

            if (dest == null)
            {
                // CHECK TO SEE IF THIS IS A QR CODE PASS THRU
                // TO HELP SECURE ONLY ALLOW LINKS TO YOUTUBE, FACEBOOK OR LOCAL
                string qrdest = Request.QueryString["qrdest"];
                if (!String.IsNullOrEmpty(qrdest) &&
                    (qrdest.ToLower().StartsWith("http://www.youtube.com") ||
                     qrdest.ToLower().StartsWith("http://youtube.com") ||
                     qrdest.ToLower().StartsWith("http://www.facebook.com") ||
                     qrdest.ToLower().StartsWith("http://facebook.com") ||
                     qrdest.ToLower().StartsWith("http://apps.facebook.com") ||
                     qrdest.ToLower().StartsWith("http://goo.gl") ||
                     qrdest.ToLower().StartsWith("/"))
                   )
                {
                    redirectURL = qrdest;
                }
                else if (!String.IsNullOrEmpty(qrdest) &&
                    (qrdest.ToLower().StartsWith("http%3A%2F%2Fwww.youtube.com") ||
                     qrdest.ToLower().StartsWith("http%3A%2F%2Fyoutube.com") ||
                     qrdest.ToLower().StartsWith("http%3A%2F%2Fwww.facebook.com") ||
                     qrdest.ToLower().StartsWith("http%3A%2F%2Ffacebook.com") ||
                     qrdest.ToLower().StartsWith("http%3A%2F%2Fapps.facebook.com") ||
                     qrdest.ToLower().StartsWith("http%3A%2F%2Fgoo.gl") ||
                     qrdest.ToLower().StartsWith("%2F"))
                   )
                {
                    redirectURL = Server.UrlDecode(qrdest);
                }
                else
                {
                    // REDIRECT TO ERROR PAGE NOT FOUND 404
                    redirectURL = System.Configuration.ConfigurationManager.AppSettings["baseprefix"] + System.Configuration.ConfigurationManager.AppSettings["pagenotfound"].ToString() + "/";
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(dest))
                {
                    switch (dest.ToUpper())
                    {
                        case "BIORE_FB":
                            redirectURL = "http://www.facebook.com/bioreskin";
                            break;
                        case "BIORE_PROVEIT":
                            redirectURL = "http://www.facebook.com/bioreskin?sk=app_205787372796203";
                            break;
                        case "BIORE_FB_DATINGRULES":
                            redirectURL = "http://www.facebook.com/DatingRules";
                            break;
                        case "BIORE_FB_DATINGRULES_B":
                            redirectURL = "http://www.facebook.com/bioreskin?sk=app_243859735683241";
                            break;
                        case "BIORE_PS_FB":
                            // THIS ONE IS SPECIFICALLY FOR PAID SEARCH
                            redirectURL = "http://www.facebook.com/bioreskin";
                            break;
                        case "BIORE_PS_PROVEIT":
                            // THIS ONE IS SPECIFICALLY FOR PAID SEARCH
                            redirectURL = "http://www.facebook.com/bioreskin?sk=app_205787372796203";
                            break;
						case "BIORE_DRUG":
                            redirectURL = "http://www.drugstore.com/templates/stdplist/default.asp?catid=168922&sctrx=dps-16&sctrxp1=168328";
                            break;
						case "CVS_STOREFINDER":
							redirectURL = "http://www.cvs.com/CVSApp/store/storefinder.jsp";
                            break;
						case "RITE_AID_STORE_LOCATOR":
							redirectURL = "http://www5.riteaid.com/savenow/locator/";
							break;
						case "BBMA":
							// May 2012 Newsletter, Billboard Music Awards
							redirectURL = "http://www.billboard.com/bbma";
							break;
						case "BIORE_FB_HOT_GUYS":
							redirectURL = "http://www.facebook.com/bioreskin?sk=app_197505253693954";
                            break;
                        case "BIORE_AS_AMZN":
                            // Added per Beatriz Trujillo @ kbsp.com 01/18/13 - MCT, CG Marketing
                            redirectURL = "http://www.amazon.com/dp/B00ATY942M";
                            break;
                        default:
                            if (!String.IsNullOrEmpty(dest) && (
                                 dest.ToUpper().StartsWith("HTTP") ||
                                 dest.ToUpper().StartsWith("/"))
                                )
                            {
                                redirectURL = Server.UrlDecode(dest);
                            }
                            break;
                    }
                }
            }
            
            //litRedirectUrl.Text = redirectURL;
            HtmlMeta metaDesc = new HtmlMeta();
            metaDesc.Attributes.Add("http-equiv", "refresh");
            metaDesc.Attributes.Add("content", "0;url=" + redirectURL);
            Page.Header.Controls.Add(metaDesc);
        }
    }
}
