﻿<%@ Page Title="" Language="C#" MasterPageFile="~/facebook/FBContentPageMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Biore2012.facebook.FreeSample.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div id="fb-root"></div>

	<script type="text/javascript">
		var extJsHost_connect_facebook_net = (("https:" == document.location.protocol) ? "https://" : "http://");
		window.fbAsyncInit = function() {
			FB.init({
				appId: '<%= System.Configuration.ConfigurationManager.AppSettings["CSBCCouponFBAppId"] %>', // App ID
				//channelURL : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
				status: true, // check login status
				cookie: true, // enable cookies to allow the server to access the session
				oauth: true, // enable OAuth 2.0
				xfbml: true  // parse XFBML
			});

			// Additional initialization code here
			FB.Canvas.setAutoGrow();
			FB.Canvas.scrollTo(0, 0);
		};

		// Load the SDK Asynchronously
		(function(d) {
			var js, id = 'facebook-jssdk'; if (d.getElementById(id)) { return; }
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = extJsHost_connect_facebook_net + "connect.facebook.net/en_US/all.js";
			d.getElementsByTagName('head')[0].appendChild(js);
		} (document));
	</script>

	<div class="freesample">
		<asp:Panel ID="panelLikeOverlay" CssClass="likeOverlay" runat="server" Visible="true">
			<h1 class="ir">Like Us&ndash;And Get a Deep (Spring) Clean!</h1>
			<p class="main">Find out if you qualify for a <strong>free trial-size sample* of 3 Bior&eacute;<sup>&reg;</sup> Skincare Products!</strong></p>
			<p class="fine">*Full-size products shown do not reflect trial-size packettes, nor products tailored to your skin type that will be distributed to qualified recipients.</p>
		</asp:Panel>
		
		<asp:Panel ID="panelUserLikes" CssClass="userLikes" runat="server" Visible="false">
			<div class="section header">
				<h1 class="ir">Get a Deep (spring) Clean for Your Skin</h1>
				<%--<h2 class="ir">Qualify to try Bior&eacute;<sup>&reg;</sup> Skincare products&#8224; for clean skin this season!</h2>--%>
				<a href="<%= System.Configuration.ConfigurationManager.AppSettings["path"] %>form-custom1?promo=biore_us_201203_TriPackette_FB_Samp" class="btn ir" target="_blank">See If You Qualify For A Sample*</a>
			</div>
			<div class="section productHolder">
				<a href="http://www.biore.com/en-US/charcoal-for-oily-skin/combination-skin-balancing-cleanser" target="_blank" class="first"><strong>NEW Bior&eacute;<sup>&reg;</sup> Combination Skin Balancing Cleanser</strong> provides an even, all-over clean by lifting away impurities without stripping skin of moisture.</a>
				<a href="http://www.biore.com/en-US/charcoal-for-oily-skin/pore-unclogging-scrub" target="_blank"><strong>Pore Unclogging Scrub</strong> gently removes dull surface cells without damaging skin, and contains salicylic acid acne treatment to help prevent future breakouts.</a>
				<a href="http://www.biore.com/en-US/pore-strips/pore-strips" target="_blank"><strong>Deep Cleansing Pore Strips</strong> clean a week's worth of build-up in just 10 minutes and instantly remove pore-clogging dirt and oil that can cause blackheads.</a>
				<p class="productFine">Full-size products shown do not reflect trial-size packettes, nor products tailored to your skin type that will be distributed to qualified recipients.</p>
			</div>
			<div class="section fine"><%--&#8224; Qualified recipients will receive three skincare samples based on their skin type. Full size products shown do not reflect sample size packettes that will be distributed to qualified recipients.<br />--%>*Users must qualify in order to receive sample. Open to legal residents of the 50 United States (including D.C.), 18 years old or older who are invited by Sponsor to participate. Limit one Offer per person/per email address. Void where prohibited. User qualification is at the sole discretion of <span class="no-wrap">Kao USA Inc.</span></div>
		</asp:Panel>
		
		<asp:Panel ID="panelNotFacebook" CssClass="notFacebook" runat="server" Visible="false">
			<h1>Like Us&ndash;And Get a Deep (Spring) Clean!</h1>
			<p>To find out if you qualify for a <strong>free trial-size sample<br />of 3 Bior&eacute;<sup>&reg;</sup> Skincare Products</strong>, go to<br />
			<a href="http://www.facebook.com/bioreskin">our Facebook page</a>, &quot;like&quot; us and visit the &quot;Free Sample&quot; tab.</p>			
		</asp:Panel>
	</div>

<%--    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
    <script type="text/javascript">
    	if (typeof jQuery == 'undefined') {
    		document.write(unescape("%3Cscript src='../../js/jquery-1.6.4.min.js' type='text/javascript'%3E%3C/script%3E"));
    	}
    </script>	--%>
<%--	<%=SquishIt.Framework.Bundle .JavaScript()
		.Add("js/main.js")
        .Add("~/js/swfobject.min.js")
        .Add("~/js/jquery-ui-1.8.2.custom.min.js")
		.Render("~/facebook/js/freesample_#.js")
	%>--%>
</asp:Content>

