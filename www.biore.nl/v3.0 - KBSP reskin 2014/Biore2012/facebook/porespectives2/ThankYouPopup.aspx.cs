﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace Biore2012.facebook.porespectives2
{
	public partial class ThankYouPopup : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(Request["post_id"]))
			{
				panThankYou.Visible = false;
				panCancelled.Visible = true;
				panError.Visible = false;
			}
			else
			{
				try
				{
					savePostData();
				}
				catch (Exception ex)
				{
					panThankYou.Visible = false;
					panCancelled.Visible = false;
					panError.Visible = true;

					LogEntry myLog = new LogEntry();
					myLog.Message = "Error saving Pore-spectives user share data to database.  Exception message: " + ex.Message;
					myLog.Priority = 1;
					Logger.Write(myLog);
					myLog = null;
				}
			}
		}

		private int savePostData()
		{
			int newID = -1;

			string postID = Request["post_id"];
			string userCopyOrg = "";

			if (Session["UserCopy"] != null)
			{
				userCopyOrg = Session["UserCopy"].ToString();
			}

			try
			{
				String conStr = System.Configuration.ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString;
				SqlConnection myConn = null;
				myConn = new SqlConnection(conStr);
				myConn.Open();

				SqlCommand cmd = new SqlCommand("sp201112FBPorespectiveAddUserFeedback", myConn);
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Parameters.Add(new SqlParameter("@UserFeedback", userCopyOrg));
				cmd.Parameters.Add(new SqlParameter("@FBPostID", postID));

				newID = Convert.ToInt32(cmd.ExecuteScalar());
				myConn.Close();
			}
			catch (Exception)
			{
				throw new ArgumentException("Error inserting entry.");
			}
			return newID;
		}
	}
}
