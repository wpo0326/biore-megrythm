﻿<%@ Page Title="Privacy | Bior&eacute;&reg; Huidverzorging" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Biore2012.privacy.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Ontdek de Bioré® Huidverzorging website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
    <style type="text/css">
        #content p {
            text-align: left;
        }
        #content ul {
            margin: auto auto auto 8em;
        }
        #content ul li {
            list-style: square;
        }

            #content ul li p {
                margin-left: 0;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div id="main">
            <div id="mainContent">
                <div class="centeringDiv">
                    <div id="content">
                        <div class="titleh1">
                            <h1>Bior&eacute;<sup>&reg;</sup> privacybeleid</h1>
                            <div id="responseRule">
                            </div>
                        </div>
                        <p><strong>PRIVACYBELEID </strong><strong>website </strong><a href="http://www.biore.nl/biore-skincare"><strong>http://www.biore.nl/biore-skincare</strong></a></p>
                        <p><strong>Voor het laatst herzien op 2 juli 2018</strong></p>
                        <p>Guhl Ikebana Cosmetics BV, een Kao Groep Company, 3818 LA Amersfoort (“Kao Company” of “wij” of “ons”) en ieder van de aan haar gelieerde ondernemingen en dochtermaatschappijen in de EMEA regio, (collectief: de “Kao Groep”) neemt data privacy serieus. Dit Privacybeleid informeert de gebruikers van www.biore.nl en alle andere websites of mobiele applicaties van Kao Company waarop dit Privacybeleid wordt getoond ("Website") hoe wij, als verwerkingsverantwoordelijke in de zin van de Europese Algemene Verordening Gegevensbescherming ("AVG"), de persoonlijke gegevens en andere informatie van zulke gebruikers verzamelen en verwerken in verband met hun gebruik van de Website. </p>
                        <p>Houd er rekening mee dat andere websites of mobiele apps van Kao Groep kunnen worden beheerst door andere vormen van privacybeleid.</p>
                        <p><strong>1. Categorie&euml;n persoonlijke gegevens en verwerkingsdoeleinden – welke persoonlijke gegevens van u verwerken wij en waarom?</strong></p>
                        <p><strong>1.1&nbsp;&nbsp;&nbsp; Metadata</strong></p>
                        <p>U kunt gebruik maken van de website zonder enige persoonlijke gegevens van u te verstrekken. In dat geval zullen wij alleen de volgende metadata verzamelen die resulteren uit uw gebruik van de Website: browsertype en -versie, operating system en interface, website van waaruit u ons bezoekt (referrer URL), Website(s) die u op onze Website bezoekt, datum en tijdstip van uw toegang tot onze Website, en internet protocol (IP) adres.</p>
                        
                        <p><strong>1.2&nbsp;&nbsp;&nbsp; Wedstrijden</strong></p>
                        <p>Als u deelneemt aan een wedstrijd verzamelen en verwerken we vaak de volgende persoonlijke gegevens over u: naam, geslacht (begroeting), postadres, e-mailadres, telefoonnummer, datum van inwerkingtreding, selectie als winnaar, prijs, antwoord op de quiz (bijvoorbeeld inzending foto). Wij verwerken zulke persoonlijke gegevens om de wedstrijd te kunnen uitvoeren, de winnaar te kunnen informeren, de prijs aan de winnaar te kunnen overhandigen, het uitvoeren van het event, en u marketing materiaal te sturen voor zover toegestaan op basis van toepasselijke wetgeving en uw belangstelling te analyseren voor marketing doeleinden. Per winactie wordt er een desbetreffend (online) algemene voorwaarden formulier opgesteld waarin alle specifieke voorwaarden m.b.t. de winactie worden gespecificeerd. </p>
                        <p><strong>1.3&nbsp;&nbsp;&nbsp; Contactformulier</strong></p>
                        <p>Op onze website bieden wij u de mogelijkheid om contact met ons op te nemen via een contactformulier. Voor dit formulier hebben wij persoonlijke gegevens van u nodig: e-mail adres en de vraag die u heeft. De persoonlijke data die u ons verstrekt zal alleen worden gebruikt om uw vraag te beantwoorden en voor de technische administratie hiervan. De data worden niet naar derden gestuurd. Uw persoonlijke data zal worden verwijderd zodra uw aanvraag is verwerkt of wanneer u de gegeven toestemming intrekt. </p>
                        <p><strong>1.4&nbsp;&nbsp;&nbsp;  Google Analytics </strong></p>
                        <p>Deze website maakt gebruik van Google Analytics, een webanalyse-service, die wordt aangeboden door Google Inc ("Google"). Google Analytics maakt gebruik van "cookies". Dit zijn tekstbestanden die op jouw computer worden geplaatst en die analyseren hoe je de site gebruikt. De informatie die door de cookies wordt herleid over jouw gebruik van de website wordt door Google overgebracht naar servers in de Verenigde Staten en daar opgeslagen. </p>
                        <p>Wanneer IP-anonimisering is geactiveerd op deze website, wordt je IP-adres ingekort binnen de lidstaten van de Europese Unie en andere verdragslanden van de Europese Economische Ruimte. Alleen in uitzonderlijke gevallen wordt het gehele IP-adres eerst overgebracht naar een server van Google in de VS en daar vervolgens ingekort. IP-anonimisering is actief op deze website. </p>
                        <p>Google gebruikt deze informatie namens de beheerder van deze website om je gebruik van deze website te evalueren, om rapporten over de website-activiteiten samen te stellen voor website-beheerders en om hun andere diensten met betrekking tot website-activiteiten en internetgebruik te leveren.</p>
                        <p>Het IP-adres, dat je browser verzendt in het kader van Google Analytics, wordt niet gecombineerd met andere gegevens waarover Google beschikt. Je kunt het gebruik van cookies weigeren door de juiste instellingen te selecteren in je browser. Houd er dan rekening mee dat je in dat geval misschien niet alle mogelijkheden van deze website kunt gebruiken.</p>
                        <p>
Je kunt je ook voor nu en de toekomst afmelden voor activiteiten van Google Analytics door de Google Analytics Opt-out Browser Add-on voor je webbrowser te downloaden en te installeren: <a href="tools.google.com/dlpage/gaoptout" target="_blank">tools.google.com/dlpage/gaoptout</a>
</p>

                        <p>Als alternatief voor de browser plug-in en speciaal voor mobiele browsers, klikt u op de volgende koppeling om een Opt-out cookie in te stellen. Deze Opt-out cookie voorkomt detectie door Google Analytics op deze website. <a href="/privacy/?google-analytics-opt-out=true">/http://www.biore.nl/privacy/?google-analytics-opt-out=true</a></p>
                        
                        <p><strong>2. Basis voor Verwerkingsdoeleinden en Consequenties - wat is de juridische basis voor het verwerken van uw persoonlijke gegevens en wat gebeurt er als u die niet wilt verstrekken?</strong></p>
                        <p>De Kao Company gaat uit van de volgende wettelijke basis voor het verzamelen, verwerken en gebruiken van persoonlijke gegevens: Uw toestemming voor het verwerken van uw gegevens voor een of meer specifieke doelen. Bijvoorbeeld het contact opnemen naar aanleiding van invullen contactformulier of website optimalisatie.</p>
                        
                        <p><strong>3. Categorieën Ontvangers en Internationale Doorgifte – aan wie geven wij uw persoonlijke gegevens door en waar zijn zij gevestigd?</strong></p>
                        <p>Wij kunnen uw persoonlijke gegevens doorgeven aan derde partijen voor de hierboven omschreven doeleinden, en wel als volgt:</p>
                        <ul>
                            <li><p><strong>Binnen de Kao Company: </strong>onze moederentiteit, de Kao Corporation in Japan en elk van de aangesloten ondernemingen en dochterondernemingen (naar elke aangesloten onderneming of dochteronderneming, met ons inbegrepen, wordt afzonderlijk verwezen als "Kao Company"; gezamenlijk wordt verwezen naar de "Kao Groep") binnen de globale Kao Groep <a href="http://www.kao.com/global/en/about/outline/group-companies" target="_blank">http://www.kao.com/global/en/about/outline/group-companies</a> kan uw persoonlijke gegevens ontvangen als noodzakelijk voor de verwerkingsdoeleinden die hierboven zijn beschreven. Al naar gelang de categorieën persoonlijke gegevens en de doeleinden waarvoor de persoonlijke gegevens zijn verzameld kunnen verschillende interne afdelingen binnen de Kao Company uw persoonlijke gegevens ontvangen.  Bovendien kunnen andere afdelingen binnen de Kao Company toegang hebben tot bepaalde persoonlijke gegevens over u op basis van een need to know, zoals de juridische afdeling.</p></li>
                            <li><p><strong>Via gegevensverwerkers:</strong> bepaalde al dan niet aangesloten derde partijen kunnen uw persoonlijke gegevens ontvangen om dit soort gegevens onder juiste instructies ("Verwerkers") te verwerken voor zover noodzakelijk voor de hierboven omschreven verwerkingsdoeleinden, zoals  website dienstverleners, dienstverleners orderverwerking, dienstverleners klantenservice, dienstverleners marketing, dienstverleners IT ondersteuning en andere dienstverleners die ons ondersteunen bij het onderhouden van onze commerciële relatie met u. De Verwerkers zullen zijn onderworpen aan contractuele verplichtingen om relevante technische en organisatorische veiligheidsmaatregelen te implementeren om de bescherming van de persoonlijke gegevens te waarborgen en om de persoonlijke gegevens alleen te verwerken zoals is geïnstrueerd.</p></li>
                            <li><p><strong>Andere ontvangers:</strong> wij kunnen uw persoonlijke gegevens – in overeenstemming met de toepasselijke wetgeving op het gebied van gegevensbescherming - ook doorgeven overheidsorganen, juridische instanties, juridische bijstand, externe adviseurs of zakenpartners. In geval van een bedrijfsfusie of acquisitie kunnen persoonlijke gegevens worden doorgegeven aan de derde partijen die betrokken zijn bij de fusie of acquisitie. Wij zullen uw persoonlijke gegevens zonder uw toestemming niet delen met derde partijen voor advertentie- of marketingdoeleinden of voor enige andere doeleinden.</p><p>Elke toegang tot uw persoonlijke gegevens is beperkt tot de personen die een need-to-know heb-ben om te kunnen voldoen aan hun functieverantwoordelijkheden. </p></li>
                            <li><p><strong>Internationale doorgiftes:</strong>  De persoonlijke gegevens die wij van u verzamelen of ontvangen kunnen worden doorgegeven en verwerkt door ontvangers die zich binnen of buiten de Europese Economische Ruimte (European Economic Area, afgekort "EEA") bevinden. Voor ontvangers buiten de EEA zijn sommigen gecertificeerd onder het EU-U.S. Privacy Shield en anderen zijn gevestigd in landen met besluiten over het beveiligingsniveau (adequacy decisions). En in elk geval is daarmee erkend dat voor de doorgifte een adequaat niveau van gegevensbescherming wordt geboden vanuit het perspectief van de Europese gegevensbeschermingswetgeving. Andere ontvangers kunnen gevestigd zijn in landen die geen adequaat beschermingsniveau bieden vanuit het perspectief van de Europese gegevensbeschermingswetgeving. Wij zullen alle noodzakelijke maatregelen treffen om er zeker van te zijn dat doorgiftes buiten de EEA voldoende beschermd zijn zoals vereist door de toe te passen wetgeving op het gebied van gegevensbescherming. Wat betreft de doorgiftes naar landen die geen voldoende niveau van gegevensbescherming bieden zullen we de doorgifte baseren op adequate waarborgen, zoals standaard clausules m.b.t. gegevensbescherming die zijn geaccepteerd door de Europese Commissie of een toezichthou-der, goedgekeurde gedragscodes in combinatie met bindende en afdwingbare verplichtingen van de ontvanger, of goedgekeurde certificeringsmechanismen samen met bindende en afdwingbare verplichtingen van de ontvanger. U kunt een kopie van zulke relevante waarborgen aanvragen door contact met ons op te nemen zoals beschreven in de onderstaande sectie 7.</p>
                                <p>Wij kunnen uw persoonlijke gegevens ook bewaren na het beëindigen van de contractuele relatie, als uw persoonlijke gegevens nodig zijn om te voldoen aan andere toepasselijke wetgeving, of als we uw persoonlijke gegevens nodig hebben om een rechtsvordering te vestigen, uit te oefenen of te verdedigen, dit uitsluitend op basis van need to know. Wij zullen het verwerken van uw persoonlijke gegevens voor dit soort beperkte doeleinden na de beëindiging van de contractuele relatie zoveel mogelijk beperken.</p>
                            </li>
                        </ul>
                        
                  
                        <p><strong>4. Bewaarperiode – hoe lang bewaren wij uw persoonlijke gegevens?</strong></p>
                        <p>Uw persoonlijke gegevens worden bewaard zolang het noodzakelijk is om u van de gevraagde dienstverlening en producten te voorzien. Als u [de contractuele relatie met ons heeft beëindigd / u uw account heeft opgeheven / de wedstrijd voltooid is] of op andere wijze uw relatie met ons heeft beëindigd, zullen wij uw persoonlijke gegevens uit onze systemen en records verwijderen en/of maatregelen nemen om deze gegevens op de juiste wijze te anonimiseren zodat u op basis daarvan niet langer geïdentificeerd kunt worden (tenzij wij uw informatie moeten behouden om te voldoen aan wettelijk of regelgevende verplichtingen waaraan de Kao Company is onderworpen, bijvoorbeeld belastingsdoeleinden). </p>
                        <p>Wij kunnen uw contactgegevens en belangstelling voor onze producten en dienstverlening voor een langere periode bewaren, als de Kao Company u marketing materiaal mag sturen. Ook kunnen wij op basis van toepasselijke wetgeving worden genoodzaakt om bepaalde persoonlijke gegevens van u te bewaren voor een periode van 10 jaar na het relevante fiscale jaar.</p>
                        <p>Wij kunnen uw persoonlijke gegevens ook bewaren na het beëindigen van de contractuele relatie, als uw persoonlijke gegevens nodig zijn om te voldoen aan andere toepasselijke wetgeving, of als we uw persoonlijke gegevens nodig hebben om een rechtsvordering te vestigen, uit te oefenen of te verdedigen, dit uitsluitend op basis van need to know. Wij zullen het verwerken van uw persoonlijke gegevens voor dit soort beperkte doeleinden na de beëindiging van de contractuele relatie zoveel mogelijk beperken.</p>
                        <p><strong>5. Uw rechten – welke rechten heeft u en hoe kunt u die rechten uitoefenen? </strong></p>
                        <p><strong>Recht om uw toestemming in te trekken:</strong> als u uw toestemming heeft gegeven voor bepaalde vormen van verzamelen, verwerken en gebruik van uw persoonlijke gegevens (met name ten aanzien van het ontvangen van communicatie op het gebied van direct marketing via e-mail, SMS/MMS, fax en telefoon) kunt u die toestemming te allen tijde intrekken met ingang van de tijd daarna. Het intrekken van uw toestemming zal de wettigheid van de bewerking voorafgaand aan het intrekken van uw toestemming niet aantasten. Neem alsjeblieft contact met ons op zoals genoemd in sectie 7 als je de overeenkomst wilt annuleren.  </p>
                        <p><strong>Aanvullende rechten:</strong> conform toepasselijke wetgeving op het gebied van gegevensbescherming kunt u het recht hebben om: (i) toegang tot uw persoonlijke gegevens te verzoeken; (ii) rectificatie van uw persoonlijke gegevens te verzoeken; (iii) verwijdering van uw persoonlijke gegevens te verzoeken; (iv) beperking van de verwerking van uw persoonlijke gegevens te verzoeken; (v) overdraagbaarheid van gegevens te verzoeken; en/of (vi) bezwaar te maken tegen het verwerken van uw persoonlijke gegevens. </p>
                        <p>Houd er a.u.b. rekening mee dat de hierboven genoemde rechten beperkt kunnen zijn door lokale wetgeving op het gebied van gegevensbescherming. Hieronder vindt u meer informatie over uw rechten voor zover de Europese Algemene Verordening Gegevensbescherming (AVG) van toepassing is:</p>
                        <ul>
                            <li><p><strong>het recht om toegang tot uw persoonlijke gegevens te vragen:</strong> u kunt het recht heb-ben om van ons een bevestiging te krijgen over de persoonlijke gegevens die in verband met u al dan niet zijn verwerkt, en, als dat het geval is, om toegang tot die persoonlijke gegevens te vragen. De toegangsinformatie omvat – onder meer – de doelstellingen van het verwerken, de desbetreffende categorieën persoonlijke gegevens, en de ontvangers of categorieën ontvangers met wie de persoonlijke gegevens zijn gedeeld resp. zullen worden gedeeld. Dit is echter geen absoluut recht en de belangen van andere personen kunnen uw toegangsrecht beperken.</p>
                             <p>U kunt het recht hebben om kosteloos een kopie te krijgen van de persoonlijke gegevens die verwerkt worden. Voor verder door u aangevraagde kopieën kunnen wij een redelijk tarief in rekening brengen, dat gebaseerd is op administratieve kosten.</p></li>
                        </ul>
                        
                        <ul>
                            <li><p><strong>Recht om rectificatie te verzoeken:</strong>  u kunt het recht hebben om van ons de rectificatie van onjuiste persoonlijke gegevens die u betreffen te verkrijgen. Al naar gelang de doeleinden van het verwerken kunt u het recht hebben om onvolledige persoonlijke gegevens aan te vullen, met inbegrip van het indienen van een aanvullende verklaring.</p></li>
                            <li><p><strong>Recht om verwijdering te verzoeken (recht om vergeten te worden):</strong> onder bepaalde omstandigheden kunt u het recht hebben om van ons de verwijdering van persoonlijke gegevens in verband met u te verkrijgen en wij kunnen verplicht zijn om zulke persoonlijke gegevens te verwijderen.</p></li>
                            <li><p><strong>Recht om beperking van de verwerking te verzoeken:</strong> onder bepaalde omstandigheden kunt u het recht hebben om van ons het beperken van het verwerken uw persoonlijke gegevens te verkrijgen. In dat geval zullen de desbetreffende gegevens worden gemarkeerd en mogen ze door ons alleen voor bepaalde doeleinden worden verwerkt.</p></li>
                            <li><p><strong>Recht om overdraagbaarheid van gegevens te verzoeken:</strong> onder bepaalde omstandig&shy;heden kunt u het recht hebben om de persoonlijke gegevens die u betreffen, die u ons heeft verschaft, te ontvangen en wel in een gestructureerd, algemeen gebruikt en machi&shy;naal leesbaar formaat en u kunt het recht hebben om die gegevens door te geven aan een andere entiteit zonder verhindering door ons.</p></li>
                            <li><p><strong>Recht om bezwaar te maken:</strong></p></li>
                        </ul>
                        <div style="max-width: 800px; background-color: #CCC; margin: auto">
                            
                                        <p><strong>Onder bepaalde omstandigheden kunt u het recht hebben om te allen tijde bezwaar te maken, om redenen die verband houden met uw specifieke situatie, tegen het verwerken van uw persoonlijke gegevens door ons en wij kunnen verplicht worden om uw persoonlijke gegevens niet langer te verwerken. Een dergelijk recht om bezwaar te maken kan met name van toepassing zijn als wij uw persoonlijke gegevens verzamelen voor profileringsdoeleinden om uw belang voor onze producten en dienstverlening beter te begrijpen of als wij uw persoonlijke gegevens verzamelen voor direct marketing.</strong></p>
                                        <p><strong>Als u een recht heeft om bezwaar te maken en u dat recht uitoefent, zullen uw persoonlijke gegevens door ons niet langer voor zulke doeleinden worden gebruikt. U kunt dit recht uitoefenen door contact met ons op te nemen zoals omschreven in sectie 7 hieronder.</strong></p>
                                        <p><strong>Een dergelijk recht van bezwaar bestaat niet - in het bijzonder - als het verwerken van uw persoonlijke gegevens noodzakelijk is om stappen te ondernemen voor het aangaan van een contract of om een al gesloten contract uit te voeren.</strong></p>
                                        <p><strong>Als u niet langer direct marketing via e-mail, SMS/MMS, fax en telefoon wilt ont-vangen moet u uw toestemming intrekken, zoals hierboven uitgelegd (contact zoals beschreven in sectie 7). </strong></p>
                                  
                        </div>
                        <p>&nbsp;</p>
                        <p>Neem om uw rechten uit te oefenen a.u.b. contact met ons op zoals omschreven in sectie 7 hieronder. U heeft ook het recht om een klacht in te dienen bij de bevoegde toezichthoudende autoriteit gegevensbescherming.</p>
                        <p><strong>6. Cookies en andere volgtechnologie&euml;n</strong></p>
                        <p>Deze Website maakt gebruik van Cookies en andere volgtechnologieën. Raadpleeg a.u.b. ons Cookie-beleid als u meer wilt weten <a href="http://www.biore.nl/cookies" target="privacy">http://www.biore.nl/cookies</a>
                        </p>
                        <p><strong>7. Vragen en contactinformatie</strong></p>
                        <p>Als u een vraag heeft over deze kennisgeving of als u uw rechten wilt uitoefenen zoals hierboven in sectie 5 omschreven, neem dan a.u.b. contact met ons op <a href="http://www.kao.com/global/en/EU-Data-Subject-Request/" target="privacy">http://www.kao.com/global/en/EU-Data-Subject-Request/</a></p>

                        <p><strong>8. Veranderingen in het Privacybeleid </strong></p>
                        <p>Wij kunnen dit Privacybeleid van tijd tot tijd actualiseren naar aanleiding van gewijzigde wettelijke, regelgevende of operationele vereisten. Wij zullen u over zulke veranderingen informeren, waaronder wanneer zij in werking treden, door het updaten van de hiervoor weergegeven "Laatste Herziening" datum of op andere wijze zoals vereist op basis van toepasselijke wetgeving. Uw voortgezet gebruik van onze Website na iedere gerealiseerde update zal inhouden dat u de veranderingen accepteert. Als u updates van het Privacybeleid niet accepteert, zou u moeten stoppen met het gebruik van onze Website.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

