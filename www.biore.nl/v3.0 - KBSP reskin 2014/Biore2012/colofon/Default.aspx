﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Biore2012.colofon.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        #mainContent {
            min-height: 750px;

        }

        #mainContent .centeringDiv {
            margin-top: 3em;
        }

            #mainContent .centeringDiv p {
                margin: 1em 0;
            }

            #mainContent .centeringDiv div {
                margin: 2em 0;
            }

            #mainContent .centeringDiv {
                font-size: 16px;
            }

    </style>
    <div id="main">
        <div id="mainContent" style="min-height: 730px">
            <div id="shadow"></div>
            <div class="centeringDiv">
       
                 <h1 style="text-align:center">Colofon</h1>
                   <div id="responseRule"></div>
             
                <br />
                <h2>GUHL IKEBANA COSMETICS BV</h2>

                <div>
                    <p>
                        <b>ADRES</b><br />
                        Berkenweg 7<br />
                        3818 LA Amersfoort<br />
                        Nederland
                    </p>
                </div>

                <div>
                    <p>
                        <b>TELEFOON/FAX</b><br />
                        Tel.: +31 (0)85 303 64 22
                    </p>
                </div>

                <div>
                    <p>
                        <b>E-MAIL</b><br />
                        info@biore.nl
                    </p>
                </div>

                <div>
                    <p>
                        <b>BESTUURDERS</b><br />
                        Eric Brockhus, Berend van Iterson
                    </p>
                </div>

                <div>
                    <p>
                        <b>HANDELSREGISTER</b><br />
                        Guhl Ikebana Cosmetics BV<br />
                        KvK. no. 39032609<br />
                        BTW no. NL0051.62.452.B01
                    </p>
                </div>


                <br />
                <br />
            </div>
        </div>
    </div>
</asp:Content>
