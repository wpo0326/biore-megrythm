﻿<%@ Page Title="Get a Clean Face | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Get healthy, radiant skin with products from Bioré® Skincare" name="description" />
    <meta content="skincare, clean face" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/UniversPro67.css")
        .Add("~/css/homepage.css")
        .Add("~/css/flexslider.css")
        .Render("~/CSS/combinedhome_#.css")
    %>
    <meta name="viewport" content="width=device-width">

    <script type="text/javascript">
        $(document).ready(function () {
            //$('.scroll-dots a').click(function (event) {
            //    event.preventDefault();
            //    var target = $(this).attr('id');
            //    console.log(target);
            //});
        });
    </script>

    <script type="text/javascript">
         //$(function() {
         //    $('#theaterItem1 a').click(
         //        function (e) {
         //            e.preventDefault();

         //            var axel = Math.random() + "";
         //            var a = axel * 10000000000000;
         //            var strScript = "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore551;ord='" + a + "'?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"                     
         //                       + "<noscript>"
         //                       + "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore551;ord=1?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"
         //                       + "</noscript>";

         //            $('body').prepend(strScript);
         //            var target = $(this).attr('target');
         //            var uri = $(this).attr('href');

         //            setTimeout(function () {
         //                if (target) {
         //                    window.open(uri, '_blank');
         //                } else {
         //                    window.location = uri;
         //                }
         //            }, 1000);
                     
         //        });
         //})
    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">


<script type="text/javascript">
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    document.write('<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore916;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore916;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>

<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main1">
    <div class="home-content">        
        <section class="home-section" id="section-01">
            <div class="welcome-module">          
                <video autoplay="autoplay" loop="loop" muted="muted" preload="auto" id="welcomeVideo">
                    <source src="<%= VirtualPathUtility.ToAbsolute("~/video/homepage/shayzilla-dev2.mp4") %>" type="video/mp4"/>
                    <source src="<%= VirtualPathUtility.ToAbsolute("~/video/homepage/shayzilla-dev2.ogv") %>" type="video/ogg"/>
                    <source src="<%= VirtualPathUtility.ToAbsolute("~/video/homepage/shayzilla-dev2.webm") %>" type="video/webm"/>
                </video>
                <div id="welcomeImage">
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/shayzilla-dev.png") %>"/>
                </div>
                <div class="welcome-module-content">
                    <h1>FIND YOUR WAY TO<br><span>COME CLEAN</span></h1>
                    <p>Dirty little secrets—we've all got 'em. Dirt, oil and blackheads hiding in our pores.<br> Let Bior&eacute;<sup>&reg;</sup> beauty and actress Shay Mitchell show you how to clean them away. </p>
                    <div class="welcome-video">
                        <a href="https://www.youtube.com/watch?v=TVBZqfDfAgE" target="_blank" class="fancybox-media fbm"></a>
                    </div>
                    <a href="#section-02"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/arrowDown.png") %>" class="section-down-arrow"/></a>
                </div>
            </div>
            <div class="scroll-dots">
                <a href="#section-01"><span class="scroll-dot enable"></span></a>
                <a href="#section-02"><span class="scroll-dot"></span></a>
                <a href="#section-03"><span class="scroll-dot"></span></a>
                <a href="#section-04"><span class="scroll-dot"></span></a>
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section" id="section-02">
            <div class="bg-left"></div><div class="bg-right"></div>
            <div class="container3">
	            <div class="container2">
		            <div class="container1">
			            <div class="headline">
				            <h4>GET FIZZY WITH IT</h4>
			            </div> 
			            <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/watch?v=B8bmBAlKF14" target="_blank" class="fancybox-media fbm"></a></div>
                                <a href="https://www.youtube.com/watch?v=B8bmBAlKF14" target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/01-baking-soda-scrub.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/01-baking-soda-scrub.gif") %>">                                
                                </a>
                                <div class="animation-skin"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/01_baking_soda_cleansing_scrub.png") %>"/></div>
                                <div class="animation-play-btn">                                    
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>
                                </div>
                            </div>
			            </div>
			            <div class="product">
				            <p>Bior&eacute;<sup>&reg;</sup> Baking Soda Cleansing Scrub</p>
			            </div>
		            </div>
	            </div>
            </div>

            <div class="scroll-dots">
                <a href="#section-01"><span class="scroll-dot"></span></a>
                <a href="#section-02"><span class="scroll-dot enable"></span></a>
                <a href="#section-03"><span class="scroll-dot"></span></a>
                <a href="#section-04"><span class="scroll-dot"></span></a>
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section float-right" id="section-03">
            <div class="bg-left"></div><div class="bg-right"></div>
            <div class="container3">
	            <div class="container2">
		            <div class="container1">
			            <div class="headline">
				            <h4>BRING ON<br> THE BUBBLY</h4>
			            </div>
			            <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/watch?v=_RyteX6GhNA" target="_blank" class="fancybox-media fbm"></a></div>
                                <a href="https://www.youtube.com/watch?v=_RyteX6GhNA"  target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/02-baking-soda-cleanser.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/02-baking-soda-cleanser.gif") %>">                                
                                </a>
                                <div class="animation-skin"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/02_baking_soda_pore_cleanser.png") %>"/></div>
                                <div class="animation-play-btn">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>
                                </div>
                            </div>
			            </div>
			            <div class="product">
				            <p>Bior&eacute;<sup>&reg;</sup> Baking Soda Pore Cleanser</p>
			            </div>
		            </div>
	            </div>
            </div>

            <div class="scroll-dots">
                <a href="#section-01"><span class="scroll-dot"></span></a>
                <a href="#section-02"><span class="scroll-dot"></span></a>
                <a href="#section-03"><span class="scroll-dot enable"></span></a>
                <a href="#section-04"><span class="scroll-dot"></span></a>
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section"" id="section-04">
            <div class="bg-left"></div><div class="bg-right"></div>
            <div class="container3">
	            <div class="container2">
		            <div class="container1">
			            <div class="headline">
				            <h4>BE <br>MAG-<br>NETIC</h4>
			            </div>
			            <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/watch?v=mi7cep5KukM" target="_blank" class="fancybox-media fbm"></a></div>
                                <a href="https://www.youtube.com/watch?v=mi7cep5KukM" target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/03-charcoal-strips.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/03-charcoal-strips.gif") %>">                                
                                </a>
                                <div class="animation-skin"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/03_charcoal_pore_strips.png") %>"/></div>
                                <div class="animation-play-btn">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>
                                </div>
                            </div>
			            </div>
			            <div class="product">
				            <p>Bior&eacute;<sup>&reg;</sup> Deep Cleansing Charcoal Pore Strips</p>
			            </div>
		            </div>
	            </div>
            </div>

            <div class="scroll-dots">
                <a href="#section-01"><span class="scroll-dot"></span></a>
                <a href="#section-02"><span class="scroll-dot"></span></a>
                <a href="#section-03"><span class="scroll-dot"></span></a>
                <a href="#section-04"><span class="scroll-dot enable"></span></a>
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section float-right" id="section-05">
            <div class="bg-left"></div><div class="bg-right"></div>
            <div class="container3">
	            <div class="container2">
		            <div class="container1">
			            <div class="headline">
				            <h4>DRAW OUT THE DIRT</h4>
			            </div>
			            <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/watch?v=xhStsDgvRMM" target="_blank" class="fancybox-media fbm"></a></div>
                                <a href="https://www.youtube.com/watch?v=xhStsDgvRMM" target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/04-charcoal-pore-cleanser.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/04-charcoal-pore-cleanser.gif") %>">                                
                                </a>
                                <div class="animation-skin"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/04_deep_pore_charcoal_cleanser.png") %>"/></div>
                                <div class="animation-play-btn">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>
                                </div>
                            </div>
			            </div>
			            <div class="product">
				            <p>Bior&eacute;<sup>&reg;</sup> Deep Pore Charcoal Cleanser</p>
			            </div>
		            </div>
	            </div>
            </div>

            <div class="scroll-dots">
                <a href="#section-01"><span class="scroll-dot"></span></a>
                <a href="#section-02"><span class="scroll-dot"></span></a>
                <a href="#section-03"><span class="scroll-dot"></span></a>
                <a href="#section-04"><span class="scroll-dot"></span></a>
                <a href="#section-05"><span class="scroll-dot enable"></span></a>
            </div>
        </section>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/jquery.flexslider.min.js")
        .Render("~/js/combinedhome_#.js")
    %>
</asp:Content>