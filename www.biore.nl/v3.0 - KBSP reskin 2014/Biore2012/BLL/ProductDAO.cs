﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.IO;
using System.Web.Caching;

namespace Biore2012.BLL
{
    public class ProductDAO
    {
        public static Product GetProduct(string ProductRefName)
        {
            Product fileContent = (Product)HttpContext.Current.Cache["ProductFile-" + ProductRefName];
            if (fileContent == null)
            {
                string src = System.Configuration.ConfigurationManager.AppSettings["XMLDataPath"] + "\\" + ProductRefName + ".xml";

                XmlSerializer mySerializer = new XmlSerializer(typeof(Product), new XmlRootAttribute("Product"));

                FileStream myFileStream = null;
                try
                {
                    myFileStream = new FileStream(src, FileMode.Open, FileAccess.Read);
                }
                catch (Exception)
                {
                    throw new HttpException(404, "Category not found");
                }
                
                try
                {
                    fileContent = (Product)mySerializer.Deserialize(myFileStream);
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    myFileStream.Close();
                }

                HttpContext.Current.Cache.Insert("ProductFile-" + ProductRefName, fileContent, new CacheDependency(src));
            }
            return fileContent;
        }
    }
}
