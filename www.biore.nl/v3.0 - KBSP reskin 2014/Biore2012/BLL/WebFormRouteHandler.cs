﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Security;
using System.Web.Compilation;
using System.Web.UI;

namespace Biore2012.BLL
{
    public interface IRoutablePage
    {
        RoutingHelper Routing { get; set; }
    }

    public class WebFormRouteHandler : IRouteHandler
    {
        public WebFormRouteHandler(string virtualPath)
            : this(virtualPath, true)
        {
        }

        public WebFormRouteHandler(string virtualPath, bool checkPhysicalUrlAccess)
        {
            this.VirtualPath = virtualPath;
            this.CheckPhysicalUrlAccess = checkPhysicalUrlAccess;
        }

        public string VirtualPath { get; private set; }

        public bool CheckPhysicalUrlAccess { get; set; }

        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            if (requestContext.HttpContext.User != null)
            {
                if (this.CheckPhysicalUrlAccess && !UrlAuthorizationModule.CheckUrlAccessForPrincipal(
                    this.VirtualPath,
                    requestContext.HttpContext.User,
                    requestContext.HttpContext.Request.HttpMethod))
                {
                    throw new SecurityException();
                }
            }

            var page = BuildManager.CreateInstanceFromVirtualPath(
                this.VirtualPath,
                typeof(Page)) as IHttpHandler;

            if (page != null)
            {
                var routablePage = page as IRoutablePage;

                if (routablePage != null) routablePage.Routing = new RoutingHelper(requestContext);
            }

            return page;
        }
    }
}
