﻿var tpj = jQuery;

tpj(document).ready(function () {
    BuyNow.init();
});

var BuyNow = {

    isMobile: function () {

        if (tpj(window).width() < 768) {
            return true;
        }
        return false;
    },
    openModal: function (name) {
        tpj.colorbox({ width: '980px', height: '740px', iframe: true, scrolling: false, href: '../buynow/BuyNow.aspx?RefName=' + name });
    },
    openTab:function(name){
        window.location = '../buynow/BuyNow.aspx?RefName=' + name;
    },
    get: function (element) {
        var name = tpj(element).parent().attr('id');
        return name;
    },
    eventHandler: function () {
        var that = this;
        tpj('.buyModal').click(function () {
            
            var name = that.get($(this));
            if (that.isMobile()) {
                that.openTab(name);
            } else {
                that.openModal(name);
            }
            return false;
        });
    },
    init: function () {
        this.eventHandler();
    }

}