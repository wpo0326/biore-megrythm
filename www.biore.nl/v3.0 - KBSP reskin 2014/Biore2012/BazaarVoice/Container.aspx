﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Container.aspx.cs" Inherits="Biore.BazaarVoice.Container" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" >
<lang="en">
    <head>
        <meta charset="utf-8"/>
        <title>BV Container Page</title>
        <meta name="robots" content="noindex, nofollow"/>
        <link rel="canonical" href="TODO.htm"/>
    </head>
    <body>
        <%= BVScript %>
        <script>$BV.container('global', {});</script>
    </body>
</html>