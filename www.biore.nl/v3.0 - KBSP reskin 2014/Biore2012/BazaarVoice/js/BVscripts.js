﻿$(document).ready(function () {
    // Event to solldown to review tab and active the tab when the user click on show review 
    $BV.ui('rr', 'show_reviews', {
        doShowContent: function () {
            $('#productInfo div.contentHolder').removeClass('open');
            $('.tabReviews').addClass('open');
            $('#BVRRContainer').show();
            Tagging.tracking("BazaarVoice Reviews","Click","ViewReview");
        }
    });
});