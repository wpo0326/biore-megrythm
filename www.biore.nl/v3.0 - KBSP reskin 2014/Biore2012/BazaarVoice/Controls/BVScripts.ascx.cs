﻿using System;
using System.Configuration;
using System.Text;

namespace Biore.BazaarVoice.Controls
{
    public partial class BVScripts : System.Web.UI.UserControl
    {
        /// <summary>
        /// This renders the BV Product ID, based on the pre-requisite that 
        /// the product ID registered on BV is the same as the product page name.
        /// </summary>
        public string BVProductScript
        {
            get
            {
                string category = string.Empty;
                string productId = BVUtils.GetProductId(Request, out category);
                if (!string.IsNullOrEmpty(productId))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendFormat("<script type=\"text/javascript\" src=\"{0}\"></script>", ConfigurationManager.AppSettings["BV_APIUrl"]);
                    
                    if (!string.IsNullOrEmpty(category)) // this is a Category page
                    {
                        // PproductId becomes category id
                        string products = ConfigurationManager.AppSettings["BV_Cat_" + category];
                        sb.AppendFormat("<script type=\"text/javascript\"> $BV.ui( 'rr', 'inline_ratings', {{productIds : [{0}],containerPrefix : 'BVRRInlineRating'}});</script>", products);
                    }
                    else
                    {
                        // set up Product ID
                        sb.AppendFormat("<script type=\"text/javascript\">$BV.configure('global', {{ productId: '{0}' }});</script>", productId);
                        // Ratings & Reviews code
                        //sb.Append("<script type=\"text/javascript\">$BV.ui('rr', 'show_reviews', {doShowContent: function () { $j('#productTabs ul li a').removeClass('active'); $j('.tabReviews a').addClass('active'); $j('#tabsContent div').removeClass('active').hide(); $j('.contentReviews').addClass('active').show(); $j('#BVRRContainer').show();}});</script>");
                    }
                    //return sb.ToString();
                    return string.Empty;  //we're not using BV for these locales.
                }
                else
                {
                    return string.Empty;
                }
            }
        }
    }
}