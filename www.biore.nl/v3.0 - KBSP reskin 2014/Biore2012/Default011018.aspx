﻿ <%@ Page Title="Krijg een schone huid | Bioré® Huidverzorging" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Krijg een gezonde en stralende huid met Bioré® Huidverzorging " name="description" />
    <meta content="huidverzorging, schoon gezicht, diepe reiniging, gezichtsverzorging, zuiverings-soda, houtskool " name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/UniversPro67.css")
        .Add("~/css/homepage.css")
        .Add("~/css/flexslider.css")
        .Render("~/CSS/combinedhome_#.css")
    %>
    <meta name="viewport" content="width=device-width">

    <script type="text/javascript" src="<%= VirtualPathUtility.ToAbsolute("~/js/homepage.js") %>"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            //$('.scroll-dots a').click(function (event) {
            //    event.preventDefault();
            //    var target = $(this).attr('id');
            //    console.log(target);
            //});
        });
    </script>

    <script type="text/javascript">
         $(function() {
             $('#theaterItem1 a').click(
                 function (e) {
                     e.preventDefault();

                     var axel = Math.random() + "";
                     var a = axel * 10000000000000;
                     var strScript = "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore551;ord='" + a + "'?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"                     
                                + "<noscript>"
                                + "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore551;ord=1?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"
                                + "</noscript>";

                     //$('body').prepend(strScript);
                     var target = $(this).attr('target');
                     var uri = $(this).attr('href');

                     setTimeout(function () {
                         if (target) {
                             window.open(uri, '_blank');
                         } else {
                             window.location = uri;
                         }
                     }, 1000);
                     
                 });
         })
    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main1">
    <div class="home-content">        
        <section class="home-section iex" id="section-01">
            <!--<div class="newicon">
                <img src="/images/homepage/new-top-icon.png" alt="new">
            </div>-->
            <div class="animation overwritte">
                <div class="animation-button">
                            
                    <a href="https://www.youtube.com/embed/E4BdM4gxnEg?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm  fancybox.iframe">
                                    <img src="/images/homepage/top-play-btn.png" alt="video play button"  />    
                    </a>
                            
                </div>
			</div>
            <div class="welcome-module">          
                <!--<video autoplay="autoplay" loop="loop" muted="muted" preload="auto" id="welcomeVideo">
                    <source src="<%= VirtualPathUtility.ToAbsolute("~/video/homepage/shayzilla.mp4") %>" type="video/mp4"/>
                    <source src="<%= VirtualPathUtility.ToAbsolute("~/video/homepage/shayzilla.ogv") %>" type="video/ogg"/>
                    <source src="<%= VirtualPathUtility.ToAbsolute("~/video/homepage/shayzilla.webm") %>" type="video/webm"/>
                </video>-->
                <div id="welcomeImage" class="hide-mobile show" style="height:100%">
                    <img class="ïmg-less" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/charcoal-top-model-bg.jpg") %>"/>
                </div>
                <div id="welcomeImage" class="hide-desktop">
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/charcoal-top-model-bg.jpg") %>"/>
                </div>
                <div class="welcome-module-content">
                    <h1>ONTDEK BIORÉ – DE <br />EXPERT VOOR HET REINIGEN <br />VAN JE PORIËN</h1>

                    <div class="centeringDiv alignCenter">
                        <h2 class="white-headline">BEVRIJD DE PORIËN.</h2>
                        <h3 class="orange-headline">BESTRIJD PUISTJES.</h3>
                        <h4 class="sub-headline">BIORÉ MET HOUTSKOOL EN BAKING SODA<br />VOOR SCHONERE HUID IN SLECHTS 2 DAGEN.</h4>
                    </div>
                    <!--               
                    <p class="welcome-module-line1">EN DE DIEP REINIGENDE KRACHT VAN</p>
                    <p class="welcome-module-line2">HOUTSKOOL EN ZUIVERINGS-SODA</p>-->
                    
                </div>
                 <div class="hide-mobile">
                   
                </div>
                <div class="bubbles-bg hide-mobile">
                     
                     <div class="bubbles-charcoal-active">
                         <img class="products-responsive" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/charcoal-anti-pimple-cleanser.png") %>"/>
                     </div>
                     <div class="bubbles-charcoal-bakingsoda">
                         <img class="products-responsive" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/charcoal-anti-pimple-scrub.png") %>"/>
                     </div>
         
                </div>
                <p class="scrollDown">SCROLL DOWN</p>
                    <div id="arrowBounce" class="hide-mobile">
                        <a href="#section-02"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/arrowDown.png") %>" class="section-down-arrow"/></a>
                    </div>
               
            </div>
            <div class="scroll-dots">
                <a href="#section-01"><span class="scroll-dot enable"></span></a>
                <a href="#section-02"><span class="scroll-dot"></span></a>
                <a href="#section-03"><span class="scroll-dot"></span></a>
                <a href="#section-04"><span class="scroll-dot"></span></a>
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
            <!--<div class="white-shadow hide-mobile"></div>-->
        </section>
        <section class="home-section" id="section-06">
            <div class="bg-left"></div>
            <div class="bg-right"></div>
            <div class="container3">
                <div class="container2">
                    <div class="container1">
                        <div class="headline">
                            <h4>ZEG DAG TEGEN PUISTJES MET ONZE HOUTSKOOL CLEANSER! </h4>
                        </div>
                        <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/embed/ffRpOxRF6Wg?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                <a href="https://www.youtube.com/embed/ffRpOxRF6Wg?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2-2018.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2-2018.gif") %>">
                                </a>
                                <div class="animation-skin-container">
                                    <div class="animation-skin">
                                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/06_anti_pickel-charcoal.png") %>" /></div>
                                </div>
                                <div class="animation-play-btn">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>" />
                                </div>
                            </div>
                        </div>
                        <div class="product">
                            <p>ONTDEK HOE JE EEN SCHONERE HUID KRIJGT IN SLECHTS 2 DAGEN MET DE ANTI-PUISTJES CLEANSER MET HOUTSKOOL. </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="scroll-dots">
                <a href="#section-01"><span class="scroll-dot"></span></a>
                <a href="#section-06"><span class="scroll-dot enable"></span></a>
                <a href="#section-02"><span class="scroll-dot"></span></a>
                <a href="#section-03"><span class="scroll-dot"></span></a>
                <a href="#section-04"><span class="scroll-dot"></span></a>
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section"" id="section-02">
            <div class="bg-left"></div><div class="bg-right"></div>
            <div class="container3">
	            <div class="container2">
	                <div class="container1">
			            <div class="headline">
				            <h4>Get fizzy en breng je huid in balans</h4>
			            </div> 
			            <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/embed/7Me0uDTQKGg?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                <a href="https://www.youtube.com/embed/7Me0uDTQKGg?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section3.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section3.gif") %>">                                
                                </a>
                                <div class="animation-skin-container">
                                    <div class="animation-skin"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/01_baking_soda_cleansing_scrub.png") %>"/></div>
                                </div>
                                <div class="animation-play-btn">                                    
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>
                                </div>
                            </div>
			            </div>
			            <div class="product">
				            <p>Ontdek hoe de Cleansing Poeder Scrub met Zuiverings-Soda werkt</p>
			            </div>
		            </div>
	            </div>
            </div>

            <div class="scroll-dots">
                <a href="#section-01"><span class="scroll-dot"></span></a>
                <a href="#section-06"><span class="scroll-dot"></span></a>
                <a href="#section-02"><span class="scroll-dot enable"></span></a>
                <a href="#section-03"><span class="scroll-dot"></span></a>
                <a href="#section-04"><span class="scroll-dot"></span></a>
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section" id="section-03">
            <div class="bg-left"></div><div class="bg-right"></div>
            <div class="container3">
	            <div class="container2">
		            <div class="container1">
			            <div class="headline">
				            <h4>Be magnetic en verwijder al het vet, vuil en onzuiverheden!</h4>
			            </div>
			            <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/embed/oUX-jHi32BI?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                <a href="https://www.youtube.com/embed/oUX-jHi32BI?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2.gif") %>">                                
                                </a>
                                <div class="animation-skin-container">
                                    <div class="animation-skin"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/03_charcoal_pore_strips.png") %>"/></div>
                                </div>
                                    <div class="animation-play-btn">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>
                                </div>
                            </div>
			            </div>
			            <div class="product">
				            <p>Bekijk hoe je de Diep Reinigende Poriestrips met Houtskool gebruikt</p>
			            </div>
		            </div>
	            </div>
            </div>

            <div class="scroll-dots">
                <a href="#section-01"><span class="scroll-dot"></span></a>
                <a href="#section-06"><span class="scroll-dot"></span></a>
                <a href="#section-02"><span class="scroll-dot"></span></a>
                <a href="#section-03"><span class="scroll-dot enable"></span></a>
                <a href="#section-04"><span class="scroll-dot"></span></a>
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section" id="section-04">
            <div class="bg-left"></div><div class="bg-right"></div>
            <div class="container3">
	            <div class="container2">
		            <div class="container1">
			            <div class="headline">
				            <h4>Turn up the heat en pak het vuil aan!</h4>
			            </div> 
			            <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/embed/F364bE8rctA?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                <a href="https://www.youtube.com/embed/F364bE8rctA?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section4.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section4.gif") %>">                                
                                </a>
                                <div class="animation-skin-container">
                                    <div class="animation-skin"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/03_self-heating-mask.png") %>"/></div>
                                </div>
                                <div class="animation-play-btn">                                    
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>
                                </div>
                            </div>
			            </div>
			            <div class="product">
				            <p>Bekijk hier hoe je het Zelfverwarmend 1 Minute Masker gebruikt</p>
			            </div>
		            </div>
	            </div>
            </div>

            <div class="scroll-dots">
                <a href="#section-01"><span class="scroll-dot"></span></a>
                <a href="#section-06"><span class="scroll-dot"></span></a>
                <a href="#section-02"><span class="scroll-dot"></span></a>
                <a href="#section-03"><span class="scroll-dot"></span></a>
                <a href="#section-04"><span class="scroll-dot enable"></span></a>
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section" id="section-05">
            <div class="bg-left"></div><div class="bg-right"></div>
            <div class="container3">
	            <div class="container2">
		            <div class="container1">
			            <div class="headline">
				            <h4>Verstop je niet meer achter je masker!<br />Bevrijd je poriën en onthul je mooie gezicht </h4>
			            </div> 
			            
			            <div class="animation">
			                <a href="https://www.youtube.com/embed/E4BdM4gxnEg?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe">
			                    <span style="width:100%; height:650px;position:absolute;top:0;left:0;z-index:300;display:block;"></span>
			                 </a>
			            
                            <div class="animation-container">
                                    <img id="imganim" class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section5.gif") %>" alt="Verstop je niet meer achter je masker!" />                                
                                
                                <!--<div class="animation-skin-container">
                                    <div class="animation-skin"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/01_baking_soda_cleansing_scrub.png") %>"/></div>
                                </div>-->
                                <!--<div class="animation-play-btn">                                    
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>
                                </div>-->
                            </div>
			            </div>
			            <div class="product">
				            <p>...en volg ons via</p>
				            <div class="social-container">
				                <div class="facebook"><a href="https://www.facebook.com/BioreBenelux/" target="_blank"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/facebook.png") %>" alt="facebook" /></a></div>
				                <div class="instagram"><a href="https://www.instagram.com/biore.benelux/" target="_blank"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/instagram.png") %>" alt="instagram" /></a></div>
				                <div class="youtube"><a href="https://www.youtube.com/channel/UCzgS-K5O_3msebMTuYMldLA" target="_blank"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/youtube.png") %>" alt="youtube" /></a></div>
				            </div>
				            <p>#freeyourpores</p>
			            </div>
		            </div>
	            </div>
            </div>

            <div class="scroll-dots">
                <a href="#section-01"><span class="scroll-dot"></span></a>
                <a href="#section-06"><span class="scroll-dot"></span></a>
                <a href="#section-02"><span class="scroll-dot"></span></a>
                <a href="#section-03"><span class="scroll-dot"></span></a>
                <a href="#section-04"><span class="scroll-dot"></span></a>
                <a href="#section-05"><span class="scroll-dot enable"></span></a>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">

    $(function() {


        var viewport = {
            width: $(window).width(),
            height: $(window).height()
        };

        var loaded = false;

        if (viewport.width > 800 && loaded == false) {
            var section5img = new Image();
            section5img.src = '/images/homepage/gif/output.gif';
            section5img.onload = function() {
                document.getElementById('imganim').src = section5img.src;
            }
            loaded = true;
        }
    });

</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/jquery.flexslider.min.js")
        .Render("~/js/combinedhome_#.js")
    %>
</asp:Content>