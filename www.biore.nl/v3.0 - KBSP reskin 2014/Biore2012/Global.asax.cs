﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Routing;
using Biore2012.BLL;

//Add "Back Off Big Pores" category -11/19/14 MCT
//Add "Take It All Off" category - 9/18/18 MCT

namespace Biore2012
{
    public class Global : System.Web.HttpApplication
    {
        public static RouteCollection GlobalRoutes;

        protected void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes(RouteTable.Routes);
        }

        public static void RegisterRoutes(RouteCollection Routes)
        {
            //If you use this you need to have the file in the physical directory for the url you are requesting.
            RouteTable.Routes.RouteExistingFiles = false;

            //Example URL:              http://localhost/en-CA/Home
            //Web.AppSettings.config:   <add key="homepage" value="Home" />

            RouteTable.Routes.Add(new Route("{resource}.axd/{*pathInfo}", new StopRoutingHandler()));


            Routes.Add("HomepageRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["HomepageRoute"],
            new RouteValueDictionary { { "routename", "HomepageRoute" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/default.aspx")));


            Routes.Add("DeepCleansingProductDetailRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["DeepCleansingProductDetailRoute"],
            new RouteValueDictionary { { "routename", "DeepCleansingProductDetailRoute" }, { "pid", "" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/biore-facial-cleansing-products/ProductDetail.aspx")));

            Routes.Add("DontBeDirtyRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["DontBeDirtyRoute"],
            new RouteValueDictionary { { "routename", "DontBeDirty" }, { "pid", "" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/biore-facial-cleansing-products/ProductDetail.aspx")));

            Routes.Add("AcnesOuttaHereRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["AcnesOuttaHereRoute"],
            new RouteValueDictionary { { "routename", "AcnesOuttaHere" }, { "pid", "" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/biore-facial-cleansing-products/ProductDetail.aspx")));

            Routes.Add("KlaarMetPuistjesRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["KlaarMetPuistjesRoute"],
            new RouteValueDictionary { { "routename", "KlaarMetPuistjes" }, { "pid", "" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/biore-facial-cleansing-products/ProductDetail.aspx")));

            Routes.Add("ComplexionClearingProductDetailRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["ComplexionClearingProductDetailRoute"],
            new RouteValueDictionary { { "routename", "ComplexionClearingProductDetailRoute" }, { "pid", "" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/biore-facial-cleansing-products/ProductDetail.aspx")));

            Routes.Add("MakeUpRemovingProductDetailRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["MakeUpRemovingProductDetailRoute"],
            new RouteValueDictionary { { "routename", "MakeUpRemovingProductDetailRoute" }, { "pid", "" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/biore-facial-cleansing-products/ProductDetail.aspx")));

            Routes.Add("TakeItAllOffRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["TakeItAllOffRoute"],
            new RouteValueDictionary { { "routename", "TakeItAllOff" }, { "pid", "" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/biore-facial-cleansing-products/ProductDetail.aspx")));

            // BWR-80
            Routes.Add("BreakupWithBlackheadsPoreStripsRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["BreakupWithBlackheadsPoreStripsRoute"],
            new RouteValueDictionary { { "routename", "BreakupWithBlackheadsPoreStripsRoute" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/biore-facial-cleansing-products/ProductDetail.aspx")));

            // BWR-80
            Routes.Add("BreakupWithBlackheadsRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["BreakupWithBlackheadsRoute"],
            new RouteValueDictionary { { "routename", "BreakupWithBlackheadsRoute" }, { "pid", "" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/biore-facial-cleansing-products/ProductDetail.aspx")));

            // MCT
            Routes.Add("BackOffBigPoresRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["BackOffBigPoresRoute"],
            new RouteValueDictionary { { "routename", "BackOffBigPoresRoute" }, { "pid", "" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/biore-facial-cleansing-products/ProductDetail.aspx")));


            Routes.Add("PoreStripsRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["PoreStripsRoute"],
            new RouteValueDictionary { { "routename", "PoreStripsRoute" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/biore-facial-cleansing-products/ProductDetail.aspx")));

            Routes.Add("ContactRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["ContactRoute"],
            new RouteValueDictionary { { "routename", "ContactRoute" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/FormConfig/ContactUs.aspx")));


            Routes.Add("MissingFormRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["MissingFormRoute"],
            new RouteValueDictionary { { "routename", "MissingFormRoute" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/FormConfig/form.aspx")));
            
            //Routes.Add("MobileContactRoute",
            //new Route(System.Configuration.ConfigurationManager.AppSettings["MobileContactRoute"],
            //new RouteValueDictionary { { "routename", "MobileContactRoute" } },
            //new RouteValueDictionary { { "", "" } },
            //new WebFormRouteHandler("~/FormConfig/form.aspx")));

            Routes.Add("NewsletterSignUpRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["NewsletterSignUpRoute"],
            new RouteValueDictionary { { "routename", "NewsletterSignUpRoute" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/FormConfig/form.aspx")));

            Routes.Add("UnsubscribeRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["UnsubscribeRoute"],
            new RouteValueDictionary { { "routename", "UnsubscribeRoute" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/FormConfig/unsubscribe.aspx")));

            Routes.Add("PromotionEndedRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["PromotionEndedRoute"],
            new RouteValueDictionary { { "routename", "PromotionEndedRoute" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/FormConfig/PromotionOver.aspx")));
            
            Routes.Add("OptinRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["OptinRoute"],
            new RouteValueDictionary { { "routename", "OptinRoute" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/FormConfig/form.aspx")));

            Routes.Add("MobileNewsletterSignUpRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["MobileNewsletterSignUpRoute"],
            new RouteValueDictionary { { "routename", "MobileNewsletterSignUpRoute" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/FormConfig/form.aspx")));

            Routes.Add("CustomFormProveItSampleRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["CustomFormProveItSampleRoute"],
            new RouteValueDictionary { { "routename", "CustomFormProveItSampleRoute" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/FormConfig/formCustomProveIt1.aspx")));

            Routes.Add("CustomFormProveItIMediaSampleRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["CustomFormProveItIMediaSampleRoute"],
            new RouteValueDictionary { { "routename", "CustomFormProveItIMediaSampleRoute" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/FormConfig/formCustomProveIt2.aspx")));

            Routes.Add("CustomFormProveItChowSampleRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["CustomFormProveItChowSampleRoute"],
            new RouteValueDictionary { { "routename", "CustomFormProveItChowSampleRoute" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/FormConfig/formCustomProveIt3.aspx")));

            Routes.Add("CustomFormProveItBFICSampleRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["CustomFormProveItBFICSampleRoute"],
            new RouteValueDictionary { { "routename", "CustomFormProveItBFICSampleRoute" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/FormConfig/formCustomProveIt4.aspx")));

            Routes.Add("CustomFormProveItCSBCSampleRoute",
            new Route(System.Configuration.ConfigurationManager.AppSettings["CustomFormProveItCSBCSampleRoute"],
            new RouteValueDictionary { { "routename", "CustomFormProveItCSBCSampleRoute" } },
            new RouteValueDictionary { { "", "" } },
            new WebFormRouteHandler("~/FormConfig/formCustomProveIt5.aspx")));

            GlobalRoutes = Routes;
        }
    

        protected void Session_Start(object sender, EventArgs e)
        {
            //HttpRequest httpRequest = HttpContext.Current.Request;
            //HttpContext.Current.Response.Write("Is Mobile: " + httpRequest.Browser.IsMobileDevice.ToString() + "<br/>");
            //HttpContext.Current.Response.Write("Is Tablet: " + httpRequest.Browser["is_tablet"].ToLower());
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            // Get the exception object.
            Exception exc = Server.GetLastError();

            // Handle HTTP errors
            if (exc.GetType() == typeof (HttpException))
            {
                if ((exc.Message.ToLower().Contains("file") && exc.Message.ToLower().Contains("does not exist")))
                {
                    string sPath = System.Configuration.ConfigurationManager.AppSettings["path"] +"404/";

                    Response.Redirect(sPath);
                }
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}