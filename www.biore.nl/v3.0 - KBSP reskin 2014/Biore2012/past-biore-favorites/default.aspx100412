﻿<%@ Page Title="Discontinued Products | Bioré® Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.past_biore_favorites._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Can't find your favorites? See the changes to the Bioré® Skincare lineup." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/productChanges.css")
        .Render("~/css/combinedpast_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="intro">
                <h1>
                    Check Out What's Changed 
                    <img src="../images/productChanges/bubbles1.png" id="bubbles1" alt="" />
                </h1>
                <p>See the updates we've made to our lineup, and products you'll want to try. </p>
            </div>            
            <div id="new">
                <a href="../deep-cleansing-products/combination-skin-balancing-cleanser"><img src="../images/productChanges/combinationSkinBalancingCleanser.jpg" border="0" class="prodImg" alt="" /></a>
                <div class="copy">
                    <h3>Combination Skin Balancing Cleanser</h3>
                    <p>Got skin that can't make up its mind? Oily in some places, dry in others? Get an even, all-over clean with our new cleanser, specially formulated for combination skin.</p>
                </div>
            </div>
            <div id="newName">
                <h2><img src="../images/productChanges/sameFormulaH.png" alt="Same Great Formula, Different Name" /></h2>
                <ul class="prodList">
                    <li>
                        <div class="itemWrapper">
                            <div class="content">
                                <p class="old">Revitalize 4-in-1 Self Foaming Cleanser</p>
                                <span class="isNow">
                                    <img src="../images/productChanges/isNowH.png" class="normal" alt="is now" />
                                    <img src="../images/productChanges/isNowOverH.png" class="over" alt="is now" />
                                </span>
                                <p class="now">4-in-1 Detoxifying Cleanser</p>
                            </div>
                            <img src="../images/productChanges/4In1.jpg" class="prodImg" alt="" />
                            <a href="../deep-cleansing-products/4-in-1-detoxifying-cleanser"></a>
                        </div>
                    </li>
                    <li>
                        <div class="itemWrapper">
                            <div class="content">
                                <p class="old">Triple Action Astringent</p>
                                <span class="isNow">
                                    <img src="../images/productChanges/isNowH.png" class="normal" alt="is now" />
                                    <img src="../images/productChanges/isNowOverH.png" class="over" alt="is now" />
                                </span>
                                <p class="now">Blemish Treating Astringent</p>
                            </div>
                            <img src="../images/productChanges/astringent.jpg" class="prodImg" alt="" />
                        <a href="../complexion-clearing-products/blemish-treating-astringent"></a>
                        </div>
                    </li>
                </ul>
            </div>
            <div id="tryInstead">
                <h2><img src="../images/productChanges/tryThisInsteadH.png" alt="Try This Instead" /></h2>
                <p class="introCopy">Don't panic. We've got you covered.</p>
                <ul class="prodList">
                    <li>
                        <div class="itemWrapper">
                            <div class="content">
                                <span class="insteadOf"><img src="../images/productChanges/insteadOfH.png" alt="instead of" /></span>
                                <p class="old">Detoxify Daily Scrub</p>
                                <span class="isNow">
                                    <img src="../images/productChanges/tryH.png" class="normal" alt="try" />
                                    <img src="../images/productChanges/tryOverH.png" class="over" alt="try" />
                                </span>
                                <p class="now">Pore Unclogging Scrub</p>
                            </div>
                            <img src="../images/productChanges/dailyScrub.jpg" class="prodImg" alt="" />
                            <a href="../deep-cleansing-products/pore-unclogging-scrub"></a>
                        </div>
                    </li>
                    <li>
                        <div class="itemWrapper">
                            <div class="content">
                                <span class="insteadOf"><img src="../images/productChanges/insteadOfH.png" alt="instead of" /></span>
                                <p class="old">Even Smoother<sup>&reg;</sup> Microderm Exfoliator</p>
                                <span class="isNow">
                                    <img src="../images/productChanges/tryH.png" class="normal" alt="try" />
                                    <img src="../images/productChanges/tryOverH.png" class="over" alt="try" />
                                </span>
                                <p class="now">Pore Unclogging Scrub</p>
                            </div>
                            <img src="../images/productChanges/evenSmoother.jpg" class="prodImg" alt="" />
                            <a href="../deep-cleansing-products/pore-unclogging-scrub"></a>
                        </div>
                    </li>
                </ul>
            </div>
            <div id="wholeClean">
                <h2><img src="../images/productChanges/goodThingsH.png" alt="Good Things Sometimes Come To An End" /></h2>
                <p class="introCopy">Our Discontinued Products:</p>
                <ul>
                    <li id="oldProd1">
                        <img src="../images/productChanges/nourishMoistureLotion.jpg" alt="" />
                        <p>Nourish Moisture Lotion SPF 15</p>
                    </li>
                    <li id="oldProd2">
                        <img src="../images/productChanges/hardDaysNight.jpg" alt="" />
                        <p>Hard Day's Night<sup>&reg;</sup> Overnight Moisturizer</p>
                    </li>
                    <li id="oldProd3">
                        <img src="../images/productChanges/seeTheFuture.jpg" alt="" />
                        <p>See the Future<sup>&reg;</sup> Fortifying Eye Cream</p>
                    </li>
                </ul>
            </div>
            <div class="teaser" id="whatsNew">
                <div id="content">
                    <img src="../images/productChanges/promoImg.png" alt="" id="promoImg" />
                    <p>Want to learn more about the great changes to the Bior&eacute;<sup>&reg;</sup> Skincare lineup? Read all about it here &raquo; </p>
                    <div class="clear"></div>
                </div>
                <img src="../images/productChanges/whatsNewPromoBg1.jpg" alt="" class="img1 bgImg" />
                <img src="../images/productChanges/whatsNewPromoBg2.jpg" alt="" class="img2 bgImg" />
                <a href="../clean-new-look/"></a>
            </div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
