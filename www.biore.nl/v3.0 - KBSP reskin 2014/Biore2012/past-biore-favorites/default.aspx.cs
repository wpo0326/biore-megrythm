﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Biore2012.past_biore_favorites {
    public partial class _default : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            Response.Redirect("~/biore-facial-cleansing-products"); // BWR-337
            // Grab masterpage, set appropriate body classes
            Site myMaster = (Site)this.Master;
            myMaster.bodyClass += " ourProducts productChanges";

        }
    }
}
