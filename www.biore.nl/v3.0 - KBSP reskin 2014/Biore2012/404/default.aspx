﻿<%@ Page Title="Pagina niet gevonden | Bior&eacute;&reg;" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012._404._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Bioré® Pagina niet gevonden" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <div id="content">
                    <h1>404 Pagina niet gevonden</h1>
                    <p>Het spijt ons. De door u opgevraagde pagina kan niet worden gevonden of er is een fout opgetreden. </p>
                    <div class="promoButton"><a href="http://www.biore.nl">Home</a></div>
                 </div>
                 <div id="photo">
                    <img src="<%= System.Configuration.ConfigurationManager.AppSettings["path"] %>images/utilityPages/polaroid.jpg" alt="" />
                 </div>
                 <div class="clear"></div>
            </div>
        </div>
    </div>
</asp:Content>