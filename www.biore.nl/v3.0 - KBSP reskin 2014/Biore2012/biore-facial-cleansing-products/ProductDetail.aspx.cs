﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Biore2012.BLL;
using System.Text;
using System.Threading;
using System.Web.UI.HtmlControls;
using System.Xml;
using Biore.BazaarVoice;

//New Floodlight tags from KBS replaced Enlighten Floodlight tags 6/10/13 -MCT
//Add alt tag rules 6/12/13 -MCT
//Add "Back Off Big Pores" category -11/19/14 MCT
//Add GTIN for Google Adsense - 3/12/15 MCT
//Re-arrange categories for new segment-emphasis - 12/01/17 MCT
//Add Micellar Water products - 9/18/18 -MCT

namespace Biore2012.our_products
{
    public partial class ProductDetail : RoutablePage
    {

        #region Floodlight private class
        public class FloodLight {                        
            public string Description { get; set; }
            public string IframeSource { get; set; }
        }
        #endregion

        #region Attributes
        public List<FloodLight> aFloodLight = new List<FloodLight>();
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {                        
            //BWR-78, BWR-79, BWR-80
            if (
                HttpContext.Current.Request.Path.Contains("complexion-clearing-products") ||
                HttpContext.Current.Request.Path.Contains("deep-cleansing-products") ||
                HttpContext.Current.Request.Path.Contains("make-up-removing-products") ||
                HttpContext.Current.Request.Path.Contains("deep-cleansing-product-family") ||
                HttpContext.Current.Request.Path.Contains("pore-detoxifying-foam-cleanser") ||  //discontinued per Digitas 11/20/14
                HttpContext.Current.Request.Path.Contains("make-up-removing-towelettes")
                )
            {
                Response.Redirect("~/biore-facial-cleansing-products", false);
                Response.StatusCode = 301;
                Response.End();
            }

            if (HttpContext.Current.Request.Path.Contains("deep-pore-charcoal-cleanser"))
            {
                spanNamePrefix.Attributes.Add("class", "spanNamePrefixDeepPoreCharcoalCleanser");
            }
            else if (HttpContext.Current.Request.Path.Contains("self-heating-one-minute-mask"))
            {
                spanNamePrefix.Attributes.Add("class", "spanNamePrefixSelfHeatingOneMinuteMask");
            }
            else
            {
                // class="spanNamePrefix" 
                spanNamePrefix.Attributes.Add("class", "spanNamePrefix");
            }

            StringBuilder sb = new StringBuilder();

            // Grab masterpage, set appropriate body classes
            Site myMaster = (Site)this.Master;
            myMaster.bodyClass += " ourProducts productDetail";
            
            // turn off floodlight pixels for the majority of the detail pages
            myMaster.FindControl("floodlightPixels").Visible = false;

            // configure prove it link for mobile
            //BioreUtils bu = new BioreUtils();
            //bu.configureProveItLinks(myMaster.isMobile, proveItPromoLink);

            // Fetch product based on route used to reach this page.
            Product theProduct = new Product();

            String pid = "";
            if (Routing.Value("pid") != null) 
            {
                
                pid = Routing.Value("pid").ToString();
                // Make sure we aren't showing a pore strip page.
                if (pid == "deep-cleansing-pore-strips-combo" ||
                    pid == "deep-cleansing-pore-strips")
                        Response.Redirect(VirtualPathUtility.ToAbsolute("~/deep-cleansing-product-family/pore-strips"));
                else
                {
                    try
                    {
                        theProduct = ProductDAO.GetProduct(pid);
                        if (Request.Url.ToString().Contains("breakup-with-blackheads/pore-strips"))
                            enableFamily();
                    }
                    catch (Exception ex)
                    {

                        if (ex is HttpException)
                        {
                            var httpEx = ex as HttpException;

                            switch (httpEx.GetHttpCode())
                            {
                                case 404:
                                    Response.Redirect("~/404/");
                                    break;

                                // others if any

                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            else
            {

                if (Request.Url.ToString().Contains("pore-strips/pore-strips"))
                {                                        

                    enableFamily();
                    theProduct = ProductDAO.GetProduct("pore-strips");
                }
                else
                    // If a user accidentally lands on the product detail page without a route.
                    theProduct = ProductDAO.GetProduct("combination-skin-balancing-cleanser");
            }
            
            string category;
            BVProductId.Value = BVUtils.GetProductId(Request, out category);
            // turn on floodlight pixels for acne clearing scrub, and set iframe source
            //if (theProduct.ProductRefName == "acne-clearing-scrub")
            //{                
                //addFloodLightOnLoad(theProduct.ProductRefName
                                          //,"http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=compl814;ord=");  
            //}
  

            // Add meta tagging.
            myMaster.Page.Title = theProduct.MetaTitle;

            HtmlGenericControl theMetaDescription = (HtmlGenericControl)Master.FindControl("head").FindControl("metaDescription");
            theMetaDescription.Attributes.Add("content", theProduct.MetaDescription);

            HtmlGenericControl theMetaKeywords = (HtmlGenericControl)Master.FindControl("head").FindControl("metaKeywords");
            theMetaKeywords.Attributes.Add("content", theProduct.MetaKeywords);

            HtmlMeta theMetaFBImage = (HtmlMeta)Master.FindControl("fbImage");
            theMetaFBImage.Attributes.Add("content", "http://" + Request.Url.Host + VirtualPathUtility.ToAbsolute(theProduct.SocialImage));


            //  ATAM this needs to be done in sidebar_ItemDataBound
            //String CategoryArrowSmallProduct = theProduct.CategoryArrowSmallProduct == null ? "" : theProduct.CategoryArrowSmallProduct.Trim();
            //if (CategoryArrowSmallProduct.Length > 0)
            //{
            //    //div
                
            //}
            //else
            //{
                
            //}

            // Populate category, write body classes to style according to category
            String productCategory = theProduct.Category.Trim();
            String productCategoryLeft = theProduct.CategoryLeft == null ? "" : theProduct.CategoryLeft.Trim();
            String productCategoryRight = theProduct.CategoryRight == null ? "" : theProduct.CategoryRight.Trim();

            if (productCategoryLeft.Length > 0 || productCategoryRight.Length > 0)
            {
                if ( productCategoryLeft.Length > 0)
                {

                    theCategoryLeft.Text = productCategoryLeft;
                   
                }
                else
                {
                    theCategoryLeft.Visible = false;
                }

                if (productCategoryRight.Length > 0)
                {
                    theCategoryRight.Text = productCategoryRight;
                }
                else
                {
                    theCategoryRight.Visible = false;
                }
            }
            else
            {
                theCategoryLeft.Text = productCategory;
                theCategoryRight.Visible = false;
            }
            

            // ATAM OLD
            //if (productCategory.ToLower() == "deep cleansing")
            //{
            //    myMaster.bodyClass += " deepCleansing";
            //    theCategoryClass.Text = "deepCleansing";
            //}
            //else if (productCategory.ToLower() == "complexion clearing")
            //{
            //    myMaster.bodyClass += " complexionClearing";
            //    theCategoryClass.Text = "complexionClearing";
            //}
            //else if (productCategory.ToLower() == "make-up removing")
            //{
            //    myMaster.bodyClass += " murt";
            //    theCategoryClass.Text = "murt";
            //}
            
            //ATAM NEWz
            if (productCategory.ToLower() == "charcoal for oily skin")
            {
                myMaster.bodyClass += " dontbedirty";
                theCategoryClass.Text = "dontbedirty";
            }
            else if (productCategory.ToLower() == "klaar met puistjes") //acne outta here
            {
                myMaster.bodyClass += " acneouttahere";
                theCategoryClass.Text = "acneouttahere";
            }
            else if (productCategory.ToLower().Contains("pore strips"))
            {
                myMaster.bodyClass += " breakupwithblackheads";
                theCategoryClass.Text = "breakupwithblackheads";
            }
            else if (productCategory.ToLower() == "baking soda for combination skin")
            {
                myMaster.bodyClass += " backoffbigpores";
                theCategoryClass.Text = "backoffbigpores";
            }
            else if (productCategory.ToLower() == "take it all off") //micellar water
            {
                myMaster.bodyClass += " takeitalloff";
                theCategoryClass.Text = "takeitalloff";
            }


            /*Left Side
-          <Green>Charcoal<Green> for Normal/Oily Skin
            o   Charcoal Cleanser
            o   Charcoal Pore Min
            o   Charcoal SHM
            o   Charcoal Pore Strip

            Right Side
            -          <Light Blue>Baking Soda<Light Blue> for Combination Skin
            o   BS Cleanser
            o   BS Powder Scrub
            -          <Pink>Pore Strips<Pink
            o   Charcoal Pore Strip
            o   14 Combo Pore Strip
             */


            // New and old image logic, including new product case

            //if (myMaster.isMobile)
            //{
            // theNewImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.NewLookImage).Replace("large", "mobile");
            //}
            //else
            //{
            theNewImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.NewLookImage);

                //string sPath = System.Configuration.ConfigurationManager.AppSettings["path"];
                //string sProductImage = VirtualPathUtility.ToAbsolute(theProduct.NewLookImage);

                //if (sProductImage.StartsWith("/") && !sProductImage.StartsWith("/en-us"))
                //{
                //    sProductImage = sProductImage.Remove(0, 1);
                //}

                //theNewImage.ImageUrl = sPath + sProductImage; //  VirtualPathUtility.ToAbsolute(theProduct.NewLookImage);
           // }

            //Display alt tag and make it title case for SEO requirements 6/12/13 -MCT
            Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(theNewImage.AlternateText = theProduct.ProductRefName.Replace("-", " "));

            if (theProduct.OldLookImage == "") oldNewControlPanel.Visible = false;
            else
            {
                //if (myMaster.isMobile)
                //{
                    //theOldImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.OldLookImage).Replace("large", "mobile");
                //}
                //else
                //{
                //    string sPath = System.Configuration.ConfigurationManager.AppSettings["path"];
                //    string sProductImage = VirtualPathUtility.ToAbsolute(theProduct.OldLookImage);

                //    if (sProductImage.StartsWith("/") && !sProductImage.StartsWith("/en-us"))
                //    {
                //        sProductImage = sProductImage.Remove(0, 1);
                //    }

                //    theOldImage.ImageUrl = sPath + sProductImage; //  VirtualPathUtility.ToAbsolute(theProduct.NewLookImage);
                //}
                //else
                //{
                    theOldImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.OldLookImage);
                //}
            }
            if (theProduct.BuyNowURL == "") { theBuyNowLink.Visible = false; }


            // Plug in data from the product
            theNamePrefix.Text = theProduct.NamePrefix == null ? "" : theProduct.NamePrefix;

            string ProductName = theProduct.Name == null ? "" : theProduct.Name.Trim();
            string ProductNameLine1 = theProduct.ProductNameLine1 == null ? "" : theProduct.ProductNameLine1.Trim();
            string ProductNameLine2 = theProduct.ProductNameLine2 == null ? "" : theProduct.ProductNameLine2.Trim();
            string ProductNameLine3 = theProduct.ProductNameLine3 == null ? "" : theProduct.ProductNameLine3.Trim();

            if (ProductNameLine1.Trim().Length == 0 && ProductNameLine2.Length == 0 && ProductNameLine3.Length == 0)
            {
                ProductNameL1.Text = ProductName.Replace("\n", "");
                ProductNameL1.Text = ProductNameL1.Text.Trim();
            }
            else
            {
                if (ProductNameLine1.Trim().Length > 0)
                {
                    ProductNameL1.Text = ProductNameLine1.Replace("\n", "");                    
                }
                else
                {
                    ProductNameL1.Visible = false;
                }

                if (ProductNameLine2.Trim().Length > 0)
                {
                    ProductNameL2.Text = ProductNameLine2.Replace("\n", "");
                }
                else
                {
                    ProductNameL2.Visible = false;
                }

                if (ProductNameLine3.Trim().Length > 0)
                {
                    ProductNameL3.Text = ProductNameLine3.Replace("\n", "");
                }
                else
                {
                    ProductNameL3.Visible = false;
                }

            }

            //Fetch Google Adsense UPC
            /*if (theProduct.ProductRefName != "deep-cleansing-pore-strips")
                GetCIData(theProduct.ProductRefName);
            else
                if (Request.RawUrl.IndexOf("/pore-strips#regular") > -1)
                    GetCIData("pore-strips#regular");
                else if (Request.RawUrl.IndexOf("/pore-strips#ultra") > -1)
                    GetCIData("pore-strips#ultra");
                else if (Request.RawUrl.IndexOf("/pore-strips#combo") > -1)
                    GetCIData("pore-strips#combo");
            */

            thePromoCopy.Text = theProduct.PromoCopy;
            theDescription.Text = theProduct.Description;
            //theBuyNowLink.NavigateUrl = theProduct.BuyNowURL;
            
            if (theProduct.BuyNowURL.Trim().Length <= 0)
            {
                divBuyNowBox.Visible = false;
            }
            else
            {
                if (theProduct.ProductRefName == "deep-cleansing-pore-strips")
                {
#if DEBUG
                    buyNowIFrame.Attributes["src"] = System.Configuration.ConfigurationManager.AppSettings["path"].ToString() + "/buynow/BuyNow.aspx?RefName=pore-strips#regular";
#else
    buyNowIFrame.Attributes["src"] = System.Configuration.ConfigurationManager.AppSettings["path"].ToString() + "buynow/BuyNow.aspx?RefName=pore-strips#regular";
#endif

                }
                else
                {
#if DEBUG
                    buyNowIFrame.Attributes["src"] = System.Configuration.ConfigurationManager.AppSettings["path"].ToString() + "/buynow/BuyNow.aspx?RefName=" + theProduct.ProductRefName;
#else
    buyNowIFrame.Attributes["src"] = System.Configuration.ConfigurationManager.AppSettings["path"].ToString() + "buynow/BuyNow.aspx?RefName=" + theProduct.ProductRefName;
#endif
                }

              
            }

            theWhatItIs.Text = theProduct.WhatItIs;
            theIngredients.Text = theProduct.ProductDetails;
            theHowItWorks.Text = theProduct.HowItWorks;
            theAdsenseContent.Text = theProduct.AdSense;
            theCautions.Text = theProduct.Cautions;
            theSidebarHeader.Text = theProduct.SideBarHeader;
            theSidebarCopy.Text = theProduct.SideBarCopy;

            // Is there a fun facts area?
            if (theProduct.FunFacts == "") theFunFactsPanel.Visible = false;
            else
            {
                theFunFacts.Text = theProduct.FunFacts;
            }


            // Is there a video?
            if (theProduct.Video == "") theVideoPanel.Visible = false;
            else
            {
                theVideoLink.NavigateUrl = VirtualPathUtility.ToAbsolute(theProduct.Video);
                string theVideoParams = string.Empty;
                if (theProduct.VideoDuration != "") theVideoParams = "videoParams::448|252|" + theProduct.VideoDuration;
                else theVideoParams = "videoParams::448|252|160";
                theVideoLink.Attributes.Add("rel", theVideoParams);
                theVideoStillImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.VideoStillImage);
            }


            // Associated Products
            List<SideBarProduct> SideBarProductsList = theProduct.SideBarProductCollection;
            theSideBarProducts.DataSource = SideBarProductsList;
            theSideBarProducts.DataBind();


            // Draw Like button if not on a mobile device
            if (myMaster.isMobile == false)
            {
                buildLikeBtn(theProduct.LikeURL);
                buildPlusBtn(theProduct.LikeURL);
                myMaster.FindControl("plusScript").Visible = true;

            }

            #region FloodLight Databind
            this.rptFloodLight.DataSource = aFloodLight;
            this.rptFloodLight.DataBind();
            #endregion            
        }

        protected void sidebar_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;
            ListView lv = (ListView)sender;

            List<SideBarProduct> myData = (List<SideBarProduct>)lv.DataSource;

            SideBarProduct theSideBarProduct = myData[dataItem.DataItemIndex];

            HyperLink theSideBarProductLink = (HyperLink)e.Item.FindControl("theSideBarProductLink");
            theSideBarProductLink.NavigateUrl = VirtualPathUtility.ToAbsolute(theSideBarProduct.ProductURL);

            Image theSideBarProductImage = (Image)e.Item.FindControl("theSideBarProductImage");
            theSideBarProductImage.ImageUrl = VirtualPathUtility.ToAbsolute(theSideBarProduct.Image);

            Literal theSideBarProductName = (Literal)e.Item.FindControl("theSideBarProductName");
            theSideBarProductName.Text = theSideBarProduct.ProductName;

            HtmlControl theSideBarListItem = (HtmlControl)e.Item.FindControl("theSideBarListItem");
            string className = theSideBarProduct.ClassName;
            if (className != "") { theSideBarListItem.Attributes.Add("class", className); }

            HtmlControl divarrowSmallProduct = (HtmlControl)e.Item.FindControl("divarrowSmallProduct");
            string arrowSmallProductClassName = "arrowSmallProduct";

            if (theSideBarProduct.CategoryArrowSmallProduct != null && theSideBarProduct.CategoryArrowSmallProduct.Trim().Length > 0)
            {
                arrowSmallProductClassName = theSideBarProduct.CategoryArrowSmallProduct.Trim();

            }
                
            divarrowSmallProduct.Attributes.Add("class", arrowSmallProductClassName);

        }

        protected void GetCIData(string ProductRefName)
        {
            try
            {
                XmlDocument navDoc = new XmlDocument();
                navDoc.Load(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["path"].ToString() + "buynow/buynow.xml"));
                
                string strNodeRoot = "Products/Product[ProductRefName = '" + ProductRefName.ToLower() + "']";

                string strProductName = navDoc.SelectSingleNode(strNodeRoot + "/ProductShortName").InnerText.ToString();
                string strProductSku = navDoc.SelectSingleNode(strNodeRoot + "/SKU").InnerText.ToString();
                string strProductUPC = navDoc.SelectSingleNode(strNodeRoot + "/UPC").InnerText.ToString();
                string strProductSmallImg = navDoc.SelectSingleNode(strNodeRoot + "/ImageCollection/Item/FileName").InnerText.ToString();
                string strProductSmallImgTxt = navDoc.SelectSingleNode(strNodeRoot + "/ImageCollection/Item/AltText").InnerText.ToString();

               
                
            }
            catch (Exception e)
            {
                Response.Write("error" + e + "<br />" + ProductRefName);
                //Invalid XML file.  Do nothing.
            }
        }

        #endregion

        #region Private methods
        private void addFloodLightOnLoad(string description, string src){
            // turn on floodlight pixels for pore strips, and set iframe source           
            this.Master.FindControl("floodlightPixels").Visible = true;

            var objFloodLight = new FloodLight();
            objFloodLight.IframeSource = src;
            objFloodLight.Description = description;
            aFloodLight.Add(objFloodLight);
        }

        private void buildLikeBtn(string likeURL)
        {
            // Build FB like code.
            StringBuilder likeBuilder = new StringBuilder();
            likeBuilder.Append("<div class=\"fb-like\" data-href=\"");
            likeBuilder.Append("http://" + Request.Url.Host + VirtualPathUtility.ToAbsolute(likeURL));
            likeBuilder.Append("\" data-send=\"false\" data-layout=\"button_count\" data-width=\"100\" data-show-faces=\"false\"></div>");
            fbLike.Text = likeBuilder.ToString();
        }

        private void buildPlusBtn(string likeURL)
        {
            // Build Google plus code.
            StringBuilder plusBuilder = new StringBuilder();
            plusBuilder.Append("<g:plusone size=\"medium\" annotation=\"none\" href=\"");
            plusBuilder.Append("http://" + Request.Url.Host + VirtualPathUtility.ToAbsolute(likeURL));
            plusBuilder.Append("\"></g:plusone>");
            plusButton.Text = plusBuilder.ToString();
        }

        private void enableFamily()
        {
            // We're on the porestrips family page.
            thePoreStripNavPanel.Visible = true;
            //_theProduct = ProductDAO.GetProduct("pore-strips");
            PoreStripProduct theRegularProduct = PoreStripProductDAO.GetProduct("pore-strips");
            //theRegularProduct.TrackingIframeSrc = "http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore437;ord=";
            PoreStripProduct theUltraProduct = PoreStripProductDAO.GetProduct("pore-strips-ultra");
            //theUltraProduct.TrackingIframeSrc = "http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore106;ord=";
            PoreStripProduct theComboProduct = PoreStripProductDAO.GetProduct("pore-strips-combo");
            //theComboProduct.TrackingIframeSrc = "http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore943;ord=";

            // Write the JSON for all three to the page.
            thePoreStripsJSON.Text = "var productRegular ="
                                    + JSONHelper.Serialize<PoreStripProduct>(theRegularProduct)
                                    + ";\n var productUltra ="
                                    + JSONHelper.Serialize<PoreStripProduct>(theUltraProduct)
                                    + ";\n var productCombo ="
                                    + JSONHelper.Serialize<PoreStripProduct>(theComboProduct)
                                    + ";";               
        }
        #endregion
    }
}


