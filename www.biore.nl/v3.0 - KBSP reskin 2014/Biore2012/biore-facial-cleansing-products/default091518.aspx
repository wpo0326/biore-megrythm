﻿<%@ Page Title="Diepe reiniging van je poriën – Krijg een schone en gezonde huid – Bekijk alle producten | Bioré® Huidverzorging" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.our_products.Default" %>
<%@ Register Src="~/BazaarVoice/Controls/BVScripts.ascx" TagPrefix="uc1" TagName="BVScripts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Bior&eacute huidverzorgende producten reinigen en exfoliëren zacht waardoor je een gezonde en schone huid krijgt! Zie het complete Bioré assortiment." name="description" />
    <meta content="Schone huid, Bioré producten, Poriën, Houtskool, baking soda, Porie-Strips, Diepe reiniging, Gecombineerde huid, Vette huid , Baking Soda, Zuiveringszout" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>
    <meta name="viewport" content="width=device-width">
    <uc1:BVScripts runat="server" ID="BVScripts" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="titleh1">
                <h1>Onze Producten</h1>
                <div id="responseRule"></div>
                <p>Richten zich op de kern van alle huidproblemen – door wekelijks te strippen en dagelijks te reinigen. Onze krachtige, porie-reinigende producten zijn verkrijgbaar als cleansers, scrubs en strips om zo je huid schoon en gezond te houden. </p>
            </div>

            

            <div id="deepCleansingProds" class="prodList">
                <h2 class="pie roundedCorners deepCleansing">Bye Bye <span>vuile huid</span>!<br />
                    <span class="arrow"></span>
                </h2>
                <ul>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CHARCOAL_100X235.png" alt="" />
                        <h3>Diepe Porie Cleanser<br />met Houtskool</h3>
                        <p>Reinigt 2x dieper & zuivert op natuurlijke wijze  </p>
                        <div id="BVRRInlineRating-deep-pore-charcoal-cleanser" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/deep-pore-charcoal-cleanser" id="A1">details ></a>
                    </li>
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreMinimizer.png" alt="" />
                        <h3>Porieverkleinende Scrub<br />met Houtskool </h3>
                        <p>Scrubt zacht & vermindert poriën zichtbaar</p>
                        <div id="BVRRInlineRating-charcoal-pore-minimizer" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/charcoal-pore-minimizer" id="details-charcoal-pore-minimizer">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/SelfHeatingMask.jpg" alt="" style="margin:0 0 2px 44px;"/>
                        <h3>Zelfverwarmend<br />1 minute masker </h3>
                        <p>Zuivert poriën 2,5 keer beter</p>
                        <div id="BVRRInlineRating-self-heating-one-minute-mask" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/self-heating-one-minute-mask" id="A2">details ></a>
                    </li>
                     <li class="productListSpacer">
                        <img id="dontBeDirtyProducts" src="../images/ourProducts/products/small/Baking_Soda_Cleanser_small.png" alt="" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:8pt;"></span> Porie Cleanser met baking soda </h3>
                        <p>Dringt in poriën & scrubt zacht </p>
                        <div id="BVRRInlineRating-baking-soda-pore-cleanser" class="ratingCategory"></div>
                        <a href="../baking-soda-for-combination-skin/baking-soda-pore-cleanser" id="A3">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-cleansing-scrub-small.png" alt="" />
                        <h3><span style="font-family: ProximaNova-Bold, Arial;color:yellow;font-size:8pt;"></span> Cleansing poeder scrub met baking soda</h3>
                        <p>Zachte scrub en diepe reiniging </p>
                        <div id="BVRRInlineRating-baking-soda-cleansing-scrub" class="ratingCategory"></div>
                        <a href="../baking-soda-for-combination-skin/baking-soda-cleansing-scrub" id="A4">details ></a>
                    </li>            
                </ul>
            </div>

<div id="complexionClearingProds" class="prodList">
                <h2 class="pie roundedCorners bigPores">Klaar met <span>puistjes</span>!<br />
                    <span class="arrow"></span>
                </h2>
                <ul>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-anti-pimple_100X235.png" alt="" />
                        <h3>Anti-Puistjes Cleanser met Houtskool</h3>
                        <p> </p>
                        <div class="ratingCategory"></div>
                        <a href="../klaar-met-puistjes/charcoal-anti-pimple-clearing-cleanser" id="A7">details ></a>
                    </li>     
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-anti-pimple_100X235.png" alt="" />
                        <h3>Anti-Puistjes Scrub met Houtskool</h3>
                        <p> </p>
                        <div class="ratingCategory"></div>
                        <a href="../klaar-met-puistjes/charcoal-anti-pimple-scrub" id="A8">details ></a>
                    </li>     
                    
                </ul>

                                       
            </div>

            <div id="murtProds" class="prodList">
                <h2 class="pie roundedCorners murt">Bye bye <span>mee-eters</span>!
                    <span class="arrow"></span>
                </h2>
                <ul>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltra.png" alt="" />
                        <h3>Ultra Diep Reinigende Porie-Strips</h3>
                        <p></p>
                        <div id="Div1" class="ratingCategory"></div>
                        <a href="../pore-strips/deep-cleansing-pore-strips-ultra" id="A5">details ></a>
                    </li>
		   <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" />
                        <h3>Diep Reinigende <br />Porie-Strips met Houtskool</h3>
                        <p>Verwijdert overtollig vet met natuurlijke houtskool</p>
                        <div id="BVRRInlineRating-charcoal-pore-strips-regular" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/charcoal-pore-strips#regular" id="details-deep-cleansing-pore-strips-charcoal">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.png" alt="" />
                        <h3>Diep Porie <br />Reinigende Combi-Strips</h3>
                        <p>Mee-eter bestrijdend duo voor gezicht & neus</p>
                        <div id="BVRRInlineRating-pore-strips-combo" class="ratingCategory"></div>
                        <a href="../pore-strips/pore-strips-combo" id="details-deep-cleansing-pore-strips-combo">details ></a>
                    </li>  
                 
                </ul>
            </div>

            
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
