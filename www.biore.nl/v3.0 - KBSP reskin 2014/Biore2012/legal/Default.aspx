﻿<%@ Page Title="Disclaimer | Bior&eacute;&reg; Huidverzorging"  Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Biore2012.legal.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Ontdek de Bioré® Huidverzorging website" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
    <style type="text/css">
        #content p
        {
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div id="main">
            <div id="mainContent">
                <div class="centeringDiv">
                    <div id="content">
                        <div class="titleh1">
                            <h1>
                                Bior&eacute;<sup>&reg;</sup> Disclaimer</h1>
                            <div id="responseRule">
                            </div>
                        </div>
                        <p>© 2017 Guhl Ikebana Cosmetics BV  ("Kao") Alle rechten voorbehouden.</p>
                
                <p>DOOR GEBRUIK TE MAKEN VAN DEZE WEBSITE BEGRIJP JE EN GA JE AKKOORD MET HET ONDERSTAANDE:
                <br /><br />
                De informatie op deze Website (de "Website") betreffende producten of diensten is alleen van toepassing in Nederland en is alleen bestemd voor persoonlijk en niet-commercieel gebruik. Alle titels, namen en grafische voorstellingen die in verband met de Website zijn gebruikt, zijn handelsmerken van Kao of zijn licentiehouders, tenzij anders aangegeven. Op alle materialen die de Website bevat, rusten auteursrechten van Kao of zijn licentiehouders. Het reproduceren, opnieuw publiceren of verspreiden van materiaal dat afkomstig is van deze Website dat verder gaat dan hetgeen nodig is om je in staat te stellen de Website te bekijken, is ten strengste verboden, met als uitzondering dat de gebruiker alleen voor persoonlijk en niet-commercieel gebruik een enkele hardcopy mag downloaden en afdrukken. Informatie over Kao producten en diensten is zonder voorafgaande kennisgeving onderhevig aan verandering. Sommige producten en diensten zijn niet verkrijgbaar in bepaalde gebieden of landen. De informatie op deze Website is alleen bedoeld voor algemene informerende doeleinden. De meningen en opinies die op de Website worden geuit, zijn niet per definitie de meningen en opinies van Kao. De vermelding van producten en/of bedrijven dient niet te worden geïnterpreteerd als een goedkeuring van, of als uiting van een connectie met deze producten en/of bedrijven.
                <br /><br />
                Deze Website is alleen voor informatieve doeleinden en is niet bedoeld ter vervanging van professionele medische adviezen, diagnoses of behandelingen. Het vertrouwen op informatie die op de Website te vinden is, is volledig op jouw eigen risico. Indien je vragen of zorgen hebt over medische zaken of gezondheidskwesties, dien je contact op te nemen met een arts.
                <br /><br />
                Je mag geen spiders, robots, dataminingtechnieken of andere geautomatiseerde apparaten of programma's gebruiken om de inhoud van de Website te catalogiseren, te downloaden, op te slaan of op andere wijze te reproduceren, op te slaan of te verspreiden. Je mag geen actie ondernemen om de Website of het gebruik van de Website door andere gebruikers te hinderen of te verstoren, inclusief, zonder beperking, door middel van overbelasting, "flooding", "mailbombing" of het laten "crashen" van de Website, het omzeilen van de beveiliging of maatregelen voor het identificeren van gebruikers of het proberen om de beperkte autorisatie of toegang die je onder deze voorwaarden hebt, te overschrijden. Het is verboden delen van de Website in een andere website op te nemen of koppelingen vanaf een andere website naar een pagina van de Website te maken, met uitzondering van de homepage. Je mag het gebruik van of de toegang tot de Website niet verkopen aan derden zonder onze voorafgaande schriftelijke toestemming.</p>
                
                <p>REDELIJKERWIJS IS ER ALLES AAN GEDAAN OM ERVOOR TE ZORGEN DAT DE INHOUD VAN DE WEBSITE JUIST EN UP-TO-DATE IS. KAO KAN ECHTER GEEN GARANTIES GEVEN OF BELOFTEN MAKEN MET BETREKKING TOT DE JUISTHEID OF VOLLEDIGHEID VAN DE INHOUD VAN DE WEBSITE OF ANDERE GARANTIES OF BELOFTES OVER DE INHOUD VAN DE WEBSITE, DIE WORDEN GELEVERD OP "AS-IS" BASIS. KAO GARANDEERT NIET DAT DE FUNCTIES DIE ZIJN OPGENOMEN IN DE WEBSITE OF IN EEN VAN DE MATERIALEN OF INHOUD DAARVAN ONVERSTOORD OF FOUTLOOS ZULLEN ZIJN, DAT DEFECTEN VERHOLPEN WORDEN,
OF DAT DE WEBSITE OF DE SERVER DIE DEZE BESCHIKAAR MAAKT VRIJ ZIJN VAN VIRUSSEN OF ANDERE SCHADELIJKE COMPONENTEN. KAO IS NIET AANSPRAKELIJK VOOR HET GEBRUIK VAN DE WEBSITE, INCLUSIEF, ZONDER BEPERKING, DE INHOUD EN EVENTUELE FOUTEN DIE DAARIN STAAN. HIERBIJ WORDEN ALLE GARANTIES, UITDRUKKELIJK OF STILZWIJGEND, AFGEWEZEN (INCLUSIEF, DOCH NIET BEPERKT TOT DIE VAN VERKOOPBAARHEID, NIET-INBREUK EN GESCHIKTHEID VOOR EEN BEPAALD DOELEINDE).
</p>
                
                <p>Deze Website kan links of referenties naar andere websites bevatten, maar Kao is niet verantwoordelijk voor de inhoud van dergelijke andere websites en is niet aansprakelijk voor schade of verwondingen die ontstaan als gevolg van de inhoud van dergelijke websites. Links naar andere websites worden slechts verstrekt voor het gemak van gebruikers van deze Website.
                <br /><br />
                Kao neemt de privacy van jou en jouw persoonsgegevens heel serieus. Bekijk ons <a href="/privacy/">privacybeleid</a> voor meer informatie over hoe we gegevens die we van je hebben verkregen, gebruiken.  
                <br /><br />
                Noch Kao, zijn agenten, dochterondernemingen, vertegenwoordigers, noch een andere partij die betrokken is bij het creëren, produceren of onderhouden van de Website (gezamenlijk de "Kao Partijen" genoemd) zijn aansprakelijk voor indirecte, incidentele of speciale schade, gevolgschade of boetes die voortvloeien uit jouw gebruik van de Website of de diensten of producten die via de Website worden aangeboden, zelfs als deze partij is gewaarschuwd over de mogelijkheid van dergelijke schade.
                <br /><br />
                In geen geval zal de gezamenlijke, totale aansprakelijkheid van de Kao Partijen tegenover jou voor alle schade, verwondingen, verliezen en gedingen (hetzij m.b.t. een contract, door een onrechtmatige daad of op andere wijze) voortvloeiend uit of met betrekking tot jouw gebruik van de Website het volgende totaal overschrijden: (a) het bedrag, indien van toepassing, dat door jou aan Kao is betaald met betrekking tot het gebruik van deze Website of (b) €100.
                <br /><br />
                Deze Website wordt geleverd als een service voor de bezoekers van de Website. Kao behoudt zich het recht voor om de inhoud van de Website te allen tijde te verwijderen, aan te passen of aan te vullen, inclusief veranderingen in deze voorwaarden, om welke reden dan ook, en zonder enige kennisgeving aan iemand. Wij zullen dergelijke veranderingen aanbrengen door ze op de Website te plaatsen. Je kunt de Website regelmatig bezoeken om dergelijke veranderingen op te merken. Indien je de Website blijft bezoeken nadat dergelijke veranderingen zijn aangebracht, duidt dit op jouw acceptatie van deze veranderingen.</p>
                
                <p>Deze overeenkomst is opgesteld en wordt geïnterpreteerd in overeenstemming met de wetten van de Amerikaanse staat Delaware, zonder toepassing van de beginselen van conflictenrecht. Je gaat ermee akkoord dat geschillen die voortvloeien uit of die op enige manier betrekking hebben op deze overeenkomst, de Website of een van de producten of diensten van Kao exclusief zullen worden voorgelegd aan de federale gerechtshoven of staatsgerechtshoven van de staat Delaware en je gaat er onherroepelijk mee akkoord dat je je zult onderwerpen aan de exclusieve rechtsbevoegdheid van deze gerechtshoven. Indien een bepaling uit deze overeenkomst onwettig, nietig of om wat voor reden dan ook onuitvoerbaar wordt geacht, dan zal die bepaling als scheidbaar van deze Overeenkomst worden beschouwd, en zullen de geldigheid en uitvoerbaarheid van overige bepalingen hierdoor niet worden beïnvloed.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

