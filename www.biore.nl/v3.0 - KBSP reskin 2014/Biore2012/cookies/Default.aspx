﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Biore2012.cookies.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .cookieTbl 
             {
    border-collapse: collapse;
}

.cookieTbl th, .cookieTbl td {
    border: 1px solid black;
    padding: .3em;
}
        
    </style>
    <div id="main">
        <div id="mainContent" style="min-height: 730px">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <br />
                <br />
                
                <h1 style="text-align:center">Cookies</h1>
                   <div id="responseRule"></div>
                <br />
                <h2>WAT ZIJN COOKIES</h2>
<p>Een cookie is een klein tekstbestand die websites op uw computer of mobiele toestel opslaat wanneer u die site bezoekt. Zo onthoudt de website de pagina's die je hebt bezocht en je voorkeuren voor langere tijd (zoals gebruikersnaam, taal, lettergrootte en andere voorkeuren) zodat je deze niet keer op keer opnieuw hoeft in te stellen bij ieder bezoek of bij het bladeren door de website pagina's.</p>
                <br /><br />
<h2>HOE GEBRUIKEN WIJ DE COOKIES EN VOOR WELK DOELEINDE</h2>
                <br />
                <table class="cookieTbl">
                    <tr>
                        <td><b>Cookie</b></td><td><b>Functie</b></td>
                    </tr>
                    <tr><td>ASP.NET_SessionId</td><td>Sessie Cookie is noodzakelijk voor de website om goed te functioneren. </td></tr>
                    <tr><td>_ga</td><td>Google Analytics</td></tr>
                    <tr><td>_gid</td><td>Google Analytics</td></tr>
                    <tr><td>_gat</td><td>Google Analytics</td></tr>
                    <tr><td>_gat_KAOGlobal</td><td>GA code specifiek voor het KAO Global Google Analytics account.</td></tr>
                    <tr><td>cookieaccept</td><td>Cookie die wordt gebruikt om aan te geven of een klant de cookie alert/disclaimer heeft begrepen en geaccepteerd bij hun eerste bezoek aan de website. </td></tr>


                </table>
                <br /><br />

<h2>BEHEREN VAN JOUW COOKIES</h2>
<p>Je kunt cookies altijd controleren en/of wissen. Je kunt alle cookies op uw computer wissen en de browser zo instellen dat cookies geblokkeerd worden. Dit betekent wel dat je daarna je voorkeuren bij ieder bezoek opnieuw moet instellen en dat sommige delen van de website niet beschikbaar zijn.</p>
<p>Ik ga akkoord met het gebruik van cookies.</p>

                <br /><br />
            </div>
        </div>
    </div>
</asp:Content>
