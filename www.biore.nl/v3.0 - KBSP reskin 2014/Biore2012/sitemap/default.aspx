﻿<%@ Page Title="Sitemap | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.sitemap._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1 style="text-align:center;">Sitemap</h1>
                 <div id="responseRule"></div>
                 <div id="linkList">
                     <div id="prodLinks">
                         <ul class="col" id="col1">
                            <li><a href="../biore-facial-cleansing-products">ONZE PRODUCTEN<span class="arrow">&rsaquo;</span></a></li>
                            <li>
                                <h2 class="pie deepCleansing"><span class="subNavHeadlines">bye bye <span class="greenBold">vuile huid</span>!</span></h2>
                                <ul>
                                    

                                    <li class="greenHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/charcoal-for-oily-skin/deep-pore-charcoal-cleanser") %>" id="A3" class="track" data-tracking="TopNav|Click|Diepe Porie Cleanser met Houtskool">Diepe Porie Cleanser met Houtskool</a></li>
                                            <li class="greenHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/charcoal-for-oily-skin/charcoal-pore-minimizer") %>">Porieverkleinende scrub met Houtskool</a></li>
                                            <li class="greenHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/charcoal-for-oily-skin/self-heating-one-minute-mask") %>">Zelfverwarmend 1 Minute Masker </a></li>
                                             <li class="greenHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/baking-soda-for-combination-skin/baking-soda-pore-cleanser") %>">Porie Cleanser met Zuiverings-soda </a></li>
                                            <li class="greenHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/baking-soda-for-combination-skin/baking-soda-cleansing-scrub") %>">Cleansing Poeder Scrub met Zuiverings-soda</a></li>
                                </ul>

                                          
                                <h2 class="pie takeitAllOff"><span class="subNavHeadlines">Hé <span class="purpleBold">SCHOON</span>heid!</span></h2>
                                <ul>
                                    <li class="purpleHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/take-it-all-off/baking-soda-cleansing-micellar-water") %>" id="topnav-baking-soda-cleansing-micellar-water">Micellair Reinigingswater met Baking Soda</a></li>
                                    <li class="purpleHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/take-it-all-off/charcoal-cleansing-micellar-water") %>" id="topnav-charcoal-cleansing-micellar-water">Micellair Reinigingswater met Houtskool</a></li> 
                                </ul>

                            </li>
                        </ul>
                        <ul class="col" id="col2">
                            <li>
                                <h2 class="pie complexionClearing"><span class="subNavHeadlines">Klaar met <span class="orangeBold">puistjes</span>!</span></h2>
                                <ul>
                                     <li class="orangeHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/klaar-met-puistjes/charcoal-anti-pimple-clearing-cleanser") %>">Anti-Puistjes Cleanser met Houtskool</a></li>
                                            <li class="orangeHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/klaar-met-puistjes/charcoal-anti-pimple-scrub") %>">Anti-Puistjes Scrub met Houtskool </a></li>
                                </ul>
                                <h2 class="pie murt"><span class="subNavHeadlines">Bye Bye <span class="redBold">Mee-eters</span>!</span></h2>
                                <ul>
                                     <li class="redHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/pore-strips/charcoal-pore-strips#regular") %>">Diep Reinigende Poriestrips met Houtskool</a></li>

				                            <li class="redHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/pore-strips/pore-strips-combo") %>">Diep Porie Reinigende Combi-Strips</a></li>

                                            <li class="redHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/pore-strips/deep-cleansing-pore-strips-ultra") %>">Ultra Diep Reinigende Porie-Strips</a></li>
                                </ul>
                                <ul>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <ul class="col noPadding" id="col3">
                    

                        <li><a href="../where-to-buy-biore">Verkrijgbaarheid <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../about-us">Over Bioré <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaocareers.com/">WERKEN BIJ <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../contact-us">Contact <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../legal">Disclaimer <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../privacy">Privacybeleid <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../cookies">Cookies <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../Colofon">Colofon <span class="arrow">&rsaquo;</span></a></li>


                     </ul>
                 </div>
                 <div id="photo"></div>
            </div>
        </div>
    </div>
</asp:Content>