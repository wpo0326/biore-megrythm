<%

session("languageid") = "EN"
session("countryid") = "US"


' SiteID drives what events are valid for this website
dim SiteID, EventCode_signup,EventCode_login, EventCode_loginpw, EventCode_confirm, EventCode_taf, EventCode_interest
dim Site_BrandNameHTML, Site_BrandNameTEXT, site_CountryID, EventCode_contact


site_CountryID = "USA"

SiteID = 1400
EventCode_signup = "biore_us_optin"
EventCode_login = "biorelogin"
EventCode_loginpw = "bioreloginpw"
Eventcode_confirm = "bioreconfirm"
EventCode_taf = "bioretaf"
EventCode_interest = "bioreinterest"
EventCode_contact = "biorecontact"
Site_BrandNameHTML = "Bior&eacute;&reg; Skincare"
Site_BrandNameTEXT = "Bior� Skincare"


' The email to CRS has the site address in the subject line
const crs_Website = "biore.com"

' The email address to which "Contact Us" submissions should be sent
const crs_EmailAddress = "jer@cybercrs.net"
'const crs_EmailAddress = "brian.roeper@kaobrands.com"

' refers to the file on the site root, to which all requests are sent
' this file is simply a shell, in that it contains the standard site header & footer
' with an INCLUDE in the middle that refers to master.asp
const start_file = "event.asp"

' the URL for the root of this site.  Used primarily when generating TAF emails
const URL_root = "http://www.biore.com/usa/"

'URL's are relative to site root
'privacy policy link
const URL_PP = "/usa/PrivacyPolicy.asp"
'homepage
const URL_HP = "/usa/index.asp"
'products page
const URL_products = "/usa/Products/Index.asp"
'unsubscribe page
const URL_unsubscribe = "/usa/signup/unsubscribe.asp"



%>