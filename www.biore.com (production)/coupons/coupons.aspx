<%@ Page Language="C#" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Security.Cryptography" %>
<script runat=server>


public class CouponFunctions
{
  static string url = "http://bricks.coupons.com/enable.asp?";
  public string finalUrl = "";
  static int myOfferCode = 110764;
  static string myCheckCode = "KA";
  static string myPinCode = "";
  static string myShortKey = "zjc5hnemb0";
  static string myLongKey = "nhjZKlDEgaiorp3GbM6Sz7eAkUqwy9NHJTuVQ1dmfvcPB2sL84XxRF5OYIWCt";
  static string cpt = "";
  static string qStr = "";

  public string getFinalUrl(int myOfferCode)
  {
	
	myPinCode = CouponFunctions.getMicroTime().ToString();
	using (MD5 md5Hash = MD5.Create())
	{
	  myPinCode = CouponFunctions.GetMd5Hash(md5Hash, myPinCode);
	}
	
	myOfferCode = CouponFunctions.myOfferCode;
	myShortKey = CouponFunctions.myShortKey;
	myLongKey = CouponFunctions.myLongKey;
	
	CouponFunctions.cpt = EncodeCPT(myPinCode, myOfferCode, myShortKey, myLongKey);
	
	CouponFunctions.qStr = "o=" + myOfferCode + "&c=" + myCheckCode + "&p=" + myPinCode + "&cpt=" + cpt;
	this.finalUrl = CouponFunctions.url + CouponFunctions.qStr;

	return this.finalUrl;
  }

/// Returns the encrypted pin code, offercode, short CipherKey and long CipherKey.
/// <param name="pinCode">User's unique identifier, as assigned by the client.</param>
/// <param name="offerCode">Offer code for the coupon, as assigned by Coupons, Inc..</param>
/// <param name="shortKey">Short CipherKey for the coupon, as assigned by Coupons, Inc..</param>
/// <param name="longKey">Long CipherKey for the coupon, as assigned by Coupons, Inc..</param>
/// <returns>An encrypted string, also known as the CPT parameter.</returns>
  private static string EncodeCPT(string pinCode, int offerCode, string shortKey, string longKey)
  {
    string decodeX = " abcdefghijklmnopqrstuvwxyz0123456789!$%()*+,-.@;<=>?[]^_{|}~";
    int[] encodeModulo;
    int[] vob;
    int ocode;
    encodeModulo = new int[256];
    vob = new int[2];
    vob[0] = offerCode % 100;
    vob[1] = ((offerCode-vob[0])/100) % 100;
    for (int i = 0; i < 61; i++)
      encodeModulo[(int)char.Parse(decodeX.Substring(i, 1))] = i;
    pinCode = pinCode.ToLower() + offerCode.ToString();
    if (pinCode.Length < 20)
    {
      pinCode = pinCode + " couponsincproduction";
      pinCode = pinCode.Substring(0, 20);
    }
    int q = 0;
    int j = pinCode.Length;
    int k = shortKey.Length;
    int s1, s2, s3;
    System.Text.StringBuilder cpt = new System.Text.StringBuilder();
    for (int i = 0; i < j; i++)
    {
      s1 = encodeModulo[(int)char.Parse(pinCode.Substring(i, 1))];
      s2 = 2 * encodeModulo[(int)char.Parse(shortKey.Substring(i % k, 1))];
      s3 = vob[i % 2];
      q = (q + s1 + s2 + s3) % 61;
      cpt.Append(longKey.Substring(q, 1));
    }
    return cpt.ToString();
  }
  
  private static int getMicroTime()
  {
    TimeSpan difference = (DateTime.UtcNow - new DateTime(1970, 1, 1));
    return (int)(difference.Ticks / 10);
  }

  private static string GetMd5Hash(MD5 md5Hash, string input)
  {

      // Convert the input string to a byte array and compute the hash. 
      byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

      // Create a new Stringbuilder to collect the bytes 
      // and create a string.
      StringBuilder sBuilder = new StringBuilder();

      // Loop through each byte of the hashed data  
      // and format each one as a hexadecimal string. 
      for (int i = 0; i < data.Length; i++)
      {
          sBuilder.Append(data[i].ToString("x2"));
      }

      // Return the hexadecimal string. 
      return sBuilder.ToString();
  }  
  
} // end class
</script>


<!DOCTYPE html>
<html>
<body>
<% 
	// KAO - Curel Intensive Healing - $2 - Banner - 005260 (2013-01) - OFFER CODE 109407
	// KAO - Cur�l UH or ID - $1 - Banner - 005261 (2013-01) 		  - OFFER CODE 109408
	//"http://bricks.coupons.com/enable.asp?o=123456&c=CI&p=ClientID&cpt=8kCg7Vf9JZeSWNy9nluI"
	// Short key and long key are same for both offers
	// Check Code: KA is also the same for both offers
	CouponFunctions cf = new CouponFunctions();
	string offerStr = Request.QueryString["offer"];
	int offer = 0;
	offer =  Convert.ToInt32(offerStr);
	//Response.Write(cf.getFinalUrl(offer));
	Response.Redirect(cf.getFinalUrl(offer));
%>
</body>
</html>