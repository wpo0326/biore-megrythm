﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.pore_care.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Pore Care 101 – Clear Blemishes & Clogged Pores | Bioré® Skincare" name="title" />
    <meta content="Clear Blemishes & Clogged Pores with a Pore Care Regimen from Bioré® Skincare." name="description" />
    <meta content="How to clean clogged pores, how to clean pores, clear blemishes, how to clear blemishes, blemish clearing" name="keywords" />

    <script type="text/javascript">
        var axel = Math.random() + "";
        var a = axel * 10000000000000;
        document.write('<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore0;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore0;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>


    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/poreCare.css")
        .Render("~/css/combinedprorecare_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="floodlightPixels" runat="server">
   <!-- header scripts here -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div class="centeringDiv">
                <div id="content">
                    <div class="titleh1">
                        <h1>PORE CARE <span>101</span></h1>
                        <div class="ruleBGa"></div>
                        <div class="responseRule"></div>

                    </div>
                    <div class="rowCont">
                        <div class="row1">                             
		                    <div class="whyCare"><h4>WHY CARE?</h4><h2>Maintaining clean, healthy pores</h2><p>is important for sustaining healthy skin since the average adult has approximately 20,000 facial pores.</p></div>
                            <div class="col1 colMobile"><img src="../images/poreCare/poreCare2.jpg" alt="" id="Img4" /></div>    
		                </div>
                       <div class="row2">
                            <div class="col1"><h4>THE QUESTION:</h4><h2>Can clogged pores lead to acne and blemishes?</h2><p><b>Really? Yes! </b>Dirt and oil collect around your pores, creating build-up that can clog the skin follicle.  If bacteria makes its way to the blockage and grows, this can cause inflamation and result in acne and blemishes.  Swelling and redness may linger in the form of whiteheads, blackheads, and red inflamed patches of skin.</p></div>
		                    <div class="col2 colDesktop"><img src="../images/poreCare/poreCare2.jpg" alt="" id="Img2" /></div>
                           <div class="col3 colMobile"><img src="../images/poreCare/poreCare3.png" alt="" id="Img5" /></div>
		                </div>
                        <div class="row3">
                            <div class="col1 colDesktop"><img src="../images/poreCare/poreCare3.png" alt="" id="Img3" /></div>
		                    <div class="col2"><h4>THE SOLUTION:</h4><h2>Cleanse, Scrub, Remove.</h2><p>Our Acne-Fighting Regimen works wonders. <br />Just choose a cleanser, a scrub, and a pore strip for clear skin. <br /><br />If you have acne, Salicylic Acid is a great acne fighting ingredient. It helps limit acne bacteria growth, while our cleansing agents help to remove sebum. For milder acne, it helps unclog pores to heal and prevent lesions. Amazing. Try our Charcoal products for Oily skin or our Baking Soda products for Combination skin.</p></div>                             
		                </div>
                        <div style="clear:both;"></div>
                        <div id="Tips10">
                            <p class="largeNumber">10</p>
                            <p class="tipsFor green">TIPS FOR</p>
                            <p class="clearSkin">CLEAR SKIN</p>
                            <div class="ruleBG"></div>
                        </div>
		                <div class="bullets">	
                            <h3 class="green">• Keep a pore care regimen.</h3>
	                        <h3>• Clean skin gently with a mild cleanser.</h3>
	                        <h3 class="green">• Remove all dirt and makeup. </h3>
	                        <h3>• Wash twice a day, especially after exercising.</h3>
	                        <h3 class="green">• Avoid over-scrubbing or repeated skin washing.</h3>
	                        <h3>• Don’t squeeze, scratch, pick, or rub pimples. It can lead to infections and scarring. </h3>
	                        <h3 class="green">• Avoid touching your face with hands. </h3>
	                        <h3>• Avoid oily cosmetics and creams. </h3>
	                        <h3 class="green">• Use water-based or noncomedogenic formulas.</h3>
	                        <h3>• Stay out of the sun. UV rays can make acne worse. Seriously.</h3>
		                </div>
	                </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
    <%--<h1>pore care - scripts</h1>--%>
     
     <!--Begin Mediavest DMP-->
				<script type="text/javascript" src="/en-US/js/kao-VivaKiDIL_6.4.js"></script>
				<!--End Mediavest DMP-->
</asp:Content>
