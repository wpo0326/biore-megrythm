﻿<%@ Page Title="Bioré | Pore Strips & Pore-obsessed Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Get healthy, radiant skin with products from Bioré® Skincare" name="description" />
    <meta content="skincare, clean face" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/UniversPro67.css")
        .Add("~/css/homepage.css")
        .Add("~/css/flexslider.css")
        .Render("~/CSS/combinedhome_#.css")
    %>
    <meta name="viewport" content="width=device-width">


    <script type="text/javascript">
        $(document).ready(function () {
            //$('.scroll-dots a').click(function (event) {
            //    event.preventDefault();
            //    var target = $(this).attr('id');
            //    console.log(target);
            //});
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $('#theaterItem1 a').click(
                function (e) {
                    e.preventDefault();

                    var axel = Math.random() + "";
                    var a = axel * 10000000000000;
                    var strScript = "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore551;ord='" + a + "'?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"
                               + "<noscript>"
                               + "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore551;ord=1?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"
                               + "</noscript>";

                    $('body').prepend(strScript);
                    var target = $(this).attr('target');
                    var uri = $(this).attr('href');

                    setTimeout(function () {
                        if (target) {
                            window.open(uri, '_blank');
                        } else {
                            window.location = uri;
                        }
                    }, 1000);

                });
        })
    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">


    <script type="text/javascript">
        var axel = Math.random() + "";
        var a = axel * 10000000000000;
        document.write('<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore916;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
    <noscript>
<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore916;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>

    <!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main1">
        <div class="home-content">
            <section id="section-2017-interim">
                <div class="top-baking-soda-2017">
                    
                    <!--<img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/top-right-bubbles.png") %>" alt="top right bubbles" /></div>-->
                     

                <div class="welcome-module clearfix">
                    

                    <div id="container1n">
                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/Biore-The-Proof-You-Can-See.jpg") %>" alt="The Proof You Can See. Sometimes Shocking, Always Satisfying." class="home-img-banner-2019" />
                        <div class="home-banner-1">
                            <div class="inner-div">
                                <h1>THE <span class="lg">PROOF</span> YOU CAN <span class="lg">SEE.</span></h1>
                                <h2>(SOMETIMES SHOCKING, ALWAYS SATISFYING)</h2>
                            </div>
                        </div>
                    </div>

               
                    <!--
                     <div class="footer-2017">
                        <div class="acne-footer-2017">
                            *Garnier Skin Active Micellar Water All-In-1 Waterproof, the leading Micellar Water according to Neilsen Scanning Data, 52 Weeks Ending April 2018.
                        </div>
                    </div>
                    -->

                    <div class="hide-mobile"></div>
                    <div class="scrollDownHldr">
                        <p class="scrollDown">SCROLL DOWN</p>
                        <div id="arrowBounce">
                            <a href="#section-2018-products">
                                <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/arrowDown.png") %>" class="section-down-arrow" /></a>
                        </div>

                    </div>
                </div>

                <div class="scroll-dots top-banner">
                    <a href="#section-2017-interim"><span class="scroll-dot enable"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                    <a href="#section-01"><span class="scroll-dot"></span></a>
                    <a href="#section-04"><span class="scroll-dot"></span></a>
                    <a href="#section-05"><span class="scroll-dot"></span></a>
                    <a href="#section-06"><span class="scroll-dot"></span></a>
                    <a href="#section-07"><span class="scroll-dot"></span></a>
                    <a href="#section-08"><span class="scroll-dot"></span></a>
                </div>
                <!--<div class="bottom-charcoal-2017">
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/bottom-left-bubbles.png") %>" alt="bottom left bubbles" />
                </div>-->
             </div>
            </section>


            <section class="home-section" id="section-2018-products">


                <div id="container2n">
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/Biore-Witch-Hazel-Formulation.jpg") %>" alt="Clogged Pores? Blackheads? Acne? Witch Please! Formulated with Witch Hazel, this blemish-busting combo deep cleans and controls oil for clear pores." class="home-img-banner-2019" />
                    <div class="home-banner-2">
                        <div class="inner-div">
                            <h1><span>Clogged Pores?</span><span>Blackheads?</span><span>Acne?</span></h1>
                            <h2>Witch Please!</h2>
                            <h3>Formulated with Witch Hazel,<br /> this blemish-busting combo deep cleans<br />& controls oil for clear pores.</h3>
                        </div>

                    </div>
                </div>

                
                <div class="footer-2019" style="color:#1f2c61;bottom:5px;">
                    <div>*Nielsen data 52 weeks ending March 2019.</div>
                </div>


                <div class="scroll-dots top-banner">
                    <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot enable"></span></a>
                    <a href="#section-01"><span class="scroll-dot"></span></a>
                    <a href="#section-04"><span class="scroll-dot"></span></a>
                    <a href="#section-05"><span class="scroll-dot"></span></a>
                    <a href="#section-06"><span class="scroll-dot"></span></a>
                    <a href="#section-07"><span class="scroll-dot"></span></a>
                    <a href="#section-08"><span class="scroll-dot"></span></a>
                </div>

            </section>




            <section class="home-section iex" id="section-01">
                <div class="welcome-module">
                    <!--
                    <video autoplay="autoplay" loop="loop" muted="muted" preload="auto" id="welcomeVideo">
                    <source src="<%= VirtualPathUtility.ToAbsolute("~/video/homepage/shayzilla.mp4") %>" type="video/mp4"/>
                    <source src="<%= VirtualPathUtility.ToAbsolute("~/video/homepage/shayzilla.ogv") %>" type="video/ogg"/>
                    <source src="<%= VirtualPathUtility.ToAbsolute("~/video/homepage/shayzilla.webm") %>" type="video/webm"/>
                    </video>
                  
                    <div id="welcomeImage" class="hide-mobile show" style="height: 100%">
                        <img class="ïmg-less" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/shay.png") %>" />
                    </div>
                    
                    <div id="welcomeImage" class="hide-desktop">
                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/shayzilla.png") %>" />
                    </div>
                    -->

                    <div class="welcome-module-content">

                       <div class="phone">
                           <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/biore-lexi-animation.gif") %>" alt="Sometimes Shocking, Always Satisfying.">
                       </div>

                       <div class="phone">
                           <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/biore-pink-sink-animation.gif") %>" alt="The Unbeatable Pore Strip. Proof You Can See.">
                           
                       </div>

                       <div class="phone">
                           <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/biore-madelaine-animation.gif") %>" alt="Free Your Pores.">
                       </div>

                        <!--
                        <h1> FIND YOUR WAY TO </br>COME CLEAN</h1>
                        <p>Dirty little secrets—we&rsquo;ve all got &rsquo;em. Dirt, oil and blackheads hiding in our pores. Let Bior&eacute;<sup>&reg;</sup> beauty and actress Shay Mitchell show you how to clean them away in&nbsp;these&nbsp;exclusive&nbsp;videos.</p>
                        
                        <div class="welcome-video hide-desktop">
                            <a href="https://www.youtube.com/embed/TVBZqfDfAgE?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a>
                        </div>

                        <div class="animation hide-mobile overwritte">
                            <div class="animation-container">
                                <div><a href="https://www.youtube.com/embed/TVBZqfDfAgE?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                <a href="https://www.youtube.com/embed/TVBZqfDfAgE?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm  fancybox.iframe">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/Biore_Intro.gif") %>">
                                    <img class="gif-holder overwritte-gif" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/Biore_Intro.gif") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/Biore_Intro.gif") %>">
                                </a>
                                <div class="animation-play-btn overwritte-btn">
                                    <a href="https://www.youtube.com/embed/TVBZqfDfAgE?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm  fancybox.iframe">
                                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>" />
                                    </a>
                                </div>
                            </div>
                        </div>
                        -->

                    </div>
                    <div class="hide-mobile"></div>
                    <div class="bubbles-bg hide-mobile" style="width: 34%">
                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/bubbles-web.png") %>" style="float: right" />
                        <!--<img class="products-responsive" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/product.png") %>" />-->
                    </div>
                </div>
                <div class="scroll-dots">
                    <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                    <a href="#section-01"><span class="scroll-dot enable"></span></a>
                    <a href="#section-04"><span class="scroll-dot"></span></a>
                    <a href="#section-05"><span class="scroll-dot"></span></a>
                    <a href="#section-06"><span class="scroll-dot"></span></a>
                    <a href="#section-07"><span class="scroll-dot"></span></a>
                    <a href="#section-08"><span class="scroll-dot"></span></a>
                </div>
                <div class="white-shadow hide-mobile"></div>
            </section>
            
            <section class="home-section"" id="section-04">
            <div class="bg-left"></div><div class="bg-right"></div>
            <div class="container3">
	            <div class="container2">
		            <div class="container1">
			            <div class="headline">
				            <h4>BE <br>MAG-<br>NETIC</h4>
			            </div>
			            <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/embed/e9Hw1XdJwio?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                <a href="https://www.youtube.com/embed/e9Hw1XdJwio?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/03-charcoal-strips.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/03-charcoal-strips.gif") %>">                                
                                </a>
                                <div class="animation-skin-container">
                                    <div class="animation-skin"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/03_charcoal_pore_strips.png") %>"/></div>
                                </div>
                                    <div class="animation-play-btn">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>
                                </div>
                            </div>
			            </div>
			            <div class="product">
				            <p>Bior&eacute;<sup>&reg;</sup> Deep Cleansing Charcoal Pore Strips</p>
			            </div>
		            </div>
	            </div>
            </div>

                <div class="scroll-dots">
                    <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                    <a href="#section-01"><span class="scroll-dot"></span></a>
                    <a href="#section-04"><span class="scroll-dot enable"></span></a>
                    <a href="#section-05"><span class="scroll-dot"></span></a>
                    <a href="#section-06"><span class="scroll-dot"></span></a>
                    <a href="#section-07"><span class="scroll-dot"></span></a>
                    <a href="#section-08"><span class="scroll-dot"></span></a>
                </div>
        
        </section>
        
        <section class="home-section float-right" id="section-05">
                <div class="bg-left"></div>
                <div class="bg-right"></div>
                <div class="container3">
                    <div class="container2">
                        <div class="container1">
                            <div class="headline">
                                <h4>DRAW OUT THE DIRT</h4>
                            </div>
                            <div class="animation">
                                <div class="animation-container">
                                    <div class="animation-border"><a href="https://www.youtube.com/embed/xhStsDgvRMM?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                    <a href="https://www.youtube.com/embed/xhStsDgvRMM?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                        <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/04-charcoal-pore-cleanser.gif") %>">
                                        <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/04-charcoal-pore-cleanser.gif") %>">
                                    </a>
                                    <div class="animation-skin-container">
                                        <div class="animation-skin">
                                            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/04_deep_pore_charcoal_cleanser.png") %>" /></div>
                                    </div>
                                    <div class="animation-play-btn">
                                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>" />
                                    </div>
                                </div>
                            </div>
                            <div class="product">
                                <p>Bior&eacute;<sup>&reg;</sup> Deep Pore Charcoal Cleanser</p>
                            </div>
                        </div>
                    </div>
                </div>

               <div class="scroll-dots">
                    <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                    <a href="#section-01"><span class="scroll-dot"></span></a>
                    <a href="#section-04"><span class="scroll-dot"></span></a>
                    <a href="#section-05"><span class="scroll-dot enable"></span></a>
                    <a href="#section-06"><span class="scroll-dot"></span></a>
                    <a href="#section-07"><span class="scroll-dot"></span></a>
                    <a href="#section-08"><span class="scroll-dot"></span></a>
                </div>
            </section>
            
 <section class="home-section"" id="section-06">
            <div class="bg-left"></div><div class="bg-right"></div>
            <div class="container3">
	            <div class="container2">
		            <div class="container1">
			            <div class="headline">
				            <h4>INTENSE <span style="white-space: nowrap;">PORE-TINGLY</span> CLEAN</h4>
			            </div>
			            <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/embed/w5fCydu7Clk?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                <a href="https://www.youtube.com/embed/w5fCydu7Clk?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/Biore-Agave-fa.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/Biore-Agave-fa.gif") %>">                                
                                </a>
                                <div class="animation-skin-container">
                                    <div class="animation-skin"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/blue_agave_whipped_mask.png") %>"/></div>
                                </div>
                                    <div class="animation-play-btn">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>
                                </div>
                            </div>
			            </div>
			            <div class="product">
				            <p>Bior&eacute;<sup>&reg;</sup> Blue Agave + Baking Soda Whipped Nourishing Detox Mask </p>
			            </div>
		            </div>
	            </div>
            </div>

                <div class="scroll-dots">
                    <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                    <a href="#section-01"><span class="scroll-dot"></span></a>
                    <a href="#section-04"><span class="scroll-dot"></span></a>
                    <a href="#section-05"><span class="scroll-dot"></span></a>
                    <a href="#section-06"><span class="scroll-dot enable"></span></a>
                    <a href="#section-07"><span class="scroll-dot"></span></a>
                    <a href="#section-08"><span class="scroll-dot"></span></a>
                </div>
        </section>
        
<section class="home-section float-right" id="section-07">
                <div class="bg-left"></div>
                <div class="bg-right"></div>
                <div class="container3">
                    <div class="container2">
                        <div class="container1">
                            <div class="headline">
                                <h4>THE UNBEATABLE PORE STRIP*. INSTANT BLACKHEAD REMOVAL.</h4>
                            </div>
                            <div class="animation">
                                <div class="animation-container">
                                    <div class="animation-border"><a href="https://www.youtube.com/embed/tizzo8_0ZXI?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                    <a href="https://www.youtube.com/embed/tizzo8_0ZXI?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                        <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/Biore-Deep-fa.gif") %>">
                                        <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/Biore-Deep-fa.gif") %>">
                                    </a>
                                    <div class="animation-skin-container">
                                        <div class="animation-skin">
                                            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/03_charcoal_pore_strips.png") %>" /></div>
                                    </div>
                                    <div class="animation-play-btn">
                                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>" />
                                    </div>
                                </div>
                            </div>
                            <div class="product">
                                <p>Bior&eacute;<sup>&reg;</sup> Charcoal Deep Cleansing Pore Strips</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="footer-2019">
                    <div>*#1 Pore Strip Brand in Nielsen data, 52 weeks ending February 2019.</div>
                </div>



               <div class="scroll-dots">
                    <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                    <a href="#section-01"><span class="scroll-dot"></span></a>
                    <a href="#section-04"><span class="scroll-dot"></span></a>
                    <a href="#section-05"><span class="scroll-dot "></span></a>
                    <a href="#section-06"><span class="scroll-dot"></span></a>
                    <a href="#section-07"><span class="scroll-dot enable"></span></a>
                    <a href="#section-08"><span class="scroll-dot"></span></a>
                </div>
            </section>
          
<section class="home-section" id="section-08">
            <div class="bg-left"></div><div class="bg-right"></div>
            <div class="container3">
	            <div class="container2">
		            <div class="container1">
			            <div class="headline">
				            <h4>CLEARER<br>SKIN IN JUST<br>2 DAYS</h4>
			            </div>
			            <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/embed/pjYQhFjDXfE?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                <a href="https://www.youtube.com/embed/pjYQhFjDXfE?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/Biore-Acne-fa.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/Biore-Acne-fa.gif") %>">                                
                                </a>
                                <div class="animation-skin-container">
                                    <div class="animation-skin"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/charcoal_acne_clearing_cleanser.png") %>"/></div>
                                </div>
                                    <div class="animation-play-btn">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>
                                </div>
                            </div>
			            </div>
			            <div class="product">
				            <p>Bior&eacute;<sup>&reg;</sup> Charcoal Acne Clearing Cleanser </p>
			            </div>
		            </div>
	            </div>
            </div>

                <div class="scroll-dots">
                    <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                    <a href="#section-01"><span class="scroll-dot"></span></a>
                    <a href="#section-04"><span class="scroll-dot"></span></a>
                    <a href="#section-05"><span class="scroll-dot"></span></a>
                    <a href="#section-06"><span class="scroll-dot"></span></a>
                    <a href="#section-07"><span class="scroll-dot"></span></a>
                    <a href="#section-08"><span class="scroll-dot enable"></span></a>
                </div>
        </section>


        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/jquery.flexslider.min.js")
        .Render("~/js/combinedhome_#.js")
    %>

    <script type="text/javascript" src="<%= VirtualPathUtility.ToAbsolute("~/js/homepage.js") %>"></script>

    <!--Begin Mediavest DMP-->
    <script type="text/javascript" src="/en-US/js/kao-VivaKiDIL_6.4.js"></script>
    <!--End Mediavest DMP-->
</asp:Content>
