﻿<%@ Page Title="About Bior&eacute;&reg; Skincare | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.about_us._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Learn more about Bioré® Skincare" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div class="centeringDiv">
                <div id="content">
                    <div class="titleh1">
                        <h1>About Bior&eacute;<sup>&reg;</sup> Skincare</h1>
                        <div id="responseRule"></div>
                    </div>
                    <div class="about-info">                        
                        <p>Did you know that the Bior&eacute;<sup>&reg;</sup> brand’s parent company is Kao Corporation (pronounced like ‘cow’) and is headquartered in Tokyo, Japan?<br /><br />
 
The story behind Kao begins with a small face-care company in Tokyo founded by Mr. Tomiro Nagase in 1887. Tomiro was on a mission to make affordable, high-quality, facial soap—which he called “Kao” – pronounced the same way as the Japanese word for “face” (makes sense, right!?).<br /><br />
 
Today, Bior&eacute;<sup>&reg;</sup> is a pore focused--or more like a pore obsessed-- face care brand in the US that continues the mission to make high quality, affordable  skincare products that provide a variety of skincare benefits.  Even though we create our own line of products in the US, we approach all of our product development with a Japanese Beauty Philosophy and are able to leverage our parent company’s amazing Japanese technology. These technologies are  backed by years of  research and development (hello skincare jackpot!). <br /><br />
 
Our Japanese beauty roots inspire us to improve and innovate in everything we do—innovation is in our DNA.  Bottom line, our products perform. With technology rooted in science, we take a no-nonsense approach to fight smarter, not harder--while having a little bit of fun along the way! Our products focus on those 20,000 pesky pores, providing deep cleansing products for the skin of your dreams.</p>
                    </div>
                    <img src="../images/about/aboutBG.png" alt="" id="Img1" />                    
                </div>
            </div>
        </div>
    </div>
</asp:Content>

