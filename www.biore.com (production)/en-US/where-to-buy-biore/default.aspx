﻿<%@ Page Title="Where To Buy | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.where_to_buy._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Quickly find where to buy your favorite Bioré® products" name="description" />
    <meta content="where to buy Bioré products, find Bioré retailers" name="keywords" />

    <!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: BIORE_WHERETOBUY_PL
URL of the webpage where the tag is expected to be placed: http://www.biore.com/en-US/where-to-buy-biore/
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 04/09/2015
-->
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore00;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore00;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->

    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/whereToBuy.css")
        .Render("~/css/combinedbuy_#.css")
    %>
    <link rel="stylesheet" type="text/css" href="/en-US/css/wheretobuy.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script src="/en-US/js/popup.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div class="centeringDiv">
                <h1 class="archer-medium">Where To Buy</h1>
                <div class="responseRule"></div>
                <div id="logoHolder">
                    <asp:ListView runat="server" ID="lvRetailer" DataSourceID="XmlDataSource1" OnItemDataBound="lvRetailer_ItemDataBound">
						<LayoutTemplate>
							<ul id="retailLogos">
								<div runat="server" id="itemPlaceholder"></div>
							</ul>

                            

						</LayoutTemplate>

                        

						<ItemTemplate>
							<li class="bubbleInfo">
								<asp:HyperLink runat="server" ID="hlRetailer" class="trigger" />
                                <div id="dpop" class="popup">
        		                	<div id="topleft" class="corner"></div>
        		                    <div id="top" class="top-bottom"></div>
        		                    <div id="topright" class="corner"></div>
                                    <div style="clear:both;"></div>
        		                    <div id="left" class="sides"></div>
        		                    <div id="right" class="sides"></div>
        		                    <div class="popup-contents">
                                    	<div><asp:HyperLink runat="server" ID="locatorLink" Target="_blank" Visible="false" style="text-indent:0px; margin:0px; padding:0px; height:25px; font-weight:normal">Store Locator</asp:HyperLink>
                                        </div>
                                        <div><asp:HyperLink runat="server" ID="onlineLink" Target="_blank" Visible="false" style="text-indent:0px; margin:0px; padding:0px; height:25px; font-weight:normal">Online Store</asp:HyperLink>
                                        </div>
                                    </div>
                                    <div style="clear:both;"></div>
        		                    <div class="corner" id="bottomleft"></div>
        		                    <div id="bottom" class="top-bottom"><img id="ImageBottom" runat="server" width="30" height="29" alt="popup tail" src="/en-US/images/wheretobuy/popup/bubble-tail2.png"/></div>
        		                    <div id="bottomright" class="corner"></div>
                                </div>
							</li>


						</ItemTemplate>
					</asp:ListView>

                    

				  <asp:XmlDataSource ID="XmlDataSource1" runat="server" 
                        DataFile="/en-US/where-to-buy-biore/wheretobuy.xml" XPath="WTBLinks/Links"></asp:XmlDataSource>

                    

                    <%--<div id="storeLogos" class="logos">
                        <p>Shop for Bior&eacute;<sup>&reg;</sup> products at a retailer near you:</p>
                        <ul>
                            <li class="walmart"><a href="http://www.walmart.com/cservice/ca_storefinder.gsp" target="_blank" id="retailerNearYou-Walmart">Walmart</a></li>
                            <li class="target"><a href="http://sites.target.com/site/en/spot/page.jsp?title=store_locator_new&ref=nav_storelocator" id="retailerNearYou-Target">Target</a></li>
                            <li class="harmon"><a href="http://store.facevaluesonline.com/store-locations.html" target="_blank" id="retailerNearYou-Harmon">Harmon</a></li>
                            <li class="riteaid"><a href="http://www.riteaid.com/stores/locator/" target="_blank" id="retailerNearYou-RiteAid">RiteAid</a></li>
                            <li class="float">
                                <div class="walgreens"><a href="http://www.walgreens.com/marketing/storelocator/find.jsp?foot=store_locator" target="_blank" id="retailerNearYou-Walgreens">Walgreens</a></div>
                                <div class="cvs"><a href="http://www.cvs.com/CVSApp/store/storefinder.jsp" target="_blank" id="retailerNearYou-CVS">CVS/pharmacy</a></div>
                            </li>
                            <li class="kmart"><a href="http://www.kmart.com/shc/s/StoreLocatorView?storeId=10151" target="_blank" id="retailerNearYou-Kmart">Kmart</a></li>
                        </ul>                
                    </div>
                    <div id="onlineLogos" class="logos">
                        <p>Shop for Bior&eacute;<sup>&reg;</sup> products online:</p>
                        <ul>
                            <li class="walmart"><a href="http://www.walmart.com/search/search-ng.do?search_constraint=0&ic=48_0&search_query=biore&Find.x=20&Find.y=4" target="_blank" id="productsOnline-Walmart">Walmart</a></li>
                            <li class="target"><a href="http://www.target.com/s?searchTerm=biore" id="productsOnline-Target">Target</a></li>
                            <li class="harmon"><a href="http://store.facevaluesonline.com/searchresult.html?catalog=yhst-29523360387793&query=biore&x=0&y=0" target="_blank" id="productsOnline-Harmon">Harmon</a></li>
                            <li class="ulta"><a href="http://search.ulta.com/nav/brand/Biore/0" target="_blank" id="productsOnline-Ulta">Ulta</a></li>
                            <li class="walgreens"><a href="http://www.walgreens.com/search/results.jsp?Ntt=biore&x=0&y=0" target="_blank" id="productsOnline-Walgreens">Walgreens</a></li>
                            <li class="drugstore"><a href="http://www.drugstore.com/templates/brand/default.asp?brand=8696&trx=SBB-0-AB&trxp1=8696" target="_blank" id="productsOnline-Drugstore">drugstore.com</a></li>
                            <li class="duanereade"><a href="http://www.duanereade.com/Default.aspx" target="_blank" id="productsOnline-Duanereade">Duanereade</a></li>
                            <li class="riteaid"><a href="http://www.riteaidonlinestore.com/s?searchKeywords=biore" target="_blank" id="productsOnline-Riteaid">RiteAid</a></li>
                            <li class="cvs"><a href="http://www.cvs.com/CVSApp/search/search.jsp?searchTerm=biore" target="_blank" id="productsOnline-CVS">CVS/pharmacy</a></li>
                        </ul>                
                    </div>--%>

                    

                </div>
                
                <div id="wtbProductsAll"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/whereToBuy/wtpProductsAll.png") %>" border="0" alt="" /></div>


            </div>
        </div>
    </div>
     
     <!--Begin Mediavest DMP-->
				<script type="text/javascript" src="/en-US/js/kao-VivaKiDIL_6.4.js"></script>
				<!--End Mediavest DMP-->
</asp:Content>

