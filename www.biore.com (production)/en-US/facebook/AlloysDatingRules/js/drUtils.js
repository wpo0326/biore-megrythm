﻿/****************************************************************
Utils JavaScript Document for Biore Alloy Dating Rules Facebook Tab

Global Utils JS document contains JavaScript functions 
that are used throughout the site.
      
*****************************************************************/

$(function() {
    //$.support.cors = true; // force cross-site scripting (as of jQuery 1.5)
	adr.init();
});

var adr = {
	currVid: "wn1",
	init: function() {
		this.fixLog();
		this.setupCarousels();
		this.setupEvents();
		//this.getFBComments();
		this.getTweets();
	},
	fixLog: function() {
		window.log = function() {
			log.history = log.history || []; // store logs to an array for reference
			log.history.push(arguments);
			if (this.console) {
				console.log(Array.prototype.slice.call(arguments));
			}
		};
	},
	setupEvents: function() {
		/* tab button switching */
		$(".tabBtn").bind("click", adr.handleBtnClick);

		/* Fix jCarousel next/prev link click */
		$(".nextBtn, .prevBtn").bind("click", function(e) { e.preventDefault(); });

		/* Video Link switching */
		$(".vidLink").bind("click", adr.handleVidLnkClick);

		/* Social Tab switching */
		$(".socialTabs").bind("click", adr.handleSocialTabsClick);

		/* Open FB Comments Popup */
		$("#fbShare").bind("click", adr.fbPopup);

		/* Listen for FB Comments to be made, setup recurring check */
		this.listenFBComments();
	},
	setupCarousels: function() {
		var $next = $('<a href="#">Next</a>'),
			$prev = $('<a href="#">Prev</a>');

		$('.videoLnkHolder').each(function(index) {
			var $this = $(this),
				addBtns = false,
				$vidCarousel = $(this).find(".vidLnkCarousel");

			if ($this.find("ul").children("li").length > 5) {
				$vidCarousel.after($next.clone().addClass("nextBtn next" + index), $prev.clone().addClass("prevBtn prev" + index));
				addBtns = true;
			}
			$vidCarousel.jCarouselLite({
				btnNext: addBtns ? ".next" + index : "",
				btnPrev: addBtns ? ".prev" + index : "",
				visible: 5,
				circular: false,
				afterEnd: adr.jcMoved
			});

			$vidCarousel.find("a").bind("click", function(e) {
				e.preventDefault();
				var itemIndex = $(this).closest("li").index();
				$("#infoArrow").removeClass().addClass("index" + itemIndex);
			});
		});
	},
	jcMoved: function(visibleEls) {
		var totalEls = $(visibleEls[0]).closest("ul").children().length;
		if ($(visibleEls[0]).index() > 0) { $(".prevBtn").show(); } else { $(".prevBtn").hide(); }
		if ($(visibleEls[visibleEls.length - 1]).index() === totalEls - 1) { $(".nextBtn").hide(); } else { $(".nextBtn").show(); }
	},
	handleBtnClick: function(e) {
		var $this = $(this),
			thisID = this.id,
			vidHolderID = this.id.replace("Lnk", "Vids");
		e.preventDefault();
		$this.siblings().removeClass("selected").end().addClass("selected");
		$("#" + vidHolderID).siblings().css("left", "-9999px").end().css("left", "auto");

		/* Move info arrow */
		$("#infoArrow").removeClass().addClass("index0");
	},
	handleVidLnkClick: function(e) {
		var $this = $(this),
			thisID = this.id,
			ecHolderID = this.id.substring(0, 2) + "Content" + this.id.replace(this.id.substring(0, 2), ""),
			iframeTemplate = '<iframe id="btsInterview" width="448" height="259" src="{{=URL}}" frameborder="0" allowfullscreen></iframe>';
		e.preventDefault();
		if ($this.hasClass("comingSoonLink")) { return; }
		$this.addClass("selected").closest("li").siblings().each(function() {
			$(this).find("a").removeClass("selected");
		});

		$("#" + ecHolderID).siblings().hide().end().show();

		// Replace video if the URL exists
		if (this.href.indexOf("#") === -1) {
			$("#vidHolder").html(iframeTemplate.replace("{{=URL}}", this.href));
		}
	},
	handleSocialTabsClick: function(e) {
		var $this = $(this);
		e.preventDefault();
		$this.siblings().removeClass("selected").end().addClass("selected");

		if (this.id === "fbTab") { $("#fbFeed").show(); $("#twFeed").hide(); }
		if (this.id === "twTab") { $("#fbFeed").hide(); $("#twFeed").show(); }
	},
	fbComments: [],
	socialItems: 4,
	getFBComments: function() {
		var urlToShow = fbCommentsUrl;

		//var url = "https://graph.facebook.com/comments/?ids=" + encodeURI(urlToShow) + "&limit=" + numToShow + "&offset=0&callback=?",
		var urlFront = "https://graph.facebook.com/",
			url = "fql?q=" + encodeURIComponent("select fromid, username, time, text from comment where object_id in (select comments_fbid from link_stat where url ='" + fbCommentsUrl + "') order by time desc limit " + adr.socialItems),
			url2 = "fql?q=" + encodeURIComponent("select username, uid from user where uid=");

		$('#fbComments').html('<p id="loading">Loading Facebook Comments...</p>');

		if (typeof FB !== "undefined") {
			FB.api({
				method: 'fql.multiquery',
				queries: {
					'query1': "SELECT fromid, username, time, text FROM comment WHERE object_id IN (SELECT comments_fbid FROM link_stat WHERE url ='" + fbCommentsUrl + "') ORDER BY time DESC LIMIT " + adr.socialItems,
					'query2': "SELECT name, username, uid FROM user WHERE uid IN (SELECT fromid FROM #query1)"
				}
			}, function(response) {
				//log(response);
				if (typeof response.error_code === "undefined" && response.length > 1 && response[0].fql_result_set.length) {
					var q1 = response[0], q2 = response[1];
					for (var i in q1.fql_result_set) {
						if (q1.fql_result_set[i].fromid) {
							adr.fbComments[i] = { "fromID": q1.fql_result_set[i].fromid, "text": q1.fql_result_set[i].text, "time": q1.fql_result_set[i].time };
							for (var j in q2.fql_result_set) {
								if (q2.fql_result_set[j].uid === q1.fql_result_set[i].fromid) {
									adr.fbComments[i]["username"] = q2.fql_result_set[j].name;
								}
							}
						}
					}
					adr.insertFBComments();
				} else {
					$('#fbComments').html('<p id="loading">There are no comments yet.</p>');
				}
			});
		} else {
			$('#fbComments').html('<p id="loading">There was an error retrieving the comments from Facebook.</p>');
		}
	},
	insertFBComments: function() {
		//log(adr.fbComments.length);
		if (adr.fbComments.length) {
			$('#fbComments').html('');
			for (var i in adr.fbComments) {
				$('#fbComments').append('<div id="wrap' + i + '"></div>');
				$('#fbComments div#wrap' + i).append('<div class="commentBlock"><a href="http://www.facebook.com/profile.php?id=' + adr.fbComments[i].fromID + '" class="UIImageBlock_Image" target="_blank"><img src="https://graph.facebook.com/' + adr.fbComments[i].fromID + '/picture"/></a></div>');
				$('#fbComments div#wrap' + i + ' div.commentBlock').append('<div class="UIImageBlock_Content"><a href="http://www.facebook.com/profile.php?id=' + adr.fbComments[i].fromID + '" class="profileName" target="_blank">' + adr.fbComments[i].username + '</a></div><div class="clear"></div>');
				$('#fbComments div#wrap' + i + ' div.commentBlock div.UIImageBlock_Content').append('<p class="postText">' + adr.fbComments[i].text + '</p>');
				$('#fbComments div#wrap' + i + ' div.commentBlock div.UIImageBlock_Content').append('<p class="createdTime">' + adr.prettifyDate(adr.fbComments[i].time) + '</p>');
			}
		} else {
			$('#fbComments').html('<p id="loading">There was an error retrieving the comments from Facebook.</p>');
		}
	},
	prettifyDate: function(dtString) {
		// takes Unix timestamp and returns "DAY at TIME(am/pm)"
		var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
			prettyDT = '',
			dt = new Date(dtString * 1000);

		if (adr.isValidDate(dt)) {
			prettyDT = days[dt.getDay()] + ' at ' + (dt.getHours() > 12 ? dt.getHours() - 12 : dt.getHours()) + ':' + dt.getMinutes() + (dt.getHours() > 12 ? "pm" : "am");
		}
		return prettyDT;
	},
	isValidDate: function(d) {
		if (Object.prototype.toString.call(d) !== "[object Date]")
			return false;
		return !isNaN(d.getTime());
	},
	fbPopup: function(e) {
		e.preventDefault();
		adr.fbCommentPopup = window.open("Comments.aspx", "fbComments", "resizable,toolbar=no,location=no,scrollbars,width=520,height=550", true);
	},
	listenFBComments: function() {
		if (typeof FB !== "undefined") {
			FB.Event.subscribe('comment.create', adr.fbCommentAdded);
		}
	},
	fbCommentCreated: function(response) {
		setTimeout(adr.getFBComments, 500);
	},
	getTweets: function() {
		var url = "http://search.twitter.com/search.json?q=from:DatingRulesShow%20OR%20%23datingrules&rpp=" + adr.socialItems + "&include_entities=true";
		var tweets = $.getJSON(url + "&callback=?", function(results) {
			var resultsData = results.results;

			$('#tweets').html('');
			adr.insertTweets(resultsData);
		});
	},
	insertTweets: function(data) {
		var tweetUrl = "https://twitter.com/#!/{{username}}/status/{{tweetid}}";
		if (data.length) {
			for (var i in data) {
				$('#tweets').append('<div id="wrap' + i + '"></div>');
				$('#tweets div#wrap' + i).append('<div class="commentBlock"><a href="https://twitter.com/' + data[i].from_user + "/status/" + data[i].id_str + '" class="UIImageBlock_Image" target="_blank"><img src="' + data[i].profile_image_url + '"/></a></div>');
				$('#tweets div#wrap' + i + ' div.commentBlock').append('<div class="UIImageBlock_Content"><a href="https://twitter.com/' + data[i].from_user + "/status/" + data[i].id_str + '" class="profileName" target="_blank">' + data[i].from_user_name + '</a></div><div class="clear"></div>');
				$('#tweets div#wrap' + i + ' div.commentBlock div.UIImageBlock_Content').append('<p class="postText">' + adr.parseTweet(data[i]) + '</p>');
				$('#tweets div#wrap' + i + ' div.commentBlock div.UIImageBlock_Content').append('<p class="createdTime">' + adr.prettifyDate(Date.parse(data[i].created_at)) + '</p>');
			}
		} else {
			$('#tweets').html('<p id="loading">There was an error retrieving the Twitter stream.</p>');
		}
	},
	parseTweet: function(tweet) {
		var twText = tweet.text;
		if (tweet.entities.urls) {
			for (var j in tweet.entities.urls) {
				twText = twText.replace(tweet.entities.urls[j].url, '<a href="' + tweet.entities.urls[j].url + '" target="_blank">' + tweet.entities.urls[j].display_url + '</a>');
			}
		}
		if (tweet.entities.media) {
			for (var j in tweet.entities.media) {
				twText = twText.replace(tweet.entities.media[j].url, '<a href="' + tweet.entities.media[j].url + '" target="_blank">' + tweet.entities.media[j].display_url + '</a>');
			}
		}
		if (tweet.entities.user_mentions) {
			for (var j in tweet.entities.user_mentions) {
				if (twText.indexOf(tweet.entities.user_mentions[j].screen_name.toLowerCase()) !== -1) {
					twText = twText.replace('@' + tweet.entities.user_mentions[j].screen_name.toLowerCase(), '<a href="https://twitter.com/' + tweet.entities.user_mentions[j].screen_name + '" target="_blank">@' + tweet.entities.user_mentions[j].name + '</a>');
					continue;
				}
				if (twText.indexOf(tweet.entities.user_mentions[j].screen_name) !== -1) {
					twText = twText.replace('@' + tweet.entities.user_mentions[j].screen_name, '<a href="https://twitter.com/' + tweet.entities.user_mentions[j].screen_name + '" target="_blank">@' + tweet.entities.user_mentions[j].name + '</a>');
				}
			}
		}
		return twText;
	}
}