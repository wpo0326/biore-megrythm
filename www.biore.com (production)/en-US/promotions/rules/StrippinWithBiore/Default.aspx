﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" Inherits="Biore2012.__days" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedrules_#.css")
    %>
    
    <style type="text/css">
    .rules #polaroid img {
        width: 90%;
    }
    .rules #mainContent 
    {
        height: auto !important;
    }
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <div id="polaroid"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/forms/what-happens-on-the-strip.jpg") %>" alt="" /></div>
                <div id="content">
                    <h1>Bioré #StrippinWithBiore Instagram Sweepstakes</h1>
                    <h2>OFFICIAL RULES<br />
                    NO PURCHASE NECESSARY TO ENTER. A PURCHASE DOES NOT IMPROVE YOUR CHANCES OF WINNING.</h2>
             
             <p><b>Sweepstakes may only be entered in or from the 50 United States and the District of Columbia and entries originating from any other jurisdiction are not eligible for entry.  This Sweepstakes is governed exclusively by the laws of the United States.  You are not authorized to participate in the Sweepstakes if you are not located within the 50 United States or the District of Columbia.</b></p>

                     <p><strong>1. Eligibility:</strong> Participation in the Bioré #StrippinWithBiore Instagram Sweepstakes (“Sweepstakes”) is open to individuals who (i) are legal residents of and residing in the 50 United States or the District of Columbia at time of entry; and (ii) are age 18 or older.  Employees of Kao USA Inc. (“Sponsor”) and Helen & Gertrude Inc. (“Administrator”), and each of their respective parents, subsidiaries, affiliates, partners, advertising and promotion agencies, manufacturers or distributors of Sweepstakes materials and their immediate families (parents, children, siblings, spouse) or members of the same household (whether related or not) of such employees/officers/directors are not eligible to register or win.  All federal, state and local laws and regulations apply.  Void outside of the 50 United States, the District of Columbia, and where prohibited by law.</p>

                 	
                <p><strong>2. Timing:</strong> Sweepstakes begins at 9:00 am Eastern Time ("ET") on May 21, 2018, and ends at 11:59 pm on May 25, 2018 (“Sweepstakes Period”). </p>

                <p><strong>3. How to Enter:</strong> On May 21, 2018, beginning at 9:00 am ET, participants can enter via Instagram as follows:
Log in to or create your Instagram account and follow the official Bioré U.S. Instagram feed @bioreus and then 1) “like” the brand post announcing the Sweepstakes, and 2) tag your friend (with their permission) (collectively, your “Entry”).  You will automatically receive one (1) Entry into the drawing for the Sweepstakes for both you and the friend tagged in the post. Any Entry that does not comply with the entry requirements will be deemed invalid. You will automatically receive one (1) entry into the drawing for the Sweepstakes. 
</p>

                <p>You may enter one (1) time per day during the Sweepstakes Period, regardless of your method of entry.  Use of any automated system to participate is prohibited and will result in disqualification. In the event of a dispute as to any Entry, the authorized account holder of the email address used to register will be deemed to be the entrant or participant.  The "authorized account holder" is the natural person assigned an email address by an Internet access provider, online service provider or other organization responsible for assigning email addresses for the domain associated with the submitted address. The potential winner may be required to show proof of being the authorized account holder.</p>

              <p><strong>4.  Random Drawing/Odds.</strong>  2 winners will be selected in a random drawing from all eligible entries received during the Sweepstakes Period.  Odds of winning depend on the number of eligible entries received for the drawing.  Drawing will be conducted by Administrator on or about May 29, 2018. By entering the Sweepstakes, entrants fully and unconditionally agree to be bound by these Official Rules and the decisions of the Sponsor, which will be final and binding in all matters relating to the Sweepstakes. Sponsor will announce the potential winners via Sponsor’s Bioré US Instagram account on or about May 25, 2018 via a direct Instagram Message to the winner (“Winner Announcement”). The potential winners must contact Sponsor via a direct Instagram message within 3 business days of the Winner Announcement to claim the prize. Except where prohibited, each potential prize winner (or parent/legal guardian if winner is a minor in his/her state of residence) will be required to sign and return a Declaration of Compliance, Liability and Publicity Release, which must be received by Sponsor within seven (7) days of receipt from Sponsor, in order to claim the prize. If a potential winner of any prize cannot be contacted, fails to sign and return the Declaration (if applicable), or the prize is returned as undeliverable, the potential winner forfeits his/her prize. Receiving a prize is contingent upon compliance with these Official Rules.  In the event that a potential winner is disqualified for any reason, Sponsor will award the applicable prize to an alternate winner by random drawing from among all remaining eligible entries. Only three (3) alternate drawings will be held, after which the prize will remain un-awarded.</p>

<p><strong>5.  Prizes.</strong> Two prizes will be awarded.  Each prize includes economy-class air transportation for winner and a guest from a major airport near winner’s home (determined by Sponsor in its sole discretion) to Las Vegas, Nevada, USA and a 3 day/2 night stay at a four star hotel in Las Vegas, NV for winner and guest (double occupancy); a travel allowance of $600 cash; and two nonrefundable tickets to both days of iHeart Fall Festival, scheduled to be held on September 21-22, 2018 (collectively, the “Prize”). Each winner and guest must travel on the dates determined by Sponsor in its sole discretion or Prize will be forfeited.  ARV of each Prize is USD $6,908; total value of all prizes is USD $13,816.  Actual value may vary based on fare fluctuations and distance between departure and destination. Winner will not receive difference between actual and approximate retail value.   </p>

<p><strong>Additional terms for Prize: </strong> The Prize winner and his/her guest must travel together on the same itinerary. Air travel must be round trip.  Sponsor will determine airline and flight itinerary in its sole discretion.  No refund or compensation will be made in the event of the cancellation or delay of any flight. If, in the judgment of Sponsor, air travel is not required due to winner’s proximity to prize location, ground transportation will be substituted for round trip air travel at Sponsor’s sole discretion. Travel companion must execute liability/publicity release prior to issuance of travel documents. Travel is subject to the terms and conditions set forth in this Sweepstakes, and those set forth by the Sponsor’s airline carrier of choice as detailed in the passenger ticket contract. All expenses and costs associated with the acceptance or use of the prize that are not expressly specified in these Official Rules as being part of the prize (including without limitation travel insurance, laundry service, premium alcoholic beverages, local and long-distance telephone calls, merchandise and souvenirs, incidental expenses, etc.) are the responsibility of the winner. Sponsor will not replace any lost, mutilated, or stolen tickets, travel vouchers or certificates.  If the winner or winner's guest is under age 21 or otherwise under the age of majority in the jurisdiction where he or she resides, he or she must be accompanied by a parent, guardian or chaperone that is at least 21 years old.  </p>

<p>Prizes are non-transferable.  No substitutions or cash redemptions.  In the case of unavailability of any prize, Sponsor reserves the right to substitute a prize of equal or greater value.  Prize Winner will not receive difference between actual and approximate retail value set forth in these Official Rules. All taxes and unspecified expenses are the responsibility of winners.</p>

<p><strong>6.  Conditions.</strong> By participating, entrants and winner agree to release and hold harmless Sponsor, Administrator, Facebook, Inc., Instagram LLC, and their advertising and promotions agencies, and each of their respective parent companies, subsidiaries, affiliates, partners, representatives, agents, successors, assigns, employees, officers and directors, from any and all liability, for loss, harm, damage, injury, cost or expense whatsoever including without limitation, property damage, personal injury and/or death which may occur in connection with, or participation in Sweepstakes, or possession, acceptance and/or use or misuse of prize or participation in any Sweepstakes-related activity and claims based on publicity rights, defamation or invasion of privacy and merchandise delivery. Entrants who do not comply with these Official Rules, or attempt to interfere with this Sweepstakes in any way shall be disqualified.</p>

<p><strong>7.  Additional Terms</strong>  Sponsor reserves the right, in its sole discretion, to cancel, terminate, modify, or suspend this Sweepstakes should (in its sole discretion) virus, bugs, unauthorized human intervention or other causes beyond its control corrupt or affect the administration, security, fairness or proper conduct of the Sweepstakes. In such case, Sponsor may select the winners from all eligible entries received prior to and/or after (if appropriate) the action taken by Sponsor. Sponsor reserves the right, at its sole discretion, to disqualify any individual it finds, in its sole discretion, to be tampering with the entry process or the operation of the Sweepstakes, web site or application. Additionally, Sponsor reserves the right to prosecute any fraudulent activities to the full extent of the law. Any other attempted form of entry is prohibited; no automatic, programmed; robotic or similar means of entry are permitted. Sponsor, and its parent companies, subsidiaries, affiliates, partners and Sweepstakes and advertising agencies are not responsible for technical, hardware, software, telephone or other communications malfunctions, errors or failures of any kind, lost or unavailable network connections, Web site, Internet, or ISP availability, unauthorized human intervention, traffic congestion, incomplete or inaccurate capture of entry information (regardless of cause) or failed, incomplete, garbled, jumbled or delayed computer transmissions which may limit one's ability to enter the Sweepstakes, including any injury or damage to participant's or any other person's computer relating to or resulting from participating in this Sweepstakes or downloading any materials in this Sweepstakes. </p>

<p><strong>8.	Winner’s List.</strong> To obtain a list of winners, send a self-addressed, stamped envelope by June 25, 2018, to Helen & Gertrude, ATTN: Bioré #StrippinWithBiore Instagram Sweepstakes, 127 Railroad St, Suite 210, Rochester, NY 14609.</p>

<p><strong>9. Privacy.</strong>  Any entry information collected from the Sweepstakes shall be used only in a manner consistent with the consent given by entrants at the time of the entry, with these Official Rules and with the Kao USA Inc. Privacy Policy.  By participating in the Sweepstakes, entrants hereby agree to Sponsor's collection and usage of their personal information in accordance with Sponsor's privacy policy and acknowledge that they have read and accepted Sponsor's privacy policy located at http://www.biore.com/en-US/privacy/.</p>

<p><strong>10. Sponsor.</strong> Kao USA Inc., 2535 Spring Grove Avenue, Cincinnati, OH 45214-1773 </p> 

<p><b>The sweepstakes is in no way sponsored, endorsed or administered by, or associated with, Facebook, Inc. or Instagram, LLC.</b></p>

<p>©2018 Kao USA Inc. All rights reserved.</p>


                </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
