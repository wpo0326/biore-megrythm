﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" Inherits="Biore2012.__days" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedrules_#.css")
    %>
    
    <style type="text/css">
    .rules #polaroid img {
        width: 90%;
    }
    .rules #mainContent 
    {
        height: auto !important;
    }
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <div id="polaroid"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/forms/pop-sparkle-chill.jpg") %>" alt="Sweepstakes" /></div>
                <div id="content">
                    <h1>Bioré Pop, Sparkle, Chill Instagram Sweepstakes</h1>
                    <h2>Official Rules<br />
                    NO PURCHASE NECESSARY TO ENTER OR WIN. A PURCHASE DOES NOT IMPROVE YOUR CHANCES OF WINNING.</h2>
             
             <p><b>Sweepstakes may only be entered in or from the 50 United States and the District of Columbia and entries originating from any other jurisdiction are not eligible for entry.  This Sweepstakes is governed exclusively by the laws of the United States.  You are not authorized to participate in the Sweepstakes if you are not located within the 50 United States or the District of Columbia.</b></p>

                     <p><strong>1. Eligibility:</strong> Participation open only to legal residents of the fifty United States or the District of Columbia, who are 18 or older as of date of entry.  Void outside of the 50 United States and the District of Columbia, and where prohibited, taxed or restricted by law. Employees, officers and directors of Sponsor and its parent companies, subsidiaries, affiliates, partners, advertising and promotion agencies, manufacturers or distributors of Sweepstakes materials and their immediate families (parents, children, siblings, spouse) or members of the same household (whether related or not) of such employees/officers/directors are not eligible to enter.  Sweepstakes may only be entered in or from the 50 United States and the District of Columbia, and entries originating from any other jurisdiction are not eligible for entry.  All federal, state and local laws and regulations apply.</p>

                 	
                <p><strong>2. Timing:</strong> The Sweepstakes consists of three (3) daily giveaways from December 19, 2018 through December 21, 2018 (each a “Daily Giveaway”), each commencing at approximately at 9:00 am Eastern Time ("ET") and ending at 11:59 pm ET (the "Daily Giveaway Period").</p>

                <p><strong>3. How to Enter:</strong> Log in to or create your Instagram account and follow the official Bioré® U.S. Instagram account @bioreus. Then (i) ‘like’ the designated brand post announcing the daily prize and entry requirements (“Daily Post”) and (ii) respond in the comment section of the Daily Post as directed in the Daily Post. You will automatically receive one (1) entry into the drawing for that Daily Giveaway. Entry into any Daily Giveaway is by this process only.</p>

                <p>Limit 1 entry per person per Instagram account per day.  No automated entry devices and/or programs permitted.  Sponsor is not responsible for lost, late, illegible, stolen, incomplete, invalid, unintelligible, misdirected, technically corrupted or garbled entries or mail, which will be disqualified, or for problems of any kind whether mechanical, human or electronic.  Proof of submission will not be deemed to be proof of receipt by Sponsor.</p>
             
             <p>By entering the Sweepstakes, entrants fully and unconditionally agree to be bound by these rules and the decisions of the judges, which will be final and binding in all matters relating to the Sweepstakes.</p>

              <p><strong>4.  Random Drawing/Odds.</strong> One (1) winner will be selected in a random drawing from all eligible entries received during each Daily Giveaway Period.  The drawings will be held on or about January 2, 2019.  Sponsor will announce the winners of each Daily Giveaway in the comment section of the original Daily Post on or about January 2, 2019 (“Winner Announcement”) and will also send winner a direct message on Instagram. The winner must contact Sponsor via a direct message on Instagram within 2 days of the Winner Announcement to redeem their prize and to provide mailing address information. If the winner does not redeem the prize within 2 days of the Winner Announcement, the prize will be forfeited, and Sponsor will award the prize to an alternate winner randomly from the original pool of entrants.  If the prize cannot be awarded after three alternate winners have been notified, the prize will remain unawarded.</p>

<p><strong>5.  Prizes.</strong> Each of the 3 winners will receive the following Daily Giveaway prizes:</p>

<p><strong>Daily Prize 1:</strong> (i) six (6) bottles of Bioré Blue Agave Baking Soda Whipped Nourishing Detox Mask (ARV $38.82) and (ii) Sleeping Eye Mask (ARV $14.99), (iii) Soft Slipper Socks - 3 Pair Pack (ARV $9.99), (iv) Candellana Candle (ARV $7.46), and (v) Fizzy Fuzzy Bath Bombs (ARV $20); Total ARV $91.26.</p>

<p><strong>Daily Prize 2:</strong> (i) three (3) bottles of Witch Hazel Pore Clarifying Toner (ARV $19.47), (ii) three (3) boxes of Bioré Witch Hazel Ultra Deep Cleansing Pore Strips (ARV $17.97), (iii) 1 (one) pair of Privé Revaux sunglasses (ARV $29.95), (iv) Glitter Clutch Bag (ARV $18.99), (v.) Birthstone Keychain (ARV $24), (vi) Gold USB Phone Charger (ARV $8.68), (vii) Glitter Beverage Tumbler (ARV $9.99); Total ARV $129.05.</p>

<p><strong>Daily Prize 3:</strong> (i) three (3) boxes of Bioré Charcoal Pore Strips (ARV $17.97), (ii) three (3) bottles of Bioré Charcoal Whipped Purifying Detox Mask (ARV $19.47), (iii) three (3) travel size bottles of Charcoal Cleansing Micellar Water (ARV $8.97), (iv) Makeup Brushes (ARV $13.99), (v.) Holographic Makeup Bag (ARV $5.98), (vi.) Beauty Blender (ARV $18.00), and (vii) selfie ring light (ARV $12.49); total ARV $96.87. </p>

<p>Total ARV of all prizes: $317.18.</p>

<p>Limit one (1) Prize per person.  Prizes are non-transferable.  No substitutions or cash redemptions.  In the case of unavailability of any prize, Sponsor reserves the right to substitute a prize of equal or greater value.  All expenses not specifically listed herein are the responsibility of winners.  All federal, state and local taxes are the sole responsibility of the winner.</p>
 
<p><strong>6.  Prize Delivery.</strong> Winners will be sent Prize within 2-4 weeks of receipt of winner’s mailing addresses.  Sponsor not responsible if the Prize cannot be delivered due to an incorrect mailing address or for lost or misdirected requests.</p>

<p><strong>7.  Conditions.</strong> All taxes are the sole responsibility of the winners. By participating, entrants and winners agree to release and hold harmless Sponsor, its advertising and promotion agencies and its parent companies, subsidiaries, affiliates, partners, representatives, agents, successors, assigns, employees, officers and directors, from any and all liability, for loss, harm, damage, injury, cost or expense whatsoever including without limitation, property damage, personal injury and/or death which may occur in connection with, preparation for, travel to, or participation in Sweepstakes, or possession, acceptance and/or use or misuse of prize or participation in any Sweepstakes-related activity and claims based on publicity rights, defamation or invasion of privacy and merchandise delivery.  Entrants who do not comply with these Official Rules, or attempt to interfere with this promotion in any way shall be disqualified. Prizes are non-transferable.  No substitutions or cash redemptions.  In the case of unavailability of any prize, Sponsor reserves the right to substitute a prize of equal or greater value.  There is no purchase or sales presentation required to participate. A purchase does not increase odds of winning.</p>

<p><strong>8.  Additional Terms for Online Sweepstakes.</strong>  Sponsor reserves the right, in its sole discretion, to cancel, terminate, modify, or suspend this Sweepstakes should (in its sole discretion) virus, bugs, non-authorized human intervention or other causes beyond its control corrupt or affect the administration, security, fairness or proper conduct of the Sweepstakes.  In such case, Sponsor will select the winners from all eligible entries received prior to and/or after (if appropriate) the action taken by Sponsor. Sponsor reserves the right, at its sole discretion, to disqualify any individual it finds, in its sole discretion, to be tampering with the entry process or the operation of the Sweepstakes or Web site. For details regarding collection of information from users of the Web site (including entrants), please consult the privacy policy on the web site. Additionally, Sponsor reserves the right to prosecute any fraudulent activities to the full extent of the law.  In case of dispute as to the identity of any entrant, entry will be declared made by the authorized account holder of the email address submitted at time of entry. “Authorized Account Holder” is defined as the natural person who is assigned an email address by an Internet access provider, online service provider, or other organization (e.g., business, educational, institution, etc.) responsible for assigning email addresses or the domain associated with the submitted email address. Any other attempted form of entry is prohibited; no automatic, programmed; robotic or similar means of entry are permitted. Sponsor, and its parent companies, subsidiaries, affiliates, partners and promotion and advertising agencies are not responsible for technical, hardware, software, telephone or other communications malfunctions, errors or failures of any kind, lost or unavailable network connections, Web site, Internet, or ISP availability, unauthorized human intervention, traffic congestion, incomplete or inaccurate capture of entry information (regardless of cause) or failed, incomplete, garbled, jumbled or delayed computer transmissions which may limit one's ability to enter the Sweepstakes, including any injury or damage to participant’s or any other person’s computer relating to or resulting from participating in this Sweepstakes or downloading any materials in this Sweepstakes.</p>

<p><i>CAUTION: ANY ATTEMPT BY AN ENTRANT TO DELIBERATELY DAMAGE ANY WEB SITE OR UNDERMINE THE LEGITIMATE OPERATION OF THE SWEEPSTAKES MAY BE A VIOLATION OF CRIMINAL AND CIVIL LAWS AND SHOULD SUCH AN ATTEMPT BE MADE, THE SPONSOR RESERVES THE RIGHT TO SEEK DAMAGES FROM ANY SUCH PERSON TO THE FULLEST EXTENT PERMITTED BY LAW.</i></p>

<p><strong>9.  Release:</strong> By receipt of any prize, winners agree to release and hold harmless Sponsor, Instagram, LLC, and Helen & Gertrude, and their respective subsidiaries, affiliates, suppliers, distributors, advertising/promotion agencies, and prize suppliers, and each of their respective parent companies and each such company’s officers, directors, employees and agents (collectively, the “Released Parties”) from and against any claim or cause of action, including, but not limited to, personal injury, death, or damage to or loss of property, arising out of participation in the Sweepstakes or receipt or use or misuse of any prize.</p>

<p><strong>10. Use of Data.</strong>  Sponsor will be collecting personal data about entrants online, in accordance with its privacy policy.  Please review the Sponsor’s privacy policy at http://www.biore.com/en-US/privacy/.  By participating in the Sweepstakes, entrants hereby agree to Sponsor’s collection and usage of their personal information and acknowledge that they have read and accepted Sponsor’s privacy policy. </p>

<p><strong>11. List of Winners.</strong> To obtain the name of the winner, send a self-addressed, stamped envelope by March 31, 2019, to Helen & Gertrude, Inc, 127 Railroad Street Suite 210, Rochester, NY 14609.</p>

<p><strong>12. Sponsor.</strong> Kao USA Inc., 2535 Spring Grove Avenue, Cincinnati, OH 45214-1773. </p> 

<p><b>This promotion is in no way sponsored, endorsed or administered by, or associated with Instagram LLC. Entrants understand that they are providing their information to the Sponsor and not to Instagram. The information entrants provide will only be used in accordance with these terms and conditions. Any questions, comments or complaints regarding this promotion must be directed to the Sponsor and not to Instagram.  Further, entrants release Instagram and its associated companies from all liability arising in respect of the promotion.</b></p>


                </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
