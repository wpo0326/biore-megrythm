﻿/*------------------------------------------*/
/* GA Event Tracking                        */
/* Mike Taylor - CG Marketing Communications*/
/*-------------------- ---------------------*/

$(function () {
    var urlPath = window.location.pathname;
    var urlPathArray = urlPath.split('/');
    var urlPathCount = urlPathArray.length - 1;
    var urlPageName = urlPathArray[urlPathCount];
    if (urlPageName == "")
        urlPageName = urlPathArray[(urlPathCount - 1)]; //if no filename found in ending path, move 1 step back in URL tree.

    var section = "";
    var subsection = "";
    var event = "";

    $('A').click(function () {

        if ($(this).attr("id")) {
            var str = $(this).attr("id");
            str = str.replace("ctl00_ContentPlaceHolder1_", "");

            section = urlPageName;
            subsection = str;
            event = "click";

            trackEvent(section, subsection, event);
        }

    });


    // click event for accordian on Product Detail
    $(".contentHolder h3").click(function() {

        str = $(this).attr("id");

        section = urlPageName;
        subsection = str;
        event = "click";

        trackEvent(section, subsection, event);
    });

    //click event for homepage theater circle nav
    $(".flex-control-nav li a").click(function() {

        var theaterSlides = [];
        var theaterCount = 0;

        $(".slides li a").each(function() { theaterSlides.push($(this).attr("id")); theaterCount++; });

        for (i = 0; i <= theaterCount; i++) {
            if ((parseInt($(this).text()) - 1) == i) {
                section = urlPageName;
                subsection = "circle-navigate-" + theaterSlides[i].replace("ctl00_ContentPlaceHolder1_", "");
                event = "click";
                trackEvent(section, subsection, event);

            }

        }

    });

    //look for a "GATrack" class.  
    //e.g. "GATrack-IdentifierCategory-IdentifierSubCategory"
    $('A[class*="GATrack"]').click(function () {

        var trackClass = $(this).attr('class');
        var ary = trackClass.split("-");

        section = "";
        subsection = "";
        event = "";
        if (ary[0] != undefined)
            section = ary[0];
        if (ary[1] != undefined)
            subsection = ary[1];
        if (ary[2] != undefined)
            event = ary[2];

        trackEvent(section, subsection, event);

    });

});

function trackPage(pageName){
	//alert("Tracking: " + pageName);
    //pageTracker._trackPageview(pageName);
    //_gaq.push(['_trackPageview', pageName]);
    firstTracker._trackPageview(pageName);
    secondTracker._trackPageview(pageName);
}

function trackEvent(section, subsection, event){
	if(section == undefined)
		section = "";
	if(subsection == undefined)
		subsection = "";
	if(event == undefined)
		event = "";
	//alert("Event: " + section + " : " + subsection + " : " + event);
	//pageTracker._trackEvent(section, subsection, event);
//_gaq.push(['_trackEvent', section, subsection, event]);
    firstTracker._trackEvent(section, subsection, event);
    secondTracker._trackEvent(section, subsection, event);
}

function trackSpotlight(url){
	// for doublelick-related tags
	//alert("Spotlight: " + url);

	var axel = Math.random()+"";
	var a = axel * 10000000000000;
	
	var img = document.createElement('img');
	img.src = url + ';num=' + a;	
}