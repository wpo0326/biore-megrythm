﻿$(document).ready(function () {
    // Event to solldown to review tab and active the tab when the user click on show review 
    $BV.ui('rr', 'show_reviews', {
        doShowContent: function () {
            $('#productTabs ul li a').removeClass('active');
            $('.tabReviews a').addClass('active');
            $('#tabsContent div').removeClass('active').hide();
            $('.contentReviews').addClass('active').show();
            $('#BVRRContainer').show();
            Tagging.tracking("BazaarVoice Reviews","Click","ViewReview");
        }
    });
});