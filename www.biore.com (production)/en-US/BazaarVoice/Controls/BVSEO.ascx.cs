﻿using System;

namespace Biore.BazaarVoice.Controls
{
    public partial class BVSEO : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string category = string.Empty;
            string productId = BVUtils.GetProductId(Request, out category);
            if (!string.IsNullOrEmpty(productId) && string.IsNullOrEmpty(category))
            {
                string sBvOutputSummary = string.Empty;
                string sBvOutputReviews = string.Empty;

                if (BVUtils.GetBVSEO(Request, productId, out sBvOutputSummary, out sBvOutputReviews))
                {
                    BVRRSummaryContainer.InnerHtml = sBvOutputSummary;
                    BVRRContainer.InnerHtml = sBvOutputReviews;
                }
            }
        }
    }
}