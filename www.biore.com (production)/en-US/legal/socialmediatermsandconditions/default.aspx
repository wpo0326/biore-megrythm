﻿<%@ Page Title="Legal | Bior&eacute;&reg; Skincare" MasterPageFile="~/Site.Master"
    Language="C#" AutoEventWireup="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
    <style type="text/css">
        #content p
        {
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="about">
        <div id="main">
            <div id="mainContent">
                <div class="centeringDiv">
                    <div id="content">
                        <div class="titleh1">
                            <h1>
                                Bior&eacute;<sup>&reg;</sup> Social</h1>
                            <div id="responseRule">
                            </div>
                        </div>

                <p><b>Thank you for engaging with Bioré® Skin Care on Social Media.</b>
                <br /><br />
These Terms constitute a binding agreement between you and Kao USA Inc., owner of the Bioré® brand, and its affiliates and subsidiaries <b>("Kao," "we," "us")</b>.  You accept these Terms if you affirmatively respond to Kao’s request to regram, repost or otherwise use content originally posted by you, including photographs, video, and text (“Your Content”), by replying <b>#agreebiore or otherwise indicating your agreement.</b>  If you do not accept these Terms, do not provide your affirmative response. 
                <br /><br />
By affirmatively responding to our request, you agree to the following:
                <br /><br />
<b>Representations.</b> You represent and warrant that (i) you are at least 18 years of age and the age of majority in your place of residence; (ii) you have the full right, power and authority to enter into this Agreement and grant the rights granted herein; (iii) you are the sole and exclusive owner of all intellectual property rights and any other rights in and to Your Content or have the necessary rights and authority to sublicense Your Content to Kao as set forth herein; (iv) the use of the Your Content as described herein will not violate the intellectual property rights or any other rights, including the rights of privacy and publicity, of any third party; and (v) this Agreement does not in any way conflict with any existing commitments on Your part. 
                <br /><br />
You agree to defend, indemnify and hold Kao harmless from and against any and all losses, costs, liability, damages, and claims of any nature, including but not limited to attorneys’ fees, arising from, growing out of, or concerning a breach of any representation or warranty made herein. 
                <br /><br />
<b>Ownership.</b> You will retain ownership to Your Content including the copyright.
                <br /><br />
<b>License/Waiver.  You consent to give Kao a royalty-free, irrevocable, perpetual, transferrable, non-exclusive license to use, reproduce, modify, publish, create derivative works from, and display Your Content in whole or in part, on a worldwide basis, and to incorporate it into other works, in any form, media or technology now known or later developed, including for promotional, marketing, advertising, and publicity purposes.</b>  You waive and release Kao from any and all claims that you may now or hereafter have in any jurisdiction based on “moral rights” with respect to Kao’s use and exploitation of Your Content.  You also unconditionally and irrevocably consent to any act or omission by Kao and any of its affiliates that would otherwise infringe your moral rights and present and future rights of a similar nature conferred by statute anywhere in the world whether occurring before or after this consent is given. You hereby waive any rights that you may have to inspect and approve of Kao’s use of Your Content as permitted herein. Nothing herein will constitute any obligation on the Kao to use any of the above rights.  
                <br /><br />
You agree to defend, indemnify and hold Kao harmless from and against any and all losses, costs, liability, damages, and claims of any nature, including but not limited to attorneys’ fees, arising from, growing out of, or concerning a breach of any warranty or representation made herein. 
                <br /><br />
You further agree that you will not hold Kao, or anyone who receives permission from either of us responsible for any liability resulting from the use of the Your Content in accordance with the terms hereof, including what might be deemed to be misrepresentation due to distortion, optical illusion or faulty reproduction which may occur in the finished product.  </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
