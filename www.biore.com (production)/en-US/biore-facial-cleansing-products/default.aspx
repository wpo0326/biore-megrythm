﻿<%@ Page Title="Free Your Pores – Get Clean, Healthy Skin – Browse All Products  | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.our_products.Default" %>
<%@ Register Src="~/BazaarVoice/Controls/BVScripts.ascx" TagPrefix="uc1" TagName="BVScripts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Bior&eacute;&reg; Skincare products cleanse and gently exfoliate, giving you healthy, clean skin! See the complete Bior&eacute;&reg; Skincare product line." name="description" />
    <meta content="Clear Skin, Biore&reg; Products" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>
    <meta name="viewport" content="width=device-width">
    <uc1:BVScripts runat="server" ID="BVScripts" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">
<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: Biore Product Page
URL of the webpage where the tag is expected to be placed: http://www.biore.com/en-US/biore-facial-cleansing-products/
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 02/01/2012
-->
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="http://fls.doubleclick.net/activityi;src=1418070;type=biore797;cat=biore083;ord=' + a + '?" width="1" height="1" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="http://fls.doubleclick.net/activityi;src=1418070;type=biore797;cat=biore083;ord=1?" width="1" height="1" style="display:none"></iframe>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: BIORE_PRODUCT_HP_PL
URL of the webpage where the tag is expected to be placed: http://www.biore.com/en-US/biore-facial-cleansing-products/
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 03/05/2014
-->
<script type="text/javascript">
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    document.write('<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore735;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore735;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>

<!-- End of DoubleClick Floodlight Tag: Please do not remove -->

    <!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: BIORE_CLEANSINGHome_PL
URL of the webpage where the tag is expected to be placed: http://www.biore.com/en-US/biore-facial-cleansing-products/
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 04/09/2015
-->
    <script type="text/javascript">
        var axel = Math.random() + "";
        var a = axel * 10000000000000;
        document.write('<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore000;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
    </script>
    <noscript>
    <iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore000;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
    </noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="intro">
                <h1>Our Products</h1>
                <div id="responseRule"></div>
                <p>Target the root of all skin problems &mdash; by stripping weekly and cleansing daily.  Our powerful, pore-cleansing products come in liquid, foam, scrub, and strip forms to keep your skin clean and healthy.</p>
            </div>

            <div id="twostepporekitsProds" class="prodList">
                <h2 class="pie roundedCorners twostepporekits"><span>two-step</span> pore kits
                    <span></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
 
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/two-step-pore-kit.png" alt="" />
                        <asp:Literal ID="litTwoStepPoreKit" runat="server" />
                        <h3>BIORÉ® 2-STEP<br />Pore Refining Kit</h3>
                        <p>Make it a double, for SRSLY purified pores</p>
                        <a href="../two-step-pore-kits/two-step-pore-kit" id="details-two-step-pore-kit">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/two-step-charcoal-pore-kit.png" alt="" />
                        <asp:Literal ID="litTwoStepCharcoal" runat="server" />
                        <h3>BIORÉ® 2-STEP<br />Deep Cleansing Charcoal Pore Kit</h3>
                        <p>Make it a double, for SRSLY purified pores</p>
                        <a href="../two-step-pore-kits/two-step-charcoal-pore-kit" id="details-two-step-charcoal-pore-kit">details ></a>
                    </li>
                </ul>
            </div>

            <div id="takeitalloffProds" class="prodList">
                <h2 class="pie roundedCorners takeitalloff">just take it <span>all off</span>
                    <span></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
 
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-cleansing-micellar-water.png" alt="" />
                        <asp:Literal ID="litBSMicellarWater" runat="server" />
                        <h3>BIORÉ® BAKING SODA<br />CLEANSING MICELLAR WATER</h3>
                        <p>Removes makeup, deep cleans pores and balances without over-drying.</p>
                        <a href="../take-it-all-off/baking-soda-cleansing-micellar-water" id="details-baking-soda-cleansing-micellar-water">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-cleansing-micellar-water.png" alt="" />
                        <asp:Literal ID="litCharcoalMicellarWater" runat="server" />
                        <h3>BIORÉ® CHARCOAL CLEANSING<br />MICELLAR WATER</h3>
                        <p>Removes makeup, deep cleans pores and removes excess oil. </p>
                        <a href="../take-it-all-off/charcoal-cleansing-micellar-water" id="details-charcoal-cleansing-micellar-water">details ></a>
                    </li>
                </ul>
            </div>

            <div id="complexionClearingProds" class="prodList">
                <h2 class="pie roundedCorners complexionClearing">acne&rsquo;s <span>outta here!<sup>&reg;</sup></span>
                    <span></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-acne-cleansing-foam.png" alt="" style="bottom:155px;" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:9pt;"></span>BIORÉ BAKING SODA ACNE CLEANSING FOAM</h3>
                        <p>Gentle foam targets acne & deep cleans pores</p>
                        <div id="BVRRInlineRating-baking-soda-acne-cleansing-foam" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/baking-soda-acne-cleansing-foam" id="details-baking-soda-acne-cleansing-foam">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-acne-scrub.png" alt="" style="bottom:155px;" />
                        <h3>BIORÉ BAKING SODA ACNE SCRUB</h3>
                        <p>Unclogs pores and balances skin to reduce breakouts.</p>
                        <div id="BVRRInlineRating-baking-soda-acne-scrub" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/baking-soda-acne-scrub" id="details-baking-soda-acne-scrub">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-acne-scrub.png" alt="" style="bottom:155px;" />
                        <h3>BIORÉ CHARCOAL ACNE SCRUB</h3>
                        <p>Smooths away acne causing dirt and absorbs excess oil to help eliminate breakouts.</p>
                        <div id="BVRRInlineRating-charcoal-acne-scrub" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/charcoal-acne-scrub" id="details-charcoal-acne-scrub">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-acne-clearing-cleanser.png" alt="" style="bottom: 172px; width: 86px;" />
                        <h3>BIORÉ CHARCOAL ACNE CLEARING CLEANSER</h3>
                        <p>Deep cleans, penetrates pores, and absorbs oil to help stop breakouts.</p>
                        <div id="BVRRInlineRating-charcoal-acne-clearing-cleanser" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/charcoal-acne-clearing-cleanser" id="details-charcoal-acne-clearing-cleanser">details ></a>
                    </li>
                    
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishFightingIceCleanser.png" alt="" />
                        <h3>BIORÉ BLEMISH FIGHTING ICE CLEANSER</h3>
                        <p>With Salicylic acid, this formula cleans pores of dirt, oil, and makeup.</p>
                        <div id="BVRRInlineRating-blemish-fighting-ice-cleanser" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/blemish-fighting-ice-cleanser" id="details-blemish-fighting-ice-cleanser">details ></a>
                    </li>
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/ACS_Tube.jpg" alt="" />
                        <h3>ACNE CLEARING<br />SCRUB</h3>
                        <p>Defeat deep-down dirt and oil in 2 days.</p>
                        <div id="BVRRInlineRating-acne-clearing-scrub" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/acne-clearing-scrub" id="details-acne-clearing-scrub">details ></a>
                    </li>-->
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishTreatingAstringentToner.png" alt="" />
                        <h3>BIORÉ WITCH HAZEL PORE CLARIFYING TONER</h3>
                        <p>Tones, refines and deep cleans while removing traces of dirt, oil and make-up.</p>
                        <div id="BVRRInlineRating-witch-hazel-pore-clarifying-toner" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/witch-hazel-pore-clarifying-toner" id="details-witch-hazel-pore-clarifying-toner">details ></a>
                    </li>
                </ul>
            </div>

            <div id="deepCleansingProds" class="prodList">
                <h2 class="pie roundedCorners deepCleansing">don&rsquo;t be <span>dirty<sup>&reg;</sup></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blue-agave-baking-soda-instant-warming-clay-mask.png" alt="" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:8pt;"></span>BIORÉ Blue Agave Baking Soda Warming Clay Mask</h3>
                        <p>Deep Cleans, and Conditions for Smooth Skin</p>
                        <div id="BVRRInlineRating-warming-clay-mask" class="ratingCategory"></div>
                        <a href="../dont-be-dirty/warming-clay-mask" id="warming-clay-mask">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blue-agave-baking-soda-whipped-nourishing-detox-mask.png" alt="" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:8pt;"></span>BIORÉ Blue Agave Baking Soda Whipped Nourishing Detox Mask</h3>
                        <p>Eliminates impurities &amp; balances combination skin.</p>
                        <div id="BVRRInlineRating-blue-agave-baking-soda-whipped-nourishing-detox-mask" class="ratingCategory"></div>
                        <a href="../dont-be-dirty/blue-agave-baking-soda-whipped-nourishing-detox-mask" id="blue-agave-baking-soda-whipped-nourishing-detox-mask">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-whipped-purifying-detox-mask.png" alt="" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:8pt;"></span>BIORÉ Charcoal Whipped Purifying Detox Mask</h3>
                        <p>Eliminates impurities &amp; purifies pores.</p>
                        <div id="BVRRInlineRating-charcoal-whipped-purifying-detox-mask" class="ratingCategory"></div>
                        <a href="../dont-be-dirty/charcoal-whipped-purifying-detox-mask" id="charcoal-whipped-purifying-detox-mask">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img id="dontBeDirtyProducts" src="../images/ourProducts/products/small/blue-agave-baking-soda-balancing-pore-cleanser.png" alt="" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:8pt;"></span>BIORÉ Blue Agave Baking soda Pore Cleanser</h3>
                        <p>Deep Cleans, Conditions, and Exfoliates for Smooth Skin</p>
                        <div id="BVRRInlineRating-balancing-cleanser" class="ratingCategory"></div>
                        <a href="../dont-be-dirty/balancing-cleanser" id="A1">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-cleansing-scrub-small.png" alt="" />
                        <h3><span style="font-family: ProximaNova-Bold, Arial;color:yellow;font-size:8pt;"></span>BIORÉ Baking soda Cleansing scrub</h3>
                        <p>Deep clean without over-scrubbing.</p>
                        <div id="BVRRInlineRating-baking-soda-cleansing-scrub" class="ratingCategory"></div>
                        <a href="../dont-be-dirty/baking-soda-cleansing-scrub" id="A1">details ></a>
                    </li>
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalBar.png" alt="" />
                        <h3>BIORÉ Pore Penetrating Charcoal Bar</h3>
                        <p>Dual-action formula exfoliates & deep cleans pores.</p>
                        <div id="BVRRInlineRating-charcoal-bar" class="ratingCategory"></div>
                        <a href="../dont-be-dirty/charcoal-bar" id="A1">details ></a>
                    </li>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CHARCOAL_100X235.png" alt="" />
                        <h3>BIORÉ Deep Pore Charcoal Cleanser</h3>
                        <p>Charcoal formula acts as a magnet to draw out dirt and oil for a tingly clean.</p>
                        <div id="BVRRInlineRating-deep-pore-charcoal-cleanser" class="ratingCategory"></div>
                        <a href="../dont-be-dirty/deep-pore-charcoal-cleanser" id="A1">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/SelfHeatingMask.jpg" alt="" style="margin:0 0 2px 44px;"/>
                        <h3>BIORÉ Charcoal Self Heating One Minute Mask</h3>
                        <p>Heats on contact to remove dirt and oil for tingly-smooth, clean skin.</p>
                        <div id="BVRRInlineRating-self-heating-one-minute-mask" class="ratingCategory"></div>
                        <a href="../dont-be-dirty/self-heating-one-minute-mask" id="A2">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/poreUncloggingScrub.png" alt="" />
                        <h3>BIORÉ PORE UNCLOGGING SCRUB</h3>
                        <p>Smooths &amp; unclogs skin by targeting dirt and oil.</p>
                        <div id="BVRRInlineRating-pore-unclogging-scrub" class="ratingCategory"></div>
                        <a href="../dont-be-dirty/pore-unclogging-scrub" id="A3">details ></a>
                    </li>
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/combinationSkinBalancingCleanser.png" alt="" />
                        <h3>COMBINATION SKIN BALANCING CLEANSER</h3>
                        <p>Balance skin's moisture and address combination skin.</p>
                        <div id="BVRRInlineRating-combination-skin-balancing-cleanser" class="ratingCategory"></div>
                        <a href="../dont-be-dirty/combination-skin-balancing-cleanser" id="details-combination-skin-balancing-cleanser">details ></a>
                    </li>-->
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/dailyCleansingCloths.png" alt="" />
                        <h3>BIORÉ Daily Deep Pore Cleansing Cloths</h3>
                        <p>Wipe away dirt, oil and makeup with an exfoliating cloth.</p>
                        <div id="BVRRInlineRating-daily-cleansing-cloths" class="ratingCategory"></div>
                        <a href="../dont-be-dirty/daily-cleansing-cloths" id="A4">details ></a>
                    </li>
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreMinimizer.png" alt="" />
                        <h3>BIORÉ Charcoal<br />
                      Pore Minimizer</h3>
                        <p>Exfoliates and sucks up gunk for visibly smaller pores.</p>
                        <div id="BVRRInlineRating-charcoal-pore-minimizer" class="ratingCategory"></div>
                        <a href="../dont-be-dirty/charcoal-pore-minimizer" id="details-charcoal-pore-minimizer">details ></a>
                    </li>
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/makeupRemovingTowelettes.png" alt="" />
                        <h3>Daily Makeup Removing Towelettes</h3>
                        <p>Remove stubborn waterproof makeup and clean pores.</p>
                        <a href="../dont-be-dirty/make-up-removing-towelettes" id="details-make-up-removing-towelettes">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/4In1DetoxifyingCleanser.png" alt="" />
                        <h3>Pore Detoxifying Foam Cleanser</h3>
                        <p>Clean, tone, and stimulate with our self-foaming formula.</p>
                        <a href="../dont-be-dirty/pore-detoxifying-foam-cleanser" id="details-pore-detoxifying-foam-cleanser">details ></a>
                    </li>  -->  
                </ul>
            </div>

            <div id="murtProds" class="prodList">
                <h2 class="pie roundedCorners murt">breakup with <span>blackheads<sup>&reg;</sup></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltra.png" alt="" />
                        <h3>BIORÉ Witch Hazel Ultra Deep Cleansing Pore Strips</h3>
                        <p>Refine pores and remove your most stubborn blackheads with these extra-strength pore strips.</p>
                        <div id="BVRRInlineRating-pore-strips-ultra" class="ratingCategory"></div>
                        <a href="../breakup-with-blackheads/pore-strips-ultra" id="details-deep-cleansing-pore-strips-ultra">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStrips.png" alt="" />
                        <h3>BIORÉ DEEP CLEANSING PORE STRIPS</h3>
                        <p>Unclog pores with magnet-like power. Instantly removes weeks' worth of gunk. </p>
                        <div id="BVRRInlineRating-pore-strips-regular" class="ratingCategory"></div>
                        <a href="../breakup-with-blackheads/pore-strips" id="details-deep-cleansing-pore-strips">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/warmingAntiBlackheadCleanser.png" alt="" />
                        <h3>BIORÉ Warming Anti-Blackhead Cleanser</h3>
                        <p>Destroy dirt & oil with a warming formula of micro-beads. </p>
                        <div id="BVRRInlineRating-warming-anti-blackhead-cleanser" class="ratingCategory"></div>
                        <a href="../breakup-with-blackheads/warming-anti-blackhead-cleanser" id="details-deep-cleansing-pore-strips-combo">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.png" alt="" />
                        <h3>BIORÉ Deep Cleansing Pore Strips Combo</h3>
                        <p>Unclog pores with magnet-like power on your nose, chin, cheeks, and forehead.</p>
                        <div id="BVRRInlineRating-pore-strips-combo" class="ratingCategory"></div>
                        <a href="../breakup-with-blackheads/pore-strips-combo" id="details-deep-cleansing-pore-strips-combo">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" class="cps-custom"/>
                        <h3>BIORÉ Deep Cleansing Charcoal Pore Strips</h3>
                        <p>Unclogs pores &amp; draws out excess oil for the deepest clean.</p>
                        <div id="BVRRInlineRating-charcoal-pore-strips-regular" class="ratingCategory"></div>
                        <a href="../breakup-with-blackheads/charcoal-pore-strips" id="details-deep-cleansing-pore-strips-charcoal">details ></a>
                    </li>
                    
                    
                    <!-- <li>
                        <img src="../images/ourProducts/products/small/combinationSkinBalancingCleanser.png" alt="" />
                        <h3>COMBINATION SKIN BALANCING CLEANSER</h3>
                        <p>Get an even, all-over clean for the complex needs of combination skin.</p>
                        <a href="../dont-be-dirty/combination-skin-balancing-cleanser" id="details-combination-skin-balancing-cleanser">details ></a>
                    </li>
                    <li class="taller" id="dailyCleansingCloths">
                        <img src="../images/ourProducts/products/small/dailyCleansingCloths.png" alt="" />
                        <h3>DAILY CLEANSING CLOTHS</h3>
                        <p>Wipe away dirt to reveal healthy, glowing skin.</p>
                        <a href="../dont-be-dirty/daily-cleansing-cloths" id="details-daily-cleansing-cloths">details ></a>
                    </li>
                    
                    <li>
                        <img src="../images/ourProducts/products/small/4In1DetoxifyingCleanser.png" alt="" />
                        <h3>4-IN-1 DETOXIFYING CLEANSER</h3>
                        <p>Revitalize and refresh for healthier-looking skin.</p>
                        <a href="../dont-be-dirty/4-in-1-detoxifying-cleanser" id="details-4-in-1-detoxifying-cleanser">details ></a>
                    </li>
                     <li>
                        <img src="../images/ourProducts/products/small/steamActivatedCleanser.jpg" alt="" />
                        <h3>STEAM ACTIVATED CLEANSER</h3>
                        <p>Harness the power of pore-opening steam for a truly deep clean.</p>
                        <a href="../dont-be-dirty/steam-activated-cleanser" id="details-steam-activated-cleanser">details ></a>
                    </li> -->
                </ul>
            </div>
            
            <!--<div id="bigPoresProds" class="prodList">
                <h2 class="pie roundedCorners bigPores">back off <span>big pores<sup>&reg;</sup></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreMinimizer.png" alt="" />
                        <h3>Charcoal<br />
                      Pore Minimizer</h3>
                        <p>Exfoliates and sucks up gunk for visibly smaller pores.</p>
                        <div id="BVRRInlineRating-charcoal-pore-minimizer" class="ratingCategory"></div>
                        <a href="../back-off-big-pores/charcoal-pore-minimizer" id="details-charcoal-pore-minimizer">details ></a>
                    </li>
                    
                </ul>
            </div>-->

            <!--<div id="complexionClearingProds" class="prodList">
                <h2 class="pie roundedCorners complexionClearing">acne&rsquo;s <span>outta here!<sup>&reg;</sup></span>
                    <span></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-acne-scrub.png" alt="" style="bottom:155px;" />
                        <h3>BAKING SODA<br />ACNE SCRUB</h3>
                        <p>Unclogs pores and balances skin to reduce breakouts.</p>
                        <div id="BVRRInlineRating-baking-soda-acne-scrub" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/baking-soda-acne-scrub" id="details-baking-soda-acne-scrub">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-acne-scrub.png" alt="" style="bottom:155px;" />
                        <h3>CHARCOAL<br />ACNE SCRUB</h3>
                        <p>Smooths away acne causing dirt and absorbs excess oil to help eliminate breakouts.</p>
                        <div id="BVRRInlineRating-charcoal-acne-scrub" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/charcoal-acne-scrub" id="details-charcoal-acne-scrub">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-acne-clearing-cleanser.png" alt="" style="bottom: 172px; width: 86px;" />
                        <h3>CHARCOAL ACNE<br />CLEARING CLEANSER</h3>
                        <p>Deep cleans, penetrates pores, and absorbs oil to help stop breakouts.</p>
                        <div id="BVRRInlineRating-charcoal-acne-clearing-cleanser" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/charcoal-acne-clearing-cleanser" id="details-charcoal-acne-clearing-cleanser">details ></a>
                    </li>
                    
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishFightingIceCleanser.png" alt="" />
                        <h3>BLEMISH FIGHTING<br />ICE CLEANSER</h3>
                        <p>With Salicylic acid, this formula cleans pores of dirt, oil, and makeup.</p>
                        <div id="BVRRInlineRating-blemish-fighting-ice-cleanser" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/blemish-fighting-ice-cleanser" id="details-blemish-fighting-ice-cleanser">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/ACS_Tube.jpg" alt="" />
                        <h3>ACNE CLEARING<br />SCRUB</h3>
                        <p>Defeat deep-down dirt and oil in 2 days.</p>
                        <div id="BVRRInlineRating-acne-clearing-scrub" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/acne-clearing-scrub" id="details-acne-clearing-scrub">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishTreatingAstringentToner.png" alt="" />
                        <h3>BLEMISH FIGHTING ASTRINGENT</h3>
                        <p>Help prevent breakouts with an oil-free toner.</p>
                        <div id="BVRRInlineRating-blemish-fighting-astringent-toner" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/blemish-fighting-astringent-toner" id="details-blemish-fighting-astringent">details ></a>
                    </li>
                </ul>
            </div>-->

        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
