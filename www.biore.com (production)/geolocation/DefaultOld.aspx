﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DefaultOld.aspx.cs" Inherits="GeoLocation._DefaultOld" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="frmMain" runat="server">
    <p>
        IP Address/Domain Name&nbsp;
        <asp:TextBox ID="txtIPAddress" runat="server"></asp:TextBox>
        
        <asp:Button ID="btnQuery" runat="server" Text="Query" OnClick="btnQuery_Click"></asp:Button></p>
    <p>
        <asp:TextBox ID="txtIPResult" runat="server" TextMode="MultiLine" Width="512px" Height="100px"></asp:TextBox></p>
        
        <p>To get foreign IP blocks, visit:<br /><a href="http://www.countryipblocks.net/country-blocks/" target="_blank">http://www.countryipblocks.net/country-blocks/</a></p>
    </form>
</body>
</html>