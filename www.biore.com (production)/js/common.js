//==============================================================
// swap image
//==============================================================
function initRollOvers() {
	if (!document.getElementById){
		return;
	}

	var preLoads = new Array();
	var allImages = document.getElementsByTagName('img');

	for (var i = 0; i < allImages.length; i++) {		
		if (allImages[i].className == 'over') {
			var src = allImages[i].getAttribute('src');
			var ftype = src.substring(src.lastIndexOf('.'), src.length);
			var oSrc = src.replace(ftype, '_on'+ftype);

			//-- set image
			allImages[i].setAttribute('pSrc', src);
			allImages[i].setAttribute('oSrc', oSrc);

			//-- preload
			preLoads[i] = new Image();
			preLoads[i].src = oSrc;

			//-- set event
			allImages[i].onmouseover = function() {
				this.setAttribute('src', this.getAttribute('oSrc'));
			}
			allImages[i].onmouseout = function() {
				this.setAttribute('src', this.getAttribute('pSrc'));
			}
		}
	}
}
window.onload = function() {
initRollOvers()
}