$(function(){
	
	var $map = $("#map");
	var $links = $("#link");

	//--- onActiveCountry ---//		
	var onActiveCountry = function($target){
		
		var country = $target.attr("class");
		var $mapCountryItem = $map.find("." + country);
		var $linksCountryItem = $links.find("." + country);
		
		$mapCountryItem.addClass("on");
		$linksCountryItem.addClass("on");
		setActiveImage($mapCountryItem.find("img"));
		
		var $attention = $mapCountryItem.find(".attention");
		if($attention.length === 0){
			$attention = $("<p></p>").addClass("attention");
			$mapCountryItem.append($attention);
		}
		$attention.show();
	};

	//--- onDeactiveCountry ---//			
	var onDeactiveCountry = function($target){

		var country = $target.removeClass("on").attr("class");
		var $mapCountryItem = $map.find("." + country);
		var $linksCountryItem = $links.find("." + country);
		
		$mapCountryItem.removeClass("on");
		$linksCountryItem.removeClass("on");			
		setDeactiveImage($mapCountryItem.find("img"));
		$mapCountryItem.find(".attention").hide();				
	};
	
	//--- setActiveImage ---//
	var setActiveImage = function($target){

		if($target.length === 0) return;
		
		var onSrc = $target.attr("data-onSrc");
		if(!onSrc){
			var originSrc = $target.attr("src");
			var extention = originSrc.substring(originSrc.lastIndexOf('.'), originSrc.length);
			var onSrc = originSrc.replace(extention, '_on'+extention);
			
			$target.attr("data-src", originSrc);
			$target.attr("data-onSrc", onSrc);
		}
		
		$target.attr("src", onSrc);
	};

	//--- setDeactiveImage ---//	
	var setDeactiveImage = function($target){
		if($target.length === 0) return;
		
		var originSrc = $target.attr("data-src");
		$target.attr("src", originSrc);
	};	

	//--- Mouse hover event ---//		
	$("#link li, #map li").hover(
		function(){
			onActiveCountry($(this));
		}, 
		function(){
			onDeactiveCountry($(this));
		}
	);
});

(function(){

//--- Read CSS ---//	
var head = document.getElementsByTagName("head")[0];
var link = createElement("link", {href:"css/script.css",rel:"stylesheet",type:"text/css"});
head.appendChild(link);
 
function createElement(tagName, attributes){
    var o = document.createElement(tagName), i;
    attributes = attributes || {};
    for(i in attributes){
        o.setAttribute(i, attributes[i]);
    }
    return o;
}
})();