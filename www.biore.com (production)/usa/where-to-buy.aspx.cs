﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class where_to_buy : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
		((WebControl)Master.FindControl("gn5")).CssClass = "global_anchor active"; //Set nav class
        Page.Header.Title = "Bior&eacute;&reg;: Where To Buy";
    }
}
