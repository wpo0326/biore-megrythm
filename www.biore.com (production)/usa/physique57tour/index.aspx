<%@ Page Language="C#" MasterPageFile="~/MasterPages/Physique.master" AutoEventWireup="true"
    CodeFile="index.aspx.cs" Inherits="physique_57_index" Debug="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta name="Description" content="Find out when the Biore Skincare & Physique 57 Tour is coming to your neighborhood." />
    <meta name="Keywords" content="Biore, Physique 57, fitness tour,post-workout breakout, Makeup Removing Towelettes" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <div id="theater">
          <h1>The Bior&eacute;&reg; Skincare & Physique 57&trade; Tour</h1>
          <h3>A Great Workout Without the Breakout</h3>
          <p>Bior&eacute;&reg; and Physique 57&trade; are teaming up to bring you a new way to take care of your skin while transforming your body.  Watch for the tour as we'll be coming to a city near you with a team of Physique 57&trade; instructors and the cleansing power of Bior&eacute;&reg; Makeup Removing Towelettes.</p>        
        </div>
        <div id="signup"> 
          <h2>Sign Up Now!</h2>
          <h3>Take advantage of our 10-city tour.</h3>
          <p>Sign up to experience a sculpting and strengthening workout, learn how to reduce your chances of post-workout blemishes, and receive a free sample of Bior&eacute;&reg; Makeup Removing Towelettes to use. <strong>All sessions are absolutely free!</strong><br />
          <em>Limited reserved space available. Sign up available for 1 class only with this offer.</em> </p> 
          <a href="scheduleClass.aspx" id="btn">Schedule a Class</a>
        </div>
        <div id="teaser">
          <a href="murt.aspx" id="murt">Bior&eacute;&reg; Makeup Removing Towelettes Instantly dissolve dirt, oil, and makeup before you start your workout. Find out more &gt;&gt;</a>
        </div>
        <div class="clear"></div>  
        <div id="homeAbbrevRules"><p>NO PURCHASE NECESSARY. Open to legal residents of the 50 United States (D.C.) 18 years or older. Ends October 21, 2010.  Void where prohibited. For Official Rules, including odds, and prize descriptions <a href="javascript:openRules()">click here</a>.</p></div>
</asp:Content>