<%@ Language="C#" MasterPageFile="~/MasterPages/Physique.master" AutoEventWireup="true"
    CodeFile="scheduleClass.aspx.cs" Inherits="physique57tour_scheduleClass" %>  

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server"> 
    <meta name="Description" content="Sign-up to join the Biore Skincare & Physique 57 Tour. Experience a Physique 57 class and receive free samples of Biore products." />
    <meta name="Keywords" content="Biore, Physique 57, fitness tour, schedule" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="top">
      <h1>The Bior&eacute;&reg; Skincare &amp; Physique 57&trade; Tour Schedule</h1>
      <p>Come experience Physique 57&trade; and the makeup-removing power of Bior&eacute;&reg;'s Makeup Removing Towelettes. <strong>All promotional classes are free.</strong></p> 
      <a href="about.aspx">Learn More About Physique 57&trade; &gt;&gt;</a>   
    </div>
    <div id="scheduleTable">
      <table>
        <tbody>
          <tr runat="server" id="NY">
            <td width="168" class="date"><asp:Literal ID="Market1Date" runat="server"></asp:Literal></td>
            <td width="150"><asp:Literal ID="Market1Location" runat="server"></asp:Literal></td>
            <td class="detailsLinkTD"><a href="scheduleClass.aspx?mktId=1" id="city1" class="detailsLink"><asp:Literal ID="litCity1Text" runat="server"></asp:Literal></a></td>
          </tr>
          <tr runat="server" id="IL">
            <td class="date"><asp:Literal ID="Market2Date" runat="server"></asp:Literal></td>
            <td><asp:Literal ID="Market2Location" runat="server"></asp:Literal></td>
            <td class="detailsLinkTD"><a href="scheduleClass.aspx?mktId=2" id="city2" class="detailsLink"><asp:Literal ID="litCity2Text" runat="server"></asp:Literal></a></td>
          </tr>
          <tr runat="server" id="CA">
            <td class="date"><asp:Literal ID="Market3Date" runat="server"></asp:Literal></td>
            <td><asp:Literal ID="Market3Location" runat="server"></asp:Literal></td>
            <td class="detailsLinkTD"><a href="scheduleClass.aspx?mktId=3" id="city3" class="detailsLink"><asp:Literal ID="litCity3Text" runat="server"></asp:Literal></a></td>
          </tr>
          <tr runat="server" id="MI">
            <td class="date"><asp:Literal ID="Market4Date" runat="server"></asp:Literal></td>
            <td><asp:Literal ID="Market4Location" runat="server"></asp:Literal></td>
            <td class="detailsLinkTD"><a href="scheduleClass.aspx?mktId=4" id="city4" class="detailsLink"><asp:Literal ID="litCity4Text" runat="server"></asp:Literal></a></td>
          </tr>
          <tr runat="server" id="FL">
            <td class="date"><asp:Literal ID="Market5Date" runat="server"></asp:Literal></td>
            <td><asp:Literal ID="Market5Location" runat="server"></asp:Literal></td>
            <td class="detailsLinkTD"><a href="scheduleClass.aspx?mktId=5" id="city5" class="detailsLink"><asp:Literal ID="litCity5Text" runat="server"></asp:Literal></a></td>
          </tr>
          <tr runat="server" id="TX">
            <td class="date"><asp:Literal ID="Market6Date" runat="server"></asp:Literal></td>
            <td><asp:Literal ID="Market6Location" runat="server"></asp:Literal></td>
            <td class="detailsLinkTD"><a href="scheduleClass.aspx?mktId=6" id="city6" class="detailsLink"><asp:Literal ID="litCity6Text" runat="server"></asp:Literal></a></td>
          </tr>
          <tr runat="server" id="WA">
            <td class="date"><asp:Literal ID="Market7Date" runat="server"></asp:Literal></td>
            <td><asp:Literal ID="Market7Location" runat="server"></asp:Literal></td>
            <td class="detailsLinkTD"><a href="scheduleClass.aspx?mktId=7" id="city7" class="detailsLink"><asp:Literal ID="litCity7Text" runat="server"></asp:Literal></a></td>
          </tr>
          <tr runat="server" id="MA">
            <td class="date"><asp:Literal ID="Market8Date" runat="server"></asp:Literal></td>
            <td><asp:Literal ID="Market8Location" runat="server"></asp:Literal></td>
            <td class="detailsLinkTD"><a href="scheduleClass.aspx?mktId=8" id="city8" class="detailsLink"><asp:Literal ID="litCity8Text" runat="server"></asp:Literal></a></td>
          </tr>
          <tr runat="server" id="DC">
            <td class="date"><asp:Literal ID="Market9Date" runat="server"></asp:Literal></td>
            <td><asp:Literal ID="Market9Location" runat="server"></asp:Literal></td>
            <td class="detailsLinkTD"><a href="scheduleClass.aspx?mktId=9" id="city9" class="detailsLink"><asp:Literal ID="litCity9Text" runat="server"></asp:Literal></a></td>
          </tr>
          <tr runat="server" id="PA">
            <td class="date"><asp:Literal ID="Market10Date" runat="server"></asp:Literal></td>
            <td><asp:Literal ID="Market10Location" runat="server"></asp:Literal></td>
            <td class="detailsLinkTD"><a href="scheduleClass.aspx?mktId=10" id="city10" class="detailsLink"><asp:Literal ID="litCity10Text" runat="server"></asp:Literal></a></td>
          </tr>
        </tbody>
      </table>    
    </div>
    <div id="detailsContainer">
      <div id="details">
        <asp:Panel ID="Market" runat="server">
            <div id="venueDetails">
              <div id="marketName"><asp:Literal ID="MarketName" runat="server"></asp:Literal></div>
              <div id="venueName"><asp:Literal ID="VenueName" runat="server"></asp:Literal></div>
              <div id="venueAddress"><asp:Literal ID="VenueAddress" runat="server"></asp:Literal></div>
              <div id="venueCSZ"><asp:Literal ID="VenueCSZ" runat="server"></asp:Literal></div>
            </div>
            <div id="venueLink"><asp:HyperLink ID="VenueLink" runat="server" Target="_blank">MAP LOCATION &gt;&gt;</asp:HyperLink>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:openWaiver();">READ AND PRINT WAIVER &gt;&gt;</a></div>
            <asp:Panel ID="Class1" runat="server">
                <div id="class1Day"><span>Day: </span><asp:Literal ID="Class1Day" runat="server"></asp:Literal></div>
                <div id="class1Date"><span>Date: </span><asp:Literal ID="Class1Date" runat="server"></asp:Literal></div>
                <div id="class1Time"><span>Time: </span><asp:Literal ID="Class1Time" runat="server"></asp:Literal></div>
                <div id="class1Spots"><span>Spots Available: </span><asp:Literal runat="server" ID="Class1Spots"></asp:Literal></div>
                <asp:Panel ID="Class1RegLink" runat="server">
                  <div class="registerLink"><a href="#" id="registerLink1" runat="server">Register Now &gt;&gt;</a></div>
                </asp:Panel>
                <asp:Panel ID="Class1Full" runat="server">
                  <div class="classFull">Class is Full</div>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="Class2" runat="server">
                <div id="class2Day"><span>Day: </span><asp:Literal ID="Class2Day" runat="server"></asp:Literal></div>
                <div id="class2Date"><span>Date: </span><asp:Literal ID="Class2Date" runat="server"></asp:Literal></div>
                <div id="class2Time"><span>Time: </span><asp:Literal ID="Class2Time" runat="server"></asp:Literal></div>
                <div id="class2Spots"><span>Spots Available: </span><asp:Literal runat="server" ID="Class2Spots"></asp:Literal></div>
                <asp:Panel ID="Class2RegLink" runat="server">
                  <div class="registerLink"><a href="#" id="registerLink2" runat="server">Register Now &gt;&gt;</a></div>
                </asp:Panel>
                <asp:Panel ID="Class2Full" runat="server">
                  <div class="classFull">Class is Full</div>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="Class3" runat="server">
                <div id="class3Day"><span>Day: </span><asp:Literal ID="Class3Day" runat="server"></asp:Literal></div>
                <div id="class3Date"><span>Date: </span><asp:Literal ID="Class3Date" runat="server"></asp:Literal></div>
                <div id="class3Time"><span>Time: </span><asp:Literal ID="Class3Time" runat="server"></asp:Literal></div>
                <div id="class3Spots"><span>Spots Available: </span><asp:Literal runat="server" ID="Class3Spots"></asp:Literal></div>                
                <asp:Panel ID="Class3RegLink" runat="server">
                  <div class="registerLink"><a href="#" id="registerLink3" runat="server">Register Now &gt;&gt;</a></div>
                </asp:Panel> 
                <asp:Panel ID="Class3Full" runat="server">
                  <div class="classFull">Class is Full</div>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
      </div>
    </div>
    <div class="clear"></div>
    <div id="btmTxtContainer">
      <div id="btmTxt">
        <span>To guarantee your spot, please register for the class.  Walk-ins will be welcome if space allows.</span><br /><br />Please arrive 10 minutes prior to the scheduled starting time of your class.<br /><br />Participants must sign a waiver prior to taking the class. Minors must be accompanied by a parent or guardian.
        <div>NO PURCHASE NECESSARY. Open to legal residents of the 50 United States (D.C.) 18 years or older. Ends October 21, 2010.  Void where prohibited. For Official Rules, including odds, and prize descriptions <a href="javascript:openRules()">click here</a>.</div>
      </div>
    </div>
</asp:Content>