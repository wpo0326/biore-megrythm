<%@ Page Language="C#" AutoEventWireup="true" CodeFile="waiver.aspx.cs" Inherits="physique_57_about" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Biore Skincare & Physique 57 Class Waiver</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="Description" content="Read the waiver for the Biore Skincare & Physique 57 Class." />
    <meta name="Keywords" content="Biore, Physique 57, fitness tour, waiver" />
    <meta name="DCSext.submit" content="true" runat="server" visible="false" id="metaSubmit" />
    <link rel="stylesheet" href="/usa/css/p57.css" />
</head>
<body id="rules_body">
    <form id="form1" runat="server">
    <div id="waiver">
        <!--<h1>
            Release and Indemnity <a href="javascript:this.close()" id="close">Close</a>
        </h1>-->
        <br />
        <div>
            <h2>
                <em>This document contains a waiver of your rights.<br />
                    Read the document before signing it.</em>
            </h2>
            <br />
            <p>
                I, on my behalf and on behalf of the other members of my family, heirs, successors
                and assigns, including but not limited to my spouse, parents and children, hereby
                grant to the Companies this full release and indemnification as consideration in
                exchange for permitting me to participate in exercise classes.</p>
            <br />
            <p>
                <span>Age</span><br />
                I confirm that I am of the age of majority in my state of residence and in the state
                in which I will be participating in the Physique 57 exercise class (the "Class")
                offered by Kao Brands Company and Jevo Inc. (the "Companies"). If not, this document
                is executed below by my parent or legal guardian.</p>
            <br />
            <p>
                <span>Personal Possessions / Personal Injury</span><br />
                I agree that the Companies are in no way responsible for the safekeeping of my personal
                belongings while I attend Class. I am voluntarily participating in the Class with
                full knowledge, understanding and appreciation of the risks inherent in any physical
                exercise and I expressly assume all risks of injury, and even death, which could
                occur by reason of my participation. I release the Company from any liability and
                agree not to sue the Companies with respect to any cause of action for bodily injury,
                property damage, or death occurring as a result of participating in exercise classes.
                I hereby assume full responsibility for risks of bodily injury, property damage
                or death resulting from my participation in the Class, no matter how arising (including,
                but not limited to, the negligence of either or both of the Companies), and in any
                event to the fullest extent allowable by law. I agree to indemnify, defend, and
                hold harmless, the Companies, at my sole cost from any and all claims arising out
                of my participation in the Class.</p>
            <br />
            <p>
                <span>Publication Release</span><br />
                I agree that participation in the Class constitutes permission for Kao Brands Company,
                its agents and representatives to use my name, address, voice, photograph and video
                image, likeness and statements about the Class experience and/or Kao Brands Company
                products for advertising and publicity purposes without further compensation to
                the fullest extent permitted by applicable law.</p>
            <p>
                <br />
                I expressly agree that the terms of release and the indemnity contained herein are
                governed by the laws of Delaware and are intended to be as broad and inclusive as
                is permitted by the laws of Delaware. Any provision of this Release found to be
                invalid by the courts having jurisdiction shall be invalid only with respect to
                such provision or portion.</p>
            <p>
                <br />
                I HAVE READ AND VOLUNTARILY SIGNED THIS RELEASE AND INDEMNITY AGREEMENT.</p>
            <br />
            <br />
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td bgcolor="#5c5c5d" height="1" width="50%">
                    </td>
                    <td bgcolor="#5c5c5d" height="1" width="50%">
                    </td>
                </tr>
                <tr>
                    <td>
                        Signature<br />
                        <br />
                        <br />
                    </td>
                    <td>
                        Full Name Printed<br />
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#5c5c5d" height="1" width="50%">
                    </td>
                    <td bgcolor="#5c5c5d" height="1" width="50%">
                    </td>
                </tr>
                <tr>
                    <td>
                        Date Signed<br />
                        <br />
                        <br />
                    </td>
                    <td>
                        Age on date of Class<br />
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#5c5c5d" height="1" width="50%">
                    </td>
                    <td bgcolor="#5c5c5d" height="1" width="50%">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Address (Street, City, State, Zip)<br />
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#5c5c5d" height="1" width="50%">
                    </td>
                    <td bgcolor="#5c5c5d" height="1" width="50%">
                    </td>
                </tr>
                <tr>
                    <td>
                        Phone (day)
                    </td>
                    <td>
                        Phone (evening)
                    </td>
                </tr>
            </table>
            <br />
            <br />
            PARENT OR GUARDIAN CONSENT FOR PARTICIPANTS <strong style="text-decoration: underline;">
                UNDER AGE OF MAJORITY:</strong><br />
            <br />
            I am the Mother/Father/Legal Guardian (circle one) of the minor who signed the foregoing
            release. The release is entered into with my agreement and consent, without further
            compensation.<br />
            <br />
            <br />
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td bgcolor="#5c5c5d" height="1" width="33%">
                    </td>
                    <td bgcolor="#5c5c5d" height="1" width="33%">
                    </td>
                    <td bgcolor="#5c5c5d" height="1" width="33%">
                    </td>
                </tr>
                <tr>
                    <td>
                        Signature
                    </td>
                    <td>
                        Full Name Printed
                    </td>
                    <td>
                        Date Signed
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </div>
    </div>
    </form>
    <!-- START OF SmartSource Data Collector TAG -->

    <script src="/usa/js/webtrends.js" type="text/javascript"></script>

    <script type="text/javascript">
        //<![CDATA[
        var _tag = new WebTrends();
        _tag.dcsGetId();
        //]]>>
    </script>

    <script type="text/javascript">
        //<![CDATA[
        // Add custom parameters here.
        //_tag.DCSext.param_name=param_value;
        _tag.dcsCollect();
        //]]>>
    </script>

    <noscript>
        <div>
            <img alt="DCSIMG" id="DCSIMG" width="1" height="1" src="http://sdc.enlighten.com/dcsx5r2z6x4568dp93c02vf9d_4y5t/njs.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=8.6.0" /></div>
    </noscript>
    <!-- END OF SmartSource Data Collector TAG -->

    <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>

    <script type="text/javascript">
        try {
            var firstTracker = _gat._getTracker("UA-385129-27"); // Global Tag
            firstTracker._setDomainName("none");
            firstTracker._trackPageview();
            var secondTracker = _gat._getTracker("UA-385129-4"); // Biore Tag
            secondTracker._setCampNameKey("enl_campaign")
            secondTracker._setCampMediumKey("enl_medium")
            secondTracker._setCampSourceKey("enl_source")
            secondTracker._trackPageview();
        } catch (err) { }
    </script>

</body>
</html>
