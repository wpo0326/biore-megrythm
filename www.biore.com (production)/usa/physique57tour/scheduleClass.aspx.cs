using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Text;

public partial class physique57tour_scheduleClass : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Header.Title = "Sign Up For A Class | Biore Skincare & Physique 57 Tour";
        (Page.Master.FindControl("Body") as HtmlControl).Attributes.Add("class", "class");
        Response.Status = "301 Moved Permanently";
        Response.AddHeader("Location","/usa/"); 

        int mktID = 1;

        if (Request.QueryString["mktID"] != null)
            mktID = Int32.Parse(Request.QueryString["mktID"].ToString());

        int classLimit = 0;
        physique ph = new physique();
        string[] classInfo = new string[8];
        classInfo = ph.getClassInfo(mktID, 1);
        MarketName.Text = classInfo[0];
        VenueName.Text = classInfo[1];
        VenueAddress.Text = classInfo[2];
        VenueCSZ.Text = classInfo[3];
        VenueLink.NavigateUrl = classInfo[4];
        Class1Day.Text = Class2Day.Text = Class3Day.Text = classInfo[5];
        Class1Date.Text = Class2Date.Text = Class3Date.Text = classInfo[6];
        
        Class1Time.Text = classInfo[7];
        classLimit = Int32.Parse(classInfo[8]);
        Class1Spots.Text = (classLimit - ph.getPhysiqueSeats(mktID, 1)).ToString();
        if (Int32.Parse(Class1Spots.Text) <= 0) { Class1RegLink.Visible = false; }
        else { Class1Full.Visible = false; registerLink1.HRef = "register.aspx?market=" + mktID.ToString() + "&class=1"; }
                
        classInfo = ph.getClassInfo(mktID, 2);
        if (classInfo[7] != "")
        {
            Class2Time.Text = classInfo[7];
            classLimit = Int32.Parse(classInfo[8]);
            Class2Spots.Text = (classLimit - ph.getPhysiqueSeats(mktID, 2)).ToString();
            if (Int32.Parse(Class2Spots.Text) <= 0) { Class2RegLink.Visible = false; }
            else { Class2Full.Visible = false; registerLink2.HRef = "register.aspx?market=" + mktID.ToString() + "&class=2"; }
        }
        else
            Class2.Visible = false;

        classInfo = ph.getClassInfo(mktID, 3);
        if (classInfo[7] != "")
        {
            Class3Time.Text = classInfo[7];
            classLimit = Int32.Parse(classInfo[8]);
            Class3Spots.Text = (classLimit - ph.getPhysiqueSeats(mktID, 3)).ToString();
            if (Int32.Parse(Class3Spots.Text) <= 0) { Class3RegLink.Visible = false; }
            else { Class3Full.Visible = false; registerLink3.HRef = "register.aspx?market=" + mktID.ToString() + "&class=3"; }
        }
        else
            Class3.Visible = false;


        setClassSpotsLink(mktID);
    }
    
    private void setClassSpotsLink(int marketID)
    {
        physique ph = new physique();
        string[] classInfo = new string[8];
        for (int i = 1; i <= 10; i++)
        {
            switch (i)
            {
                case 1:
                    if (ph.isMarketFull(i)) { litCity1Text.Text = "CLASSES FULL"; }
                    else { litCity1Text.Text = "CLASS DETAILS"; }
                    if (i == marketID) { NY.Attributes.Add("class", "selected"); }
                    classInfo = ph.getClassInfo(i, 1);
                    Market1Date.Text = classInfo[6];
                    Market1Location.Text = classInfo[0];
                    break;
                case 2:
                    if (ph.isMarketFull(i)) { litCity2Text.Text = "CLASSES FULL"; }
                    else { litCity2Text.Text = "CLASS DETAILS"; }
                    if (i == marketID) { IL.Attributes.Add("class", "selected"); }
                    classInfo = ph.getClassInfo(i, 1);
                    Market2Date.Text = classInfo[6];
                    Market2Location.Text = classInfo[0];
                    break;
                case 3:
                    if (ph.isMarketFull(i)) { litCity3Text.Text = "CLASSES FULL"; }
                    else { litCity3Text.Text = "CLASS DETAILS"; }
                    if (i == marketID) { CA.Attributes.Add("class", "selected"); }
                    classInfo = ph.getClassInfo(i, 1);
                    Market3Date.Text = classInfo[6];
                    Market3Location.Text = classInfo[0];
                    break;
                case 4:
                    if (ph.isMarketFull(i)) { litCity4Text.Text = "CLASSES FULL"; }
                    else { litCity4Text.Text = "CLASS DETAILS"; }
                    if (i == marketID) { MI.Attributes.Add("class", "selected"); }
                    classInfo = ph.getClassInfo(i, 1);
                    Market4Date.Text = classInfo[6];
                    Market4Location.Text = classInfo[0];
                    break;
                case 5:
                    if (ph.isMarketFull(i)) { litCity5Text.Text = "CLASSES FULL"; }
                    else { litCity5Text.Text = "CLASS DETAILS"; }
                    if (i == marketID) { FL.Attributes.Add("class", "selected"); }
                    classInfo = ph.getClassInfo(i, 1);
                    Market5Date.Text = classInfo[6];
                    Market5Location.Text = classInfo[0];
                    break;
                case 6:
                    if (ph.isMarketFull(i)) { litCity6Text.Text = "CLASSES FULL"; }
                    else { litCity6Text.Text = "CLASS DETAILS"; }
                    if (i == marketID) { TX.Attributes.Add("class", "selected"); }
                    classInfo = ph.getClassInfo(i, 1);
                    Market6Date.Text = classInfo[6];
                    Market6Location.Text = classInfo[0];
                    break;
                case 7:
                    if (ph.isMarketFull(i)) { litCity7Text.Text = "CLASSES FULL"; }
                    else { litCity7Text.Text = "CLASS DETAILS"; }
                    if (i == marketID) { WA.Attributes.Add("class", "selected"); }
                    classInfo = ph.getClassInfo(i, 1);
                    Market7Date.Text = classInfo[6];
                    Market7Location.Text = classInfo[0];
                    break;
                case 8:
                    if (ph.isMarketFull(i)) { litCity8Text.Text = "CLASSES FULL"; }
                    else { litCity8Text.Text = "CLASS DETAILS"; }
                    if (i == marketID) { MA.Attributes.Add("class", "selected"); }
                    classInfo = ph.getClassInfo(i, 1);
                    Market8Date.Text = classInfo[6];
                    Market8Location.Text = classInfo[0];
                    break;
                case 9:
                    if (ph.isMarketFull(i)) { litCity9Text.Text = "CLASSES FULL"; }
                    else { litCity9Text.Text = "CLASS DETAILS"; }
                    if (i == marketID) { DC.Attributes.Add("class", "selected"); }
                    classInfo = ph.getClassInfo(i, 1);
                    Market9Date.Text = classInfo[6];
                    Market9Location.Text = classInfo[0];
                    break;
                case 10:
                    if (ph.isMarketFull(i)) { litCity10Text.Text = "CLASSES FULL"; }
                    else { litCity10Text.Text = "CLASS DETAILS"; }
                    if (i == marketID) { PA.Attributes.Add("class", "selected"); }
                    classInfo = ph.getClassInfo(i, 1);
                    Market10Date.Text = classInfo[6];
                    Market10Location.Text = classInfo[0];
                    break;
                default:
                    if (ph.isMarketFull(i)) { litCity1Text.Text = "CLASSES FULL"; }
                    else { litCity1Text.Text = "CLASS DETAILS"; }
                    break;
            }
        }
    }
}