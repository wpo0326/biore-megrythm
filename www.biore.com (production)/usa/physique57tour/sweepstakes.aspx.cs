using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Text;

public partial class physique_57_sweepstakes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Header.Title = "Biore Skincare & Physique 57 Sweepstakes";
        (Page.Master.FindControl("Body") as HtmlControl).Attributes.Add("class", "sweeps");
        Response.Status = "301 Moved Permanently";
        Response.AddHeader("Location","/usa/"); 

        Literal crmMetrixLiteral;
        crmMetrixLiteral = (Literal)Master.FindControl("literal_crmmetrix");
        if (crmMetrixLiteral != null)
        {
            crmMetrixLiteral.Visible = false;
        }

        if (!IsPostBack)
        {
            OptinFormSuccess.Visible = false;
            OptinFormFailure.Visible = false;
            DateTime dt = DateTime.Now;

            for (int i = dt.Year - 13; i > dt.Year - 100; i--)
            {
                ListItem li = new ListItem();
                li.Text = i.ToString();
                li.Value = i.ToString();
                yyyy.Items.Add(li);
                li = null;
            }
            for (int i = 1; i < 13; i++)
            {
                ListItem li = new ListItem();
                li.Text = i.ToString();
                li.Value = i.ToString();
                mm.Items.Add(li);
                li = null;
            }
            for (int i = 1; i < 32; i++)
            {
                ListItem li = new ListItem();
                li.Text = i.ToString();
                li.Value = i.ToString();
                dd.Items.Add(li);
                li = null;
            }
        }
        else
        {
            Page.Header.Title = "Biore Skincare & Physique 57 Sweepstakes Confirmation";
            metaSubmit.Visible = true;
            Page.Validate();
            if (Page.IsValid)
            {
                OptinForm.Visible = false;
                int validEntry = -1;
                string DOB = mm.SelectedValue + "/" + dd.SelectedValue + "/" + yyyy.SelectedValue;
                bioreContact bc = new bioreContact();
                int ban = 0, biore = 0, jergens = 0, jf = 0, curel = 0;
                if (banoptin.Checked)
                {
                    ban = 1;
                    metaBanOptin.Visible = true;
                }
                if (BioreOptin.Checked)
                {
                    biore = 1;
                    metaBioreOptin.Visible = true;
                }
                if (cureloptin.Checked)
                {
                    curel = 1;
                    metaCurelOptin.Visible = true;
                }
                if (jergensoptin.Checked)
                {
                    jergens = 1;
                    metaJergensOptin.Visible = true;
                }
                if (jfoptin.Checked)
                {
                    jf = 1;
                    metaJohnFriedaOptin.Visible = true;
                }

                string varProductsUsing = buildCheckboxString(ProductsUsing);
                string varTypesProductsUsed = buildCheckboxString(TypesProductsUsed);
                string varProductAttributes = buildCheckboxString(ProductAttributes);
                string varFaceProblems = buildCheckboxString(FaceProblems);

                //Remove the last "," from the strings
                varProductsUsing = varProductsUsing.Remove(varProductsUsing.Length - 1);
                varTypesProductsUsed = varTypesProductsUsed.Remove(varTypesProductsUsed.Length - 1);
                varProductAttributes = varProductAttributes.Remove(varProductAttributes.Length - 1);
                varFaceProblems = varFaceProblems.Remove(varFaceProblems.Length - 1);

                validEntry = bc.bioreOptinFullAdress(0, FName.Text, LName.Text, Email.Text, Address1.Text, Address2.Text, Address3.Text, City.Text, Int32.Parse(State.SelectedValue), PostalCode.Text, Int32.Parse(Gender.SelectedValue), DOB, Int32.Parse(ProdUsedDaily.SelectedValue), Int32.Parse(PayMoreAttention.SelectedValue), Int32.Parse(WillingToPayMore.SelectedValue), Int32.Parse(RecommendFriends.SelectedValue), varProductsUsing, Int32.Parse(MostUsedBrand.SelectedValue), varTypesProductsUsed, varProductAttributes, varFaceProblems, Int32.Parse(SkinType.SelectedValue), Int32.Parse(Race.SelectedValue), Int32.Parse(Spanish.SelectedValue), ban, curel, jergens, jf, biore, 400054, "biore_p57sweeps", "Biore and Physique 57 Sweepstakes entry");

                if (validEntry == 0)
                    OptinFormSuccess.Visible = true;
                else
                    OptinFormFailure.Visible = true;
            }
        }


    }

    protected void optinValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = (BioreOptin.Checked == true);
    }

    protected void DOBValidate(object source, ServerValidateEventArgs args)
    {
        string dob = mm.SelectedValue.ToString() + "/" + dd.SelectedValue.ToString() + "/" + yyyy.SelectedValue.ToString();
        try
        {
            DateTime.Parse(dob);
            args.IsValid = true;
        }
        catch
        {
            args.IsValid = false;
        }
    }

    protected void DOBUnderage(object source, ServerValidateEventArgs args)
    {
        string dob = mm.SelectedValue.ToString() + "/" + dd.SelectedValue.ToString() + "/" + yyyy.SelectedValue.ToString();
        bool invalid = false;
        DateTime dateValue;
        if (!DateTime.TryParse(dob, out dateValue))
            invalid = true;
        try
        {
            DateTime.Parse(dob);
            DateTime dt = new DateTime();
            dt = DateTime.Parse(dob);
            if (getAgeYearDifference(dt) < 18)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
        catch
        {
            args.IsValid = false;
        }
        if (invalid)
            args.IsValid = true;
    }

    private string buildCheckboxString(CheckBoxList cb)
    {
        StringBuilder sb = new StringBuilder();
        foreach (ListItem li in cb.Items)
        {
            if (li.Selected)
            {
                sb.Append(Int32.Parse(li.Value));
                sb.Append(",");
            }
        }
        return sb.ToString();
    }

    private int getAgeYearDifference(DateTime dt)
    {
        // get the difference in years
        int years = DateTime.Now.Year - dt.Year;
        // subtract another year if we're before the
        // birth day in the current year
        if (DateTime.Now.Month < dt.Month ||
            (DateTime.Now.Month == dt.Month &&
            DateTime.Now.Day < dt.Day))
            years--;
        return years;
    }
}