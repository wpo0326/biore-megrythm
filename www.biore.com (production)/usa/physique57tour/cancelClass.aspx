﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Physique.master" AutoEventWireup="true" CodeFile="cancelClass.aspx.cs" Inherits="physique57tour_cancelClass" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="Description" content="Sign-up to join the Biore Skincare & Physique 57 Experience a Physique 57 class and receive free samples of Biore products." />
    <meta name="Keywords" content="Biore, Physique 57, fitness tour, schedule" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="cancelBg">
		<asp:Panel ID="formSuccess" runat="server">
		    <h2 title="Your workout appointment has been cancelled."></h2>
			<p>We have received your workout appointment cancellation request. If you are interested in signing up for another workout appointment time, please return to the <a href="scheduleClass.aspx">Schedule page</a>.</p>
			<div id="rescheduleButton">
				<a href="scheduleClass.aspx" title="Schedule Another Appointment">Schedule Another Appointment</a>
			</div>
		</asp:Panel>
		<asp:Panel ID="formFailure" runat="server" Visible="false">
		    <h3>We're Sorry...</h3><br />
			<p>This class has already been cancelled or an incorrect link was used.</p>
			<div id="rescheduleButton">
				<a href="scheduleClass.aspx" title="Schedule Another Appointment">Schedule Another Appointment</a>
			</div>
		</asp:Panel>
	</div>
	<div id="shortRules">NO PURCHASE NECESSARY. Open to legal residents of the 50 United States (D.C.) 18 years or older. Ends October 21, 2010.  Void where prohibited. <br>For Official Rules, including odds, and prize descriptions <a href="javascript:openRules()">click here</a>.</div>
</asp:Content> 

