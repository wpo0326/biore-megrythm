<%@ Page Language="C#" MasterPageFile="~/MasterPages/Physique.master" AutoEventWireup="true"
    CodeFile="murt.aspx.cs" Inherits="physique_57_murt" Title="Untitled Page" Debug="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta name="Description" content="Biore Makeup Removing Towelettes can help reduce the chances of post-workout breakouts and blemishes." />
    <meta name="Keywords" content="Makeup Removing Towelettes, post-workout breakout" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <div id="theater">
          <h1>Bior&eacute;&reg; Makeup Removing Towelettes</h1>
          <h3>Worried about post-workout breakouts? Throw in the Bior&eacute;&reg; Towel(ettes).</h3>
          <p>Use Bior&eacute;&reg; Makeup Removing Towelettes to instantly remove dirt, oil and makeup before you work out to significantly reduce your chances of getting post-workout blemishes.</p> 
          <a href="http://www.drugstore.com/products/prod.asp?pid=240694&catid=168922&trx=PLST-0-CAT&trxp1=168922&trxp2=240694&trxp3=1&trxp4=0&btrx=BUY-PLST-0-CAT" target="_blank" id="btn">Buy Now</a>
          <a href="http://www.biore.com/usa/products/product_detail.aspx?pid=2" id="murtProdImg">Bior&eacute;&reg; Makeup Removing Towelettes</a>       
        </div>    
        <div id="rules">NO PURCHASE NECESSARY. Open to legal residents of the 50 United States (D.C.) 18 years or older. Ends October 21, 2010.  Void where prohibited. For Official Rules, including odds, and prize descriptions <a href="javascript:openRules()">click here</a>.</div>
</asp:Content>