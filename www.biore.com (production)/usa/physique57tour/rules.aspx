<%@ Page Language="C#" AutoEventWireup="true" CodeFile="rules.aspx.cs" Inherits="physique_57_about" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Biore  Skincare & Physique 57 Sweepstakes Official Rules</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="Description" content="Read the complete rules and details for the Biore Skincare & Physique 57 Sweepstakes." />
    <meta name="Keywords" content="Biore Skincare & Physique 57 Sweepstakes, rules, complete rules" />
    <meta name="DCSext.submit" content="true" runat="server" visible="false" id="metaSubmit" />
    <link rel="stylesheet" href="/usa/css/p57.css" />
</head>
<body id="rules_body">
    <form id="form1" runat="server">
    <div id="official_rules">
        <h1 title="The 'Bior&eacute;&reg; Skincare & Physique 57&trade; Tour' Sweepstakes Official Rules">
        </h1>
        <div>
            <h2>
                NO PURCHASE NECESSARY TO ENTER OR WIN. A PURCHASE DOES NOT IMPROVE YOUR CHANCES
                OF WINNING.
            </h2><br />
            <p>
                Sweepstakes may only be entered in or from the 50 United States and the District
                of Columbia and entries originating from any other jurisdiction are not eligible
                for entry. This Sweepstakes is governed exclusively by the laws of the United States.
                You are not authorized to participate in the Sweepstakes if you are not located
                within the 50 United States or the District of Columbia.</p><br /><br />
            <ol>
                <li><strong>How to Enter.</strong> Log onto <a href="/usa/physique57tour" target="_blank">
                    http://www.biore.com/usa/physique57tour</a> and follow the directions to enter between
                    12:00:00 PM ET on August 23, 2010 and 12:00:00 PM ET on October 21, 2010. You must
                    include on the online entry form your first and last name, email address, mailing
                    address, gender, birth date and stated marketing questions. All online entrants
                    must have a valid email address.
                    <br />
                    <br />
                    Limit one (1) entry per person/email address. No automated entry devices and/or
                    programs permitted. All entries become the sole and exclusive property of the Sponsor
                    and receipt of entries will not be acknowledged or returned. Delivery of prizes
                    requires a street address (no P.O. Boxes). Sponsor is not responsible for lost,
                    late, illegible, stolen, incomplete, invalid, unintelligible, misdirected, technically
                    corrupted or garbled entries or mail, which will be disqualified, or for problems
                    of any kind whether mechanical, human or electronic. Only fully completed entry
                    forms are eligible. Proof of submission will not be deemed to be proof of receipt
                    by Sponsor.<br />
                    <br />
                </li>
                <li><strong>Start/End Dates.</strong> Sweepstakes begins at 12:00:00 PM ET on August
                    23, 2010 and ends at 12:00:00 PM ET on October 21, 2010.<br />
                    <br />
                </li>
                <li><strong>Eligibility.</strong> Participation open only to legal residents of the
                    fifty United States or the District of Columbia who are 18 or older as of date of
                    entry and who have Internet. Void outside of the 50 United States and the District
                    of Columbia, and where prohibited, taxed or restricted by law. Employees, officers
                    and directors of Kao Brands Company ("Sponsor"), Physique 57 _______, Enlighten,
                    Inc. ("Enlighten"), Zoom Media & Marketing Trachtenberg & Co. and their parent companies,
                    subsidiaries, affiliates, partners, advertising and promotion agencies, manufacturers
                    or distributors of sweepstakes materials and their immediate families (parents,
                    children, siblings, spouse) or members of the same household (whether related or
                    not) of such employees/officers/directors are not eligible to enter. Sweepstakes
                    may only be entered in or from the 50 United States and the District of Columbia,
                    and entries originating from any other jurisdiction are not eligible for entry.
                    You are not authorized to participate in the Sweepstakes if you are not located
                    within the 50 United States or the District of Columbia. All federal, state and
                    local laws and regulations apply.<br />
                    <br />
                </li>
                <li><strong>Random Drawing/Odds.</strong> Winners will be selected in a random drawing
                    from all eligible entries received on or about November 1, 2010. Odds of winning
                    depend on the number of eligible entries received for the drawing. Drawing will
                    be conducted by Enlighten on or about November 1, 2010. By entering the Sweepstakes,
                    entrants fully and unconditionally agree to be bound by these rules and the decisions
                    of the judges, which will be final and binding in all matters relating to the Sweepstakes.<br />
                    <br />
                </li>
                <li><strong>Prizes.</strong> Two hundred fifty (250) Prizes: Each winner will receive
                    a Workout Essentials Kit, which includes (i) a Physique 57&trade; DVD, (ii) Bior&eacute;&reg;
                    branded yoga mat and exercise ball, and (iii) one (1) package of Bior&eacute;&reg;
                    Make-up Removing Towelettes. Limit one prize per person, per household. ARV of each
                    Prize $75.00. Total approximate retail value of all prizes combined: $18,750.00.
                    Prizes are non-transferable. No substitutions or cash redemptions. In the case of
                    unavailability of any prize, Sponsor reserves the right to substitute a prize of
                    equal or greater value. All unspecified expenses are the responsibility of winners.<br />
                    <br />
                </li>
                <li><strong>Notification.</strong> Winners will be notified by email on or about November
                    1, 2010, and may be required to sign and return, where legal, an Affidavit of Eligibility
                    and Liability/Publicity Release within fourteen (14) days of prize notification.
                    If any winner is considered a minor in his/her jurisdiction of residence, Liability/Publicity
                    Release must be signed by his/her parent or legal guardian and such prize will be
                    delivered to minor's parent/legal guardian and awarded in the name of parent/legal
                    guardian. If any winner cannot be contacted within five (5) calendar days of first
                    notification attempt, if any prize or prize notification is returned as undeliverable,
                    if any winner rejects his/her prize or in the event of noncompliance with these
                    Sweepstakes rules and requirements, such prize will be forfeited and an alternate
                    winner will be selected from all remaining eligible entries. Upon prize forfeiture,
                    no compensation will be given. Limit one prize per person or household.<br />
                    <br />
                </li>
                <li><strong>Conditions.</strong> All federal, state and local taxes are the sole responsibility
                    of the winners. Participation in Sweepstakes and acceptance of prize constitutes
                    each winner's permission for Sponsor to use his/her name, address (city and state),
                    likeness, photograph, picture, portrait, voice, biographical information and/or
                    any statements made by each winner regarding the Sweepstakes or Sponsor for advertising
                    and promotional purposes without notice or additional compensation, except where
                    prohibited by law. By participating, entrants, and winners agree to release and
                    hold harmless Sponsor, its advertising and promotion agencies and their respective
                    parent companies, subsidiaries, affiliates, partners, representatives, agents, successors,
                    assigns, employees, officers and directors, from any and all liability, for loss,
                    harm, damage, injury, cost or expense whatsoever including without limitation, property
                    damage, personal injury and/or death which may occur in connection with, preparation
                    for, travel to, or participation in Sweepstakes, or possession, acceptance and/or
                    use or misuse of prize or participation in any Sweepstakes-related activity and
                    for any claims based on publicity rights, defamation or invasion of privacy and
                    merchandise delivery. Entrants who do not comply with these Official Rules, or attempt
                    to interfere with this Sweepstakes in any way shall be disqualified. There is no
                    purchase or sales presentation required to participate. A purchase does not increase
                    odds of winning.<br />
                    <br />
                </li>
                <li><strong>Additional Terms.</strong> In case of dispute as to the identity of any
                    entrant, entry will be declared made by the authorized account holder of the email
                    address submitted at time of entry. "Authorized Account Holder" is defined as the
                    natural person who is assigned an email address by an Internet access provider,
                    online service provider, or other organization (e.g., business, educational, institution,
                    etc.) responsible for assigning email addresses or the domain associated with the
                    submitted email address. Any potential winner may be requested to provide Sponsor
                    with proof that such winner is the authorized account holder of the email address
                    associated with the winning entry. Any other attempted form of entry is prohibited;
                    no automatic, programmed; robotic or similar means of entry are permitted. Sponsor,
                    its affiliates, partners and promotion and advertising agencies are not responsible
                    for technical, hardware, software, telephone or other communications malfunctions,
                    errors or failures of any kind, lost or unavailable network connections, web site,
                    Internet, or ISP availability, unauthorized human intervention, traffic congestion,
                    incomplete or inaccurate capture of entry information (regardless of cause) or failed,
                    incomplete, garbled, jumbled or delayed computer transmissions which may limit one's
                    ability to enter the Sweepstakes, including any injury or damage to participant's
                    or any other person's computer relating to or resulting from participating in this
                    Sweepstakes or downloading any materials in this Sweepstakes. Sponsor reserves the
                    right, in its sole discretion, to cancel, terminate, modify, extend or suspend this
                    Sweepstakes should (in its sole discretion) virus, bugs, non-authorized human intervention,
                    fraud or other causes beyond its control corrupt or affect the administration, security,
                    fairness or proper conduct of the Sweepstakes. In such case, Sponsor may select
                    the winner(s) from all eligible entries received prior to and/or after (if appropriate)
                    the action taken by Sponsor. Sponsor reserves the right, at its sole discretion,
                    to disqualify any individual it finds, in its sole discretion, to be tampering with
                    the entry process or the operation of the Sweepstakes or web site. Sponsor may prohibit
                    an entrant from participating in the Sweepstakes or winning a prize if, in its sole
                    discretion, it determines that said entrant is attempting to undermine the legitimate
                    operation of the Sweepstakes by cheating, hacking, deception, or other unfair playing
                    practices (including the use of automated quick entry programs) or intending to
                    annoy, abuse, threaten or harass any other entrants or Sponsor representatives.<br />
                    <br />
                    <em>CAUTION: ANY ATTEMPT BY AN ENTRANT TO DELIBERATELY DAMAGE ANY WEB SITE OR UNDERMINE
                        THE LEGITIMATE OPERATION OF THE SWEEPSTAKES MAY BE A VIOLATION OF CRIMINAL AND CIVIL
                        LAWS AND SHOULD SUCH AN ATTEMPT BE MADE, THE SPONSOR RESERVES THE RIGHT TO SEEK
                        DAMAGES FROM ANY SUCH PERSON TO THE FULLEST EXTENT PERMITTED BY LAW.</em><br />
                    <br />
                </li>
                <li><strong>Use of Data.</strong> Sponsor will be collecting personal data about entrants
                    online, in accordance with its privacy policy. Please review the Sponsor's privacy
                    policy at <a href="http://kaobrands.com/privacy_policy.asp" target="_blank">http://kaobrands.com/privacy_policy.asp</a>.
                    By participating in the Sweepstakes, entrants hereby agree to Sponsor's collection
                    and usage of their personal information and acknowledge that they have read and
                    accepted Sponsor's privacy policy.<br />
                    <br />
                </li>
                <li><strong>List of Winners.</strong> To obtain a list of winners, send a self-addressed,
                    stamped envelope by November 5, 2010 to: "Bior&eacute;&reg; Skincare & Physique
                    57&trade; Tour" Sweepstakes List of Winners Tractenberg & Co 116 East 16th Street,
                    2nd floor, NY, NY 10003.<br />
                    <br />
                </li>
                <li><strong>Sponsor.</strong> Kao Brands Company, 2535 Spring Grove Avenue, Cincinnati,
                    OH 45214-1773.</li>
            </ol>
            <br />
            Copyright &copy; 2010 Kao Brands Company. All rights reserved.
        </div>
    </div>
    </form>
    <!-- START OF SmartSource Data Collector TAG -->

    <script src="/usa/js/webtrends.js" type="text/javascript"></script>

    <script type="text/javascript">
        //<![CDATA[
        var _tag = new WebTrends();
        _tag.dcsGetId();
        //]]>>
    </script>

    <script type="text/javascript">
        //<![CDATA[
        // Add custom parameters here.
        //_tag.DCSext.param_name=param_value;
        _tag.dcsCollect();
        //]]>>
    </script>

    <noscript>
        <div>
            <img alt="DCSIMG" id="DCSIMG" width="1" height="1" src="http://sdc.enlighten.com/dcsx5r2z6x4568dp93c02vf9d_4y5t/njs.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=8.6.0" /></div>
    </noscript>
    <!-- END OF SmartSource Data Collector TAG -->

    <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>

    <script type="text/javascript">
        try {
            var firstTracker = _gat._getTracker("UA-385129-27"); // Global Tag
            firstTracker._setDomainName("none");
            firstTracker._trackPageview();
            var secondTracker = _gat._getTracker("UA-385129-4"); // Biore Tag
            secondTracker._setCampNameKey("enl_campaign")
            secondTracker._setCampMediumKey("enl_medium")
            secondTracker._setCampSourceKey("enl_source")
            secondTracker._trackPageview();
        } catch (err) { }
    </script>

</body>
</html>
