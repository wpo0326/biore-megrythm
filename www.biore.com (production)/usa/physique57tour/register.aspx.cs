using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class physique57tour_register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Status = "301 Moved Permanently";
        Response.AddHeader("Location","/usa/"); 
        
        string marketID = "";
        if (Request.QueryString["market"] != null) marketID = Request.QueryString["market"];
        string classID = "";
        if (Request.QueryString["class"] != null) classID = Request.QueryString["class"];

        int classLimit = 0;
        physique ph = new physique();
        string[] classInfo = new string[8];
        classInfo = ph.getClassInfo(Int32.Parse(marketID), Int32.Parse(classID));

        Page.Header.Title = "Schedule a Class | Biore Skincare & Physique 57 Tour";

        if (!IsPostBack)
        {
            MarketLocation.Text = classInfo[0];
            MarketVenue.Text = classInfo[1];
            MarketAddress.Text = classInfo[2];
            MarketCSZ.Text = classInfo[3];
            MarketDate.Text = classInfo[6];
            MarketDay.Text = classInfo[5];
            P57Time.Text = classInfo[7];

            if (marketID == "" || classID == "")
            {
                formInvalid.Visible = true;
                formEntry.Visible = false;
                formTop.Visible = false;
            }
        }
        else
        {
            metaSubmit.Visible = true;
            Page.Header.Title = "Biore Skincare & Physique 57 Sweepstakes Confirmation"; 
            Page.Validate();
            if (Page.IsValid)
            {
                ConfLocation.Text = classInfo[0];
                ConfVenue.Text = classInfo[1];
                ConfAddress.Text = classInfo[2];
                ConfCSZ.Text = classInfo[3];
                ConfDate.Text = classInfo[6];
                ConfDay.Text = classInfo[5];
                ConfTime.Text = classInfo[7];
                map.NavigateUrl = classInfo[4];

                classLimit = Int32.Parse(classInfo[8]);

                formEntry.Visible = false;
                formTop.Visible = false;
                int ID = -1;
                ID = ph.scheduleClass(Int32.Parse(marketID), Int32.Parse(classID), Email.Text, FName.Text, LName.Text, classLimit);

                if (ID == -1)
                    duplicateEntry.Visible = true;
                else if (ID == 0)
                    formError.Visible = true;
                else
                    formSuccess.Visible = true;
            }
        }
    }
}
