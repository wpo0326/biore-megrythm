using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class physique57tour_cancelClass : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {         
        Response.Status = "301 Moved Permanently";
        Response.AddHeader("Location","/usa/"); 
        
        Page.Header.Title = "Cancellation Confirmation | Biore  Skincare & Physique 57 Tour";
        int pID = -1;
        int success = -1;
        if (Request.QueryString["ID"] != null)
        {

            bool result = Int32.TryParse(Request.QueryString["ID"], out pID);
            if (!result)
            {
                formFailure.Visible = true;
                formSuccess.Visible = false;
            }
            else
            {
                physique ph = new physique();
                success = ph.cancelClass(pID);
                if (success != 0)
                {
                    formFailure.Visible = true;
                    formSuccess.Visible = false;
                }
            }
        }
    }
}
