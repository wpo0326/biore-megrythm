using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Text;

public partial class physique_57_index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Header.Title = "The Biore Skincare & Physique 57 Tour"; 
        (Page.Master.FindControl("Body") as HtmlControl).Attributes.Add("class", "home");
        Response.Status = "301 Moved Permanently";
        Response.AddHeader("Location","/usa/"); 
    }
}
