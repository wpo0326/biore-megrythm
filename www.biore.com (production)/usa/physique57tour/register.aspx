<%@ Page Language="C#" AutoEventWireup="true" CodeFile="register.aspx.cs" Inherits="physique57tour_register" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bior&eacute;.com</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="Description" content="Sign-up to join the Biore Skincare & Physique 57 Experience a Physique 57 class and receive free samples of Biore products." />
    <meta name="Keywords" content="Biore, Physique 57, fitness tour, schedule" />
    <meta name="DCSext.submit" content="true" runat="server" visible="false" id="metaSubmit" />
    <link rel="stylesheet" href="/usa/css/p57.css" />
    <style type="text/css">
        body
        {
            background: none;
        }
        form#form1
        {
            margin: 0px auto;
            width: 600px;
        }
        form#form1 h2
        {
            background: url('../images/p57/regbox_title.jpg') no-repeat;
            margin: 20px 0px 0px 23px;
            text-indent: -9999px;
            width: 554px;
            height: 18px;
        }
        form#form1 #classDetails
        {
            color: #4a544a;
            font-size: 14px;
            line-height: 18px;
            margin-left: -260px;
            position: absolute;
            top: 50px;
            left: 50%;
            width: 150px;
        }
        form#form1 #classDetails span
        {
            color: #6e6e6e;
        }
        form#form1 #venueDetails
        {
            margin-left: 353px;
            margin-bottom: 30px;
            padding-top: 6px;
            width: 190px;
        }
        form#form1 #required
        {
            color: #3b9bba;
            font-size: 12px;
            font-style: italic;
            margin: 0px 0px 12px 140px;
        }
        form#form1 label
        {
            color: #6e6e6e;
            float: left;
            font-size: 12px;
        }
        form#form1 input.textbox
        {
            background: url('../images/p57/regbox_inputBG.jpg') repeat-x transparent;
            border: 0;
            margin: 0px 0px 15px;
            padding: 0px 2px;
            width: 315px;
            height: 18px;
        }
        form#form1 input#Submit
        {
            background: url('../images/p57/regbox_submit.jpg');
            border: 0;
            cursor: pointer;
            margin-left: 192px;
            width: 84px;
            height: 30px;
            text-indent: -9999px;
            position: absolute;
            top: 455px;
            left: 50%;            
            /*The following 2 lines make ie handle the text-indent on the submit button properly*/            
            color: transparent;
            text-transform: capitalize;
        }
        form#form1 input#Submit:hover
        {
            background-position: left bottom;
        }
        form#form1 a#submit
        {
            background: url('../images/p57/regbox_submit.jpg');
            display: none;
            width: 84px;
            height: 30px;
            margin-left: 192px;
            text-indent: -9999px;
            position: absolute;
            top: 455px;
            left: 50%;
        }
        form#form1 a#submit:hover
        {
            background-position: left bottom;
        }
        form#form1 a#cancel
        {
            background: url('../images/p57/regbox_cancel.jpg');
            width: 84px;
            height: 30px;
            margin-left: 99px;
            text-indent: -9999px;
            position: absolute;
            top: 455px;
            left: 50%;
        }
        form#form1 a#cancel:hover
        {
            background-position: left bottom;
        }
        form#form1 #formEntry
        {
            margin: 0px auto;
            width: 319px;
        }
        form#form1 .errormsg
        {
            float: left;
            font-size: 11px;
            padding-left: 5px;
        }
        form#form1 .responseMsg
        {
            margin: 0px 0px 30px;
        }
        #formSuccess h2
        {
            background: url('../images/p57/regbox_confirm_title.jpg') no-repeat;
            margin: 20px 0px 0px 23px;
            text-indent: -9999px;
            width: 554px;
            height: 18px;
        }
        #formError h2, #duplicateEntry h2, #formInvalid h2
        {
            background: url('../images/p57/regbox_error_title.jpg') no-repeat;
            margin: 20px 0px 0px 23px;
            text-indent: -9999px;
            width: 554px;
            height: 18px;
        }
        #formSuccess p, #formError p, #duplicateEntry p, #formInvalid p
        {
            color: #6e6e6e;
            font-size: 14px;
            line-height: 20px;
            margin: 20px auto;
            width: 470px;
        }
        #formError p a, #duplicateEntry p a, #formInvalid p a
        {
            color: #6e6e6e;
        }
        #formSuccess #yourVenueDetails
        {
            background: #cbd2d2;
            color: #ffffff;
            font-size: 14px;
            font-weight: bold;
            margin: 0px auto;
            padding: 5px 0px 0px 20px;
            width: 280px;
            height: 85px;
        }
        #formSuccess #yourVenueDetails span
        {
            font-size: 20px;
        }
        #formSuccess a#map
        {
            color: #d1347c;
            display: block;
            font-size: 11px;
            margin: 7px auto 0px 150px;
            text-decoration: none;
            width: 300px;
        }
        #formSuccess a#map:hover
        {
            color: #3FA5C6;
        }
        #formSuccess #yourClassDetails
        {
            border-top: 1px solid #b8c3c3;
            color: #4a544a;
            font-size: 14px;
            line-height: 18px;
            margin: 10px auto 0px;
            padding-top: 14px;
            width: 300px;
        }
        #formSuccess #yourClassDetails span
        {
            font-weight: normal;
        }
        #formSuccess #emailTxt
        {
            color: #3fa5c6;
            font-size: 12px;
            margin: 5px auto 30px;
            width: 300px;
        }
        #formSuccess #enterSweeps
        {
            border-top: 1px solid #b8c3c3;
            margin: 0px auto;
            padding-top: 6px;
            width: 450px;
        }
        #formSuccess #enterSweeps p
        {
            color: #6e6e6e;
            font-size: 12px;
            line-height: 18px;
            margin: 0px;
        }
        #formSuccess #enterSweeps a
        {
            color: #d1347c;
            font-size: 12px;
            line-height: 18px;
            text-decoration: none;
        }
        #formSuccess #enterSweeps a:hover
        {
            color: #3FA5C6;
        }
        #formSuccess a#done
        {
            background: url('../images/p57/regbox_done.jpg') left top;
            display: block;
            text-indent: -9999px;
            width: 84px;
            height: 30px;
            position: absolute;
            top: 450px;
            left: 50%;
            margin-left: 191px;
        }
        #formSuccess a#done:hover
        {
            background-position: left bottom;
        }
    </style>

    <script type="text/javascript" src="/usa/js/jquery-1.3.2.min.js"></script>

    <script src="/usa/js/jquery.colorbox-min.js" type="text/javascript"></script>

    <script type="text/javascript" src="/usa/js/p57.js"></script>

</head>
<body>
    <form id="form1" runat="server">
    <div id="formInnerContainer">
        <div id="formTop" runat="server">
            <h2>
                Register for your free workout session</h2>
            <div id="classDetails">
                <div id="classDay">
                    <strong>Day:</strong> <span>
                        <asp:Literal ID="MarketDay" runat="server"></asp:Literal></span></div>
                <div id="classDate">
                    <strong>Date:</strong> <span>
                        <asp:Literal ID="MarketDate" runat="server"></asp:Literal></span></div>
                <div id="classTime">
                    <strong>Time:</strong> <span>
                        <asp:Literal ID="P57Time" runat="server"></asp:Literal></span></div>
            </div>
            <div id="venueDetails">
                <div id="marketName">
                    <asp:Literal ID="MarketLocation" runat="server"></asp:Literal></div>
                <div id="venueName">
                    <asp:Literal ID="MarketVenue" runat="server"></asp:Literal></div>
                <div id="venueAddress">
                    <asp:Literal ID="MarketAddress" runat="server"></asp:Literal></div>
                <div id="venueCSZ">
                    <asp:Literal ID="MarketCSZ" runat="server"></asp:Literal></div>
            </div>
            <div id="required">
                *All fields are required</div>
        </div>
        <asp:Panel ID="formEntry" runat="server">
            <div>
                <asp:Label ID="FNameLbl" runat="server" Text="First Name*" AssociatedControlID="FName"></asp:Label>
                <asp:RequiredFieldValidator ID="FNameValidator1" runat="server" Display="Dynamic"
                    ErrorMessage="Please enter your First Name." ControlToValidate="FName" EnableClientScript="true"
                    SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="FNameValidator2" runat="server" Display="Dynamic"
                    ErrorMessage="The characters '>' and '<' are not permitted." ValidationExpression="^[^<>]+$"
                    ControlToValidate="FName" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RegularExpressionValidator><br />
                <asp:TextBox ID="FName" runat="server" MaxLength="50" CssClass="inputTextBox textbox"></asp:TextBox>
            </div>
            <div>
                <asp:Label ID="LNameLbl" runat="server" Text="Last Name*" AssociatedControlID="LName"></asp:Label>
                <asp:RequiredFieldValidator ID="LNameValidator1" runat="server" Display="Dynamic"
                    ErrorMessage="Please enter your Last Name." ControlToValidate="LName" EnableClientScript="true"
                    SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="LNameValidator2" runat="server" Display="Dynamic"
                    ErrorMessage="The characters '>' and '<' are not permitted." ValidationExpression="^[^<>]+$"
                    ControlToValidate="LName" SetFocusOnError="true" CssClass="errormsg"></asp:RegularExpressionValidator><br />
                <asp:TextBox ID="LName" runat="server" MaxLength="50" CssClass="inputTextBox textbox"></asp:TextBox>
            </div>
            <div>
                <asp:Label ID="EmailLbl" runat="server" Text="Email*" AssociatedControlID="Email"></asp:Label>
                <asp:RequiredFieldValidator ID="EmailValidator1" runat="server" Display="Dynamic"
                    ErrorMessage="Please enter your Email Address." ControlToValidate="Email" EnableClientScript="true"
                    SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="EmailValidator2" runat="server" ErrorMessage="Please enter a valid Email Address."
                    ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                    ControlToValidate="Email" Display="Dynamic" EnableClientScript="true" SetFocusOnError="true"
                    CssClass="errormsg"></asp:RegularExpressionValidator><br />
                <asp:TextBox ID="Email" CssClass="textbox" runat="server" MaxLength="150"></asp:TextBox>
            </div>
            <div>
                <asp:Label ID="Email2Lbl" runat="server" Text="Re-Type Email*" AssociatedControlID="Email2"></asp:Label>
                <br />
                <asp:TextBox ID="Email2" CssClass="textbox" runat="server" MaxLength="150"></asp:TextBox>
                <asp:RequiredFieldValidator ID="ReTypeRequired" ControlToValidate="Email2" runat="server"
                    SetFocusOnError="true" ErrorMessage="Please re-type your Email Address" CssClass="errormsg" EnableClientScript="true" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="EmailCompare" runat="server" ControlToCompare="Email"
                    ControlToValidate="Email2" ErrorMessage="Emails must match." CssClass="errormsg" EnableClientScript="true" Display="Dynamic"></asp:CompareValidator>
            </div>
            <div>
                <a href="scheduleClass.aspx" id="cancel">Cancel</a></div>
            <!--<div><a href="#" id="submit">Submit</a></div>-->
            <asp:Button ID="Submit" CausesValidation="true" Text="Submit" runat="server" />
        </asp:Panel>
        <asp:Panel ID="formSuccess" Visible="false" runat="server">
            <div id="success" class="responseMsg">
                <h2>
                    Registration Confirmation</h2>
                <p>
                    Thank you for signing up for the Bior&eacute;&reg; Skincare & Physique 57&trade;
                    Tour. We hope you'll enjoy the sculpting and strengthening workout, while learning
                    how to reduce your chances of getting post-workout blemishes!</p>
                <div id="yourVenueDetails">
                    <span>
                        <asp:Literal ID="ConfLocation" runat="server"></asp:Literal></span>
                    <div>
                        <asp:Literal ID="ConfVenue" runat="server"></asp:Literal></div>
                    <div>
                        <asp:Literal ID="ConfAddress" runat="server"></asp:Literal></div>
                    <div>
                        <asp:Literal ID="ConfCSZ" runat="server"></asp:Literal></div>
                </div>
                <div>
                    <asp:HyperLink ID="map" runat="server" Target="_blank">MAP LOCATION &gt;&gt;</asp:HyperLink></div>
                <div id="yourClassDetails">
                    <div id="Div2">
                        <strong>Day:</strong> <span>
                            <asp:Literal ID="ConfDay" runat="server"></asp:Literal></span></div>
                    <div id="Div3">
                        <strong>Date:</strong> <span>
                            <asp:Literal ID="ConfDate" runat="server"></asp:Literal></span></div>
                    <div id="Div4">
                        <strong>Time:</strong> <span>
                            <asp:Literal ID="ConfTime" runat="server"></asp:Literal></span></div>
                </div>
                <div id="emailTxt">
                    You will also receive a confirmation email with the above information.</div>
                <div id="enterSweeps">
                    <p>
                        Enter for your chance to win a prize pack including<br />
                        Bior&eacute;&reg; Makeup Removing Towelettes and other Physique 57&trade; prizes!</p>
                    <a href="sweepstakes.aspx" target="_blank">BIOR&Eacute;&reg; SKINCARE &amp; PHYSIQUE
                        57&trade; SWEEPSTAKES &gt;&gt;</a>
                </div>
                <div>
                    <a href="scheduleClass.aspx" id="done">Done</a></div>
            </div>
        </asp:Panel>
        <asp:Panel ID="formError" Visible="false" runat="server">
            <div id="error" class="responseMsg">
                <h2>
                    Registration</h2>
                <p>
                    We're sorry, the Bior&eacute;&reg; Skincare & Physique 57&trade; Tour class you
                    are trying to sign up for is already full.</p>
                <p>
                    Please close this window and try a different time.</p>
            </div>
        </asp:Panel>
        <asp:Panel ID="duplicateEntry" Visible="false" runat="server">
            <div id="dupEntry" class="responseMsg">
                <h2>
                    Registration</h2>
                <p>
                    We're sorry, the email address you entered has already been used to sign up for
                    a Bior&eacute;&reg; Skincare & Physique 57&trade; Tour class.</p>
            </div>
        </asp:Panel>
        <asp:Panel ID="formInvalid" Visible="false" runat="server">
            <div id="invalid" class="responseMsg">
                <h2>
                    Registration</h2>
                <p>
                    The Bior&eacute;&reg; Skincare & Physique 57&trade; Tour class you requested is
                    not valid.</p>
                <p>
                    Please close this window and try again.</p>
            </div>
        </asp:Panel>
    </div>
    </form>
    <!-- START OF SmartSource Data Collector TAG -->

    <script src="/usa/js/webtrends.js" type="text/javascript"></script>

    <script type="text/javascript">
        //<![CDATA[
        var _tag = new WebTrends();
        _tag.dcsGetId();
        //]]>>
    </script>

    <script type="text/javascript">
        //<![CDATA[
        // Add custom parameters here.
        //_tag.DCSext.param_name=param_value;
        _tag.dcsCollect();
        //]]>>
    </script>

    <noscript>
        <div>
            <img alt="DCSIMG" id="DCSIMG" width="1" height="1" src="http://sdc.enlighten.com/dcsx5r2z6x4568dp93c02vf9d_4y5t/njs.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=8.6.0" /></div>
    </noscript>
    <!-- END OF SmartSource Data Collector TAG -->

    <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>

    <script type="text/javascript">
        try {
            var firstTracker = _gat._getTracker("UA-385129-27"); // Global Tag
            firstTracker._setDomainName("none");
            firstTracker._trackPageview();
            var secondTracker = _gat._getTracker("UA-385129-4"); // Biore Tag
            secondTracker._setCampNameKey("enl_campaign")
            secondTracker._setCampMediumKey("enl_medium")
            secondTracker._setCampSourceKey("enl_source")
            secondTracker._trackPageview();
        } catch (err) { }
    </script>

</body>
</html>
