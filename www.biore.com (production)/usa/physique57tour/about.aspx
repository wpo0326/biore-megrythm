<%@ Page Language="C#" MasterPageFile="~/MasterPages/Physique.master" AutoEventWireup="true"
    CodeFile="about.aspx.cs" Inherits="physique_57_about" Title="Untitled Page" Debug="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta name="Description" content="Join the Biore Skincare & Physique 57 Tour and experience a free Physique 57 class." />
    <meta name="Keywords" content="Physique 57, Biore, fitness tour, free class" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <div id="about-txt">
          <p>Physique 57&trade; is an effective workout system in New York City and Los Angeles.  It is a focused and proven cardiovascular program of isometric exercises and orthopedic stretches.  Physique 57&trade; tones your seat, abdominals, thighs and arms; strengthens your legs and back; and improves posture to produce a beautiful, sexy body.</p>
        </div>
        <div id="do-at-class">
          <h3>What do you do in a Physique 57&trade; class?</h3>
          <p>The Physique 57&trade; training sets use your own body weight for resistance, and through dynamic exercises your muscles are targeted and overloaded to the point of fatigue, then stretched for relief. This process, called Interval Overload, overloads the muscle to create long, lean sexy muscles.</p>
          <a href="http://www.physique57.com/" target="_blank" id="btn">Visit Physique 57&trade; &gt;&gt;</a>
        </div> 
        <div id="aboutAbbrevRules"><p>NO PURCHASE NECESSARY. Open to legal residents of the 50 United States (D.C.) 18 years or older. Ends October 21, 2010.  Void where prohibited. For Official Rules, including odds, and prize descriptions <a href="javascript:openRules()">click here</a>.</p></div>   
</asp:Content>