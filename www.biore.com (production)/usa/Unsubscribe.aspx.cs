using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using KAOForms;

public partial class Unsubscribe : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Header.Title = "Bior� Skincare Products - Unsubscribe";
        Master.bodyClass = "optin";
        ((WebControl)Master.FindControl("gn4")).CssClass = "global_anchor active"; //Set nav class
		
		//turn off questionpro		
		((Panel)Page.Master.FindControl("panel_SDC_Analytics")).Visible = false;
		
        //if (Request.QueryString["email"] != null)
        //{
        //    FormUtils fUtils = new FormUtils();
        //    TextBox tb = (TextBox)fUtils.findControlRecursive(Page, "Email");
        //    tb.Text = Server.UrlDecode(Request.QueryString["email"]);
        //}
        Literal crmMetrixLiteral;
        crmMetrixLiteral = (Literal)Master.FindControl("literal_crmmetrix");
        if (crmMetrixLiteral != null)
        {
            crmMetrixLiteral.Visible = false;
        }
                


    }

}
