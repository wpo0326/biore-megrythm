<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%
	Option Explicit
	Dim section, pageTitle, strPreloadImages, pageDescription, pageKeywords
	section = ""
	pageTitle = "Contact Bior� today"
	pageDescription = "Contact Bior� by phone, mail, or through our online form."
	pageKeywords = "biore contact, bior� contact, biore mailing address"
	bodyClass = "footer_pages"
	
	eventcode = lcase(request("eventcode"))
	if eventcode <> "biorecontact" then
	    response.Redirect("/usa/Optin.aspx")
	end if
%>
<!--#include virtual="/usa/includes/header_event.asp"-->
<!-- Start Page Content -->
<div id="middle">
	<div id="middle_content">
        <div id="contact">
            <div id="contact_inner">
<!--#include virtual="/usa/Event/text/usa_en.asp"-->
<!--#include virtual="/usa/Event/master.asp"-->
	        </div>
	    </div>
    </div>
</div>

<!-- End Page Content -->
<!--#include virtual="/usa/includes/footer.asp"-->