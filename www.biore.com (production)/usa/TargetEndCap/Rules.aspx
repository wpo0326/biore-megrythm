﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Rules.aspx.cs" Inherits="TargetEndCap_Rules" %>

<%@ Register TagPrefix="uc1" TagName="ucFormConfig" Src="~/Controls/ucFormConfig.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
		<title></title>
		<link href="../css/TargetEndCap.css" rel="stylesheet" type="text/css" />

		<script type="text/javascript" src="/usa/js/jquery-1.3.2.min.js"></script>

	</head>
	<body class="rules">
		<form id="form1" runat="server">
		<div id="wrapper">
			<div id="header">
				<div id="headWrap">
					<a href="/usa/" class="ir" id="logo">Bior&eacute;&reg;</a>
					<h1 class="ir">The NEW LOOK Sweepstakes! <strong>Your chance to Win a $500 Gift Card!</strong></h1>
					<img src="../images/TargetEndCap/BioreProducts.jpg" alt="Biore Deep Cleansing Pore Strips and Blemish Fighting Ice Cleanser"
						height="244" width="157" />
				</div>
			</div>
			<div id="middle">
				<h2>Bior&eacute;&reg; Skincare New Look Sweepstakes<br />
					Official Rules</h2>
				<h3>NO PURCHASE NECESSARY. A PURCHASE OR PAYMENT OF ANY KIND WILL NOT INCREASE YOUR
					CHANCES OF WINNING.</h3>
				<p><strong>1. Eligibility:</strong> Bior&eacute;&reg; Skincare New Look Sweepstakes
					(the &quot;Sweepstakes&quot;) is open only to legal residents of the fifty (50)
					United States and the District of Columbia who are at least eighteen (18) years
					old at the time of entry. Employees of Kao Brands Company, Enlighten Inc., ePrize,
					LLC, and their parent and affiliate companies as well as the immediate family (spouse,
					parents, siblings and children) and household members of each such employee are
					not eligible. The Sweepstakes is subject to all applicable federal, state, and local
					laws and regulations and is void where prohibited. Participation constitutes entrant's
					full and unconditional agreement to these Official Rules and Sponsor's and Administrator's
					decisions, which are final and binding in all matters related to the Sweepstakes.
					Winning the prize is contingent upon fulfilling all requirements set forth herein.
				</p>
				<p><strong>2. Sponsor:</strong> Kao Brands Company, 2535 Spring Grove Ave, Cincinnati,
					OH 45214. Administrator: ePrize, LLC, One ePrize Drive, Pleasant Ridge, MI 48069.</p>
				<p><strong>3. Timing:</strong> The Sweepstakes begins on September 4, 2011 at 12:00
					a.m. Eastern Time ("ET") and ends on January 31, 2012 at 11:59 p.m. ET (the "Promotion
					Period"). Sponsor's computer is the official time-keeping device for the Sweepstakes.</p>
				<p><strong>4. How to Enter:</strong> There are two ways to enter this Sweepstakes:</p>
				<ul>
					<li><strong>a. Enter Online:</strong> During the Promotion Period, you can visit www.biore.com/newlook.
						Follow the instructions to complete and submit the registration form including a
						valid home address. You will automatically receive one (1) entry into the Sweepstakes.
					</li>
					<li><strong>b. Alternate Mail-in Method:</strong> Hand print your name, address, day
						and evening phone numbers, email address (if any), and date of birth on a 3" x 5"
						piece of paper and mail it in an envelope with proper postage to &quot;Bior&eacute;
						Skincare New Look Sweepstakes,&quot; c/o Enlighten, Inc., 3027 Miller Road, Ann
						Arbor, MI 48103. You will receive one (1) entry into the Sweepstakes. Limit: One
						(1) entry per envelope. All mail-in entries must be handwritten and must be postmarked
						by January 31, 2012 and received by February 7, 2012. All entries become the exclusive
						property of Sponsor and none will be acknowledged or returned. Proof of sending
						or submission will not be deemed to be proof of receipt by Sponsor. Sponsor is not
						responsible for lost, late, incomplete, invalid, unintelligible, illegible, misdirected
						or postage-due entries, which will be disqualified. </li>
					<li><strong>Limit:</strong> You may enter one (1) time during the Promotion Period,
						regardless of method of entry. Multiple entrants are not permitted to share the
						same email address. Any attempt by any entrant to obtain more than one (1) entry
						by using multiple/different email addresses, identities, registrations and logins,
						or any other methods will void that entrant's entries and that entrant may be disqualified.
						Use of any automated system to participate is prohibited and will result in disqualification.
						In the event of a dispute as to any registration, the authorized account holder
						of the email address used to register will be deemed to be the entrant. The &quot;authorized
						account holder&quot; is the natural person assigned an email address by an Internet
						access provider, online service provider or other organization responsible for assigning
						email addresses for the domain associated with the submitted address. The potential
						winner may be required to show proof of being the authorized account holder.</li>
				</ul>
				<p><strong>5. Grand Prize Drawing:</strong> Administrator is an independent judging
					organization whose decisions, and those of the Sponsor, as to the administration
					and operation of the Sweepstakes and the selection of the potential winner are final
					and binding in all matters related to the Sweepstakes. Administrator will randomly
					select the potential Sweepstakes winner from all eligible entries received during
					the Promotion Period, on or around February 8, 2012. Potential Sweepstakes winner
					will be required to sign and return, where legal, an Affidavit/Declaration of Eligibility
					and Liability/Publicity Release within ten (10) calendar days of prize notification.
					If potential Sweepstakes winner is considered a minor in his/her jurisdiction of
					residence, Liability/Publicity Release must be signed by his/her parent or legal
					guardian and such prize will be delivered to minor's parent/legal guardian and awarded
					in the name of parent/legal guardian. The potential winner will be notified by email,
					mail or phone. If the potential winner cannot be contacted, or prize is returned
					as undeliverable, potential winner forfeits the prize. Upon prize forfeiture, no
					compensation will be given. Receiving a prize is contingent upon compliance with
					these Official Rules. In the event that the potential winner is disqualified for
					any reason, Sponsor will award the prize to an alternate winner by random drawing
					from among all remaining eligible entries. Only three (3) alternate drawings will
					be held, after which the prize will remain un-awarded. </p>
				<p><strong>6. Prizes:</strong> ONE (1) GRAND PRIZE: A $500 gift card (terms and conditions
					of the card apply). Approximate Retail Value: $500. Prize is non-transferable and
					no substitution will be made except as provided herein at the Sponsor's sole discretion.
					Sponsor reserves the right to substitute the prize for one of equal or greater value
					if the designated prize should become unavailable for any reason. Winner is responsible
					for all taxes and fees associated with prize receipt and/or use. Odds of winning
					the prize depend on the number of eligible entries received during the Promotion
					Period. </p>
				<p><strong>7. Release:</strong> By entering Sweepstakes and/or receipt of any prize,
					entrant and/or winner agree to release and hold harmless Sponsor, Enlighten Inc.,
					Administrator, and their respective subsidiaries, affiliates, suppliers, distributors,
					advertising/promotion agencies, and prize suppliers, and each of their respective
					parent companies and each such company's officers, directors, employees and agents
					(collectively, the &quot;Released Parties&quot;) from and against any claim or cause
					of action, including, but not limited to, personal injury, death, or damage to or
					loss of property, arising out of participation in the Sweepstakes or receipt or
					use or misuse of any prize. </p>
				<p><strong>8. Publicity:</strong> Except where prohibited, participation in the Sweepstakes
					constitutes the winner's consent to Sponsor's and its agents' use of winner's name,
					likeness, photograph, voice, opinions and/or hometown and state for promotional
					purposes in any media, worldwide, without further payment or consideration.
				</p>
				<p><strong>9. General Conditions:</strong> Sponsor reserves the right to cancel, suspend
					and/or modify the Sweepstakes, or any part of it, if any fraud, technical failures
					or any other factor beyond Sponsor's reasonable control impairs the integrity or
					proper functioning of the Sweepstakes, as determined by Sponsor in its sole discretion.
					Sponsor reserves the right, in its sole discretion, to disqualify any individual
					it finds to be tampering with the entry process or the operation of the Sweepstakes
					or to be acting in violation of the Official Rules of this or any other promotion
					or in an unsportsmanlike or disruptive manner. Any attempt by any person to deliberately
					undermine the legitimate operation of the Sweepstakes may be a violation of criminal
					and civil law, and, should such an attempt be made, Sponsor reserves the right to
					seek damages from any such person to the fullest extent permitted by law. Sponsor's
					failure to enforce any term of these Official Rules shall not constitute a waiver
					of that provision. </p>
				<p><strong>10. Limitations of Liability:</strong> The Released Parties are not responsible
					for: (1) any incorrect or inaccurate information, whether caused by entrants, printing
					errors or by any of the equipment or programming associated with or utilized in
					the Sweepstakes; (2) technical failures of any kind, including, but not limited
					to malfunctions, interruptions, or disconnections in phone lines or network hardware
					or software; (3) unauthorized human intervention in any part of the entry process
					or the Sweepstakes; (4) technical or human error which may occur in the administration
					of the Sweepstakes or the processing of entries; (5) late, lost, undeliverable,
					damaged or stolen mail; or (6) any injury or damage to persons or property which
					may be caused, directly or indirectly, in whole or in part, from entrant's participation
					in the Sweepstakes or receipt or use or misuse of any prize. If for any reason an
					entrant's entry is confirmed to have been erroneously deleted, lost, or otherwise
					destroyed or corrupted, entrant's sole remedy is another entry in the Sweepstakes,
					if it is possible. If the Sweepstakes, or any part of it, is discontinued for any
					reason, Sponsor, in its sole discretion, may elect to hold a random drawing from
					among all eligible entries received up to the date of discontinuance for any or
					all of the prizes offered herein. No more than the stated number of prizes will
					be awarded. In the event that production, technical, seeding, programming or any
					other reasons cause more than the stated number of prizes as set forth in these
					Official Rules to be available and/or claimed, Sponsor reserves the right to award
					only the stated number of prizes by a random drawing among all legitimate, unawarded,
					eligible prize claims.</p>
				<p><strong>11. Disputes:</strong> Except where prohibited, entrant agrees that: (1)
					any and all disputes, claims and causes of action arising out of or connected with
					this Sweepstakes or any prize awarded shall be resolved individually, without resort
					to any form of class action, and exclusively by the United States District Court
					for the Eastern District of Michigan (Southern Division) or the appropriate Michigan
					State Court located in Oakland County, Michigan; (2) any and all claims, judgments
					and awards shall be limited to actual out-of-pocket costs incurred, including costs
					associated with entering this Sweepstakes, but in no event attorneys' fees; and
					(3) under no circumstances will entrant be permitted to obtain awards for, and entrant
					hereby waives all rights to claim, indirect, punitive, incidental and consequential
					damages and any other damages, other than for actual out-of-pocket expenses, and
					any and all rights to have damages multiplied or otherwise increased. All issues
					and questions concerning the construction, validity, interpretation and enforceability
					of these Official Rules, or the rights and obligations of the entrant and Sponsor
					in connection with the Sweepstakes, shall be governed by, and construed in accordance
					with, the laws of the State of Michigan without giving effect to any choice of law
					or conflict of law rules (whether of the State of Michigan or any other jurisdiction),
					which would cause the application of the laws of any jurisdiction other than the
					State of Michigan. </p>
				<p><strong>12. Entrant's Personal Information:</strong> Information collected from entrants
					is subject to ePrize, LLC's Privacy Policy <a href="http://www.eprize.com/privacy-policy"
						target="_blank">http://www.eprize.com/privacy-policy</a> and Sponsor's Privacy
					Policy <a href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">http://www.kaobrands.com/privacy_policy.asp</a>.
				</p>
				<p><strong>13. Winner List:</strong> For a winner list, visit <a href="http://bit.ly/nXUPWo"
					target="_blank">http://bit.ly/nXUPWo</a>. The winner list will be posted after winner
					confirmation is complete.</p>
			</div>
			<div id="footer">
				<ul>
					<li id="copyright">&copy; 2011 Kao Brands Company. All Rights Reserved</li>
					<li><a href="/usa/about-biore.aspx">About Bior&eacute;<sup>&reg;</sup> Skincare</a></li>
					<li><a target="_blank" href="http://www.kaobrands.com/">Kao Brands Company</a></li>
					<li><a href="/usa/where-to-buy.aspx">Where to Buy</a></li>
					<li><a href="/usa/ContactUs.aspx">Contact Us</a></li>
					<li><a target="_blank" href="http://www.kaobrands.com/legal.asp">Legal</a></li>
					<li><a target="_blank" href="http://www.kaobrands.com/privacy_policy.asp">Privacy Policy</a></li>
				</ul>
			</div>
		</div>
		</form>
	</body>
</html>
