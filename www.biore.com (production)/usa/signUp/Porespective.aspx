<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true"
    CodeFile="Porespective.aspx.cs" Inherits="Porespective" Title="Porespective Sign Up" Debug=true %>
    <%@ Register TagPrefix="uc1" TagName="ucFormConfig" Src="~/Controls/ucFormConfig.ascx" %>

<%@ MasterType VirtualPath="~/MasterPages/Main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<script type="text/javascript" src="/usa/js/swfObject2.2.js"></script>
	<script type="text/javascript" src="/usa/js/videoUtils.js"></script>
	<!--[if lt IE 7]>
	<script>
	    DD_belatedPNG.fix('#optin_inner, #ctl00_ContentPlaceHolder1_OptinForm h1');
	</script>
	<![endif]--> 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   

   <div id="porespective">
        <div id="contact_top_starBurst"></div>
        <div id="porespective_inner">
        <asp:Panel ID="PanelForm" runat="server" Visible="false">                   
            <%--Include the Dynamic Web Form--%>
			 <script type="text/javascript">
        var nonflashVideoContentPart1 = 'To see this video you need JavaScript enabled and the latest version of Flash.  You can also use a browser that supports H.264 HTML5 Video.';
        var nonflashVideoContentPart2LinkText = 'Click here';
        var nonflashVideoContentPart2LinkUrl = 'http://www.adobe.com/go/getflashplayer';
        var nonflashVideoContentPart2Text = ' to go to the Adobe Flash download center.';
    </script>
            <uc1:ucFormConfig id="ucFormConfig" runat="server"></uc1:ucFormConfig>  
            
            <span id="facebookReminder">Be sure to &quot;Like&quot; our page so you'll be among the first to know when we publish our fresh fan pore-spectives on Facebook.</span>
            <p id="porespectiveDisclaimer"><sup>&dagger;</sup>Open to legal residents of the 50 United States (including D.C.), 18 years old or older who are invited by Sponsor to participate. Offer ends on November 27, 2011 at 11:59 p.m. ET. Limit one Offer per person/per email address.  Void where prohibited.  For complete Terms and Conditions and complete details, visit <a href="/usa/promotions/rules/porespectivesRules.aspx" target="_blank">http://www.biore.com/usa/promotions/rules/porespectivesRules.aspx</a>.</p>      
        </asp:Panel>  
        <asp:Panel ID="PanelNotEligable" runat="server" Visible="false">        
            <div id="notEligable">
                <h2>Fresh Fan Pore Spective&trade; Tell Us What You Think of OUR NEW LOOK</h2>
                <h3>We're sorry</h3>
                <p>This offer is limited to eligible members only.</p>
            </div>
        </asp:Panel>
        
        <div id="wrapUp">Bior&eacute;&reg;. Get your best clean.</div>
      <script type="text/javascript">


          $(function() {


          toggle("noneToggle1");
          toggle("noneToggle2");
          toggle("noneToggle3");
          toggle("noneToggle4");

              function toggle(className) {
                  var checkToggle = $("." + className + " input:last");
                  checkToggle.click(function() {
                  if ($("#" + checkToggle.attr('id') + ":checked").val()) {
                      $("." + className + " input").each(function() {
                      if ($(this).attr('id') != checkToggle.attr('id')) {
                                  $(this).attr("checked", false);
                                  $(this).attr('disabled', 'disabled');
                              }
                          });
                      } else {
                      $("." + className + "  input").each(function() {
                      if ($(this).attr('id') != checkToggle.attr('id')) {
                                  $(this).removeAttr('disabled');
                              }
                          });
                      }

                  });
              }
          }); 
		  
		  $(document).ready(function() {
			$("a.videoLinkInline").click(function(e) {
				e.preventDefault();
				var vidParams = $(this).attr("rel").split("::")[1].split("|");

				playVideo($(this).attr("href"), "videoPlayer", vidParams[0], vidParams[1], vidParams[2], "/usa/flash/FlashVideoPlayer_scale.swf", 'true');

			});
});
             
    </script>
	
	</div>
	</div>
    <div id="content_bottom">
    </div>
</asp:Content>
