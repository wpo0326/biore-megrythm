using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using KAOForms;
using System.Collections.Specialized;
using System.Reflection;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;


public partial class Porespective : System.Web.UI.Page
{
    private const int EVENTID = 400213;
	private static readonly byte[] IV = Encoding.ASCII.GetBytes(System.Configuration.ConfigurationManager.AppSettings["AES_IV"]);
	private static readonly byte[] KEY = Encoding.ASCII.GetBytes(System.Configuration.ConfigurationManager.AppSettings["AES_KEY"]);


    protected void Page_Load(object sender, EventArgs e)
    {
        //don't load other promos
        if (Request.QueryString["promo"] != null)
        {
            Response.Redirect("~/PageNotFound.aspx");
        }       
		
	   Master.bodyClass = "optin";
			
		
		//turn off questionpro		
		((Panel)Page.Master.FindControl("panel_SDC_Analytics")).Visible = false;

			
		//get email and memberid from key
       if(Request.QueryString["val"]!=null && !string.IsNullOrEmpty(Request.QueryString["val"].ToString())){
			
			try{
			byte[] fromQstring = StringToByteArray(Request.QueryString["val"].ToString());
			string decodedValues = decryptStringFromBytes_AES(fromQstring);
			char[] charSeparators = new char[] {'&'};
			string[] values = decodedValues.Split(charSeparators);
			
			string email = values[0].ToLower().Replace("email=","");
			string memID = values[1].ToLower().Replace("memid=","");
			
			addQueryString("email",email);
			addQueryString("memid",memID);
			
			setDisplayState("form");
			}
			catch{
				setDisplayState("notEligable");
			}
		}
		else
		{
			 setDisplayState("notEligable");			 
		}		

		
    }
	
	
	private void addQueryString(string name, string value)
	{
		NameValueCollection QS = Request.QueryString;
			QS = (NameValueCollection)Request.GetType().GetField("_queryString", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(Request);
			PropertyInfo readOnlyInfo = QS.GetType().GetProperty("IsReadOnly", BindingFlags.NonPublic | BindingFlags.Instance);
			readOnlyInfo.SetValue(QS, false, null);
			QS[name] = value;
			readOnlyInfo.SetValue(QS, true, null); 
	
	
		string key = string.Empty;
		
		
	}
	
	 private static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
	
	 private string decryptStringFromBytes_AES(byte[] cipherText)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (KEY == null || KEY.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");

            // TDeclare the streams used
            // to decrypt to an in memory
            // array of bytes.
            MemoryStream    msDecrypt   = null;
            CryptoStream    csDecrypt   = null;
            StreamReader    srDecrypt   = null;

            // Declare the RijndaelManaged object
            // used to decrypt the data.
            RijndaelManaged aesAlg      = null;

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            try
            {
                // Create a RijndaelManaged object
                // with the specified key and IV.
                aesAlg = new RijndaelManaged();
                aesAlg.Key = KEY;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                msDecrypt = new MemoryStream(cipherText);
                csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);
                srDecrypt = new StreamReader(csDecrypt);

                // Read the decrypted bytes from the decrypting stream
                // and place them in a string.
                plaintext = srDecrypt.ReadToEnd();
            }
            finally
            {
                // Clean things up.

                // Close the streams.
                if (srDecrypt != null)
                    srDecrypt.Close();
                if (csDecrypt != null)
                    csDecrypt.Close();
                if (msDecrypt != null)
                    msDecrypt.Close();

                // Clear the RijndaelManaged object.
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            return plaintext;

        }

    private void setDisplayState(string value)
    {
        switch (value)
	    {
            case "form":
                PanelForm.Visible = true;
                PanelNotEligable.Visible = false;
                break;
            case "notEligable":
                PanelNotEligable.Visible = true;
                PanelForm.Visible = false;
                break;
	    }
    }

    
}
