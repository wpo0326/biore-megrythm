<%@ Language=VBScript %>
<%
redirectUrl = "/usa/Unsubscribe.aspx"

' FIND AND ADD ANY QUERY STRING PARAMETERS IF NECESSARY '

dim reconstructedQueryString
dim key
reconstructedQueryString = ""
for each key in Request.Querystring
	Response.Write Server.URLEncode(key) & ": " & Server.URLEncode(Request.Querystring(key)) & "<BR>"
	If Not reconstructedQueryString = "" Then
		reconstructedQueryString = reconstructedQueryString & "&"
	End If
	reconstructedQueryString = reconstructedQueryString & Server.URLEncode(key) & "=" & Server.URLEncode(Request.Querystring(key))
next

If Not reconstructedQueryString = "" Then
	redirectUrl = redirectUrl & "?" & reconstructedQueryString
End If


' FINALLY DO 301 REDIRECT '

Response.Status = "301 Moved Permanently"
Response.addheader "Location", redirectUrl
Response.End

%>