﻿using KAOForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Prom : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Header.Title = "Bior&eacute;&reg; Skincare facial cleanser survey.";

        if (!IsPostBack)
        {
        }
        else
        {
            Page.Validate();
            if (Page.IsValid)
            {
                PageUtils pUtils = new PageUtils();
                Control head = pUtils.getHeaderControl(Page);
                pUtils.createMetaDataTag("DCSext.submit", "true", "metaSubmit", head);

                string products = "";
                foreach (ListItem li in Products.Items)
                {
                    if (li.Selected)
                        products += li.Value + ",";
                }
                if (Other.Text.Length > 0)
                {
                    if (Other.Text != "Other")
                        products += Other.Text;
                }

                String dbBiore = ConfigurationManager.ConnectionStrings["bioreDB"].ConnectionString;
                SqlConnection BioreConn = new SqlConnection(dbBiore);
                BioreConn.Open();
                SqlCommand myCmd = new SqlCommand();
                myCmd.CommandText = "spMiscAddPromSurvey";
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.Connection = BioreConn;
                myCmd.Parameters.Add(new SqlParameter("@Products", products));
                myCmd.Parameters.Add(new SqlParameter("@TrySample", TrySample.SelectedValue));
                myCmd.Parameters.Add(new SqlParameter("@WillPurchase", WillPurchase.SelectedValue));
                myCmd.Parameters.Add(new SqlParameter("@Facebook", Facebook.SelectedValue));
                myCmd.Parameters.Add(new SqlParameter("@TagCode", TagCode.SelectedValue));
                myCmd.Parameters.Add(new SqlParameter("@OptIn", OptIn.SelectedValue));
                SqlDataReader myReader;

                myReader = myCmd.ExecuteReader();
                while (myReader.Read())
                {
                    if (Int32.Parse(myReader[0].ToString()) != 0)
                    {
                        Survey.Visible = false;
                        InvalidEntry.Visible = true;
                    }
                }
                myReader.Close();
                BioreConn.Close();
                if (OptIn.SelectedValue == "Yes")
                    Response.Redirect("/usa/optin.aspx?src=prom");
                else
                    Response.Redirect("/usa/default.aspx?src=prom");

            }
        }
    }
    protected void productValidation(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = false;
        foreach (ListItem item in Products.Items)
        {
            if (item.Selected)
                e.IsValid = true;
        }
        if (Other.Text.Length > 0)
            e.IsValid = true;
        return;
    }

}
