<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true"
    CodeFile="Prom.aspx.cs" Inherits="Prom" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="survey.css" media="screen, projection" />
    <!--[if lt IE 7]>
	<script>
	    DD_belatedPNG.fix('#innerbody');
	</script>
	<![endif]-->

    <script type="text/javascript" language="javascript">
        var prodValid = false;
        function CheckProducts(oSrc, args) {
            if (document.getElementById("ctl00_ContentPlaceHolder1_Products_0").checked || document.getElementById("ctl00_ContentPlaceHolder1_Products_1").checked || document.getElementById("ctl00_ContentPlaceHolder1_Products_2").checked || document.getElementById("ctl00_ContentPlaceHolder1_Products_3").checked || document.getElementById("ctl00_ContentPlaceHolder1_Products_4").checked || document.getElementById("ctl00_ContentPlaceHolder1_Products_5").checked || document.getElementById("ctl00_ContentPlaceHolder1_Products_6").checked || document.getElementById("ctl00_ContentPlaceHolder1_Products_7").checked || document.getElementById("ctl00_ContentPlaceHolder1_Products_8").checked || document.getElementById("ctl00_ContentPlaceHolder1_Products_9").checked)
                prodValid = true;
            args.IsValid = prodValid;
        }
        function CheckOther(oSrc, args) {
            if (document.getElementById("ctl00_ContentPlaceHolder1_Other").value != "")
                prodValid = true;
            args.IsValid = prodValid;
        }
        function CheckPage(oSrc, args) {
            CheckProducts(oSrc, args);
            CheckOther(oSrc, args);
            args.IsValid = prodValid;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="innerbody">
        <br />
        <h1>
            <strong>Bior&eacute;&reg; Skincare Teen Prom</strong></h1>
        <br />
        <asp:Panel ID="Survey" runat="server" Visible="true">
            <p>
                Thank you for taking our survey. Please answer the following questions:</p>
            <br />
            <p>
                What products do you currently use? (select all that apply)</p>
            <asp:CheckBoxList ID="Products" runat="server">
                <asp:ListItem Text="Acne Free&reg;" Value="Acne Free"></asp:ListItem>
                <asp:ListItem Text="Bior&eacute;&reg; Skincare" Value="Biore"></asp:ListItem>
                <asp:ListItem Text="CLEAN & CLEAR&reg;" Value="Clean & Clear"></asp:ListItem>
                <asp:ListItem Text="Clearasil&reg;" Value="Clearasil"></asp:ListItem>
                <asp:ListItem Text="Neutrogena&reg;" Value="Neutrogena"></asp:ListItem>
                <asp:ListItem Text="Noxzema&reg;" Value="Noxzema"></asp:ListItem>
                <asp:ListItem Text="Oxy&reg; Skin Care" Value="Oxy"></asp:ListItem>
                <asp:ListItem Text="Proactiv&reg;" Value="Proactiv"></asp:ListItem>
                <asp:ListItem Text="St. Ives&reg;" Value="St. Ives"></asp:ListItem>
                <asp:ListItem Text="None" Value="None"></asp:ListItem>
            </asp:CheckBoxList>
            Other:
            <asp:TextBox ID="Other" runat="server" Text="" MaxLength="100"></asp:TextBox>
            <asp:TextBox runat="server" ID="ProductsTest" Text="Products_" CssClass="HiddenControl"></asp:TextBox>
            <asp:CustomValidator ControlToValidate="ProductsTest" ID="ProductsValidator" runat="server"
                Display="Dynamic" ClientValidationFunction="CheckProducts">
            </asp:CustomValidator>
            <asp:CustomValidator ID="OtherValidator" runat="server" ClientValidationFunction="CheckOther"
                Display="Dynamic">
            </asp:CustomValidator>
                <asp:RegularExpressionValidator ID="OtherValueValidator" runat="server" ErrorMessage="The characters '>' and '<' are not permitted."
                ValidationExpression="^[^<>]+$" ControlToValidate="Other" EnableClientScript="true"
                SetFocusOnError="true"></asp:RegularExpressionValidator>
            <asp:CustomValidator ID="OverallProductValidator" runat="server" ErrorMessage="Please answer all questions."
                ClientValidationFunction="CheckPage" Display="Dynamic" OnServerValidate="productValidation">
            </asp:CustomValidator>
            <br />
            <br />
            <p>
                Did you try your Bior&eacute;&reg; Skincare sample?</p>
            <asp:RadioButtonList ID="TrySample" runat="server">
                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                <asp:ListItem Text="No" Value="No"></asp:ListItem>
            </asp:RadioButtonList>
            <asp:RequiredFieldValidator ID="TrySampleValidator" ControlToValidate="TrySample"
                ErrorMessage="Please answer all questions." SetFocusOnError="true" runat="server"></asp:RequiredFieldValidator>
            <br />
            <br />
            <p>
                After trying, will you purchase any of the Bior&eacute;&reg; products you sampled?</p>
            <asp:RadioButtonList ID="WillPurchase" runat="server">
                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                <asp:ListItem Text="Maybe" Value="Maybe"></asp:ListItem>
                <asp:ListItem Text="No" Value="No"></asp:ListItem>
                <asp:ListItem Text="Didn�t Try" Value="Didn�t Try"></asp:ListItem>
            </asp:RadioButtonList>
            <asp:RequiredFieldValidator ID="WillPurchaseValidator" ControlToValidate="WillPurchase"
                ErrorMessage="Please answer all questions." SetFocusOnError="true" runat="server"></asp:RequiredFieldValidator>
            <br />
            <br />
            <p>
                Are you a fan of Bior&eacute;&reg; Skincare on Facebook?</p>
            <asp:RadioButtonList ID="Facebook" runat="server">
                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                <asp:ListItem Text="No" Value="No"></asp:ListItem>
            </asp:RadioButtonList>
            <asp:RequiredFieldValidator ID="FacebookValidator" ControlToValidate="Facebook" ErrorMessage="Please answer all questions."
                SetFocusOnError="true" runat="server"></asp:RequiredFieldValidator>
            <br />
            <br />
            <p>
                Did you access this survey using the Microsoft Tag Code?</p>
            <asp:RadioButtonList ID="TagCode" runat="server">
                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                <asp:ListItem Text="No" Value="No"></asp:ListItem>
            </asp:RadioButtonList>
            <asp:RequiredFieldValidator ID="TagCodeValidator" ControlToValidate="TagCode" ErrorMessage="Please answer all questions."
                SetFocusOnError="true" runat="server"></asp:RequiredFieldValidator>
            <br />
            <br />
            <p>
                Would you like to opt-in to receive future communications from Bior&eacute;&reg;
                Skincare?</p>
            <asp:RadioButtonList ID="OptIn" runat="server">
                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                <asp:ListItem Text="No" Value="No"></asp:ListItem>
            </asp:RadioButtonList>
            <asp:RequiredFieldValidator ID="OptInValidator" ControlToValidate="OptIn" ErrorMessage="Please answer all questions."
                SetFocusOnError="true" runat="server"></asp:RequiredFieldValidator>
            <br />
            <br />
            <asp:Button ID="Submit" Text="Submit" UseSubmitBehavior="true" runat="server" /><br />
            <br />
            <br />
            <br />
        </asp:Panel>
        <asp:Panel ID="InvalidEntry" runat="server" Visible="false">
            <h2>
                We're Sorry...</h2>
            <p>
                There has been a technical problem with your survey. Please go back and try again.</p>
        </asp:Panel>
    </div>
</asp:Content>
