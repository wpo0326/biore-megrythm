<%@ Page Language="C#" AutoEventWireup="true" CodeFile="redir.aspx.cs" Inherits="redir" Title="Untitled Page" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            If your browser doesn't redirect automatically, click <a href="<asp:Literal ID='litRedirectUrl' runat='server' />">here</a>.
        </div>
    </form>

    <!-- START OF SmartSource Data Collector TAG -->
    <script src="/usa/js/webtrends.js" type="text/javascript"></script>
    <script type="text/javascript">
        //<![CDATA[
        var _tag=new WebTrends();
        _tag.dcsGetId();
        //]]>>
    </script>
    <script type="text/javascript">
        //<![CDATA[
        // Add custom parameters here.
        //_tag.DCSext.param_name=param_value;
        _tag.dcsCollect();
        //]]>>
    </script>
    <noscript>
        <div><img alt="DCSIMG" id="DCSIMG" width="1" height="1" src="http://sdc.enlighten.com/dcsx5r2z6x4568dp93c02vf9d_4y5t/njs.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=8.6.0"/></div>
    </noscript>
    <!-- END OF SmartSource Data Collector TAG -->
    
    
    <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    
    <script type="text/javascript">
        try {
            var firstTracker = _gat._getTracker("UA-385129-27"); // Global Tag
            firstTracker._setDomainName("none");
            firstTracker._trackPageview();
            var secondTracker = _gat._getTracker("UA-385129-4"); // Biore Tag
            secondTracker._setCampNameKey("enl_campaign")
            secondTracker._setCampMediumKey("enl_medium")
            secondTracker._setCampSourceKey("enl_source")
            secondTracker._trackPageview();
        } catch(err) {}
    </script>
</body>
</html>