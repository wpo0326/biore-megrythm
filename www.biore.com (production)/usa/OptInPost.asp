<!-- #include virtual="/sitevars.asp" -->
<% 	
pageTitle = "Sign up to be notified of the latest from Bior&eacute;&reg; Skincare"
pageDescription = "Sign up to receive the latest news, coupons, and free samples from Bior&eacute;&reg; Skincare, the makers of Bior&eacute;&reg; Pore Strips, Dual Fusion&trade; Moisturizer + SPF 30, and more!"
pageKeywords = "Biore, samples, freebies, giveaways, coupons, sign up, updates, promotions"
bodyID = "new_optin" 
bodyClass = "footer_pages"
pageSpecificStyles = "/usa/css/optin.css" 

    Dim strRet, strError, objRequest, objXMLDoc, objXMLNode, jfoptin, banoptin, cureloptin, jergensoptin
    strRet = -1
    If Request.Form("jfoptin") = "on" Then
        jfoptin = "1"
    Else
        jfoptin = "0"

    End If
    If Request.Form("banoptin") = "on" Then
        banoptin = "1"
    Else
        banoptin = "0"
    End If
    If Request.Form("cureloptin") = "on" Then
        cureloptin = "1"
    Else
        cureloptin = "0"
    End If
    If Request.Form("jergensoptin") = "on" Then
        jergensoptin = "1"
    Else
        jergensoptin = "0"
    End If
	
    SET objRequest = Server.CreateObject("MSXML2.ServerXMLHTTP")
    With objRequest
        .open "POST", url_root + "webservices/bioreContact.asmx/bioreOptin", False
        .setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
        .send "fname=" & Server.UrlEncode(Request.Form("FName")) & _
            "&lname=" & Server.UrlEncode(Request.Form("LName")) & _
            "&email=" & Server.UrlEncode(Request.Form("Email")) & _
            "&postalCode=" & Server.UrlEncode(Request.Form("PostalCode")) & _
            "&gender=" & Server.UrlEncode(Request.Form("Gender")) & _
            "&DOB=" & Server.UrlEncode(Request.Form("mm") & "/" & Request.Form("dd") & "/" & Request.Form("yyyy")) & _
            "&ProdUsedDaily=" & Server.UrlEncode(Request.Form("ProdUsedDaily")) & _
            "&PayMoreAttention=" & Server.UrlEncode(Request.Form("PayMoreAttention")) & _
            "&WillingToPayMore=" & Server.UrlEncode(Request.Form("WillingToPayMore")) & _
            "&RecommendFriends=" & Server.UrlEncode(Request.Form("RecommendFriends")) & _
            "&ProductsUsing=" & Server.UrlEncode(Request.Form("ProductsUsing")) & _
            "&MostUsedBrand=" & Server.UrlEncode(Request.Form("MostUsedBrand")) & _
            "&TypesProductsUsed=" & Server.UrlEncode(Request.Form("TypesProductsUsed")) & _
            "&ProductAttributes=" & Server.UrlEncode(Request.Form("ProductAttributes")) & _
            "&FaceCareProblems=" & Server.UrlEncode(Request.Form("FaceProblems")) & _
            "&SkinType=" & Server.UrlEncode(Request.Form("SkinType")) & _
            "&Race=" & Server.UrlEncode(Request.Form("Race")) & _
            "&Spanish=" & Server.UrlEncode(Request.Form("Spanish")) & _
            "&banoptin=" & Server.UrlEncode(banoptin) & _
            "&cureloptin=" & Server.UrlEncode(cureloptin) & _
            "&jergensoptin=" & Server.UrlEncode(jergensoptin) & _
            "&jfoptin=" & Server.UrlEncode(jfoptin)
    End With
    
    SET objXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
    objXMLDoc.async = False

    If objXMLDoc.LoadXml(objRequest.ResponseXml.Xml) Then
        SET objXMLNode = objXMLDoc.SelectSingleNode("int")
        If Not objXMLNode Is Nothing Then
            strRet = objXMLNode.NodeTypedvalue
        End If
    Else
        strError = objXMLDoc.parseError.reason
        'Response.write(strError)
    End If
    SET objRequest = Nothing
    SET objXMLDoc = Nothing
%>
<!-- #include virtual = "/usa/includes/header.asp"    -->
<div id="middle">
    <div id="middle_content">
        <table class="event" cellspacing="0" cellpadding="0" width="97%">
            <tr valign="top">
                <td class="event_sidebar" valign="top">
                    <img src="/usa/event/images/sidebar_biorecontact.jpg" alt="" />
                </td>
                <td class="event_content" valign="top">
                    <% if strRet = 0 Then %>
                    <h1>
                        Thanks!</h1>
                    <p>
                        Thanks so much for signing up for Bior&eacute;&reg; brand information. We look forward to
                        letting you know about great events, contests, and product news throughout the year!</p>                    
                    <% else  %>
                    <h1>
                        We're Sorry...</h1>
                    <p>
                        There has been a problem with your form submission. Please enable javascript and
                        <a href="javascript: back();">try again</a>.</p>
                    <% end if %>
                </td>
            </tr>
        </table>
    </div>
</div>
<!-- #include virtual = "/usa/includes/footer.asp"    -->
