<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true" CodeFile="PageNotFound.aspx.cs" Inherits="products_product_detail" Title="Untitled Page" Debug="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="DCS.dcssta" content="404" />
    <meta name="Description" content="Page not found" />
    <meta name="Keywords" content="Biore, page not found" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="prod_detail" class="pagenotfound">
        <div id="prod_detail_inner">
            <h1>Page Not Found</h1>
            <p>We're sorry. The page you requested can not be found or there was an error retrieving it.</p>
            <br /><a href="/usa/">Home</a>
        </div>
    </div>
    
</asp:Content>

