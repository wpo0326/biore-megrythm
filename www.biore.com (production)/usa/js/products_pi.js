$(document).ready(function(){
	// activate tabs for product detail page
	if (  $('#product_detail_tabs').length > 0 ) {
		var $tabs = $('#product_detail_tabs').tabs();
		SWFAddress.addEventListener(SWFAddressEvent.CHANGE, handleChange);

		// set up swfaddress so the back button works
		$("#product_detail_tabs li a").click(function(){
			var theHash = this.hash.replace("#", "");
			SWFAddress.setValue(theHash);
		});
	}

	// hover functionality for product index
	$(".prod_link a").hover(
	    function(e){
            $(this).addClass("active");
            $(this).parent().parent().find("h3").addClass("active");
            
        },	
        function(e){
            $(this).removeClass("active");
            $(this).parent().parent().find("h3").removeClass("active");
	});


	// hover functionality for product detail works great with
	var prod_img = "ctl00_ContentPlaceHolder1_prod_image_sm";
	var base = $("#"+prod_img).attr("src");
	$("#works_great_with li").hover(
		function(e){
		// swap img IFF not hovering over prod_image_sm
		if ($(this).attr('id') != "prod_image_sm"){
			theLinkDivId = $(this).attr('id').replace("wgw_", "");
			$("#"+prod_img).attr("src","/usa/images/products/small/"+theLinkDivId+".png");
		}
	   },	function(e){
	    // swap img iff not hovering over prod_image_sm
		if ($(this).attr('id') != "prod_image_sm"){
			$("#"+prod_img).attr("src", base);
		}
	});


	$(".lkPop").click(function(){
		$("#TB_overlay").removeAttr("opacity");
	});
});

function handleChange(event){
	// show the correct tab, based on the hash value from swfaddress
	switch(event.path){
		case "/what_it_is":
			$("#product_detail_tabs").tabs("select", 0);
		break;
		case "/the_difference":
	$("#product_detail_tabs").tabs("select", 1);
		break;
		case "/how_to_use":

	$("#product_detail_tabs").tabs("select", 2);
		break;
		default :
			$("#product_detail_tabs").tabs("select", 0);
		break;
	}
}

