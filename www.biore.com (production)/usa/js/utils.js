function tabit() {	
		//var $mainTabs = $("#tabbed_content").tabs(); 
		var $productTabs = $("#how_it_works").tabs();
		var theHash = document.location.hash;
		$productTabs.tabs('select', theHash);
		
		$(".scrub").click(function() { $productTabs.tabs('select', 1);	return false;});
		$(".cleanser").click(function() { $productTabs.tabs('select', 2);	return false;});
		$(".toner").click(function() { $productTabs.tabs('select', 3);	return false;});
		$(".pore_strips").click(function() { $productTabs.tabs('select', 4);	return false;});
}

function openICP(optionalParamStr){
	if (optionalParamStr == undefined) {
		optionalParamStr = '';
	}
	if (optionalParamStr != '') {
		optionalParamStr = "&" + optionalParamStr;
	}
	tb_show("", "/usa/instantlyClearerPores/IC_intro.aspx?height=575&width=967" + optionalParamStr, "");
}

function openMURT(optionalParamStr){
	if (optionalParamStr == undefined) {
		optionalParamStr = '';
	}
	if (optionalParamStr != '') {
		optionalParamStr = "&" + optionalParamStr;
	}
  playVideo ("/usa/flash/videos/MURT_640x360_768K.flv", "62", "640", "360", "product_detail_flash_lbVideo", "", optionalParamStr);
  closeLink ();
}

function closeLink () {
  $(".video_close").click(function(e){
      e.preventDefault();
      tb_remove();
  });
  $(".video_close").hover(
    function () {$(this).addClass('video_close_over');},
    function () {$(this).removeClass('video_close_over');}
  );
}

function homeFlash(){
  // setup SWFObject
  var flash1_contentVersion = "9.0.115";
  var flash1_bgcolor = "transparent";
  var flash1_id_name = "home_theatre_flash";
	var flash1_swfUrl = "/usa/flash/BioreHomeTheater.swf";
	var flash1_width = "950";
	var flash1_height = "407";
  var so = new SWFObject(flash1_swfUrl, flash1_id_name, flash1_width, flash1_height, flash1_contentVersion, flash1_bgcolor);
  so.addParam("menu", "false");
  so.addParam("wmode", "transparent");
  so.addParam("FlashVars", "xmlPath=xml/home_theater_content.xml");

  // write the flash object
	so.write("home_theatre");
}

// Testing HTML to invoke	
//	<script type="text/javascript" src="/usa/js/utils.js"></script>
//<script type="text/javascript">
//    tabit();
//</script>
//    