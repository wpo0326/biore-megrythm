function displayOverlay(){
		var windowHeight, yScroll;

        if (window.innerHeight && window.scrollMaxY) {
                yScroll = window.innerHeight + window.scrollMaxY;
        } else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
                yScroll = document.body.scrollHeight;
        } else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
                yScroll = document.body.offsetHeight;
        }


        if (self.innerHeight) { // all except Explorer
                windowHeight = self.innerHeight;
        } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
                windowHeight = document.documentElement.clientHeight;
        } else if (document.body) { // other Explorers
                windowHeight = document.body.clientHeight;
        }

        // for small pages with total height less then height of the viewport
		if(yScroll < windowHeight){pageHeight = windowHeight;}
		else {pageHeight = yScroll;}

        var t = setTimeout(
			function(){
				var userAgent = navigator.userAgent.toLowerCase();
							
				//if((window.innerHeight) && (window.innerHeight > document.body.offsetHeight)){
					/*var theContainer = document.getElementById("container");
					theContainer.style.height = pageHeight + 'px';
					theContainer.style.overflow = "hidden";
					theBody.style.overflow = "hidden";*/
					//$("body").css({height: pageHeight, overflow: "hidden"});
				//}
				
				if (userAgent.indexOf('mac') != -1 && userAgent.indexOf('firefox')!=-1) {
					$("#nav-overlay").fadeIn("300").animate({top:"242px"}, {duration: 300});
				}
				else{
					$("#nav-overlay").fadeIn("300").animate({opacity:.4}, {duration: 300});}
					$("#nav-overlay").css({height: pageHeight - 242 + "px"});
			}, "10");
}

function browserHeight(){
	var windowHeight;
	
	if (self.innerHeight) { // all except Explorer
           windowHeight = self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
           windowHeight = document.documentElement.clientHeight;
    } else if (document.body) { // other Explorers
           windowHeight = document.body.clientHeight;
    }
    return windowHeight;
}

function hideOverlay(){
	var userAgent = navigator.userAgent.toLowerCase();
	if (userAgent.indexOf('mac') != -1 && userAgent.indexOf('firefox')!=-1) {
		$("#nav-overlay").fadeOut("200");
	}
	else {
		$("#nav-overlay").fadeOut("200").animate({opacity:0}, {duration: 200});
	}
}

function checkWindowHeight(){
	/*var theDiv = document.getElementById("expandable"); // change this to the name of the footer div
	if (document.documentElement.clientHeight >=  document.body.offsetHeight - 110){
		theDiv.className = "absolute";
	}
	if (document.documentElement.clientHeight <= document.body.offsetHeight -110){
		theDiv.className = "fixed";   
	}*/
}   

function checkBodyHeight(){
   /* t = setTimeout(
		function(){
		    var theDiv = document.getElementById("expandable"); // change this to the name of the footer div

			if (document.body.offsetHeight >= "830" ){
				theDiv.className = "fixed";    
			}
			if ( document.body.offsetHeight <= "830"){
				theDiv.className = "absolute";
			}

			checkWindowHeight();
		}, "50");*/
}

function resetSubscribeField(theForm){
	// if there is nothing in the text area, reset default text
	if (theForm.value.length < 1 ||theForm.subscribeEmail == ""){
		theForm.value = " your e-mail address";
	}			
}

function clearSubscribeField(theForm){
	if (theForm.value == " your e-mail address"){
		theForm.value = "";
	}
}
	
function validateSubscribe(theForm){
	// validate friends email
	if (theForm.subscribeEmail.value == ""){
		alert("Please enter your E-Mail address.");
		theForm.subscribeEmail.focus();
		return(false);
	}

	if (!isValidEmail(theForm.subscribeEmail.value)){
		theForm.subscribeEmail.focus();
		return(false);
	}
	
	return(true);
}

function validateMicrositeSubscribe(theForm){
	// validate friends email
	if (theForm.subscribeEmail.value == ""){
		alert("Please enter your E-Mail address.");
		theForm.subscribeEmail.focus();
		return(false);
	}

	if (!isValidEmail(theForm.subscribeEmail.value)){
		theForm.subscribeEmail.focus();
		return(false);
	}
	theForm.action = "/usa/optin.asp?email=" + theForm.subscribeEmail.value; 
	theForm.submit();
}




function validateUnsubscribe(theForm){
	// validate friends email
	if (theForm.unsubscribeEmail.value == "")
	{
		alert("Please enter your E-Mail address.");
		theForm.unsubscribeEmail.focus();
		return(false);
	}

	if (isValidEmail(theForm.unsubscribeEmail.value)){
	}
	else
	{	
		theForm.unsubscribeEmail.focus();
		return(false);
	}
	return(true);
}

function isValidEmail (emailStr){
	var emailPat=/^(.+)@(.+)$/
	var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]"
	var validChars="\[^\\s" + specialChars + "\]"
	var quotedUser="(\"[^\"]*\")"	
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/
	var atom=validChars + '+'
	var word="(" + atom + "|" + quotedUser + ")"
	var userPat=new RegExp("^" + word + "(\\." + word + ")*$")	
	var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$")
	var matchArray=emailStr.match(emailPat)

	if (matchArray==null){
		alert("Email address seems incorrect (check @ and .'s)")
		return false
	}

	var user=matchArray[1]
	var domain=matchArray[2]

	if (user.match(userPat)==null){
		alert("Your username (the part before the '@') doesn't seem to be valid.")
		return false
	}

	var IPArray=domain.match(ipDomainPat)
	if (IPArray!=null){
		for (var i=1;i<=4;i++){
			if (IPArray[i]>255){
				alert("Destination IP address is invalid!")
				return false
			}
		}
		return true
	}

	var	domainArray=domain.match(domainPat)
	if (domainArray==null){
		alert("The domain name doesn't seem to be valid.")
		return false
	}

	var atomPat=new RegExp(atom,"g")
	var domArr=domain.match(atomPat)
	var len=domArr.length
	if (domArr[domArr.length-1].length<2 || domArr[domArr.length-1].length>3){
		alert("The address must end in a three-letter domain, or two letter country.")
		return false
	}

	if (len<2){		
		alert("This address is missing a hostname.")
		return false
	}	

	return true;
}	


function validateTermsEmail(theForm){
var theAlert = '';

	if((theForm.termsConfirm.value == '') || ((theForm.termsConfirm.value != 'I AGREE') && (theForm.termsConfirm.value != 'I DISAGREE'))){
		theAlert += 'Please enter I AGREE or I DISAGREE\n';
		theForm.termsConfirm.focus();
	}

	if (theForm.termsEmail.value == ''){
		theAlert += 'Please enter your E-Mail address';
		theForm.termsEmail.focus();
	}
		
	if(theAlert){alert(theAlert);}
	
	if((theForm.termsEmail.value == '') || (theForm.termsConfirm.value == '') || ((theForm.termsConfirm.value != 'I AGREE') && (theForm.termsConfirm.value != 'I DISAGREE'))){
	return(false);
	}
	
	if (isValidEmail(theForm.termsEmail.value)){}
	else
	{	
		theForm.termsEmail.focus();
		return(false);
	}
	return(true);
}

function goToPage(pg) {
    dcsMultiTrack('DCS.dcsuri',pg,'DCS.dcsqry','','WT.ti',pg); 
    firstTracker._trackPageview(pg);
    secondTracker._trackPageview(pg);
}

function goToRegistration() {
    window.open("/usa/event.asp?action=entry&eventcode=biore2wksignup", "signup");
}

function getCopyrightYear(){
    var today = new Date();
    var year = today.getYear();
    if(year<1000) 
        year+=1900;
    return year;
}

/* Pore Profiler */
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function resetSubscribeField(theForm){
	// if there is nothing in the text area, reset default text
	if (theForm.value.length < 1 ||theForm.subscribeEmail == ""){
		theForm.value = " your e-mail address";
	}			
}

function validateProfilerSubscribe(theForm){
	// validate friends email
	//alert("got here");
	if (theForm.subscribeEmail.value == ""){
		alert("Please enter your E-Mail address.");
		theForm.subscribeEmail.focus();
		return(false);
	}

	if (!isValidEmail(theForm.subscribeEmail.value)){
		theForm.subscribeEmail.focus();
		return(false);
	}
	theForm.action = "/usa/optin.asp" 
	theForm.submit();
}
/* End Pore Profiler */

function detectMacFirefox2() {
  var ua = navigator.userAgent.toLowerCase();
  if (/firefox[\/\s](\d+\.\d+)/.test(ua)) {
    var firefox_version = new Number(RegExp.$1);
    if (firefox_version < 3 && ua.indexOf('mac') != -1) {
      return true;
    }
  }
  return false;
}

/* Disable/Enable checkboxes when "None of the Above" is checked/unchecked on forms */
function toggleCheckboxes(className){
  if($('#optinPart2 div.checkboxDiv span#'+className+'None input:checked').val() == 'on'){
    $('#optinPart2 div.checkboxDiv span.'+className+' input').attr('checked', false);
    $('#optinPart2 div.checkboxDiv span.'+className+' input').attr('disabled', true);        
  }
  else{
    $('#optinPart2 div.checkboxDiv span.'+className+' input').attr('disabled', false);              
  }
}