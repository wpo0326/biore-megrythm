function createLBdiv (id,vidWidth) {
  var lightboxDiv = $("<div></div>").attr({id: "lbVideo", style: "display: none;"}).addClass("videoHolder");

  var closeLink = $("<a></a>").attr("href", "#").addClass("video_close");
  lightboxDiv.append(closeLink);
  
  // Create div for video
  var videoDiv = $("<div></div>").attr({id: id, style: "padding-top:32px; height:100%;"})
  if (vidWidth == "640") {videoDiv.addClass("wide_video");} //Is it wideformat?
  lightboxDiv.append(videoDiv);
  

  //create noflash div and insert
  var noflashDiv = $("<div></div>").attr("id","noflash").addClass("no-flash");
  var noflashP1 = $("<p></p>").text("To see this video you need JavaScript enabled and the latest version of Flash.  You can also use a browser that supports H.264 HTML5 Video.").attr("style","padding:70px 100px 0px;");
  var noflashP2 = $("<p></p>").text(" to go to the flash download center.");
  noflashP2link = $("<a></a>").attr({href: "http://www.adobe.com/go/getflashplayer", target: "_blank"}).text("Click here");
  noflashP2.prepend(noflashP2link);
  noflashDiv.append(noflashP1).append(noflashP2);
  videoDiv.append(noflashDiv);

  //Insert lightboxDiv
  $("#container").append(lightboxDiv);
  return lightboxDiv;
}

function playVideo (videoURL, vidDuration, vidWidth, vidHeight, lbid, playerURL, optionalParamStr) { 
  // test function variable
  if (videoURL == "") {return false;}
  if (vidDuration == "") {vidDuration = "9999";}
  if (vidWidth == "") {vidWidth = "532";}
  if (vidHeight == "") {vidHeight = "400";}
  if (lbid == "") {lbid = "product_detail_flash_lbVideo";}
  if (playerURL == null || playerURL == "") {
    if (vidWidth == "532") {playerURL = "/usa/flash/FlashVideoPlayer.swf";}
    else {playerURL = "/usa/flash/FlashVideoPlayer_16x9.swf";}
  }
  if (optionalParamStr == null || optionalParamStr == "") {
    optionalParamStr = "";
  }

  
  var playerWidth = parseInt(vidWidth);
	// Add 22 for the height of the control bar in the Flash player, edit if the control bar height changes
	var playerHeight = parseInt(vidHeight) + 22;
	if (optionalParamStr == null || optionalParamStr == "") {
		optionalParamStr = "";
	}
	var flash1_contentVersion = "9.0.115";


  // get div if exists, create if it doesn't
  var lightboxDiv = $("#" + lbid);
  if(lightboxDiv.length == 0) {
    lightboxDiv = createLBdiv(lbid,vidWidth);
  }
        
  // setup SWFObject
	if (swfobject.hasFlashPlayerVersion(flash1_contentVersion)) {
		  var swfWidth = "532";
      var swfHeight = "423";
      if (vidWidth == "640") {
        swfWidth = "640";
        swfHeight = "383";
      }
      // setup SWFObject
      var mifso = new SWFObject(playerURL,"videoPlayer",swfWidth,swfHeight,"9.0.115","#FFFFFF");
      mifso.addParam("align", "middle");
      mifso.addParam("menu", "false");
      mifso.addParam("wmode", "transparent");
      if(navigator.plugins&&navigator.mimeTypes&&navigator.mimeTypes.length){
        mifso.addParam("allowscriptaccess", "sameDomain");
      }
      mifso.addParam("pluginspage", "http://www.macromedia.com/go/getflashplayer");
      mifso.addParam("type", "application/x-shockwave-flash");
      mifso.addParam("quality", "high");
      mifso.addVariable("videoUrl", videoURL);
      mifso.addVariable("videoWidth", vidWidth);
      mifso.addVariable("videoHeight", vidHeight);
      mifso.addVariable("videoDuration", vidDuration);
    
      // write the video object
      mifso.write(lbid);      
	} else if (supports_html5_h264_video()) {
  		//EMBED HTML5 Video Player
  		// NOTE that iPhone OS2 does not support HTML5 Video
  		var html5 = true;
  		var videoattributes = {};
  		videoattributes.src = videoURL;
  		videoattributes.width = vidWidth;
  		videoattributes.height = vidHeight;
  		videoattributes.controls = "true";
  		videoattributes.autoplay = "true";
  		videoattributes.style = "margin-top:35px; border:6px solid #9BCD9A;"
  		var video = $("<video></video>").attr(videoattributes);
  		lightboxDiv.html('');
  		var closeLink = $("<a></a>").attr("href", "#").addClass("video_close");
      lightboxDiv.append(closeLink);
      lightboxDiv.append(video);
      /*
  		lightboxDiv.append("<div id='product_detail_flash_lbVideo' class='wide_video'></div>");
  		$('#product_detail_flash_lbVideo').append(video);
  		*/
	}
  // open thickbox to display
  var tb_width = "587";
  var tb_height = "562";
  if (vidWidth == "640") {
    tb_width = "700";
    tb_height = "435";
  }
  if (vidWidth == "640" && html5) {
    tb_width = "700";
    tb_height = "407";
  }
	tb_show("","#TB_inline?height=" + tb_height + "&width=" + tb_width + "&inlineId=lbVideo" + optionalParamStr,"");
}


/***********************************************************************
supports_html5_h264_video tests the browser to see if it can
play HTML5_h264 video natively
***********************************************************************/
function supports_html5_h264_video(){
  //if Android, return true
  //if (detectAndroid()) return true;
  //if browser can't play video tag at all (IE), return false:
  if(!document.createElement('video').canPlayType){return false;}
  //if it can, check for mp4 type:
  else{
    var v = document.createElement("video");
    return !!v.canPlayType('video/mp4');
  }
 }

 /***********************************************************************
 detectAndroid returns true if the device is an Android device,
 based on the user agent string, false if not
 ***********************************************************************/
 function detectAndroid() {
 	var Android = ['android'];
 	var userAgent = navigator.userAgent.toLowerCase();
 	for (var i = 0; i < Android.length; i++) {
 		if (userAgent.indexOf(Android[i]) != -1) {
 			return true;
 		}
 	}
 	return false;
 }