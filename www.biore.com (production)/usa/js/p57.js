$(document).ready(function() {
    $('#scheduleTable table td a.detailsLink').click(function(e) {
        $('#scheduleTable table tr').removeClass('selected');
        $(this).parent().parent().addClass('selected');
        var mktNum = $(this).attr('id').replace('city', '');
        $("#detailsContainer").load("scheduleClass.aspx?mktID=" + mktNum + " #details", function() { colorbox(); });
        e.preventDefault();
    });
    colorbox();
    $('form#form1 a#cancel, form#form1 a#done').live('click', function(e) {
        e.preventDefault();
        parent.$.fn.colorbox.close();
    });    
});
function colorbox(){
    $(".registerLink a").click(function(e) {
        e.preventDefault();
        var mkt = $('#scheduleTable table tr.selected td.detailsLinkTD a').attr('id').replace('city', '');
        var theClass = $(this).attr('id').replace('ctl00_ContentPlaceHolder1_registerLink', '');
        $.fn.colorbox({
            iframe: true,
            width: 642, //+42
            height: 555, //+70
            href: "/usa/physique57tour/register.aspx?market=" + mkt + "&class=" + theClass,
            opacity: 0.75,
            onOpen: function() {$('html').css({ overflow: 'hidden' });}, //As colorbox begins loading
            /*onComplete: function() { //Once colorbox fully opens and loads all content
                $('input[type="submit"]').hide();
                $('a#submit').show();
                $('form#form1 a#submit').click(function(e) {
                    e.preventDefault();
                    var email = $('input#Email').val();
                    var email2 = $('input#Email2').val();
                    var fname = $('input#FName').val();
                    var lname = $('input#LName').val();
                    var noErrors = true;
                    if (fname == "") { $('#FNameValidator1').show(); noErrors = false; } else { noErros = true; }
                    if (lname == "") { $('#LNameValidator1').show(); noErrors = false; } else { noErros = true; }
                    if (email == "") { $('#EmailValidator1').show(); noErrors = false; } else { noErros = true; }
                    if (email != "" && email != email2) { $('#EmailCompare').show(); noErrors = false; } else { noErros = true; }
                    if (noErrors) {
                        $.getJSON('register.ashx?market=' + mkt + '&clss=' + theClass + '&email=' + email + '&fname=' + fname + '&lname=' + lname, function(data) {
                            if (data == "-1") { $('form#form1 #formInnerContainer').addClass('responseMsg').html('We\'re sorry, your email address is already signed up for a class.'); }
                            else if (data == "0") { $('form#form1 #formInnerContainer').addClass('responseMsg').html('We\'re sorry, the class you are signing up for is already full.'); }
                            else { $('form#form1 #formInnerContainer').addClass('responseMsg').html('Thank you for signing up.'); }
                        });
                    }
                });
            },*/
            onCleanup: function() { $('html').css({ overflow: 'auto' }); } //As colorbox begins closing
        });
    });
}
function gup(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return results[1];
}

function openRules() {
    $.fn.colorbox({
        iframe: true,
        width: 710,
        height: 555,
        href: "/usa/physique57tour/rules.aspx",
        opacity: 0.75,
        onOpen: function() { $('html').css({ overflow: 'hidden' }); }, //As colorbox begins loading
        onCleanup: function() { $('html').css({ overflow: 'auto' }); } //As colorbox begins closing
    });
        
    //var myRules;
    //myRules = window.open("/usa/physique57tour/rules.aspx", "Official Rules", "status=0,toolbar=0,menubar=0,directories=0,width=620,scrollbars=1");
}

function openWaiver() {
    $.fn.colorbox({
        iframe: true,
        width: 710,
        height: 555,
        href: "/usa/physique57tour/waiver.aspx",
        opacity: 0.75,
        onOpen: function() { $('html').css({ overflow: 'hidden' }); }, //As colorbox begins loading
        onCleanup: function() { $('html').css({ overflow: 'auto' }); } //As colorbox begins closing
    });
    
    //var myRules;
    //myRules = window.open("/usa/physique57tour/waiver.aspx", "Waiver", "status=0,toolbar=0,menubar=0,directories=0,width=620,scrollbars=1");
}