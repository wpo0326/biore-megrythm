$(document).ready(function() {	
		var $mainTabs = $("#tabbed_content > ul").tabs(); 
		var $productTabs = $("#how_it_works > ul").tabs(); 
		var theHash = document.location.hash;
		$productTabs.tabs('select', theHash);
		/*var theLocation = window.location.href;
		var theSelection = theLocation.split('#');*/
		/*switch(theSelection[1]){
			case 'pore_unclogging':
				$productTabs.tabs('select', 1);
				window.location.href = theSelection[0];
			break;
			case 'triple_action':
				$productTabs.tabs('select', 2);	
				window.location.href = theSelection[0];
			break;
			case 'blemish_fighting':
				$productTabs.tabs('select', 3);
				window.location.href = theSelection[0];
			break;
			case 'deep_cleansing':
				$productTabs.tabs('select', 4);
				window.location.href = theSelection[0];
			break;
		}*/
		
		/*$("#people_are_saying a").click(function() {$mainTabs.tabs('select', 1); return false;});*/
		$(".scrub").click(function() { $productTabs.tabs('select', 1);	return false;});
		$(".cleanser").click(function() { $productTabs.tabs('select', 2);	return false;});
		$(".toner").click(function() { $productTabs.tabs('select', 3);	return false;});
		$(".pore_strips").click(function() { $productTabs.tabs('select', 4);	return false;});
		
		if (navigator.appName == 'Microsoft Internet Explorer'){
			$("#where_to_buy img").attr("style", "filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=crop,src='/usa/images/common/buy_now.png')");
			$("#where_to_buy img").attr("src", "/usa/images/common/blank.gif");
		}

		$("#where_to_buy_wrapper").hoverIntent(
			function(){
				$("#where_to_buy span").fadeOut();
				$("#shadow img").fadeOut();
				$("#where_to_buy").animate({top:"20px"}, 200);
			},
			
			function(){
				$("#where_to_buy span").fadeIn();
				$("#shadow img").fadeIn();
				$("#where_to_buy").animate({top:"0px"}, 200);
			}
		);
				
		function youTubeDef (imgSource, sourceText, link, comment) {
			this.imgSource = imgSource;
			this.sourceText = sourceText;
			this.link = link;
			this.comment = comment;
		}
		
		function twitterDef (sourceText, link, comment) {
			this.sourceText = sourceText;
			this.link = link;
			this.comment = comment;
		}
		
		function flikrDef(imgSource, sourceText) {
			this.imgSource = imgSource;
			this.sourceText = sourceText;
		}
			
		function randomize(numberOfItems){
			var itemToRandomize = Math.random() * numberOfItems;
			itemToRandomize = Math.round(itemToRandomize);
			return itemToRandomize;
		}
		
		var flikr = new Array();
		flikr[0] = new flikrDef("/usa/images/instantlyClear/latest_buzz/flickr_1.jpg","source: Flickr/crayolamom");
		flikr[1] = new flikrDef("/usa/images/instantlyClear/latest_buzz/flickr_2.jpg","source: Flickr/chocokat718");
		flikr[2] = new flikrDef("/usa/images/instantlyClear/latest_buzz/flickr_1.jpg","source: Flickr/user2043589");
		flikr[3] = new flikrDef("/usa/images/instantlyClear/latest_buzz/flickr_2.jpg","source: Flickr/user43589");
		
		var flikrArray = flikr.length - 1;
		flickrRandom1 = randomize(flikrArray);
		flickrRandom2 = randomize(flikrArray);
		if(flickrRandom1 == flickrRandom2 && flickrRandom1 != 0){flickrRandom2 = flickrRandom2 - 1;}
		else if(flickrRandom1 == flickrRandom2 && flickrRandom1 == 0){flickrRandom2 = flickrRandom2 + 1;}
		
		$(
			'<img src="' + flikr[flickrRandom1].imgSource + '" alt="" border="0" />' + 
			'<p class="source">' + flikr[flickrRandom1].sourceText + '</p>' 
		).appendTo("#column1 .flickr");
		
		$(
			'<img src="' + flikr[flickrRandom2].imgSource + '" alt="" border="0" />' + 
			'<p class="source">' + flikr[flickrRandom2].sourceText + '</p>' 
		).appendTo("#column2 .flickr");
		
		
		var youTube = new Array();
		youTube[0] = new youTubeDef("/usa/images/instantlyClear/latest_buzz/youtube_1.jpg","source: youtube/pingjeepong", "http://www.youtube.com", "Watch me pull out my blackheads!");
		youTube[1] = new youTubeDef("/usa/images/instantlyClear/latest_buzz/youtube_1.jpg","source: youtube/kford", "http://www.youtube.com", "This is a test!");
		youTube[2] = new youTubeDef("/usa/images/instantlyClear/latest_buzz/youtube_1.jpg","source: youtube/user8978934", "http://www.youtube.com", "Watch me try out biore!" );
		youTube[3] = new youTubeDef("/usa/images/instantlyClear/latest_buzz/youtube_1.jpg","source: youtube/tester", "http://www.youtube.com", "wow, this is cool!");
		
		var youTubeArray = youTube.length - 1;
		var youTubeRandom = randomize(youTubeArray);
		
		$(			
			'<a href="'+ youTube[youTubeRandom].link +'" target="_blank">' + 
			'<img src="' + youTube[youTubeRandom].imgSource + '" alt="" border="0" />' + 
			'</a>' +
			'<a href="'+ youTube[youTubeRandom].link +'" target="_blank" class="you_tube_link">' + 
			youTube[youTubeRandom].comment +
			'</a>' +
			'<p class="source">' + youTube[youTubeRandom].sourceText + '</p>' 
		).appendTo("#column1 .you_tube");
		
		var twitter = new Array();
		twitter[0] = new twitterDef("source: twitter/@nikoleapril", "http://www.twitter.com", "Is there an addiction hotline for Biore pore strips?? I've made an entire mask of them. Best product ever! (via @nikoleapril)");
		twitter[1] = new twitterDef("source: twitter/@JoiseyDani", "http://www.twitter.com", "just ripped a Biore' strip off my nose...man is my nose dirty! (via @JoiseyDani)");
		twitter[2] = new twitterDef("source: twitter/@user8978934", "http://www.twitter.com", "Watch me try out biore!  (via @user8978934)" );
		twitter[3] = new twitterDef("source: twitter/@tester", "http://www.twitter.com", "wow, this is cool!  (via @tester)");
		
		var twitterArray = twitter.length - 1;
		var twitterRandom1 = randomize(twitterArray);
		var twitterRandom2 = randomize(twitterArray);
		if(twitterRandom1 == twitterRandom2 && twitterRandom1 != 0){twitterRandom2 = twitterRandom2 - 1;}
		else if(twitterRandom1 == twitterRandom2 && twitterRandom1 == 0){twitterRandom2 = twitterRandom2 + 1;}
		
		$(			
			'<a href="'+ twitter[twitterRandom1].link +'" target="_blank" class="twitter_link">' + 
			twitter[twitterRandom1].comment +
			'</a>' +
			'<p class="source">' + twitter[twitterRandom1].sourceText + '</p>' 
		).appendTo("#column1 .twitter_bubble");
		
		$(			
			'<a href="'+ twitter[twitterRandom2].link +'" target="_blank" class="twitter_link">' + 
			twitter[twitterRandom2].comment +
			'</a>' +
			'<p class="source">' + twitter[twitterRandom2].sourceText + '</p>' 
		).appendTo("#column2 .twitter_bubble");
	});