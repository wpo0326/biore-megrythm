$(document).ready(function() {
	$(".navMenu ul").hide();
    $(".global_nav li a.global_anchor").css( {backgroundPosition: "0 0"} );

    for (var i=1; i<6; i++){

		var selectedNav;
		var selectedNavNo;

		var config = {
			//interval:10,
			//sensitivity:10,
			over:navItemCheck,
			out:navItemClose
		};

		$("#globalNav_" + i).hoverIntent(config);
	 }

	 function navItemCheck(){
	 	navItemOver($(this));
	 }

	 function navItemOver(hoveredNav){
		if(hoveredNav){
            selectedNav = hoveredNav.attr("id");
        }
		else{
            selectedNav = $(this).attr("id");
        }
		selectedNavNo = selectedNav.split("_");

		if($("#globalNav_" + selectedNavNo[1] + " a").attr("class") != "global_anchor active"){
			$("#globalNav_" + selectedNavNo[1] + " a.global_anchor").animate({backgroundPosition:"(0px -34px)"},{duration:300});
		}

        if((selectedNav == "globalNav_1") || (selectedNav == "globalNav_4")){
		//if((selectedNav == "globalNav_1") && ($("#globalNav_" + selectedNavNo[1] + " a").attr("class") != "global_anchor active")){
           $("#globalNav_" + selectedNavNo[1] + " ul").slideDown(300);
           //$("#globalNav_" + selectedNavNo[1] + " ul").css("visibility","visible");
        }
    }

	function navItemClose(){
        selectedNav = $(this).attr("id");
		selectedNavNo = selectedNav.split("_");
		$("#globalNav_" + selectedNavNo[1] + " a.global_anchor").animate({backgroundPosition:"(0px 0px)"},{duration:300});

        $("#globalNav_" + selectedNavNo[1] + " ul").slideUp(300);
    }
});