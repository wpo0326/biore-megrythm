<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<%
Option Explicit
Dim section, pageTitle, pageDescription, pageKeywords
section = "Instantly Clear"
pageTitle = "Bior&eacute;&reg; Instantly Clearer Pores Products"
pageDescription = "Find out the Bior�&reg; products that are clinically proven to instantly clear pores 68% better than a leading competitor."
pageKeywords = "Bior�, Biore, instantly clear, instantly clearer pores, skin care, inbetween skin, blemish control, astringent, blemish fighter, blemish control, breakout control, acne fighter, proactive, triple action, complexion, complexion clearing, acne, pimples, pores, pore strip, pore unclogging scrub, cleansers"
bodyID = "instantlyClear"
%>
<!--#include virtual="/usa/includes/header.asp"-->
<script type="text/javascript" src="/usa/js/jquery-ui-personalized-1.6rc2.min.js"></script>
<script type="text/javascript" src="/usa/js/ICUtils.js"></script>
<div id="middle">
	<div id="middle_content">
		<div id="product_info">
			<h1>Instantly Clearer Pores - 3 super-simple steps to better skin.</h1>
			<h2>Discover out super simple way to better skin! It's clinically proven to give you <strong>instantly</strong> clearer pores, 68% <strong>better than Proactiv&reg;.*</strong></h2>
			<img src="/usa/images/instantlyClear/product_grouping.jpg" alt="" />
			<p>*System of Bior&eacute;&reg; Blemish Fighting Ice Cleanser or Pore Unclogging Scrub + Pore Strips + Triple Action Astringent vs.  3-Piece Proactiv&reg; System. Proactiv&reg; is a registered trademark of Guthy-Renker&reg; Corporation.</p>
		</div>
		<div id="where_to_buy_wrapper">
			<div id="where_to_buy">
				<a id="instantlyclearerbutton" href="http://www.drugstore.com/templates/brand/default.asp?brand=8696&trx=SEARCH-BNAV-0-BSTORE&trxp2=8696" target="_blank" ><span><img src="/usa/images/common/buy_now.png" alt="" /></span></a>
			</div>
			<div id="shadow">
				<img src="/usa/images/instantlyClear/where_to_buy_shadow.jpg" alt="" />
			</div>
		</div>
		
		<div id="tabbed_content">
			<!--
			TO RESTORE TABS: in biore.css change #product_tab_nav to top:370px
			<ul id="tab_nav">
				<li id="how_it_works_nav"><a href="#how_it_works">How It Works</a></li>
				<li id="latest_buzz_nav"><a href="#latest_buzz_wrapper">The Latest Buzz</a></li>
			</ul>
			-->
			<div id="how_it_works">
				<ul id="product_tab_nav">
					<li id="intro_nav"><a href="#intro"></a></li>
					<li id="pore_unclogging_nav"><a href="#pore_unclogging"></a></li>
					<li id="blemish_fighting_nav"><a href="#blemish_fighting"></a></li>
					<li id="triple_action_nav"><a href="#triple_action"></a></li>
					<li id="deep_cleansing_nav"><a href="#deep_cleansing"></a></li>
				</ul>
				<div id="intro">
					<div class="overflow_hidden">
						<img src="/usa/images/instantlyClear/how_it_works/scrub_cleanser.jpg" alt="Pore Unclogging Scrub or Blemish Fighting Ice Cleanser" />
						<span class="plus">+</span>
						<img src="/usa/images/instantlyClear/how_it_works/toner.jpg" alt="Triple Action Astringent" />
						<span class="plus">+</span>
						<img src="/usa/images/instantlyClear/how_it_works/pore_strips.jpg" alt="Deep Cleansing Pore Strips" />
						<span class="equals">= Instantly Clearer Pores</span>
					</div>
					<p id="combination">Great separately. Even better in combination.</p>
					<p id="people_are_saying"><a href="http://www.facebook.com/bioreskin" target="_blank">See what people are saying! Fan our Facebook page. &gt;&gt;</a></p>
					<p id="click_product">Click product names below to get more detail:</p>
					
				</div>
				<div id="pore_unclogging" class="IC_prod_details">
					<div class="img_holder"><img src="/usa/images/instantlyClear/how_it_works/pore_unclogging_img.jpg" alt="" /></div>
					<div class="box_holder">
						<h1>Pore Unclogging Scrub</h1>
						<div class="details">
							<p>Detoxify your skin as you gently scrub away dead cells and surface toxins. You'll see and feel a smooth and vibrant complexion - instantly!</p>
							<p class="smaller"><strong>Added bonus:</strong> the scrub penetrates deep down where breakouts start to help prevent future ones.</p>
							<p class="learn_more"><a href="/usa/products/complexion_clearing_detail.asp?productid=4">Learn more</a></p>
						</div>
						<div class="boxes">
							<div class="time">
								<span class="one_minute">> 1 minute</span>
								<span class="use_daily">Use Daily</span>
							</div>
							<div class="works_great_with">
								<h2>Works great with:</h2>
								<div class="prod_img">
									<img src="/usa/images/instantlyClear/how_it_works/toner_small_img.jpg" alt="toner" /><br />
									<a href="#" class="toner">astringent</a>
								</div>
								<span class="plus">+</span>
								<div class="prod_img">
									<img src="/usa/images/instantlyClear/how_it_works/pore_strips_small_img.jpg" alt="pore strips" /><br />
									<a href="#" class="pore_strips">pore strips</a>
								</div>
							</div>
						</div>
						<p class="get_detail">Click product names below to get more detail:</p>
					</div>
				</div>
				<div id="blemish_fighting" class="IC_prod_details">
					<div class="img_holder"><img src="/usa/images/instantlyClear/how_it_works/blemish_fighting_img.jpg" alt="" /></div>
					<div class="box_holder">
						<h1>Blemish Fighting Ice Cleanser</h1>
						<div class="details">
							<p>Energize your skincare routine with a gel cleanser that invigorates as it gets deep down to help prevent breakouts waiting to happen. Battle blemishes without drying out the rest of your face.</p>
							<p class="learn_more"><a href="/usa/products/complexion_clearing_detail.asp?productid=2">Learn more</a></p>
						</div>
						<div class="boxes">
							<div class="time">
								<span class="one_minute">> 1 minute</span>
								<span class="use_daily">Use Daily</span>
							</div>
							<div class="works_great_with">
								<h2>Works great with:</h2>
								<div class="prod_img">
									<img src="/usa/images/instantlyClear/how_it_works/toner_small_img.jpg" alt="toner" /><br />
									<a href="#" class="toner">astringent</a>
								</div>
								<span class="plus">+</span>
								<div class="prod_img">
									<img src="/usa/images/instantlyClear/how_it_works/pore_strips_small_img.jpg" alt="pore strips" /><br />
									<a href="#" class="pore_strips">pore strips</a>
								</div>
							</div>
						</div>
						<p class="get_detail">Click a product to get more detail:</p>
					</div>
				</div>
				<div id="triple_action" class="IC_prod_details">
					<div class="img_holder"><img src="/usa/images/instantlyClear/how_it_works/triple_action_img.jpg" alt="" /></div>
					<div class="box_holder">
						<h1>Triple Action Astringent</h1>
						<div class="details">
							<p>What's the triple action?</p>
							<ol>
								<li><span>1.</span><p>Unclog pores of dirt and build-up.</p></li>
								<li><span>2.</span><p>Tighten pores so they look smaller.</p></li>
								<li class="last"><span>3.</span><p>Penetrate to prevent future breakouts.<br /><br />All in a few quick whisks of a cotton ball that leave skin feeling refreshed and tingly (but not dried out).</p></li>
							</ol> 
							<p class="learn_more"><a href="/usa/products/complexion_clearing_detail.asp?productid=5">Learn more</a></p>
						</div>
						<div class="boxes">
							<div class="time">
								<span class="one_minute">> 1 minute</span>
								<span class="use_daily">Use Daily</span>
							</div>
							<div class="works_great_with">
								<h2>Works great with:</h2>
								<div class="prod_img">
									<img src="/usa/images/instantlyClear/how_it_works/cleanser_small_img.jpg" alt="toner" /><br />
									<a href="#" class="cleanser">cleanser</a> or <a href="#" class="scrub">scrub</a>
								</div>
								<span class="plus">+</span>
								<div class="prod_img">
									<img src="/usa/images/instantlyClear/how_it_works/pore_strips_small_img.jpg" alt="pore strips" /><br />
									<a href="#" class="pore_strips">pore strips</a>
								</div>
							</div>
						</div>
						<p class="get_detail">Click a product to get more detail:</p>
					</div>
				</div>
				<div id="deep_cleansing" class="IC_prod_details">
					<div class="img_holder"><img src="/usa/images/instantlyClear/how_it_works/deep_cleansing_img.jpg" alt="" /></div>
					<div class="box_holder">
						<h1>Blemish Fighting Ice Cleanser</h1>
						<div class="details">
							<p>Ready, get set, strip! If you've used Pore Strips, you know what's next: the unbelievably weird stuff that used to be in your pores! The nothing-quite-like-it treatment has to be experienced to be fully appreciated. It's instant gratification of the very best kind, and your skin will LOVE you for it.</p>
							<p class="learn_more"><a href="/usa/products/pore_strips_detail.asp?productid=9">Learn more</a></p>
						</div>
						<div class="boxes">
							<div class="time">
								<span class="ten_minutes">10 minutes</span>
								<span class="weekly_use">Weekly use</span>
							</div>
							<div class="works_great_with">
								<h2>Works great with:</h2>
								<div class="prod_img">
									<img src="/usa/images/instantlyClear/how_it_works/cleanser_small_img.jpg" alt="toner" /><br />
									<a href="#" class="cleanser">cleanser</a> or <a href="#" class="scrub">scrub</a>
								</div>
								<span class="plus">+</span>
								<div class="prod_img">
									<img src="/usa/images/instantlyClear/how_it_works/toner_small_img.jpg" alt="pore strips" /><br />
									<a href="#" class="toner">astringent</a>
								</div>
							</div>
						</div>
					</div>
					<p class="get_detail">Click a product to get more detail:</p>
				</div>
			</div>
			<div id="latest_buzz_wrapper">
				<div id="latest_buzz_inner_wrapper">
					<div id="latest_buzz">
						<h1 id="others_are_saying">What others are saying</h1>
						<h2 id="others_are_saying_second_line">Ma quande lingues coalesce, li grammatica del resultant lingue es�</h2>
						<div id="column_holder">
							<div id="column1">
								<div class="you_tube">
									<!--<a href="" target="_blank"><img src="/usa/images/instantlyClear/latest_buzz/youtube_1.jpg" alt="" border="0" /></a>
									<a href="" target="_blank" class="you_tube_link">Watch me pull out my blackheads!</a>
									<p class="source">source: youtube/pingjeepong</p>-->
								</div>
								<div class="twitter_bubble_wrapper">
									<div class="twitter_bubble_inner_wrapper">
										<div class="twitter_bubble">
										<!--<a href="" target="_blank" class="twitter_link">just ripped a Biore' strip off my nose...man is my nose dirty! (via @JoiseyDani)</a>
											<p class="source">source: twitter/@JoiseyDani</p>-->
										</div>
									</div>
								</div>
								<div class="flickr">
									<!--<img src="/usa/images/instantlyClear/latest_buzz/flickr_1.jpg" alt="" />
									<p class="source">source: Flickr/crayolamom</p>-->
								</div>
							</div>
							<div id="column2">
								<div class="twitter_bubble_wrapper">
									<div class="twitter_bubble_inner_wrapper">
										<div class="twitter_bubble">
											<!--<a href="" target="_blank" class="twitter_link">Is there an addiction hotline for Biore pore strips?? I've made an entire mask of them. Best product ever! (via @nikoleapril)</a>
											<p class="source">source: twitter/@nikoleapril</p>-->
										</div>
									</div>
								</div>
								<div class="flickr">
									<!--<img src="/usa/images/instantlyClear/latest_buzz/flickr_2.jpg" alt="" />
									<p class="source">source: Flickr/chocokat718</p>-->
								</div>
								<div class="blog_wrapper">
									<div class="blog_inner_wrapper">
										<div class="blog">
											<a href="" target="_blank">Biore Blemish Fighting Ice Cleanser AND Biore 4 in 1 Self Foaming Cleanser/Daily Recharge</a>
											<p>"I purchased Biore's Blemish Fighting Ice Cleanser and Biore's 4 in 1 self foaming cleanser/daily recharge TWO days ago. Thus, I've used them 4x. I am in my 20's and have moderate acne. These products are wonderful so far..."
											<p class="source">source: Reviewstream.com</p>
										</div>
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<p id="submit_your_own"><a href="/usa/instantlyClear/submit_buzz.asp?height=450&width=650modal=true" class="thickbox">Submit your own</a></p>
					</div>
					<p id="terms_links"><a href="">User Generated Content Terms &amp; Conditions</a> &nbsp; | &nbsp; <a href="">Copyright Infringement Notification</a></p>
				</div>
			</div>
		</div>
	</div>
</div>
<!--#include virtual="/usa/includes/footer.asp"-->