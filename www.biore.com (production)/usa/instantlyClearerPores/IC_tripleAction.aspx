<%--<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">--%>
<div id="ICP_instantlyClear">
  <div id="ICP_top"><a class="ICP_close" href="#"></a></div>
    <div id="ICP_middle_content">
		<div id="ICP_product_info">
			<h1>The Complete Bior&eacute;&reg; Clean.</h1>
			<h2>Pair a cleanser with Pore Strips for a powerful deep clean.</h2>
			<img src="/usa/images/instantlyClear/product_grouping.jpg" alt="" />
		</div>
		
		<div id="ICP_tabbed_content" class="ICP_triple_action">
			<ul id="ICP_product_tab_nav"><li id="ICP_intro_nav"><a href="/usa/instantlyClearerPores/IC_intro.aspx?height=605&amp;width=967" class="thickbox"></a></li>
				<li id="ICP_pore_unclogging_nav"><a href="/usa/instantlyClearerPores/IC_poreUnclogging.aspx?height=605&amp;width=967" class="thickbox"></a></li>
				<li id="ICP_blemish_fighting_nav"><a href="/usa/instantlyClearerPores/IC_blemishFighting.aspx?height=605&amp;width=967" class="thickbox"></a></li>
				<li id="ICP_deep_cleansing_nav"><a href="/usa/instantlyClearerPores/IC_deepCleansing.aspx?height=605&amp;width=967" class="thickbox"></a></li>
			</ul>
			<div id="ICP_triple_action" class="ICP_IC_prod_details">
				<div class="ICP_img_holder"><img src="/usa/images/instantlyClear/prod_details/triple_action_img.jpg" alt="" /></div>
				<div class="ICP_box_holder">
					<h1>Triple Action Astringent</h1>
					<div class="ICP_details">
						<p>What's the triple action?</p>
						<ol>
							<li><span>1.</span><p>Unclog pores of dirt and build-up.</p></li>
							<li><span>2.</span><p>Tighten pores so they look smaller.</p></li>
							<li class="ICP_last"><span>3.</span><p>Penetrate to prevent future breakouts.</p></li>
						</ol>
						<p>All in a few quick whisks of a cotton ball that leave skin feeling refreshed and tingly (but not dried out).</p>						
						<p class="ICP_learn_more_btn"><a href="/usa/products/product_detail.aspx?pid=9">Learn more</a></p>
					</div>
					<div class="ICP_boxes">
						<div class="ICP_time">
							<span class="ICP_one_minute">> 1 minute</span>
							<span class="ICP_use_daily">Use Daily</span>
						</div>
      <div class="ICP_works_great_with">
							<h2>Works great with:</h2>
							<div class="ICP_prod_img">
								<img src="/usa/images/instantlyClear/prod_details/cleanser_small_img.jpg" alt="toner" /><br />
								<a href="/usa/instantlyClearerPores/IC_blemishFighting.aspx?height=605&amp;width=967" class="thickbox ICP_cleanser">cleanser</a> or <a href="/usa/instantlyClearerPores/IC_poreUnclogging.aspx?height=605&amp;width=967" class="thickbox ICP_scrub">scrub</a>
							</div>
							<span class="ICP_plus">+</span>
							<div class="ICP_prod_img">
								<img src="/usa/images/instantlyClear/prod_details/pore_strips_small_img.jpg" alt="pore strips" /><br />
								<a href="/usa/instantlyClearerPores/IC_deepCleansing.aspx?height=605&amp;width=967" class="thickbox ICP_pore_strips">pore strips</a>
							</div>
						</div>
					</div>
					<p class="ICP_get_detail">Click a product to get more detail:</p>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(
	        function(){
		        $("a.ICP_close").click(function(e){
				        e.preventDefault();
				        tb_remove();
		        });
	        });
  </script>
<%--</asp:Content>--%>