<%--<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">--%>
<div id="ICP_instantlyClear">
  <div id="ICP_top"><a class="ICP_close" href="#"></a></div>
    <div id="ICP_middle_content">
		<div id="ICP_product_info">
			<h1>The Complete Bior&eacute;&reg; Clean.</h1>
			<h2>Pair a cleanser with Pore Strips for a powerful deep clean.</h2>
			<img src="/usa/images/instantlyClear/product_grouping.jpg" alt="" />
		</div>
		
		<div id="ICP_tabbed_content" class="ICP_deep_cleansing">
			<ul id="ICP_product_tab_nav"><li id="ICP_intro_nav"><a href="/usa/instantlyClearerPores/IC_intro.aspx?height=605&amp;width=967" class="thickbox"></a></li>
				<li id="ICP_pore_unclogging_nav"><a href="/usa/instantlyClearerPores/IC_poreUnclogging.aspx?height=605&amp;width=967" class="thickbox"></a></li>
				<li id="ICP_blemish_fighting_nav"><a href="/usa/instantlyClearerPores/IC_blemishFighting.aspx?height=605&amp;width=967" class="thickbox"></a></li>
				<li id="ICP_deep_cleansing_nav"><a href="/usa/instantlyClearerPores/IC_deepCleansing.aspx?height=605&amp;width=967" class="thickbox"></a></li>
			</ul>
			<div id="ICP_deep_cleansing" class="ICP_IC_prod_details">
				<div class="ICP_img_holder"><img src="/usa/images/instantlyClear/prod_details/deep_cleansing_img.jpg" alt="" /></div>
				<div class="ICP_box_holder">
					<h1>Blemish Fighting Ice Cleanser</h1>
					<div class="ICP_details">
						<p>Ready, set, strip! It's instant gratification of the very best kind. Combine this weekly treatment with one of the recommended cleansers for the Complete Bior&eacute;&reg; Clean!</p>
                        <p class="ICP_smaller"><strong>Twice as effective as the leading pore cleanser* at getting rid of pore-clogging build-up and blackheads.</strong></p>
						<p class="ICP_buy_now_btn"><a href="http://www.drugstore.com/qxp92370/biore/deep_cleansing_pore_strips.htm?aid=328279&aparm=PS_DeepClean" target="_blank">Buy Now</a></p>
					</div>
					<div class="ICP_boxes">
						<div class="ICP_time">
							<span class="ICP_ten_minutes">10 minutes</span>
							<span class="ICP_weekly_use">Weekly use</span>
						</div>
						<div class="ICP_works_great_with">
							<h2>Works great with:</h2>
							<div class="ICP_prod_img">
								<img src="/usa/images/instantlyClear/prod_details/cleanser_small_img.jpg" alt="Blemish Fighting Ice Cleanser" /><br />
                                <a href="/usa/instantlyClearerPores/IC_blemishFighting.aspx?height=605&amp;width=967" class="thickbox ICP_cleanser">cleanser</a>
							</div>
							<span class="ICP_plus">or</span>
							<div class="ICP_prod_img">
								<img src="/usa/images/instantlyClear/prod_details/scrub_small_img.jpg" alt="Pore Unclogging Scrub" /><br />
                                <a href="/usa/instantlyClearerPores/IC_poreUnclogging.aspx?height=605&amp;width=967" class="thickbox ICP_scrub">scrub</a>
							</div>
						</div>
					</div>
				</div>
				<p class="ICP_get_detail">Click a product to get more detail:</p>
			</div>
		</div>
		<p class="ICP_disclaimer">*per AC Nielsen data as of 4/25/2010</p>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(
	        function(){
		        $("a.ICP_close").click(function(e){
				        e.preventDefault();
				        tb_remove();
		        });
	        });
  </script>
<%--</asp:Content>--%>