<%--<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">--%>
<div id="ICP_instantlyClear">
  <div id="ICP_top"><a class="ICP_close" href="#"></a></div>
  <div id="ICP_middle_content">
		<div id="ICP_product_info">
			<h1>The Complete Bior&eacute;&reg; Clean.</h1>
			<h2>Pair a cleanser with Pore Strips for a powerful deep clean.</h2>
			<img src="/usa/images/instantlyClear/product_grouping.jpg" alt="" />
		</div>

		<div id="ICP_tabbed_content" class="ICP_intro">
			<ul id="ICP_product_tab_nav">
				<li id="ICP_intro_nav"><a href="/usa/instantlyClearerPores/IC_intro.aspx?height=605&amp;width=967" class="thickbox"></a></li>
				<li id="ICP_pore_unclogging_nav"><a href="/usa/instantlyClearerPores/IC_poreUnclogging.aspx?height=605&amp;width=967" class="thickbox"></a></li>
				<li id="ICP_blemish_fighting_nav"><a href="/usa/instantlyClearerPores/IC_blemishFighting.aspx?height=605&amp;width=967" class="thickbox"></a></li>
				<li id="ICP_deep_cleansing_nav"><a href="/usa/instantlyClearerPores/IC_deepCleansing.aspx?height=605&amp;width=967" class="thickbox"></a></li>
			</ul>
			<div id="ICP_intro">
				<div class="ICP_overflow_hidden">
					Pore Unclogging Scrub or Blemish Fighting Ice Cleanser + Deep Cleansing Pore Strips = The Complete Bior&eacute;&reg; Clean.
				</div>
				<p id="ICP_tagline">Bior&eacute;&reg; products give you the deepest clean.</p>
				<p id="ICP_model_quote">2 super-simple steps to better skin!</p>
				<p id="ICP_click_product">Click product names below to get more detail:</p>
		  </div>
	  </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(
	        function(){
		        $("a.ICP_close").click(function(e){
				        e.preventDefault();
				        tb_remove();
		        });
	        });
  </script>
<%--</asp:Content>--%>