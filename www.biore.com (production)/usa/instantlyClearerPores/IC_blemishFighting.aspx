<%--<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">--%>
<div id="ICP_instantlyClear">
  <div id="ICP_top"><a class="ICP_close" href="#"></a></div>
  <div id="ICP_middle_content">
    <div id="ICP_product_info">
      <h1>The Complete Bior&eacute;&reg; Clean.</h1>
			<h2>Pair a cleanser with Pore Strips for a powerful deep clean.</h2>
			<img src="/usa/images/instantlyClear/product_grouping.jpg" alt="" />
		</div>
		
		<div id="ICP_tabbed_content" class="ICP_blemish_fighting">
			<ul id="ICP_product_tab_nav"><li id="ICP_intro_nav"><a href="/usa/instantlyClearerPores/IC_intro.aspx?height=605&amp;width=967" class="thickbox"></a></li>
				<li id="ICP_pore_unclogging_nav"><a href="/usa/instantlyClearerPores/IC_poreUnclogging.aspx?height=605&amp;width=967" class="thickbox"></a></li>
				<li id="ICP_blemish_fighting_nav"><a href="/usa/instantlyClearerPores/IC_blemishFighting.aspx?height=605&amp;width=967" class="thickbox"></a></li>
				<li id="ICP_deep_cleansing_nav"><a href="/usa/instantlyClearerPores/IC_deepCleansing.aspx?height=605&amp;width=967" class="thickbox"></a></li>
			</ul>
			<div id="ICP_blemish_fighting" class="ICP_IC_prod_details">
        <div class="ICP_img_holder"><img src="/usa/images/instantlyClear/prod_details/blemish_fighting_img.jpg" alt="" /></div>
        <div class="ICP_box_holder">
					<h1>Blemish Fighting Ice Cleanser</h1>
					<div class="ICP_details">
                        <p>Battle blemishes without drying out the rest of your face. This energizing gel cleanser invigorates as it gets deep down to help prevent breakouts waiting to happen.</p>
                        <p class="ICP_smaller"><strong>When combined with Pore Strips for a Complete Bior&eacute;&reg; Clean:</strong> Clinically proven to remove 99% of blemish-causing dirt and oil.</p>
						<p class="ICP_buy_now_btn"><a href="http://www.drugstore.com/qxp92384/biore/blemish_fighting_ice_cleanser.htm?aid=328279&aparm=CC_BFIC" target="_blank">Buy Now</a></p>
					</div>
					<div class="ICP_boxes">
            <div class="ICP_time">
              <span class="ICP_one_minute">> 1 minute</span>
							<span class="ICP_use_daily">Use Daily</span>
						</div>
      <div class="ICP_works_great_with">
              <h2>Works great with:</h2>
              <div class="ICP_prod_img_single">
                <img src="/usa/images/instantlyClear/prod_details/pore_strips_med_img.jpg" alt="Deep Cleansing Pore Strips" /><br />
                <a href="/usa/instantlyClearerPores/IC_deepCleansing.aspx?height=605&amp;width=967" class="thickbox ICP_pore_strips">pore strips</a>
              </div>
              <!--
              <span class="ICP_plus">+</span>
              <div class="ICP_prod_img">
                <img src="/usa/images/instantlyClear/prod_details/pore_strips_small_img.jpg" alt="pore strips" /><br />
                <a href="/usa/instantlyClearerPores/IC_deepCleansing.aspx?height=605&amp;width=967" class="thickbox ICP_pore_strips">pore strips</a>
              </div>
              -->
            </div>
					</div>
					<p class="ICP_get_detail">Click a product to get more detail:</p>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(
	        function(){
		        $("a.ICP_close").click(function(e){
				        e.preventDefault();
				        tb_remove();
		        });
	        });
  </script>
<%--</asp:Content>--%>