﻿<%@ WebHandler Language="C#" Class="FacebookContests" %>

using System;
using System.Web;
using System.Text;
using System.Xml;

public class FacebookContests : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        bioreContact bc = new bioreContact();
        context.Response.Write(bc.bioreFacebookContest(context.Request.QueryString["fname"], context.Request.QueryString["lname"], context.Request.QueryString["email"], context.Request.QueryString["addr1"], context.Request.QueryString["city"], int.Parse(context.Request.QueryString["state"]), context.Request.QueryString["postalCode"], int.Parse(context.Request.QueryString["gender"]), context.Request.QueryString["DOB"], int.Parse(context.Request.QueryString["ProdUsedDaily"]), int.Parse(context.Request.QueryString["PayMoreAttention"]), int.Parse(context.Request.QueryString["WillingToPayMore"]), int.Parse(context.Request.QueryString["RecommendFriends"]), context.Request.QueryString["ProductsUsing"], int.Parse(context.Request.QueryString["MostUsedBrand"]), context.Request.QueryString["TypesProductsUsed"], context.Request.QueryString["ProductAttributes"], context.Request.QueryString["FaceCareProblems"], int.Parse(context.Request.QueryString["SkinType"]), int.Parse(context.Request.QueryString["Race"]), int.Parse(context.Request.QueryString["Spanish"]), int.Parse(context.Request.QueryString["banoptin"]), int.Parse(context.Request.QueryString["cureloptin"]), int.Parse(context.Request.QueryString["jergensoptin"]), int.Parse(context.Request.QueryString["jfoptin"]), int.Parse(context.Request.QueryString["bioreoptin"]), context.Request.QueryString["optinForm"], context.Request.QueryString["shadowFormSubLine"]));
        context.Response.End();
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}