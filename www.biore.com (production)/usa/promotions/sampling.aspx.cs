using System;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Text;

public partial class promotions_sampling : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        /* Setup defaults being shown/hidden, override for specific promo codes */
        Page.Header.Title = "Bior&eacute;&reg;";

        Int32 theSacCount = 0;
        Int32 theMrtCount = 0;
        Boolean SacAvailable = false;
        Boolean MrtAvailable = false;

        string thePromo = "";
        if (Request.QueryString["promo"] != null) thePromo = Request.QueryString["promo"];

        if (!IsPostBack)
        {
            InitializeBirthdateDropdowns();
            bioreDAO dao = new bioreDAO();

            if (thePromo.ToLower() == "sac_murt_10")
            {
                Page.Header.Title = "Bior&eacute;&reg; - Get Your Free Sample";

                //theSacCount = dao.getEventRemainingCount("biore_201010FBSACsamp");
                // Force SAC out of samples
                theSacCount = 0;
                theMrtCount = dao.getEventRemainingCount("biore_201010FBMURTsamp");

                if (theSacCount > 0) SacAvailable = true;
                if (theMrtCount > 0) MrtAvailable = true;

                if (SacAvailable == true || MrtAvailable == true)  // Check if there are still samples (SAC / MRT) available?
                {
                    default_headTxt.Visible = false;
                    fs_headTxt.Visible = true;
                    //fs_SampleChoice.Visible = true;

                    //if (SacAvailable == false) Sample.Items.Remove(Sample.Items[2]);
                    //if (MrtAvailable == false) Sample.Items.Remove(Sample.Items[1]);
                }
                else // NO - Let's show them the Out of samples message
                    showOutOfSamples();
            }
            else if (thePromo.ToLower() == "biore201009_fb_ps_samp")
            {
                int remainingCount = -1;
                remainingCount = dao.getEventRemainingCount(thePromo);
                if (remainingCount > 0)
                {
                    Page.Header.Title = "Bior&eacute;&reg; - Bior&eacute;&reg; Pore Strips Sample Giveaway";
                    default_headTxt.Visible = false;
                    psSamp_headTxt.Visible = true;
                    psSamp_Rules.Visible = true;
                }
                else
                {
                    showOutOfSamples();
                }
            }
            else // No Querystring Vars - Lets show them the 'not valid entrant' message
            {
                showNotValidPromotion();
            }
        }
        else // Postback
        {
            Page.Validate();
            if (Page.IsValid)
            {
                string DOB = mm.SelectedValue + "/" + dd.SelectedValue + "/" + yyyy.SelectedValue;
                string varProductsUsing = buildCheckboxString(ProductsUsing);
                string varTypesProductsUsed = buildCheckboxString(TypesProductsUsed);
                string varProductAttributes = buildCheckboxString(ProductAttributes);
                string varFaceProblems = buildCheckboxString(FaceProblems);
                string eventCode = "";
                int eventID = 0;
                string openEnded = "";
                int ban = 0, curel = 0, jergens = 0, jf = 0, biore = 0;
                if (banoptin.Checked)
                {
                    ban = 1;
                    metaBanOptin.Visible = true;
                }
                if (BioreOptin.Checked)
                {
                    biore = 1;
                    metaBioreOptin.Visible = true;
                }
                if (cureloptin.Checked)
                {
                    curel = 1;
                    metaCurelOptin.Visible = true;
                }
                if (jergensoptin.Checked)
                {
                    jergens = 1;
                    metaJergensOptin.Visible = true;
                }
                if (jfoptin.Checked)
                {
                    jf = 1;
                    metaJohnFriedaOptin.Visible = true;
                }
                bioreContact bc = new bioreContact();
                bioreDAO dao = new bioreDAO();

                if (thePromo.ToLower() == "sac_murt_10")
                {
                    Boolean doSubmit = false;

                    theSacCount = dao.getEventRemainingCount("biore_201010FBSACsamp");
                    theMrtCount = dao.getEventRemainingCount("biore_201010FBMURTsamp");

                    if (theSacCount > 0) SacAvailable = true;
                    if (theMrtCount > 0) MrtAvailable = true;

                    if (SacAvailable == true || MrtAvailable == true)  // Check if there are still samples (SAC / MRT) available?
                        doSubmit = true;
                    else // NO - Let's show them the Out of samples message
                        showOutOfSamples();

                    if (doSubmit == true)
                    {
                        String theNewEventName;

                        //if (Sample.Items[Sample.SelectedIndex].Value == "1")
                        //{
                            //theNewEventName = "biore_201010FBSACsamp";
                            //openEnded = "Sample request: SAC from FB";
                            //eventID = 400076;
                        //}
                        //else 
                        //{
                            theNewEventName = "biore_201010FBMURTsamp";
                            openEnded = "Sample request: MURT from FB";
                            eventID = 400077;
                        //}

                        int validEntry = -1;
                        validEntry = dao.getMemberAvailability(theNewEventName, Email.Text);

                        if (validEntry <= 0)
                        {
                            // Call Webservice
                            validEntry = -1;

                            validEntry = bc.bioreOptinFullAdress(0, FName.Text, LName.Text, Email.Text, Address1.Text, Address2.Text, Address3.Text, City.Text, Int32.Parse(State.SelectedValue), PostalCode.Text, Int32.Parse(Gender.SelectedValue), DOB, Int32.Parse(ProdUsedDaily.SelectedValue), Int32.Parse(PayMoreAttention.SelectedValue), Int32.Parse(WillingToPayMore.SelectedValue), Int32.Parse(RecommendFriends.SelectedValue), varProductsUsing, Int32.Parse(MostUsedBrand.SelectedValue), varTypesProductsUsed, varProductAttributes, varFaceProblems, Int32.Parse(SkinType.SelectedValue), Int32.Parse(Race.SelectedValue), Int32.Parse(Spanish.SelectedValue), ban, curel, jergens, jf, biore, eventID, eventCode, openEnded);

                            OptinForm.Visible = false;
                            RequestSuccess.Visible = true;
                            fs_Success.Visible = true;
                            Page.Header.Title = "Bior&eacute;&reg; - Get Your Free Sample";
                        }
                        else
                        {
                            showNotValidEntrant();
                        }
                    }
                }
                else if (thePromo.ToLower() == "biore201009_fb_ps_samp")
                {
                    eventCode = thePromo;
     
                    eventID = 400057;
                    openEnded = "Entry: Facebook Pore Strip Samples";
                    int validFBEntry = -1;

                    validFBEntry = dao.getMemberAvailability(eventCode, Email.Text);
                    if (validFBEntry <= 0)
                    {
                        validFBEntry = bc.bioreOptinFullAdress(0, FName.Text, LName.Text, Email.Text, Address1.Text, Address2.Text, Address3.Text, City.Text, Int32.Parse(State.SelectedValue), PostalCode.Text, Int32.Parse(Gender.SelectedValue), DOB, Int32.Parse(ProdUsedDaily.SelectedValue), Int32.Parse(PayMoreAttention.SelectedValue), Int32.Parse(WillingToPayMore.SelectedValue), Int32.Parse(RecommendFriends.SelectedValue), varProductsUsing, Int32.Parse(MostUsedBrand.SelectedValue), varTypesProductsUsed, varProductAttributes, varFaceProblems, Int32.Parse(SkinType.SelectedValue), Int32.Parse(Race.SelectedValue), Int32.Parse(Spanish.SelectedValue), ban, curel, jergens, jf, biore, eventID, eventCode, openEnded);

                        OptinForm.Visible = false;
                        RequestSuccess.Visible = true;
                        psSamp_Success.Visible = true;
                        fs_Success.Visible = false;
                        Page.Header.Title = "Bior&eacute;&reg; - Bior&eacute;&reg; Pore Strip Samples";
                    }
                    else
                    {
                        OptinForm.Visible = false;
                        InvalidEntry.Visible = true;
                    }
                }
                else
                {
                    OptinForm.Visible = false;
                    InvalidPromo.Visible = true;
                }
            }
        }
         }

    protected void showNotValidEntrant()
    {
        OptinForm.Visible = false;
        InvalidEntry.Visible = true;
    }
    protected void showNotValidPromotion()
    {
        OptinForm.Visible = false;
        InvalidPromo.Visible = true;
    }
    protected void showOutOfSamples()
    {
        OptinForm.Visible = false;
        OutOfSamples.Visible = true;
    }

    private string buildCheckboxString(CheckBoxList cb)
    {
        StringBuilder sb = new StringBuilder();
        foreach (ListItem li in cb.Items)
        {
            if (li.Selected)
            {
                sb.Append(Int32.Parse(li.Value));
                sb.Append(",");
            }
        }

        char[] charsToTrim = { ',' };
        return sb.ToString().TrimEnd(charsToTrim);
    }

    protected void DOBValidate(object source, ServerValidateEventArgs args)
    {
        DateTime dt = new DateTime();
        string dob = mm.SelectedValue.ToString() + "/" + dd.SelectedValue.ToString() + "/" + yyyy.SelectedValue.ToString();
        try
        {
            DateTime.Parse(dob);
            args.IsValid = true;
        }
        catch
        {
            args.IsValid = false;
        }
    }

    protected void DOBUnderage(object source, ServerValidateEventArgs args)
    {
        string dob = mm.SelectedValue.ToString() + "/" + dd.SelectedValue.ToString() + "/" + yyyy.SelectedValue.ToString();
        bool invalid = false;
        DateTime dateValue;
        if (!DateTime.TryParse(dob, out dateValue))
            invalid = true;
        try
        {
            DateTime.Parse(dob);
            DateTime dt = new DateTime();
            dt = DateTime.Parse(dob);
            if (getAgeYearDifference(dt) < 18)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
        catch
        {
            args.IsValid = false;
        }
        if (invalid)
            args.IsValid = true;
    }

    protected void InitializeBirthdateDropdowns()
    {
        DateTime dt = DateTime.Now;

        for (int i = dt.Year - 13; i > dt.Year - 100; i--)
        {
            ListItem li = new ListItem();
            li.Text = i.ToString();
            li.Value = i.ToString();
            yyyy.Items.Add(li);
            li = null;
        }
        for (int i = 1; i < 13; i++)
        {
            ListItem li = new ListItem();
            li.Text = i.ToString();
            li.Value = i.ToString();
            mm.Items.Add(li);
            li = null;
        }
        for (int i = 1; i < 32; i++)
        {
            ListItem li = new ListItem();
            li.Text = i.ToString();
            li.Value = i.ToString();
            dd.Items.Add(li);
            li = null;
        }
    }
    private int getAgeYearDifference(DateTime dt)
    {
        // get the difference in years
        int years = DateTime.Now.Year - dt.Year;
        // subtract another year if we're before the
        // birth day in the current year
        if (DateTime.Now.Month < dt.Month ||
            (DateTime.Now.Month == dt.Month &&
            DateTime.Now.Day < dt.Day))
            years--;
        return years;
    }
}
