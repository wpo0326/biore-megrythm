using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Text;

public partial class promotions_couponOptin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Header.Title = "Bior&eacute;&reg; Coupon";

        string medium = "";
        int couponCode = 0;
        string checkCode = "BO";
        string shortKey = "";
        string longKey = "";
        int validEntry = -1;
        string source = "";
        string paramUsed = "";
        int eventID = 0;
        string eventCode = "";
        string openEnded = "";
        int EventId_signup_Biore = int.Parse(ConfigurationManager.AppSettings["EventId_signup_Biore"]);
        string EventCode_signup_Biore = ConfigurationManager.AppSettings["EventCode_signup_Biore"];
        bool eventOver = true;
        bool hasPrintLimit = false; // used to turn off coupons if prints are exhausted

        if (Request.QueryString["promo"] != null)
            paramUsed = Request.QueryString["promo"];
        else if (Request.QueryString["enl_source"] != null)
            paramUsed = Request.QueryString["enl_source"];

        switch (paramUsed)
        {
            case "910576":
                couponCode = 62241;
                source = "AOL";
                medium = "Banner";
                eventCode = "biore_1coup10";
                eventID = 400024;
                shortKey = "6zwoebil25";
                longKey = "kKLbQdZumyDBsxSM5lzJHpURig7PI6a1V3wrFYn9O2tqfAehGT4ojX8EcvCWN";
				twoDollar_head.Visible = false;
                eventOver = true;
                break;
            case "biore_us_201012FBSACCoup":
                couponCode = 79633;
                source = "";
                medium = "Facebook";
                eventCode = paramUsed;
                eventID = 400089;
                shortKey = "tc1mws82e0";
                longKey = "UwDVAnJbyYlWKdCmQ93SNotTrcigfh46FpHzeXsOEjIv21Gau8kMx5qRL7ZPB";
                default_head.Visible = false;
                fb3WkHead.Visible = true;
				twoDollar_head.Visible = false;
                hasPrintLimit = true;
                if (DateTime.Now >= Convert.ToDateTime("12/04/2010 00:00:01") && DateTime.Now <= Convert.ToDateTime("12/11/2010 23:59:59"))
                    eventOver = false;
                else
                    eventOver = true;
                break;
            case "biore_us_201012FBPSCoup":
                couponCode = 79634;
                source = "";
                medium = "Facebook";
                eventCode = paramUsed;
                eventID = 400090;
                shortKey = "x3u6joehqn";
                longKey = "asjJ23Brq86hvCEP9LRQoU4I7SifyDmZbAYKVxwTXe5dkcuznMFtHGNlgp1WO";
                default_head.Visible = false;
                fb3WkHead.Visible = true;
				twoDollar_head.Visible = false;
                hasPrintLimit = true;
                if (DateTime.Now >= Convert.ToDateTime("12/11/2010 00:00:01") && DateTime.Now <= Convert.ToDateTime("12/18/2010 23:59:59"))
                    eventOver = false;
                else
                    eventOver = true;
                break;
            case "biore_us_201012FBCPSCoup":
                couponCode = 79834;
                source = "";
                medium = "Facebook";
                eventCode = paramUsed;
                eventID = 400091;
                shortKey = "6qvxkjdc4h";
                longKey = "lWkuNtZKsQxTSB2HLd5ieC4fOIcRyqbX8nUoaJpPDM39wvmAFjhGY1EgrVz76";
                hasPrintLimit = true;
                default_head.Visible = false;
                fb3WkHead.Visible = true;
				twoDollar_head.Visible = false;
                if (DateTime.Now >= Convert.ToDateTime("12/18/2010 00:00:01") && DateTime.Now <= Convert.ToDateTime("12/25/2010 23:59:59"))
                    eventOver = false;
                else
                    eventOver = true;
                break;
			 case "biore_us_201101FBCoup2Off":
                couponCode = 81442;
                source = "";
                medium = "Facebook";
                eventCode = paramUsed;
                eventID = 400114;
                shortKey = "ng3rl8xc0o";
                longKey = "8BtoM4miNdFT6COpaeDQ2YhRSVzJPjlw7gU1cvLsy9GEHfbKWnXq5kurZAIx3";
                hasPrintLimit = true;
                default_head.Visible = false;
                fb3WkHead.Visible = false;
				twoDollar_head.Visible = true;
                if (DateTime.Now >= Convert.ToDateTime("01/10/2011 00:00:01") && DateTime.Now <= Convert.ToDateTime("01/17/2011 23:59:59"))
                    eventOver = false;
                else
                    eventOver = true;
                break;
            default:
                eventOver = true;
                break;
        }

        if (!IsPostBack)
        {
            if (hasPrintLimit)
            {
                bioreDAO dao = new bioreDAO();
                int remainingCount = -1;
                remainingCount = dao.getEventRemainingCount(eventCode);
                if (remainingCount <= 0)
                {
                    OptinForm.Visible = false;
                    OutOfCoupons.Visible = true;
                }
            }
            
            if (eventOver)
            {
                OptinForm.Visible = false;
                invalidOffer.Visible = true;
            }
            
            InitializeBirthdateDropdowns();
        }
        else // Postback
        {
            Page.Validate();
            if (Page.IsValid)
            {
                // Call Webservice
                bioreContact bc = new bioreContact();

                string DOB = mm.SelectedValue + "/" + dd.SelectedValue + "/" + yyyy.SelectedValue;
                string varProductsUsing = buildCheckboxString(ProductsUsing);
                string varTypesProductsUsed = buildCheckboxString(TypesProductsUsed);
                string varProductAttributes = buildCheckboxString(ProductAttributes);
                string varFaceProblems = buildCheckboxString(FaceProblems);
                openEnded = "couponcode=" + couponCode.ToString() + "|medium=" + medium + "|source=" + source;

                int ban = 0, curel = 0, jergens = 0, jf = 0, biore = 0;
                if (banoptin.Checked)
                {
                    ban = 1;
                    metaBanOptin.Visible = true;
                }
                if (BioreOptin.Checked)
                {
                    biore = 1;
                    metaBioreOptin.Visible = true;
                }
                if (cureloptin.Checked)
                {
                    curel = 1;
                    metaCurelOptin.Visible = true;
                }
                if (jergensoptin.Checked)
                {
                    jergens = 1;
                    metaJergensOptin.Visible = true;
                }
                if (jfoptin.Checked)
                {
                    jf = 1;
                    metaJohnFriedaOptin.Visible = true;
                }
                validEntry = bc.bioreOptinFullAdress(0, FName.Text, LName.Text, Email.Text, "", "", "", "", 0, PostalCode.Text, Int32.Parse(Gender.SelectedValue), DOB, Int32.Parse(ProdUsedDaily.SelectedValue), Int32.Parse(PayMoreAttention.SelectedValue), Int32.Parse(WillingToPayMore.SelectedValue), Int32.Parse(RecommendFriends.SelectedValue), varProductsUsing, Int32.Parse(MostUsedBrand.SelectedValue), varTypesProductsUsed, varProductAttributes, varFaceProblems, Int32.Parse(SkinType.SelectedValue), Int32.Parse(Race.SelectedValue), Int32.Parse(Spanish.SelectedValue), ban, curel, jergens, jf, biore, EventId_signup_Biore, EventCode_signup_Biore, "Biore marketing questions from " + eventCode);

                if (validEntry == 0)
                    validEntry = bc.bioreStringCoupon(0, FName.Text, LName.Text, Email.Text, "", medium, couponCode, source, eventID, eventCode, openEnded);

                string newPage = "http://bricks.coupons.com/enable.asp?eb=1&o=" + couponCode + "&c=" + checkCode + "&p=" + Server.UrlEncode(Email.Text) + "&cpt=" + couponFunctions.EncodeCPT(Email.Text, couponCode, shortKey, longKey);

                if (validEntry == 0)
                {
                    metaRefresh.Visible = true;
                    metaRefresh.Text = "<meta http-equiv=\"refresh\" content=\"0;URL=" + newPage + "\" />";
                    metaRefresh.Visible = true;
                    metaSubmit.Visible = true;
                    redURL.NavigateUrl = newPage;
                    OptinForm.Visible = false;
                    OptinFormSuccess.Visible = true;
                }
                else
                {
                    OptinForm.Visible = false;
                    RequestFailure.Visible = true;
                }
            }
        }
    }

    protected void showNotValidEntrant()
    {
        OptinForm.Visible = false;
        InvalidEntry.Visible = true;
    }



    private string buildCheckboxString(CheckBoxList cb)
    {
        StringBuilder sb = new StringBuilder();
        foreach (ListItem li in cb.Items)
        {
            if (li.Selected)
            {
                sb.Append(Int32.Parse(li.Value));
                sb.Append(",");
            }
        }

        char[] charsToTrim = { ',' };
        return sb.ToString().TrimEnd(charsToTrim);
    }

    protected void DOBValidate(object source, ServerValidateEventArgs args)
    {
        DateTime dt = new DateTime();
        string dob = mm.SelectedValue.ToString() + "/" + dd.SelectedValue.ToString() + "/" + yyyy.SelectedValue.ToString();
        try
        {
            DateTime.Parse(dob);
            args.IsValid = true;
        }
        catch
        {
            args.IsValid = false;
        }
    }

    protected void InitializeBirthdateDropdowns()
    {
        DateTime dt = DateTime.Now;

        for (int i = dt.Year - 13; i > dt.Year - 100; i--)
        {
            ListItem li = new ListItem();
            li.Text = i.ToString();
            li.Value = i.ToString();
            yyyy.Items.Add(li);
            li = null;
        }
        for (int i = 1; i < 13; i++)
        {
            ListItem li = new ListItem();
            li.Text = i.ToString();
            li.Value = i.ToString();
            mm.Items.Add(li);
            li = null;
        }
        for (int i = 1; i < 32; i++)
        {
            ListItem li = new ListItem();
            li.Text = i.ToString();
            li.Value = i.ToString();
            dd.Items.Add(li);
            li = null;
        }
    }
}
