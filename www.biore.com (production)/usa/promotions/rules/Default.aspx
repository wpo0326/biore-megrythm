﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="promotions_rules_Default" %>

<%@ MasterType VirtualPath="../../MasterPages/Main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta name="Description" content="Contact the Bioré® skincare experts." />
    <meta name="Keywords" content="Bioré, contact Bioré, email Bioré, mail Bioré,, phone Bioré, address, phone number, toll free, call Bioré, write Bioré" />
    <link rel="stylesheet" type="text/css" href="/usa/css/optin2.css" media="screen, projection" />
    <!--[if lt IE 7]>
	<script>
	    DD_belatedPNG.fix('#optin_inner, #ctl00_ContentPlaceHolder1_OptinForm h1');
	</script>
	<![endif]-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="optin">
        <div id="optin_inner">
            <div id="imgContact">
            </div>
            <asp:Panel ID="OptinForm" runat="server">
                <h2>
                    Bior&eacute;&reg; Skincare Record Build Out Giveaway<br />
                    Official Rules</h2>
                <p>
                    <strong>NO PURCHASE NECESSARY TO ENTER OR WIN. A PURCHASE DOES NOT IMPROVE YOUR CHANCES
                        OF WINNING.</strong>
                </p>
                <p>
                    <strong>Promotion may only be entered in or from the 50 United States and the District
                        of Columbia and entries originating from any other jurisdiction are not eligible
                        for entry. This Promotion is governed exclusively by the laws of the United States.
                        You are not authorized to participate in the Promotion if you are not located within
                        the 50 United States or the District of Columbia. </strong>
                </p>
                <p>
                    <strong>1. How to Enter.</strong> Beginning at 12:00 PM Eastern Time ("ET") on <strong>
                        June 22nd, 2010,</strong> intended recipients of the <strong>Bior&eacute;&reg; Skincare
                            Record Build Out Email</strong> must fully complete the online survey received
                    on the secure link from Sponsor's email, and submit survey to the Bior&eacute;&reg;
                    Skincare website in accordance with Sponsor's instructions. The first <strong>3,500</strong>
                    to complete the online survey and send it back to Sponsor will each receive a sample
                    of a <strong>Bior&eacute;&reg; Pore Strip</strong>. Once there are <strong>3,500</strong>
                    entrants that have successfully responded, the Promotion will be over, and no other
                    prizes will be awarded.
                </p>
                <p>
                    Limit one (1) entry per person, per e-mail address. No automated entry devices and/or
                    programs permitted. All entries become the sole and exclusive property of the Sponsor
                    and receipt of entries will not be acknowledged or returned. Delivery of prizes
                    requires a street address. Sponsor is not responsible for lost, late, illegible,
                    stolen, incomplete, invalid, unintelligible, misdirected, postage-due, technically
                    corrupted or garbled entries or mail, which will be disqualified, or for problems
                    of any kind whether mechanical, human or electronic. Only fully completed entry
                    forms are eligible. Proof of submission will not be deemed to be proof of receipt
                    by Sponsor.
                </p>
                <p>
                    <strong>2. Start/End Dates.</strong> Promotion begins at <strong>12:01 PM</strong>
                    ET on <strong>June 22nd, 2010</strong> and ends once <strong>3,500</strong> entrants
                    have successfully submitted entry, or on <strong>July 22nd, 2010,</strong> whichever
                    occurs first.
                </p>
                <p>
                    <strong>3. Eligibility.</strong> Participation open only to legal residents of the
                    fifty United States or the District of Columbia, who are 18 or older as of date
                    of entry. Entrants must be the intended recipient of the Record Build Out email
                    from the Bior&eacute;&reg; brand. Void outside of the 50 United States and the District
                    of Columbia, and where prohibited, taxed or restricted by law. Employees, officers
                    and directors of Sponsor and its parent companies, subsidiaries, affiliates, partners,
                    advertising and promotion agencies, manufacturers or distributors of Promotion materials
                    and their immediate families (parents, children, siblings, spouse) or members of
                    the same household (whether related or not) of such employees/officers/directors
                    are not eligible to enter. Promotion may only be entered in or from the 50 United
                    States and the District of Columbia, and entries originating from any other jurisdiction
                    are not eligible for entry. All federal, state and local laws and regulations apply.</p>
                <p>
                    <strong>4. Prizes:</strong> The first <strong>3,500</strong> entrants who submit
                    the completed survey to Sponsor will receive a <strong>free sample of a Bior&eacute;&reg;
                        Pore Strip</strong>. ARV of each prize: $1.00. ARV of all prizes combined: $3,500.00.
                    Winners will be notified on-line instantly of their winning status. If any prize
                    is returned as undeliverable the prize will be forfeited. Upon prize forfeiture,
                    no compensation will be given. Limit one prize per person, per email. Odds of winning
                    depend on the number of respondents. Allow 6 (six) to 8 (eight) weeks to receive
                    prize. By entering the Promotion, entrants fully and unconditionally agree to be
                    bound by these rules, which will be final and binding in all matters relating to
                    the Promotion.
                </p>
                <p>
                    <strong>5. Conditions.</strong> All taxes are the sole responsibility of the winners.
                    By participating, entrants and winners agree to release and hold harmless Sponsor,
                    its advertising and promotion agencies and its parent companies, subsidiaries, affiliates,
                    partners, representatives, agents, successors, assigns, employees, officers and
                    directors, from any and all liability, for loss, harm, damage, injury, cost or expense
                    whatsoever including without limitation, property damage, personal injury and/or
                    death which may occur in connection with, preparation for, travel to, or participation
                    in Promotion, or possession, acceptance and/or use or misuse of prize or participation
                    in any Promotion-related activity and claims based on publicity rights, defamation
                    or invasion of privacy and merchandise delivery. Entrants who do not comply with
                    these Official Rules, or attempt to interfere with this promotion in any way shall
                    be disqualified. Prizes are non-transferable. No substitutions or cash redemptions.
                    In the case of unavailability of any prize, Sponsor reserves the right to substitute
                    a prize of equal or greater value. There is no purchase or sales presentation required
                    to participate. A purchase does not increase odds of winning.
                </p>
                <p>
                    <strong>6. Additional Terms for Online Promotion.</strong> Sponsor reserves the
                    right, in its sole discretion, to cancel, terminate, modify, or suspend this Promotion
                    should (in its sole discretion) virus, bugs, non-authorized human intervention or
                    other causes beyond its control corrupt or affect the administration, security,
                    fairness or proper conduct of the Promotion. In such case, Sponsor will select the
                    winners from all eligible entries received prior to and/or after (if appropriate)
                    the action taken by Sponsor. Sponsor reserves the right, at its sole discretion,
                    to disqualify any individual it finds, in its sole discretion, to be tampering with
                    the entry process or the operation of the Promotion or Web site. For details regarding
                    collection of information from users of the Web site (including entrants), please
                    consult the privacy policy on the web site. Additionally, Sponsor reserves the right
                    to prosecute any fraudulent activities to the full extent of the law. In case of
                    dispute as to the identity of any entrant, entry will be declared made by the authorized
                    account holder of the email address submitted at time of entry. "Authorized Account
                    Holder" is defined as the natural person who is assigned an email address by an
                    Internet access provider, online service provider, or other organization (e.g.,
                    business, educational, institution, etc.) responsible for assigning email addresses
                    or the domain associated with the submitted email address. Any other attempted form
                    of entry is prohibited; no automatic, programmed; robotic or similar means of entry
                    are permitted. Sponsor, and its parent companies, subsidiaries, affiliates, partners
                    and promotion and advertising agencies are not responsible for technical, hardware,
                    software, telephone or other communications malfunctions, errors or failures of
                    any kind, lost or unavailable network connections, Web site, Internet, or ISP availability,
                    unauthorized human intervention, traffic congestion, incomplete or inaccurate capture
                    of entry information (regardless of cause) or failed, incomplete, garbled, jumbled
                    or delayed computer transmissions which may limit one's ability to enter the Promotion,
                    including any injury or damage to participant's or any other person's computer relating
                    to or resulting from participating in this Promotion or downloading any materials
                    in this Promotion.</p>
                <p>
                    <em>CAUTION: ANY ATTEMPT BY AN ENTRANT TO DELIBERATELY DAMAGE ANY WEB SITE OR UNDERMINE
                        THE LEGITIMATE OPERATION OF THE PROMOTION MAY BE A VIOLATION OF CRIMINAL AND CIVIL
                        LAWS AND SHOULD SUCH AN ATTEMPT BE MADE, THE SPONSOR RESERVES THE RIGHT TO SEEK
                        DAMAGES FROM ANY SUCH PERSON TO THE FULLEST EXTENT PERMITTED BY LAW.</em></p>
                <p>
                    <strong>7. Use of Data.</strong> Sponsor will be collecting personal data about
                    entrants online, in accordance with its privacy policy. Please review the Sponsor's
                    privacy policy at <a href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">
                        http://www.kaobrands.com/privacy_policy.asp</a>. By participating in the Promotion,
                    entrants hereby agree to Sponsor's collection and usage of their personal information
                    and acknowledge that they have read and accepted Sponsor's privacy policy.
                </p>
                <p>
                    <strong>8. Sponsor.</strong> Kao Brands Company, 2535 Spring Grove Avenue, Cincinnati,
                    OH 45214-1773.
                </p>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
