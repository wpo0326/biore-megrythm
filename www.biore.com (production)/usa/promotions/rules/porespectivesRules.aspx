﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="promotions_rules_Default" %>

<%@ MasterType VirtualPath="../../MasterPages/Main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta name="Description" content="Bioré® skincare Porespectives Rules." />
    <link rel="stylesheet" type="text/css" href="/usa/css/optin2.css" media="screen, projection" />
    <!--[if lt IE 7]>
	<script>
	    DD_belatedPNG.fix('#optin_inner, #ctl00_ContentPlaceHolder1_OptinForm h1');
	</script>
	<![endif]-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="optin" class="rulesCopy">
        <div id="optin_inner">
            <div id="imgContact">
            </div>
            <asp:Panel ID="OptinForm" runat="server">
                <h2>
                    Bior&eacute;<sup>&reg;</sup> Skincare Fresh Fan Pore-spectives<br />
                    OFFER TERMS AND CONDITIONS</h2>

                    <p><strong>1. Eligibility:</strong> Bior&eacute;<sup>&reg;</sup> Skincare Fresh Pore-spectives (the "Offer") is open to legal residents of the fifty (50) United States (including D.C.), eighteen (18) years old or older at the time of entry who are invited by Sponsor to participate. You must be the intended recipient of Sponsor's invite to participate in this Offer. Offer is non-transferable. This Offer is void outside of the fifty (50) United States and D.C. and where prohibited.</p>
                     
                    <p><strong>2. Timing:</strong> Offer begins on November 21, 2011 at 12:00 a.m. Eastern Time ("ET") and ends on November 27, 2011 at 11:59 p.m. ET (the "Offer Period"). Sponsor's computer is the official time-keeping device for the Offer.</p>

                    <p><strong>3. Sponsor:</strong> Kao Brands Company, 2535 Spring Grove Ave, Cincinnati, OH 45214.</p>

                    <p><strong>4. How to Receive Offer:</strong> NO PURCHASE NECESSARY TO PARTICIPATE IN OFFER. During the Offer Period, follow the links and instructions provided in your invitation to complete and submit the registration form, including a valid home address for Offer fulfillment. Provide your honest feedback with respect to Bior&eacute;<sup>&reg;</sup> Skincare's new Pore Strips packaging (herein your "Submission") which is included in the email invite and on the Submission form. Your Submission must not exceed three hundred (300) characters. -By participating, you represent and warrant that your Submission: (1) is your own original work and does not infringe on the rights of any third parties; and (2) reflects your true and honest opinions, regardless of whether opinions are positive or negative.</p>

                    <p>During the Offer Period, each eligible participant who completes the Submission form will receive one (1) 8-count box of Bior&eacute;<sup>&reg;</sup> Deep Cleansing Pore Strips (&quot;Reward&quot;). No substitutions or cash redemptions. In the case of unavailability of Reward, Sponsor reserves the right to substitute a reward of equal or greater value. Approximate Retail Value of Reward: $9.99. Limit: One (1) Offer per participant/email address. Multiple participants are not permitted to share the same email address. Any attempt by any participant to obtain more than one (1) Offer by using multiple/different email addresses, identities, registrations and logins, or any other methods will void that participant's offer. Allow 4-6 weeks after Submission date for delivery of Reward. Select participant's Submissions may be featured on the Bior&eacute;<sup>&reg;</sup> Skincare page on Facebook in accordance with Section 5 below.</p>
                     
                    <p><strong>5. Publicity:</strong> Participation in the Offer constitutes a participant's consent to give Sponsor and its agents a royalty-free, irrevocable, perpetual, license to use, reproduce, modify, publish, create derivative works from, and display such Submission in whole or in part, on a worldwide basis, and to incorporate it into other works, in any form, media or technology now known or later developed, including for promotional or marketing purposes, and including, but not limited to, on the Bior&eacute;<sup>&reg;</sup> Skincare page on Facebook. For sake of clarity, Sponsor is under no obligation whatsoever to use, post or display any Submissions. Except where prohibited, participants also consent to Sponsor and its agents to use of participant's name, likeness, photograph, voice, opinions and/or hometown and state for promotional purposes in any media, worldwide, without further payment or consideration.</p>

                    <p><strong>6. General Conditions:</strong> Sponsor reserves the right to cancel, suspend and/or modify the Offer, or any part of it, without prior notice, if any fraud, technical failures or any other factor beyond Sponsor's reasonable control impairs the integrity or proper functioning of the Offer, as determined by Sponsor in its sole discretion, or for any other reason. Sponsor reserves the right in its sole discretion to disqualify any individual it finds to be tampering with the registration process or the operation of the Offer or to be acting in violation of these Terms and Conditions or any other promotion or in an unsportsmanlike or disruptive manner. Any attempt by any person to deliberately undermine the legitimate operation of the Offer may be a violation of criminal and civil law, and, should such an attempt be made, Sponsor reserves the right to seek damages from any such person to the fullest extent permitted by law. Sponsor's failure to enforce any term of these Terms and Conditions shall not constitute a waiver of that provision.</p>

                    <p><strong>7. Limitations of Liability:</strong> Sponsor, Enlighten, Inc., and their respective subsidiaries, affiliates, suppliers, distributors, advertising/promotion agencies, and each of their respective parent companies and each such company's officers, directors, employees and agents (the &quot;Released Parties&quot;) are not responsible for: (1) any incorrect or inaccurate information, whether caused by participants, printing errors or by any of the equipment or programming associated with or utilized in the Offer; (2) technical failures of any kind, including, but not limited to malfunctions, interruptions, or disconnections in phone lines or network hardware or software; (3) unauthorized human intervention in any part of the registration process or the Offer; (4) technical or human error which may occur in the administration of the Offer or the processing of registrations; (5) late, lost, undeliverable, damaged or stolen mail; or (6) any injury or damage to persons or property which may be caused, directly or indirectly, in whole or in part, from participation in the Offer or receipt or use or misuse of the Offer or Reward.</p>

                    <p><strong>8. Disputes:</strong> Except where prohibited, participant agrees that: (1) any and all disputes, claims and causes of action arising out of or connected with this Offer shall be resolved individually, without resort to any form of class action, and exclusively by the United States District Court for the Eastern District of Michigan (Southern Division) or the appropriate Michigan State Court located in Oakland County, Michigan; (2) any and all claims, judgments and awards shall be limited to actual out-of-pocket costs incurred, including costs associated with entering this Offer, but in no event attorneys' fees; and (3) under no circumstances will participant be permitted to obtain awards for, and participant hereby waives all rights to claim, indirect, punitive, incidental and consequential damages and any other damages, other than for actual out-of-pocket expenses, and any and all rights to have damages multiplied or otherwise increased. All issues and questions concerning the construction, validity, interpretation and enforceability of these Terms and Conditions, or the rights and obligations of the participant and Sponsor in connection with the Offer, shall be governed by, and construed in accordance with, the laws of the State of Michigan, without giving effect to any choice of law or conflict of law rules (whether of the State of Michigan or any other jurisdiction), which would cause the application of the laws of any jurisdiction other than the State of Michigan.</p>

                    <p><strong>9. Participant's Personal Information:</strong> Information collected from participation is subject to Sponsor's <a href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">Privacy Policy</a>.</p>

            </asp:Panel>
        </div>
    </div>
</asp:Content>
