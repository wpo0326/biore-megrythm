<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true"
    CodeFile="skinProfile.aspx.cs" Inherits="skinProfile" Title="Untitled Page" %>

<%@ MasterType VirtualPath="../MasterPages/Main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%@ register tagprefix="CC1" namespace="CustomValidators" assembly="CustomValidators" %>
    <meta name="Description" content="Contact the Biore skincare experts." />
    <meta name="Keywords" content="Biore, contact Biore, email Biore, mail Biore,, phone Biore, address, phone number, toll free, call Biore, write Biore" />
    <meta name="DCSext.submit" content="true" runat="server" visible="false" id="metaSubmit" />
    <asp:Literal ID="metaRefresh" runat="server" Visible="false"></asp:Literal>
    <link rel="stylesheet" type="text/css" href="/usa/css/optin2.css" media="screen, projection" />
    <!--[if lt IE 7]>
	<script>
	    DD_belatedPNG.fix('#optin_inner, #ctl00_ContentPlaceHolder1_OptinForm h1');
	</script>
	<![endif]-->

    <script type="text/javascript">
        $(document).ready(function() {
            $("input:visible:first").focus();
            $("td.useOption").click(
            function() {
                if ($('#<%=ProductsUsedNone.ClientID%>:checked').attr('checked')) {
                    $("#<%=ProductsUsedNone.ClientID%>").attr("disabled", "true");
                    $("#<%=ProductsUsedNone.ClientID%>").attr("checked", "false");
                }
            });
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="optin">
        <div id="optin_inner">
            <div id="imgContact">
            </div>
            <asp:Panel ID="OptinForm" runat="server">
                <h1 title="Sign me up">
                    Sign me up</h1>
                <h2>
                    Thanks for your interest in the Bior&eacute;<sup>&reg;</sup> brand!</h2>
                <p>
                    Tell us a little more about yourself! Answer a few simple questions and submit the
                    survey to get your thank you coupon, worth <strong>$2.00</strong> off any full-size
                    Bior&eacute;&reg; Skincare product.</p>
                <p class="req">
                    <em>required</em>*</p>
                <div id="optinPart1">
                    <div>
                        <asp:Label ID="FNameLbl" runat="server" Text="First Name*" AssociatedControlID="FName"></asp:Label>
                        <asp:TextBox ID="FName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="FNameValidator1" runat="server" ErrorMessage="Please enter your First Name."
                            ControlToValidate="FName" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="FNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your First Name."
                            ValidationExpression="^[^<>]+$" ControlToValidate="FName" EnableClientScript="true"
                            SetFocusOnError="true" CssClass="errormsg"></asp:RegularExpressionValidator>
                    </div>
                    <div>
                        <asp:Label ID="LNameLbl" runat="server" Text="Last Name*" AssociatedControlID="LName"></asp:Label>
                        <asp:TextBox ID="LName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="LNameValidator1" runat="server" ErrorMessage="Please enter your Last Name."
                            ControlToValidate="LName" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="LNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Last Name."
                            ValidationExpression="^[^<>]+$" ControlToValidate="LName" SetFocusOnError="true"
                            CssClass="errormsg"></asp:RegularExpressionValidator>
                    </div>
                    <div>
                        <asp:Label ID="EmailLbl" runat="server" Text="Email*" AssociatedControlID="Email"></asp:Label>
                        <asp:TextBox ID="EmailHolder" runat="server" MaxLength="150" CssClass="inputTextBox"
                            Enabled="false" Text=""></asp:TextBox>
                    </div>
                    <asp:Panel ID="Address" runat="server" Visible="false">
                        <div>
                            <asp:Label ID="Address1Lbl" runat="server" Text="Address*" AssociatedControlID="Address1"></asp:Label>
                            <asp:TextBox ID="Address1" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="Address1Validator1" runat="server" ErrorMessage="Please enter your Address."
                                ControlToValidate="Address1" EnableClientScript="true" SetFocusOnError="true"
                                CssClass="errormsg"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="Address1Validator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Address."
                                ValidationExpression="^[^<>]+$" ControlToValidate="Address1" SetFocusOnError="true"
                                CssClass="errormsg"></asp:RegularExpressionValidator>
                        </div>
                        <div>
                            <asp:Label ID="Address2Lbl" runat="server" Text="&nbsp;" AssociatedControlID="Address2"></asp:Label>
                            <asp:TextBox ID="Address2" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                        </div>
                        <div>
                            <asp:Label ID="Address3Lbl" runat="server" Text="&nbsp;" AssociatedControlID="Address3"></asp:Label>
                            <asp:TextBox ID="Address3" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                        </div>
                        <div>
                            <asp:Label ID="CityLbl" runat="server" Text="City*" AssociatedControlID="City"></asp:Label>
                            <asp:TextBox ID="City" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="CityValidator1" runat="server" ErrorMessage="Please enter your City."
                                ControlToValidate="City" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="CityValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your City."
                                ValidationExpression="^[^<>]+$" ControlToValidate="City" SetFocusOnError="true"
                                CssClass="errormsg"></asp:RegularExpressionValidator>
                        </div>
                        <div>
                            <asp:Label ID="StateLbl" runat="server" Text="State*" AssociatedControlID="State"></asp:Label>
                            <asp:DropDownList ID="State" runat="server">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="1">Alabama</asp:ListItem>
                                <asp:ListItem Value="2">Alaska</asp:ListItem>
                                <asp:ListItem Value="4">Arizona</asp:ListItem>
                                <asp:ListItem Value="5">Arkansas</asp:ListItem>
                                <asp:ListItem Value="9">California</asp:ListItem>
                                <asp:ListItem Value="10">Colorado</asp:ListItem>
                                <asp:ListItem Value="11">Connecticut</asp:ListItem>
                                <asp:ListItem Value="12">Delaware</asp:ListItem>
                                <asp:ListItem Value="13">District Of Columbia</asp:ListItem>
                                <asp:ListItem Value="15">Florida</asp:ListItem>
                                <asp:ListItem Value="16">Georgia</asp:ListItem>
                                <asp:ListItem Value="17">Hawaii</asp:ListItem>
                                <asp:ListItem Value="18">Idaho</asp:ListItem>
                                <asp:ListItem Value="19">Illinois</asp:ListItem>
                                <asp:ListItem Value="20">Indiana</asp:ListItem>
                                <asp:ListItem Value="21">Iowa</asp:ListItem>
                                <asp:ListItem Value="22">Kansas</asp:ListItem>
                                <asp:ListItem Value="23">Kentucky</asp:ListItem>
                                <asp:ListItem Value="24">Louisiana</asp:ListItem>
                                <asp:ListItem Value="25">Maine</asp:ListItem>
                                <asp:ListItem Value="27">Maryland</asp:ListItem>
                                <asp:ListItem Value="28">Massachusetts</asp:ListItem>
                                <asp:ListItem Value="29">Michigan</asp:ListItem>
                                <asp:ListItem Value="30">Minnesota</asp:ListItem>
                                <asp:ListItem Value="31">Mississippi</asp:ListItem>
                                <asp:ListItem Value="32">Missouri</asp:ListItem>
                                <asp:ListItem Value="33">Montana</asp:ListItem>
                                <asp:ListItem Value="34">Nebraska</asp:ListItem>
                                <asp:ListItem Value="35">Nevada</asp:ListItem>
                                <asp:ListItem Value="36">New Hampshire</asp:ListItem>
                                <asp:ListItem Value="37">New Jersey</asp:ListItem>
                                <asp:ListItem Value="38">New Mexico</asp:ListItem>
                                <asp:ListItem Value="39">New York</asp:ListItem>
                                <asp:ListItem Value="40">North Carolina</asp:ListItem>
                                <asp:ListItem Value="41">North Dakota</asp:ListItem>
                                <asp:ListItem Value="43">Ohio</asp:ListItem>
                                <asp:ListItem Value="44">Oklahoma</asp:ListItem>
                                <asp:ListItem Value="45">Oregon</asp:ListItem>
                                <asp:ListItem Value="47">Pennsylvania</asp:ListItem>
                                <asp:ListItem Value="49">Rhode Island</asp:ListItem>
                                <asp:ListItem Value="50">South Carolina</asp:ListItem>
                                <asp:ListItem Value="51">South Dakota</asp:ListItem>
                                <asp:ListItem Value="52">Tennessee</asp:ListItem>
                                <asp:ListItem Value="53">Texas</asp:ListItem>
                                <asp:ListItem Value="54">Utah</asp:ListItem>
                                <asp:ListItem Value="55">Vermont</asp:ListItem>
                                <asp:ListItem Value="57">Virginia</asp:ListItem>
                                <asp:ListItem Value="58">Washington</asp:ListItem>
                                <asp:ListItem Value="59">West Virginia</asp:ListItem>
                                <asp:ListItem Value="60">Wisconsin</asp:ListItem>
                                <asp:ListItem Value="61">Wyoming</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="StateValidator" runat="server" ErrorMessage="Please select your State."
                                ControlToValidate="State" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                        </div>
                    </asp:Panel>
                    <div>
                        <asp:Label ID="PostalCodeLbl" runat="server" Text="ZIP Code*" AssociatedControlID="PostalCode"></asp:Label>
                        <asp:TextBox ID="PostalCode" runat="server" MaxLength="5" CssClass="inputTextBox"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="PostalCodeValidator1" runat="server" ErrorMessage="Please enter your ZIP Code"
                            ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true"
                            CssClass="errormsg"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="PostalCodeValidator2" runat="server" ErrorMessage="Please enter a valid 5-digit ZIP code."
                            ValidationExpression="^\d{5}$" ControlToValidate="PostalCode" EnableClientScript="true"
                            SetFocusOnError="true" CssClass="errormsg"></asp:RegularExpressionValidator>
                    </div>
                    <div class="country">
                        <asp:Label ID="CountryLbl" runat="server" Text="Country&nbsp;"></asp:Label>
                        <span class="fake_input">United States</span>
                    </div>
                    <div>
                        <asp:Label ID="GenderLbl" runat="server" Text="Gender*" AssociatedControlID="Gender"></asp:Label>
                        <asp:DropDownList ID="Gender" runat="server">
                            <asp:ListItem Value="">Select</asp:ListItem>
                            <asp:ListItem Value="1">Female</asp:ListItem>
                            <asp:ListItem Value="2">Male</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="GenderValidator" runat="server" ErrorMessage="Please enter your Gender."
                            ControlToValidate="Gender" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                    </div>
                    <div class="bd">
                        <asp:Label ID="DOBLbl" runat="server" Text="Birthdate*"></asp:Label>
                        <asp:DropDownList ID="yyyy" runat="server" CssClass="inputSelectBDay">
                            <asp:ListItem Value="">Year</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="yyyyValidator" runat="server" ErrorMessage="Please enter your birth Year."
                            ControlToValidate="yyyy" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg year"></asp:RequiredFieldValidator>
                        <asp:DropDownList ID="mm" runat="server" CssClass="inputSelectBDay">
                            <asp:ListItem Value="">Mon</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="mmValidator" runat="server" ErrorMessage="Please enter your birth Month."
                            ControlToValidate="mm" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg month"></asp:RequiredFieldValidator>
                        <asp:DropDownList ID="dd" runat="server" CssClass="inputSelectBDay">
                            <asp:ListItem Value="">Day</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="ddValidator" runat="server" ErrorMessage="Please enter your birth Day."
                            ControlToValidate="dd" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg day"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="DOBValidator" runat="server" ErrorMessage="Please enter a valid date."
                            OnServerValidate="DOBValidate" EnableClientScript="false" CssClass="errormsg" Display="Static" SetFocusOnError="true"></asp:CustomValidator>
                        <asp:CustomValidator ID="DOBUnderageCheck" runat="server" ErrorMessage="You must be 18 or older."
                            OnServerValidate="DOBUnderage" EnableClientScript="false" CssClass="errormsg" Display="Static" SetFocusOnError="true"></asp:CustomValidator>
                    </div>
                </div>
                <div id="optinPart2">
                    <div>
                        <asp:Label ID="ProdUsedDailyLbl" runat="server" Text="How many skin care products do you use on a daily basis?*"
                            AssociatedControlID="ProdUsedDaily" CssClass="ddlabels"></asp:Label>
                        <asp:DropDownList ID="ProdUsedDaily" runat="server">
                            <asp:ListItem Value="">Select</asp:ListItem>
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5 or more</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="ProdUsedDailyValidator" runat="server" ErrorMessage="Please answer all questions."
                            ControlToValidate="ProdUsedDaily" EnableClientScript="true" SetFocusOnError="true"
                            CssClass="errormsg"></asp:RequiredFieldValidator>
                    </div>
                    <div>
                        <asp:Label ID="PayMoreAttentionLbl" runat="server" Text="I pay more attention/spend more time on my skin than other people.*"
                            AssociatedControlID="PayMoreAttention" CssClass="ddlabels"></asp:Label>
                        <asp:DropDownList ID="PayMoreAttention" runat="server">
                            <asp:ListItem Value="">Select...</asp:ListItem>
                            <asp:ListItem Value="1">Agree strongly</asp:ListItem>
                            <asp:ListItem Value="2">Agree somewhat</asp:ListItem>
                            <asp:ListItem Value="3">Neither agree nor disagree</asp:ListItem>
                            <asp:ListItem Value="4">Disagree somewhat</asp:ListItem>
                            <asp:ListItem Value="5">Disagree strongly</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="PayMoreAttentionValidator" runat="server" ErrorMessage="Please answer all questions."
                            ControlToValidate="PayMoreAttention" EnableClientScript="true" SetFocusOnError="true"
                            CssClass="errormsg"></asp:RequiredFieldValidator>
                    </div>
                    <div>
                        <asp:Label ID="WillingToPayMoreLbl" runat="server" Text="I am willing to pay more for skin care products I really want.*"
                            AssociatedControlID="WillingToPayMore" CssClass="ddlabels"></asp:Label>
                        <asp:DropDownList ID="WillingToPayMore" runat="server">
                            <asp:ListItem Value="">Select...</asp:ListItem>
                            <asp:ListItem Value="1">Agree strongly</asp:ListItem>
                            <asp:ListItem Value="2">Agree somewhat</asp:ListItem>
                            <asp:ListItem Value="3">Neither agree nor disagree</asp:ListItem>
                            <asp:ListItem Value="4">Disagree somewhat</asp:ListItem>
                            <asp:ListItem Value="5">Disagree strongly</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="WillingToPayMoreValidator" runat="server" ErrorMessage="Please answer all questions."
                            ControlToValidate="WillingToPayMore" EnableClientScript="true" SetFocusOnError="true"
                            CssClass="errormsg"></asp:RequiredFieldValidator>
                    </div>
                    <div>
                        <asp:Label ID="RecommendFriendsLbl" runat="server" Text="How likely are you to recommend your favorite skin care products to friends?*"
                            AssociatedControlID="RecommendFriends" CssClass="ddlabels"></asp:Label>
                        <asp:DropDownList ID="RecommendFriends" runat="server">
                            <asp:ListItem Value="">Select...</asp:ListItem>
                            <asp:ListItem Value="1">Very likely</asp:ListItem>
                            <asp:ListItem Value="2">Somewhat likely</asp:ListItem>
                            <asp:ListItem Value="3">Somewhat unlikely</asp:ListItem>
                            <asp:ListItem Value="4">Not at all likely</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RecommendFriendsValidator" runat="server" ErrorMessage="Please answer all questions."
                            ControlToValidate="RecommendFriends" EnableClientScript="true" SetFocusOnError="true"
                            CssClass="errormsg"></asp:RequiredFieldValidator>
                    </div>
                    <%--                 <div class="checkboxDiv">
					<asp:Label ID="ProductsUsingLbl" runat="server" Text="Which Bior&eacute;&reg; products do you currently use?*  (select all that apply)"
                        AssociatedControlID="ProductsUsing" CssClass="checkboxHdrLabel"></asp:Label>
						<asp:CheckBoxList id="ProductsUsing" runat="server" RepeatLayout="Flow">
							<asp:ListItem Value="21" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Steam Activated Cleanser</asp:ListItem>
							<asp:ListItem Value="22" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Makeup Removing Towelettes</asp:ListItem>
							<asp:ListItem Value="6" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Revitalize 4-in-1 Self-Foaming Cleanser</asp:ListItem>
							<asp:ListItem Value="7" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Detoxify Daily Scrub</asp:ListItem>
							<asp:ListItem Value="8" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Refresh Daily Cleansing Cloths</asp:ListItem>
							<asp:ListItem Value="9" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Purify Self-Heating Mask</asp:ListItem>
							<asp:ListItem Value="10" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Restore Skin-Boosting Night Serum</asp:ListItem>
							<asp:ListItem Value="11" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Enliven Cooling Eye Gel</asp:ListItem>
							<asp:ListItem Value="12" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Nourish Moisture Lotion SPF 15</asp:ListItem>
							<asp:ListItem Value="14" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Blemish Fighting Ice Cleanser</asp:ListItem>
							<asp:ListItem Value="13" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Pore Unclogging Scrub</asp:ListItem>
							<asp:ListItem Value="16" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Warming Anti-Blackhead Cream Cleanser</asp:ListItem>
							<asp:ListItem Value="15" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Triple Action Astringent</asp:ListItem>
							<asp:ListItem Value="19" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Ultra Deep Cleansing Pore Strips</asp:ListItem>
							<asp:ListItem Value="17" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Deep Cleansing Pore Strips</asp:ListItem>
							<asp:ListItem Value="18" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Combo Pack Deep Cleansing Pore Strips</asp:ListItem>
							<asp:ListItem Value="1" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Dual Fusion&reg; Moisturizer + SPF 30</asp:ListItem>
							<asp:ListItem Value="2" Class="currUse checkboxesForToggling">Bior&eacute;&reg; See the Future&reg; Fortifying Eye Cream</asp:ListItem>
							<asp:ListItem Value="3" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Hard Day's Night&reg; Overnight Moisturizer</asp:ListItem>                             
							<asp:ListItem Value="4" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Even Smoother&reg; Microderm Exfoliator</asp:ListItem>
							<asp:ListItem Value="5" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Clean Things Up&reg; Nourishing Gel Cleanser</asp:ListItem>
							<asp:ListItem Value="20" ID="currUseNone" Class="checkboxesForToggling" onclick="toggleCheckboxes('currUse');">None of the above</asp:ListItem>
						</asp:CheckBoxList>
                        <CC1:RequiredFieldValidatorForCheckBoxLists
                            ControlToValidate="ProductsUsing"
                            runat="server"
                            ID="ProductsUsingValidator" EnableClientScript="true" SetFocusOnError="true"
                        CssClass="errormsg">Please check at least one box.
                        </CC1:RequiredFieldValidatorForCheckBoxLists>
                </div>--%>
                    <div class="radioGroup">
                        <p>
                            Which Bior&eacute;&reg; Skincare products do you currently use, or have you previously
                            used?<br />
                            (select all that apply)</p>
                        <table>
                            <tr>
                                <th class="nameCol">
                                    Deep Cleansing
                                </th>
                                <th class="buttonCol">
                                    Currently Use
                                </th>
                                <th class="buttonCol">
                                    Previously Used
                                </th>
                            </tr>
                            <tr>
                                <td class="nameCol">
                                    Steam Activated Cleanser
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="SAC1" Text="" GroupName="SAC" runat="server" />
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="SAC2" Text="" GroupName="SAC" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="nameCol">
                                    Makeup Removing Towelettes
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="MURT1" Text="" GroupName="MURT" runat="server" />
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="MURT2" Text="" GroupName="MURT" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="nameCol">
                                    Revitalize 4-in-1 Self Foaming Cleanser
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="Revitalize1" Text="" GroupName="Revitalize" runat="server" />
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="Revitalize2" Text="" GroupName="Revitalize" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="nameCol">
                                    Detoxify Daily Scrub
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="Detoxify1" Text="" GroupName="Detoxify" runat="server" />
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="Detoxify2" Text="" GroupName="Detoxify" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="nameCol">
                                    Even Smoother&reg; Microderm Exfoliator
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="EvenSmoother1" Text="" GroupName="EvenSmoother" runat="server" />
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="EvenSmoother2" Text="" GroupName="EvenSmoother" runat="server" />
                                </td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <th class="nameCol">
                                    Complexion Clearing
                                </th>
                                <th class="buttonCol">
                                    Currently Use
                                </th>
                                <th class="buttonCol">
                                    Previously Used
                                </th>
                            </tr>
                            <tr>
                                <td class="nameCol">
                                    Pore Unclogging Scrub
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="PUS1" Text="" GroupName="PUS" runat="server" />
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="PUS2" Text="" GroupName="PUS" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="nameCol">
                                    Blemish Fighting Ice Cleanser
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="BFIC1" Text="" GroupName="BFIC" runat="server" />
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="BFIC2" Text="" GroupName="BFIC" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="nameCol">
                                    Triple Action Astringent
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="Astringent1" Text="" GroupName="Astringent" runat="server" />
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="Astringent2" Text="" GroupName="Astringent" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="nameCol">
                                    Warming Anti-Blackhead Cream Cleanser
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="WarmingCleanser1" Text="" GroupName="WarmingCleanser" runat="server" />
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="WarmingCleanser2" Text="" GroupName="WarmingCleanser" runat="server" />
                                </td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <th class="nameCol">
                                    Pore Strips
                                </th>
                                <th class="buttonCol">
                                    Currently Use
                                </th>
                                <th class="buttonCol">
                                    Previously Used
                                </th>
                            </tr>
                            <tr>
                                <td class="nameCol">
                                    Deep Cleansing Pore Strips
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="DCPoreStrips1" Text="" GroupName="DCPoreStrips" runat="server" />
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="DCPoreStrips2" Text="" GroupName="DCPoreStrips" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="nameCol">
                                    Combo Pack Deep Cleansing Pore Strips
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="ComboPoreStrips1" Text="" GroupName="ComboPoreStrips" runat="server" />
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="ComboPoreStrips2" Text="" GroupName="ComboPoreStrips" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="nameCol">
                                    Ultra Deep Cleansing Pore Strips
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="UltraPoreStrips1" Text="" GroupName="UltraPoreStrips" runat="server" />
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="UltraPoreStrips2" Text="" GroupName="UltraPoreStrips" runat="server" />
                                </td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <th class="nameCol">
                                    Moisturizing
                                </th>
                                <th class="buttonCol">
                                    Currently Use
                                </th>
                                <th class="buttonCol">
                                    Previously Used
                                </th>
                            </tr>
                            <tr>
                                <td class="nameCol">
                                    Nourish Moisture Lotion SPF 15
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="Nourish1" Text="" GroupName="Nourish" runat="server" />
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="Nourish2" Text="" GroupName="Nourish" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="nameCol">
                                    Hard Day's Night&reg; Overnight Moisturizer
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="HardDaysNight1" Text="" GroupName="HardDaysNight" runat="server" />
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="HardDaysNight2" Text="" GroupName="HardDaysNight" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="nameCol">
                                    See The Future&reg; Fortifying Eye Cream
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="SeeTheFuture1" Text="" GroupName="SeeTheFuture" runat="server" />
                                </td>
                                <td class="useOption">
                                    <asp:RadioButton ID="SeeTheFuture2" Text="" GroupName="SeeTheFuture" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="nameCol none">
                                    None of the above
                                </td>
                                <td class="none">
                                    <asp:RadioButton ID="ProductsUsedNone" Checked="true" Text="" GroupName="None" runat="server" />
                                </td>
                                <td class="none">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <asp:Label ID="MostUsedBrandLbl" runat="server" Text="What brand of face care products do you use most often?* (select one)"
                            AssociatedControlID="MostUsedBrand" CssClass="ddlabels"></asp:Label>
                        <asp:DropDownList ID="MostUsedBrand" runat="server">
                            <asp:ListItem Value="">Select...</asp:ListItem>
                            <asp:ListItem Value="1">Aveeno&reg;</asp:ListItem>
                            <asp:ListItem Value="2">Avon</asp:ListItem>
                            <asp:ListItem Value="3">Bior&eacute;&reg; products</asp:ListItem>
                            <asp:ListItem Value="4">Cetaphil&reg;</asp:ListItem>
                            <asp:ListItem Value="5">Clean &amp; Clear&reg;</asp:ListItem>
                            <asp:ListItem Value="6">Clearasil&reg;</asp:ListItem>
                            <asp:ListItem Value="7">Clinique</asp:ListItem>
                            <asp:ListItem Value="8">Dove&reg;</asp:ListItem>
                            <asp:ListItem Value="9">Garnier Nutritioniste</asp:ListItem>
                            <asp:ListItem Value="10">L'Oreal&reg;</asp:ListItem>
                            <asp:ListItem Value="11">Mary Kay&reg;</asp:ListItem>
                            <asp:ListItem Value="12">Neutrogena&reg;</asp:ListItem>
                            <asp:ListItem Value="13">Noxzema&reg;</asp:ListItem>
                            <asp:ListItem Value="14">Olay&reg;</asp:ListItem>
                            <asp:ListItem Value="15">ProActiv&reg; Solutions</asp:ListItem>
                            <asp:ListItem Value="16">St. Ives&reg;</asp:ListItem>
                            <asp:ListItem Value="17">Store Brand</asp:ListItem>
                            <asp:ListItem Value="18">Other Department Store brand</asp:ListItem>
                            <asp:ListItem Value="19">Other</asp:ListItem>
                            <asp:ListItem Value="20">None of these</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="MostUsedBrandValidator" runat="server" ErrorMessage="Please answer all questions."
                            ControlToValidate="MostUsedBrand" EnableClientScript="true" SetFocusOnError="true"
                            CssClass="errormsg"></asp:RequiredFieldValidator>
                    </div>
                    <div class="checkboxDiv">
                        <asp:Label ID="TypesProductsUsedLbl" runat="server" Text="Which of the following types of products do you typically use?* (select all that apply)"
                            AssociatedControlID="TypesProductsUsed" CssClass="checkboxHdrLabel"></asp:Label>
                        <asp:CheckBoxList ID="TypesProductsUsed" runat="server" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Class="typUse checkboxesForToggling">Liquid/Cream/Gel cleanser</asp:ListItem>
              							<asp:ListItem Value="2" Class="typUse checkboxesForToggling">Cleansing Cloths</asp:ListItem>
              							<asp:ListItem Value="3" Class="typUse checkboxesForToggling">Facial moisturizing lotion/cream</asp:ListItem>
              							<asp:ListItem Value="4" Class="typUse checkboxesForToggling">Facial moisturizing lotion/cream with SPF</asp:ListItem>
              							<asp:ListItem Value="5" Class="typUse checkboxesForToggling">Facial scrub/exfoliator</asp:ListItem>
              							<asp:ListItem Value="6" Class="typUse checkboxesForToggling">Acne/Blemish spot treatment</asp:ListItem>
              							<asp:ListItem Value="7" Class="typUse checkboxesForToggling">Skin discoloration treatment product (sun damage)</asp:ListItem>
              							<asp:ListItem Value="8" Class="typUse checkboxesForToggling">Eye gel/cream/treatment</asp:ListItem>
              							<asp:ListItem Value="9" Class="typUse checkboxesForToggling">Night cream/ overnight moisturizer</asp:ListItem>
              							<asp:ListItem Value="10" Class="typUse checkboxesForToggling">Make-up remover</asp:ListItem>
              							<asp:ListItem Value="11" ID="typUseNone" Class="checkboxesForToggling" onclick="toggleCheckboxes('typUse');">None of the above</asp:ListItem>
                        </asp:CheckBoxList>
                        <CC1:RequiredFieldValidatorForCheckBoxLists ControlToValidate="TypesProductsUsed"
                            runat="server" ID="TypesProductsUsedValidator" EnableClientScript="true" SetFocusOnError="true"
                            CssClass="errormsg">Please check at least one box.
                        </CC1:RequiredFieldValidatorForCheckBoxLists>
                    </div>
                    <div class="checkboxDiv">
                        <asp:Label ID="ProductAttributesLbl" runat="server" Text="Which, if any, of the following attributes would you be interested in in a face care product?* (select all that apply)"
                            AssociatedControlID="ProductAttributes" CssClass="checkboxHdrLabel"></asp:Label>
                        <asp:CheckBoxList ID="ProductAttributes" runat="server" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Class="attr checkboxesForToggling">Convenience</asp:ListItem>
              							<asp:ListItem Value="2" Class="attr checkboxesForToggling">Anti-aging</asp:ListItem>
              							<asp:ListItem Value="3" Class="attr checkboxesForToggling">Deep cleaning</asp:ListItem>
              							<asp:ListItem Value="4" Class="attr checkboxesForToggling">Gentle / sensitive skin</asp:ListItem>
              							<asp:ListItem Value="5" Class="attr checkboxesForToggling">For acne-prone skin</asp:ListItem>
              							<asp:ListItem Value="6" Class="attr checkboxesForToggling">For combination skin</asp:ListItem>
              							<asp:ListItem Value="7" Class="attr checkboxesForToggling">Makeup removal / enhancement</asp:ListItem>
              							<asp:ListItem Value="8" ID="attrNone" Class="checkboxesForToggling" onclick="toggleCheckboxes('attr');">None of the above</asp:ListItem>
                        </asp:CheckBoxList>
                        <CC1:RequiredFieldValidatorForCheckBoxLists ControlToValidate="ProductAttributes"
                            runat="server" ID="ProductAttributesValidator" EnableClientScript="true" SetFocusOnError="true"
                            CssClass="errormsg">Please check at least one box.
                        </CC1:RequiredFieldValidatorForCheckBoxLists>
                    </div>
                    <div class="checkboxDiv">
                        <asp:Label ID="FaceProblemsLbl" runat="server" Text="Which, if any, of the following face care problems would you say you are extremely
                        or very concerned with?* (select all that apply)" AssociatedControlID="FaceProblems"
                            CssClass="checkboxHdrLabel"></asp:Label>
                        <asp:CheckBoxList ID="FaceProblems" runat="server" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Class="problems checkboxesForToggling">Acne</asp:ListItem>
              							<asp:ListItem Value="2" Class="problems checkboxesForToggling">Age spots</asp:ListItem>
              							<asp:ListItem Value="3" Class="problems checkboxesForToggling">Blackheads</asp:ListItem>
              							<asp:ListItem Value="4" Class="problems checkboxesForToggling">Blemishes / pimples</asp:ListItem>
              							<asp:ListItem Value="5" Class="problems checkboxesForToggling">Blemish marks / scars</asp:ListItem>
              							<asp:ListItem Value="6" Class="problems checkboxesForToggling">Crows feet around eyes</asp:ListItem>
              							<asp:ListItem Value="7" Class="problems checkboxesForToggling">Dark circles under eyes</asp:ListItem>
              							<asp:ListItem Value="8" Class="problems checkboxesForToggling">Deep lines / wrinkles</asp:ListItem>
              							<asp:ListItem Value="9" Class="problems checkboxesForToggling">Facial hair</asp:ListItem>
              							<asp:ListItem Value="10" Class="problems checkboxesForToggling">Facial skin discoloration</asp:ListItem>
              							<asp:ListItem Value="11" Class="problems checkboxesForToggling">Fine lines / wrinkles</asp:ListItem>
              							<asp:ListItem Value="12" Class="problems checkboxesForToggling">Lack of firmness</asp:ListItem>
              							<asp:ListItem Value="13" Class="problems checkboxesForToggling">Large / enlarged pores</asp:ListItem>
              							<asp:ListItem Value="14" Class="problems checkboxesForToggling">Oily / shiny areas</asp:ListItem>
              							<asp:ListItem Value="15" Class="problems checkboxesForToggling">Puffy eyes</asp:ListItem>
              							<asp:ListItem Value="16" Class="problems checkboxesForToggling">Redness / rosacea</asp:ListItem>
              							<asp:ListItem Value="17" Class="problems checkboxesForToggling">Sensitive skin</asp:ListItem>
              							<asp:ListItem Value="18" Class="problems checkboxesForToggling">Sun damage</asp:ListItem>
              							<asp:ListItem Value="19" Class="problems checkboxesForToggling">Uneven skin texture</asp:ListItem>
              							<asp:ListItem Value="20" Class="problems checkboxesForToggling">Uneven skin tone</asp:ListItem>		
              							<asp:ListItem Value="21" ID="problemsNone" Class="checkboxesForToggling" onclick="toggleCheckboxes('problems');">None of the above</asp:ListItem>
                        </asp:CheckBoxList>
                        <CC1:RequiredFieldValidatorForCheckBoxLists ControlToValidate="FaceProblems" runat="server"
                            ID="FaceProblemsValidator" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg">Please check at least one box.
                        </CC1:RequiredFieldValidatorForCheckBoxLists>
                    </div>
                    <div>
                        <asp:Label ID="SkinTypeLbl" runat="server" Text="How would you describe your skin type?* (select one)"
                            AssociatedControlID="SkinType" CssClass="ddlabels"></asp:Label>
                        <asp:DropDownList ID="SkinType" runat="server">
                            <asp:ListItem Value="">Select..</asp:ListItem>
                            <asp:ListItem Value="1">Dry</asp:ListItem>
                            <asp:ListItem Value="2">Normal</asp:ListItem>
                            <asp:ListItem Value="3">Oily</asp:ListItem>
                            <asp:ListItem Value="4">Combination Normal to Oily</asp:ListItem>
                            <asp:ListItem Value="5">Combination Normal to Dry</asp:ListItem>
                            <asp:ListItem Value="6">Combination Oily-Dry</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="SkinTypeValidator" runat="server" ErrorMessage="Please answer all questions."
                            ControlToValidate="SkinType" EnableClientScript="true" SetFocusOnError="true"
                            CssClass="errormsg"></asp:RequiredFieldValidator>
                    </div>
                    <div>
                        <asp:Label ID="RaceLbl" runat="server" Text="What is your racial background?" AssociatedControlID="Race"
                            CssClass="ddlabels"></asp:Label>
                        <asp:DropDownList ID="Race" runat="server">
                            <asp:ListItem Value="0">Select...</asp:ListItem>
                            <asp:ListItem Value="1">White or Caucasian</asp:ListItem>
                            <asp:ListItem Value="2">Black or African-American</asp:ListItem>
                            <asp:ListItem Value="3">Asian or Pacific Islander</asp:ListItem>
                            <asp:ListItem Value="4">American Indian or Alaska Native</asp:ListItem>
                            <asp:ListItem Value="5">Other</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div>
                        <asp:Label ID="SpanishLbl" runat="server" Text="Are you Spanish, Hispanic, or Latino/a?"
                            AssociatedControlID="Spanish" CssClass="ddlabels"></asp:Label>
                        <asp:DropDownList ID="Spanish" runat="server">
                            <asp:ListItem Value="0">Select...</asp:ListItem>
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                            <asp:ListItem Value="2">No</asp:ListItem>
                        </asp:DropDownList>
                        
                        <asp:HiddenField ID="MemberID" Value="" runat="server" />
                        <asp:HiddenField ID="Email" Value="" runat="server" />
                    </div>
                    <div id="optin_checkboxes">
                        <p>
                            Your privacy is important to us. You can trust that Kao Brands will not share your
                            personal information with other companies (see our <a href="http://www.kaobrands.com/privacy_policy.asp"
                                target="_blank" style="color: #40A7C6;">Privacy Policy</a>).</p>
                        <%--                    <p>
                        <asp:CheckBox Checked="false" ID="BioreOptin" runat="server" />Yes, tell me about future
                        Bior&eacute;&reg; product news and offerings.</p>
                    <p>
                        <asp:CheckBox ID="MultiOptin" runat="server" />Yes, I'd like to receive emails and
                        newsletters from other great products from Kao Brands Company:</p>
				    <p class="optinLine">
                        <asp:CheckBox ID="banoptin" runat="server" /><a href="http://www.feelbanfresh.com"
                         target="_blank">Ban&reg;</a><br />
                        Ban&reg; antiperspirant and deodorant products keep you 3x cooler and fresher<sup>&#134;</sup>
                        <br /><span class="ban_disc"><sup>&#134;</sup> vs. ordinary invisible solids</span></p>
				    <p class="optinLine">
                        <asp:CheckBox ID="cureloptin" runat="server" /><a href="http://www.curel.com/index.asp"
                            target="_blank">Cur&eacute;l&reg;</a><br />
                        Cur&eacute;l&reg; Skincare's full line of hand and body moisturizers delivers freedom<br />from dry skin.</p>
                    <p class="optinLine">
                        <asp:CheckBox ID="jergensoptin" runat="server" /><a href="http://www.jergens.com/default.asp"
                            target="_blank">Jergens&reg;</a><br />
                        Jergens&reg; Skincare collection of moisturizers delivers a natural glow, smooth
                        and firm skin and an allure that captivates.</p>
                    <p class="optinLine">
                        <asp:CheckBox ID="jfoptin" runat="server" /><a href="http://www.johnfrieda.com/index.asp"
                            target="_blank">John Frieda&reg;</a><br />
                        John Frieda&reg; provides solutions for hair care problems, resulting in hair that looks fabulous.</p>--%>
                    </div>
                </div>
                <p>
                    <asp:Button UseSubmitBehavior="true" ID="submit" Text="Sign Me Up" runat="server"
                        CssClass="submit" />
                </p>
            </asp:Panel>
            <asp:Panel ID="CouponFormSuccess" runat="server" Visible="false">
                <h2>
                    Thanks!</h2>
                <p>
                    Thanks for telling us more about yourself and completing our online survey. You
                    can print your $2.00 coupon offer by clicking the link below.
                </p>
                <p>
                    <asp:HyperLink ID="CouponLink2" runat="server"><strong>Download your coupon now</strong></asp:HyperLink></p>
            </asp:Panel>
            <asp:Panel ID="OptinFormFailure" runat="server" Visible="false">
                <h2>
                    We're Sorry...</h2>
                <p>
                    This offer is only valid to Bior&eacute;&reg; members and is limited
                    to one per person. If you are not a Bior&eacute;&reg; member, click <a href="/usa/optin.aspx">
                        here</a> to become one.</p>
            </asp:Panel>
            <asp:Panel ID="TechnicalProblem" runat="server" Visible="false">
                <h2>
                    We're Sorry...</h2>
                <p>
                    There has been a technical problem with your entry. Please go back and
                        try again.
                </p>
            </asp:Panel>
        </div>
    </div>
    <!-- end #content -->
    <div id="content_bottom">
    </div>
</asp:Content>
