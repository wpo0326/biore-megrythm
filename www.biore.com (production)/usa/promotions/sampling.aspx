<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true"
    CodeFile="sampling.aspx.cs" Inherits="promotions_sampling" Title="Untitled Page" %>

<%@ MasterType VirtualPath="../MasterPages/Main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%@ register tagprefix="CC1" namespace="CustomValidators" assembly="CustomValidators" %>
    <meta name="Description" content="Contact the Biore skincare experts." />
    <meta name="Keywords" content="Biore, contact Biore, email Biore, mail Biore,, phone Biore, address, phone number, toll free, call Biore, write Biore" />
    <meta name="DCSext.submit" content="true" runat="server" visible="false" id="metaSubmit" />
    <meta name="DCSext.curelOptin" content="true" runat="server" visible="false" id="metaCurelOptin" />
    <meta name="DCSext.jergensOptin" content="true" runat="server" visible="false" id="metaJergensOptin" />
    <meta name="DCSext.banOptin" content="true" runat="server" visible="false" id="metaBanOptin" />
    <meta name="DCSext.bioreOptin" content="true" runat="server" visible="false" id="metaBioreOptin" />
    <meta name="DCSext.johnFriedaOptin" content="true" runat="server" visible="false" id="metaJohnFriedaOptin" />
    <link rel="stylesheet" type="text/css" href="../css/optin.css" media="screen, projection" />
    <!--[if lt IE 7]>
	<script>
	    DD_belatedPNG.fix('#optin_inner, #ctl00_ContentPlaceHolder1_OptinForm h1');
	</script>
	<![endif]-->

    <script type="text/javascript">
        $(document).ready(function() {
            $("input:visible:first").focus();
            $("#<%=MultiOptin.ClientID%>").click(
            function() {
                if ($('#<%=MultiOptin.ClientID%>:checked').val()) {
                    $("#<%=banoptin.ClientID%>").attr("checked", "true");
                    $("#<%=cureloptin.ClientID%>").attr("checked", "true");
                    $("#<%=jergensoptin.ClientID%>").attr("checked", "true");
                    $("#<%=jfoptin.ClientID%>").attr("checked", "true");
                    $("#<%=banoptin.ClientID%>").removeAttr("disabled");
                    $("#<%=cureloptin.ClientID%>").removeAttr("disabled");
                    $("#<%=jergensoptin.ClientID%>").removeAttr("disabled");
                    $("#<%=jfoptin.ClientID%>").removeAttr("disabled");
                }
                else {
                    $("#<%=banoptin.ClientID%>").attr("disabled", "true");
                    $("#<%=cureloptin.ClientID%>").attr("disabled", "true");
                    $("#<%=jergensoptin.ClientID%>").attr("disabled", "true");
                    $("#<%=jfoptin.ClientID%>").attr("disabled", "true");
                }
            });
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="SelectForm" Visible="true" runat="server">
        <div id="optin">
            <div id="optin_inner">
                <div id="imgContact">
                </div>
                <asp:Panel ID="OptinForm" runat="server">
                  <asp:Panel ID="fs_headTxt" runat="server" runat="server" Visible="false">
                    <h1 title="Get Your Free Sample">
                        Get Your Free Sample</h1>
                    <h2>
                        Thanks for your interest in the Bior&eacute;<sup>&reg;</sup> Skincare brand!</h2>
                    <p>
                        Thank you for your continued interest in Bior&eacute;&reg; Skincare products. We want to know more about you! Please complete the form below and we'll send you a sample of our Makeup Removing Towelette. We hope you love it!

</p>
                    <p class="req">
                        <em>required</em>*</p>
                   </asp:Panel>
                   <asp:Panel ID="psSamp_headTxt" runat="server" Visible="false">
                    <h1 title="Sign Me Up">Sign Me Up</h1>
                    <h2>Thank you for your continued interest in Bior&eacute;&reg; Skincare products.</h2>
                    <p> We want to know more about you! Please complete the form below to request your sample of one Bior&eacute;&reg; Pore Strips. Quantities are limited; we only have 2,500 to give away. You must be 18 years or older to participate. Samples are available only while supplies last. Request yours now! </p>
                    <p class="req">
                        <em>required</em>*</p>
                   </asp:Panel>
                   <asp:Panel ID="default_headTxt" runat="server" runat="server" Visible="false">
                    <h1 title=""></h1>
                    <h2>Thanks for your interest in the Bior&eacute;<sup>&reg;</sup> brand!</h2>
                    <p>Thank you for your continued interest in Bior&eacute;&reg; Skincare products. We
                        want to know more about you! Please complete the form below.</p>
                    <p class="req"><em>required</em>*</p>
                   </asp:Panel>
                    <div id="optinPart1">
                        <div>
                            <asp:Label ID="FNameLbl" runat="server" Text="First Name*" AssociatedControlID="FName"></asp:Label>
                            <asp:TextBox ID="FName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="FNameValidator1" runat="server" ErrorMessage="Please enter your First Name."
                                ControlToValidate="FName" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="FNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your First Name."
                                ValidationExpression="^[^<>]+$" ControlToValidate="FName" EnableClientScript="true"
                                SetFocusOnError="true" CssClass="errormsg"></asp:RegularExpressionValidator>
                        </div>
                        <div>
                            <asp:Label ID="LNameLbl" runat="server" Text="Last Name*" AssociatedControlID="LName"></asp:Label>
                            <asp:TextBox ID="LName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="LNameValidator1" runat="server" ErrorMessage="Please enter your Last Name."
                                ControlToValidate="LName" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="LNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Last Name."
                                ValidationExpression="^[^<>]+$" ControlToValidate="LName" SetFocusOnError="true"
                                CssClass="errormsg"></asp:RegularExpressionValidator>
                        </div>
                        <div>
                            <asp:Label ID="EmailLbl" runat="server" Text="Email*" AssociatedControlID="Email"></asp:Label>
                            <asp:TextBox ID="Email" runat="server" MaxLength="150"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="EmailValidator1" runat="server" ErrorMessage="Please enter your Email Address."
                                ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="EmailValidator2" runat="server" ErrorMessage="Please enter a valid Email Address."
                                ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RegularExpressionValidator>
                        </div>
                        <div>
                            <asp:Label ID="Address1Lbl" runat="server" Text="Address*" AssociatedControlID="Address1"></asp:Label>
                            <asp:TextBox ID="Address1" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="Address1Validator1" runat="server" ErrorMessage="Please enter your Address."
                                ControlToValidate="Address1" EnableClientScript="true" SetFocusOnError="true"
                                CssClass="errormsg"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="Address1Validator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Address."
                                ValidationExpression="^[^<>]+$" ControlToValidate="Address1" SetFocusOnError="true"
                                CssClass="errormsg"></asp:RegularExpressionValidator>
                        </div>
                        <asp:Panel ID="addr2" runat="server">
                            <asp:Label ID="Address2Lbl" runat="server" Text="&nbsp;" AssociatedControlID="Address2"></asp:Label>
                            <asp:TextBox ID="Address2" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                        </asp:Panel>
                        <asp:Panel ID="addr3" runat="server">
                            <asp:Label ID="Address3Lbl" runat="server" Text="&nbsp;" AssociatedControlID="Address3"></asp:Label>
                            <asp:TextBox ID="Address3" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                        </asp:Panel>
                        <div>
                            <asp:Label ID="CityLbl" runat="server" Text="City*" AssociatedControlID="City"></asp:Label>
                            <asp:TextBox ID="City" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="CityValidator1" runat="server" ErrorMessage="Please enter your City."
                                ControlToValidate="City" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="CityValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your City."
                                ValidationExpression="^[^<>]+$" ControlToValidate="City" SetFocusOnError="true"
                                CssClass="errormsg"></asp:RegularExpressionValidator>
                        </div>
                        <div>
                            <asp:Label ID="StateLbl" runat="server" Text="State*" AssociatedControlID="State"></asp:Label>
                            <asp:DropDownList ID="State" runat="server">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="1">Alabama</asp:ListItem>
                                <asp:ListItem Value="2">Alaska</asp:ListItem>
                                <asp:ListItem Value="4">Arizona</asp:ListItem>
                                <asp:ListItem Value="5">Arkansas</asp:ListItem>
                                <asp:ListItem Value="9">California</asp:ListItem>
                                <asp:ListItem Value="10">Colorado</asp:ListItem>
                                <asp:ListItem Value="11">Connecticut</asp:ListItem>
                                <asp:ListItem Value="12">Delaware</asp:ListItem>
                                <asp:ListItem Value="13">District Of Columbia</asp:ListItem>
                                <asp:ListItem Value="15">Florida</asp:ListItem>
                                <asp:ListItem Value="16">Georgia</asp:ListItem>
                                <asp:ListItem Value="17">Hawaii</asp:ListItem>
                                <asp:ListItem Value="18">Idaho</asp:ListItem>
                                <asp:ListItem Value="19">Illinois</asp:ListItem>
                                <asp:ListItem Value="20">Indiana</asp:ListItem>
                                <asp:ListItem Value="21">Iowa</asp:ListItem>
                                <asp:ListItem Value="22">Kansas</asp:ListItem>
                                <asp:ListItem Value="23">Kentucky</asp:ListItem>
                                <asp:ListItem Value="24">Louisiana</asp:ListItem>
                                <asp:ListItem Value="25">Maine</asp:ListItem>
                                <asp:ListItem Value="27">Maryland</asp:ListItem>
                                <asp:ListItem Value="28">Massachusetts</asp:ListItem>
                                <asp:ListItem Value="29">Michigan</asp:ListItem>
                                <asp:ListItem Value="30">Minnesota</asp:ListItem>
                                <asp:ListItem Value="31">Mississippi</asp:ListItem>
                                <asp:ListItem Value="32">Missouri</asp:ListItem>
                                <asp:ListItem Value="33">Montana</asp:ListItem>
                                <asp:ListItem Value="34">Nebraska</asp:ListItem>
                                <asp:ListItem Value="35">Nevada</asp:ListItem>
                                <asp:ListItem Value="36">New Hampshire</asp:ListItem>
                                <asp:ListItem Value="37">New Jersey</asp:ListItem>
                                <asp:ListItem Value="38">New Mexico</asp:ListItem>
                                <asp:ListItem Value="39">New York</asp:ListItem>
                                <asp:ListItem Value="40">North Carolina</asp:ListItem>
                                <asp:ListItem Value="41">North Dakota</asp:ListItem>
                                <asp:ListItem Value="43">Ohio</asp:ListItem>
                                <asp:ListItem Value="44">Oklahoma</asp:ListItem>
                                <asp:ListItem Value="45">Oregon</asp:ListItem>
                                <asp:ListItem Value="47">Pennsylvania</asp:ListItem>
                                <asp:ListItem Value="49">Rhode Island</asp:ListItem>
                                <asp:ListItem Value="50">South Carolina</asp:ListItem>
                                <asp:ListItem Value="51">South Dakota</asp:ListItem>
                                <asp:ListItem Value="52">Tennessee</asp:ListItem>
                                <asp:ListItem Value="53">Texas</asp:ListItem>
                                <asp:ListItem Value="54">Utah</asp:ListItem>
                                <asp:ListItem Value="55">Vermont</asp:ListItem>
                                <asp:ListItem Value="57">Virginia</asp:ListItem>
                                <asp:ListItem Value="58">Washington</asp:ListItem>
                                <asp:ListItem Value="59">West Virginia</asp:ListItem>
                                <asp:ListItem Value="60">Wisconsin</asp:ListItem>
                                <asp:ListItem Value="61">Wyoming</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="StateValidator" runat="server" ErrorMessage="Please select your State."
                                ControlToValidate="State" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                        </div>
                        <div>
                            <asp:Label ID="PostalCodeLbl" runat="server" Text="ZIP Code*" AssociatedControlID="PostalCode"></asp:Label>
                            <asp:TextBox ID="PostalCode" runat="server" MaxLength="5" CssClass="inputTextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="PostalCodeValidator1" runat="server" ErrorMessage="Please enter your ZIP Code"
                                ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true"
                                CssClass="errormsg"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="PostalCodeValidator2" runat="server" ErrorMessage="Please enter a valid 5-digit ZIP code."
                                ValidationExpression="^\d{5}$" ControlToValidate="PostalCode" EnableClientScript="true"
                                SetFocusOnError="true" CssClass="errormsg"></asp:RegularExpressionValidator>
                        </div>
                        <div class="country">
                            <asp:Label ID="CountryLbl" runat="server" Text="Country&nbsp;"></asp:Label>
                            <span class="fake_input">United States</span>
                        </div>
                        <div>
                            <asp:Label ID="GenderLbl" runat="server" Text="Gender*" AssociatedControlID="Gender"></asp:Label>
                            <asp:DropDownList ID="Gender" runat="server">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="1">Female</asp:ListItem>
                                <asp:ListItem Value="2">Male</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="GenderValidator" runat="server" ErrorMessage="Please enter your Gender."
                                ControlToValidate="Gender" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                        </div>
                        <div class="bd">
                            <asp:Label ID="DOBLbl" runat="server" Text="Birthdate*"></asp:Label>
                            <asp:DropDownList ID="yyyy" runat="server" CssClass="inputSelectBDay">
                                <asp:ListItem Value="">Year</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="yyyyValidator" runat="server" ErrorMessage="Please enter your birth Year."
                                ControlToValidate="yyyy" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg year"></asp:RequiredFieldValidator>
                            <asp:DropDownList ID="mm" runat="server" CssClass="inputSelectBDay">
                                <asp:ListItem Value="">Mon</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="mmValidator" runat="server" ErrorMessage="Please enter your birth Month."
                                ControlToValidate="mm" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg month"></asp:RequiredFieldValidator>
                            <asp:DropDownList ID="dd" runat="server" CssClass="inputSelectBDay">
                                <asp:ListItem Value="">Day</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="ddValidator" runat="server" ErrorMessage="Please enter your birth Day."
                                ControlToValidate="dd" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg day"></asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="DOBValidator" runat="server" ErrorMessage="Please enter a valid date."
                                OnServerValidate="DOBValidate" EnableClientScript="false" CssClass="errormsg"></asp:CustomValidator>
                                <asp:CustomValidator ID="UnderageValidator" runat="server" ErrorMessage="You must be 18 or older."
                                OnServerValidate="DOBUnderage" EnableClientScript="false" CssClass="errormsg"></asp:CustomValidator>
                        </div>
                    </div>
                    <div id="optinPart2">
                        <div>
                            <asp:Label ID="ProdUsedDailyLbl" runat="server" Text="How many skin care products do you use on a daily basis?*"
                                AssociatedControlID="ProdUsedDaily" CssClass="ddlabels"></asp:Label>
                            <asp:DropDownList ID="ProdUsedDaily" runat="server">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5 or more</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="ProdUsedDailyValidator" runat="server" ErrorMessage="Please answer all questions."
                                ControlToValidate="ProdUsedDaily" EnableClientScript="true" SetFocusOnError="true"
                                CssClass="errormsg"></asp:RequiredFieldValidator>
                        </div>
                        <div>
                            <asp:Label ID="PayMoreAttentionLbl" runat="server" Text="I pay more attention/spend more time on my skin than other people.*"
                                AssociatedControlID="PayMoreAttention" CssClass="ddlabels"></asp:Label>
                            <asp:DropDownList ID="PayMoreAttention" runat="server">
                                <asp:ListItem Value="">Select...</asp:ListItem>
                                <asp:ListItem Value="1">Agree strongly</asp:ListItem>
                                <asp:ListItem Value="2">Agree somewhat</asp:ListItem>
                                <asp:ListItem Value="3">Neither agree nor disagree</asp:ListItem>
                                <asp:ListItem Value="4">Disagree somewhat</asp:ListItem>
                                <asp:ListItem Value="5">Disagree strongly</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="PayMoreAttentionValidator" runat="server" ErrorMessage="Please answer all questions."
                                ControlToValidate="PayMoreAttention" EnableClientScript="true" SetFocusOnError="true"
                                CssClass="errormsg"></asp:RequiredFieldValidator>
                        </div>
                        <div>
                            <asp:Label ID="WillingToPayMoreLbl" runat="server" Text="I am willing to pay more for skin care products I really want.*"
                                AssociatedControlID="WillingToPayMore" CssClass="ddlabels"></asp:Label>
                            <asp:DropDownList ID="WillingToPayMore" runat="server">
                                <asp:ListItem Value="">Select...</asp:ListItem>
                                <asp:ListItem Value="1">Agree strongly</asp:ListItem>
                                <asp:ListItem Value="2">Agree somewhat</asp:ListItem>
                                <asp:ListItem Value="3">Neither agree nor disagree</asp:ListItem>
                                <asp:ListItem Value="4">Disagree somewhat</asp:ListItem>
                                <asp:ListItem Value="5">Disagree strongly</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="WillingToPayMoreValidator" runat="server" ErrorMessage="Please answer all questions."
                                ControlToValidate="WillingToPayMore" EnableClientScript="true" SetFocusOnError="true"
                                CssClass="errormsg"></asp:RequiredFieldValidator>
                        </div>
                        <div>
                            <asp:Label ID="RecommendFriendsLbl" runat="server" Text="How likely are you to recommend your favorite skin care products to friends?*"
                                AssociatedControlID="RecommendFriends" CssClass="ddlabels"></asp:Label>
                            <asp:DropDownList ID="RecommendFriends" runat="server">
                                <asp:ListItem Value="">Select...</asp:ListItem>
                                <asp:ListItem Value="1">Very likely</asp:ListItem>
                                <asp:ListItem Value="2">Somewhat likely</asp:ListItem>
                                <asp:ListItem Value="3">Somewhat unlikely</asp:ListItem>
                                <asp:ListItem Value="4">Not at all likely</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RecommendFriendsValidator" runat="server" ErrorMessage="Please answer all questions."
                                ControlToValidate="RecommendFriends" EnableClientScript="true" SetFocusOnError="true"
                                CssClass="errormsg"></asp:RequiredFieldValidator>
                        </div>
                        <div class="checkboxDiv">
                            <asp:Label ID="ProductsUsingLbl" runat="server" Text="Which Bior&eacute;&reg; products do you currently use?*  (select all that apply)"
                                AssociatedControlID="ProductsUsing" CssClass="checkboxHdrLabel"></asp:Label>
                            <asp:CheckBoxList ID="ProductsUsing" runat="server" RepeatLayout="Flow">
                                <asp:ListItem Value="21" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Steam Activated Cleanser</asp:ListItem>
                  							<asp:ListItem Value="22" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Makeup Removing Towelettes</asp:ListItem>
                  							<asp:ListItem Value="6" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Revitalize 4-in-1 Self-Foaming Cleanser</asp:ListItem>
                  							<asp:ListItem Value="7" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Detoxify Daily Scrub</asp:ListItem>
                  							<asp:ListItem Value="8" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Refresh Daily Cleansing Cloths</asp:ListItem>
                  							<asp:ListItem Value="9" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Purify Self-Heating Mask</asp:ListItem>
                  							<asp:ListItem Value="10" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Restore Skin-Boosting Night Serum</asp:ListItem>
                  							<asp:ListItem Value="11" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Enliven Cooling Eye Gel</asp:ListItem>
                  							<asp:ListItem Value="12" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Nourish Moisture Lotion SPF 15</asp:ListItem>
                  							<asp:ListItem Value="14" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Blemish Fighting Ice Cleanser</asp:ListItem>
                  							<asp:ListItem Value="13" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Pore Unclogging Scrub</asp:ListItem>
                  							<asp:ListItem Value="16" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Warming Anti-Blackhead Cream Cleanser</asp:ListItem>
                  							<asp:ListItem Value="15" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Triple Action Astringent</asp:ListItem>
                  							<asp:ListItem Value="19" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Ultra Deep Cleansing Pore Strips</asp:ListItem>
                  							<asp:ListItem Value="17" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Deep Cleansing Pore Strips</asp:ListItem>
                  							<asp:ListItem Value="18" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Combo Pack Deep Cleansing Pore Strips</asp:ListItem>
                  							<asp:ListItem Value="2" Class="currUse checkboxesForToggling">Bior&eacute;&reg; See the Future&reg; Fortifying Eye Cream</asp:ListItem>
                  							<asp:ListItem Value="3" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Hard Day's Night&reg; Overnight Moisturizer</asp:ListItem>                             
                  							<asp:ListItem Value="4" Class="currUse checkboxesForToggling">Bior&eacute;&reg; Even Smoother&reg; Microderm Exfoliator</asp:ListItem>
                  							<asp:ListItem Value="20" ID="currUseNone" Class="checkboxesForToggling" onclick="toggleCheckboxes('currUse');">None of the above</asp:ListItem>
                            </asp:CheckBoxList>
                            <CC1:RequiredFieldValidatorForCheckBoxLists ControlToValidate="ProductsUsing" runat="server"
                                ID="ProductsUsingValidator" EnableClientScript="true" SetFocusOnError="true"
                                CssClass="errormsg">Please check at least one box.
                        </CC1:RequiredFieldValidatorForCheckBoxLists>
                        </div>
                        <div>
                            <asp:Label ID="MostUsedBrandLbl" runat="server" Text="What brand of face care products do you use most often?* (select one)"
                                AssociatedControlID="MostUsedBrand" CssClass="ddlabels"></asp:Label>
                            <asp:DropDownList ID="MostUsedBrand" runat="server">
                                <asp:ListItem Value="">Select...</asp:ListItem>
                                <asp:ListItem Value="1">Aveeno&reg;</asp:ListItem>
                                <asp:ListItem Value="2">Avon</asp:ListItem>
                                <asp:ListItem Value="3">Bior&eacute;&reg; products</asp:ListItem>
                                <asp:ListItem Value="4">Cetaphil&reg;</asp:ListItem>
                                <asp:ListItem Value="5">Clean &amp; Clear&reg;</asp:ListItem>
                                <asp:ListItem Value="6">Clearasil&reg;</asp:ListItem>
                                <asp:ListItem Value="7">Clinique</asp:ListItem>
                                <asp:ListItem Value="8">Dove&reg;</asp:ListItem>
                                <asp:ListItem Value="9">Garnier Nutritioniste</asp:ListItem>
                                <asp:ListItem Value="10">L'Oreal&reg;</asp:ListItem>
                                <asp:ListItem Value="11">Mary Kay&reg;</asp:ListItem>
                                <asp:ListItem Value="12">Neutrogena&reg;</asp:ListItem>
                                <asp:ListItem Value="13">Noxzema&reg;</asp:ListItem>
                                <asp:ListItem Value="14">Olay&reg;</asp:ListItem>
                                <asp:ListItem Value="15">ProActiv&reg; Solutions</asp:ListItem>
                                <asp:ListItem Value="16">St. Ives&reg;</asp:ListItem>
                                <asp:ListItem Value="17">Store Brand</asp:ListItem>
                                <asp:ListItem Value="18">Other Department Store brand</asp:ListItem>
                                <asp:ListItem Value="19">Other</asp:ListItem>
                                <asp:ListItem Value="20">None of these</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="MostUsedBrandValidator" runat="server" ErrorMessage="Please answer all questions."
                                ControlToValidate="MostUsedBrand" EnableClientScript="true" SetFocusOnError="true"
                                CssClass="errormsg"></asp:RequiredFieldValidator>
                        </div>
                        <div class="checkboxDiv">
                            <asp:Label ID="TypesProductsUsedLbl" runat="server" Text="Which of the following types of products do you typically use?* (select all that apply)"
                                AssociatedControlID="TypesProductsUsed" CssClass="checkboxHdrLabel"></asp:Label>
                            <asp:CheckBoxList ID="TypesProductsUsed" runat="server" RepeatLayout="Flow">
                                <asp:ListItem Value="1" Class="typUse checkboxesForToggling">Liquid/Cream/Gel cleanser</asp:ListItem>
                  							<asp:ListItem Value="2" Class="typUse checkboxesForToggling">Cleansing Cloths</asp:ListItem>
                  							<asp:ListItem Value="3" Class="typUse checkboxesForToggling">Facial moisturizing lotion/cream</asp:ListItem>
                  							<asp:ListItem Value="4" Class="typUse checkboxesForToggling">Facial moisturizing lotion/cream with SPF</asp:ListItem>
                  							<asp:ListItem Value="5" Class="typUse checkboxesForToggling">Facial scrub/exfoliator</asp:ListItem>
                  							<asp:ListItem Value="6" Class="typUse checkboxesForToggling">Acne/Blemish spot treatment</asp:ListItem>
                  							<asp:ListItem Value="7" Class="typUse checkboxesForToggling">Skin discoloration treatment product (sun damage)</asp:ListItem>
                  							<asp:ListItem Value="8" Class="typUse checkboxesForToggling">Eye gel/cream/treatment</asp:ListItem>
                  							<asp:ListItem Value="9" Class="typUse checkboxesForToggling">Night cream/ overnight moisturizer</asp:ListItem>
                  							<asp:ListItem Value="10" Class="typUse checkboxesForToggling">Make-up remover</asp:ListItem>
                  							<asp:ListItem Value="11" ID="typUseNone" Class="checkboxesForToggling" onclick="toggleCheckboxes('typUse');">None of the above</asp:ListItem>
                            </asp:CheckBoxList>
                            <CC1:RequiredFieldValidatorForCheckBoxLists ControlToValidate="TypesProductsUsed"
                                runat="server" ID="TypesProductsUsedValidator" EnableClientScript="true" SetFocusOnError="true"
                                CssClass="errormsg">Please check at least one box.
                        </CC1:RequiredFieldValidatorForCheckBoxLists>
                        </div>
                        <div class="checkboxDiv">
                            <asp:Label ID="ProductAttributesLbl" runat="server" Text="Which, if any, of the following attributes would you be interested in in a face care product?* (select all that apply)"
                                AssociatedControlID="ProductAttributes" CssClass="checkboxHdrLabel"></asp:Label>
                            <asp:CheckBoxList ID="ProductAttributes" runat="server" RepeatLayout="Flow">
                                <asp:ListItem Value="1" Class="attr checkboxesForToggling">Convenience</asp:ListItem>
                  							<asp:ListItem Value="2" Class="attr checkboxesForToggling">Anti-aging</asp:ListItem>
                  							<asp:ListItem Value="3" Class="attr checkboxesForToggling">Deep cleaning</asp:ListItem>
                  							<asp:ListItem Value="4" Class="attr checkboxesForToggling">Gentle / sensitive skin</asp:ListItem>
                  							<asp:ListItem Value="5" Class="attr checkboxesForToggling">For acne-prone skin</asp:ListItem>
                  							<asp:ListItem Value="6" Class="attr checkboxesForToggling">For combination skin</asp:ListItem>
                  							<asp:ListItem Value="7" Class="attr checkboxesForToggling">Makeup removal / enhancement</asp:ListItem>
                  							<asp:ListItem Value="8" ID="attrNone" Class="checkboxesForToggling" onclick="toggleCheckboxes('attr');">None of the above</asp:ListItem>
                            </asp:CheckBoxList>
                            <CC1:RequiredFieldValidatorForCheckBoxLists ControlToValidate="ProductAttributes"
                                runat="server" ID="ProductAttributesValidator" EnableClientScript="true" SetFocusOnError="true"
                                CssClass="errormsg">Please check at least one box.
                        </CC1:RequiredFieldValidatorForCheckBoxLists>
                        </div>
                        <div class="checkboxDiv">
                            <asp:Label ID="FaceProblemsLbl" runat="server" Text="Which, if any, of the following face care problems would you say you are extremely
                        or very concerned with?* (select all that apply)" AssociatedControlID="FaceProblems"
                                CssClass="checkboxHdrLabel"></asp:Label>
                            <asp:CheckBoxList ID="FaceProblems" runat="server" RepeatLayout="Flow">
                                <asp:ListItem Value="1" Class="problems checkboxesForToggling">Acne</asp:ListItem>
                  							<asp:ListItem Value="2" Class="problems checkboxesForToggling">Age spots</asp:ListItem>
                  							<asp:ListItem Value="3" Class="problems checkboxesForToggling">Blackheads</asp:ListItem>
                  							<asp:ListItem Value="4" Class="problems checkboxesForToggling">Blemishes / pimples</asp:ListItem>
                  							<asp:ListItem Value="5" Class="problems checkboxesForToggling">Blemish marks / scars</asp:ListItem>
                  							<asp:ListItem Value="6" Class="problems checkboxesForToggling">Crows feet around eyes</asp:ListItem>
                  							<asp:ListItem Value="7" Class="problems checkboxesForToggling">Dark circles under eyes</asp:ListItem>
                  							<asp:ListItem Value="8" Class="problems checkboxesForToggling">Deep lines / wrinkles</asp:ListItem>
                  							<asp:ListItem Value="9" Class="problems checkboxesForToggling">Facial hair</asp:ListItem>
                  							<asp:ListItem Value="10" Class="problems checkboxesForToggling">Facial skin discoloration</asp:ListItem>
                  							<asp:ListItem Value="11" Class="problems checkboxesForToggling">Fine lines / wrinkles</asp:ListItem>
                  							<asp:ListItem Value="12" Class="problems checkboxesForToggling">Lack of firmness</asp:ListItem>
                  							<asp:ListItem Value="13" Class="problems checkboxesForToggling">Large / enlarged pores</asp:ListItem>
                  							<asp:ListItem Value="14" Class="problems checkboxesForToggling">Oily / shiny areas</asp:ListItem>
                  							<asp:ListItem Value="15" Class="problems checkboxesForToggling">Puffy eyes</asp:ListItem>
                  							<asp:ListItem Value="16" Class="problems checkboxesForToggling">Redness / rosacea</asp:ListItem>
                  							<asp:ListItem Value="17" Class="problems checkboxesForToggling">Sensitive skin</asp:ListItem>
                  							<asp:ListItem Value="18" Class="problems checkboxesForToggling">Sun damage</asp:ListItem>
                  							<asp:ListItem Value="19" Class="problems checkboxesForToggling">Uneven skin texture</asp:ListItem>
                  							<asp:ListItem Value="20" Class="problems checkboxesForToggling">Uneven skin tone</asp:ListItem>		
                  							<asp:ListItem Value="21" ID="problemsNone" Class="checkboxesForToggling" onclick="toggleCheckboxes('problems');">None of the above</asp:ListItem>
                            </asp:CheckBoxList>
                            <CC1:RequiredFieldValidatorForCheckBoxLists ControlToValidate="FaceProblems" runat="server"
                                ID="FaceProblemsValidator" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg">Please check at least one box.
                        </CC1:RequiredFieldValidatorForCheckBoxLists>
                        </div>
                        <div>
                            <asp:Label ID="SkinTypeLbl" runat="server" Text="How would you describe your skin type?* (select one)"
                                AssociatedControlID="SkinType" CssClass="ddlabels"></asp:Label>
                            <asp:DropDownList ID="SkinType" runat="server">
                                <asp:ListItem Value="">Select..</asp:ListItem>
                                <asp:ListItem Value="1">Dry</asp:ListItem>
                                <asp:ListItem Value="2">Normal</asp:ListItem>
                                <asp:ListItem Value="3">Oily</asp:ListItem>
                                <asp:ListItem Value="4">Combination Normal to Oily</asp:ListItem>
                                <asp:ListItem Value="5">Combination Normal to Dry</asp:ListItem>
                                <asp:ListItem Value="6">Combination Oily-Dry</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="SkinTypeValidator" runat="server" ErrorMessage="Please answer all questions."
                                ControlToValidate="SkinType" EnableClientScript="true" SetFocusOnError="true"
                                CssClass="errormsg"></asp:RequiredFieldValidator>
                        </div>
                        <div>
                            <asp:Label ID="RaceLbl" runat="server" Text="What is your racial background?" AssociatedControlID="Race"
                                CssClass="ddlabels"></asp:Label>
                            <asp:DropDownList ID="Race" runat="server">
                                <asp:ListItem Value="0">Select...</asp:ListItem>
                                <asp:ListItem Value="1">White or Caucasian</asp:ListItem>
                                <asp:ListItem Value="2">Black or African-American</asp:ListItem>
                                <asp:ListItem Value="3">Asian or Pacific Islander</asp:ListItem>
                                <asp:ListItem Value="4">American Indian or Alaska Native</asp:ListItem>
                                <asp:ListItem Value="5">Other</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div>
                            <asp:Label ID="SpanishLbl" runat="server" Text="Are you Spanish, Hispanic, or Latino/a?"
                                AssociatedControlID="Spanish" CssClass="ddlabels"></asp:Label>
                            <asp:DropDownList ID="Spanish" runat="server">
                                <asp:ListItem Value="0">Select...</asp:ListItem>
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="2">No</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:Panel ID="fs_SampleChoice" runat="server" Visible="false">
                            <asp:Label ID="SampleLbl" runat="server" Text="Please select the sample you wish to receive.*"
                                AssociatedControlID="Sample" CssClass="ddlabels"></asp:Label>
                            <asp:DropDownList ID="Sample" runat="server">
                                <asp:ListItem Value="">Select...</asp:ListItem>
                                <asp:ListItem Value="2">Bior&eacute;&reg; Makeup Removing Towelettes</asp:ListItem>
                                <asp:ListItem Value="1">Bior&eacute;&reg; Steam Activated Cleanser</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="SampleValidator" runat="server" ErrorMessage="Please answer all questions."
                                ControlToValidate="Sample" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg" Visible="false"></asp:RequiredFieldValidator>
                        </asp:Panel>
                        <div id="optin_checkboxes">
                            <p>
                                Your privacy is important to us. You can trust that Kao Brands will not share your
                                personal information with other companies (see our <a href="http://www.kaobrands.com/privacy_policy.asp"
                                    target="_blank" style="color: #40A7C6;">Privacy Policy</a>).</p>
                            <p>
                                <asp:CheckBox Checked="false" ID="BioreOptin" runat="server" />Yes, tell me about
                                future Bior&eacute;&reg; product news and offerings.</p>
                            <p>
                                <asp:CheckBox ID="MultiOptin" runat="server" />Yes, I'd like to receive emails and
                                newsletters from other great products from Kao Brands Company:</p>
                            <p class="optinLine">
                                <asp:CheckBox ID="banoptin" runat="server" /><a href="http://www.feelbanfresh.com"
                                    target="_blank">Ban&reg;</a><br />
                                Ban&reg; antiperspirant and deodorant products keep you 3x cooler and fresher.<sup>&#134;</sup>
                                <br />
                                <span class="ban_disc"><sup>&#134;</sup> vs. ordinary invisible solids</span></p>
                            <p class="optinLine">
                                <asp:CheckBox ID="cureloptin" runat="server" /><a href="http://www.curel.com/index.asp"
                                    target="_blank">Cur&eacute;l&reg;</a><br />
                                Cur&eacute;l&reg; Skincare's full line of hand and body moisturizers delivers freedom<br />
                                from dry skin.</p>
                            <p class="optinLine">
                                <asp:CheckBox ID="jergensoptin" runat="server" /><a href="http://www.jergens.com/default.asp"
                                    target="_blank">Jergens&reg;</a><br />
                                Jergens&reg; Skincare collection of moisturizers delivers a natural glow, smooth
                                and firm skin and an allure that captivates.</p>
                            <p class="optinLine">
                                <asp:CheckBox ID="jfoptin" runat="server" /><a href="http://www.johnfrieda.com/index.asp"
                                    target="_blank">John Frieda&reg;</a><br />
                                John Frieda&reg; provides solutions for hair care problems, resulting in hair that looks fabulous.</p>
                        </div>
                    </div>
                    <p>
                        <asp:Button UseSubmitBehavior="true" ID="submit" Text="Sign Me Up" runat="server"
                            CssClass="submit" />
                    </p>
                    <p style="font-size:9px;">
                        Limit one (1) per person and per household. Offer limited to legal residents of the 50 United States (including D.C.) 18 years or older. You must have a valid home street address. Not redeemable in manner other than provided herein. Allow 8 to 10 weeks for delivery, except where specified. Void where prohibited, taxed, or restricted by law. Not responsible for lost, late, incomplete, or misdirected requests. Requests not complying with all offer requirements will not be honored. Duplicate requests will constitute fraud. Offer good while supplies last.
                    </p>
                    <asp:Panel ID="psSamp_Rules" runat="server" Visible="false">
                        <p style="font-size: smaller;">Terms & Conditions OFFER GOOD TO THE FIRST 2,500 QUALIFIED REGISTRANTS UNTIL SEPTEMBER 30, 2010. ALLOW EIGHT TO TEN WEEKS FOR DELIVERY. To receive one Bior&eacute;&reg; Pore Strip sample, go to <a href="#">http://www.biore2010.com/usa/promotions/sampling.aspx?promo=biore201009_fb_ps_samp</a> and complete registration. Offer limited to residents of the fifty (50) United States and D.C., aged 18 or older. Limit one (1) per person and per household. Duplicate requests will constitute fraud. You must have a valid home street address. Theft, diversion, reproduction, transfer, sale or purchase of this offer is prohibited and constitutes fraud. Offer good in the Continental United States and DC only. Void where prohibited, taxed, or restricted by law. Requests from clubs or organizations will not be honored. Not redeemable in any manner other than provided herein. Not responsible for lost, late, incomplete, postage due, or misdirected requests. Requests not complying with all offer requirements will not be honored. Any fraudulent submission will be prosecuted to the fullest extent of the law. Sponsored by: Kao Brands Company 2535 Spring Grove Ave., Cincinnati, OH 45214. </p>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="RequestSuccess" Visible="false" runat="server">
                  <asp:Panel ID="fs_Success" Visible="false" runat="server">
                    <h2>
                        Bior&eacute;&reg; Sample Giveaway</h2>
                    <p>
                        Thank you for signing up. Please allow 8-10 weeks for delivery.
                    </p>
                  </asp:Panel>
                  <asp:Panel ID="psSamp_Success" Visible="false" runat="server">
                    <h2>Thanks!</h2>
                    <p>Thank you so much for your interest in Bior&eacute;&reg; Skincare products!  Your sample will arrive within 6 – 8 weeks.  </p>
                  </asp:Panel>
                  <asp:Panel ID="default_Success" Visible="false" runat="server">
                    <h2>Bior&eacute;&reg; Sample Giveaway</h2>
                    <p>Thank you for signing up.</p>
                  </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="InvalidEntry" Visible="false" runat="server">
                    <h2>We're Sorry</h2>
                    This offer is limited to one per household. If you would like to be notified of
                    future offers and promotions, <a href="/usa/optin.aspx">click here</a>.
                </asp:Panel>
                <asp:Panel ID="InvalidPromo" Visible="false" runat="server">
                    <h2>We're Sorry</h2>
                    The promotion you're looking for is not available at this time. If
                    you would like to be notified of future offers and promotions, <a href="/usa/optin.aspx">
                        click here</a>.
                </asp:Panel>
                <asp:Panel ID="OutOfSamples" Visible="false" runat="server">
                                        <h2>We're Sorry</h2>
Unfortunately, due to an overwhelming response, we are currently out of samples.
                    We appreciate your interest. If you would like to be notified of future offers and
                    promotions, <a href="/usa/optin.aspx">click here</a>.
                </asp:Panel>
            </div>
    </asp:Panel>
</asp:Content>
