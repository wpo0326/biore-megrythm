using System;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Text;

public partial class skinProfile : System.Web.UI.Page
{
    Int32 theCount = 0;
    string theMemID = "";
    string theEmail = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        /* Setup defaults being shown/hidden, override for specific promo codes */
        Page.Header.Title = "Bior&eacute;&reg;";

        //For samples restricted to email recipients
        if (Request.QueryString["memID"] != null) theMemID = Request.QueryString["memID"];
        if (Request.QueryString["email"] != null) theEmail = Server.UrlDecode(Request.QueryString["email"].ToLower());

        if (theMemID == "" || theEmail == "")
            Response.Redirect("/usa/pageNotFound.aspx");


        if (!IsPostBack)
        {
            Email.Value = theEmail;
            MemberID.Value = theMemID;
            EmailHolder.Text = theEmail;
            int minAge = 13;
            int validMember = -1;
            int memberEntryCounts = 0;

            // For samples restricted to email recipients
            Page.Header.Title = "Bior&eacute;&reg; - Get Your Free Sample";
            //default_headTxt.Visible = false;
            //fs_headTxt.Visible = true;
            //ageReq.Visible = false;

            String theConnectionString = ConfigurationManager.ConnectionStrings["consumerInfo"].ConnectionString;
            SqlConnection theConnection = new SqlConnection(theConnectionString);
            theConnection.Open();


            try
            {
                validMember = checkMemberIsValid(theConnection, Int32.Parse(theMemID), theEmail);

                if (validMember <= 0)
                {
                    OptinFormFailure.Visible = true;
                    OptinForm.Visible = false;
                }
                else
                    memberEntryCounts = getMemberEntryCounts(theConnection, Int32.Parse(theMemID));

                if (memberEntryCounts > 0)
                {
                    OptinFormFailure.Visible = true;
                    OptinForm.Visible = false;
                }
                else
                {
                    DOBUnderageCheck.Visible = false;
                    DOBUnderageCheck.Enabled = false;
                }
                theConnection.Close();
                theConnection.Dispose();
                InitializeBirthdateDropdowns(minAge);
            }
            catch (Exception ex400047)
            {
                StringBuilder sb = new StringBuilder(500);
                sb.Append("Error processing skinProfile.aspx.cs\r\n");
                sb.Append("Email: " + theEmail + "\r\n");
                sb.Append("Member ID: " + theMemID.ToString() + "\r\n");
                sb.Append("Exception: " + ex400047.ToString());
                LogEntry myLog = new LogEntry();
                myLog.Priority = 1;
                myLog.Message = sb.ToString();
                myLog.EventId = 400047;
                Logger.Write(myLog);
                myLog = null;
                TechnicalProblem.Visible = true;
                OptinForm.Visible = false;
            }

        }
        else // Postback
        {
            Page.Validate();
            if (Page.IsValid)
            {
                int validEntry = -1;
                string DOB = mm.SelectedValue + "/" + dd.SelectedValue + "/" + yyyy.SelectedValue;
                string varProductsUsing = getProductsUsingOrUsedValues(1);
                string varProductsUsed = getProductsUsingOrUsedValues(2);
                string varTypesProductsUsed = buildCheckboxString(TypesProductsUsed);
                string varProductAttributes = buildCheckboxString(ProductAttributes);
                string varFaceProblems = buildCheckboxString(FaceProblems);
                string eventCode = "";
                int eventID = 0;
                string openEnded = "";

                int couponCode = 63911;
                string source = "";
                string medium = "Email";
                eventCode = "biore_2010Buildout";
                eventID = 400046;
                openEnded = "couponcode=" + couponCode.ToString() + "|medium=" + medium + "|source=" + source;
                string checkCode = "BO";
                string shortKey = "6zwoebil25";
                string longKey = "kKLbQdZumyDBsxSM5lzJHpURig7PI6a1V3wrFYn9O2tqfAehGT4ojX8EcvCWN";

                bioreContact bc = new bioreContact();

                validEntry = bc.bioreStringCoupon(0, FName.Text, LName.Text, Email.Value, "", medium, couponCode, source, eventID, eventCode, openEnded);

                eventCode = "biore_2010Buildout";
                eventID = 400046;
                openEnded = "Biore record build out coupon entry";

                validEntry = bc.bioreOptinNewTrait(Int32.Parse(theMemID), FName.Text, LName.Text, Email.Value, "", "", "", "", 0, PostalCode.Text, Int32.Parse(Gender.SelectedValue), DOB, Int32.Parse(ProdUsedDaily.SelectedValue), Int32.Parse(PayMoreAttention.SelectedValue), Int32.Parse(WillingToPayMore.SelectedValue), Int32.Parse(RecommendFriends.SelectedValue), varProductsUsing, varProductsUsed, Int32.Parse(MostUsedBrand.SelectedValue), varTypesProductsUsed, varProductAttributes, varFaceProblems, Int32.Parse(SkinType.SelectedValue), Int32.Parse(Race.SelectedValue), Int32.Parse(Spanish.SelectedValue), 0, 0, 0, 0, 0, eventID, eventCode, openEnded);


                string newPage = "http://bricks.coupons.com/enable.asp?eb=1&o=" + couponCode + "&c=" + checkCode + "&p=" + Server.UrlEncode(theEmail) + "&cpt=" + couponFunctions.EncodeCPT(Email.Value, couponCode, shortKey, longKey);
                CouponLink2.NavigateUrl = newPage;

                OptinForm.Visible = false;
                metaSubmit.Visible = true;

                CouponFormSuccess.Visible = true;
            }
        }
    }

    private string buildCheckboxString(CheckBoxList cb)
    {
        StringBuilder sb = new StringBuilder();
        foreach (ListItem li in cb.Items)
        {
            if (li.Selected)
            {
                sb.Append(Int32.Parse(li.Value));
                sb.Append(",");
            }
        }

        char[] charsToTrim = { ',' };
        return sb.ToString().TrimEnd(charsToTrim);
    }

    //private string 

    private string getProductsUsingOrUsedValues(int UsingOrUsed)
    {
        StringBuilder sb = new StringBuilder();
        string puVals = "";

        if (ProductsUsedNone.Checked)
            puVals = "20";
        else
        {
            if (UsingOrUsed == 1)
            {
                if (DualFusion1.Checked)
                    sb.Append("1,");
                if (SeeTheFuture1.Checked)
                    sb.Append("2,");
                if (HardDaysNight1.Checked)
                    sb.Append("3,");
                if (EvenSmoother1.Checked)
                    sb.Append("4,");
                if (CleanThingsUp1.Checked)
                    sb.Append("5,");
                if (Revitalize1.Checked)
                    sb.Append("6,");
                if (Detoxify1.Checked)
                    sb.Append("7,");
                if (Nourish1.Checked)
                    sb.Append("12,");
                if (PUS1.Checked)
                    sb.Append("13,");
                if (BFIC1.Checked)
                    sb.Append("14,");
                if (Astringent1.Checked)
                    sb.Append("15,");
                if (WarmingCleanser1.Checked)
                    sb.Append("16,");
                if (DCPoreStrips1.Checked)
                    sb.Append("17,");
                if (ComboPoreStrips1.Checked)
                    sb.Append("18,");
                if (UltraPoreStrips1.Checked)
                    sb.Append("19,");
                if (SAC1.Checked)
                    sb.Append("21,");
                if (MURT1.Checked)
                    sb.Append("22,");
            }
            else
            {
                if (DualFusion2.Checked)
                    sb.Append("1,");
                if (SeeTheFuture2.Checked)
                    sb.Append("2,");
                if (HardDaysNight2.Checked)
                    sb.Append("3,");
                if (EvenSmoother2.Checked)
                    sb.Append("4,");
                if (CleanThingsUp2.Checked)
                    sb.Append("5,");
                if (Revitalize2.Checked)
                    sb.Append("6,");
                if (Detoxify2.Checked)
                    sb.Append("7,");
                if (Nourish2.Checked)
                    sb.Append("12,");
                if (PUS2.Checked)
                    sb.Append("13,");
                if (BFIC2.Checked)
                    sb.Append("14,");
                if (Astringent2.Checked)
                    sb.Append("15,");
                if (WarmingCleanser2.Checked)
                    sb.Append("16,");
                if (DCPoreStrips2.Checked)
                    sb.Append("17,");
                if (ComboPoreStrips2.Checked)
                    sb.Append("18,");
                if (UltraPoreStrips2.Checked)
                    sb.Append("19,");
                if (SAC2.Checked)
                    sb.Append("21,");
                if (MURT2.Checked)
                    sb.Append("22,");
            }
            puVals = sb.ToString();
            if (puVals.Length > 0)
                puVals = puVals.Substring(0, puVals.Length - 1);
        }
        return puVals;
    }

    protected void DOBValidate(object source, ServerValidateEventArgs args)
    {
        string dob = mm.SelectedValue.ToString() + "/" + dd.SelectedValue.ToString() + "/" + yyyy.SelectedValue.ToString();
        try
        {
            DateTime.Parse(dob);
            args.IsValid = true;
        }
        catch
        {
            args.IsValid = false;
        }
    }

    protected void DOBUnderage(object source, ServerValidateEventArgs args)
    {
        string dob = mm.SelectedValue.ToString() + "/" + dd.SelectedValue.ToString() + "/" + yyyy.SelectedValue.ToString();
        bool invalid = false;
        DateTime dateValue;
        if (!DateTime.TryParse(dob, out dateValue))
            invalid = true;
        try
        {
            DateTime.Parse(dob);
            DateTime dt = new DateTime();
            dt = DateTime.Parse(dob);
            if (getAgeYearDifference(dt) < 18)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
        catch
        {
            args.IsValid = false;
        }
        if (invalid)
            args.IsValid = true;
    }

    protected void InitializeBirthdateDropdowns(int minAge)
    {
        DateTime dt = DateTime.Now;

        for (int i = dt.Year - minAge; i > dt.Year - 100; i--)
        {
            ListItem li = new ListItem();
            li.Text = i.ToString();
            li.Value = i.ToString();
            yyyy.Items.Add(li);
            li = null;
        }
        for (int i = 1; i < 13; i++)
        {
            ListItem li = new ListItem();
            li.Text = i.ToString();
            li.Value = i.ToString();
            mm.Items.Add(li);
            li = null;
        }
        for (int i = 1; i < 32; i++)
        {
            ListItem li = new ListItem();
            li.Text = i.ToString();
            li.Value = i.ToString();
            dd.Items.Add(li);
            li = null;
        }
    }

    private int checkMemberIsValid(SqlConnection theConnection, int iMemberID, string sEmail)
    {
        int iValidMember = -1;
        // Check the member is valid
        SqlCommand theCommand = new SqlCommand("spEMCheckMember", theConnection);
        theCommand.CommandType = CommandType.StoredProcedure;

        //output parameters
        theCommand.Parameters.Add(new SqlParameter("@iError", SqlDbType.Int));
        theCommand.Parameters["@iError"].Direction = ParameterDirection.Output;
        //input parameters
        theCommand.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar, 150));
        theCommand.Parameters["@email"].Value = sEmail;
        theCommand.Parameters.Add(new SqlParameter("@memID", SqlDbType.Int));
        theCommand.Parameters["@memID"].Value = iMemberID;

        theCommand.ExecuteNonQuery();
        iValidMember = (int)theCommand.Parameters["@iError"].Value;
        theCommand.Dispose();
        return iValidMember;

    }

    private int getMemberEntryCounts(SqlConnection theConnection, int iMemberID)
    {
        int iMemberEntryCounts = -1;
        SqlCommand theCommand = new SqlCommand("spEMGetMemberEventCount", theConnection);
        theCommand.CommandType = CommandType.StoredProcedure;

        //output parameters
        theCommand.Parameters.Add(new SqlParameter("@iError", SqlDbType.Int));
        theCommand.Parameters["@iError"].Direction = ParameterDirection.Output;
        //input parameters
        theCommand.Parameters.Add(new SqlParameter("@eventID", SqlDbType.Int));
        theCommand.Parameters["@eventID"].Value = 400047;
        theCommand.Parameters.Add(new SqlParameter("@memID", SqlDbType.Int));
        theCommand.Parameters["@memID"].Value = iMemberID;
        theCommand.ExecuteNonQuery();
        iMemberEntryCounts = (int)theCommand.Parameters["@iError"].Value;
        theCommand.Dispose();
        return iMemberEntryCounts;
    }

    private int getAgeYearDifference(DateTime dt)
    {
        // get the difference in years
        int years = DateTime.Now.Year - dt.Year;
        // subtract another year if we're before the
        // birth day in the current year
        if (DateTime.Now.Month < dt.Month ||
            (DateTime.Now.Month == dt.Month &&
            DateTime.Now.Day < dt.Day))
            years--;
        return years;
    }
}
