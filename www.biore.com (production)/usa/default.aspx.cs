using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;

public partial class _Default : System.Web.UI.Page
{
    public string FooterCssClass
    {
        set
        {
            if (Master.FindControl("footer") != null)
            {
                ((HtmlGenericControl)Master.FindControl("footer")).Attributes.Add("class", value);
            }
        }
    }
	
    protected void Page_Load(object sender, EventArgs e)
    {
			FooterCssClass = "withHomePromo"; //Set body class
	}
}
