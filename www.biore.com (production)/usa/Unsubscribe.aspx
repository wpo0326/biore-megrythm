<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true"
    CodeFile="Unsubscribe.aspx.cs" Inherits="Unsubscribe" Title="Untitled Page" Debug=true %>

<%@ MasterType VirtualPath="MasterPages/Main.master" %>
 <%@ Register TagPrefix="uc1" TagName="ucFormUnsubscribe" Src="~/Controls/ucFormUnsubscribe.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

<%@ Register TagPrefix="CC1" Namespace="CustomValidators"
    Assembly="CustomValidators" %>
    <meta name="Description" content="Unsubscribe from the latest news, coupons, and free samples from Biore" />
    <meta name="Keywords" content="Biore, unsubscribe" />
    <meta name="DCSext.submit" content="true" runat="server" visible="false" id="metaSubmit" />
    <link rel="stylesheet" type="text/css" href="css/FormConfig.css" media="screen, projection" />
	<!--[if lt IE 7]>
	<script>
	    DD_belatedPNG.fix('#optin_inner, #ctl00_ContentPlaceHolder1_OptinForm h1');
	</script>
	<![endif]--> 


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="optin">
        <div id="optin_inner">

        <div id="imgContact"><asp:Image ID="Image1" ImageUrl="~/images/optin/sidebar_biorecontact.jpg" runat="server" /></div>
        <%--Include the Dynamic Web Form--%>
    <uc1:ucFormUnsubscribe id="ucFormUnsubscribe" runat="server"></uc1:ucFormUnsubscribe>
        
    </div>
    </div>
    <!-- end #content -->
    <div id="content_bottom">
    </div>
</asp:Content>
