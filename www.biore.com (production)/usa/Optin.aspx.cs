using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;

public partial class Optin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Header.Title = "Contact Bior� today";
        Master.bodyClass = "optin";
		((WebControl)Master.FindControl("gn4")).CssClass = "global_anchor active"; //Set nav class		
		
		//turn off questionpro		
		((Panel)Page.Master.FindControl("panel_SDC_Analytics")).Visible = false;

		if (Request.QueryString["promo"] != null)
		{
			string promoID = Request.QueryString["promo"].ToString().ToLower();
			bool promoMatch = false;
			if (promoID.Contains("biore_us_201108_walmartcoup")) { promoMatch = true; }
			if (promoID.Contains("biore_us_201108_newlooktargetendcap")) { promoMatch = true; }
            if (promoID.Contains("biore_us_201111_freshfanporespectives")) { promoMatch = true; }
			if(promoMatch)
			{
				Response.Redirect("~/PageNotFound.aspx");
			}
		}
		
    }

    
}
