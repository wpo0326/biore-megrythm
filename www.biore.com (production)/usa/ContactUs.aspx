﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="ContactUs" %>

  <%@ Register TagPrefix="uc1" TagName="ucFormConfig" Src="~/Controls/ucFormConfig.ascx" %>

<%@ MasterType VirtualPath="MasterPages/Main.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

	<!--[if lt IE 7]>
	<script>
	    DD_belatedPNG.fix('#optin_inner, #ctl00_ContentPlaceHolder1_OptinForm h1');
	</script>
	<![endif]--> 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   

   <div id="optin">
        <div id="contact_top"></div>
        <div id="optin_inner">
        
        <%--Include the Dynamic Web Form--%>
        <uc1:ucFormConfig id="ucFormConfig" runat="server"></uc1:ucFormConfig>
         
    
      </div>
    </div>
    <!-- end #content -->
    <div id="content_bottom">
    </div>
</asp:Content>
