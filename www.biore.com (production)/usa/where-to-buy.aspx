<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true" CodeFile="where-to-buy.aspx.cs" Inherits="where_to_buy" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="Description" content="Find out where you can buy Biore skincare products." />
    <meta name="Keywords" content="Biore, where to buy Biore, buy Biore products, Biore retailer, shop for Biore, buy Biore online" />
	<!--[if lt IE 7]>
	<script type="text/javascript">
	    DD_belatedPNG.fix('#where_to_buy h1, #bnm a, #online a');
	</script>
	<![endif]--> 	
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="where_to_buy">
        <h1>Where To Buy</h1>

        <div id="bnm">
            <h2>Shop for Bior&eacute;&reg; Products at a retailer near you:</h2>
            <a href="http://www.walmart.com/storeLocator/ca_storefinder_results.do" rel="ext" target="_blank" id="walmart">Walmart&trade; - Save money. Live better.</a>
            <a href="http://www.kmart.com/shc/s/StoreLocatorView?storeId=10151" rel="ext" target="_blank" id="kmart">K-mart&trade;</a>
            <a href="http://sites.target.com/site/en/spot/page.jsp?title=store_locator_new&ref=nav_storelocator" rel="ext" target="_blank" id="target">Target&reg;</a>
            <a href="http://www.riteaid.com/stores/locator/" rel="ext" target="_blank" id="rite_aid">RiteAid Pharmacy - With us, it's personal.</a>
            
            <a href="http://www.walgreens.com/marketing/storelocator/find.jsp?foot=store_locator" rel="ext" target="_blank" id="walgreens_bnm">Walgreens</a>
            <a href="http://www.cvs.com/CVSApp/store/storefinder.jsp" rel="ext" target="_blank" id="cvs_bnm">CVS/pharmacy&reg;</a>
            <a href="http://store.facevaluesonline.com/store-locations.html" rel="ext" target="_blank" id="harmon_bnm">Harmon&reg; Face Values&trade; Discount Health & Beauty</a>
        </div>
        
        <div id="online">
            <h2>Shop for Bior&eacute;&reg; Products online:</h2>
            <a href="http://www.cvs.com/CVSApp/search/search_results.jsp?oss=1&removeAllFacets=true&addFacet=SRCH:biore" rel="ext" target="_blank" id="cvs_online">CVS/pharmacy&reg;</a>
            <a href="http://www.walgreens.com/search/results.jsp?Ntt=biore&x=0&y=0" rel="ext" target="_blank" id="walgreens_online">Walgreens</a>
            <a href="http://www.walmart.com/search/search-ng.do?search_constraint=0&ic=48_0&search_query=biore&Find.x=20&Find.y=4" rel="ext" target="_blank" id="walmart_online">Walmart</a>
            <a href="http://www.duanereade.com/Default.aspx" rel="ext" target="_blank" id="duane_reade">Duane Reade&trade; - Your City, Your Drugstore.</a>
            <a href="http://www.drugstore.com/templates/brand/default.asp?brand=8696&trx=SBB-0-AB&trxp1=8696" rel="ext" target="_blank" id="drugstore">Drugstore.com - The Uncommon Drugstore</a>
            <a href="http://search.ulta.com/nav/brand/Biore/0" rel="ext" target="_blank" id="ulta">Ulta&trade; Beauty</a>
            <a href="http://store.facevaluesonline.com/searchresult.html?catalog=yhst-29523360387793&query=biore&x=0&y=0" rel="ext" target="_blank" id="harmon_online">Harmon&reg; Face Values&trade; Discount Health & Beauty</a>
        </div>
    </div>
	<div class="promoWhereToBuy" id="prod_promo">
		<h5>Buy Bior&eacute;<sup>&reg;</sup> Skincare Pore Strips?</h5>
		<p>
			<a href="http://www.facebook.com/bioreskin?sk=app_205787372796203" target="_blank">Pore Stripping just got more rewarding. Look for Prove It!&trade; Reward Points codes in specially marked Bior&eacute;<sup>&reg;</sup> Pore Strip Packages&mdash;then join us on Facebook to start earning points toward free products and coupons in Prove It!&trade; Reward Points.<span>Click Here to Start Earning Now</span>
			</a>
		</p>
	</div>
</asp:Content>

