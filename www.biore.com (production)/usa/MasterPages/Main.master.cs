﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;

public partial class MasterPages_Main : System.Web.UI.MasterPage
{
    private string _bodyClass;
    public string bodyClass
    {
        get { return _bodyClass; }
        set { _bodyClass = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
		litDate.Text = DateTime.Now.ToString("yyyy");
		
        //Make SQL Connection and retrieve data
        using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["bioreDB"].ToString()))
        {
            SqlCommand cmd = new SqlCommand("GetCategoriesWithProducts", cn);
            cmd.CommandType = CommandType.StoredProcedure;

            // Create the DataSet
            DataSet ds = new DataSet();

            cn.Open();

			using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
			{
				adapter.Fill(ds);

				//Fill Data
				rp1.DataSource = ds.Tables[0];
				rp1.DataBind();
				lt1.Text = ds.Tables[0].Rows[0]["Title"].ToString();

				rp2.DataSource = ds.Tables[1];
				rp2.DataBind();
				lt2.Text = ds.Tables[1].Rows[0]["Title"].ToString();

				rp3.DataSource = ds.Tables[2];
				rp3.DataBind();
				lt3.Text = ds.Tables[2].Rows[0]["Title"].ToString();

				rp4.DataSource = ds.Tables[3];
				rp4.DataBind();
				lt4.Text = ds.Tables[3].Rows[0]["Title"].ToString();
			}
        }
    }
	
	protected string ConvertReg(String strval)
    {
        //Convert string to lowercase
        string myString = strval.ToString().Replace("&#174;", "<sup>&#174;</sup>");
        myString = myString.Replace("&reg;", "<sup>&#174;</sup>");
        return myString;
    }
}
