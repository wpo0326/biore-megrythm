<%@ OutputCache Duration="300" VaryByParam="none" %>
<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="_Default" Title="Bior&#233;&reg; Skincare Products - Get Your Best Clean" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="Description" content="Find out how to get your best clean with Biore skincare products." />
    <meta name="Keywords" content="Biore, skin care, skincare, cleanser" />
    <link rel="stylesheet" type="text/css" href="optin/css/thickbox.css" media="screen, projection" />
    <link rel="stylesheet" type="text/css" href="instantlyClearerPores/css/IC_lightbox-prod.css" media="screen, projection" />

	<script type="text/javascript" src="/usa/js/utils.js"></script>
	<script type="text/javascript" src="/usa/js/swfobject.js"></script>
  <script type="text/javascript" src="/usa/optin/js/thickbox.js"></script>
  <script type="text/javascript" src="/usa/js/videoLB.js"></script>
  <script type="text/javascript">
        $(document).ready(
	        function(){
		        $(".video_close").click(function(e){
				        e.preventDefault();
				        tb_remove();
		        });
		        $(".video_close").hover(
			        function () {$(this).addClass('video_close_over');},
			        function () {$(this).removeClass('video_close_over');}
		        );
		        
		        homeFlash(); // Setup and display home page Flash
	        }
        );
        </script>
        <meta name="google-site-verification" content="eozCSUqraEX1zUMItwEoxtxQLKCNQt5QAcxLO8lqc1c" />
  <!--[if lt IE 7]>
	<link rel="stylesheet" type="text/css" href="/usa/instantlyClearerPores/css/ie6.css" media="screen, projection" />
    <script type="text/javascript">
	    DD_belatedPNG.fix('#home_promos div,#home_promos p,#home_promos div#promoCoupon p a');
	</script>
	<![endif]-->
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    		<div id="home_theatre">
				<div id="no_flash">
					<h2>Prove It!&trade; Reward Points</h2>
					<h3>Get Rewards for Your Routine</h3>
					<p>
						With <strong>Prove It!&trade; Reward Points</strong> on Facebook, you can earn points for the things you already
						do to take care of your skin! Then, redeem your points for free products, coupons, and more to come! Plus,
						when you sign up, you get a <strong>bonus reward instantly!*</strong>
						<a href="http://www.facebook.com/bioreskin?sk=app_205787372796203" id="noFlashLink" target="_blank">Join us on Facebook and start earning points today! >></a>
					</p>
					<p id="disclaimer">*Open to legal residents of the 50 US (DC), 18 years and older. Starts 8/1/11 and ends 12/31/13. Void where prohibited. Subject to <a href="https://proveitrewards.bnservers.com/tc.html" target="_blank">Terms and Conditions</a>. Sponsored by Kao Brands Company, 2535 Spring Grove Avenue, Cincinnati, OH 45214.</p>
				</div>
    		</div>
		
        <div id="home_promos">
            <div id="promo1"><p><a href="instantlyClearerPores/IC_intro.aspx?height=605&amp;width=967" class="thickbox"><strong>Get the Complete Bior&eacute;<sup>&reg;</sup> Clean.</strong><br />Find out how to pair up<br />for more power.</a></p></div>
            <div id="promo2"><p><a href="the-buzz.aspx"><strong>People are talking.</strong><br />And tweeting, and taking pictures. See the latest posts from around the web.</a></p></div>
            <div id="promo3"><p><a href="optin.aspx"><strong>Love our stuff?</strong><br />Stay in the loop. Sign up for news and special Bior&eacute;<sup>&reg;</sup> product offers.</a></p></div>
        </div>
		
</asp:Content>