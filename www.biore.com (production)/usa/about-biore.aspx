<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true" CodeFile="about-biore.aspx.cs" Inherits="about_biore" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="Description" content="Learn the philosophy behind Biore Skincare." />
    <meta name="Keywords" content="Biore, skin care, skincare, skin philosophy, skincare philosophy" />
	<!--[if lt IE 7]>
	<script>
	    DD_belatedPNG.fix('#about h1, #about a span');
	</script>
	<![endif]--> 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="about">
    <h1>About Bior&eacute;<sup>&reg;</sup> Skincare</h1>
    <div>
        <p>The Bior&eacute;<sup>&reg;</sup> Skincare experts know that half the thrill of beautiful skin is how you get there. That's why we created a skincare line that promises a few surprises, a touch of fun, and lots of cool options to meet your daily, weekly or specialty skincare needs. Because getting great skin doesn't need to be boring or run-of-the-mill.</p>
        <p>From daily basic care to deep cleansing and complexion clearing, Bior&eacute;<sup>&reg;</sup> products help you think differently about your skin. You can trust us to share what we know about taking care of your skin, along with fun tips, so you too can become an insider on great skin.</p>
        <p class="indent">With so many extraordinary skincare products, you can pick and choose how you'd like to put your fresh face forward. Your skin will thank you. Bior&eacute;<sup>&reg;</sup> Skincare products are available at select food, drug and mass merchant stores.</p>
    </div>
    <a href="http://www.kaobrands.com/" target="_blank" class="moreLink aboutGo"><span>Go to Kao Brands Company</span></a>
</div>

</asp:Content>

