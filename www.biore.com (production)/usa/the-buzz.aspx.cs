using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.Xml;

public partial class the_buzz : System.Web.UI.Page
{
    public string BodyCssClass
    {
        set
        {
            if (Master.FindControl("middle_content") != null)
            {
                ((HtmlGenericControl)Master.FindControl("middle_content")).Attributes.Add("class", value);
            }
        }
    }
	
    public string NavClass
    {
        set
        {
            if (Master.FindControl("gn3") != null)
            {
                ((WebControl)Master.FindControl("gn3")).CssClass = value;
            }
        }
    }	

    protected void Page_Load(object sender, EventArgs e)
    {
        BodyCssClass = "mc_buzz"; //Set body class
		((WebControl)Master.FindControl("gn3")).CssClass = "global_anchor active"; //Set body class
        Page.Header.Title = "Bior&eacute;&reg;: The Buzz";
        ltTwit.Text = twitRender();
        ltFlick.Text = flickRender();
		//((HtmlGenericControl)Master.FindControl("promo_disclaimer")).InnerHtml = "This page contains live feeds from other sites; what you see should be considered information only, and does not imply endorsement of any kind. Clicking a post will open a new browser window. We are not responsible for what other people post.";
    }

    protected string twitRender()
    {
        StringBuilder sb = new StringBuilder();

        XmlDocument docTwit = new XmlDocument();
        //docTwit.Load("http://search.twitter.com/search.atom?geocode=37.09024%2C-95.712891%2C2000.0km&lang=en&q=biore+near%3Aus+within%3A2000km");
docTwit.Load("http://search.twitter.com/search.atom?lang=en&q=biore");
        // Create a Namespace Manager based on the loaded feed's Name Table
        XmlNamespaceManager ns = new XmlNamespaceManager(docTwit.NameTable);

        // Add the Atom namespace to the Namespace manager.
        ns.AddNamespace("atom", "http://www.w3.org/2005/Atom");

        sb.Append("<div id=\"twitter_list\">");

        int count = 1;
        string imgPath = "";
        string URL = "";

        if (docTwit.InnerXml != null)
        {
            foreach (XmlNode theNode in docTwit.SelectNodes("/atom:feed/atom:entry", ns))
            {
                if (count < 10 && (ForbiddenWords.IsWordForbidden(theNode.SelectSingleNode("atom:title", ns).InnerXml.ToString(), true) == false))
                // if (count < 10) 
                {
                    foreach (XmlNode theNode2 in theNode.SelectNodes("atom:link", ns))
                    {
                        String testAttribute = "";
                        testAttribute = theNode2.Attributes["type"].Value.ToString();

                        if (testAttribute != "" && testAttribute == "text/html")
                        {
                            URL = theNode2.Attributes["href"].Value.ToString();
                        }
                        else if (testAttribute != "" && testAttribute == "image/png")
                        {
                            imgPath = theNode2.Attributes["href"].Value.ToString();
                        }
                    }
                    sb.Append("<dl><dt>");
                    sb.Append("<img height=\"48\" width=\"48\" alt=\"\" src=\"");
                    sb.Append(imgPath);
                    sb.Append("\"/>");
                    sb.Append("</dt><dd>");
                    sb.Append("<a target=\"_blank\" href=\"");
                    sb.Append(URL);
                    sb.Append("\"/>");
                    sb.Append(theNode.SelectSingleNode("atom:title", ns).InnerXml);
                    sb.Append("</a><br/><span>");
                    sb.Append("<a target=\"_blank\" href=\"");
                    sb.Append(theNode.SelectSingleNode("atom:author/atom:uri", ns).InnerXml);
                    sb.Append("\"/>");
                    sb.Append(theNode.SelectSingleNode("atom:author/atom:name", ns).InnerXml);
                    sb.Append("</a></span>");
                    DateTime pubDate = Convert.ToDateTime(theNode.SelectSingleNode("atom:updated", ns).InnerXml.ToString());
                    sb.Append(String.Format("{0:MM/dd/yy}", pubDate));
                    sb.Append("</dd></dl>");
                    count++;
                }

            }
            sb.Append("</div>");
        }
        else { sb.Append("The Feed is currently down.</div>"); }
        return sb.ToString();
    }

    protected string flickRender()
    {
        StringBuilder sb = new StringBuilder();

        XmlDocument docFlick = new XmlDocument();
        docFlick.Load("http://api.flickr.com/services/feeds/photos_public.gne?tags=biore&lang=en-us");

        // Create a Namespace Manager based on the loaded feed's Name Table
        XmlNamespaceManager ns = new XmlNamespaceManager(docFlick.NameTable);

        // Add the Atom namespace to the Namespace manager.
        ns.AddNamespace("atom", "http://www.w3.org/2005/Atom");

        int count = 1;          //Counter
        string flickName = "";  //Flicker User Name
        string imgPath = "";
        string URL = "";

        if (docFlick.InnerXml != null)
        {
			foreach (XmlNode theNode in docFlick.SelectNodes("/atom:feed/atom:entry", ns))
			{
            // Make sure to display only one image posted by a user to avoid ballhogs
            if (count < 4 && theNode.SelectSingleNode("atom:author/atom:name", ns).InnerXml.ToString() != flickName)
            {
                foreach (XmlNode theNode2 in theNode.SelectNodes("atom:link", ns))
                {
                    String testAttribute = "";
                    testAttribute = theNode2.Attributes["type"].Value.ToString();

                    if (testAttribute != "" && testAttribute == "text/html")
                    {
                        URL = theNode2.Attributes["href"].Value.ToString();
                    }
                    else if (testAttribute != "" && testAttribute == "image/jpeg")
                    {
                        imgPath = theNode2.Attributes["href"].Value.ToString();
                    }
                }

                flickName = theNode.SelectSingleNode("atom:author/atom:name", ns).InnerXml.ToString();
                sb.Append("<div><p><a target=\"_blank\" href=\"");
                sb.Append(URL);
                sb.Append("\"/>");
                sb.Append("<img alt=\"\" height=\"267\" width=\"322\" src=\"");
                sb.Append(imgPath);
                sb.Append("\"/>");
                sb.Append("</a><span>");
                sb.Append("<a target=\"_blank\" href=\"");
                sb.Append(theNode.SelectSingleNode("atom:author/atom:uri", ns).InnerXml);
                sb.Append("\"/>");
                sb.Append(flickName);
                sb.Append("</a> - ");
                DateTime pubDate = Convert.ToDateTime(theNode.SelectSingleNode("atom:published", ns).InnerXml.ToString());
                sb.Append(String.Format("{0:MM/dd/yy}", pubDate));
                sb.Append("</span></p></div>");
                count++;
            }
        }
		}
        
		return sb.ToString();
    }
}
