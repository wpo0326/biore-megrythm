<%@ OutputCache Duration="300" VaryByParam="none" %>
<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true" CodeFile="the-buzz.aspx.cs" Inherits="the_buzz" Title="Bioré: The Buzz" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta name="Description" content="Find out what people are saying about Biore skincare products." />
    <meta name="Keywords" content="Biore, skincare, tweet, pictures, web post, Twitter, the buzz" />
	<!--[if lt IE 7]>
	<script type="text/javascript">
	    DD_belatedPNG.fix('#buzzColA h2, #buzzColA h3, #twitter, #twitter_list, .mc_buzz p.buzzMore a span, #flickr div');
	</script>
	<![endif]--> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
            <div id="buzzColA">
                <h2>The Buzz</h2>
                <h3>People are talking. And tweeting, and taking pictures. The latest from around the web.</h3>
                
                <div id="twitter">
                    <asp:Literal ID="ltTwit" runat="server" />
                    <p class="buzzMore"><a href="http://search.twitter.com/search?q=biore" target="_blank" class="moreLink"><span>Read more on Twitter</span></a></p>
                </div>
            </div>
            
            <div id="buzzColB">
            
                <div id="flickr">
                    <asp:Literal ID="ltFlick" runat="server" />
					<p class="buzzMore"><a href="http://www.flickr.com/search/?q=biore" target="_blank" class="moreLink"><span>See more on Flickr</span></a></p>
                </div>    
            </div>

			<p id="disclaimer_buzz">This page contains live feeds from other sites; what you see should be considered information only, and does not imply endorsement of any kind. We are not responsible for what other people post. FYI: clicking a post will open a new browser window.</p>
</asp:Content>

