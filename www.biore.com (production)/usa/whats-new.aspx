<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true"
    CodeFile="whats-new.aspx.cs" Inherits="whats_new" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta name="Description" content="Find out what's new from the Biore skincare experts." />
    <meta name="Keywords" content="Biore, skin care, cleansers, new, steam activated, makeup removing towelettes, new products," />
    <link rel="stylesheet" type="text/css" href="optin/css/thickbox.css" media="screen, projection" />
    <link rel="stylesheet" type="text/css" href="instantlyClearerPores/css/IC_lightbox-prod.css"
        media="screen, projection" />

    <script type="text/javascript" src="/usa/optin/js/thickbox.js"></script>

    <!--[if lt IE 7]>
	<link rel="stylesheet" type="text/css" href="/usa/instantlyClearerPores/css/ie6.css" media="screen, projection" />
    <script>
	    DD_belatedPNG.fix('#whats_new h1, #whats_new h2, #whats_new img, #whats_new a span');
	</script>
	<![endif]-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="whats_new">
        <h1>
            What's New</h1>
        <h2>
            Our Latest and Greatest Products &amp; News</h2>
        <div style="display: table; height: 121px; #position: relative; overflow: hidden;
            width: 458px; padding-left: 23px; margin-bottom: 9px">
            <div style="#position: absolute; #top: 50%; display: table-cell; vertical-align: middle;">
                <div style="#position: relative; #top: -50%; width: 304px;">
                    <h3>Prove It!&trade; Reward Points</h3>
                    <p>Get rewards for your routine! Start earning Prove It!&trade; Reward Points on Facebook towards free products and coupons, plus, more to come!</p>
                    <p>
                        <a href="http://www.facebook.com/bioreskin?sk=app_205787372796203" target="_blank"><span>Learn More</span></a></p>
                </div>
            </div>
            <img src="/usa/images/whats_new/promo1.png" width="117" height="110" alt="" />
        </div>
        <div style="display: table; height: 121px; #position: relative; overflow: hidden;
            width: 458px; padding-left: 23px; margin-bottom: 9px">
            <div style="#position: absolute; #top: 50%; display: table-cell; vertical-align: middle;">
                <div style="#position: relative; #top: -50%; width: 304px;">
                    <h3>Out With The Old, In With The Blue</h3>
                    <p>We're debuting a fresh, new look to match the invigorating formulas that you know and love!</p>
                    <p>
                        <a href="/usa/new-style/"><span>Learn More</span></a></p>
                </div>
            </div>
            <img id="newLookProdShot" src="/usa/images/whats_new/promoNewLook.png" width="161" height="121" alt=""  />
        </div>
        <div style="display: table; height: 121px; #position: relative; overflow: hidden;
            width: 458px; padding-left: 23px; margin-bottom: 9px">
            <div style="#position: absolute; #top: 50%; display: table-cell; vertical-align: middle;">
                <div style="#position: relative; #top: -50%; width: 304px;">
                     <h3>
                        Steam Activated Cleanser</h3>
                    <p>
                        Harness pore-opening shower steam for an exhilarating deep clean.</p>
                    <p>
                        <a href="/usa/products/product_detail.aspx?pid=1"><span>Learn More</span></a></p>
                </div>
            </div>
            <img src="/usa/images/whats_new/promo2.png" width="149" height="121" alt=""  />
        </div>
    </div>
</asp:Content>
