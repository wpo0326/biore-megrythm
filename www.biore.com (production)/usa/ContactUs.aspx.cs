﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ContactUs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
		//turn off questionpro		
		((Panel)Page.Master.FindControl("panel_SDC_Analytics")).Visible = false;
		
		if (Request.QueryString["promo"] != null)
		{
			string promoID = Request.QueryString["promo"].ToString().ToLower();
			bool promoMatch = false;
			if (promoID.Contains("biore_us_201108_walmartcoup")) { promoMatch = true; }
			if (promoID.Contains("biore_us_201108_newlooktargetendcap")) { promoMatch = true; }
            if (promoID.Contains("biore_us_201111_freshfanporespectives")) { promoMatch = true; }
			if(promoMatch)
			{
				Response.Redirect("~/PageNotFound.aspx");
			}
		}
    }
}
