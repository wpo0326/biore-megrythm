<%
  redirectURL="http://www.drugstore.com/templates/stdplist/default.asp?catid=168922&sctrx=dps-16&sctrxp1=168328"
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>Biore</title>
	<meta name="title" content="<%=strRedirectURL%>" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
  <meta http-equiv="refresh" content="0;url=<%=redirectURL%>" />
</head>

<body>
    If your browser doesn't redirect automatically, click <a href="<%=redirectURL%>">here</a>.
    
    <!-- Google Code for Any Site Visitor Conversion Page -->
    <script type="text/javascript">
    <!-- var google_conversion_id = 1030395866; var google_conversion_language = "en_US"; var google_conversion_format = "3"; var google_conversion_color = "ffffff"; var google_conversion_label = "jhxICPLBrwEQ2q-q6wM"; //-->
    </script>

    <script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js"></script>
    <noscript><div style="display:inline;"> <img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1030395866/?label=jhxICPLBrwEQ2q-q6wM&amp;guid=ON&amp;script=0"/></div>
    </noscript>

    <!-- Start of DoubleClick Spotlight Tag: Please do not remove-->
    <!-- Activity Name for this tag is:Jergens Homepage -->
    <!-- Web site URL where tag should be placed: http://www.jergens.com -->
    <!-- This tag must be placed within the opening <body> tag, as close to the beginning of it as possible-->
    <!-- Creation Date:6/29/2007 -->

    <img src="http://ad.doubleclick.net/activity;src=1493701;type=kaoho686;cat=jerge457;ord=1;num=696173758?" width="1" height="1" border="0" alt="" class="hidden"/><!-- End of DoubleClick Spotlight Tag: Please do not remove--><div id="LogoSignUpContainer">

    <!-- START OF SmartSource Data Collector TAG -->
    <script src="/usa/js/webtrends.js" type="text/javascript"></script>
    <script type="text/javascript">
    //<![CDATA[
    var _tag=new WebTrends();
    _tag.dcsGetId();
    //]]>>
    </script>
    <script type="text/javascript">
    //<![CDATA[
    // Add custom parameters here.
    //_tag.DCSext.param_name=param_value;
    _tag.dcsCollect();
    //]]>>
    </script>
    <noscript>
    <div><img alt="DCSIMG" id="DCSIMG" width="1" height="1" src="http://sdc.enlighten.com/dcsx5r2z6x4568dp93c02vf9d_4y5t/njs.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=8.6.0"/></div>
    </noscript>
    <!-- END OF SmartSource Data Collector TAG -->

    <!-- Start of google analytics -->
    <script type="text/javascript">
      var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
      document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    
    <script type="text/javascript">
      try {
        var firstTracker = _gat._getTracker("UA-385129-27"); // Global Tag
        firstTracker._setDomainName("none");
        firstTracker._trackPageview();
        var secondTracker = _gat._getTracker("UA-385129-4"); // Biore Tag
        secondTracker._setCampNameKey("enl_campaign")
        secondTracker._setCampMediumKey("enl_medium")
        secondTracker._setCampSourceKey("enl_source")
        secondTracker._trackPageview();
      } catch(err) {}
    </script>
    <!-- End of google analytics -->

    <!-- Advertiser 'Enlighten - Kao Brands Company',  Include user in segment 'Jergens - Site Retargeting Pixel' - DO NOT MODIFY THIS PIXEL IN ANY WAY -->
    <img src="http://ads.bluelithium.com/pixel?id=577963&t=2" width="1" height="1" />
    <!-- End of segment tag -->
</body>
</html>
