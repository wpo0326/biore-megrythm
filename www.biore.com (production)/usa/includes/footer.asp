    <div id="footer">
        <ul>
            <li><a href="/usa/about-biore.aspx">About Bior&eacute;<sup>&reg;</sup> Skincare</a></li>
            <li><a href="http://www.kaobrands.com/" target="_blank">Kao Brands Company</a></li>
            <li><a href="/usa/where-to-buy.aspx">Where to Buy</a></li>
            <li><a href="/usa/event.asp?action=entry&EventCode=biorecontact">Contact Us</a></li>

            <li><a href="http://www.kaobrands.com/legal.asp" target="_blank">Legal</a></li>
            <li><a href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">Privacy Policy</a></li>
            <li id="copyright">&copy; 2010 Kao Brands Company. All Rights Reserved</li>
        </ul>
    </div>
	<div id="left_shadow"></div>
	<div id="right_shadow"></div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	var userAgent = navigator.userAgent.toLowerCase();
	if (userAgent.indexOf('mac') != -1 && userAgent.indexOf('firefox')!=-1) {
		$("#nav-overlay").addClass("macFirefox");
	}
	$("#nav-overlay").css({filter: "alpha(opacity=0)"});
});
</script>

<!-- Start of DoubleClick Spotlight Tag: Please do not remove-->
<!-- Activity Name for this tag is:Biore Homepage -->
<!-- Web site URL where tag should be placed: http://www.biore.com -->
<!-- This tag must be placed within the opening <body> tag, as close to the beginning of it as possible-->
<!-- Creation Date:8/13/2007 -->
<% Randomize() %>
<IMG SRC="http://ad.doubleclick.net/activity;src=1418070;type=biore797;cat=biore881;ord=1;num=<% Response.Write(int(rnd()*3141581723+10)) %>?" WIDTH=1 HEIGHT=1 BORDER=0>
<!-- End of DoubleClick Spotlight Tag: Please do not remove-->

<!-- START OF SmartSource Data Collector TAG -->
<script src="/usa/js/webtrends.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
var _tag=new WebTrends();
_tag.dcsGetId();
//]]>>
</script>
<script type="text/javascript">
//<![CDATA[
// Add custom parameters here.
//_tag.DCSext.param_name=param_value;
_tag.dcsCollect();
//]]>>
</script>
<noscript>
<div><img alt="DCSIMG" id="DCSIMG" width="1" height="1" src="http://sdc.enlighten.com/dcsx5r2z6x4568dp93c02vf9d_4y5t/njs.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=8.6.0"/></div>
</noscript>
<!-- END OF SmartSource Data Collector TAG -->


<script type="text/javascript">
  var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
  document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>

<script type="text/javascript">
  try {
    var firstTracker = _gat._getTracker("UA-385129-27"); // Global Tag
    firstTracker._setDomainName("none");
    firstTracker._trackPageview();
    var secondTracker = _gat._getTracker("UA-385129-4"); // Biore Tag
    secondTracker._setCampNameKey("enl_campaign")
    secondTracker._setCampMediumKey("enl_medium")
    secondTracker._setCampSourceKey("enl_source")
    secondTracker._trackPageview();
  } catch(err) {}
</script>


<!-- BEGIN CUSTOMIZED SURVEY POPUP CODE -->
<script>

function getCookie(c_name)
{
if (document.cookie.length>0)
  {
  c_start=document.cookie.indexOf(c_name + "=");
  if (c_start!=-1)
    {
    c_start=c_start + c_name.length+1;
    c_end=document.cookie.indexOf(";",c_start);
    if (c_end==-1) c_end=document.cookie.length;
    return unescape(document.cookie.substring(c_start,c_end));
    }
  }
return "";
}
function getQuerystring(key, default_)
{
  if (default_==null) default_="";
  key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regex = new RegExp("[\\?&]"+key+"=([^&#]*)");
  var qs = regex.exec(window.location.href);
  if(qs == null)
    return default_;
  else
    return qs[1];
}

var showPostSurvey = false;
var wt_cookie_id_val = '';
var tmp_wt_cookie_val = getCookie('WT_FPC');
var tmp_wt_cookie_val_split_val = tmp_wt_cookie_val.split( ':' );
for ( i = 0; i < tmp_wt_cookie_val_split_val.length; i++ ) {
	tmpnamevalpair = tmp_wt_cookie_val_split_val[i];
	if (tmpnamevalpair.substring(0,3) == 'id=') {
		wt_cookie_id_val = tmpnamevalpair.substring(3,tmpnamevalpair.length);
		tmplastnum = wt_cookie_id_val.substring(wt_cookie_id_val.lastIndexOf('.')-1,wt_cookie_id_val.lastIndexOf('.'));
		// half should get pre-survey invitation and the other have get the post-survey invitation
		if (tmplastnum != Number.NaN && tmplastnum <5) showPostSurvey = true;
	}
}

var config = new Object();

if (showPostSurvey) {
	config.surveyID = 1352391; //(Biore-POST)
	config.popupMessage = '<br />Please click start survey, then minimize window. Once you are through with your visit to the Bior&eacute;&reg; site please complete the survey.';
} else {
	config.surveyID = 1352329; //(Biore-PRE)
	config.popupMessage = '<br />Please take the survey <strong>BEFORE</strong> you visit the site';
}
config.sCustom = '&custom1=' + wt_cookie_id_val + '&custom2=' + getQuerystring('enl_medium','') + '&custom3=' + getQuerystring('enl_source','');
config.popupInvitationHeader = 'Bior&eacute;&reg; Survey';

config.animationMode = 2;
config.takeSurveyURL = 'http://www.questionpro.com/akira/TakeSurvey';
config.windowPositionLeft = 150;
config.windowPositionTop = 45;
config.home = 'http://www.questionpro.com/';
config.isRightToLeft = false;
config.surveyStartMessage = 'Start Survey';
config.popupInvitationLaterMessage = 'Later';
config.showFooter = false;
config.invitationDelay = 0;
config.skipCount = 0;
config.popupMode = 2;
</script>
<script language="javascript" type="text/javascript"  src="/usa/js/questionProPopupInvitationCustom.js"></script>

<noscript><a href="http://www.questionpro.com/akira/TakeSurvey?id=1352329">Start Survey</a>  <a href="http://www.questionpro.com/">Survey</a></noscript>

<!-- END SURVEY POPUP CODE -->
</body>
</html>
       