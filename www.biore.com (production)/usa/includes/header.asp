<% Dim bodyID, bodyClass 
Dim strHSql, counter, rs, Nav, catPage, productPage, intCategory, strProductId, pageSpecificStyles

%>
<!--#include virtual="/usa/includes/adovbs.asp" -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" dir="ltr" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        <%=pageTitle%>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta name="description" content="<%=pageDescription%>" />
    <meta name="keywords" content="<%=pageKeywords %>" />
    <link rel="shortcut icon" href="/usa/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="/usa/css/global.css" media="screen, projection" />
	<% if pageSpecificStyles <> "" then %>
	<link rel="stylesheet" type="text/css" href="<%=pageSpecificStyles%>" />
	<% end if %>

   <!-- <script type="text/JavaScript" src="/usa/optin/js/prototype_packer.js"></script>-->
    <script type="text/javascript" src="/usa/js/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="/usa/js/jquery.hoverIntent.minified.js"></script>
    <script type="text/javascript" src="/usa/js/jquery.backgroundPosition.js"></script>
    <script type="text/javascript" src="/usa/js/nav.js"></script>
    <script type="text/javascript" src="/usa/js/common.js"></script>
</head>
<body id="<%=bodyID%>" class="<%=bodyClass%>">
    <div id="container">
        <div id="header">
			<a id="facebook" href="http://www.facebook.com/bioreskin" target="_blank" title="See what people are saying about us! Fan our Facebook page"></a>
            		
            <h1 id="logo">
                <a href="/usa">Bior&eacute;&reg; Beauty Starts Here</a></h1>
         <ul class="global_nav">
		 <!--#include virtual="/usa/includes/_db_conn_.asp"-->
            <li id="globalNav_1" class="navMenu"><a href="/usa/products/index.aspx" class="global_anchor">Our Products</a>
                <div>
               <ul class="navSubMenu">
                <%
strHSql = "GetProductCategories"
productPage = "product_detail.aspx"
Set rsH = Server.CreateObject("ADODB.Recordset")
rsH.Open strHSql,objConn
counter = 0
ReDim Nav(3,1)
Do While NOT rsH.EOF
    Nav(counter,0) = rsH("CategoryID")
    Nav(counter,1) = rsH("Title")
    counter = counter + 1
    rsH.MoveNext
Loop

rsH.Close

For counter = lBound(Nav) to uBound(Nav)
                %>
                <li class="menuHdr"><%=Nav(counter,1) %></li>
                            <%
    strHSql = "GetProductsByCategory " + CStr(Nav(counter,0))
    Set rsH = Server.CreateObject("ADODB.Recordset")
    rsH.Open strHSql,objConn
    Do While NOT rsH.EOF

                            %>
                <li><a href="/usa/products/<%=productPage %>?pid=<%=rsH("ProductID")%>"><%=rsH("NameText") %></a></li>
                            <%
        rsH.MoveNext
    Loop
                            %>
                <%
Next
rsH.Close
                %>
                </ul>
                </div>
            </li>


            <li id="globalNav_2" class="navMenu"><a href="/usa/whats-new.aspx" class="global_anchor">What's New</a></li>
            <li id="globalNav_3" class="navMenu"><a href="/usa/the-buzz.aspx" class="global_anchor">The Buzz</a></li>
            <li id="globalNav_4" class="navMenu"><a href="/usa/optin.aspx" class="global_anchor">Sign Me Up</a></li>
            <li id="globalNav_5" class="navMenu"><a href="/usa/where-to-buy.aspx" class="global_anchor">Where To Buy</a></li>
        </ul>
            <div id="placeholder_nav">
            </div>
        </div>
