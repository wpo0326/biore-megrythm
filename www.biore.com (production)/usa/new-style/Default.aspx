<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="Default" Title="Untitled Page" Debug=true %>

<%@ MasterType VirtualPath="~/MasterPages/Main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<meta name="Description" content="Our new look features the same super-effective invigorating Bior� Skincare formulas inside, a strikingly fresh makeover outside." />
	
	<!--[if lt IE 7]>
	<script>
	    DD_belatedPNG.fix('#comingSoon_inner');
	</script>
	<![endif]-->

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   

   <div id="comingSoon">
        <div id="comingSoon_top"></div>
        <div id="comingSoon_inner">		
			<h1 id="hdr_ComingSoon">Allow Us to Freshen Up (Our Look) A Bit. Coming Soon!</h1>
			<p id="p1">Say &quot;goodbye&quot; to green&mdash;we're stepping up the style of all our Bior&eacute;&reg; Skincare products with fresh white and blue hues! We designed our clean new look to match what's inside: the same innovative formulas that keep your complexion feeling fresh and clean.</p>
			<h2 id="hdr_ProveIt">Prove It!&trade; Reward Points</h2>
			<a href="http://www.facebook.com/bioreskin?sk=app_205787372796203" id="linkStartEarning" target="_blank">Start Earning Now</a>
			<h3 id="hdr_KeepEarning">Keep Earning Rewards</h3>
			<p id="p2">And while our look's getting an upgrade, you�ll still be able to earn rewards with <b>Prove It!&trade; Reward Points</b> on Facebook! Sign up today to start racking up points and redeem them for coupons, free products, plus, more to come!</p>
			<h3 id="hdr_SignUp">Sign up for our newsletter to be the first to hear all about our <b>New Look</b>&mdash;plus, get the latest offers from Bior&eacute;&reg;* Skincare.</h3>
			<a href="/usa/optin.aspx" id="linkSignUp">Sign Me Up</a>
			<h3 id="hdr_BestClean">Bior&eacute;&reg;. Get Your Best Clean.</h3>
		</div>
	</div>
    <div id="content_bottom">
    </div>
</asp:Content>
