using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class redir : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Header.Title = "Bior&eacute;&reg; Skincare";
        
      	string redirectURL = "/usa/";
      	string dest = Request.QueryString["dest"];
      
      	if (dest == null)
      	{
        		// REDIRECT TO ERROR PAGE NOT FOUND 404
        		redirectURL = "/usa/PageNotFound.aspx";
      	}
      	else
      	{
          	if (!string.IsNullOrEmpty(dest))
          	{
                switch (dest.ToUpper())
            	  {
            		    case "BIORE_PS_FB":
            		        redirectURL = "http://www.facebook.com/bioreskin";
            		        break;
            		    case "BIORE_DRUG":
            		        redirectURL = "http://www.drugstore.com/templates/stdplist/default.asp?catid=168922&sctrx=dps-16&sctrxp1=168328";
            		        break;
            		    default:
            		        break;
                }
          	}      
      	}
      	litRedirectUrl.Text = redirectURL;
      	HtmlMeta metaDesc = new HtmlMeta();
      	metaDesc.Attributes.Add("http-equiv", "refresh");
      	metaDesc.Attributes.Add("content", "0;url=" + redirectURL);
      	Page.Header.Controls.Add(metaDesc);
    }
}
