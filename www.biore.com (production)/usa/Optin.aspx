<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true"
    CodeFile="Optin.aspx.cs" Inherits="Optin" Title="Untitled Page" Debug=true %>
    <%@ Register TagPrefix="uc1" TagName="ucFormConfig" Src="~/Controls/ucFormConfig.ascx" %>

<%@ MasterType VirtualPath="MasterPages/Main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

	<!--[if lt IE 7]>
	<script>
	    DD_belatedPNG.fix('#optin_inner, #ctl00_ContentPlaceHolder1_OptinForm h1');
	</script>
	<![endif]--> 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   

   <div id="optin">
        <div id="contact_top"></div>
        <div id="optin_inner">
        
        <%--Include the Dynamic Web Form--%>
    <uc1:ucFormConfig id="ucFormConfig" runat="server"></uc1:ucFormConfig>
      <script type="text/javascript">


          $(function() {


          toggle("noneToggle1");
          toggle("noneToggle2");
          toggle("noneToggle3");
          toggle("noneToggle4");

              function toggle(className) {
                  var checkToggle = $("." + className + " input:last");
                  checkToggle.click(function() {
                  if ($("#" + checkToggle.attr('id') + ":checked").val()) {
                      $("." + className + " input").each(function() {
                      if ($(this).attr('id') != checkToggle.attr('id')) {
                                  $(this).attr("checked", false);
                                  $(this).attr('disabled', 'disabled');
                              }
                          });
                      } else {
                      $("." + className + "  input").each(function() {
                      if ($(this).attr('id') != checkToggle.attr('id')) {
                                  $(this).removeAttr('disabled');
                              }
                          });
                      }

                  });
              }
          }); 
             
    </script>
       
	</div>
	</div>
    <div id="content_bottom">
    </div>
</asp:Content>
