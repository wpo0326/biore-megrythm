using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;


public partial class couponEmailRedirect : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string promo = "";
        if (Request.QueryString["email"] == null || Request.QueryString["promo"] == null)
            Response.Redirect("/usa/pageNotFound.aspx&src=cer1");

        promo = Request.QueryString["promo"];

        string medium = "Email";
        int couponCode = 0;
        string checkCode = "BO";
        string shortKey = "";
        string longKey = "";
        int validEntry = -1;
        int memberID = 0;
        string email = "";
        string PIN = "";
        string fname = "";
        string lname = "";
        string source = "";
        int eventID = 0;
        string eventCode = "";
        string openEnded = "";

        switch (promo)
        {
            case "2010072WkChallenge":
                couponCode = 64701;
                shortKey = "rkxuv57geb";
                longKey = "k1lrs7nzCWxB4gHGSO5cwe8bo2pyiAVRZKmXLN69qYjDuEJPa3dIMTvfthQUF";
                email = Request.QueryString["email"].ToLower();
                eventCode = "biore_2coup10";
                eventID = 400025;
                source = "ePrize";
                break;
            default:
                Response.Redirect("/usa/pageNotFound.aspx&src=cer2");
                break;
        }
        openEnded = "couponCode=" + couponCode.ToString() + "|medium=" + medium + "|source=" + source;

        bioreContact myContact = new bioreContact();
        validEntry = myContact.bioreStringCoupon(memberID, fname, lname, email, PIN, medium, couponCode, source, eventID, eventCode, openEnded);

        if (validEntry == 0)
            Response.Redirect("http://bricks.coupons.com/enable.asp?eb=1&o=" + couponCode + "&c=" + checkCode + "&p=" + Server.UrlEncode(email) + "&cpt=" + couponFunctions.EncodeCPT(email, couponCode, shortKey, longKey));
        else
            Response.Redirect("/usa/pageNotFound.aspx&src=cer3");
    }
}
