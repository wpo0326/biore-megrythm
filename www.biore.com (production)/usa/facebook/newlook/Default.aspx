﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="usa_facebook_newlook_Default" %>
<%@ Register TagPrefix="uc1" TagName="ucFormConfig" Src="~/Controls/ucFormConfig.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bior&eacute;&reg; New Look</title>
    <link href="css/main.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
    <script type="text/javascript">
        if (typeof jQuery == 'undefined') {
            var localjQuerySrc = 'js/jquery-1.6.2.min.js';
            document.write(unescape("%3Cscript src='" + localjQuerySrc + "' type='text/javascript'%3E%3C/script%3E"));
        }
    </script>
	<script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script> 
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
			<div id="fb-root"></div>
            <h1>Allow us to freshen up (our look) a bit</h1>
			<h2>Coming Soon!</h2>
			<p id="text">Say "goodbye" to green–we're stepping up the style of all our Bior&eacute;<sup>&reg;</sup> Skincare products with fresh white and blue hues! We designed our clean new look to match what's inside: the same innovative formulas that keep your complexion feeling fresh and clean.</p>
			<h3>Keep Earning Rewards</h3>
			<div id="proveItContainer">
				<p id="text">And while our look's getting an upgrade, you'll still be able to earn rewards with <strong>Prove It!&trade; Reward Points</strong> on Facebook! Sign up today to start racking up points and redeem them for coupons, free products, plus, more to come!</p>
				<p>Prove It!&trade; Reward Points <a href="http://www.facebook.com/bioreskin?sk=app_205787372796203" id="proveItLink">Start Earning Now &gt;&gt;</a></p>
			</div>
			<div id="signUpContainer">
				<p>Sign up for our newsletter to be the first to hear all about our New Look—plus, get the latest offers from Bior&eacute;<sup>&reg;</sup> Skincare.</p>
				<p><a href="../../optin.aspx?enl_campaign=biore_facebook_newLook-comingSoon&enl_medium=affiliate&enl_source=newLook-comingSoon_SignMeUpBtn" target="_blank">Sign me up &gt;&gt;</a></p>
			</div>
			<h4>Bior&eacute;<sup>&reg;</sup>. Get Your Best Clean.</h4>
		</div>
    </form>
</body>
</html>