﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usa_facebook_specialofferswalmartcoupon_Default : System.Web.UI.Page
{
    private string _bodyClass;
    public string bodyClass
    {
        get { return _bodyClass; }
        set { _bodyClass = value; }
    }
	
	  // This code is in place to allow IE to keep its Cookies and Sessions despite being
        // set on an iFrame in Facebook.  This only affect certain versions & users with specific
        // privacy settings.
        protected override void OnPreRender(EventArgs e)
        {
            Response.AppendHeader("P3P", "CP=\"CAO CUR OUR\"");
            base.OnPreRender(e);
        }
		
    protected void Page_Load(object sender, EventArgs e)
    {
        string signedReqJson = decodeSignedReq();        
		
		 //if there is no signedRequest, kick to error page
            if(Session["signedRequest"]==null){
                like.Visible = false;
                panelLikeDefault.Visible = false;
                panelForm.Visible = false;
                nolike.Visible = false;
                panelError.Visible = true;
                return;
            }
			
		bool userLikes = parseJsonForLike(Session["signedRequest"].ToString());
        if (userLikes)
        {
            like.Visible = true;
            nolike.Visible = false;
			panelError.Visible = false;
			if (Request.QueryString["show"] != null && Request.QueryString["show"].ToString() == "1")
			{
				panelLikeDefault.Visible = false;
				panelForm.Visible = true;
			}
			else
			{
				panelLikeDefault.Visible = true;
				panelForm.Visible = false;
			}
        }
        else
        {
            like.Visible = false;
            nolike.Visible = true;
        }
    }
    public bool parseJsonForLike(string decodedJson)
    {
        bool result = false;
        if (!string.IsNullOrEmpty(decodedJson))
        {
            if (decodedJson.IndexOf("liked\":") != -1)
            {
                return decodedJson.IndexOf("liked\":true") != -1;
            }
        }
        return result;
    }
    public string decodeSignedReq()
    {
        string signedReq = "FAIL";
        if (!string.IsNullOrEmpty(Request["signed_request"]))
        {
            string encodedSignedReq = Request["signed_request"];
            string[] splitEncReq = encodedSignedReq.Split('.');
            string encodedSig = splitEncReq[0];
            string payload = splitEncReq[1];

            // Needs to pad the payload to the proper length for a Base64Url encoded string
            int missingChars = 4 - (payload.Length % 4);
            if (missingChars < 4)
            {
                for (int i = 0; i < missingChars; i++)
                {
                    payload += "=";
                }
            }

            payload.Replace("-", "+");
            payload.Replace("_", "/");

            // Decode the Payload!  (to UTF8)
            string json = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(payload));
            signedReq = json;
			Session["signedRequest"] = signedReq;
        }
        return signedReq;
    }
}