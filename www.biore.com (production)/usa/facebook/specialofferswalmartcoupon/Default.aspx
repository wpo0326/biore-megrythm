﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="usa_facebook_specialofferswalmartcoupon_Default" %>
<%@ Register TagPrefix="uc1" TagName="ucFormConfig" Src="~/Controls/ucFormConfig.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bior&eacute;&reg; Special Offers - Walmart Coupon</title>
    <link href="css/main.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
    <script type="text/javascript">
        if (typeof jQuery == 'undefined') {
            var localjQuerySrc = 'js/jquery-1.6.2.min.js';
            document.write(unescape("%3Cscript src='" + localjQuerySrc + "' type='text/javascript'%3E%3C/script%3E"));
        }
    </script>
	<script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script> 
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Panel ID="like" runat="server">
				<asp:Panel ID="panelLikeDefault" runat="server">
					<h1>Beat the Summer Heat</h1>
					<h2>With Cool Savings from Bior&eacute;<sup>&reg;</sup> Skincare.</h2>
					<p id="save">Save $1.00 on any Bior&eacute;<sup>&reg;</sup> Skincare Product. And get your freshest clean.</p>
					<p id="walmart">Available at Walmart - Save money, Live Better.</p>
					<p id="hurry">Hurry! Supplies are Limited!</p>
					<p id="btn"><a href="Default.aspx?show=1&promo=biore_us_201108_WalmartCoup&enl_campaign=biore_facebook&enl_medium=affiliate&enl_source=WalmartCoupon">Get your coupon &gt;&gt;</a></p>
					<div id="btm"></div>
					<p id="disclaimer">*Offer good while supplies last. Limit one (1) per person and per household. Offer limited to legal residents of the 50 United States (including D.C.) 18 years or older. Offer excludes trial sizes.</p>
				</asp:Panel>
			  <asp:Panel ID="panelForm" runat="server">
				<%-- Load Config Form --%>
				<uc1:ucFormConfig ID="ucFormMemberInfo" runat="server" />   
				 <script type="text/javascript">
					  $(function() {
					  toggle("noneToggle1");
					  toggle("noneToggle2");
					  toggle("noneToggle3");
					  toggle("noneToggle4");
						  function toggle(className) {
							  var checkToggle = $("." + className + " input:last");
							  checkToggle.click(function() {
							  if ($("#" + checkToggle.attr('id') + ":checked").val()) {
								  $("." + className + " input").each(function() {
								  if ($(this).attr('id') != checkToggle.attr('id')) {
											  $(this).attr("checked", false);
											  $(this).attr('disabled', 'disabled');
										  }
									  });
								  } else {
								  $("." + className + "  input").each(function() {
								  if ($(this).attr('id') != checkToggle.attr('id')) {
											  $(this).removeAttr('disabled');
										  }
									  });
								  }

							  });
						  }
					  }); 						 
				</script>
				</asp:Panel>
			
			</asp:Panel>
            <asp:Panel ID="nolike" runat="server">
                <h1>Psst! We've got something for you.</h1>
                <h2>Like us to peel away the pore strip and get it!</h2>
                <div id="pore">$??? Off</div>
            </asp:Panel>
			 <asp:Panel ID="panelError" runat="server" Visible="false">
			<div id="Div1">
                <h1>We're Sorry, there was an issue loading this page.</h1>
                <p id="P1">The page must be loaded in Facebook.</p>
                <p id="P2">Please refresh the page to try again.</p>
            </div>
        </asp:Panel>
        </div>
    </form>
</body>
</html>