﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="usa_facebook_proveitrewardpoints_Default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bior&eacute;&reg; Prove It!&trade; Reward Points</title>
    <link href="css/main.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="wrapper">
            <div id="container">
                <h1>Prove It!&trade; Reward Points</h1>
                <h2>Coming Soon!</h2>
                <h3>Prove It! Reward Points</h3>
                <h4>Get Rewards for Your Routine</h4>
                <p class="text">Have you heard? We're launching an extra special rewards program that lets you earn points for all the little things you do every day! Build up points - instead of dirt and oil - and redeem them for free products and coupons, plus, more to come!*</p>
                <p id="bold" class="text">Sign up for our newsletter to hear the latest about Prove It!&trade; Reward Points and other Bior&eacute;<sup>&reg;</sup> Skincare offers.</p>
                <p id="btnLink"><a href="http://www.biore.com/usa/optin.aspx" target="_blank">Keep Me In The Loop &gt;&gt;</a></p>
            </div>
            <p id="disclaimer">*Open to legal residents of the 50 US (DC), 18 years and older. Starts 8/1/11 and ends 12/31/13. Void where prohibited. Subject to <a href="https://proveitrewards.bnservers.com/tc.html " target="_blank">Terms and Conditions</a>. Sponsored by Kao Brands Company, 2535 Spring Grove Avenue, Cincinnati, OH 45214.</p>
        </div>
    </form>
</body>
</html>
