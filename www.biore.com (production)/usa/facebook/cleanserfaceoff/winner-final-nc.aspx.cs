﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usa_facebook_cleanserfaceoff_winner_final_nc : System.Web.UI.Page
{
	// This code is in place to allow IE to keep its Cookies and Sessions despite being
	// set on an iFrame in Facebook.  This only affect certain versions & users with specific
	// privacy settings.
	protected override void OnPreRender(EventArgs e)
	{
		Response.AppendHeader("P3P", "CP=\"CAO CUR OUR\"");
		base.OnPreRender(e);
	}
	protected void Page_Load(object sender, EventArgs e)
	{
		initFBShare();


		//Need to remove -- start --
		//panFBNonFanContent.Visible = true;
		//panFBFanContent.Visible = true;
		//panVoteComplete.Visible = true;
		//Need to remove -- end --


		//Need to uncomment
		//initFB();
	}

	private void initFB()
	{
		string signedReqJson = FBUtil.decodeSignedReq();
		bool userLikes = false;

		// Make sure there was a valid signedRequest JSON sent back
		if (!string.IsNullOrEmpty(signedReqJson))
		{
			userLikes = FBUtil.parseJsonForLike(signedReqJson);
		}

		// Turn on/off Fan/Non-Fan content
		if (userLikes)
		{
			showFanContent();
		}
		else
		{
			showNonFanContent();
		}
	}

	private void initFBShare()
	{
		// write AppID to JS variable
		litJSfbAppID.Text = "var fbAppID = '" + System.Configuration.ConfigurationManager.AppSettings["ccFBappId"] + "';";
		////litFBShareURL.Text = System.Configuration.ConfigurationManager.AppSettings["ccFBShareURL"];
		//litFBShareURL2.Text = System.Configuration.ConfigurationManager.AppSettings["ccFBShareURL"];
		//litFBSharePicURL.Text = System.Configuration.ConfigurationManager.AppSettings["ccFBSharePicURL"];
		//litFBShareName.Text = System.Configuration.ConfigurationManager.AppSettings["ccFBShareName"];
		//litFBShareCaption.Text = System.Configuration.ConfigurationManager.AppSettings["ccFBShareCaption"];
		//litFBShareDesc.Text = System.Configuration.ConfigurationManager.AppSettings["ccFBShareDesc"];
		//litFBShareMsg.Text = System.Configuration.ConfigurationManager.AppSettings["ccFBShareMsg"];
		//litFBShareRedirURL.Text = System.Configuration.ConfigurationManager.AppSettings["ccFBShareRedirURL"];
	}

	public void showFanContent()
	{
		// Turn on any Panels that are Fan only here, turn off non-Fan Panels
		panFBFanContent.Visible = true;
		panFBNonFanContent.Visible = false;
		panSuppCopy.Visible = false;
		//Session["bioreFBLike"] = "true";
	}

	public void showNonFanContent()
	{
		//  Turn off any Panels that are Fan only here, turn on non-Fan Panels
		panSuppCopy.Visible = true;
		panFBNonFanContent.Visible = true;
		panFBFanContent.Visible = false;
	}
}
