﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="productRevitalize.aspx.cs" Inherits="facebook__dev_cleanserfaceoff_cleanserpages_productRevitalize" %>
<%@ OutputCache Duration="180" VaryByParam="None"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" 
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="http://www.facebook.com/2008/fbml" lang="en">
<head id="Head1" runat="server">
	<title>Bioré® Cleanser Contest - Blemish Fighting Ice Cleanser</title>	
    <meta property="og:title" content="Bioré® Revitalize 4-in-1 Self Foaming Cleanser"/>
    <meta property="og:type" content="product"/>
    <meta property="og:url" content="http://www.biore.com/usa/facebook/cleanserfaceoff/cleanserpages/productRevitalize.aspx"/>
    <meta property="og:image" content="http://www.biore.com/usa/facebook/cleanserfaceoff/images/products/productRevitalize.png"/>
    <meta property="og:description" content="I voted for Bioré® Revitalize 4-in-1 Self Foaming Cleanser as my favorite Bioré® cleanser in Round 1 of the Favorite Cleanser Face-Off. You can play favorites, too, by voting now!"/> 
    <meta property="fb:admins" content="100000907851544"/>
    <!-- <meta property="fb:app_id" content="170445746336411"/>  -->
    <!-- <meta http-equiv="refresh" content="0;url=http://www.facebook.com/bioreskin"/> -->
</head>
</head>
<body>
    <form id="form1" runat="server">
    <div>
		<div id="fb-root"></div>
		<p>Please use the page at <a href="http://www.facebook.com/bioreskin">Facebook.com/Bioreskin</a> to vote on which cleanser you prefer.</p>    
    </div>
    </form>
    <!-- START OF SmartSource Data Collector TAG -->
    <script src="/usa/js/webtrends.js" type="text/javascript"></script>
    <script type="text/javascript">
        //<![CDATA[
        var _tag=new WebTrends();
        _tag.dcsGetId();
        //]]>>
    </script>
    <script type="text/javascript">
        //<![CDATA[
        // Add custom parameters here.
        //_tag.DCSext.param_name=param_value;
        _tag.dcsCollect();
        //]]>>
    </script>
    <noscript>
        <div><img alt="DCSIMG" id="DCSIMG" width="1" height="1" src="http://sdc.enlighten.com/dcsx5r2z6x4568dp93c02vf9d_4y5t/njs.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=8.6.0"/></div>
    </noscript>
<!-- END OF SmartSource Data Collector TAG -->


    <script type="text/javascript">
      var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
      document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>

    <script type="text/javascript">
      try {
        var firstTracker = _gat._getTracker("UA-385129-27"); // Global Tag
        firstTracker._setDomainName("none");
        firstTracker._trackPageview();
        var secondTracker = _gat._getTracker("UA-385129-4"); // Biore Tag
        secondTracker._setCampNameKey("enl_campaign")
        secondTracker._setCampMediumKey("enl_medium")
        secondTracker._setCampSourceKey("enl_source")
        secondTracker._trackPageview();
      } catch(err) {}
    </script>
    <script type="text/javascript">
        window.location="http://www.facebook.com/bioreskin";
    </script>
</body>
</html>