﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="winner-final-nc.aspx.cs" Inherits="usa_facebook_cleanserfaceoff_winner_final_nc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" 
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="http://www.facebook.com/2008/fbml" lang="en">
<head id="Head1" runat="server">
    <title></title>
	<link href="css/styles.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body class="round4 faceoff8 winner nocoup">
	<script type="text/javascript">
		var fbheight = 1000;
		var getLikes = false;
	</script>
    <form id="form1" runat="server">
    <div id="container">
		<%-- Facebook Content for people that LIKE the page --%>
		<asp:Panel ID="panFBFanContent" CssClass="FBFanDiv" runat="server" Visible="true">
			<div class="textReplacement">
				<h1>Bior&eacute;&reg; Favorite Cleanser Face-off</h1>
				<h2>From the makers of Bior&eacute;&reg; Pore Strips</h2>
				<h3>8 cleansers faced off. You voted. One winner emerged.</h3>	
			
				<div id="prodLeft" class="product">
					<h4>Pore Unclogging Scrub</h4>
					<p>is our Favorite Cleanser Face-off Champion&mdash;and your absolute favorite.</p>
					<p>Thank you for letting us know which Bior&eacute;&reg; cleanser you love the best!</p>
				</div>
				<div id="specialOffer">
					<p>Thank you for voting in the Bior&eacute;&reg; Skincare Favorite Cleanser Face-Off! Unfortunately, due to an overwhelming response, we are all out of coupons. We appreciate your interest in Bior&eacute;&reg; Skincare. To make sure you are the first to know about offers and promotions, <a href="http://www.biore.com/usa/optin.aspx" target="_blank">click here</a>.</p>
					
				</div>						
			</div>
			<p class="disclaimer">*Limit one (1) per person and per household. Offer limited to legal residents of the 50 United States (including D.C.) 18 years or older. Not redeemable in manner other than provided herein. Cannot be combined with other offers or discounts. Void where prohibited, taxed, or restricted by law. Not responsible for lost, late, incomplete, or misdirected requests. Requests not complying with all offer requirements will not be honored. Duplicate requests will constitute fraud. Offer good 4/5/11-4/6/11 or while supplies last.</p>
			
			<script type="text/javascript">
				fbheight = 1300;
			</script>		
		</asp:Panel>
		
		<%-- Facebook Content for people that do NOT LIKE the page --%>
        <asp:Panel ID="panFBNonFanContent" CssClass="FBNonFanDiv" runat="server" Visible="false">
			<div class="textReplacement">
				<h1>Bior&eacute;&reg; Favorite Cleanser Face-off</h1>
				<h2>From the makers of Bior&eacute;&reg; Pore Strips</h2>
				<h3>The Bior&eacute;&reg; Skincare <strong>Favorite Cleanser Face-Off</strong> is On!</h3>
				<p>Like us to vote!</p>	
			</div>
			
			<script type="text/javascript">
				fbheight = 900;
			</script>		
        </asp:Panel>
        
        <%-- Content for people that have voted --%>
		<%--<asp:Panel ID="panVoteComplete" CssClass="voted" runat="server" Visible="false">
			<div class="textReplacement">
				<h1>Bior&eacute;&reg; Favorite Cleanser Face-off</h1>
				<h2>From the makers of Bior&eacute;&reg; Pore Strips</h2>
				<h3>8 cleansers faced off. You voted. One winner emerged.</h3>				
			</div>
			
			<div id="votedShield">
				<div id="votedProdLeft" class="voteBox">
					<h4>Steam Activated Cleanser</h4>
					<p><span>Processing...</span></p>
				</div>
				<div id="votedProdRight" class="voteBox">
					<h4>Detoxify Daily Scrub</h4>
					<p><span>Processing...</span></p>
				</div>
				<a href="http://www.biore.com/usa/optin.aspx?promo=biore_us_201104FaceOffBFICSamp" target="_blank">Request your free cleanser sample* Click Here</a>
			</div>
			
			<script type="text/javascript">
				fbheight = 1040;
				getLikes = true;
			</script>		
		</asp:Panel>--%>
        
        <%-- Content that shows regardless of LIKE status --%>
        <asp:Panel ID="panSuppCopy" CssClass="suppCopy" runat="server" Visible="false">
			<div id="supplementalCopy">
				<ul>
					<li class="col1 instr1"><span>1</span> Check out the Bior&eacute;<sup>&reg;</sup> cleansers facing off in each bracket</li>
					<%--<li class="col2 instr3"><span>3</span> Request your <strong>free cleanser sample</strong><br />(one per household, see rules below)*</li>--%>
					<li class="col2 instr3"><span>3</span> Check back to find out which cleanser won—and to vote again on the next round!</li>
				
					<li class="col1 instr2"><span>2</span> Vote on your favorite</li></ul>
				<p>Be sure to check back again on Tuesday, 4/5&mdash;we'll <strong>reveal</strong> which cleanser is the <strong>Champion</strong>, plus a <strong>special offer from the Bior&eacute;<sup>&reg;</sup> Skincare team!**</strong> That's what we call a win-win situation.</p>
				<h4>So go ahead, play favorites!</h4>
				<%--<p class="disclaimer">*Receive one sample trial size Bior&eacute;<sup>&reg;</sup> Blemish Fighting Ice Cleanser. Limit one (1) per person and per household. Offer limited to legal residents of the 50 United States (including D.C.) 18 years or older. You must have a valid home street address. Not redeemable in manner other than provided herein. Cannot be combined with other offers or discounts. Allow 8 to 10 weeks for delivery, except where specified. Void where prohibited, taxed, or restricted by law. Not responsible for lost, late, incomplete, or misdirected requests. Requests not complying with all offer requirements will not be honored. Duplicate requests will constitute fraud. Offer good through 4/5/11 11:59PM EST or while supplies last.</p>--%>
				<p class="disclaimer">** 4/5/11-4/6/11 or while supplies last.</p>
			</div>
		</div>
	</asp:Panel>
    
    <%-- Utility and JavaScript --%>
    <div id="fb-root"></div>    
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <script type="text/javascript" src="//connect.facebook.net/en_US/all.js"></script>

    <script type="text/javascript">
		// The following creates the fbAppID JS variable
    	<asp:Literal ID="litJSfbAppID" runat="server" /> 
    </script>    
	<script src="js/main.js" type="text/javascript"></script>
	<!-- START OF SmartSource Data Collector TAG -->
	<script src="/usa/js/webtrends.js" type="text/javascript"></script>
	<script type="text/javascript">
		//<![CDATA[
		var _tag=new WebTrends();
		_tag.dcsGetId();
		//]]>>
	</script>
	<script type="text/javascript">
		//<![CDATA[
		// Add custom parameters here.
		//_tag.DCSext.param_name=param_value;
		_tag.dcsCollect();
		//]]>>
	</script>
	<noscript>
		<div><img alt="DCSIMG" id="DCSIMG" width="1" height="1" src="http://sdc.enlighten.com/dcsx5r2z6x4568dp93c02vf9d_4y5t/njs.gif?dcsuri=/nojavascript&amp;WT.js=No&amp;WT.tv=8.6.0"/></div>
	</noscript>
<!-- END OF SmartSource Data Collector TAG -->


	<script type="text/javascript">
	  var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	  document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
	</script>

	<script type="text/javascript">
	  try {
		var firstTracker = _gat._getTracker("UA-385129-27"); // Global Tag
		firstTracker._setDomainName("none");
		firstTracker._trackPageview();
		var secondTracker = _gat._getTracker("UA-385129-4"); // Biore Tag
		secondTracker._setCampNameKey("enl_campaign")
		secondTracker._setCampMediumKey("enl_medium")
		secondTracker._setCampSourceKey("enl_source")
		secondTracker._trackPageview();
	  } catch(err) {}
	</script>
	<%-- <asp:HiddenField ID="hidVoted" runat="server" /> --%>
    </form>
</body>
</html>