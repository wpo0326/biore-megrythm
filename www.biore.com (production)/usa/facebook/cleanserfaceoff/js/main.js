﻿// Things to do on page load
$(function(undefined) {
	// Shim the console object so it doesn't throw a Javascript error if there is no console defined
	//	if (console === undefined) {
	//		console = { log: function() { } };
	//	}

	// Initialize FB object for FB SDK if needed
	FB.init({
		appId: fbAppID,
		status: true, // check login status
		cookie: true, // enable cookies to allow the server to access the session
		xfbml: true  // parse XFBML
	});

	// Do something when a like button is click in the page
	FB.Event.subscribe('edge.create', function() {
		//$("#hidVoted").val("true");
		$("form").submit();
	});

	//FB.Event.subscribe('edge.remove', function() {
	//Do stuff if they unlike the page
	//});	

	// Delay calling FB.Canvas.setAutoResize to allow for FB initilization
	// Or can use FB.Canvas.setSize({ width: 520, height: 1200 });
	setTimeout(function() {
		FB.Canvas.setSize({ width: 520, height: fbheight || 1000 });
		FB.Canvas.setAutoResize();
	}, 500);

	//Setup Share link event
	$("a.openDialogLink").click(function(e) {
		e.preventDefault();
		openFeedDialog();
	});

	if (getLikes) {
		var $prodLeft = $("#votedProdLeft p span");
		var $prodRight = $("#votedProdRight p span");
		var prodLeftUrl = $("#votedProdLeft").attr("rel");
		var prodRightUrl = $("#votedProdRight").attr("rel");
		setTimeout(function() {
			displayLikes(prodLeftUrl, $prodLeft);
			displayLikes(prodRightUrl, $prodRight);
		}, 250);
	}
});

function displayLikes(url, $pageElement) {
	FB.api(
	{
		method: 'links.getStats',
		urls: url
	},
	function(response) {
		//console.log(response);
		//console.log(response[0].total_count);
		$pageElement.text(response[0].total_count + " votes");
		//return response[0].total_count;
	});
}