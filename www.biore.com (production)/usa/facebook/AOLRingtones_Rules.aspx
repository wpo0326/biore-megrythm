<%@ Page Language="C#" MasterPageFile="../MasterPages/Main.master" AutoEventWireup="true"
    CodeFile="AOLRingtones_Rules.aspx.cs" Inherits="Rules" Title="Untitled Page" Debug=true %>

<%@ MasterType VirtualPath="../MasterPages/Main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

<%@ Register TagPrefix="CC1" Namespace="CustomValidators"
    Assembly="CustomValidators" %>
    <meta name="Description" content="" />
    <meta name="Keywords" content="Bior�, contact Bior�, email Bior�, mail Bior�,, phone Bior�, address, phone number, toll free, call Bior�, write Bior�" />
    <meta name="DCSext.submit" content="true" runat="server" visible="false" id="metaSubmit" />
    <link rel="stylesheet" type="text/css" href="css/rules.css" media="screen, projection" />
	<!--[if lt IE 7]>
	<script>
	    DD_belatedPNG.fix('#optin_inner, #ctl00_ContentPlaceHolder1_OptinForm h1');
	</script>
	<![endif]--> 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="rules">
        <div id="optin_inner">
			<h1>BIOR&Eacute;&reg; SKINCARE RINGTONE GIVEAWAY</h1>
			<h2>Official Rules</h2>
			<p><strong>NO PURCHASE NECESSARY TO ENTER OR WIN. A PURCHASE DOES NOT IMPROVE YOUR CHANCES OF WINNING.</strong></p>
			<p><strong>Giveaway may only be entered in or from the 50 United States and the District of Columbia and entries originating from any other jurisdiction are not eligible for entry. This Giveaway is governed exclusively by the laws of the United States. You are not authorized to participate in the Giveaway if you are not located within the 50 United States or the District of Columbia.</strong></p>
			<ol>
			  <li><strong>How to Enter</strong>. Beginning at <strong>12:00 p.m.</strong> Eastern Time (&quot;ET&quot;) on Friday, July 30, 2010, Sponsor will post a message at <a href="http://www.facebook.com/bioreskin" target="_blank">http://www.facebook.com/bioreskin</a> informing consumers about the giveaway. The first one-thousand (1,000) participants to complete entry will receive an mp3 ringtone download. Once one-thousand (1,000) eligible participants have successfully entered, the Bior&eacute;&reg; Skincare Ringtone Giveaway (&quot;Giveaway&quot;) will be over and no other prizes will be awarded. Participants enter by inputting their mobile phone number in the application (&quot;Application&quot;) on the Giveaway tab on the <strong>Bior&eacute;&reg; Skincare</strong> Page on Facebook. Once participants input their phone number into the Application they receive a text message with a SERIAL # (&quot;Serial&quot;) and a PIN # (&quot;Pin&quot;) on their mobile phone. Users can then enter the Serial and Pin at <a href="http://www.us.puretracks.com" target="_blank">http://www.us.puretracks.com</a> to redeem one (1) Prize. Once redeemed, the Prize is sent directly to the Participant's mobile phone for use. NOTE: For prize fulfillment, message and data rates may apply and be charged to your wireless bill or be deducted from your prepaid balance. Visit your carrier for more information. Participants are responsible for all data charges and text message fees imposed by their wireless carrier.
				<p>Limit one (1) Pin per person, per phone number. No automated entry devices and/or programs permitted. Ringtones can only be downloaded for US/US Mobile Networks from the following carriers:</p>
        <p>AT&amp;T Mobile</p>
        <p>Alltel</p>
        <p>Verizon Wireless</p>
        <p>T-Mobile</p>
        <p>Sprint (including Virgin/Boost/Nextel)</p>
        <p>Cellular One</p></li> 
			  <li><strong>Start/End Dates</strong>.  Giveaway begins at <strong>12:00 p.m.</strong> on July 30, 2010 and ends once one-thousand (1000) eligible participants have successfully completed entry, or on September 30, 2010, whichever occurs first.</li>
				<li><strong>Eligibility</strong>.  Participation open to individuals who (i) are legal residents of the 50 United States or the District of Columbia who at time of entry reside within the 50 United States and the District of Columbia; and, (ii) are age 18 years or older. Void outside of the 50 United States and the District of Columbia, and where prohibited, taxed or restricted by law. Employees, officers and directors of Sponsor, AOL Employees and its parent companies, subsidiaries, affiliates, partners, advertising and giveaway agencies, manufacturers or distributors of promotion materials and their immediate families (parents, children, siblings, spouse) or members of the same household (whether related or not) of such employees/officers/directors are not eligible to enter. Giveaway may only be entered in or from the 50 United States and the District of Columbia, and entries originating from any other jurisdiction are not eligible for entry. All federal, state and local laws and regulations apply.</li>								
				<li><strong>Prizes</strong>.  <strong>One thousand (1,000) Prizes</strong>. One thousand (1,000) Prize winners will receive one (1) mp3 ringtone (&quot;Prize&quot;). Prize winners will select from a list of available ringtones. Approximate Retail Value (&quot;ARV&quot;) of each Prize: $0.99. Total ARV of all Prizes combined: $990.00. Limit one (1) Prize per person. Prizes are non-transferable. No substitutions or cash redemptions. In the case of unavailability of any prize, Sponsor reserves the right to substitute a prize of equal or greater value. All expenses not specifically listed herein are the responsibility of winners.</li>
				<li><strong>Conditions</strong>.  All taxes are the sole responsibility of the winners. By participating, entrants and winners agree to release and hold harmless Sponsor, Facebook, AOL and their advertising and promotion agencies and their parent companies, subsidiaries, affiliates, partners, representatives, agents, successors, assigns, employees, officers and directors, from any and all liability, for loss, harm, damage, injury, cost or expense whatsoever including without limitation, property damage, personal injury and/or death which may occur in connection with, preparation for, travel to, or participation in Giveaway, or possession, acceptance and/or use or misuse of prize or participation in any Giveaway-related activity and claims based on publicity rights, defamation or invasion of privacy and merchandise delivery. Entrants who do not comply with these Official Rules, or attempt to interfere with this giveaway in any way shall be disqualified. There is no purchase or sales presentation required to participate. A purchase does not increase odds of winning. </li>
				<li><strong>Additional Terms for Online Giveaway</strong>. Sponsor reserves the right, in its sole discretion, to cancel, terminate, modify, or suspend this Giveaway should (in its sole discretion) virus, bugs, non-authorized human intervention or other causes beyond its control corrupt or affect the administration, security, fairness or proper conduct of the Giveaway. In such case, Sponsor will select the winners from all eligible entries received prior to and/or after (if appropriate) the action taken by Sponsor. Sponsor reserves the right, at its sole discretion, to disqualify any individual it finds, in its sole discretion, to be tampering with the entry process or the operation of the Giveaway or Application. For details regarding collection of information from users of the Application (including entrants), please consult the <a href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">privacy policy</a> on the Application. Additionally, Sponsor reserves the right to prosecute any fraudulent activities to the full extent of the law. In case of dispute as to the identity of any entrant, entry will be declared made by the authorized account holder of the wireless phone number submitted at time of entry. &quot;Authorized Account Holder&quot; is defined as the natural person who is assigned the wireless phone number. Any other attempted form of entry is prohibited; no automatic, programmed; robotic or similar means of entry are permitted. Sponsor, and its parent companies, subsidiaries, affiliates, partners and giveaway and advertising agencies are not responsible for technical, hardware, software, telephone or other communications malfunctions, errors or failures of any kind, lost or unavailable network connections, Web site, Internet, or ISP availability, unauthorized human intervention, traffic congestion, incomplete or inaccurate capture of entry information (regardless of cause) or failed, incomplete, garbled, jumbled or delayed computer transmissions which may limit one's ability to enter the Giveaway, including any injury or damage to participant's or any other person's computer relating to or resulting from participating in this Giveaway or downloading any materials in this Giveaway.
        <p><em>CAUTION: ANY ATTEMPT BY AN ENTRANT TO DELIBERATELY DAMAGE ANY APPLICATION OR UNDERMINE THE LEGITIMATE OPERATION OF THE GIVEAWAY MAY BE A VIOLATION OF CRIMINAL AND CIVIL LAWS AND SHOULD SUCH AN ATTEMPT BE MADE, THE SPONSOR RESERVES THE RIGHT TO SEEK DAMAGES FROM ANY SUCH PERSON TO THE FULLEST EXTENT PERMITTED BY LAW.</em></p></li>
				<li><strong>Use of Data</strong>. Sponsor will be collecting personal data about entrants online, in accordance with its privacy policy solely for purposes of conducting this Giveaway. Please review the Sponsor's privacy policy at <a href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">http://www.kaobrands.com/privacy_policy.asp</a>. By participating in the Giveaway, entrants hereby agree to Sponsor's collection and usage of their personal information and acknowledge that they have read and accepted Sponsor's privacy policy.<br />http://www.biore.com/usa/</li>
				<li><strong>Sponsor</strong>. Kao Brands Company, 2535 Spring Grove Avenue, Cincinnati, OH 45214-1773.  Facebook is not a sponsor or administrator of the Giveaway. Any questions or comments regarding this Giveaway should be directed to Sponsor and not to Facebook.</li>
</ol>
<p>&copy;2010 Kao Brands Company. All rights reserved.</p>


    </div>
    </div>
    <!-- end #content -->
    <div id="content_bottom">
    </div>
</asp:Content>
