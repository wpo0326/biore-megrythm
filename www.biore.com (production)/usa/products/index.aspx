<%@ OutputCache Duration="300" VaryByParam="none" %>
<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true"
    CodeFile="index.aspx.cs" Inherits="products_index" Title="Untitled Page" Debug="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta name="Description" content="View all of the Biore skincare products." />
    <meta name="Keywords" content="Biore, products, deep cleansing, skin care, cleansers, remove makeup, detoxify, scrub, microderm, exfoliator, pore unclogging, astringent, pore strips, moisturizer, eye cream" />
    <script type="text/javascript" src="/usa/js/products.js"></script>
	<!--[if lt IE 7]>
	<script type="text/javascript">
	    DD_belatedPNG.fix('#prod_index h2, #signup a span, #cat_bar_grad');
	</script>
	<![endif]--> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="prod_index">
        <div id="cat_bar_grad"></div>
        <div id="signup">
			<img id="proveItLogo" src="../images/products/index/h2_ProveItLogo.png" alt="Prove It!&trade; Reward Points"/>
            <img id="loveOurStuff" src="../images/products/index/h3_LoveOurStuff.png" alt="love our stuff?">
            <p>
			
                Join us on Facebook<br />
                and start earning points<br />
                for great rewards.<br />
				<a target="_blank" href="http://www.facebook.com/bioreskin?sk=app_205787372796203">Start Earning Now
				</a>
			</p>
        </div>
		<div id="comingSoon">
			<a href="/usa/new-style/" >Coming Soon! Keep an eye out for our New Pagckageing!</a>		
		</div>
        <div id="deep_cleansing" class="prod_cat">
            <h3>
            <asp:Literal ID="lt1" runat="server" /></h3>
            <asp:Repeater ID="rp1" runat="server">
                <ItemTemplate>
                     <div class="prod_link" id="plink_<%# Eval("ProductID")%>">
                        <a href="product_detail.aspx?pid=<%# Eval("ProductID")%>">
                        <span style="display: table; height: 122px; #position: relative; overflow: hidden;">
                            <span style=" #position: absolute; #top: 50%;display: table-cell; vertical-align: middle;">
                                <span style=" #position: relative; #top: -50%">
                                    <span class="prod_summ" id="psumm_<%# Eval("ProductID")%>"><%# Eval("Summary")%></span>
                                </span>
                            </span>
                        </span>
                     
                          <span style="display: table; height: 52px; #position: relative; overflow: hidden;">
                            <span style=" #position: absolute; #top: 50%;display: table-cell; vertical-align: middle;">
                                <span style=" #position: relative; #top: -50%">
                                    <span class="prod_title"><%# ConvertReg(Eval("NameTextDisplay").ToString())%></span>                            
                                </span>
                            </span>
                        </span>
                        </a>
                    </div>   
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div id="complexion_clearing" class="prod_cat">
            <h3>
                <asp:Literal ID="lt2" runat="server" /></h3>
            <asp:Repeater ID="rp2" runat="server">
                <ItemTemplate>
                     <div class="prod_link" id="plink_<%# Eval("ProductID")%>">
                        <a href="product_detail.aspx?pid=<%# Eval("ProductID")%>">
                        <span style="display: table; height: 122px; #position: relative; overflow: hidden;">
                            <span style=" #position: absolute; #top: 50%;display: table-cell; vertical-align: middle;">
                                <span style=" #position: relative; #top: -50%">
                                    <span class="prod_summ" id="psumm_<%# Eval("ProductID")%>"><%# Eval("Summary")%></span>
                                </span>
                            </span>
                        </span>
                     
                          <span style="display: table; height: 52px; #position: relative; overflow: hidden;">
                            <span style=" #position: absolute; #top: 50%;display: table-cell; vertical-align: middle;">
                                <span style=" #position: relative; #top: -50%">
                                    <span class="prod_title"><%# ConvertReg(Eval("NameTextDisplay").ToString())%></span>                            
                                </span>
                            </span>
                        </span>
                        </a>
                    </div> 
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div id="pore_strips" class="prod_cat">
            <h3>
                <asp:Literal ID="lt3" runat="server" /></h3>
            <asp:Repeater ID="rp3" runat="server">
                <ItemTemplate>
                     <div class="prod_link" id="plink_<%# Eval("ProductID")%>">
                        <a href="product_detail.aspx?pid=<%# Eval("ProductID")%>">
                        <span style="display: table; height: 122px; #position: relative; overflow: hidden;">
                            <span style=" #position: absolute; #top: 50%;display: table-cell; vertical-align: middle;">
                                <span style=" #position: relative; #top: -50%">
                                    <span class="prod_summ" id="psumm_<%# Eval("ProductID")%>"><%# Eval("Summary")%></span>
                                </span>
                            </span>
                        </span>
                     
                          <span style="display: table; height: 52px; #position: relative; overflow: hidden;">
                            <span style=" #position: absolute; #top: 50%;display: table-cell; vertical-align: middle;">
                                <span style=" #position: relative; #top: -50%">
                                    <span class="prod_title"><%# ConvertReg(Eval("NameTextDisplay").ToString())%></span>                            
                                </span>
                            </span>
                        </span>
                        </a>
                    </div> 
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div id="moisturizing" class="prod_cat">
            <h3>
                <asp:Literal ID="lt4" runat="server" /></h3>
            <asp:Repeater ID="rp4" runat="server">
                <ItemTemplate>
                     <div class="prod_link" id="plink_<%# Eval("ProductID")%>">
                        <a href="product_detail.aspx?pid=<%# Eval("ProductID")%>">
                        <span style="display: table; height: 122px; #position: relative; overflow: hidden;">
                            <span style=" #position: absolute; #top: 50%;display: table-cell; vertical-align: middle;">
                                <span style=" #position: relative; #top: -50%">
                                    <span class="prod_summ" id="psumm_<%# Eval("ProductID")%>"><%# Eval("Summary")%></span>
                                </span>
                            </span>
                        </span>
                     
                          <span style="display: table; height: 52px; #position: relative; overflow: hidden;">
                            <span style=" #position: absolute; #top: 50%;display: table-cell; vertical-align: middle;">
                                <span style=" #position: relative; #top: -50%">
                                    <span class="prod_title"><%# ConvertReg(Eval("NameTextDisplay").ToString())%></span>                            
                                </span>
                            </span>
                        </span>
                        </a>
                    </div> 
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
