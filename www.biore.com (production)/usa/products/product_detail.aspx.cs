using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;

public partial class products_product_detail : System.Web.UI.Page
{
    #region MasterPage Setters
    /// <summary>
    /// Methods to set tag properties on the Master Page.
    /// </summary>
	public int pID = 0;

    public string BodyCssClass
    {
        set
        {
            if (Master.FindControl("middle_content") != null)
            {
                ((HtmlGenericControl)Master.FindControl("middle_content")).Attributes.Add("class", value);
            }
        }
    }
	
    public string FooterCssClass
    {
        set
        {
            if (Master.FindControl("footer") != null)
            {
                ((HtmlGenericControl)Master.FindControl("footer")).Attributes.Add("class", value);
            }
        }
    }

    public string PromoDisclaimerText
    {
        set
        {
            if (Master.FindControl("promo_disclaimer") != null)
            {
                ((HtmlGenericControl)Master.FindControl("promo_disclaimer")).InnerHtml = value;
            }
        }
    }
#endregion
	
    protected void Page_Load(object sender, EventArgs e)
    {
		BodyCssClass = "mc_prod_detail"; //Set body class		
		((WebControl)Master.FindControl("gn1")).CssClass = "global_anchor active"; //Set nav class
		
        if (Request.QueryString["pid"] != null)
        {
            Regex reg = new Regex(@"^\d{1,2}$");
            if (reg.IsMatch(Request.QueryString["pid"])) //Make sure value is numeric
            {               
                pID = Int32.Parse(Request.QueryString["pid"].ToString());
            }
        }

        Page.Header.Title = "Bior&eacute;&reg; Skincare: Product Detail";

        if (pID > 0 && pID < 18 && pID != 5 && pID != 15) //Check DB only if QString > 0
        {
            //Make SQL Connection and retrieve data
            using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["bioreDB"].ToString()))
            {
                SqlCommand cmd = new SqlCommand("GetProductInfo", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                // Create the DataSet
                DataSet ds = new DataSet();

                cmd.Parameters.Add("@pID", SqlDbType.Int);
                cmd.Parameters["@pID"].Value = pID;
                cn.Open();

                using(SqlDataAdapter adapter = new SqlDataAdapter(cmd))
				{
					adapter.Fill(ds);

					//Fill Data
					renderMainContent(ds);	//Render Middle Content/When to Use and Buy Now
					renderBadge(ds);        //Render Badge
					renderBtnCirc(ds);      //Place Circular Button
					renderPromo(ds);        //Generate Promo if it exists  
					renderSeeDetail(ds);    //Generate See Detail link//Generate See Detail link
					
					//Bind WorksWith List
					rp1.DataSource = ds.Tables[1];
					rp1.DataBind();			
				}				
            }
        }
        else Response.Redirect("index.aspx");
    }
	
	protected string ConvertCase(String strval){
        //Convert string to lowercase
        string myString = strval.ToString();
        return ConvertReg(myString.ToLower());
    }
	
		protected string ConvertReg(String strval)
    {
        //Convert string to lowercase
        string myString = strval.ToString().Replace("&#174;", "<sup>&#174;</sup>");
        myString = myString.Replace("&reg;", "<sup>&#174;</sup>");
        return myString;
    }

    #region Render Page Content functions	
    protected void renderMainContent(DataSet ds)
    {
        /// <summary>
        /// Method to render Middle Content/When to Use and Buy Now html portion of product detail.
        /// </summary>
        ///
		StringBuilder sb = new StringBuilder();
		StringBuilder df = new StringBuilder();
		StringBuilder htu = new StringBuilder();
		
        subhead.Text = ds.Tables[0].Rows[0]["Subhead"].ToString();

		sb.Append(ds.Tables[0].Rows[0]["WhatItIs"].ToString());
		sb.Append(" <a href=\"#TB_inline&height=420&width=472&inlineId=ltIngred&opacity=.5\" class=\"thickbox lkPop\">");
        sb.Append("<span>Ingredients</span></a>");
		ltWhatItIs.Text = sb.ToString();
		
        df.Append(ds.Tables[0].Rows[0]["TheDifference"].ToString());
        df.Append(renderSpotPromo(ds));        //Spot Promo Display
        ltDifference.Text = df.ToString();
        
		htu.Append(ds.Tables[0].Rows[0]["HowToUse"].ToString());
		htu.Append(" <a id=\"cautionsURL\" href=\"#TB_inline&height=420&width=472&inlineId=ltCautions&opacity=.5\" class=\"thickbox lkPop\">");
        htu.Append("<span>Cautions</span></a>");
		ltHowToUse.Text = htu.ToString();		
		
		when_freq.Text = ds.Tables[0].Rows[0]["WhenFrequency"].ToString();
        when_duration.Text = ds.Tables[0].Rows[0]["WhenDuration"].ToString();
        hlBuy.NavigateUrl = ds.Tables[0].Rows[0]["BtnBuyNowURL"].ToString();
        hlBuy.Target = "_blank";
        prod_image.ImageUrl = "/usa/images/products/h1/" + pID + ".gif";
        prod_image_sm.ImageUrl = "/usa/images/products/small/" + ds.Tables[1].Rows[0]["WorksWithProductID"] + ".png";
        prod_image_sm.AlternateText = Server.HtmlDecode(ds.Tables[1].Rows[0]["NameText"].ToString());
        prod_main_info.Attributes["class"] = "prod_main_info p" + pID;
		ltIngred.Text = renderIngredients(ds.Tables[0].Rows[0]["Ingredients"].ToString().ToUpper()); //Render Ingredients
		ltCautions.Text = renderCautions(ds.Tables[0].Rows[0]["Cautions"].ToString()); //Render Cautions

		//Fun Facts heading for Pore Strips
        if (ds.Tables[0].Rows[0]["CategoryID"].ToString() == "3") //Pore Strips "The Difference" = Fun Facts
        {
            hlDiff.Text = "fun facts";
        }
		
		//Set Meta Content
        Page.Header.Title = Server.HtmlDecode(ds.Tables[0].Rows[0]["MetaTitle"].ToString());
		StringBuilder mt = new StringBuilder();

        mt.Append("<meta name=\"Description\" content=\"");
		mt.Append(Server.HtmlDecode(ds.Tables[0].Rows[0]["MetaDesc"].ToString()));
		mt.Append("\" />\n");
        mt.Append("<meta name=\"Keywords\" content=\"");
		mt.Append(Server.HtmlDecode(ds.Tables[0].Rows[0]["MetaTags"].ToString()));
		mt.Append("\" />\n");		
		ltMeta.Text = mt.ToString(); 
 }	
	
	protected void renderBtnCirc(DataSet ds)
    {
        /// <summary>
        /// Method to render BtnCirc LearnMore/Watch Video portion of product detail.
        /// </summary>
        int btnCircVal = Int32.Parse(ds.Tables[0].Rows[0]["BtnCirc"].ToString());
        int btnCircPosVal = 0;

        //Set Position only if value != DBNull
        if (!DBNull.Value.Equals(ds.Tables[0].Rows[0]["BtnCircPos"]))
        {
            btnCircPosVal = Int32.Parse(ds.Tables[0].Rows[0]["BtnCircPos"].ToString());
        }

        if (btnCircVal > 0)
        {
            String btnCircID = (btnCircVal == 1) ? "learn_more" : "watch_video";
            String btnCircPos = (btnCircPosVal == 1) ? " low" : "";
            String btnCircLabel = (btnCircVal == 1) ? "Learn More" : "Watch Video";

            StringBuilder sb = new StringBuilder();
            sb.Append("<div class=\"");
            sb.Append(btnCircID);
            sb.Append(btnCircPos);


			//Assemble lightbox content
            if (btnCircVal == 2) //Pull video content
            {	
				string divID = "lbVideo";
				string btnURL = ds.Tables[0].Rows[0]["PR_TBAssetURL"].ToString();
				string vidDuration = ds.Tables[0].Rows[0]["PR_TBAssetDuration"].ToString();
				string vidWidth = ds.Tables[0].Rows[0]["PR_TBAssetWidth"].ToString();
				string vidHeight = ds.Tables[0].Rows[0]["PR_TBAssetHeight"].ToString();

                //Write Javascript function call for playVideo()
                btnPlayVideo.Text = renderVideo(btnURL, "product_detail_flash_lbVideo", vidDuration, vidWidth, vidHeight);

                //Write Watch Video button
                sb.Append("\"><a id=\"btnVid\" class=\"showVideo\" href=\"\">");
                sb.Append(btnCircLabel);
                sb.Append("</a>\n");
				
                //Determine whether to display Video or Image
                if (btnURL.IndexOf("mp4") > 0)
                {
                    //sb.Append(renderVideo(btnURL, divID));
                }
                else {
					sb.Append(renderImage(btnURL, divID));
                }
            }
			else if (btnCircVal == 1)
			{
                sb.Append("\"><a href=\"");
                sb.Append(ds.Tables[0].Rows[0]["PR_TBAssetURL"]);
                sb.Append("\" class=\"thickbox\">");
                sb.Append(btnCircLabel);
                sb.Append("</a>");
			}
            else //Pull image content
            {
                sb.Append("\"><a href=\"");
                sb.Append(ds.Tables[0].Rows[0]["PR_TBAssetURL"]);
                sb.Append("\">");
                sb.Append(btnCircLabel);
                sb.Append("</a>");
            }
            sb.Append("</div>");            
            ltCirc.Text = sb.ToString();
        }                   
    }
	
    protected string renderVideo(string URL, string ID, string vidDuration, string vidWidth, string vidHeight)
    {
        /// <summary>
        /// Method to render BtnCirc Watch Video Javascript function calls for playVideo()
        /// </summary>
        StringBuilder sb = new StringBuilder();

        //example string being built : playVideo("", btnURL ,63,640,360,"product_detail_flash_lbVideo");
		sb.Append("playVideo(\"/usa/flash/videos/");
		sb.Append(URL.Trim());
		sb.Append("\", \"");
		sb.Append(vidDuration.Trim());
		sb.Append("\", \"");
		sb.Append(vidWidth.Trim());
		sb.Append("\", \"");
		sb.Append(vidHeight.Trim());
		sb.Append("\", \"");
		sb.Append(ID.Trim());
		sb.Append("\", \"\");");


        return sb.ToString();
    }	

    protected void renderPromo(DataSet ds)
    {
        /// <summary>
        /// Method to generate promo html portion of product detail.
        /// </summary>
                int promoID = Int32.Parse(ds.Tables[0].Rows[0]["PromoID"].ToString()); //Set promoID 
                
                if (promoID > 0)
                {
					string divID = "lbPromo";
                    StringBuilder pr = new StringBuilder();
                    pr.Append("<div id=\"prod_promo\" class=\"promo");
                    pr.Append(promoID);
                    pr.Append("\">\n");
                    pr.Append("<h5>");
                    pr.Append(ds.Tables[2].Rows[0]["PM_Title"]);
                    pr.Append("</h5>\n");
                    pr.Append("<p>");
					pr.Append("<a href=\"");
					
					//Branch based on media type swf or html
                    String pmURL = ds.Tables[2].Rows[0]["PM_TBAssetURL"].ToString();
					string vidDuration = ds.Tables[2].Rows[0]["PM_TBAssetDuration"].ToString();
            		string vidWidth = ds.Tables[2].Rows[0]["PM_TBAssetWidth"].ToString();
            		string vidHeight = ds.Tables[2].Rows[0]["PM_TBAssetHeight"].ToString();

                    if (pmURL.IndexOf("mp4") > 0){
                        //Write Javascript call to playVideo
                        promoPlayVideo.Text = renderVideo(pmURL, "product_detail_flash_lbVideo", vidDuration, vidWidth, vidHeight);
                        //Finish link for swf
                        pr.Append("\" id=\"promoVid\" class=\"showVideo\"");
					}
					else if (pmURL.IndexOf(".com") > 0){
						//Finish link for external URL
						pr.Append(pmURL);
						pr.Append("\" target=\"_blank\"");
					}
					else if (pmURL.IndexOf("standardLink=1") > 0){
						//Finish link for internal URL
						pr.Append(pmURL);
						pr.Append("\"");
					}					
					else {
                        //Finish link for html
                        pr.Append(pmURL);
						//pr.Append(divID);
						pr.Append("\" class=\"thickbox\"");
					}

					pr.Append(">");
                    pr.Append(ds.Tables[2].Rows[0]["PM_Description"]);
                    pr.Append("</a>");
					pr.Append("</p></div>\n");
                    ltPromo.Text = pr.ToString();
                    FooterCssClass = "withProdPromo";

                    //Set Disclaimer if it exists
                    if (!DBNull.Value.Equals(ds.Tables[2].Rows[0]["PM_Disclaimer"]))
                    {
                        PromoDisclaimerText = ds.Tables[2].Rows[0]["PM_Disclaimer"].ToString();
                    }

                }
    }

    protected string renderImage(string URL, string ID)
    {
        /// <summary>
        /// Method to render Image Watch Image  and Lightbox.
        /// </summary>
        StringBuilder sb = new StringBuilder();
        sb.Append("\n<div id=\"");
        sb.Append(ID);
        sb.Append("\" style=\"display:none;\">\n");
        sb.Append("<a href=\"#\" class=\"video_close\"></a>\n");
        sb.Append("<img src=\"/usa/images/products/lightbox/");
        sb.Append(URL);
        sb.Append("\" alt=\"\" height=\"401\" width=\"537\" style=\"margin-top:30px;\" />\n");
		sb.Append("</div>\n");
        
        return sb.ToString();
    }

    protected string renderIngredients(string innerText)
    {
        /// <summary>
        /// Method to render Ingredients Watch Image  and Lightbox.
        /// </summary>
        StringBuilder sb = new StringBuilder();
        sb.Append("\n<div id=\"ltIngred\" style=\"display:none;\"><div id=\"ltIngredInner\">\n");
        sb.Append("<a href=\"#\" class=\"video_close\"></a>\n");
        sb.Append(innerText);
		sb.Append("</div></div>\n");
        
        return sb.ToString();
    }

    protected string renderCautions(string innerText)
    {
        /// <summary>
        /// Method to render Cautions Watch Image and Lightbox.
        /// </summary>
        StringBuilder sb = new StringBuilder();
        sb.Append("\n<div id=\"ltCautions\" style=\"display:none;\"><div id=\"ltCautionsInner\">\n");
        sb.Append("<a href=\"#\" class=\"video_close\"></a>\n");
        sb.Append(innerText);
		sb.Append("</div></div>\n");
        
        return sb.ToString();
    }		
	
	protected void renderSeeDetail(DataSet ds)
    {
        /// <summary>
        /// Method to see detail link in product detail.
        /// </summary>
        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["SeeDetailActive"].ToString()))
        {
            StringBuilder sd = new StringBuilder();
            string divID = "lbSeeDetail";
			sd.Append("<a style=\"left:");
            sd.Append(ds.Tables[0].Rows[0]["SeeDetailX"]);
            sd.Append("px; top:");
            sd.Append(ds.Tables[0].Rows[0]["SeeDetailY"]);
            sd.Append("px;\" href=\"");
            sd.Append("#TB_inline&height=438&width=579&inlineId=");
			sd.Append(divID);
			sd.Append("\" class=\"thickbox seeDetail\"");			
            sd.Append("\">");
            sd.Append("see <span>detail</span></a>");
			sd.Append(renderImage(ds.Tables[0].Rows[0]["SeeDetailText"].ToString(), divID));
			ltSD.Text = sd.ToString();
        }
    }
	
    protected void renderBadge(DataSet ds)
    {
        /// <summary>
        /// Method to render badge in product detail.
        /// </summary>
        if (!DBNull.Value.Equals(ds.Tables[0].Rows[0]["BadgeID"]))
        {
            StringBuilder bd = new StringBuilder();
            bd.Append("<span style=\"left:");
            bd.Append(ds.Tables[0].Rows[0]["BadgeX"]);
            bd.Append("px; top:");
            bd.Append(ds.Tables[0].Rows[0]["BadgeY"]);
            bd.Append("px;\" class=\"b");
            bd.Append(ds.Tables[0].Rows[0]["BadgeID"]);
            bd.Append("\" title=\"");
            bd.Append(ds.Tables[0].Rows[0]["Badge_Title"]);
			bd.Append("\">");
            bd.Append("</span>");
            ltBadge.Text = bd.ToString();
        }
    }
	
	protected string renderSpotPromo(DataSet ds){
        /// <summary>
        /// Method to render spot promo.
        /// </summary>	
		string sPromo = "";
        if (!DBNull.Value.Equals(ds.Tables[0].Rows[0]["SpotPromo"]))
        {
            sPromo = ds.Tables[0].Rows[0]["SpotPromo"].ToString();
        }	
		
		return sPromo;
	}
	#endregion
}
