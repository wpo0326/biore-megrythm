<%@ OutputCache Duration="600" VaryByParam="pid" %>
<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true" CodeFile="product_detail.aspx.cs" Inherits="products_product_detail" Title="Untitled Page" Debug="true" %>
<%@ MasterType VirtualPath="~/MasterPages/Main.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
	<asp:Literal ID="ltMeta" runat="server"/>
    <link type="text/css" href="/usa/optin/css/thickbox.css" rel="stylesheet" />
    <!--<link type="text/css" href="/usa/css/products.css" rel="stylesheet" />
    <link type="text/css" href="/usa/css/home_promo.css" rel="stylesheet" media="screen,projection" />-->
    <link type="text/css" href="/usa/instantlyClearerPores/css/IC_lightbox-prod.css" rel="stylesheet" media="screen,projection" />

    <script type="text/javascript" src="/usa/js/jquery-ui-1.7.2.custom.min.js"></script>
    <script type="text/javascript" src="/usa/js/swfaddress.js"></script>                 
    <script type="text/javascript" src="/usa/js/swfobject2.2.js"></script>
    <script type="text/javascript" src="/usa/js/products.js"></script>
    <script type="text/javascript" src="/usa/optin/js/thickbox.js"></script>
    <script type="text/javascript" src="/usa/js/videoLB.js"></script>
    <script type="text/javascript">
          function closeLink () {
            $(".video_close").click(function(e){
			        e.preventDefault();
			        tb_remove();
		        });
		        $(".video_close").hover(
			        function () {$(this).addClass('video_close_over');},
			        function () {$(this).removeClass('video_close_over');}
		        );
          }
          $(document).ready(function(){
            closeLink ();
            // Setup click events based on link IDs to load video
            $("#btnVid").click(function (e){
              e.preventDefault();
              <asp:Literal ID="btnPlayVideo" runat="server" />
              closeLink ();
            });
            $("#promoVid").click(function (e){
              e.preventDefault();
              <asp:Literal ID="promoPlayVideo" runat="server" />
              closeLink ();
            });
            /*checkBodyHeight();
		        checkWindowHeight();
		        if (navigator.userAgent.indexOf('Safari') == -1) {
			        $("#tabs li a").click(checkBodyHeight);
		        }
		        window.onresize = checkWindowHeight;*/
            if (location.href.indexOf("#CompleteBioreClean") != -1) 
                tb_show("","/usa/instantlyClearerPores/IC_intro.aspx?height=605&width=967", "");
            if (location.href.indexOf("&showvideo=1") != -1 && $("#btnVid").length > 0)
              $("#btnVid").trigger('click');
	        }
        );
    </script>
	<!--[if lt IE 7]>
	<script>
	    DD_belatedPNG.fix('#ctl00_ContentPlaceHolder1_prod_main_info, #prod_detail_inner, .prod_main_info a, #prod_add_info ul, #prod_image_sm img, #btnVid, #buy_now a, #prod_promo, #prod_promo a span, a.lkPop span, div#ltIngredInner, div#ltCautionsInner, #ctl00_ContentPlaceHolder1_prod_main_info span, a.thickbox, p#cottonPromo');
	</script>
	<![endif]-->
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="prod_detail">
        <div id="prod_detail_inner">

            <asp:Literal ID="ltCirc" runat="server" />
            <div id="buy_now"><asp:HyperLink ID="hlBuy" runat="server" Text="Buy Now" /></div>

            <div id="prod_main_info" runat="server">

                <h1><asp:Image ID="prod_image" runat="server" /></h1>
                <asp:Literal ID="ltBadge" runat="server"/>
                <asp:Literal ID="ltSD" runat="server"/>
                <h3><asp:Literal ID="subhead" runat="server" /></h3>

                <div id="product_detail_tabs" class="ui-tabs">
                    <ul id="tab_nav">
                        <li id="what_it_is_tab"><a href="#what_it_is">what it is</a></li>
                        <li id="the_difference_tab" runat="server"><asp:HyperLink ID="hlDiff" NavigateUrl="#the_difference" runat="server">the difference</asp:HyperLink></li>
                        <li id="how_to_use_tab"><a href="#how_to_use">how to use</a></li>
                    </ul>

                    <div id="what_it_is"><asp:Literal ID="ltWhatItIs" runat="server" /></div>
                    <div id="the_difference" class="ui-tabs-hide"><asp:Literal ID="ltDifference" runat="server" /></div>
                    <div id="how_to_use" class="ui-tabs-hide"><asp:Literal ID="ltHowToUse" runat="server" /></div>
                </div>

            </div>
			<asp:Literal ID="ltIngred" runat="server"/>
			<asp:Literal ID="ltCautions" runat="server"/>
            <div id="prod_add_info">
                <h4>when to use:</h4>
                <ul id="when_to_use">
                    <li id="calendar"><asp:Literal ID="when_freq" runat="server" /></li>
                    <li id="clock"><asp:Literal ID="when_duration" runat="server" /></li>
                </ul>

                <h4>works great with:</h4>
                <ul id="works_great_with">
                    <li id="prod_image_sm"><asp:Image ID="prod_image_sm" runat="server" /></li>
                    <asp:Repeater ID="rp1" runat="server">
                        <ItemTemplate>
                            <li id="wgw_<%# Eval("WorksWithProductID")%>"><a href="product_detail.aspx?pid=<%# Eval("WorksWithProductID")%>"><span class="wgwOuter"><span class="wgwInner"><%# ConvertCase(Eval("NameText").ToString())%></span></span></a></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>

        </div>
    </div>
    <asp:Literal ID="ltPromo" runat="server" />
</asp:Content>

