using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public class bioreDAO
{
    public DataSet getProductInfoDataSet(int prodId)
    {
        String connectionString = ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString;
        DataSet ds = new DataSet();
        SqlConnection myConn = new SqlConnection(connectionString);
        SqlCommand myCommand = new SqlCommand("sp_GetProductInfo", myConn);
        myCommand.CommandType = CommandType.StoredProcedure;

        SqlParameter myParm1 = myCommand.Parameters.Add("@pID", SqlDbType.Int);
        myParm1.Value = prodId;

        try
        {
            myConn.Open();
            SqlDataAdapter myDataAdapter = new SqlDataAdapter();
            myDataAdapter.SelectCommand = myCommand;
            myDataAdapter.Fill(ds);
        }
        catch (Exception ex101)
        {
            ds = null;
            LogEntry myLog = new LogEntry();
            myLog.EventId = 101;
            myLog.Message = "getProductInfoDataSet failed in bioreDAO.cs.\r\n";
            myLog.Message += ex101.ToString();
            myLog.Priority = 1;
            Logger.Write(myLog);
            myLog = null;
        }
        finally
        {
            if (myConn != null)
            {
                myConn.Close();
                myConn = null;
            }
        }

        return ds;
    }

    public DataSet getProductMatrixDataSet()
    {
        String connectionString = ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString;
        DataSet ds = new DataSet();
        SqlConnection myConn = new SqlConnection(connectionString);
        SqlCommand myCommand = new SqlCommand("sp_GenerateProductMatrix", myConn);
        myCommand.CommandType = CommandType.StoredProcedure;

        try
        {
            myConn.Open();
            SqlDataAdapter myDataAdapter = new SqlDataAdapter();
            myDataAdapter.SelectCommand = myCommand;
            myDataAdapter.Fill(ds);
        }
        catch
        {
            ds = null;
        }
        finally
        {
            if (myConn != null)
            {
                myConn.Close();
                myConn = null;
            }
        }

        return ds;
    }

    public int getEventRemainingCount(string eventCode)
    {
        int remainingCount = -1;
        SqlConnection myConn = new SqlConnection();
        myConn = new SqlConnection(ConfigurationManager.ConnectionStrings["consumerInfo"].ConnectionString);
        myConn.Open();
        SqlCommand myCom = new SqlCommand("spEMGetRemainingEntriesCount", myConn);
        myCom.CommandType = CommandType.StoredProcedure;
        SqlParameter myP1 = myCom.Parameters.Add("@iError", SqlDbType.Int);
        myP1.Direction = ParameterDirection.Output;

        SqlParameter myP2 = myCom.Parameters.Add("@eventCode", SqlDbType.VarChar, 50);
        myP2.Value = eventCode;

        try
        {
            myCom.ExecuteNonQuery();
            remainingCount = (int)myCom.Parameters["@iError"].Value;
        }
        catch (Exception ex101)
        {
            LogEntry myLog = new LogEntry();
            myLog.Priority = 1;
            myLog.EventId = 101;
            myLog.Message = "Error getting entryCount for " + eventCode;
            myLog.Message += "\r\n" + ex101.ToString();
            Logger.Write(myLog);
            myLog = null;
        }
        finally
        {
            if (myCom != null)
            {
                myCom.Dispose();
                myCom = null;
            }
            if (myConn != null)
            {
                myConn.Close();
                myConn = null;
            }
        }
        return remainingCount;
    }

    public int getMemberAvailability(string eventCode, string email)
    {
        int memberCount = -1;
        SqlConnection myConn = new SqlConnection();
        myConn = new SqlConnection(ConfigurationManager.ConnectionStrings["consumerInfo"].ConnectionString);
        myConn.Open();
        SqlCommand myCom = new SqlCommand("spEMcheckMemberCount", myConn);
        myCom.CommandType = CommandType.StoredProcedure;
        SqlParameter myP1 = myCom.Parameters.Add("@iError", SqlDbType.Int);
        myP1.Direction = ParameterDirection.Output;

        SqlParameter myP2 = myCom.Parameters.Add("@eventCode", SqlDbType.VarChar, 50);
        myP2.Value = eventCode;

        SqlParameter myP3 = myCom.Parameters.Add("@email", SqlDbType.VarChar, 150);
        myP3.Value = email;

        try
        {
            myCom.ExecuteNonQuery();
            memberCount = (int)myCom.Parameters["@iError"].Value;
        }
        catch (Exception ex101)
        {
            LogEntry myLog = new LogEntry();
            myLog.Priority = 1;
            myLog.EventId = 101;
            myLog.Message = "Error getting entryCount for " + eventCode;
            myLog.Message += "\r\nEmail " + email;
            myLog.Message += "\r\n" + ex101.ToString();
            Logger.Write(myLog);
            myLog = null;
        }
        finally
        {
            if (myCom != null)
            {
                myCom.Dispose();
                myCom = null;
            }
            if (myConn != null)
            {
                myConn.Close();
                myConn = null;
            }
        }
        return memberCount;
    }

    public int checkEventAccessByEmail(int eventID, string email)
    {
        int valid = -1;
        using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["consumerInfo"].ConnectionString))
        {
            SqlCommand cmd = new SqlCommand("spCheckRestrictedEventEmail", cn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@Email", SqlDbType.VarChar, 150);
            cmd.Parameters["@Email"].Value = email;
            cmd.Parameters.Add("@EventID", SqlDbType.Int);
            cmd.Parameters["@EventID"].Value = eventID;

            cmd.Parameters.Add("@Valid", SqlDbType.Int);
            cmd.Parameters["@Valid"].Direction = ParameterDirection.Output;

            cn.Open();


            try
            {
                cmd.ExecuteNonQuery();
                valid = (int)cmd.Parameters["@Valid"].Value;
            }
            catch (Exception ex101)
            {
                LogEntry myLog = new LogEntry();
                myLog.Priority = 1;
                myLog.EventId = 101;
                myLog.Message = "Error checking spCheckRestrictedEventEmail";
                myLog.Message += "\r\nEmail " + email;
                myLog.Message += "\r\nEvent " + eventID;
                myLog.Message += "\r\n" + ex101.ToString();
                Logger.Write(myLog);
                myLog = null;
                throw;
            }

            return valid;

        }
    }
}
