using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using KAO_ASPNET_Utils;
/// <summary>
/// Summary description for physique
/// </summary>
public class physique
{
    public int getPhysiqueSeats(int marketID, int classID)
    {
        int returnValue = -6;
        String connectionString = ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString;
        SqlConnection myConn = new SqlConnection(connectionString);
        myConn.Open();
        SqlCommand myCommand = new SqlCommand("getPhysiqueSeats", myConn);
        myCommand.CommandType = CommandType.StoredProcedure;

        SqlParameter myParm1 = myCommand.Parameters.Add("@Available", SqlDbType.Int);
        myParm1.Direction = ParameterDirection.Output;
        SqlParameter myParm2 = myCommand.Parameters.Add("@MarketID", SqlDbType.Int);
        myParm2.Value = marketID;
        SqlParameter myParm3 = myCommand.Parameters.Add("@ClassID", SqlDbType.Int);
        myParm3.Value = classID;

        try
        {
            myCommand.ExecuteNonQuery();
            returnValue = (int)myCommand.Parameters["@Available"].Value;
        }
        catch (Exception pex100)
        {
            StringBuilder myString = new StringBuilder(500);
            myString.Append("Error getting available seats for Physique\r\n");
            myString.Append("Market ID: " + marketID.ToString() + "\r\n");
            myString.Append("Class ID: " + classID.ToString() + "\r\n");
            myString.Append("Error:\r\n");
            myString.Append(pex100.ToString());

            LogEntry myLog = new LogEntry();
            myLog.EventId = 100;
            myLog.Priority = 1;
            myLog.Message = myString.ToString();
            Logger.Write(myLog);
            myLog = null;
            myString = null;
        }
        finally
        {
            if (myCommand != null)
            {
                myCommand.Dispose();
                myCommand = null;
            }
            if (myConn != null)
            {
                myConn.Close();
                myConn = null;
            }
        }
        return returnValue;
    }

    public int cancelClass(int pID)
    {
        int returnValue = -1;
        String connectionString = ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString;
        SqlConnection myConn = new SqlConnection(connectionString);
        myConn.Open();
        SqlCommand myCommand = new SqlCommand("removePhysique", myConn);
        myCommand.CommandType = CommandType.StoredProcedure;

        SqlParameter myParm1 = myCommand.Parameters.Add("@Success", SqlDbType.Int);
        myParm1.Direction = ParameterDirection.Output;
        SqlParameter myParm2 = myCommand.Parameters.Add("@ID", SqlDbType.Int);
        myParm2.Value = pID;

        try
        {
            myCommand.ExecuteNonQuery();
            returnValue = (int)myCommand.Parameters["@Success"].Value;
        }
        catch (Exception pex101)
        {
            StringBuilder myString = new StringBuilder(500);
            myString.Append("Error cancelling class for Physique\r\n");
            myString.Append("ID: " + pID.ToString() + "\r\n");
            myString.Append("Error:\r\n");
            myString.Append(pex101.ToString());

            LogEntry myLog = new LogEntry();
            myLog.EventId = 101;
            myLog.Priority = 1;
            myLog.Message = myString.ToString();
            Logger.Write(myLog);
            myLog = null;
            myString = null;
        }
        finally
        {
            if (myCommand != null)
            {
                myCommand.Dispose();
                myCommand = null;
            }
            if (myConn != null)
            {
                myConn.Close();
                myConn = null;
            }
        }
        return returnValue;
    }

    public int scheduleClass(int marketID, int classID, string email, string fname, string lname, int classLimit)
    {
        int returnValue = -1;
        string ETKey = "";
        ETKey = "BiorePhysique" + marketID.ToString();
        string classTime = "";

        string[] classInfo = new string[8];
        classInfo = getClassInfo(marketID, classID);
        classTime = classInfo[7];

        String connectionString = ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString;
        SqlConnection myConn = new SqlConnection(connectionString);
        myConn.Open();
        SqlCommand myCommand = new SqlCommand("addPhysique", myConn);
        myCommand.CommandType = CommandType.StoredProcedure;

        SqlParameter myParm1 = myCommand.Parameters.Add("@ID", SqlDbType.Int);
        myParm1.Direction = ParameterDirection.Output;
        SqlParameter myParm2 = myCommand.Parameters.Add("@MarketID", SqlDbType.Int);
        myParm2.Value = marketID;
        SqlParameter myParm3 = myCommand.Parameters.Add("@ClassID", SqlDbType.Int);
        myParm3.Value = classID;
        SqlParameter myParm4 = myCommand.Parameters.Add("@Email", SqlDbType.VarChar, 150);
        myParm4.Value = email;
        SqlParameter myParm5 = myCommand.Parameters.Add("@FirstName", SqlDbType.VarChar, 50);
        myParm5.Value = fname;
        SqlParameter myParm6 = myCommand.Parameters.Add("@LastName", SqlDbType.VarChar, 50);
        myParm6.Value = lname;
        SqlParameter myParm7 = myCommand.Parameters.Add("@ClassLimit", SqlDbType.Int);
        myParm7.Value = classLimit;

        try
        {
            myCommand.ExecuteNonQuery();
            returnValue = (int)myCommand.Parameters["@ID"].Value;
        }
        catch (Exception pex102)
        {
            StringBuilder myString = new StringBuilder(500);
            myString.Append("Error scheduling class for Physique\r\n");
            myString.Append("Market: " + marketID.ToString() + "\r\n");
            myString.Append("Class: " + classID.ToString() + "\r\n");
            myString.Append("Email: " + email + "\r\n");
            myString.Append("First Name: " + fname + "\r\n");
            myString.Append("Last Name: " + lname + "\r\n");
            myString.Append("Class Limit: " + classLimit.ToString() + "\r\n");
            myString.Append("Error:\r\n");
            myString.Append(pex102.ToString());

            LogEntry myLog = new LogEntry();
            myLog.EventId = 102;
            myLog.Priority = 1;
            myLog.Message = myString.ToString();
            Logger.Write(myLog);
            myLog = null;
            myString = null;
        }
        finally
        {
            if (myCommand != null)
            {
                myCommand.Dispose();
                myCommand = null;
            }
            if (myConn != null)
            {
                myConn.Close();
                myConn = null;
            }
        }

        if (returnValue != -1)
        {
            int validEmail = -1;
            etEmail et = new etEmail();
            validEmail = et.sendETEmail(email, ETKey, returnValue, classTime, fname);

            if (validEmail == -1)
            {
                StringBuilder myString = new StringBuilder(500);
                myString.Append("Error sending confirmation email\r\n");
                myString.Append("Market: " + marketID.ToString() + "\r\n");
                myString.Append("Class: " + classID.ToString() + "\r\n");
                myString.Append("Email: " + email + "\r\n");
                myString.Append("First Name: " + fname + "\r\n");
                myString.Append("Last Name: " + lname + "\r\n");
                myString.Append("Class Limit: " + classLimit.ToString() + "\r\n");

                LogEntry myLog = new LogEntry();
                myLog.EventId = 103;
                myLog.Priority = 1;
                myLog.Message = myString.ToString();
                Logger.Write(myLog);
                myLog = null;
                myString = null;
            }

        }
        return returnValue;
    }

    public string[] getClassInfo(int marketID, int classID)
    {
        string location="";
        string venue ="";
        string address = "";
        string csz = "";
        string map = "";
        string cDay = "";
        string cDate = "";
        string cTime = "";
        string cLimit = "0";

        switch (classID)
        {
            case 1:
                cTime = "4:30-5:30 P.M.";
                break;
            case 2:
                cTime = "6:00-7:00 P.M.";
                break;
            case 3:
                cTime = "7:30-8:30 P.M.";
                break;
        }

        switch (marketID)
        {
            case 1:
                location = "New York";
                venue = "Physique 57 Studio";
                address = "161 Avenue of the Americas";
                csz = "New York, NY 10013";
                map = "http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=161+Avenue+of+the+Americas,+New+York,+NY&sll=37.0625,-95.677068&sspn=51.310143,114.169922&ie=UTF8&hq=&hnear=161+Avenue+of+the+Americas,+New+York,+10013&z=16";
                cDay = "Monday";
                cDate = "September 20th";
                cLimit = "23";
                break;
            case 2:
                location = "Miami Beach";
                venue = "Green Monkey";
                address = "1827 Purdy Avenue";
                csz = "Miami Beach, FL 33139";
                map = "http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=1827+Purdy+Avenue,+Miami+Beach,+FL+33139&sll=25.794115,-80.144186&sspn=0.008288,0.021093&ie=UTF8&hq=&hnear=1827+Purdy+Ave,+Miami+Beach,+Miami-Dade,+Florida+33139&z=16";
                cDay = "Thursday";
                cDate = "September 23rd";
                cLimit = "44";
                break;
            case 3:
                location = "Seattle";
                venue = "Be Luminious";
                address = "900 Lenora Street, Suite 128";
                csz = "Seattle, WA 98121";
                map = "http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Be+Luminous+Yoga&sll=25.795401,-80.14224&sspn=0.007187,0.013937&ie=UTF8&hq=&hnear=900+Lenora+St,+Seattle,+King,+Washington+98121&z=16&iwloc=A&cid=16987616515760609229";
                cDay = "Tuesday";
                cDate = "September 28th";
                cLimit = "33";
                break;
            case 4:
                location = "Portland";
                venue = "Urban Pilates";
                address = "1737 NE Alberta St., Suite 102";
                csz = "Portland, OR 97211";
                map = "http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Urban+Pilates+%26+The+Fitness+Suite&sll=37.0625,-95.677068&sspn=32.197599,86.396484&ie=UTF8&hq=&hnear=1737+NE+Alberta+St+%23102,+Portland,+Multnomah,+Oregon+97211&ll=45.560639,-122.647183&spn=0.006941,0.021093&z=16&iwloc=A&cid=647641865423200222";
                cDay = "Thursday";
                cDate = "September 30th";
                cLimit = "33";
                break;
            case 5:
                location = "Chicago";
                venue = "Dance Spa";
                address = "1890 N Milwaukee Ave";
                csz = "Chicago, IL 60647";
                map = "http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Dance+SPA&sll=47.617573,-122.336968&sspn=0.010761,0.027874&ie=UTF8&hq=&hnear=1890+N+Milwaukee+Ave,+Chicago,+Cook,+Illinois+60647&z=16&iwloc=A&cid=3046950914138049873";
                cDay = "Tuesday";
                cDate = "October 5th";
                cLimit = "33";
                break;
            case 6:
                location = "Austin";
                venue = "Dharma Yoga";
                address = "3110 Guadalupe";
                csz = "Austin, TX 78705";
                map = "http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Dharma+Yoga&sll=40.725849,-74.00413&sspn=0.012099,0.027874&ie=UTF8&hq=&hnear=3110+Guadalupe+St,+Austin,+Travis,+Texas+78705&z=17&iwloc=A&cid=1318685777377418177";
                cDay = "Thursday";
                cDate = "October 7th";
                cLimit = "48";
                break;
            case 7:
                location = "Philadelphia";
                venue = "Sankhya Yoga";
                address = "725 North 4th Street, 2nd Floor";
                csz = "Philidelphia, PA 19123";
                map = "http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Sankhya+Yoga&sll=42.286882,-83.746655&sspn=0.01181,0.027874&g=725+North+4th+Street,+2nd+Floor&ie=UTF8&hq=&hnear=725+N+4th+St,+Philadelphia,+Pennsylvania+19123&z=16&iwloc=A&cid=13204765049133941285";
                cDay = "Wednesday";
                cDate = "October 13th";
                cLimit = "39";
                break;
            case 8:
                location = "Washington, DC";
                venue = "Stroga Yoga";
                address = "1808 Adams Mill Rd., NW";
                csz = "Washington, DC 20009";
                map = "http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Stroga&sll=39.962969,-75.144556&sspn=0.012236,0.027874&ie=UTF8&hq=&hnear=1808+Adams+Mill+Rd+NW,+Washington+D.C.,+District+of+Columbia,+20009&ll=38.922708,-77.043107&spn=0.01242,0.027874&z=16&iwloc=A&cid=17803948825074658917";
                cDay = "Thursday";
                cDate = "October 14th";
                cLimit = "44";
                break;
            case 9:
                location = "San Francisco";
                venue = "The Pad Studios";
                address = "1690 Union St.";
                csz = "San Francisco, CA 94123";
                map = "http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=1690+Union+St.,+San+Francisco,+CA+94123&sll=38.922708,-77.043107&sspn=0.01242,0.027874&ie=UTF8&hq=&hnear=1690+Union+St,+San+Francisco,+California+94123&z=17";
                cDay = "Tuesday";
                cDate = "October 19th";
                cLimit = "28";
                break;
            case 10:
                location = "San Diego";
                venue = "Yoga One";
                address = "1150 7th Avenue";
                csz = "San Diego, CA 92101";
                map = "http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=1150+7th+Avenue,+San+Diego,+CA+92101&sll=37.76596,-122.464294&sspn=0.00631,0.013937&ie=UTF8&hq=&hnear=1150+7th+Ave,+San+Diego,+California+92101&z=17";
                cDay = "Thursday";
                cDate = "October 21st";
                cLimit = "44";
                break;
        }
        string[] classArray  = {location, venue, address, csz, map, cDay, cDate, cTime, cLimit};

        return classArray;
    }

    public bool isMarketFull(int marketID)
    {
        bool marketFull = false;
        int seatsAvailable = 0;
        string[] classInfo = new string[8];

        for (int i = 1; i < 4; i++)
        {
            classInfo = getClassInfo(marketID, i);
            seatsAvailable += Int32.Parse(classInfo[8]) - getPhysiqueSeats(marketID, i);
        }

        if (seatsAvailable > 0) { marketFull = false; }
        else { marketFull = true; }

        return marketFull;
    }
}
