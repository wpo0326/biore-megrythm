﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// Summary description for optin
/// </summary>
public class optin
{
    public optin()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    string[] genderVals = ConfigurationManager.AppSettings["Gender"].Split(',');
    string[] pudVals = ConfigurationManager.AppSettings["ProductsUsedDaily"].Split(',');
    string[] pmaVals = ConfigurationManager.AppSettings["PayMoreAttention"].Split(',');
    string[] wtpmVals = ConfigurationManager.AppSettings["WillingToPayMore"].Split(',');
    string[] ltrVals = ConfigurationManager.AppSettings["LikelyToRecommend"].Split(',');
    string[] puVals = ConfigurationManager.AppSettings["ProductsUsing"].Split(',');
    string[] mubVals = ConfigurationManager.AppSettings["MostUsedBrand"].Split(',');
    string[] paVals = ConfigurationManager.AppSettings["ProductAttributes"].Split(',');
    string[] fcpVals = ConfigurationManager.AppSettings["FaceCareProblems"].Split(',');
    string[] raceVals = ConfigurationManager.AppSettings["Race"].Split(',');
    string[] spanVals = ConfigurationManager.AppSettings["Spanish"].Split(',');
    string[] tpuVals = ConfigurationManager.AppSettings["TypesProductsUsed"].Split(',');
    string[] stVals = ConfigurationManager.AppSettings["SkinType"].Split(',');
    string[] StateVals = ConfigurationManager.AppSettings["State"].Split(',');
    public string getFieldValue(string fieldName, int fieldValue)
    {
        string _fieldValue = "";
        switch (fieldName)
        {
            case "gender":
                if (fieldValue == 0)
                    _fieldValue = "";
                else
                    _fieldValue = genderVals[fieldValue - 1];
                break;
            case "ProdUsedDaily":
                _fieldValue = pudVals[fieldValue - 1];
                break;
            case "PayMoreAttention":
                _fieldValue = pmaVals[fieldValue - 1];
                break;
            case "WillingToPayMore":
                _fieldValue = wtpmVals[fieldValue - 1];
                break;
            case "LikelyToRecommend":
                _fieldValue = ltrVals[fieldValue - 1];
                break;
            case "ProductsUsing":
                _fieldValue = puVals[fieldValue - 1];
                break;
            case "MostUsedBrand":
                _fieldValue = mubVals[fieldValue - 1];
                break;
            case "TypesProductsUsed":
                _fieldValue = tpuVals[fieldValue - 1];
                break;
            case "ProductAttributes":
                _fieldValue = paVals[fieldValue - 1];
                break;
            case "FaceCareProblems":
                _fieldValue = fcpVals[fieldValue - 1];
                break;
            case "SkinType":
                _fieldValue = stVals[fieldValue - 1];
                break;
            case "Race":
                if (fieldValue == 0)
                    _fieldValue = "";
                else
                    _fieldValue = raceVals[fieldValue - 1];
                break;
            case "Spanish":
                if (fieldValue == 0)
                    _fieldValue = "";
                else
                    _fieldValue = spanVals[fieldValue - 1];
                break;
            case "State":
                if (fieldValue == 0)
                    _fieldValue = "";
                else
                    _fieldValue = StateVals[fieldValue - 1];
                break;
        }
        return _fieldValue;
    }
}
