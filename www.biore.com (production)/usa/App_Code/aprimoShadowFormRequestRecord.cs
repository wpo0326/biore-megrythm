using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;

/// <summary>
/// Summary description for aprimoShadowFormRequestRecord
/// </summary>
public class aprimoShadowFormRequestRecord
{
    // NOTE: FOLLOWING VALUE IS SPECIFIC TO THIS BRAND
    public static string BrandValue = ConfigurationManager.AppSettings["Brand_String"];

    public aprimoShadowFormRequestRecord()
    {
    }

    public static void addAprimoShadowFormRequestRecord(string Email_To, string Email_Subj, string Email_Body)
    {
        try
        {
            // MAKE COPY IN DB OF APRIMO SHADOW FORM REQUEST
            using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["consumerInfo"].ToString()))
            {
                using (SqlCommand cmd = new SqlCommand("spAddAprimoShadowFormRequest", cn))
                {
                    // NOTE: the "using" command will take care of disposing the SqlCommand
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Create the DataSet
                    DataSet ds = new DataSet();

                    cmd.Parameters.Add("@Brand", SqlDbType.VarChar, 100);
                    cmd.Parameters["@Brand"].Value = BrandValue;
                    cmd.Parameters.Add("@Email_To", SqlDbType.VarChar, 320);
                    cmd.Parameters["@Email_To"].Value = Email_To;
                    cmd.Parameters.Add("@Email_Subj", SqlDbType.VarChar, 500);
                    cmd.Parameters["@Email_Subj"].Value = Email_Subj;
                    cmd.Parameters.Add("@Email_Body", SqlDbType.Text);
                    cmd.Parameters["@Email_Body"].Value = Email_Body;

                    // NOTE: the "using" command will take care of closing the SqlConnection
                    cn.Open();

                    // NOTE: the "using" command will take care of disposing the SqlCommand
                    cmd.ExecuteNonQuery();
                }
            }
        }
        catch (Exception ex101)
        {
            LogEntry myLog = new LogEntry();
            myLog.Priority = 1;
            myLog.Message = "aprimoShadowFormRequestRecord ERROR - addAprimoShadowFormRequestRecord\r\n\r\n";
            myLog.Message += "-------------------------------------\r\n";
            myLog.Message += ex101.Message.ToString() + "\r\n\r\n";
            myLog.Message += "-------------------------------------\r\n";
            myLog.Message += "Email_To: " + Email_To + " \r\n";
            myLog.Message += "Email_Subj: " + Email_Subj + " \r\n";
            myLog.Message += "Email_Body: " + Email_Body + " \r\n";
            myLog.EventId = -101;
            Logger.Write(myLog);
            myLog = null;

        }

    }
}
