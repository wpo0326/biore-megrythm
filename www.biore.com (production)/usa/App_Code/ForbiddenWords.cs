﻿using System;
using System.Collections;
using System.IO;
using System.Web;

/// <summary>
/// Summary description for ForbiddenWords
/// </summary>
public class ForbiddenWords
{
    public static void Initialize()
    {
        StringReader forbiddenWordsReader = null;

        try
        {
            forbiddenWordsReader = new StringReader(Resources.Resources.ForbiddenWords);
        }
        catch
        {
            HttpContext.Current.Cache["ForbiddenWords"] = new ArrayList();
            return;
        }

        ArrayList forbiddenWordsList = new ArrayList();

        while (forbiddenWordsReader.Peek() != -1)
        {
            forbiddenWordsList.Add(forbiddenWordsReader.ReadLine());
        }

        HttpContext.Current.Cache["ForbiddenWords"] = forbiddenWordsList;

        StringReader inStringWordsReader = null;

        try
        {
            inStringWordsReader = new StringReader(Resources.Resources.InStringWords);
        }
        catch
        {
            HttpContext.Current.Cache["InStringWords"] = new ArrayList();
            return;
        }

        ArrayList inStringWordsList = new ArrayList();

        while (inStringWordsReader.Peek() != -1)
        {
            inStringWordsList.Add(inStringWordsReader.ReadLine());
        }

        HttpContext.Current.Cache["InStringWords"] = inStringWordsList;

        //Utilities.LogMessageToFile("INIT: Forbidden Words List Initialized.");
    }

    public static bool IsWordForbidden(string wordSrc, bool wholeWordOnly)
    {
        /*
        string legalCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_áéíóúñüÁÉÈÍÓÖÚÑÜß°€£àâäåèêëîïôöœùûüůůÿçÀÂÄÅÉÊËÎÏÔŒÙÛÜŮŮŸÇ";

        // First check to see if the word contains illegal characters
        foreach (char wordChar in wordSrc.ToCharArray())
        {
            if (!legalCharacters.Contains(wordChar.ToString()))
            {
                return true;
            }
        }
        */
        // Now see if its a bad word
        ArrayList forbiddenWordsList = HttpContext.Current.Cache["ForbiddenWords"] as ArrayList;

        if (forbiddenWordsList == null)
        {
            //Utilities.LogMessageToFile("ERR: Forbidden Words List referenced, but is not present in cache.");
            throw new InvalidOperationException("Must call ForbiddenWords.Initialize() first. 1");

        }
        //Utilities.LogMessageToFile("RESP: Referenced successfully. Length: " + forbiddenWordsList.Count.ToString());
        ArrayList inStringWordsList = HttpContext.Current.Cache["InStringWords"] as ArrayList;

        if (inStringWordsList == null)
        {
            throw new InvalidOperationException("Must call ForbiddenWords.Initialize() first. 2");
            //ForbiddenWords.Initialize();
        }

        wordSrc = wordSrc.ToLower();

        string[] words = wordSrc.Split(' ');
        foreach (string word in words)
        {
            if (MatchWord(word, forbiddenWordsList, wholeWordOnly))
            {
                return true;
            }
            if (ContainsWord(word, inStringWordsList))
            {
                return true;
            }
        }
        return false;
    }

    private static bool MatchWord(string word, ArrayList forbiddenWordsList, bool wholeWordOnly)
    {
        if (wholeWordOnly)
        {
            foreach (string forbiddenWord in forbiddenWordsList)
            {
                string forbiddenWordLower = forbiddenWord.ToLower();
                if (word == forbiddenWordLower)
                {
                    return true;
                }
            }
        }
        else
        {
            foreach (string forbiddenWord in forbiddenWordsList)
            {
                string forbiddenWordLower = forbiddenWord.ToLower();
                if (word.Contains(forbiddenWordLower))
                {
                    return true;
                }
            }
        }

        return false;
    }
    private static bool ContainsWord(string word, ArrayList inStringWordsList)
    {
        foreach (string forbiddenWord in inStringWordsList)
        {
            string forbiddenWordLower = forbiddenWord.ToLower();
            if (word.Contains(forbiddenWordLower))
            {
                return true;
            }
        }
        return false;
    }
}
