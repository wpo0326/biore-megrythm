﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for FBUtil
/// </summary>
public class FBUtil
{
	public FBUtil()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static string decodeSignedReq()
    {
        string signedReq = "";

        if (!string.IsNullOrEmpty(HttpContext.Current.Request["signed_request"]))
        {
            string encodedSignedReq = HttpContext.Current.Request["signed_request"];

            string[] splitEncReq = encodedSignedReq.Split('.');
            string encodedSig = splitEncReq[0];
            string payload = splitEncReq[1];

            // Needs to pad the payload to the proper length for a Base64Url encoded string if is isn't correct already
            int missingChars = 4 - (payload.Length % 4);
            if (missingChars < 4)
            {
                for (int i = 0; i < missingChars; i++)
                {
                    payload += "=";
                }
            }

            //Response.Write(payload);
            // Replace encoded characters - and _ with correct characters + and /
            payload.Replace("-", "+");
            payload.Replace("_", "/");

            // Decode the Payload (to UTF8)
            string json = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(payload));

            signedReq = json;
        }
        return signedReq;
    }

    public static bool parseJsonForLike(string decodedJson)
    {
        bool result = false;

        if (!string.IsNullOrEmpty(decodedJson))
        {
            if (decodedJson.IndexOf("liked\":") != -1)
            {
                return decodedJson.IndexOf("liked\":true") != -1;
            }
        }

        return result;
    }
}
