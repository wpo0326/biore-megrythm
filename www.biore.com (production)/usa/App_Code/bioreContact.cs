using KAO_ASPNET_Utils;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Web;
using System.Collections;
using System.Configuration;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;

/// <summary>
/// Summary description for bioreContact
/// </summary>
[WebService(Namespace = "http://www.biore.com/usa/services/contact")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1, EmitConformanceClaims = true)]
public class bioreContact : System.Web.Services.WebService
{

    public bioreContact()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    string SMTP_Server = ConfigurationManager.AppSettings["SMTP_Server"];
    string site_CountryID = ConfigurationManager.AppSettings["site_CountryID"];
    int HookId = int.Parse(ConfigurationManager.AppSettings["HookId"]);

    // CI variables for all sites
    int SiteID_Ban = int.Parse(ConfigurationManager.AppSettings["SiteID_Ban"]);
    int EventId_signup_Ban = int.Parse(ConfigurationManager.AppSettings["EventId_signup_Ban"]);
    string EventCode_signup_Ban = ConfigurationManager.AppSettings["EventCode_signup_Ban"];
    int SiteID_Biore = int.Parse(ConfigurationManager.AppSettings["SiteID_Biore"]);
    int EventId_signup_Biore = int.Parse(ConfigurationManager.AppSettings["EventId_signup_Biore"]);
    string EventCode_signup_Biore = ConfigurationManager.AppSettings["EventCode_signup_Biore"];
    int SiteID_Curel = int.Parse(ConfigurationManager.AppSettings["SiteID_Curel"]);
    int EventId_signup_Curel = int.Parse(ConfigurationManager.AppSettings["EventId_signup_Curel"]);
    string EventCode_signup_Curel = ConfigurationManager.AppSettings["EventCode_signup_Curel"];
    int SiteID_Jergens = int.Parse(ConfigurationManager.AppSettings["SiteID_Jergens"]);
    int EventId_signup_Jergens = int.Parse(ConfigurationManager.AppSettings["EventId_signup_Jergens"]);
    string EventCode_signup_Jergens = ConfigurationManager.AppSettings["EventCode_signup_Jergens"];
    int SiteID_JF = int.Parse(ConfigurationManager.AppSettings["SiteID_JF"]);
    int EventId_signup_JF = int.Parse(ConfigurationManager.AppSettings["EventId_signup_JF"]);
    string EventCode_signup_JF = ConfigurationManager.AppSettings["EventCode_signup_JF"];

    // CI Biore Traits
    int ProdUsedTrait = int.Parse(ConfigurationManager.AppSettings["traitID_ProdUsedDaily"]);
    int RecommendTrait = int.Parse(ConfigurationManager.AppSettings["traitID_LikelyRecommend"]);
    int WillingPayMoreTrait = int.Parse(ConfigurationManager.AppSettings["traitID_WillingToPayMore"]);
    int PayMoreAttentionTrait = int.Parse(ConfigurationManager.AppSettings["traitID_PayMoreAttention"]);
    int ProductsUsingTrait = int.Parse(ConfigurationManager.AppSettings["traitID_ProductsUsing"]);
    int ProductsUsedTrait = int.Parse(ConfigurationManager.AppSettings["traitID_ProductsUsed"]);
    int MostUsedBrandTrait = int.Parse(ConfigurationManager.AppSettings["traitID_MostUsedBrand"]);
    int ProductAttributesTrait = int.Parse(ConfigurationManager.AppSettings["traitID_ProductAttributes"]);
    int FaceCareProblemsTrait = int.Parse(ConfigurationManager.AppSettings["traitID_FaceCareProblems"]);
    int RaceTrait = int.Parse(ConfigurationManager.AppSettings["traitID_Race"]);
    int SpanishTrait = int.Parse(ConfigurationManager.AppSettings["traitID_Spanish"]);
    int TypesProductsUsedTrait = int.Parse(ConfigurationManager.AppSettings["traitID_TypesProductsUsed"]);
    int SkinTypeTrait = int.Parse(ConfigurationManager.AppSettings["traitID_SkinType"]);



    string MatchZIPPattern = @"^\d{5}$";
    private DateTime dateValue = new DateTime();

    public static bool IsEmail(string Email)
    {
        string strRegex = @"^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}" +
            @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
            @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
        Regex re = new Regex(strRegex);
        if (re.IsMatch(Email))
            return (true);
        else
            return (false);
    }

    [WebMethod]
    public int bioreOptin(string fname, string lname, string email, string postalCode, int gender, string DOB, int ProdUsedDaily, int PayMoreAttention, int WillingToPayMore, int RecommendFriends, string ProductsUsing, int MostUsedBrand, string TypesProductsUsed, string ProductAttributes, string FaceCareProblems, int SkinType, int Race, int Spanish, int banoptin, int cureloptin, int jergensoptin, int jfoptin)
    {
        return bioreOptinFullAdress(0, fname, lname, email, "", "", "", "", 0, postalCode, gender, DOB, ProdUsedDaily, PayMoreAttention, WillingToPayMore, RecommendFriends, ProductsUsing, MostUsedBrand, TypesProductsUsed, ProductAttributes, FaceCareProblems, SkinType, Race, Spanish, banoptin, cureloptin, jergensoptin, jfoptin, 1, EventId_signup_Biore, EventCode_signup_Biore, "Biore Opt-In");
    }

    [WebMethod]
    public int bioreStringCoupon(int memberID, string fname, string lname, string email, string PIN, string medium, int coupon, string source, int eventID, string eventCode, string openEnded)
    {
        int statusFlag = -1;
        bool invalidEntry = false;
        // check for valid entry
        if (email.Length > 150 || email.Length == 0)
            invalidEntry = true;
        if (PIN.Length > 0)
        {
            if (PIN.Length > 255)
                invalidEntry = true;
        }
        if (email == null || !IsEmail(email))
            invalidEntry = true;
        if (medium.Length == 0)
            invalidEntry = true;
        if (eventCode.Length == 0)
            invalidEntry = true;
        if (eventID == 0)
            invalidEntry = true;
        if (openEnded.Length == 0)
            invalidEntry = true;

        if (!invalidEntry) // form has valid entries - process
        {
            eventDAO myEvent = new eventDAO();
            spStatus myStatus = new spStatus();

            myEvent.init();

            myStatus = myEvent.setEntryForm(eventCode, 0, 0, eventID, SiteID_Biore, memberID, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], email, "", "", "", "", "", "", "", HttpContext.Current.Request.ServerVariables["URL"], "entry: " + eventCode, openEnded, "", fname, "", lname, "", "", "", "", "", "", site_CountryID, "", "empty", "", "", "", "", "");

            statusFlag = myStatus.spFlag;

            myEvent.destroy();
        }
        else
        {
            StringBuilder myString = new StringBuilder(500);
            myString.Append("bioreStringCoupon failed in bioreContact.cs.\r\n\r\n");
            myString.Append("Member ID: " + memberID.ToString() + " \r\n");
            myString.Append("PIN: " + PIN + " \r\n");
            myString.Append("Email: " + email + " \r\n");
            myString.Append("First Name: " + fname + " \r\n");
            myString.Append("Last Name: " + lname + " \r\n");
            myString.Append("EventID: " + eventID.ToString() + " \r\n");
            myString.Append("Event Code: " + eventCode + " \r\n");
            myString.Append("Open Ended: " + openEnded + " \r\n");
            myString.Append("Coupon: " + coupon.ToString() + " \r\n");
            myString.Append("Medium: " + medium + " \r\n");
            myString.Append("Source: " + source + " \r\n");

            LogEntry myLog = new LogEntry();
            myLog.Priority = 1;
            myLog.Message = myString.ToString();
            myLog.EventId = -1403;
            Logger.Write(myLog);
            myLog = null;
        }
        return statusFlag;
    }

    [WebMethod]
    public int bioreOptout(string email)
    {
        int statusFlag = -1;
        bool invalidEntry = false;
        if (email.Length > 150 || email.Length == 0)
            invalidEntry = true;
        if (email == null || !IsEmail(email))
            invalidEntry = true;
        if (!invalidEntry)
        {
            spStatus optout = new spStatus();
            eventDAO myEvent = new eventDAO();

            myEvent.init();

            optout = myEvent.setOptout(SiteID_Biore, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], email, HttpContext.Current.Request.ServerVariables["URL"]);
            statusFlag = optout.spFlag;

            myEvent.destroy();

        }
        else
        {
            LogEntry myLog = new LogEntry();
            myLog.Priority = 1;
            myLog.Message = "Biore Opt-Out failed.\r\n\r\n";
            myLog.Message += "Email: " + email + " \r\n";
            myLog.EventId = -1403;
            Logger.Write(myLog);
            myLog = null;
        }
        return statusFlag;
    }

    [WebMethod]
    public int bioreOptinFullAdress(int memberID, string fname, string lname, string email, string addr1, string addr2, string addr3, string city, int state, string postalCode, int gender, string DOB, int ProdUsedDaily, int PayMoreAttention, int WillingToPayMore, int RecommendFriends, string ProductsUsing, int MostUsedBrand, string TypesProductsUsed, string ProductAttributes, string FaceCareProblems, int SkinType, int Race, int Spanish, int banoptin, int cureloptin, int jergensoptin, int jfoptin, int bioreoptin, int eventID, string eventCode, string openEnded)
    {
        return bioreOptinNewTrait(memberID, fname, lname, email, addr1, addr2, addr3, city, state, postalCode, gender, DOB, ProdUsedDaily, PayMoreAttention, WillingToPayMore, RecommendFriends, ProductsUsing, "", MostUsedBrand, TypesProductsUsed, ProductAttributes, FaceCareProblems, SkinType, Race, Spanish, banoptin, cureloptin, jergensoptin, jfoptin, bioreoptin, eventID, eventCode, openEnded);
    }

    [WebMethod]
    public int bioreOptinNewTrait(int memberID, string fname, string lname, string email, string addr1, string addr2, string addr3, string city, int state, string postalCode, int gender, string DOB, int ProdUsedDaily, int PayMoreAttention, int WillingToPayMore, int RecommendFriends, string ProductsUsing, string ProductsUsed, int MostUsedBrand, string TypesProductsUsed, string ProductAttributes, string FaceCareProblems, int SkinType, int Race, int Spanish, int banoptin, int cureloptin, int jergensoptin, int jfoptin, int bioreoptin, int eventID, string eventCode, string openEnded)
    {
        int statusFlag = -1;
        bool invalidEntry = false;
        // check for valid entry
        if (fname.Length > 50 || fname.Length == 0 || fname == null || fname.IndexOf(">") > 0 || fname.IndexOf("<") > 0 || lname.Length > 50 || lname.Length == 0 || lname == null || lname.IndexOf(">") > 0 || lname.IndexOf("<") > 0)
            invalidEntry = true;
        if (memberID == 0)
        {
            if (email.Length > 150 || email.Length == 0)
                invalidEntry = true;
            if (email == null || !IsEmail(email))
                invalidEntry = true;
            if (!Regex.IsMatch(postalCode, MatchZIPPattern))
                invalidEntry = true;
        }
        if (DOB.Length > 0)
        {
            try
            {
                dateValue = DateTime.Parse(DOB);
            }
            catch
            {
                invalidEntry = true;
            }
        }
        if (gender < 0 || gender > 2)
            invalidEntry = true;
        if (ProdUsedDaily < 0 || ProdUsedDaily > 5)
            invalidEntry = true;
        if (PayMoreAttention < 0 || PayMoreAttention > 5)
            invalidEntry = true;
        if (WillingToPayMore < 0 || WillingToPayMore > 5)
            invalidEntry = true;
        if (RecommendFriends < 0 || RecommendFriends > 4)
            invalidEntry = true;
        if (MostUsedBrand < 0 || MostUsedBrand > 20)
            invalidEntry = true;
        if (SkinType < 0 || SkinType > 6)
            invalidEntry = true;
        if (Race < 0 || Race > 5)
            invalidEntry = true;
        if (Spanish < 0 || Spanish > 2)
            invalidEntry = true;


        if (!invalidEntry) // form has valid entries - process
        {
            optin myOptin = new optin();
            eventDAO myEvent = new eventDAO();
            spStatus myStatus = new spStatus();
            etEmail welcomeEmail = new etEmail();

            myEvent.init();

            int validEmail = -1;
            int bioreEmail = -1;
            int banEmail = -1;
            int curelEmail = -1;
            int jergensEmail = -1;
            int jfEmail = -1;

            string contactContent = "";
            string contactVia = "";
            string contactFreq = "";
            if (bioreoptin == 1)
            {
                myStatus = myEvent.checkMemberInterests(SiteID_Biore, email);
                bioreEmail = myStatus.spFlag;

                if (bioreEmail == 0)
                    validEmail = welcomeEmail.sendETEmail("", "", email, "", "", "BioreWelcome");

                contactContent = "samples,contests,sweepstakes,other info,";
                contactVia = "email,direct mail,";
                contactFreq = "as needed,";
            }
            myStatus = myEvent.setEntryForm(eventCode, 0, 0, eventID, SiteID_Biore, memberID, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], email, "", "", "", contactContent, contactVia, contactFreq, "", HttpContext.Current.Request.ServerVariables["URL"], "entry: " + eventCode, openEnded, "", fname, "", lname, addr1, addr2, addr3, city, myOptin.getFieldValue("State", state), postalCode, site_CountryID, myOptin.getFieldValue("gender", gender), DOB, "", "", "", "", "");

            memberID = myStatus.MemId;
            statusFlag = myStatus.spFlag;

            if (memberID > 0)
            {
                myStatus = myEvent.setMktgTrait(eventID, memberID, 0, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], HttpContext.Current.Request.ServerVariables["URL"], ProdUsedTrait, myOptin.getFieldValue("ProdUsedDaily", ProdUsedDaily));
                myStatus = myEvent.setMktgTrait(eventID, memberID, 0, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], HttpContext.Current.Request.ServerVariables["URL"], RecommendTrait, myOptin.getFieldValue("LikelyToRecommend", RecommendFriends));
                myStatus = myEvent.setMktgTrait(eventID, memberID, 0, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], HttpContext.Current.Request.ServerVariables["URL"], WillingPayMoreTrait, myOptin.getFieldValue("WillingToPayMore", WillingToPayMore));
                myStatus = myEvent.setMktgTrait(eventID, memberID, 0, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], HttpContext.Current.Request.ServerVariables["URL"], PayMoreAttentionTrait, myOptin.getFieldValue("PayMoreAttention", PayMoreAttention));

                if (ProductsUsing.Length > 0)
                {
                    string[] puVals = ProductsUsing.Split(',');
                    string puString = "";
                    for (int i = 0; i < puVals.Length; i++)
                    {
                        puString += myOptin.getFieldValue("ProductsUsing", Int32.Parse(puVals[i])) + ',';
                    }

                    myStatus = myEvent.setMktgTrait(eventID, memberID, 0, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], HttpContext.Current.Request.ServerVariables["URL"], ProductsUsingTrait, puString);
                }
                if (ProductsUsed.Length > 0)
                {
                    string[] pusedVals = ProductsUsed.Split(',');
                    string pusedString = "";
                    for (int i = 0; i < pusedVals.Length; i++)
                    {
                        pusedString += myOptin.getFieldValue("ProductsUsing", Int32.Parse(pusedVals[i])) + ',';
                    }

                    myStatus = myEvent.setMktgTrait(eventID, memberID, 0, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], HttpContext.Current.Request.ServerVariables["URL"], ProductsUsedTrait, pusedString);
                }
                myStatus = myEvent.setMktgTrait(eventID, memberID, 0, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], HttpContext.Current.Request.ServerVariables["URL"], MostUsedBrandTrait, myOptin.getFieldValue("MostUsedBrand", MostUsedBrand));

                string[] tpuVals = TypesProductsUsed.Split(',');
                string tpuString = "";
                for (int i = 0; i < tpuVals.Length; i++)
                {
                    tpuString += myOptin.getFieldValue("TypesProductsUsed", Int32.Parse(tpuVals[i])) + ',';
                }

                myStatus = myEvent.setMktgTrait(eventID, memberID, 0, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], HttpContext.Current.Request.ServerVariables["URL"], TypesProductsUsedTrait, tpuString);

                string[] paVals = ProductAttributes.Split(',');
                string paString = "";
                for (int i = 0; i < paVals.Length; i++)
                {
                    paString += myOptin.getFieldValue("ProductAttributes", Int32.Parse(paVals[i])) + ',';
                }

                myStatus = myEvent.setMktgTrait(eventID, memberID, 0, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], HttpContext.Current.Request.ServerVariables["URL"], ProductAttributesTrait, paString);

                string[] fcpVals = FaceCareProblems.Split(',');
                string fcpString = "";
                for (int i = 0; i < fcpVals.Length; i++)
                {
                    fcpString += myOptin.getFieldValue("FaceCareProblems", Int32.Parse(fcpVals[i])) + ',';
                }

                myStatus = myEvent.setMktgTrait(eventID, memberID, 0, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], HttpContext.Current.Request.ServerVariables["URL"], FaceCareProblemsTrait, fcpString);
                myStatus = myEvent.setMktgTrait(eventID, memberID, 0, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], HttpContext.Current.Request.ServerVariables["URL"], SkinTypeTrait, myOptin.getFieldValue("SkinType", SkinType));
                if (Race != 0)
                {
                    myStatus = myEvent.setMktgTrait(eventID, memberID, 0, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], HttpContext.Current.Request.ServerVariables["URL"], RaceTrait, myOptin.getFieldValue("Race", Race));
                    if (Spanish != 0)
                    {
                        myStatus = myEvent.setMktgTrait(eventID, memberID, 0, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], HttpContext.Current.Request.ServerVariables["URL"], SpanishTrait, myOptin.getFieldValue("Spanish", Spanish));
                    }
                }

                if (cureloptin == 1)
                {
                    myStatus = myEvent.checkMemberInterests(SiteID_Curel, email);
                    curelEmail = myStatus.spFlag;

                    myStatus = myEvent.setEntryForm(EventCode_signup_Curel, 0, HookId, EventId_signup_Curel, SiteID_Curel, memberID, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], email, "", "", "", "samples,contests,sweepstakes,other info,", "email,direct mail,", "as needed,", "", HttpContext.Current.Request.ServerVariables["URL"], "entry: " + EventCode_signup_Curel, "Curel Opt-In", "", fname, "", lname, addr1, addr2, addr3, city, myOptin.getFieldValue("State", state), postalCode, site_CountryID, myOptin.getFieldValue("gender", gender), DOB, "", "", "", "", "");

                    if (curelEmail == 0)
                        validEmail = welcomeEmail.sendETEmail("", "", email, "", "", "CurelWelcome");
                }
                if (bioreoptin == 1 && eventID != EventId_signup_Biore)
                {
                    myStatus = myEvent.setEntryForm(EventCode_signup_Biore, 0, 0, EventId_signup_Biore, SiteID_Biore, memberID, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], email, "", "", "", "samples,contests,sweepstakes,other info,", "email,direct mail,", "as needed,", "", HttpContext.Current.Request.ServerVariables["URL"], "entry: " + EventCode_signup_Biore, "Biore Opt-In", "", fname, "", lname, addr1, addr2, addr3, city, myOptin.getFieldValue("State", state), postalCode, site_CountryID, myOptin.getFieldValue("gender", gender), DOB, "", "", "", "", "");
                }
                if (banoptin == 1)
                {
                    myStatus = myEvent.checkMemberInterests(SiteID_Ban, email);
                    banEmail = myStatus.spFlag;

                    myStatus = myEvent.setEntryForm(EventCode_signup_Ban, 0, HookId, EventId_signup_Ban, SiteID_Ban, memberID, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], email, "", "", "", "samples,contests,sweepstakes,other info,", "email,direct mail,", "as needed,", "", HttpContext.Current.Request.ServerVariables["URL"], "entry: " + EventCode_signup_Ban, "Ban Opt-In", "", fname, "", lname, addr1, addr2, addr3, city, myOptin.getFieldValue("State", state), postalCode, site_CountryID, myOptin.getFieldValue("gender", gender), DOB, "", "", "", "", "");

                    if (banEmail == 0)
                        validEmail = welcomeEmail.sendETEmail("", "", email, "", "", "BanWelcome");
                }
                if (jergensoptin == 1)
                {
                    myStatus = myEvent.checkMemberInterests(SiteID_Jergens, email);
                    jergensEmail = myStatus.spFlag;

                    myStatus = myEvent.setEntryForm(EventCode_signup_Jergens, 0, HookId, EventId_signup_Jergens, SiteID_Jergens, memberID, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], email, "", "", "", "samples,contests,sweepstakes,other info,", "email,direct mail,", "as needed,", "", HttpContext.Current.Request.ServerVariables["URL"], "entry: " + EventCode_signup_Jergens, "Jergens Opt-In", "", fname, "", lname, addr1, addr2, addr3, city, myOptin.getFieldValue("State", state), postalCode, site_CountryID, myOptin.getFieldValue("gender", gender), DOB, "", "", "", "", "");

                    if (jergensEmail == 0)
                        validEmail = welcomeEmail.sendETEmail("", "", email, "", "", "JergensWelcome");
                }
                if (jfoptin == 1)
                {
                    myStatus = myEvent.checkMemberInterests(SiteID_JF, email);
                    jfEmail = myStatus.spFlag;

                    myStatus = myEvent.setEntryForm(EventCode_signup_JF, 0, HookId, EventId_signup_JF, SiteID_JF, memberID, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"], email, "", "", "", "samples,contests,sweepstakes,other info,", "email,direct mail,", "as needed,", "", HttpContext.Current.Request.ServerVariables["URL"], "entry: " + EventCode_signup_JF, "John Frieda Opt-In", "", fname, "", lname, addr1, addr2, addr3, city, myOptin.getFieldValue("State", state), postalCode, site_CountryID, myOptin.getFieldValue("gender", gender), DOB, "", "", "", "", "");

                    if (jfEmail == 0)
                        validEmail = welcomeEmail.sendETEmail("", "", email, "", "", "JFWelcome");
                }
            }
            myEvent.destroy();
        }
        else
        {
            StringBuilder myString = new StringBuilder(1000);
            myString.Append("Invalid entry in bioreContact.cs for bioreOptinFullAddress.\r\n\r\n");
            myString.Append("First Name: " + fname + " \r\n");
            myString.Append("Last Name: " + lname + " \r\n");
            myString.Append("Email: " + email + " \r\n");
            myString.Append("Addr1: " + addr1 + " \r\n");
            myString.Append("Addr2: " + addr2 + " \r\n");
            myString.Append("Addr3: " + addr3 + " \r\n");
            myString.Append("City: " + city + " \r\n");
            myString.Append("State: " + state.ToString() + " \r\n");
            myString.Append("ZIP: " + postalCode + " \r\n");
            myString.Append("gender: " + gender.ToString() + " \r\n");
            myString.Append("DOB: " + DOB + " \r\n");
            myString.Append("ProdUsedDaily: " + ProdUsedDaily.ToString() + " \r\n");
            myString.Append("PayMoreAttention: " + PayMoreAttention.ToString() + " \r\n");
            myString.Append("WillingToPayMore: " + WillingToPayMore.ToString() + " \r\n");
            myString.Append("RecommendFriends: " + RecommendFriends.ToString() + " \r\n");
            myString.Append("ProductsUsing: " + ProductsUsing + " \r\n");
            myString.Append("MostUsedBrand: " + MostUsedBrand.ToString() + " \r\n");
            myString.Append("TypesProductsUsed: " + TypesProductsUsed + " \r\n");
            myString.Append("ProductAttributes: " + ProductAttributes + " \r\n");
            myString.Append("FaceCareProblems: " + FaceCareProblems + " \r\n");
            myString.Append("SkinType: " + SkinType.ToString() + " \r\n");
            myString.Append("Race: " + Race.ToString() + " \r\n");
            myString.Append("Spanish: " + Spanish.ToString() + " \r\n");
            myString.Append("banoptin: " + banoptin.ToString() + " \r\n");
            myString.Append("cureloptin: " + cureloptin.ToString() + " \r\n");
            myString.Append("jergensoptin: " + jergensoptin.ToString() + " \r\n");
            myString.Append("jfoptin: " + jfoptin.ToString() + " \r\n");
            myString.Append("bioreoptin: " + bioreoptin.ToString() + " \r\n");

            LogEntry myLog = new LogEntry();
            myLog.Priority = 1;
            myLog.Message = myString.ToString();
            myLog.EventId = -1421;
            Logger.Write(myLog);
            myLog = null;
        }
        return statusFlag;
    }

    [WebMethod]
    public int bioreFacebookContest(string fname, string lname, string email, string addr1, string city, int state, string postalCode, int gender, string DOB, int ProdUsedDaily, int PayMoreAttention, int WillingToPayMore, int RecommendFriends, string ProductsUsing, int MostUsedBrand, string TypesProductsUsed, string ProductAttributes, string FaceCareProblems, int SkinType, int Race, int Spanish, int banoptin, int cureloptin, int jergensoptin, int jfoptin, int bioreoptin, int eventID, string eventCode, string openEnded)
    {
        int validEntry = -1;
        validEntry = bioreOptinFullAdress(0, fname, lname, email, addr1, "", "", city, state, postalCode, gender, DOB, ProdUsedDaily, PayMoreAttention, WillingToPayMore, RecommendFriends, ProductsUsing, MostUsedBrand, TypesProductsUsed, ProductAttributes, FaceCareProblems, SkinType, Race, Spanish, banoptin, cureloptin, jergensoptin, jfoptin, bioreoptin, eventID, eventCode, openEnded);
        if (validEntry == 0)
        {
            return 0;
        }
        else
        {
            StringBuilder myString = new StringBuilder(1000);
            myString.Append("bioreFacebookContest failed in bioreContact.cs\r\n\r\n");
            myString.Append("First Name: " + fname + " \r\n");
            myString.Append("Last Name: " + lname + " \r\n");
            myString.Append("Email: " + email + " \r\n");
            myString.Append("Addr1: " + addr1 + " \r\n");
            myString.Append("City: " + city + " \r\n");
            myString.Append("State: " + state.ToString() + " \r\n");
            myString.Append("ZIP: " + postalCode + " \r\n");
            myString.Append("gender: " + gender.ToString() + " \r\n");
            myString.Append("DOB: " + DOB + " \r\n");
            myString.Append("ProdUsedDaily: " + ProdUsedDaily.ToString() + " \r\n");
            myString.Append("PayMoreAttention: " + PayMoreAttention.ToString() + " \r\n");
            myString.Append("WillingToPayMore: " + WillingToPayMore.ToString() + " \r\n");
            myString.Append("RecommendFriends: " + RecommendFriends.ToString() + " \r\n");
            myString.Append("ProductsUsing: " + ProductsUsing + " \r\n");
            myString.Append("MostUsedBrand: " + MostUsedBrand.ToString() + " \r\n");
            myString.Append("TypesProductsUsed: " + TypesProductsUsed + " \r\n");
            myString.Append("ProductAttributes: " + ProductAttributes + " \r\n");
            myString.Append("FaceCareProblems: " + FaceCareProblems + " \r\n");
            myString.Append("SkinType: " + SkinType.ToString() + " \r\n");
            myString.Append("Race: " + Race.ToString() + " \r\n");
            myString.Append("Spanish: " + Spanish.ToString() + " \r\n");
            myString.Append("banoptin: " + banoptin.ToString() + " \r\n");
            myString.Append("cureloptin: " + cureloptin.ToString() + " \r\n");
            myString.Append("jergensoptin: " + jergensoptin.ToString() + " \r\n");
            myString.Append("jfoptin: " + jfoptin.ToString() + " \r\n");
            myString.Append("bioreoptin: " + bioreoptin.ToString() + " \r\n");

            LogEntry myLog = new LogEntry();
            myLog.Priority = 1;
            myLog.Message = myString.ToString();
            myLog.EventId = -1426;
            Logger.Write(myLog);
            myLog = null;
            return -1;
        }

    }

    [WebMethod]
    public void bioreErrorLogTest()
    {
        LogEntry myLog = new LogEntry();
        myLog.Priority = 1;
        myLog.Message = "Biore Test Error Logging";
        myLog.EventId = -1;
        Logger.Write(myLog);
        myLog = null;
    }
}
