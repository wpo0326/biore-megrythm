﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KAOForms;

public partial class FormConfig_Report_saywhat : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
          EventCountData ecData = new EventCountData();
          ConsumerInfoDAO ciDAO = new ConsumerInfoDAO();

         //event ids
          int FBSample = 400147;
          int FBCoupon = 400146;

          //set Samp count
          ecData = ciDAO.getEventCount(FBSample);
          litSampEventCount.Text = ecData.CurrentCount.ToString();

          //set Coup count
          ecData = ciDAO.getEventCount(FBCoupon);
          litCoupEventCount.Text = ecData.CurrentCount.ToString();
        
    }
}
