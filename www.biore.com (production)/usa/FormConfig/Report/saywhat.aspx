﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="saywhat.aspx.cs" Inherits="FormConfig_Report_saywhat" %>
<%@ OutputCache Duration="180" VaryByParam="None"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
          <table border="1">
            <tr> 
                <th>ID</th>
                <th>Current Count</th>
            </tr>
            <tr>
                <td>Samp</td>
                <td>
                    <asp:Literal ID="litSampEventCount" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td>Coup</td>
                <td>
                    <asp:Literal ID="litCoupEventCount" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
