<%@ Language=VBScript %>
<%
redirectUrl = "/usa/products/index.aspx"
' FOLLOWING initialQueryStringAppendChar SHOULD BE "?" IF redirectUrl
' HAS NOT QUERY STRING PARAMS.  IT SHOULD BE "&" IF IT DOES HAVE QUERY STRING PARAMS.
initialQueryStringAppendChar = "&"

' FIND AND ADD ANY QUERY STRING PARAMETERS IF NECESSARY '

dim reconstructedQueryString
dim key
reconstructedQueryString = ""
for each key in Request.Querystring
	Response.Write Server.URLEncode(key) & ": " & Server.URLEncode(Request.Querystring(key)) & "<BR>"
	If Not reconstructedQueryString = "" Then
		reconstructedQueryString = reconstructedQueryString & "&"
	End If
	reconstructedQueryString = reconstructedQueryString & Server.URLEncode(key) & "=" & Server.URLEncode(Request.Querystring(key))
next

If Not reconstructedQueryString = "" Then
	redirectUrl = redirectUrl & initialQueryStringAppendChar & reconstructedQueryString
End If


' FINALLY DO 301 REDIRECT '

Response.Status = "301 Moved Permanently"
Response.addheader "Location", redirectUrl
Response.End

%>