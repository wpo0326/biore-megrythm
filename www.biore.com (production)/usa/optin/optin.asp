<div id="optin">
    <div id="inner">
	    <ul id="steps">
		    <li id="oneLink" class="on"><a href="#">Step One</a></li>
		    <li id="twoLink"><a href="#">Step Two</a></li>
		    <li id="threeLink"><a href="#">Step Three</a></li>
		    <li id="thankyouLink"><a href="#">Thank You</a></li>
	    </ul>
	    <form action="" method="post" name="theForm" id="theForm">
		    <div id="one" class="stepDiv">
		    <h2>Thanks for your interest in the Bior&eacute;<sup>&reg;</sup> brand!</h2>
		    <p class="intro">Please enter the information below to hear about all our latest promotions, contests, and news.</p>
		    <p class="error" id="errorMsg">Error message</p>
		    <p class="req" id="reqMsg">required<sup>*</sup></p>			
			    <div>
				    <label id="fnameLbl" for="firstName">First Name</label>
				    <input name="fname" id="firstName" type="text" maxlength="50" class="textBx oneField" tabindex="1"/>
			    </div>
			    <div>
				    <label id="lnameLbl" for="lastName">Last Name</label>
				    <input name="lname" id="lastName" type="text" maxlength="50" class="textBx oneField" tabindex="2" />
			    </div>
			    <div>
				    <label id="emailLbl" for="email"><sup class="req">*</sup>Email</label>
				    <input name="email" id="email" type="text" maxlength="100" class="textBx oneField" tabindex="3" />
                </div>
			    <div>
				    <label id="countryLbl" for="CountryID"><sup class="req">*</sup>Country</label>
	                <select name="CountryID" id="CountryID" tabindex="4" class="oneField">
	                    <option selected="selected" value="">Select country...</option>
	                    <option value="USA ">United States</option>
	                    <option value="CAN">Canada</option>
	                </select>
			    </div>
			    <div>
				    <label id="genderLbl" for="gender" class="oneField">Gender</label>
	                <select name="gender" id="gender" tabindex="5">
	                    <option selected="selected" value="">Select gender...</option>
					    <option value="F">Female</option>
					    <option value="M">Male</option>
	                </select>
			    </div>
			    <div>
				    <label id="bdayLbl" for="bdayY" class="oneField"><sup class="req">*</sup>Birthdate</label>
	                <select name="bdayY" id="bdayY" class="bdaySelect oneField" tabindex="6">
	                    <option value=''>Year</option>
	                    <%  counter=Year(now)-13
	                        While counter > Year(Now)-100 %>
                            <option value="<%=counter %>"><%=counter %></option>
                        <%      counter = counter - 1
                            Wend %>
	                </select>
				    <select name="bdayM" class="bdaySelect oneField" tabindex="7">
	                    <option value=''>Mon</option>
	                    <% for counter=1 to 12 %>
	                        <option value="<%=counter %>"><%=Left(MonthName(counter),3) %></option>
                        <% next %>	                        
	                </select>
				    <select name="bdayD" class="bdaySelect" tabindex="8">
	                    <option selected="selected" value="">Day</option>
	                    <% for counter=1 to 31 %>
	                        <option value="<%=counter%>"><%=counter %></option>
	                    <% next %>
	                </select>
			    </div>			 
		    </div>
		    <div id="two" class="stepDiv">
		        <p class="error" id="errorMsg2">Error message</p>
		        <p class="req" id="reqMsg2">required<sup>*</sup></p>	
		        <div>
				    <label for="mktg1"><sup class="req">*</sup> How many skin care products do you use on a daily basis?</label>
	                <select name="mktg1" id="mktg1">
	                    <option value="" selected="selected">Please select...</option>
	                    <option value="1">1</option>
	                    <option value="2">2</option>
	                    <option value="3">3</option>
	                    <option value="4">4</option>
	                    <option value="5 or more">5 or more</option>
	                </select>
			    </div>
			    <div>
				    <label for="mktg2"><sup class="req">*</sup> I pay more attention/spend more time on my skin than other people.</label>
	                <select name="mktg2" id="mktg2">
	                    <option value="" selected="selected">Please select...</option>
	                    <option value="Agree strongly">Agree strongly</option>
	                    <option value="Agree somewhat">Agree somewhat</option>
	                    <option value="Neither agree nor disagree">Neither agree nor disagree</option>
	                    <option value="Disagree somewhat">Disagree somewhat</option>
	                    <option value="Disagree strongly">Disagree strongly</option>
	                </select>
			    </div>
		        <div>
				    <label for="mktg3"><sup class="req">*</sup> I am willing to pay more for skin care products I really want.</label>
	                <select name="mktg3" id="mktg3">
	                    <option value="" selected="selected">Please select...</option>
	                    <option value="Agree strongly">Agree strongly</option>
	                    <option value="Agree somewhat">Agree somewhat</option>
	                    <option value="Neither agree nor disagree">Neither agree nor disagree</option>
	                    <option value="Disagree somewhat">Disagree somewhat</option>
	                    <option value="Disagree strongly">Disagree strongly</option>
	                </select>
			    </div>  
			    <div>
				    <label for="mktg4"><sup class="req">*</sup> How likely are you to recommend your favorite skin care products to friends?</label>
	                <select name="mktg4" id="mktg4">	                
	                    <option value="" selected="selected">Please select...</option>
	                   <option value="Very likely">Very likely</option>
	                    <option value="Somewhat likely">Somewhat likely</option>
	                    <option value="Somewhat unlikely">Somewhat unlikely</option>
	                    <option value="Not at all likely">Not at all likely</option>	            
	                </select>
			    </div>  
		    </div>
		    <div id="three" class="stepDiv">
		        <p class="error" id="errorMsg3">Error message</p>
		        <div class="yes">
		            <input type="checkbox" checked="checked"  name="optin" id="optinChkBx" /><label id="mainYes">Yes, tell me about future Bior&eacute;<sup>&reg;</sup> brand product news and offerings.</label>
		        </div>
		        <div class="yes">
		            <label>Yes, I'd like to receive emails and newsletters from other great products from Kao Brands Company:</label>    		    
		        </div>
		        <div>
		            <input type="checkbox" checked="checked" name="banoptin" id="banoptin" /><label><a href="http://www.feelbanfresh.com/default.asp" target="_blank">Ban<sup>&reg;</sup> antiperspirant and deodorant</a> products keep you 3x cooler and fresher, even under pressure.<sup>&#134;</sup><br /> 
<span class="font-size:smaller;"><sup>&#134;</sup> vs. ordinary invisible solids</span>
</label>
		        </div>
		        <div>            
                    <input type="checkbox"  checked="checked" name="cureloptin" id="cureloptin" /><label><a href="http://www.curel.com/index.asp" target="_blank">Cur&eacute;l<sup>&reg;</sup> Skincare's</a> full line of hand and body moisturizers delivers freedom from itchy, dry skin.</label>
                </div>
		        <div>    
                    <input type="checkbox" checked="checked" name="jergensoptin" id="jergensoptin" /><label><a href="http://www.jergens.com/default.asp" target="_blank">Jergens<sup>&reg;</sup> Skincare</a> collection of moisturizers delivers a natural glow, smooth and firm skin and an allure that captivates.</label>
                </div>
		        <div>     
                    <input type="checkbox"  checked="checked" name="jfoptin" id="jfoptin" /><label><a href="http://www.johnfrieda.com/index.asp" target="_blank">John Frieda<sup>&reg;</sup></a> provides solutions for any hair care problem, resulting in hair that looks fabulous.</label>
                </div>
                <p class="privLink">Before submitting your information, please view our <a href="/usa/privacyPolicy.asp" target="_blank">privacy policy</a></p>		         
		    </div>
		    <div id="thankyou" class="stepDiv">
		        <h2 title="Thanks!">thanks!</h2>
		        <p>Thanks for signing up to hear about great events, contests, and product news offered by the Bior&eacute;<sup>&reg;</sup> Skincare team!</p>
		    </div>
    		
		    <div id="buttons">
			    <p id="prevBtn"><a href="#" id="prevBtnA">Prev</a></p>
			    <p id="doneBtn"><a href="#" id="doneBtnA" class="lbAction" rel="deactivate">Done</a></p>
			    <p id="signUpBtn"><a href="#" id="signUpBtnA">Sign Up</a></p>
			    <p id="nextBtn"><a href="#" id="nextBtnA" tabindex="9">Next</a></p>
		    </div>		
	    </form>
	</div>
	<div id="left"></div>
	<div id="right"></div>
	<div id="topOptin"></div>
	<div id="bottomOptin"></div>
</div>
<script type="text/javascript">setTimeout(optin.init, "50");</script>
