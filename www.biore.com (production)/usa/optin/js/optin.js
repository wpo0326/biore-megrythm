var optin = {
    theDivs : null,
    theStepsLis : null,
	previousButton : null,
	nextButton : null,
	doneButton : null,
	signUpButton : null,
	theCount : null,
	visited : [true, false, false],
	valid : false,
	valid2 : false,
	valid3: false,
	errorMsgText : null,
	errorMsgHolder : null,
	reqMsgHolder : null,
	errorMsgHolder2 : null,
	reqMsgHolder2 : null,
	//threeIntro : null,
	errorMsgHolder3 : null,
	browserBtn : false,
	
	init: function() {
	    
	    theForm = document.getElementById("theForm");
	    optin.previousButton = document.getElementById("prevBtn");
	    optin.nextButton = document.getElementById("nextBtn");
	    optin.doneButton = document.getElementById("doneBtn");
	    optin.signUpButton = document.getElementById("signUpBtn");
	    optin.signUpButtonAnchor = document.getElementById("signUpBtnA");
	    optin.errorMsgHolder = document.getElementById("errorMsg");
	    optin.reqMsgHolder = document.getElementById("reqMsg");
	    optin.errorMsgHolder2 = document.getElementById("errorMsg2");
	    optin.reqMsgHolder2 = document.getElementById("reqMsg2");
	    //optin.threeIntro = document.getElementById("threeIntro");
	    optin.errorMsgHolder3 = document.getElementById("errorMsg3");
	    optin.thankYouButton = document.getElementById("thankyouLink");
	    optin.theCount = 0;
	    optin.submitted = false;
	    optin.theDivs = optin.getElementsByClass('stepDiv');
	    optin.theStepsLis = document.getElementById("steps").getElementsByTagName("li");    
	    
	    for (var j =0; j<3; j++)	 {
	        optin.theStepsLis[j].number = j;
	        optin.addEvent(optin.theStepsLis[j], 'click', optin.jumpNav);
	    }
	    optin.addEvent(optin.nextButton, 'click', optin.goForward);
	    optin.addEvent(optin.previousButton, 'click', optin.goBack);
	    optin.addEvent(optin.signUpButtonAnchor, 'click', submitOptinForm);
	    optin.addEvent(optin.doneButton, 'click', tb_remove);
	    
	    var browser=navigator.appName;
	    if(browser == "Microsoft Internet Explorer") {	        
	        document.getElementById("left").style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=crop,src='/usa/optin/images/left.png')";
	        document.getElementById("right").style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=crop,src='/usa/optin/images/right.png')";
	        document.getElementById("topOptin").style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=crop,src='/usa/optin/images/top.png')";
	        document.getElementById("bottomOptin").style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=crop,src='/usa/optin/images/bottom.png')";
	    }
	    
	    document.getElementById("firstName").focus();
	    optin.addEvent(theForm.optin, 'click', 
	    function(){
			optin.errorMsgHolder3.style.display = "none";
			optin.valid3= formStep3Validate(theForm);
			if(optin.valid3 == false){
				optin.showError();
			}
	    });
	    
	    
	    optin.addEvent(optin.thankYouButton, 'click',
	    function(e){optin.stopDefault(e);});
	    
	    optin.addEvent(document.getElementById("doneBtnA"), 'click',
	    function(e){optin.stopDefault(e);});
	   
	},

	jumpNav : function(e) {
		optin.stopDefault(e);
	    
	    if(this.className == "active") {        
	       // if((this.id == "twoLink" && optin.theDivs[optin.theCount].id) == "one" || (this.id == "threeLink" && optin.theDivs[optin.theCount].id == "two") || (this.id == "threeLink" && optin.theDivs[optin.theCount].id) == "one"){
				optin.valid = formValidate(theForm);
				optin.valid2= formStep2Validate(theForm);
				optin.valid3= formStep3Validate(theForm);
	        //}	        
	        
	        if((optin.theCount==0 && optin.valid==false)|| optin.theCount==1 && optin.valid2==false || optin.theCount==2 && optin.valid3==false) {
	            optin.showError();
	        }
			else {

	            optin.theDivs[optin.theCount].style.display = "none";
	            optin.theStepsLis[optin.theCount].className = "";
    	        
	            optin.theCount = this.number;
    	        
	             if(optin.theCount < 3) {	        
	                for (var i =0; i<3; i++) {
	                    if(optin.visited[i] == true) {
	                    optin.theStepsLis[i].className = "active";
	                    }
	                }
	            }     
	            optin.displayButtons();
	            clearErrorClass();
	        }
	    }
	},
	
	goForward: function(e) {
	        
	        optin.valid = formValidate(theForm);
	        optin.valid2= formStep2Validate(theForm);
	        optin.valid3= formStep3Validate(theForm);
			
	        if((optin.theCount==0 && optin.valid==false) || optin.theCount==1 && optin.valid2==false || optin.theCount==2 && optin.valid3==false) {
	            optin.showError();
	            optin.stopDefault(e);
			}
	        else {
				optin.theDivs[optin.theCount].style.display = "none";
	            optin.theStepsLis[optin.theCount].className = "";
        	   
	            optin.theCount++;
	            if(optin.theCount < 3) {
	                optin.visited[optin.theCount] = true;
	                for (var i =0; i<3; i++) {
	                    if(optin.visited[i] == true) {
	                        optin.theStepsLis[i].className = "active";
	                    }
	                }
	            } 	   	    	    
	            else {
	                optin.theStepsLis[0].className = "";
	                optin.theStepsLis[1].className = "";
	                optin.theStepsLis[2].className = "";
	            }   
	            optin.displayButtons();
	            optin.stopDefault(e);
	    }
	    
	},
	
	goBack: function(e) {
	        
	        optin.valid = formValidate(theForm);
	        optin.valid2= formStep2Validate(theForm);
	        	        
	        if((optin.theCount==0 && optin.valid==false)|| optin.theCount==1 && optin.valid2==false || optin.theCount==2 && optin.valid3==false) {
	            optin.showError();
	            optin.stopDefault(e);
	        }
	        else {
	            optin.theDivs[optin.theCount].style.display = "none";
	            optin.theStepsLis[optin.theCount].className = "";
	            clearErrorClass();
	            optin.theCount--;
        	    
	            if(optin.theCount < 3) {	        
	                for (var i =0; i<3; i++) {
	                    if(optin.visited[i] == true) {
	                        optin.theStepsLis[i].className = "active";
	                    }
	                }
	            }     
	            optin.displayButtons();
	            optin.stopDefault(e);
	    }
	},
	
	displayButtons : function() {          
	    optin.reqMsgHolder.style.display = "block";
	    optin.errorMsgHolder.style.display = "none";
	    optin.reqMsgHolder2.style.display = "block";
	    optin.errorMsgHolder2.style.display = "none";
	    optin.reqMsgHolder2.style.display = "block";
	    //optin.threeIntro.style.display = "block";
	    optin.errorMsgHolder3.style.display = "none";
	    
	    optin.theDivs[optin.theCount].style.display = "block";
	    optin.theStepsLis[optin.theCount].className = "on";    
 	    
	    var oneFields = optin.getElementsByClass("oneField",document.getElementById("one"),"*");
	    var twoFields = document.getElementById("two").getElementsByTagName("select");
	    var threeFields = document.getElementById("three").getElementsByTagName("input");	    
	    
	    switch(optin.theCount) {
	        case 0 :
	            optin.previousButton.style.visibility = "hidden";
	            optin.doneButton.style.visibility = "hidden";
	            optin.signUpButton.style.visibility = "hidden";
	            optin.nextButton.style.visibility = "visible";
	            
	            for(var i = 0; i<oneFields.length; i++) {oneFields[i].tabIndex =  i+1;}
	            for(var j = 0; j<twoFields.length; j++) {twoFields[j].tabIndex =  "";}	            
                for(var k = 0; k<threeFields.length; k++) {threeFields[k].tabIndex =  "";}
	            
	            document.getElementById("prevBtnA").tabIndex = "";
	            document.getElementById("doneBtnA").tabIndex = i+2;
	            document.getElementById("signUpBtnA").tabIndex = "";
	            
	            document.getElementById("firstName").focus();           
	            break;
	        
	        case 1 :
	            optin.previousButton.style.visibility = "visible";
	            optin.doneButton.style.visibility = "hidden";
	            optin.signUpButton.style.visibility = "hidden";
	            optin.nextButton.style.visibility = "visible";
	            
	            for(var i = 0; i<oneFields.length; i++) {oneFields[i].tabIndex =  "";}
	            for(var j = 0; j<twoFields.length; j++) {twoFields[j].tabIndex =  j+1;}	            
	            for(var k = 0; k<threeFields.length; k++) {threeFields[k].tabIndex =  "";}
	                        
	            document.getElementById("prevBtnA").tabIndex = j+2;
	            document.getElementById("doneBtnA").tabIndex = j+3;
	            document.getElementById("signUpBtnA").tabIndex = "";
	            document.getElementById("mktg1").focus();         
	                      
	            break;
	        
	        case 2 :
	            optin.previousButton.style.visibility = "visible";
	            optin.doneButton.style.visibility = "hidden";
	            optin.signUpButton.style.visibility = "visible";
	            optin.nextButton.style.visibility = "hidden";
	            
	            for(var i = 0; i<oneFields.length; i++) {oneFields[i].tabIndex =  "";}
	            for(var j = 0; j<twoFields.length; j++) {twoFields[j].tabIndex =  "";}	            
	            for(var k = 0; k<threeFields.length; k++) {threeFields[k].tabIndex =  k+1;}
	            
	            document.getElementById("prevBtnA").tabIndex = k+2;
	            document.getElementById("doneBtnA").tabIndex = "";
	            document.getElementById("signUpBtnA").tabIndex = k+3;
	            document.getElementById("optinChkBx").focus();
	            break;
	        
	        case 3 :
	            optin.previousButton.style.visibility = "hidden";
	            optin.doneButton.style.visibility = "visible";
	            optin.signUpButton.style.visibility = "hidden";
	            optin.nextButton.style.visibility = "hidden";
	            
	            for(var i = 0; i<oneFields.length; i++) {oneFields[i].tabIndex =  "";}
	            for(var j = 0; j<twoFields.length; j++) {twoFields[j].tabIndex =  "";}	            
	            for(var k = 0; k<threeFields.length; k++) {threeFields[k].tabIndex =  "";}
	            
	            document.getElementById("prevBtnA").tabIndex = "";
	            document.getElementById("doneBtnA").tabIndex = "";
	            document.getElementById("signUpBtnA").tabIndex = "";
	           // document.getElementById("doneBtnA").focus();
	            break;	        
	    } 
	       	    
	},
	
	showError : function() {
	    optin.reqMsgHolder.style.display = "none";
	    optin.errorMsgHolder.innerHTML = optin.errorMsgText;
	    optin.errorMsgHolder.style.display = "block";
	    
	    optin.reqMsgHolder2.style.display = "none";
	    optin.errorMsgHolder2.innerHTML = optin.errorMsgText;
	    optin.errorMsgHolder2.style.display = "block";
	    
	    //optin.threeIntro.style.display = "none";
	    optin.errorMsgHolder3.innerHTML = optin.errorMsgText;
	    optin.errorMsgHolder3.style.display = "block";
	},
	
	getElementsByClass : function(searchClass,node,tag) {
        var classElements = new Array();
        if ( node == null )
            node = document;
        if ( tag == null )
            tag = '*';
        var els = node.getElementsByTagName(tag);
        var elsLen = els.length;
        for (i = 0, j = 0; i < elsLen; i++) {
            if (  els[i].className.indexOf(searchClass) > -1 ) {
                classElements[j] = els[i];
                j++;
            }
        }
        return classElements;
    },   
	
	addEvent : function(obj, type, func) {
    if (obj.addEventListener) {obj.addEventListener(type, func, false);}
    else if (obj.attachEvent) {
       obj["e" + type + func] = func;
       obj[type + func] = function() {obj["e" + type + func] (window.event);}
       obj.attachEvent("on" + type, obj[type + func]);
    }
    else {obj["on" + type] = func;}
	},
 
	stopDefault : function(e) {
     if (!e) {e = window.event;}
     if (!e.preventDefault) {
         e.preventDefault = function() { this.returnValue = false; }
     }
     e.preventDefault();
     return false;
  }
}

