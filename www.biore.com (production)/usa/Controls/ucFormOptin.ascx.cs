﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KAOForms;


namespace KaoBrands.FormStuff
{
    public partial class ucFormOptin : System.Web.UI.UserControl
    {
        public CheckBox _cbSiteOptin{ get { return this.siteOptin; } }
        public CheckBox _cbBanOptin{ get { return this.banoptin; } }
        public CheckBox _cbBioreOptin { get { return this.bioreoptin; } }
        public CheckBox _cbCurelOptin { get { return this.cureloptin; } }
        public CheckBox _cbJergensOptin { get { return this.jergensoptin; } }
        public CheckBox _cbJFriedaOptin { get { return this.johnfriedaoptin; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                banoptinLabel.Text = "<a href=\"http://www.feelbanfresh.com\" target=\"_blank\">Ban<sup>&reg;</sup></a> antiperspirant and deodorant products keep you 3x cooler and fresher<sup>&#134;</sup><br /><span class=\"ban_disc\"><sup>&#134;</sup> vs. ordinary invisible solids</span>";

                bioreoptinLabel.Text = "<a href=\"http://www.biore.com/usa/\" target=\"_blank\">Bior&eacute;<sup>&reg;</sup></a> Skincare delivers beautiful skin by providing lots of options to meet daily, weekly and specialty skin care needs.";

                cureloptinLabel.Text = "<a href=\"http://www.curel.com/default.aspx\" target=\"_blank\">Cur&eacute;l<sup>&reg;</sup></a> Skincare's full line of hand, foot and body moisturizers delivers freedom from dry skin.";

                jergensoptinLabel.Text = "<a href=\"http://www.jergens.com/default.aspx\" target=\"_blank\">Jergens<sup>&reg;</sup></a> Skincare collection of moisturizers delivers a natural glow, smooth and firm skin and an allure that captivates.";

                johnfriedaoptinLabel.Text = "<a href=\"http://www.johnfrieda.com/en-US/Home/\" target=\"_blank\">John Frieda<sup>&reg;</sup></a> Hair Care provides solutions for hair care problems, resulting in hair that looks fabulous.";

            }
            else
            {
                             
            }

        }

        protected void optinValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (siteOptin.Checked == true);
        }

        private void SubmitOptinData()
        {

        }

       
    }
}