﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using KAOForms;

namespace KaoBrands.FormStuff
{
    public partial class ucFormConfig : System.Web.UI.UserControl
    {
        public bool ResultSucess { get; set; }
        private FormConfig oFormConfigValues;
        private DefaultValues oDefaultValues;
        private Control HeaderControl;
        private int siteID;
        private string formID = string.Empty;
        bool loadForm = false;

        #region Page Load/Init

        protected override void OnInit(EventArgs e)
        {
            // Allow The Base Class To Intialize First
            base.OnInit(e);

            //Set Default Config Objects
            oDefaultValues = DefaultFormDAO.GetDefaultValues();

            //initialize form utility class
            FormUtils fUtils = new FormUtils();

            //Get Form ID
            formID = fUtils.getFormID(Request.Url.ToString(), Request.QueryString["promo"], oDefaultValues); 

            //check if the formID parameter is valid
            if (!fUtils.formIDisValid(formID))
            { 
                setPostBackState("invalid",null);
            }
            else
            {
                //check if form ID is active
                oFormConfigValues = FormConfigDAO.GetFormByUniquePromoId(formID);
                if (oFormConfigValues.isActive.ToLower() != "true")
                {
                    setPostBackState("inactive",null);                   
                }
                else
                {
                    //Need to buid dynamic controls before Page_Load so they can be accessed on postback
                    PageUtils pUtils = new PageUtils();
                    pUtils.BuildCustomQuestionsPanel(QuestionPanel, oFormConfigValues, oDefaultValues, Request);
                    loadForm = true;
                }
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //this loadForm flag is needed to stop the page from loading if the Form ID is not valid
            //the Form ID logic is the page init, so even if that function sets a display state
            //and exits, the page load function will always run.  We don't want the paage load
            //to execute if the form ID is not valid.
            if (!loadForm)
            {
                return;
            }

            FormUtils fUtils = new FormUtils();
            //the form is valid and active, check if session is valid
            if (!fUtils.sessionValid(oFormConfigValues.SessionData))
            {
                //Session is Not valid
                setPostBackState("invalid", null);
                return;
            }

            //initialize general page content (image, header text, etc)
            initializePage();

            //Set PromoID meta tag
            PageUtils pUtils = new PageUtils();
            pUtils.createMetaDataTag("DCSext.PromoFormID", oFormConfigValues.UniquePromoId, "metaPromoID", HeaderControl);
     
            if (!IsPostBack)
            {
          
                //Check Form Scheule
                bool isActive = fUtils.isScheduleActive(oFormConfigValues.Schedule);
                if (!isActive) { setPostBackState("inactive",null); return; }

                //Check if valid member for this form
                bool validMember = fUtils.isValidMember(oFormConfigValues.MembersOnly, Request);

                //If the member is not valis, set not member state
                if (!validMember) { setPostBackState("notmember", null); return; }
                else
                {           
                    //If the member is valid, check if this is a members only form, if so...
                    //if Members Only form, set hash session key               
                    if (!string.IsNullOrEmpty(oFormConfigValues.MembersOnly.isMemberOnly) && oFormConfigValues.MembersOnly.isMemberOnly.ToLower() == "true")
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString[oFormConfigValues.MembersOnly.EmailParam])
                        && !string.IsNullOrEmpty(Request.QueryString[oFormConfigValues.MembersOnly.MemerIDParam]))
                        {

                            string key = string.Empty;
                            string qsEmail = Request.QueryString[oFormConfigValues.MembersOnly.EmailParam];
                            int qsMemID = int.Parse(Request.QueryString[oFormConfigValues.MembersOnly.MemerIDParam]);
                            key = fUtils.generateMemberKey(oFormConfigValues.UniquePromoId, qsMemID, qsEmail);

                            //Set Session Value                        
                            Session["urlHash"] = key;

                            ucFormMemberInfo._txEmail.Text = qsEmail;
                            ucFormMemberInfo._txEmail.Enabled = false;
                        }
                    }
                }

                //Check if Max Entries have been reached
                bool capExceeded = fUtils.MaxCapLimitExceeded(oFormConfigValues.MaxEntriesData, oFormConfigValues.ConsumerInfoData.EventID);
                if (capExceeded) 
                {
                    bool hasSecondaryID = fUtils.hasSecondaryID(oFormConfigValues.MaxEntriesData);
                    if (hasSecondaryID)
                    {
                        //if the secondary ID does not match the currentID (to prevent accidental endless loop)
                        if (oFormConfigValues.UniquePromoId.ToLower() != oFormConfigValues.MaxEntriesData.SecondaryPromoId.ToLower())
                        {
                            string url = fUtils.buildSecondaryIDUrl(Request, oFormConfigValues.MaxEntriesData.SecondaryPromoId);
                            Response.Redirect(url);
                            return;
                        }                        
                    }
                    else
                    {
                        setPostBackState("maxcap", null);
                        return;
                    }
                }

                //set the default state of the form

                //if form type is not redirect, set default state
                if (!string.IsNullOrEmpty(oFormConfigValues.isRedirect) && oFormConfigValues.isRedirect.ToLower() == "true")
                {
                    SubmitData();
                }
                else
                {                   
                    setDefaultState();
                }
                
            
            }
            else
            {                

                bool maxCapExceeded = fUtils.MaxCapLimitExceeded(oFormConfigValues.MaxEntriesData, oFormConfigValues.ConsumerInfoData.EventID);
                bool userCapExceeded = fUtils.UserCapLimitExceeded(oFormConfigValues.LimitPerUserData, ucFormMemberInfo._txEmail.Text, oFormConfigValues.ConsumerInfoData.EventCode);

                //if the form is members check the querystring parameters agaisnt the session key
                if (!string.IsNullOrEmpty(oFormConfigValues.MembersOnly.isMemberOnly) && oFormConfigValues.MembersOnly.isMemberOnly.ToLower() == "true")
                {
                    //check querystring to see if the original hash matches the new hash                    
                    if (!string.IsNullOrEmpty(Request.QueryString[oFormConfigValues.MembersOnly.EmailParam])
                        && !string.IsNullOrEmpty(Request.QueryString[oFormConfigValues.MembersOnly.MemerIDParam]))
                    {
                        
                        string key = string.Empty;
                        string qsEmail = Request.QueryString[oFormConfigValues.MembersOnly.EmailParam];
                        int qsMemID = int.Parse(Request.QueryString[oFormConfigValues.MembersOnly.MemerIDParam]);
                        key = fUtils.generateMemberKey(oFormConfigValues.UniquePromoId, qsMemID, qsEmail);

                        if (Session["urlHash"].ToString() != key)
                        {
                            //show error, emails do not match
                            setPostBackState("notmember", null);
                            return;
                        }

                        if (qsEmail.ToLower() != ucFormMemberInfo._txEmail.Text.ToLower())
                        {
                            //show error, emails do not match
                            setPostBackState("notmember", null);
                            return;
                        }
                    }
                    else
                    {
                        //show error, email was not supplied in URL
                        setPostBackState("notmember", null);
                        return;
                    }
                }

                //Check if max event limit has been reached, if so, do not submit and set the cap reached state
                if (maxCapExceeded)
                {
                    setPostBackState("maxcap", null);
					return;
                }
                //Check if per user limit has been reached, if so, do not submit and set the cap reached state
                else if (userCapExceeded)
                {
                    setPostBackState("usercap", null);   
					return;					
                }
                else
                {
                    //For some reason we have to set minimum age again before validation
                    setMinimumAge();

                    Page.Validate();
                    if (Page.IsValid)
                    {
                        //if page is valid, submit data
                          //if this form requires session, make sure they can only sumit once
                        if (oFormConfigValues.SessionData !=null &&
                            !string.IsNullOrEmpty(oFormConfigValues.SessionData.sessionRequired) 
                            && oFormConfigValues.SessionData.sessionRequired.ToLower() =="true")
                        {
                            //form is session based, check to see if it has been submitted already:
                            if (Session[oFormConfigValues.UniquePromoId + "_submit"] != null
                                && Session[oFormConfigValues.UniquePromoId + "_submit"].ToString().ToLower() == "true")
                            {
                                //form has alreay been submitted, show one per user message
                                setPostBackState("usercap", null);
                                return;
                            }
                            else
                            {
                                //form has not been submitted, submit the form:
                                SubmitData();
                            }
                        }
                        else
                        {
                            //not a session based form, go ahead and submit:
                            SubmitData();
                        }
                    }
                    else
                    {
                        //if the page is not valid, return to setDefaultState();
                        PanelForm.Visible = true;
                    }
                }

            }

        }

        private void initMetrix()
        {
            //This code was pulled from the current Opt-In form
            Literal crmMetrixLiteral;
            MasterPage mp = Page.Master;
            if (mp != null)
            {
                crmMetrixLiteral = (Literal)mp.FindControl("literal_crmmetrix");
                if (crmMetrixLiteral != null)
                {
                    crmMetrixLiteral.Visible = false;
                }
            }
        }

        private void setMinimumAge()
        {
            //this function sets the minium age for the ucFormMemberInfo control
            if (oFormConfigValues.AgeLimit.hasLimit == "true")
            {
                try
                {
                    int minAge;
                    minAge = int.Parse(oFormConfigValues.AgeLimit.MinAge);
                    if (minAge >= 0)
                    {
                        ucFormMemberInfo.minimumAge = minAge;
                    }
                }
                catch (Exception)
                {
                }

            }

        }

        private void initializePage()
        {
            PageUtils pUtils = new PageUtils();
            //Get page header control
            HeaderControl = pUtils.getHeaderControl(Page);
            
            //initialize site metrix
            initMetrix();
            
            //get the site ID from the appconfig
            siteID = oDefaultValues.CurrentSiteID;
            
            //Set CSS Form Class
            pUtils.setCssClass(PageContainer, oFormConfigValues.Layout.BodyClass);

            //Set Equalize Column Flag
            pUtils.setEqualColumnFlag(equalHeightVars, oFormConfigValues.Layout);

            //Set Meta data
            pUtils.setMetaData(Page, oFormConfigValues.MetaInfoData, oDefaultValues.cssPath,HeaderControl);

            //Set Brand image
            pUtils.setBrandImage(oFormConfigValues.Images, Image1);

            //Set Header Image
            pUtils.setHeaderImage(oFormConfigValues.Images, Page, PageHeader);

            //Populate form header literal
            pUtils.setLiteralText(oFormConfigValues.CopyData.HeaderText, LitHeaderText);

            //Populate form disclaimer literal
            pUtils.setLiteralText(oFormConfigValues.CopyData.DisclaimerText,LitDisclaimer);

        }               

        #endregion

        #region Display States

        private void setDefaultState()
        {
            //this function sets up the default (non postback) state of the form
            PageUtils pUtils = new PageUtils();

            //set minimum age            
            setMinimumAge();

            //populate form description
            pUtils.setLiteralText(oFormConfigValues.CopyData.DescriptionText, LitDescriptionText);
  
           //hide optionalFields
            pUtils.setOptionalFields(oFormConfigValues.OptionalFields, ucFormMemberInfo);

            //hide opt-in
            pUtils.setOptinFields(siteID,oFormConfigValues.OptinsData,oDefaultValues,ucFormOptin);
                         
            //populate form submit button text
            pUtils.setSubmitButton(oFormConfigValues.SubmitData.ButtonText, this.submit);
        }

        private void setPostBackState(string pageID, FormSubmitData SubmitData)
        {
            PageUtils pUtils = new PageUtils();

            //Hide Form
            PanelForm.Visible = false;

            switch (pageID)
            {
                case "maxcap":
                    pUtils.createMetaDataTag("DCSext.status", "4000", "metaMsg", HeaderControl);
                    pUtils.setLiteralText(oFormConfigValues.MaxEntriesData.CustomPastCapText, LitDescriptionText);
                    break; 
                case "usercap":
                    pUtils.createMetaDataTag("DCSext.status", "5000", "metaMsg", HeaderControl);
                    pUtils.setLiteralText(oFormConfigValues.LimitPerUserData.CustomPastCapText, LitDescriptionText);
                    break;
                case "notmember":
                    pUtils.createMetaDataTag("DCSext.status", "3000", "metaMsg", HeaderControl);
                    pUtils.setLiteralText(oFormConfigValues.MembersOnly.CustomError, LitDescriptionText);
                    break;
                case "coupon":
                    pUtils.setLiteralText(oFormConfigValues.CopyData.ConfimationText, LitDescriptionText);
                    string couponUrl = pUtils.setCouponLink(oFormConfigValues.CouponData, SubmitData.MemberData.Email, DescriptionContainer);
                    pUtils.setCouponRedirect(oFormConfigValues.CouponData, couponUrl, HeaderControl);         
                    break;
                case "error":
                    pUtils.setLiteralText(oDefaultValues.SubmitErrorCopy, LitDescriptionText);
                    break;
                case "invalid":
                    HeaderControl = pUtils.getHeaderControl(Page);
                    MetaInfo mDataIvalid = new MetaInfo();
                    mDataIvalid.PageTitle = oDefaultValues.DefaultPageTitle;
                    mDataIvalid.Keywords = "";
                    mDataIvalid.Description = "We're sorry";
                    pUtils.setMetaData(Page, mDataIvalid, oDefaultValues.cssPath, HeaderControl);

                    if (!string.IsNullOrEmpty(oDefaultValues.InvalidFormCopy))
                    {
                        LitDescriptionText.Text = oDefaultValues.InvalidFormCopy;
                    }
                    else
                    {
                        Response.Redirect("~/" + oDefaultValues.ErrorPage);
                    }
                    break;
                case "inactive":
                    HeaderControl = pUtils.getHeaderControl(Page);
                    MetaInfo mDataInactive = new MetaInfo();
                    mDataInactive.PageTitle = oDefaultValues.DefaultPageTitle;
                    mDataInactive.Keywords = "";
                    mDataInactive.Description = "We're sorry";
                    pUtils.setMetaData(Page, mDataInactive, oDefaultValues.cssPath, HeaderControl);

                    if (!string.IsNullOrEmpty(oFormConfigValues.CopyData.InactiveFormCopy))
                    {
                        LitDescriptionText.Text = oFormConfigValues.CopyData.InactiveFormCopy;
                    }
                    else if (!string.IsNullOrEmpty(oDefaultValues.InactiveFormCopy))
                    {
                        //if the form does not have copy, attempt to use the default copy
                        LitDescriptionText.Text = oDefaultValues.InactiveFormCopy;
                    }
                    else
                    {
                        //if there is no copy, redirect to error page
                        Response.Redirect("~/" + oDefaultValues.ErrorPage);
                    }
                    break;
                case "default":
                    pUtils.setLiteralText(oFormConfigValues.CopyData.ConfimationText, LitDescriptionText);
                    break;
                default:
                    break;
            }
        }

          #endregion

        #region Question Data

        private MemberData getMemberInfo()
        {
            string DOB = string.Empty;
            if (oFormConfigValues.OptionalFields.DOBVisible.ToLower() == "true")
            {
                DOB = ucFormMemberInfo._ddMonth.SelectedValue + "/" + ucFormMemberInfo._ddDay.SelectedValue + "/" + ucFormMemberInfo._ddYear.SelectedValue;

            }
            else
            {
                DOB = "empty";
            }
            MemberData mData = new MemberData();
            mData.FirstName = ucFormMemberInfo._txFirstName.Text;
            mData.LastName = ucFormMemberInfo._txLastName.Text;
            mData.Email = ucFormMemberInfo._txEmail.Text;
            mData.Gender = ucFormMemberInfo._ddGender.SelectedValue;
            mData.DOB = DOB;
            mData.Address1 = ucFormMemberInfo._txAddress1.Text;
            mData.Address2 = ucFormMemberInfo._txAddress2.Text;
            mData.Address3 = ucFormMemberInfo._txAddress3.Text;
            mData.City = ucFormMemberInfo._txCity.Text;
            mData.State = ucFormMemberInfo._ddState.SelectedValue;
            mData.PostalCode = ucFormMemberInfo._txPostalCode.Text;
            mData.Country = "United States";
            mData.Phone = ucFormMemberInfo._txPhone.Text;
            mData.QuestionComment = ucFormMemberInfo._txComment.Text;
            return mData;
        }

        private List<UserAnswer> getMarketingQuestionData()
        {
            //build an answerlist of all submitted custom/marketing questions and parameter values
            List<UserAnswer> MarketingAnswers = new List<UserAnswer>();

            //uses functions in the FormAnswersBLL class
            FormAnswersBLL fAnswers = new FormAnswersBLL();

            //if the marketing panel exists, get the submitted values
            Panel mp = (Panel)QuestionPanel.FindControl("MarketingPanel");
            if (mp != null)
            {
                MarketingAnswers.AddRange(fAnswers.buildAnswerList(mp, oDefaultValues.CustomQuestionsData.QuestionCollection));
            }


            return MarketingAnswers;

        }

        private List<UserAnswer> getCustomQuestionData()
        {
            //build an answerlist of all submitted custom questions and parameter values
            List<UserAnswer> CustomAnswers = new List<UserAnswer>();

            //uses functions in the fAnswers class
            FormAnswersBLL fAnswers = new FormAnswersBLL();

            //if the custom questions panel exists, get the submitted values
            Panel ccp = (Panel)QuestionPanel.FindControl("CustomQuestionPanel");
            if (ccp != null)
            {
                CustomAnswers.AddRange(fAnswers.buildAnswerList(ccp, oFormConfigValues.CustomQuestionsData.QuestionCollection));
            }

            return CustomAnswers;
        }

        private List<UserAnswer> getParameterData()
        {
            //build an answerlist of all parameter values
            List<UserAnswer> ParameterList = new List<UserAnswer>();

            //if Collect Parameters is true, then get the querystring values
            if (oFormConfigValues.QueryStringParemtersData.collectParemters.ToLower() == "true")
            {
                //uses functions in the FormAnswersBLL class
                FormAnswersBLL fAnswers = new FormAnswersBLL();
                ParameterList.AddRange(fAnswers.buildParemterList(oFormConfigValues.QueryStringParemtersData.ParameterCollection, Request, true));
            }

            return ParameterList;
        }

        private List<UserAnswer> getPostData()
        {
            //build an answerlist of all parameter values
            List<UserAnswer> ParameterList = new List<UserAnswer>();

            //if Collect Parameters is true, then get the Post Data values
            if (oFormConfigValues.PostDataParameters.collectParemters.ToLower() == "true")
            {
                FormAnswersBLL fAnswers = new FormAnswersBLL();
                Panel hp = (Panel)QuestionPanel.FindControl("HiddenFields");
                ParameterList.AddRange(fAnswers.buildPostDataList(hp, oFormConfigValues.PostDataParameters.ParameterCollection));
            }

            return ParameterList;
        }

        private OptinData getOptinData()
        {
            OptinData oData = new OptinData();
            oData.OptinSite = returnCheckBox(ucFormOptin._cbSiteOptin);
            oData.OptinBan = returnCheckBox(ucFormOptin._cbBanOptin);
            oData.OptinBiore = returnCheckBox(ucFormOptin._cbBioreOptin);
            oData.OptinCurel = returnCheckBox(ucFormOptin._cbCurelOptin);
            oData.OptinJergens = returnCheckBox(ucFormOptin._cbJergensOptin);
            oData.OptinJohnFrieda = returnCheckBox(ucFormOptin._cbJFriedaOptin);
            return oData;

        }

        protected int returnCheckBox(CheckBox cb)
        {
            int cbVal = (cb.Checked) ? 1 : 0;
            return cbVal;
        }

        #endregion

        #region Submit

        private void SubmitData()
        {

            FormSubmitBLL FormSubmit = new FormSubmitBLL();
            FormSubmitData sData = new FormSubmitData();
            SubmitResultData ResultData = new SubmitResultData();
            FormUtils fUtils = new FormUtils();
            PageUtils pUtils = new PageUtils();

            sData.UniquePromoId = oFormConfigValues.UniquePromoId;
            sData.Type = oFormConfigValues.Type;
            sData.CurrentSiteID = oDefaultValues.CurrentSiteID;
            sData.BrandSiteData = oDefaultValues.BrandSiteData;
            sData.CouponData = oFormConfigValues.CouponData;
            sData.ConsumerInfoData = oFormConfigValues.ConsumerInfoData;       

            //if the form is a redirect, then submit member email, else do normal submit
            if (!string.IsNullOrEmpty(oFormConfigValues.isRedirect) && oFormConfigValues.isRedirect.ToLower() == "true")
            {
                sData.MemberData.Email = Request.QueryString[oFormConfigValues.MembersOnly.EmailParam];
                sData.isRedirect = true;
            }
            else
            {            
                sData.MemberData = getMemberInfo();
                sData.OptinData = getOptinData();
                sData.MarketingAnswers = getMarketingQuestionData();
                sData.CustomAnswers = getCustomQuestionData();
                sData.ParameterData = getParameterData();
                sData.PostData = getPostData();                
            }

            //check for medium and sourc parameters
            if (Request.QueryString["enl_medium"] != null)
            {
                FormValidationBLL fValidator = new FormValidationBLL();
                sData.CouponData.Medium = fValidator.sanitizeString(Request.QueryString["enl_medium"]);
            }

            if (Request.QueryString["enl_source"] != null)
            {
                 FormValidationBLL fValidator = new FormValidationBLL();
                 sData.CouponData.Source = fValidator.sanitizeString(Request.QueryString["enl_source"]);
            }

            //get open ended value
            sData.ConsumerInfoData.OpenEnded = fUtils.getOpenEnded(oFormConfigValues.Type, oFormConfigValues.ConsumerInfoData, sData);

            ResultData = FormSubmit.submitFormData(sData);
              

            if (ResultData.Success)
            {
                PanelForm.Visible = false;
                //set form submitted session
                Session[oFormConfigValues.UniquePromoId + "_submit"] = "true";
				
                //create analytics success tags
                createAnalyticsSuccessTags(ResultData, sData.OptinData);

                if (oFormConfigValues.Type.ToLower() == "coupon" && !string.IsNullOrEmpty(oFormConfigValues.CouponData.CouponCode))
                {
                    setPostBackState("coupon", sData);
                }
                else
                {
                    setPostBackState("default", null);
                }
            }
            else
            {
                //set error state
                PanelForm.Visible = false;
                setPostBackState("error", null);

                //set anaytics error tags
                createAnalyticsErrorTags(ResultData);
            }

        }

        #endregion

        #region Analytics

        private void createAnalyticsSuccessTags(SubmitResultData ResultData, OptinData OptinData)
        {
            PageUtils pUtils = new PageUtils();

            //create the submit=true meta tag
            pUtils.createMetaDataTag("DCSext.submit", "true", "metaSubmit", HeaderControl);
            //create the member id meta tag
            pUtils.createMetaDataTag("DCSext.id", ResultData.MemberID.ToString(), "metaID", HeaderControl);
            //if the StatusMsg is not null, create the status meta tag
            if (!string.IsNullOrEmpty(ResultData.StatusMsg))
            {
                pUtils.createMetaDataTag("DCSext.status", ResultData.StatusMsg, "metaMsg", HeaderControl);
            }
            //create the optin meta tags
            pUtils.createAnalyticsOptinTags(oDefaultValues.CurrentSiteID, OptinData, HeaderControl);

        }

        private void createAnalyticsErrorTags(SubmitResultData ResultData)
        {
            PageUtils pUtils = new PageUtils();

            //create the submit=true meta tag
            pUtils.createMetaDataTag("DCSext.submit", "true", "metaSubmit", HeaderControl);

            //if the StatusMsg is not null, create the status meta tag
            if (!string.IsNullOrEmpty(ResultData.StatusMsg))
            {
                pUtils.createMetaDataTag("DCSext.status", ResultData.StatusMsg, "metaMsg", HeaderControl);
            }
        }

        #endregion
    }
}