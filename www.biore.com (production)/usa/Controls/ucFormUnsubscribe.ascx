﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucFormUnsubscribe.ascx.cs" Inherits="KaoBrands.FormStuff.ucFormUnsubscribe" %>

<div id="unsubscribe">
		<div id="container">
			<div id="main-content">
				<div id="optOut">
					<asp:Panel ID="OptinForm" runat="server">
					
						<h1>Unsubscribe</h1>
						<p>If you'd no longer like to hear about the latest news, promotions and contests, just
							enter your e-mail address below.</p>
						<p class="req">required<span class="required">*</span></p>
						<div class="tbin">
							<div class="input-container">
								<asp:Label ID="emailLbl" runat="server" AssociatedControlID="Email" Text="Email*" />
								<asp:TextBox ID="Email" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
								<asp:RequiredFieldValidator ID="EmailValidator1" Display="Dynamic" runat="server"
									ErrorMessage="Please enter your Email Address." ControlToValidate="Email" EnableClientScript="true"
									SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
								<asp:RegularExpressionValidator ID="EmailValidator2" Display="Dynamic" runat="server"
									ErrorMessage="Please enter a valid Email Address." ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
									ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RegularExpressionValidator>								
								<asp:RegularExpressionValidator ID="EmailValidator3" runat="server" Display="Dynamic"
									ErrorMessage="Please limit the entry to 50 characters."
									ValidationExpression="^[\s\S]{0,50}$" ControlToValidate="Email" SetFocusOnError="true"
									CssClass="errormsg"></asp:RegularExpressionValidator>
								<asp:RegularExpressionValidator ID="EmailValidator4" runat="server" Display="Dynamic"
									ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Email."
									ValidationExpression="^[^<>]+$" ControlToValidate="Email" EnableClientScript="true"
									SetFocusOnError="true" CssClass="errormsg" ></asp:RegularExpressionValidator>
								<!--end Required Field Validator Container-->
							</div>
						</div>
						<div id="submit-container">
							<p>
								<asp:Button UseSubmitBehavior="true" ID="submit" Text="Unsubscribe" runat="server" CssClass="submit buttonLink" />
							</p>
						</div>
					</asp:Panel>
					<asp:Panel ID="OptinFormSuccess" runat="server">
						<h1>We're sorry to see you go...</h1>
						<p>Your request has been sent and we&rsquo;ll go ahead and remove your e-mail address from our list.</p>
						<p>If you change your mind, you can always re-subscribe right here on our website.</p>
						<p><a href="<%= ResolveClientUrl("~/") %>">Return home</a>.</p>
					</asp:Panel>
					<asp:Panel ID="OptinFormFailure" runat="server">
						<h1>We're Sorry...</h1>
						<p>There has been a problem with your form submission.</p>
						<p>Please go back and <a href="javascript: history.back(-1)">try again</a>.</p>
					</asp:Panel>
				</div> <%-- end #optOut --%>
			</div> <%-- end #main-content --%>
			<div id="unsubscribe-img">
			</div>
		</div> <%-- end #container --%>
	</div>

