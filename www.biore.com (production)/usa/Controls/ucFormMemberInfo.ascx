﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucFormMemberInfo.ascx.cs" Inherits="KaoBrands.FormStuff.ucFormMemberInfo" %>
<%@ Register TagPrefix="tbv" Namespace="KaoBrands.FormStuff" %>


<asp:Panel ID="PanelMemberInfo" CssClass="MemberInfoContainer png-fix" runat="server">

    <div class="NameWrapper">
	     <div class="FirstNameContainer Question">
		    <asp:Label ID="FNameLbl" runat="server" Text="First Name*" AssociatedControlID="FName"></asp:Label>
		    <asp:TextBox ID="FName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
		    <div class="ErrorContainer">
		        <asp:RequiredFieldValidator ID="FNameValidator1" runat="server" Display="Dynamic"
			        ErrorMessage="Please enter your First Name." ControlToValidate="FName" EnableClientScript="true"
			        SetFocusOnError="true" CssClass="errormsg" ></asp:RequiredFieldValidator>
			</div>
			<div class="ErrorContainer">
		        <asp:RegularExpressionValidator ID="FNameValidator2" runat="server" Display="Dynamic"
			        ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your First Name."
			        ValidationExpression="^[^<>]+$" ControlToValidate="FName" EnableClientScript="true"
			        SetFocusOnError="true" CssClass="errormsg" ></asp:RegularExpressionValidator>
			</div>
	        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
		        ErrorMessage="Please limit the entry to 50 characters."
		        ValidationExpression="^[\s\S]{0,50}$" ControlToValidate="FName" SetFocusOnError="true"
		        CssClass="errormsg"></asp:RegularExpressionValidator>

	    </div>
	     <div class="LastNameContainer Question">
		    <asp:Label ID="LNameLbl" runat="server" Text="Last Name*" AssociatedControlID="LName"></asp:Label>
		    <asp:TextBox ID="LName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
		    <div class="ErrorContainer">
		        <asp:RequiredFieldValidator ID="LNameValidator1" runat="server" Display="Dynamic"
			        ErrorMessage="Please enter your Last Name." ControlToValidate="LName" EnableClientScript="true"
			        SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
			</div>
			<div class="ErrorContainer">
		        <asp:RegularExpressionValidator ID="LNameValidator2" runat="server" Display="Dynamic"
			        ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Last Name."
			        ValidationExpression="^[^<>]+$" ControlToValidate="LName" SetFocusOnError="true"
			        CssClass="errormsg"></asp:RegularExpressionValidator>
		    </div>
		    <div class="ErrorContainer">
		    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="Dynamic"
		        ErrorMessage="Please limit the entry to 50 characters."
		        ValidationExpression="^[\s\S]{0,50}$" ControlToValidate="LName" SetFocusOnError="true"
		        CssClass="errormsg"></asp:RegularExpressionValidator>
			</div>	
	    </div>
	</div>
	   
	    <div class="EmailWrapper">
	    <div class="EmailContainer Question">
		    <asp:Label ID="EmailLbl" runat="server" Text="Email*" AssociatedControlID="Email"></asp:Label>
		    <asp:TextBox ID="Email" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
		    <div class="ErrorContainer">
		        <asp:RequiredFieldValidator ID="EmailValidator1" runat="server" Display="Dynamic"
			        ErrorMessage="Please enter your Email Address." ControlToValidate="Email" EnableClientScript="true"
			        SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
		    </div>
		    <div class="ErrorContainer">
		        <asp:RegularExpressionValidator ID="EmailValidator2" runat="server" Display="Dynamic"
			        ErrorMessage="Please enter a valid Email Address." ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
			        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RegularExpressionValidator>
		    </div>
		    <div class="ErrorContainer">
		       <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Display="Dynamic"
		        ErrorMessage="Please limit the entry to 150 characters."
		        ValidationExpression="^[\s\S]{0,150}$" ControlToValidate="FName" SetFocusOnError="true"
		        CssClass="errormsg"></asp:RegularExpressionValidator>
	        </div>
	    </div>		    
	    <asp:Panel ID="PanelConfirmEmail" CssClass="ConfirmEmailContainer Question" runat="server">
		    <asp:Label ID="ConfirmEmailLbl" runat="server" Text="Confirm Email*" AssociatedControlID="Email"></asp:Label>
		    <asp:TextBox ID="ConfirmEmail" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
		    <div class="ErrorContainer">
		        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
			        ErrorMessage="Please confirm your Email Address." ControlToValidate="ConfirmEmail" EnableClientScript="true"
			        SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
            </div>
            <div class="ErrorContainer">
                <asp:CompareValidator ID="CompareEmailValidator" runat="server" 
                    ErrorMessage="The email values do not match"  EnableClientScript="true"
                    ControlToCompare="Email" ControlToValidate="ConfirmEmail"
                    Display="Dynamic" CssClass="errormsg" SetFocusOnError="true"></asp:CompareValidator>    
	        </div>
	    </asp:Panel>
		</div>
		<div class="seperator png-fix"></div>
		
	
        <asp:Panel ID="PanelAddress" CssClass="AddressContainer" runat="server">  
            <div class="AddressWrapper">
                <div class="Address1Container Question">  
				    <asp:Label ID="Address1Lbl" runat="server" Text="Address 1*" AssociatedControlID="Address1"></asp:Label>
			        <asp:TextBox ID="Address1" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
			        <div class="ErrorContainer">
			            <asp:RequiredFieldValidator ID="Address1Validator1" runat="server" Display="Dynamic"
				            ErrorMessage="Please enter your Address." ControlToValidate="Address1" EnableClientScript="true"
				            SetFocusOnError="true" CssClass="errormsg"/>
			        </div>
			        <div class="ErrorContainer">
			            <asp:RegularExpressionValidator ID="Address1Validator2" runat="server" Display="Dynamic"
				            ErrorMessage="Please do not use special characters in the Address 1 field." ValidationExpression="^[^<>]+$"
				            ControlToValidate="Address1" EnableClientScript="true" SetFocusOnError="true"
				            CssClass="errormsg"/>
		           </div>
		           <div class="ErrorContainer">
		              <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" Display="Dynamic"
		                    ErrorMessage="Please limit the entry to 50 characters."
		                    ValidationExpression="^[\s\S]{0,50}$" ControlToValidate="Address1" SetFocusOnError="true"
		                    CssClass="errormsg"></asp:RegularExpressionValidator>
			        </div>
			    </div>
		        <div class="Address2Container Question">
			        <asp:Label ID="Address2Lbl" runat="server" Text="Address 2" AssociatedControlID="Address2"></asp:Label>
			        <asp:TextBox ID="Address2" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
			        <div class="ErrorContainer">
			            <asp:RegularExpressionValidator ID="Address2Validator1" runat="server" Display="Dynamic"
				            ErrorMessage="Please do not use special characters in the Address 2 field." ValidationExpression="^[^<>]+$"
				            ControlToValidate="Address2" EnableClientScript="true" SetFocusOnError="true"
				            CssClass="errormsg"/>
		           </div>
		            <div class="ErrorContainer">
		                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" Display="Dynamic"
		                    ErrorMessage="Please limit the entry to 50 characters."
		                    ValidationExpression="^[\s\S]{0,50}$" ControlToValidate="Address2" SetFocusOnError="true"
		                    CssClass="errormsg"></asp:RegularExpressionValidator>
			        </div>	        
		        </div>
		        <div class="Address3Container Question">
			        <asp:Label ID="Address3Lbl" runat="server" Text="Address 3" AssociatedControlID="Address3"></asp:Label>
			        <asp:TextBox ID="Address3" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
			        <div class="ErrorContainer">
			            <asp:RegularExpressionValidator ID="Address3Validator1" runat="server" Display="Dynamic"
				            ErrorMessage="Please do not use special characters in the Address 3 field." ValidationExpression="^[^<>]+$"
				            ControlToValidate="Address3" EnableClientScript="true" SetFocusOnError="true"
				            CssClass="errormsg"/>
				    </div>
		            <div class="ErrorContainer">
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" Display="Dynamic"
		                    ErrorMessage="Please limit the entry to 50 characters."
		                    ValidationExpression="^[\s\S]{0,50}$" ControlToValidate="Address3" SetFocusOnError="true"
		                    CssClass="errormsg"></asp:RegularExpressionValidator>
			        </div>
		        </div>
		        <div class="CityContainer Question">
			        <asp:Label ID="CityLbl" runat="server" Text="City*" AssociatedControlID="City"></asp:Label>
			        <asp:TextBox ID="City" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
			        <div class="ErrorContainer">
			            <asp:RequiredFieldValidator ID="CityValidator1" runat="server" Display="Dynamic"
				            ErrorMessage="Please enter your City." ControlToValidate="City" EnableClientScript="true"
				            SetFocusOnError="true" CssClass="errormsg"/>
				    </div>
			        <div class="ErrorContainer">
			            <asp:RegularExpressionValidator ID="CityValidator2" runat="server" Display="Dynamic"
				            ErrorMessage="Please do not use special characters in the City field." ValidationExpression="^[^<>]+$"
				            ControlToValidate="City" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"/>
		            </div>
		            <div class="ErrorContainer">
		                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" Display="Dynamic"
		                    ErrorMessage="Please limit the entry to 50 characters."
		                    ValidationExpression="^[\s\S]{0,50}$" ControlToValidate="City" SetFocusOnError="true"
		                    CssClass="errormsg"></asp:RegularExpressionValidator>
		            </div>
		        </div>	
		    </div>		    	
		    <div class="StateContainer Question">
			    <asp:Label ID="StateLbl" runat="server" Text="State*" AssociatedControlID="State"></asp:Label>
			    <asp:DropDownList ID="State" runat="server">
				    <asp:ListItem Value="" Selected="true">Select State</asp:ListItem>
				    <asp:ListItem Value="AL">Alabama</asp:ListItem>
				    <asp:ListItem Value="AK">Alaska</asp:ListItem>
				    <asp:ListItem Value="AZ">Arizona</asp:ListItem>
				    <asp:ListItem Value="AR">Arkansas</asp:ListItem>
				    <asp:ListItem Value="CA">California</asp:ListItem>
				    <asp:ListItem Value="CO">Colorado</asp:ListItem>
				    <asp:ListItem Value="CT">Connecticut</asp:ListItem>
				    <asp:ListItem Value="DE">Delaware</asp:ListItem>
				    <asp:ListItem Value="DC">District Of Columbia</asp:ListItem>
				    <asp:ListItem Value="FL">Florida</asp:ListItem>
				    <asp:ListItem Value="GA">Georgia</asp:ListItem>
				    <asp:ListItem Value="HI">Hawaii</asp:ListItem>
				    <asp:ListItem Value="ID">Idaho</asp:ListItem>
				    <asp:ListItem Value="IL">Illinois</asp:ListItem>
				    <asp:ListItem Value="IN">Indiana</asp:ListItem>
				    <asp:ListItem Value="IA">Iowa</asp:ListItem>
				    <asp:ListItem Value="KS">Kansas</asp:ListItem>
				    <asp:ListItem Value="KY">Kentucky</asp:ListItem>
				    <asp:ListItem Value="LA">Louisiana</asp:ListItem>
				    <asp:ListItem Value="ME">Maine</asp:ListItem>
				    <asp:ListItem Value="MD">Maryland</asp:ListItem>
				    <asp:ListItem Value="MA">Massachusetts</asp:ListItem>
				    <asp:ListItem Value="MI">Michigan</asp:ListItem>
				    <asp:ListItem Value="MN">Minnesota</asp:ListItem>
				    <asp:ListItem Value="MS">Mississippi</asp:ListItem>
				    <asp:ListItem Value="MO">Missouri</asp:ListItem>
				    <asp:ListItem Value="MT">Montana</asp:ListItem>
				    <asp:ListItem Value="NE">Nebraska</asp:ListItem>
				    <asp:ListItem Value="NV">Nevada</asp:ListItem>
				    <asp:ListItem Value="NH">New Hampshire</asp:ListItem>
				    <asp:ListItem Value="NJ">New Jersey</asp:ListItem>
				    <asp:ListItem Value="NM">New Mexico</asp:ListItem>
				    <asp:ListItem Value="NY">New York</asp:ListItem>
				    <asp:ListItem Value="NC">North Carolina</asp:ListItem>
				    <asp:ListItem Value="ND">North Dakota</asp:ListItem>
				    <asp:ListItem Value="OH">Ohio</asp:ListItem>
				    <asp:ListItem Value="OK">Oklahoma</asp:ListItem>
				    <asp:ListItem Value="OR">Oregon</asp:ListItem>
				    <asp:ListItem Value="PA">Pennsylvania</asp:ListItem>
				    <asp:ListItem Value="RI">Rhode Island</asp:ListItem>
				    <asp:ListItem Value="SC">South Carolina</asp:ListItem>
				    <asp:ListItem Value="SD">South Dakota</asp:ListItem>
				    <asp:ListItem Value="TN">Tennessee</asp:ListItem>
				    <asp:ListItem Value="TX">Texas</asp:ListItem>
				    <asp:ListItem Value="UT">Utah</asp:ListItem>
				    <asp:ListItem Value="VT">Vermont</asp:ListItem>
				    <asp:ListItem Value="VA">Virginia</asp:ListItem>
				    <asp:ListItem Value="WA">Washington</asp:ListItem>
				    <asp:ListItem Value="WV">West Virginia</asp:ListItem>
				    <asp:ListItem Value="WI">Wisconsin</asp:ListItem>
				    <asp:ListItem Value="WY">Wyoming</asp:ListItem>
			    </asp:DropDownList>
			    <div class="ErrorContainer">
			        <asp:RequiredFieldValidator ID="StateValidator" runat="server" Display="Dynamic"
				        ErrorMessage="Please select your State." ControlToValidate="State" EnableClientScript="true"
				        SetFocusOnError="true" CssClass="errormsg"/>
				</div>
		    </div>  	
        </asp:Panel>
        
        <asp:Panel ID="PanelPostalCode" cssClass="PostalCodeContainer Question" runat="server">
            <asp:Label ID="PostalCodeLbl" runat="server" Text="ZIP Code*" AssociatedControlID="PostalCode"></asp:Label>
	        <asp:TextBox ID="PostalCode" runat="server" MaxLength="5" CssClass="inputTextBox"></asp:TextBox>
	        <div class="ErrorContainer">
	            <asp:RequiredFieldValidator ID="PostalCodeValidator1" runat="server" Display="Dynamic"
		            ErrorMessage="Please enter your ZIP Code." ControlToValidate="PostalCode" EnableClientScript="true"
		            SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>						
	        </div>
            <div class="ErrorContainer">
                <asp:RegularExpressionValidator ID="PostalCodeValidator3" runat="server" Display="Dynamic"
							ErrorMessage="Please enter a valid 5 digit ZIP code." ValidationExpression="^\d{5}$"
							ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true"
							CssClass="errormsg"></asp:RegularExpressionValidator>
                
            </div>
        </asp:Panel>
        
        <asp:Panel ID="PanelCoutnry" cssClass="CountryContainer Question" runat="server">
            <asp:Label ID="CountryLbl" runat="server" Text="Country&nbsp;"></asp:Label>
	        <span class="fake_input">United States</span>
        </asp:Panel>      
         <div class="seperator png-fix"></div>
   
   
        <asp:Panel ID="PanelGender" CssClass="GenderContainer Question" runat="server">
	        <asp:Label ID="GenderLbl" runat="server" Text="Gender" AssociatedControlID="Gender"></asp:Label>            
	        <asp:DropDownList ID="Gender" runat="server">
		        <asp:ListItem Value="">Select</asp:ListItem>
		        <asp:ListItem Value="Female">Female</asp:ListItem>
		        <asp:ListItem Value="Male">Male</asp:ListItem>
	        </asp:DropDownList>
	        <div class="ErrorContainer">
	            <asp:RequiredFieldValidator ID="GenderValidator" runat="server" Display="Dynamic"
		        ErrorMessage="Please enter your Gender." ControlToValidate="Gender" EnableClientScript="true"
		        SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
    	    </div>
    	</asp:Panel>
    	
		<asp:Panel ID="PanelDOB" CssClass="BirthdayContainer Question" runat="server">
		    <asp:Label ID="DOBLbl" runat="server" Text="Birthdate*" AssociatedControlID="yyyy"></asp:Label>
		    <asp:DropDownList ID="mm" runat="server" CssClass="inputSelectBDay month"></asp:DropDownList>
		    <asp:DropDownList ID="dd" runat="server" CssClass="inputSelectBDay day"></asp:DropDownList>
		    <asp:DropDownList ID="yyyy" runat="server" CssClass="inputSelectBDay year"></asp:DropDownList>
		    <div class="ErrorContainer">
		    <asp:RequiredFieldValidator ID="mmValidator1" runat="server" Display="Dynamic" ErrorMessage="Please enter your Birth Month."
			    ControlToValidate="mm" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg month"></asp:RequiredFieldValidator>
		    </div>
		    <div class="ErrorContainer">
		    <asp:RequiredFieldValidator ID="ddValidator2" runat="server" Display="Dynamic" ErrorMessage="Please enter your Birth Day."
			    ControlToValidate="dd" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg day"></asp:RequiredFieldValidator>
		    </div>
		    <div class="ErrorContainer">
		    <asp:RequiredFieldValidator ID="yyyyValidator3" runat="server" Display="Dynamic" ErrorMessage="Please enter your Birth Year."
			    ControlToValidate="yyyy" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg year"></asp:RequiredFieldValidator>
		    </div>
		    <div class="ErrorContainer">
		    <asp:CustomValidator ID="DOBValidator4" runat="server" Display="Dynamic" ErrorMessage="Please enter a valid date."
			    OnServerValidate="DOBValidate" EnableClientScript="false" CssClass="errormsg"></asp:CustomValidator>
		    </div>
		    <div class="ErrorContainer">
		    <asp:CustomValidator ID="DOBMinAgeValidator5" runat="server" Display="Dynamic" ErrorMessage="Minimum age requirements are not met."
			    OnServerValidate="DOBValidateMinAge" EnableClientScript="false" CssClass="errormsg"></asp:CustomValidator>
		    </div>
		</asp:Panel>
		
		<asp:Panel ID="PanelPhone" CssClass="PhoneContainer Question" runat="server">   
	        <asp:Label ID="PhoneLbl" runat="server" Text="Phone" AssociatedControlID="Phone"></asp:Label>
			<asp:TextBox ID="Phone" MaxLength="50" runat="server" />
			<div class="ErrorContainer">
			    <asp:RegularExpressionValidator ID="PhoneValidator1" runat="server" ErrorMessage="Your Phone is not required, but please do not use special characters in the Phone field."
				    ValidationExpression="^[^<>]+$" ControlToValidate="Phone" EnableClientScript="true"
				    SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
			</div>
			<div class="ErrorContainer">
			    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" Display="Dynamic"
		                ErrorMessage="Please limit the entry to 50 characters."
		                ValidationExpression="^[\s\S]{0,50}$" ControlToValidate="Phone" SetFocusOnError="true"
		                CssClass="errormsg"></asp:RegularExpressionValidator>
	        </div>
	    </asp:Panel> 	   
		
		<asp:Panel ID="PanelComment" CssClass="CommentContainer Question" runat="server">   
			    <asp:Label ID="QuestionCommentLbl" runat="server" Text="Question / Comment*" 
				AssociatedControlID="QuestionComment"></asp:Label>
			    <asp:TextBox ID="QuestionComment" runat="server" TextMode="MultiLine" />
			<div class="ErrorContainer">
			    <asp:RequiredFieldValidator ID="QuestionCommentValidator" runat="server" 
				ErrorMessage="Please complete: Question / Comment." ControlToValidate="QuestionComment" 
				EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg questioncomment" />
		    </div>
		    <div class="ErrorContainer">
		    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" Display="Dynamic"
		                ErrorMessage="Please limit the entry to 5000 characters."
		                ValidationExpression="^[\s\S]{0,5000}$" ControlToValidate="QuestionComment" SetFocusOnError="true"
		                CssClass="errormsg"></asp:RegularExpressionValidator>
		    </div>
		    <div class="ErrorContainer">
			    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ErrorMessage="The characters '>' and '<' are not permitted."
				    ValidationExpression="^[^<>]+$" ControlToValidate="QuestionComment" EnableClientScript="true"
				    SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
			</div>
		</asp:Panel>
        <div class="seperator png-fix"></div>

</asp:Panel>