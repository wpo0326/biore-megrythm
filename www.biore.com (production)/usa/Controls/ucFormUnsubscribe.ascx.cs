﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KAOForms;

namespace KaoBrands.FormStuff
{
    public partial class ucFormUnsubscribe : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            OptinFormSuccess.Visible = false;
            OptinFormFailure.Visible = false;
            if (Request.QueryString["email"] != null && !Page.IsPostBack)
            {
                Email.Text = Server.UrlDecode(Request.QueryString["email"]);
            }

            if (Page.IsPostBack)
            {
                OptinForm.Visible = false;
                FormSubmitBLL SubmitDAO = new FormSubmitBLL();
                SubmitResultData ResultData = new SubmitResultData();

                //Submit
                ResultData = SubmitDAO.submitUnsubscribeForm(Email.Text);

                if (ResultData.Success)
                {
                    OptinFormSuccess.Visible = true;
                }
                else
                {
                    OptinFormFailure.Visible = true;
                }
            }
        }
    }
}