﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucFormOptin.ascx.cs" Inherits="KaoBrands.FormStuff.ucFormOptin" %>

<script type="text/javascript">
    //var $jq = jQuery.noConflict();
    $(function() {

    $("#<%=banoptin.ClientID%>, #<%=cureloptin.ClientID%>, #<%=jergensoptin.ClientID%>, #<%=bioreoptin.ClientID%>,#<%=johnfriedaoptin.ClientID%>").click(function() {
    if ($('#<%=banoptin.ClientID%>:checked').val()
                  || $('#<%=cureloptin.ClientID%>:checked').val()
                  || $('#<%=jergensoptin.ClientID%>:checked').val()
                  || $('#<%=bioreoptin.ClientID%>:checked').val()
                  || $('#<%=johnfriedaoptin.ClientID%>:checked').val()) {
            $('#<%=MultiOptin.ClientID%>').attr("checked", "true");
            }
            else { $('#<%=MultiOptin.ClientID%>').removeAttr("checked"); }
        });
        $("#<%=MultiOptin.ClientID%>").click(function() {
        if ($('#<%=MultiOptin.ClientID%>:checked').val()) {
            $("#<%=banoptin.ClientID%>").attr("checked", "true");
            $("#<%=cureloptin.ClientID%>").attr("checked", "true");
            $("#<%=jergensoptin.ClientID%>").attr("checked", "true");
            $("#<%=bioreoptin.ClientID%>").attr("checked", "true");
            $("#<%=johnfriedaoptin.ClientID%>").attr("checked", "true");
            } else {
            $("#<%=banoptin.ClientID%>").removeAttr("checked");
            $("#<%=cureloptin.ClientID%>").removeAttr("checked");
            $("#<%=jergensoptin.ClientID%>").removeAttr("checked");
            $("#<%=bioreoptin.ClientID%>").removeAttr("checked");
            $("#<%=johnfriedaoptin.ClientID%>").removeAttr("checked");
            }
        });

        function optinJsValidate(sender, args) {

            $('#<%=siteOptin.ClientID%>').is(':checked') ? args.IsValid = true : args.IsValid = false;
        }
    });
</script>


<div class="OptinContainer">
    <div class="seperator png-fix"></div>
	<p class="privacy"><span>
		Your privacy is important to us. You can trust that Kao Brands Company will use personal information in accordance with our <a href="http://www.kaobrands.com/privacy_policy.asp"
			target="_blank">Privacy Policy</a>.
	</p>
	<div class="CurrentSiteOptinContainer">
	    <ul>
		    <li>
			    <asp:CheckBox Checked="false" ID="siteOptin" runat="server" />
			    <asp:Label ID="OptinLabel" runat="server" Text=""
				    AssociatedControlID="siteOptin" CssClass="siteOptinChkbox" />
			    <div class="ErrorContainer">
			        <asp:CustomValidator ID="siteOptinValidator" Display="Dynamic" runat="server" ErrorMessage="" 
				        ClientValidationFunction="optinJsValidate" OnServerValidate="optinValidate" SetFocusOnError="true"
				        EnableClientScript="false" CssClass="errormsg" Visible="true" />
				</div>
		    </li>
	    </ul>
	</div>
    
    <asp:Panel ID="PanelMultiOptin" CssClass="MultiBrandOptinContainer" runat="server">            
		<ul>            
			<li id="optinToAll">
				<asp:CheckBox ID="MultiOptin" runat="server" CssClass="multiOptin" />
				<asp:Label ID="MultiOptinLabel" runat="server" Text="Yes, I'd like to receive emails and newsletters from other great products from Kao Brands Company:"
					AssociatedControlID="MultiOptin" />
			</li>			
			<li id="banListItem" runat="server">
				<asp:CheckBox ID="banoptin" runat="server" />
				<asp:Label ID="banoptinLabel" runat="server" Text="" AssociatedControlID="banoptin"
					CssClass="brandLabels" />
			</li>
			<li id="bioreListItem" runat="server">
				<asp:CheckBox ID="bioreoptin" runat="server" />
				<asp:Label ID="bioreoptinLabel" runat="server" Text="" AssociatedControlID="bioreoptin"
					CssClass="brandLabels" />
			</li>
			<li id="curelListItem" runat="server">
				<asp:CheckBox ID="cureloptin" runat="server" />
				<asp:Label ID="cureloptinLabel" runat="server" Text="" AssociatedControlID="cureloptin"
					CssClass="brandLabels" />
			</li>
			<li id="jergensListItem" runat="server">
				<asp:CheckBox ID="jergensoptin" runat="server" />
				<asp:Label ID="jergensoptinLabel" runat="server" Text="" AssociatedControlID="jergensoptin"
					CssClass="brandLabels" />
			</li>
			<li id="jfriedaListItem" runat="server">
				<asp:CheckBox ID="johnfriedaoptin" runat="server" />
				<asp:Label ID="johnfriedaoptinLabel" runat="server" Text="" AssociatedControlID="johnfriedaoptin"
					CssClass="brandLabels" />
			</li>
			<li>
				<p class="privDisclaimer">Before submitting your information, please view our <a
					href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">privacy policy</a>.</p>
			</li>
		</ul>
	</asp:Panel>		
					
   
</div> 