﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucFormConfig.ascx.cs" Inherits="KaoBrands.FormStuff.ucFormConfig" %>
<%@ Register TagPrefix="uc1" TagName="ucFormMemberInfo" Src="~/Controls/ucFormMemberInfo.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ucFormOptin" Src="~/Controls/ucFormOptin.ascx" %>



<asp:Panel ID="PageContainer" cssClass="noForm" runat="server">
<div id="contactFormWrap">
    <div id="BrandImageContainer" class="png-fix">
        <asp:Image ID="Image1" runat="server" Visible="false" CssClass="png-fix"/>	
	</div>
	
	<div id="ContactFormContainer">
	     <!-- Header --> 
        <div id="formHeader">
            <h1 id="PageHeader" runat="server">
                <asp:Literal ID="LitHeaderText" runat="server"></asp:Literal>
            </h1> 
        </div>
         
        <!-- Description --> 
        <asp:Panel ID="DescriptionContainer" CssClass="DescriptionContainer png-fix" runat="server"> 
            <asp:Literal ID="LitDescriptionText" runat="server"></asp:Literal>  
        </asp:Panel>     

        
        <!-- From Fields --> 
        <asp:Panel ID="PanelForm" CssClass="FormContainer" runat="server">              
            <p class="req png-fix"><em>Required*</em></p>
            <uc1:ucFormMemberInfo ID="ucFormMemberInfo" runat="server" />                    
            <asp:Panel ID="QuestionPanel" cssClass="QuestionContainer" runat="server">            
            </asp:Panel>
           <uc2:ucFormOptin ID="ucFormOptin" runat="server" />
            <div id="submit-container" class="SubmitContainer png-fix">
                <asp:Button UseSubmitBehavior="true" ID="submit" Text="" runat="server" CssClass="submit buttonLink png-fix" />
            </div>
        </asp:Panel>    
              
        <div class="FormBottom png-fix"></div>
                  
        <!-- Disclaimer --> 
        <div id="DisclaimerContainer" class="png-fix">
            <asp:Literal ID="LitDisclaimer" runat="server"></asp:Literal>  
        </div>
        
        
	</div>

</div>
</asp:Panel>

<script type="text/javascript">
var equalHeight;
var equalHeightElements;
var equalHeightPixels;
<asp:Literal runat="server" id="equalHeightVars"></asp:Literal>
    if(equalHeight==true)
    {
        <asp:Literal runat="server" id="equalHeightFunction"></asp:Literal>
        $(equalHeightElements).equalHeights(equalHeightPixels);
        $(".submit").bind("click", function() {
            
				$(equalHeightElements).equalHeights(equalHeightPixels); 
		});
    }
    
    /***********************************************************************
    * Equal Heights Plugin
    * Equalize the heights of elements. Great for columns or any elements
    * that need to be the same size (floats, etc).
    * 
    * Version 1.0
    * Updated 12/10/2008
    *
    * Copyright (c) 2008 Rob Glazebrook (cssnewbie.com) 
    *
    * Usage: $(object).equalHeights([minHeight], [maxHeight]);
    * 
    * Example 1: $(".cols").equalHeights(); Sets all columns to the same height.
    * Example 2: $(".cols").equalHeights(400); Sets all cols to at least 400px tall.
    * Example 3: $(".cols").equalHeights(100,300); Cols are at least 100 but no more
    * than 300 pixels tall. Elements with too much content will gain a scrollbar.
    * 
    ***********************************************************************/

    (function($) {
        $.fn.equalHeights = function(minHeight, maxHeight) {
            tallest = (minHeight) ? minHeight : 0;
            this.each(function() {
                $(this).height("auto");
            });
            this.each(function() {
                if ($(this).height() > tallest) {
                    tallest = $(this).height();
                }
            });
            if ((maxHeight) && tallest > maxHeight) tallest = maxHeight;
            return this.each(function() {
                $(this).height(tallest); //.css("overflow", "auto");
            });           
           
        }   
  
    })(jQuery);
    


</script>

