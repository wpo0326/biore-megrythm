<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
 <style type="text/css">
        <!--
        a, img, td {
            margin: 0;
            padding: 0;
            border: 0;
        }

        a, object:focus {
            outline: none;
        }

        body {
            /*background-image: url(img/bg.gif);*/
            background-repeat: repeat-x;
            background-color: #212D56;
        }
        -->
        .date{
            color :#d1d1d1;
            font-size: 11px;
            margin: 10px;
            font-family: arial;
        }
    </style>
<title>Bior&eacute;&reg;</title>

<script type="text/javascript" language="JavaScript" src="js/swfobject.js"></script>

 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>

    <script src="//js.maxmind.com/js/apis/geoip2/v2.1/geoip2.js" type="text/javascript"></script>

    <style type="text/css">
        #mainContent {
            display: none;
        }
    </style>

<script type="text/javascript" language="JavaScript">
    var flash1_contentVersion = "6.0.29";
	if (swfobject.hasFlashPlayerVersion(flash1_contentVersion)) {
		//EMBED SWFObject Flash Player
		var flashvars = {};
		var flashparams = {};
		flashparams.menu = "false";
		flashparams.quality = "high";
		flashparams.wmode = "opaque";
		flashparams.bgcolor = "#FFFFFF";
		var flashattributes = {};

		swfobject.embedSWF("biore.swf", "mainDiv", 771, 449, flash1_contentVersion, null, flashvars, flashparams, flashattributes);

	}
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div align="center">
<table  id="mainContent" width="770" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="img/spacer.gif" width="10" height="50"></td>
</tr>
<tr>
<td>
<div id="mainDiv">
<div><img src="img/BioreGlobalLanding-Top.png" alt="Bior&eacute;&reg;" width="771" height="420"></div>
<div>
<img src="img/BioreGlobalLanding-Bottom-1.png" width="171" height="29"><a href="http://www.kao.co.jp/biore/"><img src="img/BioreGlobalLanding-Bottom-2.png" alt="Japan" width="78" height="29"></a><a href="http://www.kao.com.tw/"><img src="img/BioreGlobalLanding-Bottom-3.png" alt="Taiwan" width="87" height="29"></a><a href="http://www.indonesiabiore.com/"><img src="img/BioreGlobalLanding-Bottom-4.png" alt="Indonesia" width="130" height="29"></a><a href="/en-US/"><img src="img/BioreGlobalLanding-Bottom-5.png" alt="USA" width="44" height="29"></a><a href="http://www.biore.ca/"><img src="img/BioreGlobalLanding-Bottom-6.png" alt="Canada" width="65" height="29"></a><a href="http://www.biore.co.uk/"><img src="img/BioreGlobalLanding-Bottom-7.png" alt="UK" width="35" height="29"></a><a href="http://www.biore.com.au/"><img src="img/BioreGlobalLanding-Bottom-8.png" alt="Australia" width="80" height="29"></a><a href="http://www.biore.com.mx/"><img src="img/BioreGlobalLanding-Bottom-9.png" alt="Mexico" width="81" height="29"></a></div>
</div>
</td>
</tr>
<tr>
<td><img src="img/copy.gif" alt="Copyright (c) 1994-2004 KAO CORPORATION. All Rights Reserved." width="305" height="25"></td>
</tr>
<tr>
<td><img src="img/spacer.gif" width="10" height="30"></td>
</tr>
</table>
</div>

 <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        //Biore Tag
        ga('create', 'UA-385129-4', 'auto');
        ga('send', 'pageview');

    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            document.getElementById("year").innerHTML = new Date().getFullYear();
        });


    </script>

    <script type="text/javascript">

        var onSuccess = function (location) {

            if (location.country.iso_code == "US") {

                ga('send', 'event', 'Global Redirect', 'Redirect', 'Global Redirect', '1', { 'nonInteraction': 1 })
                var url = window.location.href.toLowerCase();

                if (url.indexOf("index.html") > -1) {
                    window.location.replace(url.replace("index.html", "en-US"));
                }
                else {
                    window.location.replace(window.location.href + "/en-US");
                }
            }
            else {
                $('#mainContent').css('display', 'block');
                $('body').css('background-image', 'url("img/bg.gif")');
                $('body').css("background-color", "#fff");
            }
        };

        var onError = function (error) {

            $('#mainContent').css('display', 'block');
            $('body').css('background-image', 'url("img/bg.gif")');
            $('body').css("background-color", "#fff");
        };

        if (window.location.href.indexOf("reset=1") < 0) {
            geoip2.country(onSuccess, onError);
        }
        else {
            $('#mainContent').css('display', 'block');
            $('body').css('background-image', 'url("img/bg.gif")');
            $('body').css("background-color", "#fff");
        }

    </script>


</body>
</html>
