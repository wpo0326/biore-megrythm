﻿<%@ Page Title="Frequently Asked Questions" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="megrhythm.frequently_asked_questions.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <meta name="description" content="Answers to Your Questions about MegRhythm products">
    <link rel="stylesheet" type="text/css" href="/Content/nestedAccordion.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="main-container imgback">
        <div class="wrapper internal clearfix">
            <div id="wrapper-body">

                <section>
                    <h1>MEGRHYTHM FREQUENTLY ASKED QUESTIONS (FAQs)  </h1>
                    <!--<p><b>FAQs:  Answers to Your Questions about MegRhythm Products.  Select from the topics below:</p>-->
                </section>

                <div class="main">
                    <ul id="cbp-ntaccordion" class="cbp-ntaccordion">
                        <li>
                            <h4 class="cbp-nttrigger">Is it okay to keep wearing the MegRhythm eye mask while I sleep?</h4>
                            <div class="cbp-ntcontent">
                                <p>Yes; it is okay to fall asleep with the eye mask on. However, the sheet may feel a little stiff after the end of the warming cycle. </p>
                                <!--<ul class="cbp-ntsubaccordion">
                                    <li>
                                        <h4 class="cbp-nttrigger">QUESTION</h4>
                                        <div class="cbp-ntcontent">
                                            <p>ANSWER</p>
                                        </div>
                                    </li> 
                                </ul>-->
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger">Can I use the MegRhythm eye mask in conjunction with a cosmetic eye mask?</h4>
                            <div class="cbp-ntcontent">
                                <p>It is not recommended to use MegRhythm eye mask at the same time as other eye masks. </p>
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger">Can I use the MegRhythm eye mask with make-up on?</h4>
                            <div class="cbp-ntcontent">
                                <p>Yes; you can use the MegRhythm mask with make-up on. However, eye make-up may smudge or rub-off during use. Discontinue use of eye mask immediately if you feel it is too hot or experience other abnormalities.</p>
                            </div>
                        </li>

                        <li>
                            <h4 class="cbp-nttrigger">Can I use the MegRhythm eye mask while wearing false eyelashes, eyelash extensions, mascara, or if I have permed eyelashes or permanent make-up on?</h4>
                            <div class="cbp-ntcontent">

                                <h5>False eyelashes / eyelash extensions</h5>
                                <ul>
                                    <li><p>If you can remove them easily, please remove them before use. </p></li>
                                    <li><p>If you cannot remove them, check care instructions for lashes before using the product. If a salon product, consult the salon where they were fitted to check that steam and heat will not affect the adhesives or curls.</p></li>
                                </ul>
                                <h5>Mascara</h5>
                                <ul>
                                    <li><p>You can use MegRhythm eye mask while wearing mascaras. However, if the mascara is not waterproof, there may be some smudging.</p></li>
                                </ul>
                                <h5>Permed Eyelashes</h5>
                                <ul>
                                    <li><p>You can use MegRhythm eye mask if you have had an eyelash perm. As generally recommended with usage of eye area products after application of eyelash perms, do not use for at least 48 hours after the procedure or for longer if specifically advised by your specialist.  As with all perms, exposure to moisture and humidity may lessen the effect or duration of the perm.</p></li>
                                </ul>
                                <h5>Permanent Make-up</h5>
                                <ul>
                                    <li><p>Since we do not have expertise in this field, consult your cosmetologist before using.</p></li>
                                </ul>

                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger">Can I use more than one MegRhythm eye mask in a single day? </h4>
                            <div class="cbp-ntcontent">
                                <p>The masks are optimized to deliver the warming benefit for approximately 10 minutes.  Other masks may be used at later times in the day if desired. However, discontinue use of eye mask immediately if you feel it is too hot or experience other abnormalities.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger">I’m pregnant; can I still use the MegRhythm eye mask?</h4>
                            <div class="cbp-ntcontent">
                                <p>We recommend consulting your physician prior to using if you are pregnant, recently gave birth, or if you are lactating.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger">The MegRhythm eye mask is puffing up. Is that normal?</h4>
                            <div class="cbp-ntcontent">
                                <p>When steam is generated, it sometimes causes the eye mask to puff up. For example, it is likely to puff up if you keep it folded once removed from the pouch, or if you use it in low pressure circumstances like on an airplane. The mask will not burst and the steam will gradually release.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger">Can I use the MegRhythm eye mask on an airplane?</h4>
                            <div class="cbp-ntcontent">
                                <p>Yes; even though the heat generating unit contains iron powder, it is safe to bring it on-board. Due to low cabin pressure, the mask may puff-up more than usual but it will not burst. We encourage using the mask for in-flight relaxation. </p>
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger">Can I transport the MegRhythm eye mask in my carry-on luggage?</h4>
                            <div class="cbp-ntcontent">
                                <p>Yes; the mask is safe to pack in your carry-on luggage or in your checked luggage. It is not considered a dangerous good.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger">What is the best way to dispose of the MegRhythm eye mask after use? Should I dispose it as a combustible waste or an incombustible waste?</h4>
                            <div class="cbp-ntcontent">
                                <p>Once the heat has dissipated and the eye mask has cooled, please dispose of it in the garbage following the rules in your local area.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger">Is it okay for children to use MegRhythm Eye Mask? </h4>
                            <div class="cbp-ntcontent">
                                <p>MegRhythm Eye Mask can be used by children under adult supervision. Keep out of reach of children when not in use under adult supervision.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger">Which part of the MegRhythm eye mask contains the fragrance? Is there any chance the fragrance can get into my eyes from the steam?</h4>
                            <div class="cbp-ntcontent">
                                <p>The fragrance is located within the fabric cover. It is designed so that the fragrance will not be exposed directly to the skin when steam is generated.  </p>
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger">Is it OK if steam gets in the eyes?  Is it OK if I open my eyes during use?</h4>
                            <div class="cbp-ntcontent">
                                <p>The steam is the same temperature as a tepid bath--around 104°F (40°C). It is OK if steam gets in the eyes, your eyes should remain closed during use. Please ensure that the eye mask itself does not directly touch your eyes. </p>
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger">Can I use MegRhythm eye mask if I have an eye disorder, inflamed eyes or a problem with my eyes?</h4>
                            <div class="cbp-ntcontent">
                                <p>We recommend consulting your physician prior to using the MegRhythm eye mask if you have any eye disorder or other condition. MegRhythm eye mask is not a therapeutic device. Do not use MegRhythm eye mask if you have an eye disorder, symptoms of an eye disorder or inflamed eyes. Examples of eye disorders include but are not limited to glaucoma, cataract, dry eyes, eye floaters, bloodshot eyes, and conjunctivitis (also known as “pink eye”).</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger">Is it okay for me to use MegRhythm eye mask if I had laser eye surgery (LASIK)?</h4>
                            <div class="cbp-ntcontent">
                                <p>We recommend consulting your physician prior to using the MegRhythm eye mask. Consult your doctor if you wish to use while you are still recovering from surgery.</p>
                            </div>
                        </li>
                         <li>
                            <h4 class="cbp-nttrigger">Can I use MegRhythm eye mask while wearing contact lenses?</h4>
                            <div class="cbp-ntcontent">
                                <p>Yes, you can use MegRhythm eye mask while wearing contact lenses. Do not press down on the eyes / eye mask during use. However, discontinue use of the eye mask immediately if you feel it is too hot or experience other abnormalities.</p>
                            </div>
                        </li>
                         <li>
                            <h4 class="cbp-nttrigger"> Can I use MegRhythm eye mask in conjunction with eye medication such as eye drops?</h4>
                            <div class="cbp-ntcontent">
                                
                                <p>If eye drops are applied, wait a few minutes before using eye mask.</p>
                               
                               
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger">Is there any risk that MegRhythm eye mask could cause a low temperature burn? </h4>
                            <div class="cbp-ntcontent">
                                <p>MegRhythm eye mask is designed to emit heat around 104°F (40°C) for approximately 10 minutes and has been tested to be safe. However, the level and duration of perceived heat may vary by individual and usage environment. If you feel it is too hot or experience other abnormalities, discontinue using the mask immediately.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger">My pet swallowed content from the MegRhythm steam mask. Will it be okay?</h4>
                            <div class="cbp-ntcontent">
                                <p>Keep out of the reach of pets. In case of ingestion; consult with your veterinarian immediately. The eye mask can be toxic if ingested and could obstruct the stomach or the bowel, so please consult a vet immediately.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger">What is the MegRhythm steam eye mask?</h4>
                            <div class="cbp-ntcontent">
                                <p>This eye mask gently envelops your eyes and the surrounding areas with steam, pleasantly heated to approximately 104°F (40°C). The steam
 lasts for about 10 minutes. The thin, comfortable mask fits snugly over hardworking eyes. The steam soothes away built-up tension and provides relaxation. However, discontinue use of eye mask immediately if you feel it is too hot or experience other abnormalities. Available in lavender, citrus or unscented.
</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger">What are the benefits of the MegRhythm eye mask?</h4>
                            <div class="cbp-ntcontent">
                                <p>The MegRhythm eye mask promotes gradual relaxation through a gentle warming steam that surrounds the eyes.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger"> It says that the MegRhythm eye mask heats up when removed from pouch, but how long does it take to heat?</h4>
                            <div class="cbp-ntcontent">
                                <p>The eye mask heats within two minutes after it is removed from pouch. Heating time varies slightly depending on the ambient temperature.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="cbp-nttrigger">Where can I buy MegRhythm?</h4>
                            <div class="cbp-ntcontent">
                                <p>MegRhythm is available at amazon.com</p>
                            </div>
                        </li>



                        


 




                        




                        




                        






                        



                        



                        



                        






                    </ul>
                </div>

            </div>
        </div>

    </div>


    <script>
        $(function () {
            /*
            - how to call the plugin:
            $( selector ).cbpNTAccordion( [options] );
            - destroy:
            $( selector ).cbpNTAccordion( 'destroy' );
            */

            $('#cbp-ntaccordion').cbpNTAccordion();

        });
    </script>
</asp:Content>
