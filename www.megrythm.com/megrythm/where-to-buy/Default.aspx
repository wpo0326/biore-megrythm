﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="megrhythm.where_to_buy.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <meta name="description" content="Buy Ban 24 Hour Odor and Wetness Protection roll-ons and invisible solids.  Available online and at most drug stores, grocery stores and key retailers.">
        <link rel="stylesheet" type="text/css" href="/Content/wheretobuy.css">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <div class="main-container imgback">
        <div class="wrapper internal clearfix">
            <div id="wrapper-body">
                
                <section>
                    <h1>Where to Buy</h1>
                    <p></p>
                </section>

                <div id="logoHolder">
                    <asp:ListView runat="server" ID="lvRetailer" OnItemDataBound="lvRetailer_ItemDataBound">
						<LayoutTemplate>
							<ul id="retailLogos">
								<div runat="server" id="itemPlaceholder"></div>
							</ul>

						</LayoutTemplate>

						<ItemTemplate>
							<li class="bubbleInfo">
								<asp:HyperLink runat="server" ID="hlRetailer" class="trigger" />
                                <div id="dpop" class="popup">
        		                	<div id="topleft" class="corner"></div>
        		                    <div id="top" class="top-bottom"></div>
        		                    <div id="topright" class="corner"></div>
                                    <div style="clear:both;"></div>
        		                    <div id="left" class="sides"></div>
        		                    <div id="right" class="sides"></div>
        		                    <div class="popup-contents">
                                    	<div><asp:HyperLink runat="server" ID="locatorLink" Target="_blank" Visible="false" style="text-indent:0px; margin:0px; padding:0px; height:25px; font-weight:normal">Store Locator</asp:HyperLink>
                                        </div>
                                        <div><asp:HyperLink runat="server" ID="onlineLink" Target="_blank" Visible="false" style="text-indent:0px; margin:0px; padding:0px; height:25px; font-weight:normal">Online Store</asp:HyperLink>
                                        </div>
                                    </div>
                                    <div style="clear:both;"></div>
        		                    <div class="corner" id="bottomleft"></div>
        		                    <div id="bottom" class="top-bottom"><img id="ImageBottom" runat="server" width="30" height="29" alt="popup tail" src="/images/where-to-buy/popup/bubble-tail2.png"/></div>
        		                    <div id="bottomright" class="corner"></div>
                                </div>
							</li>


						</ItemTemplate>
					</asp:ListView>

                </div>

            </div>
        </div>

    </div>
            <script src="/Scripts/wheretobuy-popup.js" type="text/javascript"></script>
</asp:Content>
