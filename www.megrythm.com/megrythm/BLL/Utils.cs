﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Collections;
using System.Configuration;
using System.Web.UI.WebControls;

namespace megrhythm.BLL
{
    public class Utils
    {
        public static XmlCDataSection genCData(string textIn)
        {
            XmlDocument doc = new XmlDocument();
            return doc.CreateCDataSection(textIn);
        }
        public static string remCData(XmlCDataSection textIn)
        {

            return textIn.InnerText.ToString();
        }

        public static List<string> parseAppSettings(string keyPattern)
        {
            //Definitely want to cache this
            //We can use arrayList because text and value are the same
            List<string> arr = new List<string>();
            foreach (string appSetting in ConfigurationManager.AppSettings.AllKeys)
            {
                if (appSetting.Contains(keyPattern) == true)
                    arr.Add(ConfigurationManager.AppSettings.Get(appSetting));
            }
            return arr;
        }

        bool TryParseXML(string xml)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(xml);
                xd = null;
                return true;
            }
            catch (XmlException)
            {
                return false;
            }
            catch (Exception)
            {

                return false;
            }

        }

        public void queueNumber(int min, int max, string defaultVal, DropDownList DDL)
        {
            /// <summary>
            /// Method to bind ranged numbers to DropDownLists
            /// </summary>

            bindDDList(defaultVal, "", DDL); //Create default value

            if (DDL.ID.ToString() != "yyyy") //Days/Month increment to max value
            {
                for (int i = min; i < max; i++)
                {
                    bindDDList(i.ToString(), i.ToString(), DDL);
                }
            }
            else
            {
                for (int i = max; i > min; i--) //Year decrements to min value
                {
                    bindDDList(i.ToString(), i.ToString(), DDL);
                }
            }
        }

        public void bindDDList(string text, string value, DropDownList DDL)
        {
            /// <summary>
            /// Method to bind values/numbers to DropDownLists
            /// </summary>
            /// 
            ListItem li = new ListItem();
            li.Text = text;
            li.Value = value;
            DDL.Items.Add(li);
            li = null;
        }


        public bool inBounds(int val, int min, int max)
        {
            /// <summary>
            /// Method to return whether a value to determine bounds
            /// </summary>
            /// 
            bool valid = false;

            if ((val <= max) && (val >= min))
            {
                valid = true;
            }

            return valid;
        }
    }
}