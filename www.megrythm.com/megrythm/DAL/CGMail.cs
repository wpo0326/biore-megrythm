﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


namespace megrhythm.DAL
{
    public class CGMail
    {
        public static string MultiPartMime(String FromEmail, String FromName, String ToEmails,
                                           String ToNames, String Subject, String PlainString, String HtmlString)
        {
            string strReturn = "FALSE";
            //get arrays to determine number of emails to send.
            String[] toNamesArray = ToNames.Split(',');
            String[] toEmailsArray = ToEmails.Split(',');

            try
            {
                //pulls associated names as long as there is an email address
                for (int i = 0; i < toEmailsArray.Length; i++)
                {
                    //create the mail message
                    MailMessage mail = new MailMessage();

                    //build the email body
                    string plainStr;
                    string htmlStr;
                    plainStr = BuildPlainString(toNamesArray[i], FromName, PlainString);
                    htmlStr = BuildHTMLString(toNamesArray[i], FromName, HtmlString);

                    //first we create the Plain Text part
                    AlternateView plainView = AlternateView.CreateAlternateViewFromString(plainStr, null, "text/plain");

                    //then we create HTML part
                    AlternateView htmlView = AlternateView.CreateAlternateViewFromString(htmlStr, null, "text/html");

                    mail.AlternateViews.Add(plainView);
                    //mail.AlternateViews.Add(htmlView);

                    //set the addresses
                    mail.From = new MailAddress(FromEmail, FromName);
                    mail.To.Add(new MailAddress(toEmailsArray[i], toNamesArray[i]));

                    //set the content
                    mail.Subject = Subject;

                    //send the message
                    SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SMTP_Server"], 25);
                    smtp.UseDefaultCredentials = true;

                    smtp.Send(mail);

                    mail.Dispose();
                }

                strReturn = "TRUE";
            }
            catch (Exception ex)
            {
                strReturn = ex.ToString();
            }

            return strReturn;
        }

        public static string MultiPartMimeContact(String UserEmail, String FromEmail, String ToEmails,
                                                  String Subject, String FirstName, String LastName, String Age,
                                                  String Street1, String Street2, String City, String Province,
                                                  String PostalCode, String Country, String Phone, String Comments,
                                                  String UPC_Code, String Mfg_Code, String Website, String OptinFlag)
        {
            string strReturn = "FALSE";

            try
            {
                //create the mail message
                MailMessage mail = new MailMessage();

                //build the email body
                string htmlStr = "";
                string plainStr = "";

                /*
                     // create value-pair listing for CRS- import
                        ContactMessage = "Title:" + Sanitize(Convert.ToInt32(_with3.Form["Title"))] 
                         * + "\r\n" + "First_Name:" + Sanitize(CapFirstLetter[Convert.ToInt32(_with3.Form["FName"))]] 
                         * + "\r\n" + "Last_Name:" + Sanitize(CapFirstLetter[Convert.ToInt32(_with3.Form["LName"))]] 
                         * + "\r\n" + "Address1:" + Sanitize(CapFirstLetter[Convert.ToInt32(_with3.Form["Addr1"))]] 
                         * + "\r\n" + "Address2:" + Sanitize(CapFirstLetter[Convert.ToInt32(_with3.Form["Addr2"))]] 
                         * + "\r\n" + "City:" + Sanitize(CapFirstLetter[Convert.ToInt32(_with3.Form["City"))]] 
                         * + "\r\n" + "State:" + Sanitize(Convert.ToInt32(_with3.Form["RegionID"))]
                         * * + "\r\n" + "Country:" + Sanitize(Convert.ToInt32(_with3.Form["CountryID"])]
                         * * * + "\r\n" + "PostalCode:" + Sanitize(Convert.ToInt32(_with3.Form["PostalCode"])]
                         * * * + "\r\n" + "Email:" + Sanitize(Convert.ToInt32(_with3.Form["Email"])]
                         * * * + "\r\n" + "Age:" + crs_Age + "\r\n";
                         * * * + "\r\n" + "Phone:" + Sanitize(Convert.ToInt32(_with3.Form["Email"])]
                         * * * + "\r\n" + "Opt_In_Out:" + crs_Opt_In_Out
                         * * * + "\r\n" + "Comment:" + Sanitize(Convert.ToInt32(_with3.Form["Email"])] + " ** UPC=" + UPC + " ** " 
                         * * * + "\r\n" + "Website:" + crs_Website
                         * 
                         *  TO: crs_EmailAddress
                         *  FROM: "contact@" + crs_Website
                         *  SUBJECT = "Contact from " + crs_Website
                         *  PLAINTEXT EMAIL!!! 
                    */

                //format fields required in this format per KAO's CRS template
                StringBuilder sr = new StringBuilder();
                sr.Append("<p>Title:</p>");
                sr.Append("<p>First_Name:" + FirstName + "</p>");
                sr.Append("<p>Last_Name:" + LastName + "</p>");
                sr.Append("<p>Address1:" + Street1 + "</p>");
                sr.Append("<p>Address2:" + Street2 + "</p>");
                sr.Append("<p>City:" + City + "</p>");
                sr.Append("<p>State:" + Province + "</p>");
                sr.Append("<p>Country:" + Country + "</p>");
                sr.Append("<p>Zip/Postal_Code:" + PostalCode + "</p>");
                sr.Append("<p>Email:" + UserEmail.ToLower() + "</p>");
                sr.Append("<p>Age:" + Age + "</p>");
                sr.Append("<p>Phone:" + Phone + "</p>");
                sr.Append("<p>Opt_In_Out:" + OptinFlag + "</p>");
                sr.Append("<p>Comment:" + Comments + "</p>");
                sr.Append("<p>UPC:" + UPC_Code + "</p>");
                sr.Append("<p>Mfg_Code:" + Mfg_Code + "</p>");
                sr.Append("<p>Website:" + Website + "</p>");


                htmlStr = sr.ToString();
                plainStr = htmlStr.Replace("<p>", "");
                plainStr = plainStr.Replace("<br />", "\n");
                plainStr = plainStr.Replace("</p>", "\n");

                //create as plain text
                AlternateView plainView = AlternateView.CreateAlternateViewFromString(plainStr, null, "text/plain");
                mail.AlternateViews.Add(plainView);

                //create as HTML
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(htmlStr, null, "text/html");
                mail.AlternateViews.Add(htmlView);

                //set the addresses
                mail.From = new MailAddress(FromEmail);
                mail.To.Add(new MailAddress(ToEmails));
                //send copy to developer to review support issues
                //mail.Bcc.Add(new MailAddress("mtaylor@cgmarcom.com", "Michael Taylor"));
                //add Reply-To per Brian Kaeppner and Ellen Kuntz 01/06/11 -MCT
                //mail.ReplyTo = new MailAddress(UserEmail.ToLower());  //ReplyTo is deprecated in .NET 4.  Use ReplyToList instead -MCT
                mail.ReplyToList.Add(UserEmail.ToLower());

                //set the content
                mail.Subject = Subject;

                //send the message
                //SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SMTP_Server"], 25);
                //smtp.UseDefaultCredentials = true;       

                SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SMTP_Server"]);
                smtp.UseDefaultCredentials = true;
                smtp.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                smtp.PickupDirectoryLocation = "c:\\inetpub\\mailroot\\pickup\\";

                smtp.Send(mail);

                mail.Dispose();

                strReturn = "TRUE";
            }
            catch (Exception ex)
            {
                strReturn = ex.ToString();
            }

            return strReturn;
        }


        private static string BuildPlainString(string ToNameDisplay, string FromName, string PlainStr)
        {
            string strReturn = "";
            strReturn += PlainStr;
            return strReturn;
        }

        private static string BuildHTMLString(string ToNameDisplay, string FromName, string HtmlStr)
        {
            string strReturn = "";

            strReturn = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\"><html><head><style type=\"text/css\">";
            strReturn += "body {font-family:verdana,arial,sans-serif; font-size:11px; color:#333333; text-decoration: none;}";
            strReturn += "a {color:#c68429; text-decoration:underline;} .footer{font-size:9pt;font-family:arial,sans-serif} </style></head>";
            strReturn += "<body>";
            strReturn += HtmlStr;
            strReturn += "</body>";
            strReturn += "</html>";

            return strReturn;
        }


        public static string contactUsConfirmationHTML()
        {
            StringBuilder sbString = new StringBuilder();
            sbString.Append("<p>Thank you for your interest in John Frieda.  Because we look forward to providing you with assistance, a KAO Brands representative will contact you via email within 24-48 hours.</p>");

            return sbString.ToString();
        }

        public static string errorHTML(string Toname, string PageLocation, string PageError)
        {
            StringBuilder sbString = new StringBuilder();
            sbString.Append("Page Location: <br />" + PageLocation + "<br /><br />");
            sbString.Append(PageError);

            return sbString.ToString();
        }
    }
}