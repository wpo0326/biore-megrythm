﻿<%@ Page Title="Privacy Policy" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="megrhythm.privacy.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <meta name="description" content="Privacy policy for Ban 24 Hour Odor and Wetness Protection with roll-ons and invisible solids">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <div class="main-container imgback">
        <div class="wrapper internal clearfix">
            <div id="wrapper-body">
                
                <section>
                    <h1>Kao USA Inc. ("Kao") Privacy Policy</h1>
                    
                    <p>At Kao, we respect your privacy and take reasonable measures to protect your privacy. This policy applies only to the web site on which it appears, and does not apply to any other Kao web sites or services. By accessing or using this web site, you agree and consent to our use of your information as described in this policy.</p>
                <br />
                <h3>EFFECTIVE DATE</h3>
                <p>This privacy policy is effective and was last updated on July 9, 2014.</p>
                <br />
                <h3>WHAT INFORMATION WE COLLECT ABOUT YOU</h3>
                <p><strong>Personally Identifiable Information</strong><br />
                This web site is structured so that, in general, you can visit without revealing any personally identifiable information. Once you choose to provide us with such information, you may be assured that it will only be used in accordance with the principles set forth in this document.
                <br /><br />
                On some Kao web pages, you can interact with entry forms for the purposes of contacting us, subscribing to communications, registering for an account, engaging in promotional events or other purposes. The types of personally identifiable information collected at these pages may include name, mailing address and email address. We may also ask you to voluntarily provide us with information regarding your personal or professional interests, demographics, experience with our products, and contact preferences.
                <br /><br />
                We do not collect any personally identifiable information from you through this web site unless you provide it to us. If you email us or answer questions we have placed on this web site, you are voluntarily releasing that information to us for use as set forth in this policy. Please do not submit personally identifiable information to us if you do not want us to have this information in our database.</p>
                <br />
                <h3>PERFORMANCE INFORMATION </h3>
                <p>Kao may record your interactions with our advertisements, our web sites, emails or other applications we provide using Clickstream Data and Cookies. “Clickstream Data” is a recording of what you click on while browsing this web site. This data can tell us the type of computer and browsing software you use and the address of the web site from which you linked to this web site.
                <br /><br />
                “Cookies” are small text files that are placed on your computer by a web site for the purpose of facilitating and enhancing your communication and interaction with that web site, remembering your preferences and collecting aggregate (i.e., not personally identifiable) information. Many web sites, including ours, use cookies for these purposes.
                <br /><br />
                The web site may also include cookies set by third parties, including Google, which 1) helps us measure the performance of the web site, 2) allows us to share relevant information and advertising with you based on your and other visitors’ past visits to this web site when you surf the web, and 3) helps us measure your interactions with any Kao advertising you see on other sites. Finally, Kao will use data from Google’s Interest-based advertising or third-party audience data, such as age, gender and interests, together with our own data, to deliver relevant advertising to you based upon your demographic profile and interests. Specifically, this web site has implemented the following Google Analytics features to support our advertising across the web: Remarketing (to show you ads on sites across the Internet), Google Display Network Impression Reporting, the DoubleClick Campaign Manager Integration, and Google Analytics Demographics and Interest Reporting.
                <br /><br />
                You can opt-out of Google Analytics for Display Advertising and customize Google Display Network ads by using the Ads Settings, or by using the Google Analytics opt-out Browser add-on. You can find more information about Google Analytics <a href="https://www.google.com/analytics/" target="_blank">here</a>.
                <br /><br />
                You may stop or restrict the placement of cookies on your computer or flush them from your browser by adjusting your web browser preferences and browser plug-in settings, in which case you may still use our web site, but it may interfere with some of its functionality.</p>
                <br />
                <h3>INFORMATION FROM CHILDREN </h3>
                <p>Kao has no intention of collecting personally identifiable information from children under the age of 13. Kao will maintain procedures to assure that personally identifiable information about children under the age of 13 is only collected with explicit consent of such child’s parent or guardian. If a child under the age of 13 has provided Kao with personally identifiable information without such consent, Kao asks that a parent or guardian of the child contact Kao using our Contact Us page to inform us of that fact. Kao will use reasonable efforts to delete such information from our database.</p>
                <br />
                <h3>HOW WE USE YOUR INFORMATION</h3>
                <p>Kao uses your information to understand your needs and provide better products and services. Kao may use data collected from you to personalize your web site experience, tailor future communications, and send you targeted offers as described below. Occasionally, Kao may use your personally identifiable information to contact you for market research or to provide you with marketing information we think would be of particular interest. We will always give you the opportunity to opt out of receiving such contacts.
                <br /><br />
                Kao may share the personally identifiable information you provide with other Kao divisions or affiliates, provided that, such other divisions or affiliates have privacy practices that are similar to those set forth in this policy.
                <br /><br />
                Kao may permit its vendors and subcontractors to access your personally identifiable information, but they are only permitted to do so in connection with services they are performing for Kao. They are not authorized by Kao to use your personally identifiable information for their own benefit. Kao may disclose personally identifiable information as required by law or legal process.
                <br /><br />
                Kao may disclose personally identifiable information to investigate suspected fraud, harassment or other violations of any law, rule or regulation, or the terms or policies for the web site.
                <br /><br />
                In the event of a sale, merger, liquidation, dissolution, reorganization or acquisition of Kao, or a Kao business unit, information Kao has collected about you may be sold or otherwise transferred. However, this will only happen if the party acquiring the information agrees to use personally identifiable information in a manner which is substantially similar to the uses described in this policy.
                <br /><br />
                Sweepstakes, contests and other promotions may set forth additional uses of your personally identifiable information in connection with such promotions.</p>
                <br />
                <h3>THIRD PARTY COLLECTION AND SHARING</h3>
                <p>At times, personally identifiable information may be collected from you on behalf of Kao and a third party who is identified at the time of collection. This may include co-branded promotions. In such instances, your personally identifiable information may be provided to both Kao and such third party. While Kao will use your personally identifiable information as set forth in this policy, such third party will use your personally identifiable information as set forth in their own privacy policy. Such third parties may collect information regarding your activities over time and across different web sites. Therefore, you should review such policies prior to providing your personally identifiable information. Kao is not responsible for the actions of such third parties.</p>
                <br />
                <h3>TARGETED CONTENT AND MESSAGING</h3>
                <p>We believe that content, messages and advertising are more relevant and valuable to you when they are based upon your interests, needs and demographics. Therefore, we may deliver content, messages and advertising specifically to you that are based upon your prior activities on our web sites and information provided to us or gathered as described in this policy. For example, if you have previously expressed an interest in hair care products through your activities on our web site, we may deliver more information to you about hair care products than other products for which you have not expressed an interest or interacted with on the web site. While we may use this information to tailor what we deliver to you, we will still handle and secure your personally identifiable information as set forth in this policy.
                <br /><br />
                Through the use of the Google services (described above), the cookies we place on this web site, and cookies placed by third parties on this web site or through other sites you visit on the internet, we may cause advertisements and content to appear on this web site and elsewhere on the internet based upon your activities over time and across different web sites. In most instances, Kao is simply a content provider and does not directly possess such behavioral information about your online activities. You are able to limit such targeted advertising by setting your browser to block third party cookies or by visiting <a href="http://www.aboutads.info/consumers" target="_blank">www.aboutads.info/consumers</a> to learn more about such advertising practices and to exercise options with respect to such practices at <a href="http://www.aboutads.info/choices" target="_blank">www.aboutads.info/choices</a>.
                <br /><br />
                We do not respond to or honor “do not track” (a/k/a DNT) signals or similar mechanisms transmitted by web browsers.</p>
                <br />
                <h3>YOUR CHOICE</h3>
                <p>Kao will not use or share the information provided to us online in ways unrelated to the ones described in this policy without letting you know and offering you a choice.</p>
                <br />
                <h3>LINKS TO OTHER SITES</h3>
                <p>This web site may contain links to other web sites and services. This can include clicking “Buy Now” or a similar feature to take you to the web sites of various third party retailers where you can browse or purchase Kao products. We do not control such third party web sites and services and are not responsible for their actions or privacy practices. The existence of a link on our web site does not mean that we endorse or have vetted such third party web site or service. You are encouraged to carefully review the policies and practices of every web site and service that you visit.</p>
                <br />
                <h3>SECURITY</h3>
                <p>Kao is committed to taking reasonable steps to ensure the security of the information we collect. To prevent unauthorized access, maintain data accuracy, and ensure the appropriate use of personally identifiable information, we have put in place appropriate physical, electronic, and managerial procedures to safeguard and secure the personally identifiable information we collect online. If we provide you a direct means for maintaining your information in our database, such as through a “profile center” application, you are responsible for taking reasonable precautions to protect your login data, such ID and password. Kao will not be responsible for breach that results from a lost or stolen login.</p>
                <br />
                <h3>NON-CONFIDENTIAL</h3>
                <p>Any communication or material you transmit to us by email or otherwise, including any data, questions, comments, suggestions, or the like is, and will be treated as, non-confidential and nonproprietary. Except to the extent expressly covered by this policy, anything you transmit or post may be used by us for any purpose, including but not limited to, reproduction, disclosure, transmission, publication, broadcast and posting. You expressly agree that we are free to use any ideas, concepts, know-how, or techniques contained in any communication you send to us without compensation and for any purpose whatsoever, including but not limited to, developing, manufacturing and marketing products and services using such information. Furthermore, you confirm that any media files you submit to us (eg. pictures, videos, etc) are your property and you have permission of anyone depicted in these files to transfer rights to Kao.</p>
                <br />
                <h3>ACCESS TO AND CORRECTION OF INFORMATION / OPT-OUT</h3>
                <p>Kao will maintain reasonable procedures for individuals to gain access to their personally identifiable information and preferences. Kao will correct any information that is inaccurate or incomplete or allow you to change your individual consent level. You may do so by <a href="/Contact-Us/">contacting us</a> with the relevant details for your request.
                <br /><br />
                If you wish for us to stop using your personally identifiable information and delete it from our list of active users, please <a href="/Contact-Us/">contact us</a> with the details necessary to complete your request. We will process and honor such requests within a reasonable period of time after receipt. However, even though we may remove your personally identifiable information from our list of active users, we are not responsible for removing your personally identifiable information from the lists of any third party who has been provided your information in accordance with this policy, such as a business partner.
                <br /><br />
                As described above, you are able to opt-out of certain targeted advertising by setting your browser to block third party cookies or by visiting <a href="http://www.aboutads.info/choices" target="_blank">www.aboutads.info/choices</a>.</p>
                <br />
                <h3>DATA RETENTION</h3>
                <p>Except as described in the section above, we may retain and continue to use information collected under this privacy policy for as long as we have a business need for such information.</p>
                <br />
                <h3>COMMENT AND QUESTIONS</h3>
                <p>If you have comments or questions about our privacy policy or your personally identifiable information, please use our <a href="/Contact-Us/">Contact Us</a> page.</p>
                <br />
                <h3>UNITED STATES OF AMERICA</h3>
                <p>This web site and our related databases are maintained in the United States of America. By using the web site, you freely and specifically give us your consent to collect and store, your information in the United States and to use your information as specified within this policy.</p>
                <br />
                <h3>CHANGES TO PRIVACY POLICY</h3>
                <p style="margin-bottom:0">Kao will, and reserves the right to, modify and update this policy or its related business practices at any time at our discretion. Any such material changes will be posted here for reference. However, Kao will not make material changes to this policy, such as how it discloses personally identifiable information, without giving you a chance to opt-out of such differing uses.</p>
            

                 


                </section>

            </div>
        </div>

    </div>
</asp:Content>
