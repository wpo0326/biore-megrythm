﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="megrhythm._Default" %>

<asp:Content runat="server" ID="HeaderContent" ContentPlaceHolderID="HeadContent">
    <meta name="description" content="MegRhythm gentle steam eye mask soothes eyes and promotes relaxation">
</asp:Content>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div class="main-container">
        <div class="main clearfix home">

            <div class="orbit-container clearfix">
                <!--<img src="/img/homepage/slide1.jpg" alt="slide1" />-->
                <!-- Carousel
    ================================================== -->
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <!--<ol class="carousel-indicators hide">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                        
                  </ol>-->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            
                                <div class="container">
                                    <img src="/images/theater1.jpg">
                                    <div class="carousel-caption">
                                        <p class="carousel-title"></p>
                                    </div>
                                </div>
                                <!--<img class="slideimg" src="/images/theater1..jpg" alt="MegRhythm">-->
                          
                        </div>
                        <!--<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>-->
                    </div>
                    <!-- /.carousel -->
                </div>

            </div>

        </div>


    </div>
    <div class="main-container home homerow1">
        <div class="main wrapper clearfix megrhythmcallouts">
            <div class="callout-left">
                <h1 class="offpage">MegRhythm Products</h1>
                <h2>GENTLE STEAM EYE MASK</h2>
                <h3>SOOTHES EYES + PROMOTES RELAXATION.</h3>

                <p>
                    A warm, gentle steam relaxes daily tension
and soothes hard-working eyes. Feel
rejuvenated and rested in just 10 minutes!
Available in Lavender, Citrus and Unscented.
                </p>

                <div class="amazon">
                    <a href="https://www.amazon.com/MegRhythm-Lavender-Gentle-Steam-Count/dp/B074Z5ZSSV?_encoding=UTF8&redirect=true&th=1" target="_blank">
                        <img src="/images/amazon-buy-now.png" alt="Buy MegRhythm products now on Amazon.com" /></a>
                </div>

                <h4>OR watch this video to learn more. <span class="icon-arrow-right2"></span></h4>
            </div>
            <div class="callout-right">
                <div class="animations-container">
                    <!--disabled: fancybox-media fbm  fancybox.iframe"-->
                     <a href="https://www.youtube.com/watch?v=3un-iwDImwc" target="_blank" class="popup-youtube fancybox-medias fbms fancybox.iframes">
                        <img class="gif-holder overwritte-gif" src="/video/thumbnails/KAO_MegRhythm_Snkble_YouTube.jpg" data-origin="/video/thumbnails/KAO_MegRhythm_Snkble_YouTube.jpg">
                    </a>                   
                </div>
            </div>
        </div>
        <div class="main wrapper clearfix">
            <h2>DERMATOLOGIST & OPHTHALMOLOGIST TESTED</h2>
        </div>
        <div class="callouts-container clearfix">

            <div class="callouts">
                <div id="callout1" class="resp fixed">
                    <div class="calloutImg">                     
                            <img src="/images/derm-panel1.jpg" />
                    </div>
                </div>
                <div id="callout2" class="resp fixed">
                    <div class="calloutImg">
                            <img src="/images/derm-panel2.jpg" />
                    </div>
                </div>
                <div id="callout3" class="resp end">
                    <div class="calloutImg">
                            <img src="/images/derm-panel3.jpg" />
                    </div>
                </div>
            </div>
        </div>
        <div class="main wrapper clearfix">
            <h2>AVAILABLE IN LAVENDER, CITRUS AND UNSCENTED</h2>
            <div class="singleImg">
                <img src="/images/megrhythm-group-package.jpg" alt="MegRhythm available in Lavender, citrus and unscented" />
            </div>
        </div>
        <div class="main wrapper clearfix">
            <h2>CONSUMER REVIEWS</h2>
            <h3>There are over 200+ reviews on Amazon with an average rating of 4.4 out of 5 stars!</h3>
        </div>
        <div class="callouts-container clearfix">

            <div class="callouts">
                <div id="ratings1" class="resp fixed">
                    <div class="calloutImg">
                            <img src="/images/ratings1.png" />
                    </div>
                </div>
                <div id="ratings2" class="resp fixed">
                    <div class="calloutImg">
                            <img src="/images/ratings2.png" />
                    </div>
                </div>
                <div id="ratings3" class="resp end">
                    <div class="calloutImg">
                            <img src="/images/ratings3.png" />
                    </div>
                </div>
            </div>
        </div>
        <div class="main wrapper clearfix">
            <h2>USAGE OCCASIONS</h2>
        </div>
        <div class="callouts-container clearfix">

            <div class="callouts">
                <div id="usage1" class="resp fixed">
                    <div class="calloutImg">
                     
                            <img src="/images/usage1.jpg" /><div>WORK</div>
                    </div>
                </div>
                <div id="usage2" class="resp fixed">
                    <div class="calloutImg">
                       
                            <img src="/images/usage2.jpg" /><div>TRAVEL</div>
                    </div>
                </div>
                <div id="usage3" class="resp end">
                    <div class="calloutImg">
                        
                            <img src="/images/usage3.jpg" /><div>RELAXATION / NIGHT</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main wrapper clearfix">
            <div class="pinkBg">
                <div class="pinkContainer" id="howitworks">
                    <div class="clearfix">
                        <div class="heatPad">
                            <img src="/images/heating-pad.png" alt="Heating Pad" />
                        </div>
                        <div class="howitworks clearfix">
                            <h2>HOW IT WORKS</h2>
                            <p>
                                Steam Eye Mask is lined
                        with a heat-generating pad
                        containing iron powder
                        and moisture. Once the pack is opened, the heat-­generating pad comes into contact with oxygen in the air, oxidizing the iron powder and producing heat. Steam is produced when the moisture is being evaporated by the heat.
                            </p>
                        </div>
                    </div>
                    <div id="3comfortfeatures">
                        <h2>3 COMFORT FEATURES OF THE MEGRHYTHM STEAM EYE MASK</h2>

                        <div class="whiteRadius clearfix">
                            <div class="heatBullet">1</div>
                            <div class="heatText">
                                <div class="heatImgRight">
                                    <img src="/images/steam-glass.png" alt="comfortable steam heat">
                                </div>
                                <div class="heatTxtLeft"><h4>WARM STEAM</h4>
                                Steam produced from the comfortable heat (104ºF)
moistens and soothes hardworking eyes. Enjoy the steam bath!</div>
                            </div>
                        </div>
                        <div class="whiteRadius clearfix">
                            <div class="heatBullet">2</div>
                            <div class="heatText">
                                <h4>WARMS UP IMMEDIATELY</h4>
                                Using a unique technology, the eye mask warms up immediately upon
opening of pouch. Besides using it before bedtime, it is also perfect for a quick
break in between work.
                                <div class="heatImgCenter"><img src="/images/heat-map-timing.png" alt="eye mask warms up immediately" /></div>
                            </div>
                        </div>
                        <div class="whiteRadius clearfix">
                            <div class="heatBullet">3</div>
                            <div class="heatText">
                                <div class="heatImgRight">
                                    <img src="/images/convenience.png" alt="ear strap design for comfort + convenience">
                                </div>
                                <div class="heatTxtLeft"><h4>SPECIAL EAR STRAP DESIGN FOR COMFORT + CONVENIENCE</h4>
                                Whether you are lying down or sitting up, the ear straps prevent
slipping and sliding, giving you comfort in any position. The eye mask
is designed to conform to your eye contours, providing maximum
coverage for your soothing experience.</div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

        <div class="main wrapper clearfix">
            <h2 id="howtouse">HOW TO USE</h2>
        </div>
        <div class="main wrapper clearfix">
            <div class="callout-left">
                <img src="/images/instructions.png" alt="Instructions for use" class="instructionsImg" />
            </div>
            <div class="callout-right">
                <div class="instructionsTxt">
                    <p>Please read complete instructions and warnings before use.</p>
                    <p>Do not use with other eye masks</p>
                    <p>
                        If eye drops are applied, please wait a
            few minutes before using eye mask
                    </p>
                    <p>
                        Eye mask may cause smudging or
            removal of eye makeup
                    </p>
                    <p>Heat will last approximately 10 minutes</p>
                </div>
            </div>
        </div>
         <div class="main wrapper clearfix">&nbsp;</div>
    </div>


</asp:Content>
