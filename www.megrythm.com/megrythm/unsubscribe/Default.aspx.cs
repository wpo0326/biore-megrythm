﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using megrhythm.DAL;

namespace megrhythm.unsubscribe
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            OptinFormSuccess.Visible = false;
            OptinFormFailure.Visible = false;

            int intSiteID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SiteID"]);
            string strIPAddr = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            string strBrowserInfo = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];
            string strURL = HttpContext.Current.Request.ServerVariables["URL"];

            string strResultData = "";

            if (Request.QueryString["email"] != null)
            {
                Email.Text = Server.UrlDecode(Request.QueryString["email"]);
            }

            if (IsPostBack)
            {
                OptinForm.Visible = false;

                //Submit
                strResultData = CGOptin.ProcessOptOut(intSiteID, strIPAddr, strBrowserInfo, Email.Text, strURL);

                if (strResultData == "TRUE")
                {
                    OptinFormSuccess.Visible = true;
                }
                else
                {
                    OptinFormFailure.Visible = true;
                }
            }
        }
    }
}