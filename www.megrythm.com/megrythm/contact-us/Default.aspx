﻿<%@ Page Title="Contact Us" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="megrhythm.Contact_Us.Default" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">

    <div class="main-container imgback">
        <div class="wrapper internal clearfix">
            <div id="wrapper-body">
                
                <section>
                    <h1>Contact Us</h1>

                    <div class="contactText">
                            <h2>By Mail</h2>

                            <p>Consumer Care Dept.<br />
                            Kao USA Inc.<br />
                            2535 Spring Grove Ave<br />
                            Cincinnati, OH 45214</p>
                            

                            <h2>By Phone</h2>

                            <p>If you're calling from the United States, Canada or Puerto Rico, you can call us toll free at 1-866-537-4367, between 9am - 5pm (EST).</p>
                            <h2>Online</h2>
                   
                            <p>Fill out the form <span class="contactDirection"></span>.</p>
                     </div>

                    <asp:Panel ID="ContactFormPanel" runat="server">
                        <div id="contactFormLeftContainer">
                            <div class="leftField">
                                <div class="mmSweet">
                                    <asp:Label ID="lblFirstName" runat="server" AssociatedControlID="FirstName">First Name*:</asp:Label>
                                    <asp:TextBox ID="FirstName" runat="server" CssClass="textboxes"></asp:TextBox>
                                </div>

                                <asp:Label ID="lblFName" runat="server" AssociatedControlID="FName">First Name*:</asp:Label>
                                <asp:RequiredFieldValidator ForeColor="Red" ID="rf_FName" runat="server" ErrorMessage="required<br />" Display="Dynamic" ControlToValidate="FName"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="FName" runat="server" CssClass="textboxes" required="required" MaxLength="50"></asp:TextBox>
                            </div>

                            <div class="rightField">
                                <asp:Label ID="lblLName" runat="server" AssociatedControlID="LName">Last Name*:</asp:Label>
                                <asp:RequiredFieldValidator ForeColor="Red" ID="rf_LName" runat="server" ErrorMessage="required<br />" Display="Dynamic" ControlToValidate="LName"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="LName" runat="server" CssClass="textboxes" required="required" MaxLength="50"></asp:TextBox>

                            </div>

                            <div class="clearfix"></div>

                            <div class="leftField">
                                <asp:Label ID="lblEmail" runat="server" AssociatedControlID="Email">Email Address*:</asp:Label>
                                <asp:RequiredFieldValidator ForeColor="Red" ID="rf_email" runat="server" ErrorMessage="required<br />" Display="Dynamic" ControlToValidate="Email"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="rf_email2" runat="server" Display="Dynamic"
                                    ErrorMessage="<br />Please enter a valid Email Address." ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                    ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="Email" runat="server" CssClass="textboxes" required="required" MaxLength="150"></asp:TextBox>

                            </div>

                            <div class="rightField">
                                <asp:Label ID="RetypeEmailLbl" runat="server" Text="Re-type Email*" AssociatedControlID="RetypeEmail"></asp:Label>
                                <asp:RequiredFieldValidator ID="RetypeValidator1" runat="server" ErrorMessage="Please re-type your Email Address.<br />"
								ControlToValidate="RetypeEmail" EnableClientScript="true" SetFocusOnError="true"
								Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
							<asp:CompareValidator ID="RetypeValidator2" Text="Make sure you retyped the Email correctly.<br />"
								ControlToValidate="RetypeEmail" ControlToCompare="Email" EnableClientScript="true"
								SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" runat="server" />
							<asp:TextBox ID="RetypeEmail" runat="server" required="required" MaxLength="150" CssClass="textboxes"></asp:TextBox>
							

                            </div>

                            <div class="clearfix"></div>

                            <div class="leftField">
                                <asp:Label ID="Address1Lbl" runat="server" Text="Address 1*" AssociatedControlID="Address1"></asp:Label>
                                <asp:RequiredFieldValidator ID="Address1Validator1" runat="server" ErrorMessage="Please enter your Address.<br />"
								ControlToValidate="Address1" EnableClientScript="true" SetFocusOnError="true"
								Display="Dynamic" CssClass="errormsg" />
							<asp:RegularExpressionValidator ID="Address1Validator2" runat="server" ErrorMessage="Please do not use special characters in the Address 1 field.<br />"
								ValidationExpression="^[^<>]+$" ControlToValidate="Address1" EnableClientScript="true"
								SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
							<asp:TextBox ID="Address1" runat="server" required="required" MaxLength="50" CssClass="textboxes"></asp:TextBox>
							
                            </div>

                            <div class="rightField">
                                <asp:Label ID="Address2Lbl" runat="server" Text="Address 2" AssociatedControlID="Address2"></asp:Label>
                                <asp:RegularExpressionValidator ID="Address2Validator1" runat="server" ErrorMessage="Please do not use special characters in the Address 2 field.<br />"
								ValidationExpression="^[^<>]+$" ControlToValidate="Address2" EnableClientScript="true"
								SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
							<asp:TextBox ID="Address2" runat="server" MaxLength="50" CssClass="textboxes"></asp:TextBox>
							

                            </div>

                            <div class="clearfix"></div>

                            <div class="leftField">
                                <asp:Label ID="CityLbl" runat="server" Text="City*" AssociatedControlID="City"></asp:Label>
                                <asp:RequiredFieldValidator ID="CityValidator1" runat="server" ErrorMessage="Please enter your City.<br />"
								ControlToValidate="City" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
							<asp:RegularExpressionValidator ID="CityValidator2" runat="server" ErrorMessage="Please do not use special characters in the City field.<br />"
								ValidationExpression="^[^<>]+$" ControlToValidate="City" EnableClientScript="true"
								SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
							<asp:TextBox ID="City" runat="server" required="required" MaxLength="50" CssClass="textboxes"></asp:TextBox>
							
                            </div>

                            <div class="rightField">
                                <asp:Label ID="StateLbl" runat="server" Text="State*" AssociatedControlID="State"></asp:Label>
                                <asp:RequiredFieldValidator ID="StateValidator" runat="server" ErrorMessage="Please select your State.<br />"
								ControlToValidate="State" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
										    <asp:DropDownList ID="State" runat="server">
				    <asp:ListItem Value="" Selected="true">Select State</asp:ListItem>
				    <asp:ListItem Value="AL">Alabama</asp:ListItem>
				    <asp:ListItem Value="AK">Alaska</asp:ListItem>
				    <asp:ListItem Value="AZ">Arizona</asp:ListItem>
				    <asp:ListItem Value="AR">Arkansas</asp:ListItem>
				    <asp:ListItem Value="CA">California</asp:ListItem>
				    <asp:ListItem Value="CO">Colorado</asp:ListItem>
				    <asp:ListItem Value="CT">Connecticut</asp:ListItem>
				    <asp:ListItem Value="DE">Delaware</asp:ListItem>
				    <asp:ListItem Value="DC">District Of Columbia</asp:ListItem>
				    <asp:ListItem Value="FL">Florida</asp:ListItem>
				    <asp:ListItem Value="GA">Georgia</asp:ListItem>
				    <asp:ListItem Value="HI">Hawaii</asp:ListItem>
				    <asp:ListItem Value="ID">Idaho</asp:ListItem>
				    <asp:ListItem Value="IL">Illinois</asp:ListItem>
				    <asp:ListItem Value="IN">Indiana</asp:ListItem>
				    <asp:ListItem Value="IA">Iowa</asp:ListItem>
				    <asp:ListItem Value="KS">Kansas</asp:ListItem>
				    <asp:ListItem Value="KY">Kentucky</asp:ListItem>
				    <asp:ListItem Value="LA">Louisiana</asp:ListItem>
				    <asp:ListItem Value="ME">Maine</asp:ListItem>
				    <asp:ListItem Value="MD">Maryland</asp:ListItem>
				    <asp:ListItem Value="MA">Massachusetts</asp:ListItem>
				    <asp:ListItem Value="MI">Michigan</asp:ListItem>
				    <asp:ListItem Value="MN">Minnesota</asp:ListItem>
				    <asp:ListItem Value="MS">Mississippi</asp:ListItem>
				    <asp:ListItem Value="MO">Missouri</asp:ListItem>
				    <asp:ListItem Value="MT">Montana</asp:ListItem>
				    <asp:ListItem Value="NE">Nebraska</asp:ListItem>
				    <asp:ListItem Value="NV">Nevada</asp:ListItem>
				    <asp:ListItem Value="NH">New Hampshire</asp:ListItem>
				    <asp:ListItem Value="NJ">New Jersey</asp:ListItem>
				    <asp:ListItem Value="NM">New Mexico</asp:ListItem>
				    <asp:ListItem Value="NY">New York</asp:ListItem>
				    <asp:ListItem Value="NC">North Carolina</asp:ListItem>
				    <asp:ListItem Value="ND">North Dakota</asp:ListItem>
				    <asp:ListItem Value="OH">Ohio</asp:ListItem>
				    <asp:ListItem Value="OK">Oklahoma</asp:ListItem>
				    <asp:ListItem Value="OR">Oregon</asp:ListItem>
				    <asp:ListItem Value="PA">Pennsylvania</asp:ListItem>
				    <asp:ListItem Value="RI">Rhode Island</asp:ListItem>
				    <asp:ListItem Value="SC">South Carolina</asp:ListItem>
				    <asp:ListItem Value="SD">South Dakota</asp:ListItem>
				    <asp:ListItem Value="TN">Tennessee</asp:ListItem>
				    <asp:ListItem Value="TX">Texas</asp:ListItem>
				    <asp:ListItem Value="UT">Utah</asp:ListItem>
				    <asp:ListItem Value="VT">Vermont</asp:ListItem>
				    <asp:ListItem Value="VA">Virginia</asp:ListItem>
				    <asp:ListItem Value="WA">Washington</asp:ListItem>
				    <asp:ListItem Value="WV">West Virginia</asp:ListItem>
				    <asp:ListItem Value="WI">Wisconsin</asp:ListItem>
				    <asp:ListItem Value="WY">Wyoming</asp:ListItem>
			    </asp:DropDownList>

							
                            </div>

                            <div class="clearfix"></div>

                             <div class="leftField">
                                 <div class="localeContainer">
                                    <asp:Label ID="PostalCodeLbl" runat="server" Text="Postal Code*" AssociatedControlID="PostalCode"></asp:Label>
                                     <asp:RequiredFieldValidator ID="PostalCodeValidator1" runat="server" ErrorMessage="Please enter your Postal Code.<br />"
								    ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true"
								    Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
							    <asp:TextBox ID="PostalCode" runat="server" required="required" MaxLength="20" CssClass="textboxes"></asp:TextBox>
							    
							    <%-- The following RegEx Validators is for US Zip/Postal codes.  IMPORTANT: 
								     Please test against your user base before using as you may need to modify it for other formats!  --%>
							    <%--<asp:RegularExpressionValidator ID="PostalCodeValidator2" runat="server" 
								    ErrorMessage="Please enter a valid 5-digit ZIP code." ValidationExpression="^\d{5}(-\d{4})$" 
								    ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true" 
								    Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>--%>
                                </div>
                             </div>

                            <div class="rightField">
                                <asp:Label ID="lblPhone" runat="server" AssociatedControlID="Phone">Phone*</asp:Label>
                                <asp:RequiredFieldValidator ForeColor="Red" ID="rf_phone" runat="server" ErrorMessage="required<br />" Display="Dynamic" ControlToValidate="Phone"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ForeColor="Red" ID="re_phone" ControlToValidate="Phone" runat="server" ErrorMessage="Valid phone required.<br />" Display="Dynamic" ValidationExpression="^\(?\d{3}\)?[- ]?\d{3}[- ]?\d{4}$"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="Phone" runat="server" CssClass="textboxes" required="required" MaxLength="14"></asp:TextBox>
                            </div>

                            <div class="clearfix"></div>

                            <div class="leftField">
                                <asp:Label ID="GenderLbl" runat="server" Text="Gender*" AssociatedControlID="Gender"></asp:Label>
                                <asp:RequiredFieldValidator ID="GenderValidator" runat="server" ErrorMessage="Please enter your Gender.<br />"
								ControlToValidate="Gender" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
							<asp:DropDownList ID="Gender" runat="server">
								<asp:ListItem Value="">Select</asp:ListItem>
								<asp:ListItem Value="F">Female</asp:ListItem>
								<asp:ListItem Value="M">Male</asp:ListItem>
							</asp:DropDownList>
							
                                
                            </div>
                           
                            <div class="rightField">
                                <asp:Label ID="DOBLbl" runat="server" Text="Birthdate*" AssociatedControlID="yyyy"></asp:Label>
                                <asp:CustomValidator ID="DOBValidator" runat="server" ErrorMessage="Please enter a valid date.<br />"
								OnServerValidate="DOBValidate" EnableClientScript="false" Display="Dynamic" CssClass="errormsg validdate"></asp:CustomValidator>
							<asp:RequiredFieldValidator ID="yyyyValidator" runat="server" ErrorMessage="Please enter your Birth Year.<br />"
								ControlToValidate="yyyy" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg year"></asp:RequiredFieldValidator>
							<asp:RequiredFieldValidator ID="mmValidator" runat="server" ErrorMessage="Please enter your Birth Month.<br />"
								ControlToValidate="mm" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg month"></asp:RequiredFieldValidator>
							<asp:RequiredFieldValidator ID="ddValidator" runat="server" ErrorMessage="Please enter your Birth Day.<br />"
								ControlToValidate="dd" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg day"></asp:RequiredFieldValidator>
							<asp:DropDownList ID="yyyy" runat="server" CssClass="inputSelectBDay"></asp:DropDownList>
							<asp:DropDownList ID="mm" runat="server" CssClass="inputSelectBDay"></asp:DropDownList>
							<asp:DropDownList ID="dd" runat="server" CssClass="inputSelectBDay"></asp:DropDownList>
							
                            </div>

                             <div class="clearfix"></div>

                            <div class="leftField">
                                <div class="UPCCodeContainer">
                                 <div class="help-tip">
	                                <p>You will find the UPC code on the back of the product.  The code will appear beneath the bar code, as pictured here.<br />
                                    <img src="/images/forms/upc_ex.jpg" alt="UPC Code" title="UPC Code" /></p>
                                </div>
                                    <asp:Label ID="UPCCodeLbl" runat="server" Text="Product UPC Code" AssociatedControlID="UPC_Code"></asp:Label>
                                    <div class="ErrorContainer">
		                                <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server" Display="Dynamic"
		                                        ErrorMessage="Please limit the entry to 12 characters, no spaces or dashes<br />"
		                                        ValidationExpression="^[\s\S]{0,12}$" ControlToValidate="UPC_Code" SetFocusOnError="true"
		                                        CssClass="errormsg"></asp:RegularExpressionValidator>

                                        <asp:RegularExpressionValidator ID="re_UPC" runat="server" Display="Dynamic"
		                                    ErrorMessage="Please do not enter spaces, dashes or special characters.<br />"
		                                    ValidationExpression="^[a-zA-Z0-9]{0,12}$" ControlToValidate="UPC_Code" SetFocusOnError="true"
		                                    CssClass="errormsg"></asp:RegularExpressionValidator>
		                            </div>
	                                <asp:TextBox ID="UPC_Code" runat="server" MaxLength="12" CssClass="inputTextBox"></asp:TextBox>
                                    
                                 </div>
                            </div>

                            <div class="rightField">
                                <div class="MFGCodeContainer">
                                    <div class="help-tip">
	                                    <p>The MFG code can typically be found stamped on the bottom of the package.  It may also appear in the crimped edge of tube-based products.<br />
                                        <img src="/images/forms/mfg_ex.jpg" alt="MFG Code" title="MFG Code" /></p>
                                    </div>
                <asp:Label ID="MFGLbl" runat="server" Text="Product MFG Code" AssociatedControlID="Mfg_Code"></asp:Label>
                                    <div class="ErrorContainer">
		            <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server" Display="Dynamic"
		                    ErrorMessage="Please limit the entry to 12 characters.<br />"
		                    ValidationExpression="^[\s\S]{0,12}$" ControlToValidate="Mfg_Code" SetFocusOnError="true"
		                    CssClass="errormsg"></asp:RegularExpressionValidator>

                    <asp:RegularExpressionValidator ID="re_MFG" runat="server" Display="Dynamic"
		                    ErrorMessage="Please do not enter spaces, dashes or special characters.<br />"
		                    ValidationExpression="^[a-zA-Z0-9]{0,12}$" ControlToValidate="Mfg_Code" SetFocusOnError="true"
		                    CssClass="errormsg"></asp:RegularExpressionValidator>
                                </div>
	            <asp:TextBox ID="Mfg_Code" runat="server" MaxLength="12" CssClass="inputTextBox"></asp:TextBox>
                
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div id="contactFormRightContainer">

                            <asp:Label ID="lblQuestionComment" runat="server" AssociatedControlID="QuestionComment">Comments*:</asp:Label>
                            <asp:RequiredFieldValidator ID="rfQuestionComment" runat="server" ErrorMessage="Please enter your comment/question.<br />"
								ControlToValidate="QuestionComment" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="QuestionComment" runat="server" TextMode="MultiLine"></asp:TextBox>

                            <asp:HiddenField ID="hfHookID" runat="server" />
                           
                            <asp:Button ID="submitform" runat="server" Text="Submit Form" CssClass="applyNow" OnClick="Contact_Click" />
                            <div id="process_message" runat="server">
                                <p><em><strong>Processing your submission...</strong></em></p>
                            </div>

                            <br class="clearBoth" />
                            <br />

                        </div>
                        <br class="clearBoth" />
                    </asp:Panel>
                    <asp:Panel ID="ContactFormThanks" runat="server">

                        <h1>Thank you!  Your message has been sent.  Someone will be contacting you shortly.</h1>

                    </asp:Panel>
                    <asp:Panel ID="ContactFormError" CssClass="shortformContent" runat="server">
                        <asp:Literal ID="litError" runat="server"></asp:Literal>
                    </asp:Panel>


                </section>

            </div>
        </div>

    </div>

       <script type="text/javascript">
           $(document).ready(function () {
               $('.help-tip').click(function () {
                   if ($(this).find('p').hasClass('helpTipOn')) {
                       $(this).find('p').removeClass('helpTipOn');
                   }
                   else {
                       $(this).find('p').addClass('helpTipOn');

                   }
                   return false;
               });
           });
    </script>
</asp:Content>

