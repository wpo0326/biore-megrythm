﻿$(function () {
    var windowSize = $(window).width();
    var mobileSize = 768;
    var playBtn = $('.animation-play-btn');
    var playButtonSize = playBtn.css('width');
    $(window).resize(function () {
        playBtn.attr('style', '');
        playButtonSize = playBtn.css('width');
    });
    function isMobile() {
        if (navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)
        ) {
            return true;
        }
        else {
            return false;
        }
    }

    $(window).resize(function () {
        var newSize = $(window).width();
        windowSize = newSize;

        removeLightBox();

    });

    function removeLightBox() {

        if (windowSize > mobileSize) {
            if (!$('.fbm').hasClass('fancybox-media')) {
                $('.fbm').addClass('fancybox-media');
            }
        } else {
            if ($('.fbm').hasClass('fancybox-media')) {
                $('.fbm').removeClass('fancybox-media');
            }
        }
    }

    //scroll to top
    // browser window scroll (in pixels) after which the "back to top" link is shown
    var offset = 600,
        //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
        offset_opacity = 1500,
        //duration of the top scrolling animation (in ms)
        scroll_top_duration = 700,
        //grab the "back to top" link
        $back_to_top = $('.cd-top');

    //hide or show the "back to top" link
    $(window).scroll(function () {
        ($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if ($(this).scrollTop() > offset_opacity) {
            $back_to_top.addClass('cd-fade-out');
        }
    });

    //smooth scroll to top
    $back_to_top.on('click', function (event) {
        event.preventDefault();
        $('body,html').animate({
            scrollTop: 0,
        }, scroll_top_duration
        );
    });

    // SMOOTH SCROLLING: Select all links with hashes
    $('a[href*="#"]')
      // Remove links that don't actually link to anything
      .not('[href="#"]')
      .not('[href="#0"]')
      .click(function (event) {
          // On-page links
          if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
            &&
            location.hostname == this.hostname
          ) {
              // Figure out element to scroll to
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
              // Does a scroll target exist?
              if (target.length) {
                  // Only prevent default if animation will happen
                  event.preventDefault();
                  $('html, body').animate({
                      scrollTop: target.offset().top
                  }, 1000, function () {
                      // Callback after animation
                      // Must change focus!
                      var $target = $(target);
                      $target.focus();
                      if ($target.is(":focus")) { // Checking if the target was focused
                          return false;
                      } else {
                          $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                          $target.focus(); // Set focus again
                      };
                  });
              }
          }
      });
});