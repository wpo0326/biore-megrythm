﻿<%@ Page Title="Page Not Found | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012._404._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Bioré® Skincare – Page Not Found" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <div id="content">
                    <h1>404 Page Not Found</h1>
                    <p>We're sorry. The page you requested cannot be found or there was an error retrieving it.</p>
                    <div class="promoButton"><a href="http://www.biore.fr">Home</a></div>
                 </div>
                 <div id="photo">
                    <img src="<%= System.Configuration.ConfigurationManager.AppSettings["path"] %>images/utilityPages/polaroid.jpg" alt="" />
                 </div>
                 <div class="clear"></div>
            </div>
        </div>
    </div>
</asp:Content>