﻿<%@ Page Title="" Language="C#" MasterPageFile="~/facebook/FBContentPageMaster.Master" AutoEventWireup="true"
	CodeBehind="Default.aspx.cs" Inherits="Biore2012.facebook.AlloysDatingRules.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<meta property="fb:admins" content="1064275431,25700203,1584985879"/>	
	<meta property="fb:app_id" content="<%= System.Configuration.ConfigurationManager.AppSettings["datingrulesFBAppId"] %>"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div id="fb-root">
	</div>

	<script type="text/javascript">
		var extJsHost_connect_facebook_net = (("https:" == document.location.protocol) ? "https://" : "http://");
		window.fbAsyncInit = function() {
			FB.init({
				appId: '<%= System.Configuration.ConfigurationManager.AppSettings["datingrulesFBAppId"] %>', // App ID
				//channelURL : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
				status: true, // check login status
				cookie: true, // enable cookies to allow the server to access the session
				oauth: true, // enable OAuth 2.0
				xfbml: true  // parse XFBML
			});

			// Additional initialization code here
			FB.Canvas.setAutoGrow();
			FB.Canvas.scrollTo(0, 0);

			checkForAdr = (function() {
				if (typeof adr !== "undefined") {
					adr.getFBComments();
				} else {
					setTimeout(checkForAdr, 100);
				}
			}());
		};

		// Load the SDK Asynchronously
		(function(d) {
			var js, id = 'facebook-jssdk'; if (d.getElementById(id)) { return; }
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = extJsHost_connect_facebook_net + "connect.facebook.net/en_US/all.js";
			d.getElementsByTagName('head')[0].appendChild(js);
		} (document));
	</script>

	<script>		!function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = "//platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } } (document, "script", "twitter-wjs");</script>

	<div class="datingRules videoFeedsPg">
		<div id="topNav">
			<a href="Default.aspx" class="btn videoFeeds current">Videos and Feeds</a>
			<a href="TopTenDatingRules.aspx" class="btn topTen">Top Ten Dating Rules</a>
		</div>
		<div id="top">
			<h1 class="ir">Who doesn't need dating (and skincare) tips?</h1>
			<h2 class="ir">Get them both with Alloy's Dating Rules from my Future Self featuring Bior&eacute;&reg; Skincare
				products!</h2>
			<a href="<%= System.Configuration.ConfigurationManager.AppSettings["path"] %>charcoal-for-oily-skin/combination-skin-balancing-cleanser"
				target="_blank" class="productLink">
				<p id="cleanerCopy">It can be tough to balance work, school, friends and love. But balancing your skin should
					be simple.<br />
					Try <span><span>NEW</span> Combination Skin Balancing Cleanser</span>.</p>
			</a>
			<p><strong>Series premieres January 9th.</strong> Watch new episodes every Monday, Wednesday and Friday
				at <a href="http://facebook.com/DatingRules" target="_top">Facebook.com/DatingRules</a>.</p>
		</div>
		<div id="vidHolder">
			<iframe id="btsInterview" width="448" height="259" src="http://www.youtube.com/embed/OeAcV-86BwM" frameborder="0"
				allowfullscreen></iframe>
		</div>
		<div id="vidOptions">
			<p>Click below to watch full episodes and exclusive behind the scenes footage with the stars of the show.
				Watch the entire series at <a href="http://facebook.com/DatingRules" target="_top">Facebook.com/DatingRules</a>.</p>
			<p id="btnHolder"><a href="#" id="whatsNewLnk" class="tabBtn selected ir">What's New</a> <a href="#"
				id="fullEpisodesLnk" class="tabBtn ir">Full Episodes</a> <a href="#" id="behindScenesLnk" class="tabBtn ir">
					Behind the Scenes</a> </p>
			<div id="videoHolders">
				<div class="videoLnkHolder" id="whatsNewVids">
					<div class="vidLnkCarousel">
						<ul>
							<li><a href="http://www.youtube.com/embed/OeAcV-86BwM" id="wn7" class="vidLink selected ir">Episode 7: "Keep Calm and Carry On"<span class="infoArrow"></span></a></li>
							<li><a href="http://www.youtube.com/embed/Gw7HaGkglA8" id="wn6" class="vidLink ir">Face to Face Fresh Exclusive: #2<span class="infoArrow"></span></a></li>
							<li><a href="http://www.youtube.com/embed/hILGFpOwQS8" id="wn5" class="vidLink ir">Episode 4: "Heal Yourself"<span class="infoArrow"></span></a></li>
							<li><a href="http://www.youtube.com/embed/S0QzC3gj0gA" id="wn4" class="vidLink ir">Face to Face Fresh Exclusive: #1<span class="infoArrow"></span></a></li>
							<li><a href="http://www.youtube.com/embed/3UzwfobENHw" id="wn3" class="vidLink ir">The Stars Talk
								Fresh Faces<span class="infoArrow"></span></a></li>
							<li><a href="http://www.youtube.com/embed/44aue0JwQJY" id="wn2" class="vidLink ir">Trailer<span class="infoArrow"></span></a></li>
							<li><a href="http://www.youtube.com/embed/W8pGjnsedoo" id="wn1" class="vidLink ir">Meet the Cast<span
								class="infoArrow"></span></a></li>
							<li><a href="#" id="wn8" class="vidLink comingSoonLink ir">Face to Face Fresh Exclusive: #3<span class="infoArrow"></span></a></li>
							<%--<li><a href="http://www.youtube.com/embed/tHKKwhGypAI" id="wn8" class="vidLink ir">Face to Face Fresh Exclusive: #3<span class="infoArrow"></span></a></li>--%>
							<li><a href="#" id="wn9" class="vidLink comingSoonLink ir">Behind the Scenes<span class="infoArrow"></span></a></li>
						</ul>
					</div>
					<div class="ecHolder contentHolder">
						<div id="wnContent7" class="extraContent">
							<h3>Episode 7: "Keep Calm and Carry On"</h3>
							<p>Lucy faces some big changes at work&mdash;and<br />
								in her love life.</p>
						</div>
						<div id="wnContent6" class="extraContent">
							<h3>Face to Face Fresh Exclusive: #2</h3>
							<p>Watch fresh, candid interviews with the stars of the show. The girls reveal everything from beauty secrets
								to their mobile phone addictions.</p>
						</div>
						<div id="wnContent5" class="extraContent">
							<h3>Episode 4: "Heal Yourself"</h3>
							<p>Lucy and the girls need a thorough cleanse&mdash;of their skin and their love lives!</p>
						</div>
						<div id="wnContent4" class="extraContent">
							<h3>Face to Face Fresh Exclusive: #1</h3>
							<p>Watch fresh, candid interviews with the stars of the show. The girls reveal everything from beauty secrets
								to their mobile phone addictions.</p>
						</div>
						<div id="wnContent3" class="extraContent">
							<h3>The Stars Talk Fresh Faces</h3>
							<p>Watch the stars of the show dish about their dating (and skincare) do's and don'ts.</p>
						</div>
						<div id="wnContent2" class="extraContent">
							<h3>Trailer</h3>
							<p>Watch the official trailer for Dating Rules From My Future Self.</p>
						</div>
						<div id="wnContent1" class="extraContent">
							<h3>Meet The Cast</h3>
							<p>Get a sneak peek of the Dating Rules From My Future Self cast and find out what's in store for the show.</p>
						</div>
						<div id="wnContent8" class="extraContent">
							<h3>Face to Face Fresh Exclusive: #3</h3>
							<p>Watch fresh, candid interviews with the stars of the show. The girls reveal everything from beauty secrets
								to their mobile phone addictions.</p>
						</div>
						<div id="wnContent9" class="extraContent">
							<h3>Behind the Scenes</h3>
							<p>What's it <em>really</em> like on the set of Dating Rules From My Future Self? Find out!</p>
						</div>
					</div>
				</div>
				<div class="videoLnkHolder" id="fullEpisodesVids">
					<div class="vidLnkCarousel">
						<ul>
							<li><a href="http://www.youtube.com/embed/OeAcV-86BwM" id="fe2" class="vidLink selected ir">Episode 7: "Keep Calm and Carry On"<span class="infoArrow"></span></a></li>
							<li><a href="http://www.youtube.com/embed/hILGFpOwQS8" id="fe1" class="vidLink ir">Episode 4: "Heal Yourself"<span class="infoArrow"></span></a></li>
						</ul>
					</div>
					<div class="feHolder contentHolder">
						<div id="feContent2" class="extraContent">
							<h3>Episode 7: "Keep Calm and Carry On"</h3>
							<p>Lucy faces some big changes at work&mdash;and<br />
								in her love life.</p>
						</div>
						<div id="feContent1" class="extraContent">
							<h3>Episode 4: "Heal Yourself"</h3>
							<p>Lucy and the girls need a thorough cleanse&mdash;of their skin and their love lives!</p>
						</div>
					</div>
				</div>
				<div class="videoLnkHolder" id="behindScenesVids">
					<div class="vidLnkCarousel">
						<ul>
							<li><a href="http://www.youtube.com/embed/Gw7HaGkglA8" id="bs5" class="vidLink selected ir">Face to Face Fresh Exclusive: #2<span class="infoArrow"></span></a></li>
							<li><a href="http://www.youtube.com/embed/S0QzC3gj0gA" id="bs4" class="vidLink ir">Face to Face Fresh Exclusive: #1<span class="infoArrow"></span></a></li>
							<li><a href="http://www.youtube.com/embed/3UzwfobENHw" id="bs3" class="vidLink ir">The Stars Talk
								Fresh Faces<span class="infoArrow"></span></a></a></li>
							<li><a href="http://www.youtube.com/embed/44aue0JwQJY" id="bs2" class="vidLink ir">Trailer<span class="infoArrow"></span></a></li>
							<li><a href="http://www.youtube.com/embed/W8pGjnsedoo" id="bs1" class="vidLink ir">Meet the Cast<span
								class="infoArrow"></span></a></li>
							<li><a href="#" id="bs6" class="vidLink comingSoonLink ir">Face to Face Fresh Exclusive: #3<span class="infoArrow"></span></a></li>
							<%--<li><a href="http://www.youtube.com/embed/tHKKwhGypAI" id="bs6" class="vidLink ir">Face to Face Fresh Exclusive: #3<span class="infoArrow"></span></a></li>--%>
							<li><a href="#" id="bs7" class="vidLink comingSoonLink ir">Behind the Scenes<span class="infoArrow"></span></a></li>
						</ul>
					</div>
					<div class="bsHolder contentHolder">
						<div id="bsContent5" class="extraContent">
							<h3>Face to Face Fresh Exclusive: #2</h3>
							<p>Watch fresh, candid interviews with the stars of the show. The girls reveal everything from beauty secrets
								to their mobile phone addictions.</p>
						</div>
						<div id="bsContent4" class="extraContent">
							<h3>Face to Face Fresh Exclusive: #1</h3>
							<p>Watch fresh, candid interviews with the stars of the show. The girls reveal everything from beauty secrets
								to their mobile phone addictions.</p>
						</div>
						<div id="bsContent3" class="extraContent">
							<h3>The Stars Talk Fresh Faces</h3>
							<p>The stars of the show dish about their dating (and skincare) do's and don'ts.</p>
						</div>
						<div id="bsContent2" class="extraContent">
							<h3>Trailer</h3>
							<p>Watch the official trailer for Dating Rules From My Future Self.</p>
						</div>
						<div id="bsContent1" class="extraContent">
							<h3>Meet The Cast</h3>
							<p>Get a sneak peek of the Dating Rules From My Future Self cast and find out what's in store for the show.</p>
						</div>
						<div id="bsContent6" class="extraContent">
							<h3>Face to Face Fresh Exclusive: #3</h3>
							<p>Watch fresh, candid interviews with the stars of the show. The girls reveal everything from beauty secrets
								to their mobile phone addictions.</p>
						</div>
						<div id="bsContent7" class="extraContent">
							<h3>Behind the Scenes</h3>
							<p>What's it <em>really</em> like on the set of Dating Rules From My Future Self? Find out!</p>
						</div>
					</div>
				</div>
			</div>
			<div id="socialFeeds">
				<h2 class="ir">What's everyone saying?</h2>
				<div id="smartphoneLayout">
					<a href="#" id="fbTab" class="socialTabs ir">Facebook<span></span></a> <a href="#" id="twTab"
						class="socialTabs selected ir">Twitter<span></span></a>
					<div id="fbFeed" class="socialFeedOutput">
						<div id="fbComments">
						</div>
						<a href="#" id="fbShare" class="ir">Comment and Share</a></div>
					<div id="twFeed" class="socialFeedOutput">
						<div id="tweets">
						</div>
						<a href="https://twitter.com/share?screen_name=DatingRulesShow" id="twShare" class="twitter-share-button"
							data-lang="en" data-count="none" data-size="large" data-related="DatingRulesShow:Dating advice from your future self?">
							Tweet</a></div>
				</div>
			</div>
		</div>
	</div>

	<script>
		var fbCommentsUrl = '<%= System.Configuration.ConfigurationManager.AppSettings["datingrulesCommentUrl"] %>';
	</script>

	<%=SquishIt.Framework.Bundle.JavaScript()
		.Add("~/facebook/js/jquery-1.6.4.min.js")
		.Add("~/facebook/AlloysDatingRules/js/jcarousellite_1.0.1.js")
		.Add("~/facebook/AlloysDatingRules/js/drUtils.js")
		.Render("~/facebook/AlloysDatingRules/js/combinedDatingRules_#.js")
	%>
</asp:Content>
