﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Biore2012.promotions.rules
{
	public partial class A_Story_to_Pore_Over_Rules : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Site myMaster = (Site)this.Master;
			myMaster.bodyClass += " stpo utility rules";
		}
	}
}
