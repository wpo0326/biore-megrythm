﻿<%@ Page Title="À propos de  Bior&eacute;&reg;" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.about_us._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Apprenez-en plus sur les produits Bioré®" name="description" />
    <meta content="soin de la peau, peau nette, imperfections, pore, point noir, nettoyage en profondeur, produit visage, produit points noirs" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div class="centeringDiv">
                <div id="content">
                    <div class="titleh1">
                        <h1>À PROPOS DE BIORÉ</h1>
                        <div id="responseRule"></div>
                    </div>
                    <div class="about-info">                        
                        <p>Bioré est une marque de soin de la peau dédiée aux peaux jeunes, qui s'attaque à l'ennemi public n°1 : les pores obstrués ! Ce sont des produits malins et sensoriels plutôt que des solutions abrasives et trop radicales. Nos formats Gels, Patchs, Exfoliants, Poudre sont des produits nettoyants qui ne vont pas passer par 4 chemins pour combattre les problèmes à leur source.  Résultat, une peau nette et rafraîchie, prête pour affronter le quotidien !</p>
                    </div>
                    <img src="../images/about/aboutBG.png" alt="" id="Img1" />                    
                </div>
            </div>
        </div>
    </div>
</asp:Content>

