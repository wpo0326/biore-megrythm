﻿<%@ Page Title="À propos de Bioré" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.about_us._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="À propos de Bioré" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
    <style type="text/css">   
         .disclaimer {font-size: .7em;}
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent" class="aboutBlueBG">
            <div class="centeringDiv">
                <div id="content" class="aboutBlueTxt">
                    <div class="titleh1">
                        <h1>À PROPOS DE BIORÉ</h1>
                        <div id="responseRule"></div>
                    </div>
                    <div class="about-info">     
                        
                        <p><span>Découvez Bioré - l’expert des pores !</span></p>

                        <p>N°1 des nettoyants visage au Japon*, Bioré est une marque japonaise spécialiste des soins de la peau qui s’attaque à l’ennemi public n°1 : les pores obstrués.</p>

<p>Au Charbon ou au Bicarbonate de Sodium, les produits Bioré débarrassent vos pores des impuretés et de l’excès de sébum pour vous aider à combattre efficacement les imperfections et les points noirs incrustés.</p>

<p>Nos produits ciblés en formats gels, patchs, exfoliants, eaux micellaires ou poudres nettoient les pores en profondeur pour éliminer efficacement les problèmes à leur racine.</p>

<p>Résultat ? Une peau nette et rafraîchie, prête à affronter le quotidien.</p>

<p>Bye bye impuretés, hello peau éclatante de santé !</p>

                        <p><span>Bioré - Libérez vos pores.</span></p>
                        
                        <div class="disclaimer">
                        
                        <p>*Ventes valeur et volume nettoyants visage data SRI, de juillet 2015 à juin 2016.</p>
                        
                        </div>
                                           
                       
                    </div>
                    <img src="../images/about/aboutBG.png" alt="" id="Img1" />                    
                </div>
            </div>
        </div>
    </div>
</asp:Content>

