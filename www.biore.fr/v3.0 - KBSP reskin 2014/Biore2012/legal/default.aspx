﻿<%@ Page Title="Mentions Légales " MasterPageFile="../Site.Master"
    Language="C#" AutoEventWireup="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Mentions Légales " name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
    <style type="text/css">
        #content p
        {
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="about">
        <div id="main">
            <div id="mainContent">
                <div class="centeringDiv">
                    <div id="content">
                        <div class="titleh1">
                            <h1>
                                MENTIONS LÉGALES</h1>
                            <div id="responseRule">
                            </div>
                        </div>
                        <!--<p>© 2016 Kao USA Inc. ("Kao") All rights reserved.</p>-->
                
                <p>EN VISITANT LE PRÉSENT SITE WEB, VOUS COMPRENEZ ET ACCEPTEZ CE QUI SUIT :
                <br /><br />
                Les renseignements contenus dans le présent site Web (le « site ») concernant tout produit et service ne s’appliquent qu’à la France et sont destinés à un usage personnel et non commercial. Tous les titres, noms et éléments graphiques utilisés dans le site sont des marques de commerce de Kao ou de ses concédants, à moins d’indication contraire. Toutes les informations contenues dans ce site sont la propriété exclusive de Kao ou de ses concédants. La reproduction, la republication et la distribution de tout élément figurant dans ce site à des fins autres que pour vous permettre de consulter le site sont strictement interdites, exception faite que l’utilisateur peut, pour des raisons personnelles et non commerciales, les télécharger et en faire une copie papier. Les renseignements relatifs aux produits et services de Kao peuvent être modifiés sans préavis. Certains produits et services peuvent ne pas être offerts dans certaines régions. Les renseignements contenus dans le présent site sont fournis à titre informatif seulement. Les points de vue et opinions exprimés sur ce site ne sont pas nécessairement ceux de Kao. Toute mention de produits et/ou d’entreprises ne doit en aucun cas être considérée comme un endossement desdits produits et/ou entreprises ou comme un énoncé d’affiliation.
                <br /><br />
                Les renseignements présentés sur le présent site sont fournis à titre informatif et ne peuvent en aucun cas se substituer à un avis, diagnostic ou traitement médical professionnel. L’utilisation des renseignements fournis sur le site est entièrement à vos propres risques. Si vous avez des questions concernant des aspects médicaux ou de santé, contactez un professionnel de la santé.
                <br /><br />
                Vous ne pouvez utiliser aucun moteur de recherche Web, robot, technique de forage de données ni aucun autre dispositif ou procédé automatique pouvant permettre de cataloguer, télécharger, enregistrer ou autrement reproduire, stocker ou distribuer le contenu du site. Vous ne pouvez prendre aucune mesure visant à interférer avec le site, l’interrompre ou interrompre l’utilisation par d’autres usagers incluant, sans s’y limiter, en surchargeant, inondant, bombardant de courriels ou paralysant le site, contournant les mesures de sécurité ou d’authentification des utilisateurs ou tentant de dépasser les privilèges d’autorisation et d’accès accordés en vertu des présentes modalités. Vous ne pouvez intégrer des portions du site à l’intérieur d’un autre site ou établir des liens entre un autre site et toute page du présent, exception faite de la page d’accueil. Vous ne pouvez revendre à un tiers l’utilisation du site et son accès sans notre consentement écrit préalable.</p>
                
                <p>TOUTES LES PRÉCAUTIONS ONT ÉTÉ PRISES POUR S’ASSURER QUE LE CONTENU DU SITE EST EXACT ET À JOUR. Kao NE FAIT AUCUNE REPRÉSENTATION OU GARANTIE QUANT À L’EXACTITUDE OU AU CARACTÈRE COMPLET DU SITE OU DE SON CONTENU, QUI SONT FOURNIS EN L’ÉTAT. Kao NE GARANTIT PAS QUE L’USAGE DU SITE SE FERA SANS INTERRUPTION OU SANS ERREUR, QUE LES DÉFECTUOSITÉS, LE CAS ÉCHÉANT, SERONT CORRIGÉES, OU QUE LE SITE OU LE SERVEUR QUI LE MET À VOTRE DISPOSITION EST OU SERA EXEMPT DE VIRUS OU D’AUTRES COMPOSANTES DOMMAGEABLES. Kao NE PEUT ÊTRE TENUE RESPONSABLE DE L’UTILISATION DU SITE, Y COMPRIS, SANS S’Y LIMITER, DU CONTENU ET DE TOUTE ERREUR. Kao EXCLUT TOUTE GARANTIE EXPLICITE OU IMPLICITE (NOTAMMENT DE QUALITÉ MARCHANDE, D’ABSENCE DE CONTREFAÇON ET D’ADÉQUATION À UN USAGE PARTICULIER).</p>
                
                <p>Le présent site peut contenir des liens ou des références à d’autres sites Web. Kao ne peut toutefois être tenue responsable du contenu desdits sites ni des dommages ou blessures découlant des contenus en question. Les liens vers d’autres sites sont fournis aux utilisateurs uniquement pour des raisons de commodité.
                <br /><br />
                Kao se soucie de la protection de votre vie privée et de vos données personnelles. Pour obtenir de plus amples renseignements à ce sujet, consultez notre  <a href="/en-US/privacy/">Politique de confidentialité</a>.
                <br /><br />
                Ni Kao, ni ses agents, ses sociétés affiliées, représentants ou toute autre partie participant à la création, à la production ou à l’administration du site (collectivement les « parties de Kao ») ne peuvent être tenus responsables des dommages indirects, accessoires, consécutifs, spéciaux ou exemplaires résultant de l’utilisation du site ou de tout produit ou service offert sur le site, et cela, même si lesdites parties ont été informées de la possibilité de tels dommages.
                <br /><br />
                La responsabilité globale des parties de Kao concernant les dommages, blessures, pertes ou causes d’action (que ce soit de manière contractuelle, délictuelle ou autre) résultant de l’utilisation du site ne peut en aucun cas dépasser : (a) le montant que vous avez versé, s’il y a lieu, à Kao pour utiliser le site ou (b) 100 $.
                <br /><br />
                Le présent site est fourni à titre de service à ses visiteurs. Kao se réserve le droit de supprimer, modifier ou amender le contenu du site en tout temps, y compris modifier les présentes modalités, pour quelque raison, et cela, sans préavis. Les changements seront publiés sur le site. Consultez celui-ci régulièrement afin de prendre connaissance de tout changement apporté. En continuant d’accéder au site après de tels changements, vous indiquez de ce fait votre acceptation des changements en question.</p>
                
                <p>Les présentes modalités sont régies par le droit français. Vous acceptez que tout litige lié à l’utilisation de ce site ou de tout produit ou service de Kao soit soumis exclusivement au droit français et vous acceptez irrévocablement de le soumettre à la juridiction des tribunaux compétents. Si une disposition des présentes modalités devait être illégale, nulle et non avenue ou, pour n’importe quel motif, non exécutoire, une telle disposition serait alors jugée dissociable des présentes et n’aurait aucun effet sur la validité ou le caractère exécutoire de toute disposition restante.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
