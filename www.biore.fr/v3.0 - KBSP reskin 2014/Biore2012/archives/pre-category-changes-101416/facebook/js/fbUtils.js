﻿(function($, undefined) {
    window.fbUtils = {
        accessToken: "",
        loggedin: false,
        albums: {},
        fbDataPull: {},
        fbAlbumImageDataPull: {},
        uid: "",
        scope: ["user_photos"],
        //initialize the Facebook API
        init: function() {

            //set the FB permissions
            fbUtils.setFBScope();
        },

        setFBScope: function() {
            var scopeString = "";
            for (var i = 0, item; item = fbUtils.scope[i]; i++) {
                //if profile pic, populate profile pic dropdown
                scopeString += item + ",";
            }
            scopeString = scopeString.substring(0, scopeString.length - 1);
            //if there is a login button, add scope
            if ($(".selectFromFB")) {
                $(".selectFromFB").attr("scope", scopeString);
            }
            return scopeString;
        },

        //Set up FB Event Listeners
        initEvents: function() {
            FB.Event.subscribe("auth.authResponseChange", function(response) {
                if (response.status === 'connected' && fbUtils.loggedin === false) {
                    fbUtils.setConnectedState(response);
                    fbUtils.showLoggedInState();
                } else if (response.status === 'not_authorized') {

                } else {

                }
            });
        },

        //login to facebook
        login: function() {
            var scopeList = fbUtils.setFBScope();
            FB.login(function(response) { }, { scope: scopeList });
        },

        //Log Out of Facebook
        logout: function() {
            FB.logout(function(response) {
                fbUtils.loggedin = false;
            });
        },

        //Check the login state
        getLoginState: function() {
            //check FB login status
            FB.getLoginStatus(function(response) {
                if (response.status === 'connected' && fbUtils.loggedin === false) {
                    fbUtils.setConnectedState(response);
                } else if (response.status === 'not_authorized') {
                    fbUtils.loggedin = false;
                    //fbUtils.login();
                } else {
                    // fbUtils.login();
                    fbUtils.loggedin = false;
                }
            });


        },

        //Set the display state basedon the response
        setConnectedState: function(response) {
            fbUtils.loggedin = true;
            fbUtils.uid = response.authResponse.userID;
            fbUtils.accessToken = response.authResponse.accessToken;
            fbUtils.initialBatchRequest();
        },

        //FB Initial Batch request
        initialBatchRequest: function() {
            FB.api("/", "POST",
                { access_token: fbUtils.accessToken,
                    batch: [/*fbDataPull[0]*/
                        {"method": "GET", "name": "get-albums", "omit_response_on_success": false, "relative_url": "/me/albums" }
                    ]
                },
                function(response) {
                    if (!response || response.error) {
                        //show error
                        fbUtils.showErrorState();
                    }
                    else if (response.status === "not_authorized") {
                        fbUtils.showErrorState();
                    } else {
                        fbUtils.fbDataPull = response;
                        fbUtils.albums = $.parseJSON(fbUtils.fbDataPull[0].body);
                        utils.populateAlbumList();
                    }
                });
        },

        getImageInfoBatchRequest: function(albumID, $target) {
            fbUtils.fbAlbumImageDataPull = {};
            FB.api("/", "POST",
                { access_token: fbUtils.accessToken,
                    batch: [/*fbImagePull[0]*/
                        {"method": "GET", "name": "get-albumImages", "omit_response_on_success": false, "relative_url": "/" + albumID + "/Photos?limit=200" }
                    ]
                },
                 function(response) {
                     if (!response || response.error) {
                         //show error
                         fbUtils.showErrorState($target);
                     }
                     else if (response.status === "not_authorized") {
                         fbUtils.showErrorState($target);
                     } else {
                         utils.populateAlbumImageList(response[0].body, $target);
                     }
                 });

        },

        openWindow: function(url, width, height) {
            var popUpWidth = width || "580";
            var popUpHeight = height || "400";
            window.open(url, "feedDialog", "toolbar=0,status=0,width=" + popUpWidth + ",height=" + popUpHeight);
            return false;
        },


        //Logged In state
        showLoggedInState: function() {
            // $("#overlay").show();
            // $("#fbError").hide();
        },

        //Logged Out state
        showLoggedOutState: function() {
            // $("#fbLoggedIn").addClass("hide");
            // $("#fbError").hide();
        },

        //Error state
        showErrorState: function($target) {
            if ($target.length) {
                $target.html('').append($('<h3>').html('<strong>Oops,</strong> <span style="font-weight: normal">this photo page did not load. To fix this issue, make sure your pop-up blocker is turned off and that you have authenticated with Facebook to allow us access to your photo information. Once resolved, click Cancel in this window to go back and try again.</span>'));
            }
        }

    },
    window.utils = {
        initEvents: function() {
            // Select From Albums button
            $(".selectFromFB").live('click', function(e){
                e.preventDefault();
                // Authenticate
                if (!fbUtils.loggedin) {
                    fbUtils.login();
                }

                utils.openAlbumLightbox(e);

                setTimeout(function() {
                    fbUtils.getImageInfoBatchRequest($('#fbAlbum').val(), $('.imageWrap'));
                }, 1000);

            });

            $('#cancelSelectFBImage').live('click', utils.closeAlbumLightbox);

            $('.howDoesItWorkPopupOverlay').live('click', utils.closeAlbumLightbox);

            //Facebook Profile Albums select box change
            $('#fbAlbum').live('change', function(e) {
                e.preventDefault();
                //clear list
                $('.imageWrap').html('');
                //get images from facebook
                fbUtils.getImageInfoBatchRequest($('#fbAlbum').val(), $('.imageWrap'));
            });


            //Facebook Profile image list click
            $('.imageWrap a').live("click", function(e) {
                var $this = $(this);
                e.preventDefault();
                var picID = $this.attr('href');
                //clear current image
                $('#selectFBImage').attr({'href': picID, 'rel': $this.find('img').attr('style')});

                $this
                    .closest('li')
                        .siblings('li').removeClass('selected')
                    .end().addClass('selected');
            });

            $('#selectFBImage').live('click', function(e){
                var imgUrl = $(this).attr('href'),
                    imgStyle = $(this).attr('rel');
                e.preventDefault();

                if (imgStyle != null && imgStyle.length) {

                    imgStyle = imgStyle.replace('75', '120');

                    utils.closeAlbumLightbox(e);

                    $('#ctl00_ContentPlaceHolder1_hidAddPhoto').val(imgUrl);
                    $('.maskedImage').attr({'src': imgUrl, 'style': imgStyle});
                    $('.AddPhotoContainer .errormsg').css({'visibility': 'hidden'});
                }

            });
        },
        //populate the album Drowpdown with Facebook Names
        populateAlbumList: function() {
            $("#fbAlbum").html('');
            for (var i = 0, album; album = fbUtils.albums.data[i]; i++) {
                if (album.count) {
                    $("#fbAlbum").append($("<option>").val(album.id).html(album.name));
                    $("#fbAlbum").find('option').eq(0).prop('selected', true);
                }
            }
            fbUtils.getImageInfoBatchRequest(fbUtils.albums.data[0].id, $('.imageWrap'));
        },
        //populate the list of photos for the selected album
        populateAlbumImageList: function(ImageDataArray, $target) {
            utils.selectedAlbumData = {};
            //get image data for the selected album (from FB data)
            utils.selectedAlbumData = $.parseJSON(ImageDataArray);

            if (typeof utils.selectedAlbumData.data !== 'undefined' && utils.selectedAlbumData.data.length) {
                $target.html('').append($('<ul>'));
                //for each image, add to list
                for (var i = 0, photo; photo = utils.selectedAlbumData.data[i]; i++) {
                    var imageToUse = utils.getThumbImage(photo.images);
                    var largeImage = utils.getLargeImage(photo.images);
                    if (typeof imageToUse === 'undefined' ||
                        typeof imageToUse.source === 'undefined' ||
                        imageToUse == null ||
                        imageToUse.source == null) {
                            imageToUse.source = photo.picture;
                    }

                    if (imageToUse.height > imageToUse.width) {

                    }


                    $target.find('ul')
                        .append($("<li>")
                            .append($("<a>")
                                .attr("href", largeImage.source)
                                .attr("id", "thumb" + i)
                                .append($("<img>")
                                    .attr("src", imageToUse.source)
                                    .attr("style", utils.getImageRatioStyle(imageToUse))
                                )
                            )
                        );
                }
            } else {
                $target.html('').append($('<h3>').text('There are no available photos in that album.'));
            }

        },

        getImageRatioStyle: function(image) {
            var ratio, newH, newW;
            if (image.width <= image.height) {
                newW = "75px";
                newH = "auto";
            } else {
                newH = "75px";
                newW = "auto";
            }
            return "width: " + newW + "; height: " + newH + ";";
        },
        getThumbImage: function(images) {
            var imageToUse = images[0];
            images.sort(function(a, b) {
                var a1 = a.width, b1 = b.width;
                if (a1 == b1) { return 0; }
                return a1 > b1 ? 1 : -1;
            });
            for (var i = 0, image; image = images[i]; i++) {
                if (image.width >= 80) {
                    imageToUse = image;
                    break;
                }
            }

            return imageToUse;
        },

        getLargeImage: function(images) {
            var imageToUse = images[images.length-1];
            images.sort(function(a, b) {
                var a1 = a.width, b1 = b.width;
                if (a1 == b1) { return 0; }
                return a1 < b1 ? 1 : -1;
            });
            for (var i = 0, image; image = images[i]; i++) {
                if (image.width >= 80 && image.width <= 750) {
                    imageToUse = image;
                    break;
                }
            }

            return imageToUse;
        },

        openAlbumLightbox: function(e){
            e.preventDefault();
            $('.selectPhotoWrap').fadeIn(600);
            $('.howDoesItWorkPopupOverlay').fadeIn(300);
        },
        closeAlbumLightbox: function(e){
            e.preventDefault();
            $('.selectPhotoWrap').fadeOut(600);
            $('.howDoesItWorkPopupOverlay').fadeOut(300);
        }
    };

    fbUtils.init();
    utils.initEvents();
})(jQuery);
