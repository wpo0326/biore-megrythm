﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Facebook;
using System.Configuration;

namespace Biore2012.facebook.StoryToPoreOver
{
	public partial class Default : System.Web.UI.Page
	{
		public string tabToShow { get; set; }
		public string daysLeft { get; set; }
		DateTime today = DateTime.Now;
		DateTime targetDate = DateTime.Parse(System.Configuration.ConfigurationManager.AppSettings["AuthorPartnershipLaunchDate"]);
		List<TargetTab> tabStates = new List<TargetTab>();
		TargetTab tab10am = new TargetTab("AuthorPartnershipTab1");
		TargetTab tab1pm = new TargetTab("AuthorPartnershipTab2");
		TargetTab tab3pm = new TargetTab("AuthorPartnershipTab3");
		TargetTab tabPost = new TargetTab("AuthorPartnershipPost");
		TargetTab tabInterim = new TargetTab("AuthorPartnershipInterim");
		public string tab10amislocked { get; set; }
		public string tab1pmislocked { get; set; }
		public string tab3pmislocked { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			initializeTabs();
			determineDisplayState();
			daysLeft = determineDaysLeft();

			if (daysLeft == "0")
			{
				litDaysCopy.Text = "stpoCountdownZeroDayCopy";
			}
			if (daysLeft == "1")
			{
				litDaysCopy.Text = "stpoCountdownDayCopy";
			}
		}

		private void initializeTabs()
		{
			tabStates.Add(tab10am);
			tabStates.Add(tab1pm);
			tabStates.Add(tab3pm);
			tabStates.Add(tabPost);
			tabStates.Add(tabInterim);
		}

		private string determineDaysLeft()
		{
			TimeSpan timeToGo = targetDate - today;
			int diffInDays = (int)Math.Ceiling(timeToGo.TotalDays);

			return diffInDays.ToString();
		}

		private void determineDisplayState()
		{
			string signedReqJson = "";
			FacebookSignedRequest signedReqObj = new FacebookSignedRequest();

			decodeFBSignedRequest(ref signedReqJson, ref signedReqObj);

			//if there is no signedRequest, kick to error page
			if (Session["signedRequestObj"] == null)
			{
				setDisplayState("notfacebook");
				return;
			}

			if (signedReqObj != null && signedReqObj.page.liked)
			{
				setDisplayState("pageliked");
			}
			else
			{
				setDisplayState("likeoverlay");
			}
		}

		private void setDisplayState(string state)
		{
			panLikeOverlay.Visible = false;
			phUserLikesPre10am.Visible = false;
			phUserLikesTopContent10am.Visible = false;
			phUserLikesTopContent1p3p.Visible = false;
			panTabContainer.Visible = false;
			phPostLaunchDayContent.Visible = false;
			pan10amGiveaway.Visible = false;
			pan10amGiveawayOver.Visible = false;
			ucQuiz.Visible = false;
			pan300Giveaway.Visible = false;
			ucQuizPostLaunch.Visible = false;
			phDisclaimerPostLaunch.Visible = false;
			phDisclaimerLaunchDay.Visible = false;
			phDisclaimerPreLaunch.Visible = false;
			ucQuizInterim.Visible = false;
			phInterimContent.Visible = false;

			// For Dev purposes to force a state (must comment out the switch statement)
			//panUserLikes.Visible = true;
			//state = "pageliked";

			switch (state)
			{
				case "likeoverlay":
					panLikeOverlay.Visible = true;
					setBodyClass("panLikeOverlay");
					phDisclaimerPreLaunch.Visible = true;
					break;
				case "pageliked":
					if (!tabInterim.IsLocked)
					{	// After 6pm on 11/12, enable Interim page
						ucQuizInterim.Visible = true;
						Panel proveItPanel = (ucQuizInterim.FindControl("panProveIt") as Panel);
						if (proveItPanel != null) { proveItPanel.Visible = false; }
						PlaceHolder proveItTopPh = (ucQuizInterim.FindControl("phProveItTop") as PlaceHolder);
						if (proveItTopPh != null) { proveItTopPh.Visible = false; }
						//panProveIt.Visible = false;
						phInterimContent.Visible = true;
						setBodyClass("phPostLaunchDayContent phInterimContent");
					} 
					else if (!tabPost.IsLocked)
					{	// After 5pm on Launch, enable post launch page

						ucQuizPostLaunch.Visible = true;
						Panel proveItPanel = (ucQuizPostLaunch.FindControl("panProveIt") as Panel);
						if (proveItPanel != null) { proveItPanel.Visible = false; }
						PlaceHolder proveItTopPh = (ucQuizPostLaunch.FindControl("phProveItTop") as PlaceHolder);
						if (proveItTopPh != null) { proveItTopPh.Visible = false; }
						//panProveIt.Visible = false;
						phPostLaunchDayContent.Visible = true;
						phDisclaimerPostLaunch.Visible = true;
						setBodyClass("phPostLaunchDayContent");
					}
					else if (!tab3pm.IsLocked)
					{	// After 3pm on Launch, before 5pm, enable tabs 10am/1pm/3pm
						// Enable 10am Giveaway over content
						panTabContainer.Visible = true;
						phUserLikesTopContent1p3p.Visible = true;
						pan10amGiveawayOver.Visible = true;
						ucQuiz.Visible = true;
						pan300Giveaway.Visible = true;
						phDisclaimerLaunchDay.Visible = true;
						setBodyClass("phUserLikesAfter3p");
						tab10amislocked = "";
						tab1pmislocked = "";
						tab3pmislocked = "";
					}
					else if (!tab1pm.IsLocked)
					{	// After 1pm on Launch, before 3pm, enable tabs 10am/1pm
						panTabContainer.Visible = true;
						phUserLikesTopContent1p3p.Visible = true;
						pan10amGiveaway.Visible = true;
						ucQuiz.Visible = true;
						phDisclaimerLaunchDay.Visible = true;
						setBodyClass("phUserLikesAfter1p");
						tab10amislocked = "";
						tab1pmislocked = "";
						tab3pmislocked = "preunlock";
					}
					else if (!tab10am.IsLocked)
					{	// After 10am on Launch, before 1pm, enable tab 10am
						panTabContainer.Visible = true;
						phUserLikesTopContent10am.Visible = true;
						pan10amGiveaway.Visible = true;
						phDisclaimerLaunchDay.Visible = true;
						setBodyClass("phUserLikesAfter10a");
						tab10amislocked = "";
						tab1pmislocked = "preunlock";
						tab3pmislocked = "preunlock";
					}
					else
					{	// Before 10am, leave all tabs off, enable pre10am page
						phUserLikesPre10am.Visible = true;
						phDisclaimerPreLaunch.Visible = true;
						setBodyClass("phUserLikesPre10am");
						tab10amislocked = "preunlock";
						tab1pmislocked = "preunlock";
						tab3pmislocked = "preunlock";
					}
					break;
				default:
					panLikeOverlay.Visible = true;
					phDisclaimerPreLaunch.Visible = true;
					setBodyClass("panLikeOverlay");
					break;
			}
			setBodyClass(state);
		}

		private void setBodyClass(string state)
		{
			FBContentPageMaster myMaster = (FBContentPageMaster)this.Master;
			myMaster.bodyClass += " stpo stpoPhase2 " + state;
		}

		private static void decodeFBSignedRequest(ref string signedReqJson, ref FacebookSignedRequest signedReqObj)
		{
			signedReqJson = FBUtils.decodeSignedReq();
			signedReqObj = FBUtils.serializeSignedRequestJSON<FacebookSignedRequest>(signedReqJson);

			FBUtils.setSignedRequestSession(signedReqJson, signedReqObj);
		}

		private class TargetTab
		{
			public string TabName { get; set; }
			public DateTime TargetTime	{ get; set; }
			public bool IsLocked { get { return compareTargetTimeToNow(); } }

			public TargetTab(string tabName)
			{
				this.TabName = tabName;
				string date = tabName == "AuthorPartnershipInterim" ? ConfigurationManager.AppSettings["AuthorPartnershipInterimDate"] : ConfigurationManager.AppSettings["AuthorPartnershipLaunchDate"];
				this.TargetTime = DateTime.Parse(date + "T" + ConfigurationManager.AppSettings[tabName]);
			}

			private bool compareTargetTimeToNow()
			{
				DateTime now = DateTime.Now;
				TimeSpan timeDiff = this.TargetTime - now;

				if (timeDiff.TotalMilliseconds <= 0)
				{
					return false;
				}
				return true;
			}
		}
	}
}
