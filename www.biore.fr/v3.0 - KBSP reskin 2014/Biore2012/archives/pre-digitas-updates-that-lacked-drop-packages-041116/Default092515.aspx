﻿<%@ Page Title="Get a Clean Face | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Get healthy, radiant skin with products from Bioré® Skincare" name="description" />
    <meta content="skincare, clean face" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/UniversPro67.css")
        .Add("~/css/homepage.css")
        .Add("~/css/flexslider.css")
        .Render("~/CSS/combinedhome_#.css")
    %>
    <meta name="viewport" content="width=device-width">

    <script type="text/javascript">
         $(function() {
             $('#theaterItem1 a').click(
                 function (e) {
                     e.preventDefault();

                     var axel = Math.random() + "";
                     var a = axel * 10000000000000;
                     var strScript = "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore551;ord='" + a + "'?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"                     
                                + "<noscript>"
                                + "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore551;ord=1?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"
                                + "</noscript>";

                     $('body').prepend(strScript);
                     var target = $(this).attr('target');
                     var uri = $(this).attr('href');

                     setTimeout(function () {
                         if (target) {
                             window.open(uri, '_blank');
                         } else {
                             window.location = uri;
                         }
                     }, 1000);
                     
                 });
         })
    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">


<script type="text/javascript">
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    document.write('<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore916;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore916;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>

<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div class="flexslider"  id="theater" >
            <ul class="slides">
                <li id="theaterItem3" class="theaterHolder">        
                   <div class="ContenTop clearfix">
                       <div class="mainContent">
                            
                           <div class="txt">                                                              
                               <h2>Breakup with blackheads.<sup>&reg;</sup></h2>
                               <p class="forChance">Start fresh with Bioré<sup>&reg;</sup> Deep Cleansing<br class="desktopBreak" /> Charcoal Pore Strips.</p>                               
                           </div>
                           <!--<div class="womanImage">
                                <img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/backgroundImage.jpg") %>" alt="" />
                           </div>-->
                           
                           <div class="products">
                                <img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/brittanyshow-products.png") %>" alt="" />
                           </div>
                            

                           <div style="clear:both;"></div>

                           <!--new button-->
                           <div class="btnLearnMore">
                              <a href= "http://www.biore.com/en-US/breakup-with-blackheads/charcoal-pore-strips#regular" >
                                  <img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/cta-btn.png") %>" alt="" />
                              </a>
                           </div>

                       </div>
                       <div class="blackBorder"></div>
                   </div>
                   <div class="ContentBottom">
                      <!--  <div class="wrapperItems">
                           <img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/brittanySnow-street.png") %>" alt="" />
                           <a href="http://strip.biore.com" class="btnWin track" target="_blank" data-tracking="Mastheads|Click|Brittany">VIEW ENTRIES ></a>
                        </div>-->
                   </div>
                 </li> 
                
               <li id="theaterItem1" class="theaterHolder">
                <div class="fma1Product">
                    <div class="wrapperItem">
                        <h2 class="newest-headline">our newest charcoal products have arrived</h2>
                        <a href="biore-facial-cleansing-products/Charcoal.aspx"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/fmas/fma5-new.png") %>" alt="Our newest charcoal products have arrived. Prepare your pores for de-gunk-ification. Find Out More." /></a>
                        <h2 class="prepeare-headline">PREPARE YOUR PORES FOR <span> De-GUNK-if ication</span></h2>
                        <a href="biore-facial-cleansing-products/Charcoal.aspx"  class="btnWin track" data-tracking="Mastheads|Click|Charcoal">find out more<span></span></a>
                    </div>
                </div>
               </li>
                
                <li id="theaterItem2" class="theaterHolder">
                    <div class="NTable">
                        <div class="NRow r1">
                            <div class="NCell c1">
                                <h1>PORE CARE <span>101</span></h1>
                                <div class="ruleBG"></div>
                                <div id="responseRule"></div>
                            </div>
                        </div>
                        <div class="NRow r2">
                            <div class="NCell c2"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/FMA3BG.jpg") %>" alt="" /></div>
                        </div>
                        <div class="NRow r3">
                            <div class="NCell c3"><img class="strikeOut" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/poreCAreStrike.gif") %>" alt="" />Learn How to Battle<img class="strikeOut" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/poreCAreStrike.gif") %>" alt="" /></div>
                        </div>
                        <div class="NRow r4">
                            <div class="NCell c4">clogged pores &amp; acne</div>
                        </div>
                         <div class="NRow r5">
                            <div class="NCell c5">FROM EVERY ANGLE FOR CLEAN, HEALTHY LOOKING SKIN EVERYDAY.</div>
                        </div>
                        <div class="NRow r6">
                            <div class="NCell c6"><a href="<%= VirtualPathUtility.ToAbsolute("~/pore-care/") %>" class="track" data-tracking="Mastheads|Click|PoreCare"><div class="fma3Face">GET STARTED</div></a></div>
                            <br /><br /><br />
                        </div>
                    </div>

                </li>

                
               <!--
               <li id="theaterItem3" class="theaterHolder">        
                    <div class="fma2Headline"><span class="fma2Great">Hurry</span> — program ending! </div><br />
                        <div class="fma2HeadlineTwo">Clear out your points by December 31, 2014</div>
                              <div class="fma2Bubbles"><div class="fma2Prove"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/fmas/fma2Prove.png") %>" alt="" /></div></div>
                                <div class="fma2HowWrap">
                                  
                                  <div id="mobileFMA" class="fma2row">
                                      <div id="fma2ListM">
                                          <div>&#8226; Earn <b>Prove It!&reg;</b> Reward Points.</div>
                                          <div>&#8226; Redeem. &#8226; Celebrate.</div>
                                      </div>
                                  </div>
                              </div>
                      <a target="_blank" href="https://apps.facebook.com/proveitrewards/"><div class="fma2Face">Redeem Now</div></a>
                      <div class="fma2newsletter"><a href="/en-US/email-newsletter-sign-up/">Opt-in to receive our newsletter</a></div>
                 </li> 
                 
                 <!--<li id="theaterItem4" class="theaterHolder">
                    <div class="pinkBubbles"> 
                        <div class="komenContainer">      
                                <h1>Free your pores with purpose.</h1>
                                <div class="ruleBG2"></div>

                                <p class="c2">The Bior&eacute;&reg; Skincare brand is proud to support Susan G. Komen&reg; in its mission to save lives and end breast cancer forever.</p>

                                <div><img src="<%= VirtualPathUtility.ToAbsolute("~/images/fmas/PinkProject_Biore.png") %>" alt="" /></div>
                                
                                 <p class="c4">Remove a week's worth of buildup in just 10 minutes and join the fight by purchasing limited edition packaging.</p>
                                
                                <p class="c5">Show your support for Susan G. Komen&reg; with our limited edition Deep Cleansing Pore Strips packaging. Regardless of sales, Kao USA Inc., on behalf of its  JERGENS&reg;, Bior&eacute;&reg;, Ban&reg;, Frizz Ease&reg;, and Cur&eacute;l&reg; brands, is donating a total of $250,000  in 2014 to Susan G. Komen&reg; and invites you to clean for a cause!</p>
                                
                                    <div class="c6"><a href="http://ww5.komen.org/KaoUSA.aspx" target="_blank"><div class="fma4Face">LEARN MORE</div></a></div>
                                    <br /><br /><br />
                                </div> 
                        </div>

                </li>-->
                
                
                       
            </ul>
            <div class="centeringDiv" id="pagerWrapper"><div id="pager"></div></div>
        </div>
        
    </div>
  
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/jquery.flexslider.min.js")
        .Render("~/js/combinedhome_#.js")
    %>
</asp:Content>