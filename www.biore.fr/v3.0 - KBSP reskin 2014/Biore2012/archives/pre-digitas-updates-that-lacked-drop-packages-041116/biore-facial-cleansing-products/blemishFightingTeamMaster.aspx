﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/productDetail.css")
        .Add("~/css/ghindaVideoPlayer.css")
        .Render("~/css/combined_detail_#.css")
    %>
    <meta name="description" id="metaDescription" runat="server" />
    <meta name="keywords" id="metaKeywords" runat="server" />
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="x-rim-auto-match" content="none">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<style>
h2.complexionClearing{background-color:#f48025;}
</style>
<div id="main">

    <div id="mainContent" style="background: url(../images/ourProducts/productsBg.jpg) no-repeat top center;">

        <div id="shadow"></div>

        <div class="centeringDiv">

            <div class="col floatRight" id="productName">

                <h2 class="roundedCorners pie complexionClearing">Complexion Clearing</h2>

                <h1 style="font-size:44px; color:#f48025;">hello<br />blemish-fighting team.<br /><span style="font-size:37px; color:#4e87c3;">goodbye all kinds of acne.</span></h1>

                <p><span class="new"></span> Deep Cleansing Pore Strips, Blemish Treating Astringent and Any Complexion Clearing Cleanser or Pore Unclogging Scrub. </p>

                
                

                <div class="plusBtn" id="plusBtnDiv"><g:plusone size="medium" annotation="none" href="http://bioreus.cgsrv.com/en-US/social/permalinks/acne-clearing-scrub/"></g:plusone></div>

                <div class="likeBtn" id="likeBtnDiv"><div class="fb-like" data-href="http://bioreus.cgsrv.com/en-US/social/permalinks/acne-clearing-scrub/" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false"></div></div>     

                <div class="clear"></div>           

            </div>

            <div class="col bottomBorder" id="productImages">

                <div id="newProd">

                    <img id="ctl00_ContentPlaceHolder1_theNewImage" src="/en-US/images/ourProducts/products/large/blemishFightingTeam.png" style="border-width:0px; margin-left:-80px;" width="140%" />

                </div>


            </div>

            <div class="col floatRight" id="productInfo">

                <div class="contentHolder open">

                    <div class="collapsibleContent" id="whatItIsContent">

                        
                        <h4 style="margin-bottom:5px; color:#f48025;">Together, this trio treats all forms of acne and helps keep it from coming back.</h4>
                        <p>It’s time to change up your skin routine for one that targets pores and keeps your skin consistently clear. 
                    <ul>
                    <li>Non-irritating </li>
                    <li>Helps treat and prevent whiteheads and blackheads</li>
                    <li>Evens out skin texture </li>
                    </ul> 
                       </p>
                    </div>
                </div>
                <div class="clear"></div>

            </div>
                <div class="clear"></div>
                
			<div style="padding:2% 0 4% 10%;"><img src="~/../../images/ourProducts/know-the-team.png" alt="Get To Know The Team"></div>
            
            <div id="alsoLike">
				<div style="float:left; width:300px;">
                	<h3 style="color:#f48025;">Must Haves:</h3>
                </div>
				<div style="float:left; width:300px;">
                	<h3 style="color:#f48025;">Choose Your Cleanser:</h3>
                </div>
                <div class="clear"></div>

                

                <div id="prodCarouselWrapper">

                    <a href="#" class="carouselNav circle disabled pie" id="prev">Previous</a>

                    <div id="prodCarousel">

                        <ul>

                                  <li id="ctl00_ContentPlaceHolder1_theSideBarProducts_ctrl2_theSideBarListItem">

                                      <a id="ctl00_ContentPlaceHolder1_theSideBarProducts_ctrl2_Deep-Cleansing-Pore-Strips" href="/en-US/deep-cleansing-product-family/pore-strips"><img id="ctl00_ContentPlaceHolder1_theSideBarProducts_ctrl2_theSideBarProductImage" src="/en-US/images/ourProducts/products/small/deepCleansingPoreStrips.jpg" style="border-width:0px;" />

                                        <span>Deep Cleansing Pore Strips &raquo;</span>

                                      </a>

                                  </li>

                                  <li id="ctl00_ContentPlaceHolder1_theSideBarProducts_ctrl1_theSideBarListItem" style="border-right:solid 2px #d8d8d8;">

                                      <a id="ctl00_ContentPlaceHolder1_theSideBarProducts_ctrl1_Blemish-Treating-Astringent" href="/en-US/complexion-clearing-products/blemish-treating-astringent"><img id="ctl00_ContentPlaceHolder1_theSideBarProducts_ctrl1_theSideBarProductImage" src="/en-US/images/ourProducts/products/small/blemishTreatingAstringent.jpg" style="border-width:0px;" />

                                        <span>Blemish Treating Astringent &raquo;</span>

                                      </a>

                                  </li>
                                  

                                  <li id="ctl00_ContentPlaceHolder1_theSideBarProducts_ctrl2_theSideBarListItem">

                                      <a id="ctl00_ContentPlaceHolder1_theSideBarProducts_ctrl2_Acne_Clearing_Scrub" href="/en-US/complexion-clearing-products/acne-clearing-scrub"><img id="ctl00_ContentPlaceHolder1_theSideBarProducts_ctrl2_theSideBarProductImage" src="/en-US/images/ourProducts/products/small/acneClearingScrub.jpg" style="border-width:0px;" />

                                        <span>Acne Clearing Scrub &raquo;</span>

                                      </a>

                                  </li>

                                  <li id="ctl00_ContentPlaceHolder1_theSideBarProducts_ctrl1_theSideBarListItem">

                                      <a id="ctl00_ContentPlaceHolder1_theSideBarProducts_ctrl1_Blemish_Fighting_Ice_Cleanser" href="/en-US/complexion-clearing-products/blemish-fighting-ice-cleanser"><img id="ctl00_ContentPlaceHolder1_theSideBarProducts_ctrl1_theSideBarProductImage" src="/en-US/images/ourProducts/products/small/blemishFightingIceCleanser.jpg" style="border-width:0px;" />

                                        <span>Blemish Fighting Ice Cleanser &raquo;</span>

                                      </a>

                                  </li>

                                  <li id="ctl00_ContentPlaceHolder1_theSideBarProducts_ctrl0_theSideBarListItem">

                                      <a id="ctl00_ContentPlaceHolder1_theSideBarProducts_ctrl0_Warming_Anti_Blackhead_Cleanser" href="/en-US/complexion-clearing-products/warming-anti-blackhead-cleanser"><img id="ctl00_ContentPlaceHolder1_theSideBarProducts_ctrl0_theSideBarProductImage" src="/en-US/images/ourProducts/products/small/warmingAntiBlackheadCleanser.jpg" style="border-width:0px;" />

                                        <span>Warming Anti-Blackhead Cleanser &raquo;</span>

                                      </a>

                                  </li>

                                  <li id="ctl00_ContentPlaceHolder1_theSideBarProducts_ctrl0_theSideBarListItem">

                                      <a id="ctl00_ContentPlaceHolder1_theSideBarProducts_ctrl0_Pore-Unclogging-Scrub" href="/en-US/deep-cleansing-products/pore-unclogging-scrub"><img id="ctl00_ContentPlaceHolder1_theSideBarProducts_ctrl0_theSideBarProductImage" src="/en-US/images/ourProducts/products/small/poreUncloggingScrub.jpg" style="border-width:0px;" />

                                        <span>Pore Unclogging Scrub &raquo;</span>

                                      </a>

                                  </li>
                        </ul>	
                        

                    </div>

                    <a href="#" class="carouselNav circle pie" id="next">Next</a>

                </div>

            </div>





            <div class="clear"></div>



            

        </div>

    </div>

</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
<!--script type="text/javascript">
    <asp:Literal ID="thePoreStripsJSON" runat="server" />
</script>-->

<script type="text/javascript" src="/en-US/js/combined_detail_3FA8663A9E9129EDF078286F8D22E3F3.js"></script>

<%=SquishIt.Framework.Bundle .JavaScript()
    .Add("~/js/swfobject.min.js")
    .Add("~/js/jquery.ba-hashchange.min.js")
    .Add("~/js/jquery.touchwipe.min.js")
    .Add("~/js/jquery-ui-1.8.2.custom.min.js")
    .Add("~/js/jquery.ghindaVideoPlayer.js")
    .Add("~/js/productDetail.js")
    .Render("~/js/combined_detail_#.js")
%>

</asp:Content>   