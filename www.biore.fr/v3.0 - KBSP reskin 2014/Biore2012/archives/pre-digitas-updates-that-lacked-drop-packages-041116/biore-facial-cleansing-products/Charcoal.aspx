﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Charcoal.aspx.cs" Inherits="Biore2012.biore_facial_cleansing_products.Charcoal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Purify pores with our Charcoal Mask and Charcoal Face Wash. Try Deep Pore Charcoal Cleanser and Self Heating One Minute Mask from Bioré® Skincare." />
    <meta content="Charcoal Mask, Charcoal Face Wash, charcoal" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/charcoalLanding.css")
        .Render("~/css/combinednew_#.css")
    %>
    <meta name="viewport" content="width=device-width">
    <script type="text/javascript" src='<%= VirtualPathUtility.ToAbsolute("~/js/jquery-1.8.1.min.js") %>'></script>
    <link rel="stylesheet" type="text/css" href="<%= VirtualPathUtility.ToAbsolute("~/css/colorbox.css") %>" />
    <script type="text/javascript" src='<%= VirtualPathUtility.ToAbsolute("~/js/jquery.colorbox.min.js") %>'></script>
    <script type="text/javascript">
        $(function () {
            $('#charTryNowWrap a').click(
                function (e) {
                    e.preventDefault();

                    var axel = Math.random() + "";
                    var a = axel * 10000000000000;
                    var strScript = "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore820;ord='" + a + "'?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"                    
                               + "<noscript>"
                               + "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore820;ord=1?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"
                               + "</noscript>";

                    $('body').prepend(strScript);
                    var target = $(this).attr('target');
                    var uri = $(this).attr('href');

                    setTimeout(function () {
                        if (target) {
                            window.open(uri, '_blank');
                        } else {
                            window.location = uri;
                        }
                    }, 1000);

                });
        })
    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: Biore Whats New Page
URL of the webpage where the tag is expected to be placed: http://www.biore.com/en-US/special-offers-and-skincare-tips/
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 02/01/2012
-->
<script type="text/javascript">
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    document.write('<iframe src="http://fls.doubleclick.net/activityi;src=1418070;type=biore797;cat=biore504;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="http://fls.doubleclick.net/activityi;src=1418070;type=biore797;cat=biore504;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>

<!-- End of DoubleClick Floodlight Tag: Please do not remove -->

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: BIORE_CHARCOAL_HP_PL
URL of the webpage where the tag is expected to be placed: http://www.biore.com/en-US/biore-facial-cleansing-products/charcoal.aspx
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 03/13/2014
-->
<script type="text/javascript">
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    document.write('<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore980;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore980;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>

<!-- End of DoubleClick Floodlight Tag: Please do not remove -->

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: BIORE_WHATSNEW_HP_PL
URL of the webpage where the tag is expected to be placed: http://www.biore.com/en-US/biore-facial-cleansing-products/charcoal.aspx
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 03/05/2014
-->
<script type="text/javascript">
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    document.write('<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore345;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=biore345;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>

<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main" class="charcoalLanding">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
                <div class="charcoalHeaderSpacer"></div>
                <h1><img src="<%= VirtualPathUtility.ToAbsolute("~/images/charcoalLanding/charcoalHeader.png") %>" border="0" alt="" /></h1>
                
            <div id="charcoalProdWrap">
                <div id="charDescript">
                    <div id="charPaint"></div>
                    <!--<h2 class="archer-book">Yes, <span class="charGrey archer-bold">CHARCOAL!</span></h2>-->
                    <p>Charcoal sucks up deep-down dirt, oil and impurities. We like to think of it as a gunk-magnet. Originally found in nature, now you can find it in all three of the new products in our charcoal line. </p>
                </div>
                <div id="charProducts"></div>
            </div>

            <div id="charTryNowWrap">
                <div id="charTryProd">
                <div class="wrapper-image">
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/dont-be-dirty/charcoal-pore-strips") %>"><img src="../images/charcoalLanding/deepCleansingCharcoalPoreStrips.png" alt="6 Deep Cleansing Charcoal Pore Strips" /></a>
                    <!--<a href="http://strip.biore.com" class="second-link"></a>-->
                </div>
                    <p><b><a href="<%= VirtualPathUtility.ToAbsolute("~/dont-be-dirty/charcoal-pore-strips") %>" class="charCharcoal">DEEP CLEANSING<br />
CHARCOAL PORE STRIPS</a></b><br />
                Degunk clogged pores with the oil-absorbing power of these charcoal strips.</p>
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/dont-be-dirty/charcoal-pore-strips#adsense") %>" id="charcoal-pore-strips"><div class="charTry">Buy Now </div></a>
                </div>
                
                <div id="charTryProd" class="poreMinimizer">
                <a href="<%= VirtualPathUtility.ToAbsolute("~/back-off-big-pores/charcoal-pore-minimizer") %>"><img src="../images/charcoalLanding/charcoalPoreMinimizer.png" alt="Charcoal Pore Minimizer" /></a>
                <p><b><a href="<%= VirtualPathUtility.ToAbsolute("~/back-off-big-pores/charcoal-pore-minimizer") %>" class="charTeal">CHARCOAL<br />
PORE MINIMIZER</a></b><br />
                This gentle exfoliator cleans deep down to reduce the appearance of pores. </p>
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/back-off-big-pores/charcoal-pore-minimizer#adsense") %>"id="charcoal-pore-minimizer"><div class="charTry">Buy Now </div></a>
                </div>
                
                <div id="charTryProd" class="charcoalBar">
                <a href="<%= VirtualPathUtility.ToAbsolute("~/dont-be-dirty/charcoal-bar") %>"><img src="../images/charcoalLanding/charcoalPenetratingCharcoalBar.png" alt="Pore Penetrating Charcoal Bar" /></a>
                <p><b><a href="<%= VirtualPathUtility.ToAbsolute("~/dont-be-dirty/charcoal-bar") %>" class="charGreeen">PORE PENETRATING<br />
CHARCOAL BAR</a></b><br />
                Infused with charcoal, this cleansing bar gets pores so clean they tingle. </p>
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/dont-be-dirty/charcoal-bar#adsense") %>" id="pore-penetrating-charcoal-bar"><div class="charTry">Buy Now </div></a>
                </div>
                
                 <div id="charTryProd" class="charcoalCleanser">
                <a href="<%= VirtualPathUtility.ToAbsolute("~/dont-be-dirty/deep-pore-charcoal-cleanser") %>"><img src="../images/charcoalLanding/deepPoreCharcoalCleanser.png" alt="Deep Pore Charcoal Cleanser" /></a>
                <p><b><a href="<%= VirtualPathUtility.ToAbsolute("~/dont-be-dirty/deep-pore-charcoal-cleanser") %>" class="charGreeen">DEEP PORE<br />
CHARCOAL CLEANSER</a></b><br />
                Trap 2x more surface toxins* while instantly deep cleaning all 200,000 of your pores. </p>
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/dont-be-dirty/deep-pore-charcoal-cleanser#adsense") %>" id="deep-pore-charcoal-cleanser"><div class="charTry">Buy Now </div></a>
                </div>
                
                <div id="charTryProd" class="charcoalMask">
                <a href="<%= VirtualPathUtility.ToAbsolute("~/dont-be-dirty/self-heating-one-minute-mask") %>"><img src="../images/charcoalLanding/selfHeatingCharcoalMask.png" alt="Self Heating One Minute Mask" /></a>
                <p><b><a href="<%= VirtualPathUtility.ToAbsolute("~/dont-be-dirty/self-heating-one-minute-mask") %>" class="charGreeen">SELF-HEATING<br />
ONE-MINUTE MASK</a></b><br />
                Draw out 2.5x* more surface toxins while leaving your skin feeling tingly-smooth. </p>
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/dont-be-dirty/self-heating-one-minute-mask#adsense") %>" id="self-heating-one-minute-mask"><div class="charTry">Buy Now </div></a>
                </div>
            	<div class="clear"></div>
            </div>
            <div id="charFootnote">
           <p>*Compared to a basic cleanser.</p>
           </div>
        </div>
    </div>
	<div class="clear"></div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
