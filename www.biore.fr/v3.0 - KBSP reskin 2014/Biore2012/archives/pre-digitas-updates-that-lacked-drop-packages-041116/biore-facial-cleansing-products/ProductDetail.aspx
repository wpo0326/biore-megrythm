﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductDetail.aspx.cs" Inherits="Biore2012.our_products.ProductDetail" %>
<%@ Register Src="~/BazaarVoice/Controls/BVScripts.ascx" TagPrefix="uc1" TagName="BVScripts" %>
<%@ Register Src="~/BazaarVoice/Controls/BVSEO.ascx" TagPrefix="uc1" TagName="BVSEO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/productDetail.css")
        .Add("~/css/ghindaVideoPlayer.css")
        .Add("~/css/colorbox.css")
        .Render("~/css/combined_detail_#.css")
        
    %>
    <meta name="viewport" content="width=device-width">
    <meta name="description" id="metaDescription" runat="server" />
    <meta name="keywords" id="metaKeywords" runat="server" />
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="x-rim-auto-match" content="none">
    <script type="text/javascript" src='<%= VirtualPathUtility.ToAbsolute("~/BazaarVoice/js/BVscripts.js") %>'></script>
    <script type="text/javascript" src='<%= VirtualPathUtility.ToAbsolute("~/js/jquery-1.8.1.min.js") %>'></script>
    <script type="text/javascript" src='<%= VirtualPathUtility.ToAbsolute("~/js/jquery.colorbox.min.js") %>'></script>
    <script async defer src="//assets.pinterest.com/js/pinit.js"></script> 
    <uc1:BVScripts runat="server" ID="BVScripts" />
	<script type="text/javascript">
	    $(function() {
	        $(".buyNow").click(
	            function (e) {
	                e.preventDefault();
	                var name=$("#ctl00_ContentPlaceHolder1_BVProductId").val();
	                if (name)
    	                BVRoiBeacon(name);
	                location.href = "#adsense";
	            });
	    });
    </script>
    
	
    <%--<script type="text/javascript">
        $(function() {

            var $window = $(window);

            function checkWidth() {
                
                

                var windowsize = $window.width();

                //Devices above 1000 pixels
                if (windowsize > 1000) {
                    $(".buyNow").colorbox({width:"980px", height:"600px", inline:true, href:"#<%=divBuyNowBox.ClientID %>"});
                }

                //Devices between 480 & 1000 pixels
                if (windowsize > 480 && windowsize < 1000) {
                    $(".buyNow").colorbox({width:"460px", height:"600px", inline:true, href:"#<%=divBuyNowBox.ClientID %>"});
                }

                //Devices below 480 pixels
                if (windowsize <= 1000) {
                    //$('.buyNow').click(function(){
                        //var url=$("#<%=buyNowIFrame.ClientID %>").attr('src');
                        //window.open(url, 'popupWindow');
                        //$("body").append($(this).attr("href"));
                        //return false;
                    //});
                    //$(".buyNow").colorbox({width:"320px", height:"480px", inline:true, href:"#<%=divBuyNowBox.ClientID %>"});
                    
                }
            }

            checkWidth();

            // listen for resizing
            $(window).resize(checkWidth);


           
        });
    </script>--%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">

    <asp:Repeater ID="rptFloodLight" runat="server" >
        <ItemTemplate>

<!-- Start of DoubleClick Floodlight Tag: Please do not remove ><%# Eval("Description") %> -->
<script type="text/javascript">
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    document.write('<iframe src="<%# Eval("IframeSource") %>' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="<%# Eval("IframeSource") %>1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
        
      </ItemTemplate>
    </asp:Repeater>
    
     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div style="display:none">
    <div id="divBuyNowBox" style="height:100%;" runat="server">
        <iframe id="buyNowIFrame" runat="server" scrolling="no"  />
    </div>
</div>
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="col floatRight" id="productName">
                <h2 class="pie archer-medium <asp:literal ID="theCategoryClass" runat="server" />"><span class="spanCateloryLeft"><asp:literal ID="theCategoryLeft" runat="server" /></span><span class="spanCategoryRight"><asp:literal ID="theCategoryRight" runat="server" /></span></h2>
                <h1 class="h1ProductName"><span id="spanNamePrefix" runat="server"><asp:literal ID="theNamePrefix" runat="server" /></span><span class="spanProductName"><span class="spanProductNameLine1"><asp:literal ID="ProductNameL1" runat="server" /></span><span class="spanProductNameLine2"><asp:literal ID="ProductNameL2" runat="server" /></span><span class="spanProductNameLine3"><asp:literal ID="ProductNameL3" runat="server" /></span></span></h1>
                <p class="descript archer-medium"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/ourProducts/productDetail/productRule.png") %>" /><br />
                    <span class="new"><asp:literal ID="thePromoCopy" runat="server" /><asp:literal ID="theDescription" runat="server" /></span> 
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/ourProducts/productDetail/productRule.png") %>" />
                </p><br />
                <div id="BVRRSummaryContainer"></div>
                <asp:HiddenField  ID="BVProductId" runat="server" />
                <!--<asp:LinkButton ID="theBuyNowLink" runat="server" CssClass="buyNow">Buy Now</asp:LinkButton>-->
                <a href="#" id="theBuyNowLink" class="buyNow">Buy Now</a>
                <div class="plusBtn" id="plusBtnDiv"><asp:Literal ID="plusButton" runat="server" /></div>
                <div class="likeBtn" id="likeBtnDiv"><asp:Literal ID="fbLike" runat="server" /></div>   
                <div class="pinterestBtn"><a data-pin-do="buttonBookmark" href="https://www.pinterest.com/pin/create/button/"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a></div> 
                <div class="clear"></div>           
            </div>
            <div class="col" id="productImages">
                <div class="prodImgHolder on" id="newProd">
                        <asp:Image ID="theNewImage" runat="server" />
                </div>
                <div class="prodImgHolder" id="oldProd">
                    <asp:Image ID="theOldImage" runat="server" /> 
                </div>
                <asp:Panel ID="oldNewControlPanel" runat="server" Visible="true">
                <ul class="prodLookNav hide">
                    <li><a href="#newProd" class="on">New Look <span class="pie circleNewOld circleLeft"></span></a></li>
                    <li><a href="#oldProd">Old Look <span class="pie circleNewOld circleRight"></span></a></li>
                </ul>
                </asp:Panel>
            </div>
            <asp:Panel ID="thePoreStripNavPanel" CssClass="col floatRight" runat="server" Visible="false">
                <ul id="poreStripNav">
                    <li class="poreStripNav selected" id="regularNav">
                        <div class="imgWrapper">
                            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/ourProducts/productDetail/poreStripNavRegular.png") %>" />
                            
                            <a class="poreNav" href="pore-strips">
                                <div class="pie">
                                    <span class="dc">Deep Cleansing</span>
                                    <span class="ps pie">Pore Strips</span>
                                </div>
                            </a>
                        </div>                        
                    </li>
                    <li class="poreStripNav" id="comboNav">
                        <div class="imgWrapper">
                            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/ourProducts/productDetail/poreStripNavCombo.png") %>" />
                            <span class="pie roundedBoxBg"></span>
                            <a class="poreNav" href="pore-strips-combo">
                                <div class="pie">
                                    <span class="dc">Deep Cleansing</span>
                                    <span class="ps pie">Pore Strips Combo</span>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li class="poreStripNav" id="ultraNav">
                        <div class="imgWrapper">
                            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/ourProducts/productDetail/poreStripNavUltra.png") %>" />
                            
                            <a class="poreNav" href="pore-strips-ultra">
                                <div class="pie">
                                    <span class="dc">Deep Cleansing</span>
                                    <span class="ps pie">Pore Strips Ultra</span>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li class="clear"></li>
                </ul>
                </asp:Panel>
            <div class="col floatRight" id="productInfo">
                <div class="contentHolder open">
                    <h3 class="pie archer-book">About <span class="pie circle"></span></h3>
                    <div class="collapsibleContent ProximaNova-Regular" id="whatItIsContent">
                        <asp:Literal ID="theWhatItIs" runat="server" />
                    </div>
                </div>

                <div class="contentHolder">
                    <h3 class="pie archer-book">Product Details <span class="pie circle"></span></h3>
                    <div class="collapsibleContent ProximaNova-Regular" id="ingredientsContent">
                        <asp:Literal ID="theIngredients" runat="server" />
                    </div>
                </div>
              <!--  <asp:Panel ID="theFunFactsPanel" runat="server" Visible="true">
                <div class="contentHolder">
                    <h3 class="pie archer-book">Fun Facts <span class="pie circle"></span></h3>
                    <div class="collapsibleContent ProximaNova-Regular" id="funFactsContent">
                        <asp:Literal ID="theFunFacts" runat="server" />
                    </div>
                </div>
                </asp:Panel>-->
                <div class="clear"></div>

                <div class="contentHolder">
                    <h3 class="pie archer-book">how it works <span class="pie circle"></span></h3>
                    <div class="collapsibleContent ProximaNova-Regular" id="howItWorksContent">
                        <asp:Literal ID="theHowItWorks" runat="server" />
                        <!--<h4>Cautions</h4>
                        <asp:Literal ID="theCautions" runat="server" /> -->
                    </div>
                </div>
                <!-- <asp:Panel ID="theVideoPanel" runat="server" visible="true">
                <div class="videoHolder">
                    <div class="video pie">
                        <div id="lbVideo">
                            <asp:Hyperlink ID="theVideoLink" runat="server" Target="_blank" CssClass="videoLink" >
                                <asp:Image ID="theVideoStillImage" runat="server" />
                            </asp:Hyperlink>
                        </div>
                    </div>
                </div>
                </asp:Panel> -->
                 <div class="contentHolder tabReviews">
                    <h3 class="pie archer-book">Reviews<span class="pie circle"></span></h3>
                    <div class="collapsibleContent ProximaNova-Regular" id="reviewsContent">
                        <div id="BVRRContainer"></div>                    
                    </div>
                </div>
                <asp:Panel ID="adsensePanel" runat="server">
                    <asp:Literal ID="theAdsenseContent" runat="server"></asp:Literal>
                </asp:Panel>
                    
            </div>
            <div class="col" id="alsoLike">
                <h3><asp:literal ID="theSidebarHeader" runat="server"></asp:literal></h3>
                <asp:literal runat="server" id="theSidebarCopy"></asp:literal>
                <div id="prodCarouselWrapper">
                    <a href="#" class="carouselNav circle disabled pie" id="prev">Previous</a>
                    <div id="prodCarousel">
                        <ul>
                            <asp:ListView ID="theSideBarProducts" runat="server" OnItemDataBound="sidebar_ItemDataBound">
                                <LayoutTemplate>
                                    <li runat="server" id="itemPlaceholder"></li>
                                </LayoutTemplate>
                                <ItemTemplate>
                                  <li runat="server" id="theSideBarListItem">
                                      <asp:HyperLink ID="theSideBarProductLink" runat="server">
                                        <asp:Image ID="theSideBarProductImage" runat="server" />
                                         <span class="archer-book"> <asp:Literal ID="theSideBarProductName" runat="server" /> </span> <div id="divarrowSmallProduct" runat="server"></div>
                                      </asp:HyperLink>
                                  </li>
                                   
                                </ItemTemplate>
                            </asp:ListView>
                        </ul>
                    </div>
                    <a href="#" class="carouselNav circle pie" id="next">Next</a>
                </div>
            </div>


            <div class="clear"></div>

            <asp:Literal ID="theOutput" runat="server"></asp:Literal>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
<script type="text/javascript">
    <asp:Literal ID="thePoreStripsJSON" runat="server" />
</script>

<%=SquishIt.Framework.Bundle .JavaScript()
    .Add("~/js/swfobject.min.js")
    .Add("~/js/jquery.ba-hashchange.min.js")
    .Add("~/js/jquery.touchwipe.min.js")
    .Add("~/js/jquery-ui-1.8.2.custom.min.js")
    .Add("~/js/jquery.ghindaVideoPlayer.js")
    .Add("~/js/productDetail.js")
    .Render("~/js/combined_detail_#.js")
%>
   <uc1:BVSEO runat="server" ID="BVSEO" />
</asp:Content>   