﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KAOForms;


namespace KaoBrands.FormStuff
{
    public partial class ucFormMemberInfo : System.Web.UI.UserControl
    {
        private int _minimumAge;

        public int minimumAge
        {
            get
            {               
                return _minimumAge; 
            }
            set
            {
                _minimumAge = value;
            }
        }

        public TextBox _txFirstName { get { return this.FName; } }
        public TextBox _txLastName { get{ return this.LName; } }
        public TextBox _txEmail { get { return this.Email; } }
        public TextBox _txAddress1 { get { return this.Address1; } }
        public TextBox _txAddress2 { get { return this.Address2; } }
        public TextBox _txAddress3 { get { return this.Address3; } }
        public TextBox _txCity { get { return this.City; } }
        public DropDownList _ddState { get { return this.State; } }
        public TextBox _txPostalCode { get { return this.PostalCode; } }
        public TextBox _txPhone { get { return this.Phone; } }
        public DropDownList _ddGender { get { return this.Gender; } }
        public DropDownList _ddMonth { get { return this.mm; } }
        public DropDownList _ddDay { get { return this.dd; } }
        public DropDownList _ddYear { get { return this.yyyy; } }
        public TextBox _txUpcCode { get { return this.UPC_Code; } }
        public TextBox _txMfgCode { get { return this.Mfg_Code; } }
        public TextBox _txComment { get { return this.QuestionComment; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                setDateValues();

                ///////////////////////////////////////////////////////////////////////////////////
                // CUSTOM CODE TO PREPOPULATE FIRST NAME, LAST NAME AND/OR ZIP CODE
                if (!String.IsNullOrEmpty(Request.QueryString["FirstNamePP"])) _txFirstName.Text = Request.QueryString["FirstNamePP"];
                if (!String.IsNullOrEmpty(Request.QueryString["LastNamePP"])) _txLastName.Text = Request.QueryString["LastNamePP"];
                if (!String.IsNullOrEmpty(Request.QueryString["ZIPCodePP"])) _txPostalCode.Text = Request.QueryString["ZIPCodePP"];
                ///////////////////////////////////////////////////////////////////////////////////
            }
            else
            {
               
            }
        }

        private void setDateValues()
        {
            DateTime dt = DateTime.Now;
            FormUtils ut = new FormUtils();
            ut.queueNumber(dt.Year - 100, dt.Year - _minimumAge, "Year", yyyy); //Build Year
            ut.queueNumber(1, 13, "Mon", mm); //Build Day
            ut.queueNumber(1, 32, "Day", dd); //Build Month
        }

        protected void DOBValidate(object source, ServerValidateEventArgs args)
        {
            string dob = mm.SelectedValue.ToString() + "/" + dd.SelectedValue.ToString() + "/" + yyyy.SelectedValue.ToString();
            try
            {
                DateTime.Parse(dob);
                args.IsValid = true;
            }
            catch
            {
                args.IsValid = false;
            }
        }

        protected void DOBValidateMinAge(object source, ServerValidateEventArgs args)
        {
           
            string dob = mm.SelectedValue.ToString() + "/" + dd.SelectedValue.ToString() + "/" + yyyy.SelectedValue.ToString();
            try
            {
                DateTime DoB = DateTime.Parse(dob);
                if (calculateAgeInYears(DoB, DateTime.Now) >= _minimumAge)
                {
                    args.IsValid = true;
                }
                else
                {
                    args.IsValid = false;
                    DOBMinAgeValidator5.ErrorMessage = "You must be at least " + _minimumAge + " years old to participate.";
                }

            }
            catch
            {
                args.IsValid = false;
            }           
        
        }

        protected int calculateAgeInYears(DateTime dateOfBirth, DateTime dateToCalculateAge)
        {
            int ageCalculation = -1;
            if (dateOfBirth != null && dateToCalculateAge != null)
            {
                int DoB_DayOfYear = dateOfBirth.DayOfYear;
                int DateNow_DayOfYear = dateToCalculateAge.DayOfYear;

                // Adjust DayOfYear values to allow for Leap Year comparison - e.g. make March 1st always Day 61 in this calculation
                if (!DateTime.IsLeapYear(dateOfBirth.Year) && dateOfBirth.Month > 2) DoB_DayOfYear++;
                if (!DateTime.IsLeapYear(dateToCalculateAge.Year) && dateOfBirth.Month > 2) DateNow_DayOfYear++;

                ageCalculation = dateToCalculateAge.Year - dateOfBirth.Year;
                if (DoB_DayOfYear > DateNow_DayOfYear)
                {
                    // Have not had their birthday yet
                    ageCalculation--;
                }

            }
            return ageCalculation;
        }

       
    }
}