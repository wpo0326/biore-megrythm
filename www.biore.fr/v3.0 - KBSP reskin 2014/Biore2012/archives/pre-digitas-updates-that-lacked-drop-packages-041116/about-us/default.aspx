﻿<%@ Page Title="About Bior&eacute;&reg; Skincare | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.about_us._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Learn more about Bioré® Skincare" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div class="centeringDiv">
                <div id="content">
                    <div class="titleh1">
                        <h1>About Bior&eacute;<sup>&reg;</sup> Skincare</h1>
                        <div id="responseRule"></div>
                    </div>
                    <img src="../images/about/aboutBG.png" alt="" id="Img1" />
                    <p>Bior&eacute;<sup>&reg;</sup> Skincare targets the root of all skin problems &mdash; the evil clogged pore. Our powerful, pore&ndash;cleansing products come in liquid, foam, scrub, and strip forms so you can remove pore&ndash;clogging debris! Go from work to working out, then for some weekend fun with skin that&#8217;s always beautiful. </p>
                    <p class="boldBlue">Bior&eacute;<sup>&reg;</sup> Skincare products are available at select food, drug and mass&ndash;merchant stores</p>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

