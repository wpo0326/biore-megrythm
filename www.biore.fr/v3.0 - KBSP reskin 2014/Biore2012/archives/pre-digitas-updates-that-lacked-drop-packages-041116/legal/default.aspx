﻿<%@ Page Title="Legal | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
    <style type="text/css">
    #content p {text-align:left;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="about">
 <div id="main">
        <div id="mainContent">
            <div class="centeringDiv">
                <div id="content">
                    <div class="titleh1">
                        <h1>Bior&eacute;<sup>&reg;</sup> Legal</h1>
                        <div id="responseRule"></div>
                    </div>
                 <p>© 2016 Kao Canada Inc. ("Kao"). All rights reserved. </p><br />
                 <p>BY USING THIS WEB SITE YOU UNDERSTAND AND AGREE THAT:</p><br />
                 
                 <p>Information contained on this Web Site (the "Site") concerning any products or services is only applicable in the United States and is for your personal and non-commercial use only. All titles, names and graphics used in connection with the Site are trademarks of Kao or its licensors, except as otherwise indicated. All materials contained on the Site are the copyrighted property of Kao or its licensors. Reproduction, republication or distribution of any material from this Site beyond what is required to enable you to view the Site is strictly prohibited, except that the user may, for personal and non-commercial use only, download and make a single hard copy. Information regarding Kao products and services is subject to change without notice. Some products and services may not be available in certain areas. The information contained on this Site is intended for general information purposes only. The views and opinions expressed on the Site are not necessarily those of Kao. Mention of products and/or companies should not be understood as an endorsement of, or a statement of affiliation with, such products and/or companies.</p>
                
                 
</asp:Content>
                