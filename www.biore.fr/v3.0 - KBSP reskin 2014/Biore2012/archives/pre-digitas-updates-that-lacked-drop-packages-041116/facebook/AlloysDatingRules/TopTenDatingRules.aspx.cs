﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;

namespace Biore2012.facebook.AlloysDatingRules
{
	public partial class TopTenDatingRules : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			//InitSlider      
			List<DatingRule> drs = DatingRule.GetDatingRules();
			DatingRule centerGrid = new DatingRule();
			centerGrid.ID = "99";
			centerGrid.Name = "centerGrid";
			centerGrid.Description = "Need more advice?  Tune in to: Dating Rules From My Future Self <span>Watch all the episodes at Facebook.com/DatingRules</span>";

			drs = (List<DatingRule>)RandomPermutation(drs);
			drs.Add(centerGrid);
			
			lvGrid.DataSource = drs;			
			lvGrid.DataBind();
		}

		protected void lvGrid_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
		    ListViewDataItem dataItem = (ListViewDataItem)e.Item;
		    ListView lv = (ListView)sender;

		    List<DatingRule> myData = (List<DatingRule>)lv.DataSource;

		    DatingRule dr = myData[dataItem.DataItemIndex];

		    HyperLink imglink = (HyperLink)e.Item.FindControl("hlGridItem");
			imglink.NavigateUrl = "#" + dr.ID;
		    imglink.Text = dr.Name;

		    Image pImage = new Image();

			if (dr.ID == "99")
			{
				pImage.ImageUrl = "images/centerBlock.png";
				
				imglink.NavigateUrl = "http://facebook.com/datingrules";
				imglink.Target = "_top";
				imglink.CssClass = "centerBlock";

				HtmlGenericControl overlayDiv = (HtmlGenericControl)e.Item.FindControl("dvOverlay");
				overlayDiv.Visible = false;

				Literal centerBlockText = (Literal)e.Item.FindControl("litCenterBlockText");
				centerBlockText.Text = "<a href='http://facebook.com/datingrules' target='_top' class='drPromoText'>Watch all the episodes at <strong>Facebook.com/DatingRules</strong></a>";
				
				Literal addClosingLI = (Literal)e.Item.FindControl("litLastLI");
				addClosingLI.Text = "</li>";
			}
			else
			{
				pImage.ImageUrl = "images/gridImage_" + dr.ID + ".jpg";

				imglink.Attributes.Add("rel", (dr.UpVotes - dr.DownVotes).ToString());

				Image imgStatus = new Image();
				if (dr.UpVotes >= dr.DownVotes)
				{
					imgStatus.ImageUrl = "images/voteUpBtn.png";
				}
				else
				{
					imgStatus.ImageUrl = "images/voteDownBtn.png";
				}
				imgStatus.CssClass = "voteStatus";
				imglink.Controls.Add(imgStatus);
			}

			imglink.Controls.Add(pImage);
			buildOverlay(dr, e);
		}

		private void buildOverlay(DatingRule dr, ListViewItemEventArgs e)
		{
			Image oImage = (Image)e.Item.FindControl("imgOverlay");
			oImage.ImageUrl = "images/gridImageLarge_" + dr.ID + ".jpg";

			Literal lDesc = (Literal)e.Item.FindControl("litDescription");
			lDesc.Text = dr.Description;

			HyperLink luvItlink = (HyperLink)e.Item.FindControl("hlLuvIt");
			luvItlink.NavigateUrl = "#" + dr.ID;

			HyperLink sharelink = (HyperLink)e.Item.FindControl("hlShare");
			sharelink.NavigateUrl = "#" + dr.ID;

			HyperLink mehlink = (HyperLink)e.Item.FindControl("hlMeh");
			mehlink.NavigateUrl = "#" + dr.ID;
		}

		public static List<T> RandomPermutation<T>(IEnumerable<T> sequence)
		{
			List<T> retList = new List<T>();
			int Length = sequence.Count();
			T[] retArray = sequence.ToArray();
			Random random = new Random();
			for (int i = 0; i < Length; i += 1)
			{
				int swapIndex = random.Next(i, Length);
				if (swapIndex != i)
				{
					T temp = retArray[i];
					retArray[i] = retArray[swapIndex];
					retArray[swapIndex] = temp;
				}
			}

			foreach (T item in retArray)
			{
				retList.Add(item);	
			}

			return retList;
		}
	}



	class DatingRule
	{
		public string ID { get; set; }
		public string Name { get; set; }
		public int UpVotes { get; set; }
		public int DownVotes { get; set; }
		public string Description { get; set; }

		public DatingRule()
        {

        }

		public static List<DatingRule> GetDatingRules() 
		{
			List<DatingRule> DatingRules = new List<DatingRule>();

			// Make Database call to get rules and assign to object
			DataSet drDs = DatingRule.GetDatingRulesDAO();
			
			// Loop through the dataset and create a list of objects
			
			foreach (DataRow row in drDs.Tables[0].Rows)
			{
				DatingRule dr = new DatingRule();
				dr.ID = (string)row["ID"];
				dr.Name = (string)row["Name"];
				dr.UpVotes = (int)row["UpVotes"];
				dr.DownVotes = (int)row["DownVotes"];
				dr.Description = (string)row["Description"];

				DatingRules.Add(dr);
			}
			return DatingRules;
		}

		private static string createDatingRulesJSON()
		{
			String drJson = "";
			
			// Deserialize object to JSON
			JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
			drJson = jsSerializer.Serialize(DatingRule.GetDatingRules());

			return drJson;
		}

		private static DataSet GetDatingRulesDAO()
		{
			string connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbConnection"].ConnectionString;
			DataSet ds = new DataSet();
			try
			{
				using (SqlConnection con = new SqlConnection(connString))
				{

					SqlCommand cmd = con.CreateCommand();
					cmd.CommandText = "sp201202FBDatingRulesTopTen_GetRules";
					cmd.CommandType = CommandType.StoredProcedure;

					using (SqlDataAdapter myAdapt = new SqlDataAdapter(cmd))
					{
						con.Open();
						myAdapt.Fill(ds, "201202FBDatingRulesTopTen");
					}
				}
			}
			catch (Exception ex)
			{
				string error = ex.Message;
				throw;
			}


			return ds;
		}
	}
}
