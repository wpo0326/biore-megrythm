﻿$(document).ready(function() {
    productDetail.init();

    setTimeout(function () {
        $('.plusBtn, .likeBtn, .pinterestBtn').show()
    },1500);
    
});

var productDetail = {
    updatePlusOne: false,
    init: function () {
        productDetail.customDots();
        productDetail.initEvents();
        productDetail.checkHashAndUpdate();
        //productDetail.LinkToBrittany();
        if ($(".ie7").length == 0) {
            $(window).hashchange(function() {
                productDetail.updatePlusOne = false;
                productDetail.checkHashAndUpdate();
            });
        }
    },
    initEvents: function() {

        // add hide class for product detail
        $(".collapsibleContent").addClass("hide");

        $(".open, .prodLookNav").removeClass("hide");


        // click event for accordian on Product Detail
        $(".contentHolder h3").click(function() {
            global.showHideContent(this, $(".contentHolder"), $(this).parent(), false);
        });

        // click event for old / new product show / hide on Product Detail
        $(".prodLookNav a").click(function(e) {
            e.preventDefault();
            var elementClicked = $(this).attr("href");
            $(".prodImgHolder, .prodLookNav a").removeClass("on");
            $(this).addClass("on");
            $(elementClicked).addClass("on");
        });

        // subnav for pore strip
        /*$(".poreNav").click(function(e) {
            e.preventDefault();
            var href = $(this).attr("href").replace("#", "");
            productDetail.updatePage(window["product" + href]);
            $(".poreStripNav").removeClass("selected");
            $(this).parent().parent().addClass("selected");
            window.location.hash = href.toLowerCase();
        });*/

        $(".carouselNav").click(function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "next") {
                $("#prodCarousel ul").addClass("moreLeft");
                $("#prodCarouselWrapper #prev").removeClass("disabled");
            }
            else {
                $("#prodCarousel ul").removeClass("moreLeft");
                $("#prodCarouselWrapper #next").removeClass("disabled");
            }
            $(this).addClass("disabled");
        });

        $("#prodCarousel").touchwipe({
            wipeLeft: function() {
                $("#prodCarousel ul").addClass("moreLeft");
                $("#prodCarouselWrapper #prev").removeClass("disabled");
                $("#prodCarouselWrapper #next").addClass("disabled");
            },
            wipeRight: function() {
                $("#prodCarousel ul").removeClass("moreLeft");
                $("#prodCarouselWrapper #next").removeClass("disabled");
                $("#prodCarouselWrapper #prev").addClass("disabled");
            },
            preventDefaultEvents: false
        });
    },
    checkHashAndUpdate: function() {
        var location = window.location.href;
        var hashIndex = location.indexOf("#");
        var poreStripIndex = location.indexOf("pore-strips");
        if (hashIndex >= 0) {
            var hash = location.substring(hashIndex + 1, location.length);
            $("#" + hash + "Nav a").click();
        }
        else if (poreStripIndex >= 0) {
            //window.location.hash = "regular";
        }
    },
    updatePage: function(theProduct) {
        var localPath = productDetail.getLocalPath();
        var location = "";
        if (window.location.port != '18570') {  // Hack to allow dev in VS
            location = "/" + localPath;
        }

        var locationHref = window.location.href;
        var hashIndex = locationHref.indexOf("#");
        if (hashIndex >= 0) {
            $("#ctl00_ContentPlaceHolder1_theNewImage").attr("src", theProduct["NewLookImage"].replace("~", location));
            $("#ctl00_ContentPlaceHolder1_theOldImage").attr("src", theProduct["OldLookImage"].replace("~", location));
        }

        $("#productName h1 .spanProductName").html(theProduct["Name"]);
        $("#productName p .new").html(theProduct["Description"]);
        $("#productName p .new").html(theProduct["Description"]);
        $("#whatItIsContent").html(theProduct["WhatItIs"]);
        $("#ingredientsContent").html(theProduct["ProductDetails"]);
        $("#funFactsContent").html(theProduct["FunFacts"]);
        //$("#howItWorksContent").html(theProduct["HowItWorks"] + "<h4>Cautions</h4>" + theProduct["Cautions"]);
        $("#howItWorksContent").html(theProduct["HowItWorks"]);
        $("#ctl00_ContentPlaceHolder1_theBuyNowLink").attr("href", theProduct["BuyNowURL"]);
        $("#ctl00_ContentPlaceHolder1_adsensePanel").html(theProduct["AdSense"]);
        
        if (window.location.port != '18570') {
            $("#ctl00_ContentPlaceHolder1_buyNowIFrame").attr("src", '/en-US/buynow/BuyNow.aspx?RefName=' + encodeURIComponent(window.location.href.substring(window.location.href.lastIndexOf('/') + 1)));
        } else {
            $("#ctl00_ContentPlaceHolder1_buyNowIFrame").attr("src", '/en-US/buynow/BuyNow.aspx?RefName=' + encodeURIComponent(window.location.href.substring(window.location.href.lastIndexOf('/') + 1)));
        }
        
        productDetail.updateTrackingIframe(theProduct["TrackingIframeSrc"]);
        productDetail.updateLikeBtn(theProduct);
        if ($(".ie7").length == 0 && !productDetail.updatePlusOne) {
            productDetail.updatePlusOneBtn(theProduct);
        }
    },
    updateLikeBtn: function(theProduct) {
        var nUrl = theProduct["LikeURL"].replace("~", "");
        var hostname = window.location.hostname;
        var localPath = productDetail.getLocalPath();
        $('#likeBtnDiv').html('<div class="fb-like" data-href="http://' + hostname + "/" + localPath + nUrl + '/" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false"></div>');
        if (typeof FB != "undefined") {
            FB.XFBML.parse(document.getElementById('likeBtnDiv'));
        }
    },
    updatePlusOneBtn: function(theProduct) {
        productDetail.updatePlusOne = true;
        var nUrl = theProduct["LikeURL"].replace("~", "");
        var hostname = window.location.hostname;
        var localPath = productDetail.getLocalPath();
        $('#plusBtnDiv div:first').html('<g:plusone></g:plusone>');
        if (typeof gapi != "undefined") {
            setTimeout(function() {
                gapi.plusone.render($('#plusBtnDiv div').get(0), { "annotation": "none", "href": "http://" + hostname + '/' + localPath + nUrl + "/", "size": "medium" });
            }, 0);
        }
    },
    getLocalPath: function() {
        var windowLocation = window.location.href;
        var hostname = window.location.hostname;
        var substringLocation = windowLocation.substring(windowLocation.indexOf(hostname) + hostname.length + 1, windowLocation.length);
        substringLocation = substringLocation.substring(0, substringLocation.indexOf("/"));
        return substringLocation;
    },
    socialScriptLoaded: function(scriptID) {
        if (scriptID == "gplus1js") {
            var location = window.location.href;
            var hashIndex = location.indexOf("#");
            if (hashIndex >= 0) {
                var hash = location.substring(hashIndex + 1, location.length);
                var uppercaseHash = hash.substring(0, 1).toUpperCase();
                uppercaseHash = uppercaseHash + hash.substring(1, hash.length);
                if ($(".ie7").length == 0 && !productDetail.updatePlusOne) {
                    productDetail.updatePlusOneBtn(window["product" + uppercaseHash]);
                }
            }
        }
    },
    updateTrackingIframe: function (iframeSrc) {
        var axel = Math.random() + "";
        var a = axel * 10000000000000;
        var strScript = "<iframe src=\""+iframeSrc + a + "'?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"
                   + "<noscript>"
                   + "<iframe src=\""+iframeSrc + "1?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"
                   + "</noscript>";

        $('body').prepend(strScript);        
    },
    customDots: function () {
        // This work for add class to page that has custom dots differents to old and news dots. Add page on array that want to be custom dots class
        var products = ['charcoal-pore-strips'],
            product = $('#aspnetForm').attr('action');

        for (var i = 0; i < products.length; i++) {
            if (products[i] == product) {
                $('body').addClass('customDots');
                break;
            }
        }


    },
    // Remove this fucntion when brittany snow image remove on page detail 
    /*LinkToBrittany: function () {
        var product = $('#aspnetForm').attr('action'),
        imageLink = $('#newProd img'),
        url = 'http://strip.biore.com';
        imageLink.css('cursor', 'pointer');
        imageLink.click(function () {
            window.location = url;
        });
    }*/
}


