﻿<%@ Page Title="Impressum" MasterPageFile="../Site.Master"
    Language="C#" AutoEventWireup="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Impressum " name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
    <style type="text/css">
        #content p
        {
            text-align: left;
            margin: 0 auto 40px 0;
        }

        .about #mainContent {
            background: #FFF none !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="about">
        <div id="main">
            <div id="mainContent">
                <div class="centeringDiv">
                    <div id="content">
                        <div class="titleh1">
                            <h1>
                                Impressum</h1>
                            <div id="responseRule">
                            </div>
                        </div>
                        
                        <h2>
                            KAO FRANCE SARL</h2><br />
                        <h2>
                            ADRESSE :</h2>
                        <p>11 rue Louis Philippe<br />
                            92522 Neuilly-sur-Seine Cedex</p>
                        <p>RCS Nanterre 418 324 539 au capital social de 4 800 000€</p>
                        <h2>
                            TÉLÉPHONE :</h2>
                        <p>08 11 03 23 33</p>
                        <h2>
                            E-MAIL :</h2>
                        <p>
                            <a href="mailto:infoconseils@biore.fr" target="_blank">infoconseils@biore.fr</a></p>
                        <h2>
                            DIRECTEUR GÉNÉRAL :</h2>
                        <p>
                            Eric Brockhus</p>
                        <h2>
                            Numéro SIRET</h2>
                        <p>
                            418 324 539 00059</p>
                        <h2>
                            NUMÉRO DE TVA INTRACOM</h2>
                        <p>
                            FR 45 418 324 539</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
