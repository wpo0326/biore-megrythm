﻿<%@ Page Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs"
    Inherits="Biore2012.FormConfig.ContactUs"  Title ="Nous contacter | Bioré® Skincare" %>
<%@ Register TagPrefix="skm" Namespace="skmValidators" Assembly="skmValidators" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%#SquishIt.Framework.Bundle .Css()
        .Add("~/css/formConfig.css")
        .Render("~/css/combinedform_#.css")
    %>
       <style type="text/css">
        h3 {
            font-weight:bold;
            color: #000;
        }
        .NameWrapper h3, .QuestionWrapper h3 {
            margin-top: 1.5em;
        }
        .NameWrapper h3 > p {
            font-size: .8em; margin-top:0; padding-top: 0;
        }

        #ctl00_ContentPlaceHolder1_bioreGDPR {
            width: 10px;
            float: left;
            margin: 2px 1em 0 0;
        }
        .optincontainer label {
            float: left;
            width: 80% !important;
        }

           .optincontainer .ErrorContainer {
               margin-left: 0 !important;
           }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow">
            </div>
            <div class="centeringDiv">
                <div class="responsive contact">
                    <div id="contactFormWrap">
                        <div id="BrandImageContainer" class="png-fix">
                            <img class="png-fix" src="/images/forms/contactUsPhoto.jpg" style="border-width: 0px;" />
                        </div>
                        <div id="ContactFormContainer">
                            <asp:Panel ID="ContactDetails" runat="server">
                                <!-- Header -->
                                <div id="formHeader">
                                    <h1 id="ctl00_ContentPlaceHolder1_ucForm_PageHeader">
                                        Nous contacter
                                    </h1>
									<div id="responseRule" style="width: 400px;margin: 5px 5px 45px;"></div>
                                </div>
                                <!-- Description -->
                                <div class="DescriptionContainer png-fix">
                                    <!--<h2>
                                        By Mail</h2>
                                    <p>
                                        Kao (UK) Limited<br />
                                        130 Shaftesbury Avenue<br />
                                        London W1D 5EU<br /></p>-->
                                   <h2>Par téléphone :</h2>
                                    <p>
                                        <span>0811 03 23 33</span>
                                    <h2>Via notre site Internet :</h2>
                                    <p>
                                        Remplissez le formulaire ci-dessous</p>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="ContactForm" runat="server">
                                <!-- From Fields -->
                                <div class="FormContainer">
                                    <p class="req png-fix">
                                        <em>Champs obligatoires*</em></p>
                                    <div class="MemberInfoContainer png-fix">
                                         <div class="EmailWrapper">
                                             <div><h3>Veuillez renseigner votre adresse e-mail :</h3>
                                            </div>
                                            <div class="EmailContainer Question">
                                                <asp:Label ID="EmailLbl" runat="server" Text="Email*" AssociatedControlID="Email"></asp:Label>
                                                <asp:TextBox ID="Email" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="EmailValidator1" runat="server" ErrorMessage="Veuillez entrer votre Courriel."
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="EmailValidator2" runat="server" ErrorMessage="Please enter a valid Email Address."
                                                        ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="ConfirmEmailContainer Question">
                                                <asp:Label ID="RetypeEmailLbl" runat="server" Text="Confirmez votre email*" AssociatedControlID="RetypeEmail"></asp:Label>
                                                <asp:TextBox ID="RetypeEmail" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="RetypeValidator1" runat="server" ErrorMessage="Veuillez entrer votre Courriel.."
                                                        ControlToValidate="RetypeEmail" EnableClientScript="true" SetFocusOnError="true"
                                                        Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="RetypeValidator2" Text="Make sure you retyped the Email correctly."
                                                        ControlToValidate="RetypeEmail" ControlToCompare="Email" EnableClientScript="true"
                                                        SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        <div class="NameWrapper">
                                            <div><h3>Quelles données souhaitez-vous partager avec nous pour nous aider à étudier votre demande ? </h3>
                                                <p>Par exemple : Nom, Prénom, numéro de téléphone  </p>
                                            <div class="FirstNameContainer Question">
                                                <asp:Label ID="FNameLbl" runat="server" Text="Prénom" AssociatedControlID="FName"></asp:Label>
                                                <asp:TextBox ID="FName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    
                                                    <asp:RegularExpressionValidator ID="FNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your First Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="FName" EnableClientScript="true"
                                                        SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="LastNameContainer Question">
                                                <asp:Label ID="LNameLbl" runat="server" Text="Nom de famille" AssociatedControlID="LName"></asp:Label>
                                                <asp:TextBox ID="LName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    
                                                    <asp:RegularExpressionValidator ID="LNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Last Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="LName" SetFocusOnError="true"
                                                        Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                       
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelAddress" class="AddressContainer">
                                            <div class="AddressWrapper">
                                                <div class="Address1Container Question">
                                                    <asp:Label ID="Address1Lbl" runat="server" Text="Adresse" AssociatedControlID="Address1"></asp:Label>
                                                    <asp:TextBox ID="Address1" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        
                                                        <asp:RegularExpressionValidator ID="Address1Validator2" runat="server" ErrorMessage="Please do not use special characters in the Address 1 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address1" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="Address2Container Question">
                                                    <asp:Label ID="Address2Lbl" runat="server" Text="Complément d’adresse" AssociatedControlID="Address2"></asp:Label>
                                                    <asp:TextBox ID="Address2" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="Address2Validator1" runat="server" ErrorMessage="Please do not use special characters in the Address 2 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address2" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>

                                                <div class="CityContainer Question">
                                                    <asp:Label ID="CityLbl" runat="server" Text="Ville " AssociatedControlID="City"></asp:Label>
                                                    <asp:TextBox ID="City" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                       
                                                        <asp:RegularExpressionValidator ID="CityValidator2" runat="server" ErrorMessage="Please do not use special characters in the City field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="City" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="PostalCodeContainer Question">
                                            <asp:Label ID="PostalCodeLbl" runat="server" Text="Code postal" AssociatedControlID="PostalCode"></asp:Label>
                                            <asp:TextBox ID="PostalCode" runat="server" MaxLength="20" CssClass="inputTextBox"></asp:TextBox>
                                            <div class="ErrorContainer">
                                                
                                            </div>
                                        </div>
                                        <div class="CountryContainer Question">
                                            <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_CountryLbl">Pays
                                            </span><span class="fake_input">France</span>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelGender" class="GenderContainer Question">
                                            <asp:Label ID="GenderLbl" runat="server" Text="Sexe" AssociatedControlID="Gender"></asp:Label>
                                            <asp:DropDownList ID="Gender" runat="server">
                                                <asp:ListItem Value="">Choisir</asp:ListItem>
                                                <asp:ListItem Value="F">F&#233;minin</asp:ListItem>
                                                <asp:ListItem Value="M">Masculin</asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                               
                                            </div>
                                        </div>
                                        <div class="BirthdayContainer Question">
                                            <asp:Label ID="DOBLbl" runat="server" Text="Date de naissance" AssociatedControlID="yyyy"></asp:Label>
                                            <asp:DropDownList ID="yyyy" runat="server" CssClass="inputSelectBDay year">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="mm" runat="server" CssClass="inputSelectBDay month">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="dd" runat="server" CssClass="inputSelectBDay day">
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:CustomValidator ID="DOBValidator" runat="server" ErrorMessage="Please enter a valid date."
                                                    OnServerValidate="DOBValidate" EnableClientScript="false" Display="Dynamic" CssClass="errormsg validdate"></asp:CustomValidator>
                                                
                                            </div>
                                        </div>
                                        <div class="PhoneContainer Question">
                                            <%--<label for="Phone">Phone</label>--%>
                                            <asp:Label ID="PhoneLbl" runat="server" Text="Téléphone" AssociatedControlID="Phone"></asp:Label>
                                            <asp:TextBox ID="Phone" MaxLength="50" runat="server" />
                                            <div class="ErrorContainer">
                                                <asp:RegularExpressionValidator ID="PhoneValidator1" runat="server" ErrorMessage="Your Phone is not required, but please do not use special characters in the Phone field."
                                                    ValidationExpression="^[^<>]+$" ControlToValidate="Phone" EnableClientScript="true"
                                                    SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                <%-- The following RegEx Validators is for US type numbers.  IMPORTANT: 
								 Please test against your user base before using as you may need to modify it for other formats!  --%>
                                                <%--<asp:RegularExpressionValidator ID="PhoneValidator2" runat="server"
								ErrorMessage="<br/>Your Home Phone is not required, but please enter only valid characters."
								ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"
								ControlToValidate="Phone" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg phone"></asp:RegularExpressionValidator>--%>
                                            </div>
                                        </div>
                                    
                                        <div class="QuestionWrapper">
                                            <div><h3>Comment pouvons-nous vous aider ?</h3></div>
                                            <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelComment" class="CommentContainer Question">
                                                <asp:Label ID="QuestionCommentLbl" runat="server" Text="Question / Commentaire*" AssociatedControlID="QuestionComment"></asp:Label>
                                                <asp:TextBox ID="QuestionComment" runat="server" TextMode="MultiLine" />
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="QuestionCommentValidator" runat="server" ErrorMessage="Veuillez entrer votre  Veuillez entrer votre Commentaire."
                                                        ControlToValidate="QuestionComment" EnableClientScript="true" SetFocusOnError="true"
                                                        Display="Dynamic" CssClass="errormsg questioncomment" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                    </div>
                                    <div id="ctl00_ContentPlaceHolder1_ucForm_QuestionPanel" class="QuestionContainer">
                                    </div>
                                    <div class="optincontainer">                                    
                                        <ul>
		                                        <li>
		                                            <asp:CheckBox ID="bioreGDPR" runat="server" />
			                                        <asp:Label ID="bioreoptinLabel" runat="server" Text="* Oui, j’accepte d’être contacté(e) par Bioré au sujet de ma demande formulée ci-dessus. Je consens à ce que mes données soient partagées dans le cadre de la <a href='/privacy/' target='_blank' style='text-decoration:underline'>politique de confidentialité</a> en vigueur." AssociatedControlID="bioreGDPR" CssClass="siteOptinChkbox" />

			                                        <div class="ErrorContainer">
                                    			        <skm:CheckBoxValidator ID="cbv_bioreGDPR" runat="server" ControlToValidate="bioreGDPR" ErrorMessage="*obligatoires" CssClass="errormsg" Display="Dynamic"></skm:CheckBoxValidator>
				                                    </div>

		                                        </li>
                                        </ul>
                                    </div>
                                    <div id="submit-container" class="SubmitContainer png-fix">
                                        <asp:Button runat="server" CssClass="submit buttonLink png-fix" Text="Envoyer ›" />
                                    </div>
                                    <asp:HiddenField ID="hfHookID" runat="server" />
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="ContactFormSuccess" runat="server">
                                <div id="contactSuccess">
                                
                                <div id="formHeader">
                                    <h1 id="ctl00_ContentPlaceHolder1_ucForm_PageHeader">
                                        Nous contacter
                                    </h1>
									<div id="responseRule" style="width: 400px;margin: 5px 5px 45px;"></div>
                                </div>
                                    <h2>
                                        Merci!</h2>
                                    <p>
                                        
Merci pour votre message. Un membre de notre Service Consommateurs prendra contact avec vous au plus vite. 

                            </asp:Panel>
                            <asp:Panel ID="ContactFormFailure" runat="server">
                                <div id="contactFailure">
                                  <div id="formHeader">
                                    <h1 id="ctl00_ContentPlaceHolder1_ucForm_PageHeader">
                                        Nous contacter
                                    </h1>
									<div id="responseRule" style="width: 400px;margin: 5px 5px 45px;"></div>
                                </div>
                                    <h2>
                                        We're Sorry...</h2>
                                    <p>
                                        There has been a problem with your form submission. Please <a href="javascript: back();">
                                            try again</a>.</p>
                                    <asp:Literal ID="litError" runat="server"></asp:Literal>
                                </div>
                            </asp:Panel>
                            <div class="FormBottom png-fix">
                            </div>
                            <!-- Disclaimer -->
                            <div id="DisclaimerContainer" class="png-fix">
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">

                    </script>

                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script type="text/javascript">
        $(function() {
            if ($(".signUp").length) {
                toggle("noneToggle1");
                toggle("noneToggle2");
                toggle("noneToggle3");
                toggle("noneToggle4");
            }
        });
        function toggle(className) {
            var checkToggle = $("." + className + " input:last");
            checkToggle.click(function() {
                if ($("#" + checkToggle.attr('id') + ":checked").val()) {
                    $("." + className + " input").each(function() {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).attr("checked", false);
                            $(this).attr('disabled', 'disabled');
                        }
                    });
                } else {
                    $("." + className + "  input").each(function() {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).removeAttr('disabled');
                        }
                    });
                }
            });
        }
    </script>

</asp:Content>
