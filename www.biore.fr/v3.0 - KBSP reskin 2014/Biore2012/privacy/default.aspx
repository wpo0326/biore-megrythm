﻿<%@ Page Title="Politique de Confidentialité " MasterPageFile="../Site.Master"
    Language="C#" AutoEventWireup="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Politique de Confidentialité " name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
    <style type="text/css">
        #content p {
            text-align: left;
        }
        #content ul {
            margin: auto auto auto 8em;
        }
        #content ul li {
            list-style: square;
        }

            #content ul li p {
                margin-left: 0;
            }

        .about #mainContent {
            background: #FFF none !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="about">
        <div id="main">
            <div id="mainContent">
                <div class="centeringDiv">
                    <div id="content">
                        <div class="titleh1">
                            <h1>
                                POLITIQUE DE CONFIDENTIALITÉ</h1>
                            <div id="responseRule">
                            </div>
                        </div>
                        
                        <p><br /><br /><b>POLITIQUE DE CONFIDENTIALITE SITE WEB Biore fr<br />
                        Dernière révision le 25 mai 2018</b></p>

                      <p> Chez Kao, nous respectons votre vie privée et nous prenons des mesures raisonnables pour la protéger. La présente politique s’applique uniquement au site Web sur lequel elle apparaît et ne s’applique pas aux autres sites Web ou services de Kao. En accédant au présent site Web ou en l’utilisant, vous acceptez et consentez à ce que nous utilisions vos informations conformément à la présente politique.</p>

<p><b>DATE D’ENTRÉE EN VIGUEUR</b></p>
<p>La présente politique de confidentialité est en vigueur et a été mise à jour le 25 mai 2018.</p>

                        <p>Kao France SARL RCS Nanterre 418 324 539 (« <b>Société Kao</b> » ou « <b>nous</b> », « <b>notre</b> », « <b>nos</b> »), ainsi que chacune de ses filiales et sociétés affiliées dans la région EMEA, collectivement le « <b>Groupe Kao</b> »), prend au sérieux la question de la confidentialité des données. La présente Politique de Confidentialité informe les utilisateurs de www.biore.fr et de tous les autres sites Web ou applications mobiles appartenant à la Société Kao sur lesquels est publiée cette Politique de Confidentialité (« <b>Site Web</b> »), de la manière dont, en tant que responsable de traitement au sens du Règlement Général sur la Protection des Données (« <b>RGPD</b> ») nous collectons et traitons les données personnelles et autres informations de ces utilisateurs dans le cadre de leur utilisation de notre Site Web.</p>

<p>Sachez que les autres sites Web ou applications mobiles du Groupe Kao peuvent être régis par d'autres politiques de confidentialité.</p>

<p><b>1.	Catégories de Données Personnelles et Finalités de Traitement - Quelles sont les données personnelles que nous traitons vous concernant et pourquoi ?</b></p>

<p><b>1.1	Métadonnées</b><br />
Vous pouvez utiliser le Site Web sans communiquer aucune des données personnelles vous concernant. Dans ce cas, nous collectons uniquement les métadonnées suivantes, qui résultent de votre utilisation du Site Web: type et version de navigateur, système d'exploitation et interface, site Web en provenance duquel vous consultez le nôtre (URL de référence), page(s) Web que vous visitez sur notre Site Web, date et heure d'accès à notre Site Web et adresse de protocole Internet (IP).</p>

<p>Votre adresse IP sera utilisée pour permettre votre accès à notre Site Web. Les métadonnées, y compris l'adresse IP raccourcie, seront utilisées pour améliorer la qualité et les fonctionnalités de notre Site Web et de nos services, en analysant le comportement d'utilisation de nos utilisateurs.</p>

<p><b>1.2 Formulaire « contactez-nous »</b><br />
Sur notre Site Web, nous vous donnons la possibilité de nous contacter via  le formulaire « contactez-nous ». Pour cela, il vous sera demandé de nous communiquer les données personnelles suivantes vous concernant : nom, pays, adresse électronique. Nous traitons ces données personnelles pour les finalités suivantes : répondre à vos questions ou demandes et le suivi technique qui en découle. Les données renseignées seront traitées par notre Service Consommateur externalisé.</p>

<p><b>1.3	Jeux-concours</b><br />
Lorsque vous participez à un jeu concours, nous collectons et traitons les données personnelles suivantes vous concernant : nom, sexe (titre de civilité), adresse postale, adresse électronique, numéro de téléphone, date de la participation au jeu, désignation en tant que vainqueur, prix, réponse au quiz. Nous traitons ces données personnelles pour les finalités suivantes : organiser le jeu-concours, informer le vainqueur, offrir le prix au vainqueur, organiser l'événement, vous fournir des contenus marketing dans la mesure permise par la loi applicable et analyser vos centres d'intérêt à des fins marketing.</p>

<p><b>Fondements et Conséquences du traitement - Quel est le fondement du traitement de vos données personnelles et que se passe-t-il si vous choisissez de ne pas les communiquer ?</b></p>

<p>Nous nous appuyons sur les fondements suivants pour la collecte, le traitement et l'utilisation de vos données personnelles :</p>

    <ul>
<li><p>votre consentement au traitement de vos données pour une ou plusieurs finalités spécifiques ;</p></li> 
        </ul>

<p>La communication de vos données personnelles n'est pas requise en vertu d'une obligation légale ou contractuelle. La communication de vos données personnelles n'est pas nécessaire à la conclusion d'un contrat avec nous, ou pour recevoir nos services/produits suite à une demande de votre part. La communication de vos données personnelles est laissée à votre discrétion. </p>
       
<p>Le fait de ne pas communiquer vos données personnelles peut s'avérer désavantageux pour vous -- vous pourriez pas exemple ne pas être en mesure de recevoir certains produits et services. Néanmoins, sauf indication contraire, le fait de ne pas communiquer vos données personnelles n'engendrera aucune conséquence juridique à votre encontre.
</p>

<p><b>3. Catégories de Destinataires et Transferts Internationaux - A qui transférons-nous vos données personnelles et où ces entités sont-elles situées ?</b></p>

<p>Nous pouvons transférer vos données personnelles à des tiers, pour les finalités de traitement décrites ci-dessus, de la manière suivante :</p>

                        <ul>
<li><p>Au sein de la Société Kao : Notre entité mère basée au Japon, Kao Corporation et chacune de ses filiales et sociétés affiliées (« Société Kao » désignant chaque filiale, chaque société affiliée et y compris nous-mêmes, collectivement le « Groupe Kao ») au sein du Groupe Kao mondial http://www.kao.com/global/en/worldwide.html  peuvent recevoir vos données personnelles dans la mesure nécessaire aux finalités de traitement décrites ci-dessus. En fonction des catégories de données à caractère personnel et des finalités pour lesquelles les données personnelles ont été collectées, plusieurs départements internes de la Société Kao sont susceptibles de recevoir vos données personnelles. Par exemple, [notre département informatique peut avoir accès à vos données de compte et nos départements de commerce électronique et de ventes peuvent accéder à vos données de compte ou à vos données relatives aux commandes de produits]. En outre, d'autres départements de la Société Kao peuvent avoir accès à certaines données personnelles vous concernant, sur la base d'un besoin légitime, tel que le département juridique, le département financier, ou en cas d'audit interne.</p></li>
<li><p>Avec les sous-traitants : Certains tiers, affiliés ou non affiliés, peuvent recevoir vos données personnelles pour les traiter selon des instructions appropriées (les « Sous-traitants ») dans la mesure nécessaire aux finalités de traitement décrites ci-dessus, parmi lesquels les prestataires de services du site Web, les prestataires de traitement de commandes, les fournisseurs de services clients, les prestataires de services de marketing, les prestataires de services de support informatique, ainsi que d'autres prestataires de services qui nous aident à entretenir notre relation commerciale avec vous. Les Sous-traitants seront contractuellement tenus de mettre en place de mesures de sécurité techniques et organisationnelles appropriées, pour protéger les données personnelles et de traiter les données personnelles uniquement selon les instructions fournies. </p></li>
<li><p>Autres destinataires : Conformément à la loi applicable en matière de protection des données, nous sommes susceptibles de transférer des données personnelles aux forces de l'ordre, autorités gouvernementales, autorités judiciaires, conseillers juridiques, consultants externes, ou partenaires commerciaux. En cas de fusion ou acquisition d'entreprise, des données personnelles peuvent être transférées à des tiers impliqués dans la fusion ou l'acquisition. Nous ne divulguerons pas vos données personnelles à des tiers à des fins publicitaires ou marketing, ni à aucune autre fin sans votre autorisation.</p></li>
                            </ul>

<p>Tout accès à vos données personnelles est limité aux personnes physiques qui ont besoin d’y accéder dans l'exercice de leurs missions professionnelles.</p>

<p><b>Transferts internationaux.</b> Les données personnelles que nous collectons ou recevons vous concernant peuvent être transférées et traitées par des destinataires qui sont basés au sein et en dehors de l'Espace Economique Européen (« <b>EEE</b> »). S'agissant des destinataires situés en dehors de l'EEE, certains sont certifiés en vertu du Bouclier de Protection des Données UE-Etats-Unis et d'autres sont situés dans des pays offrant des décisions d'adéquation (notamment, l'Andorre, l'Argentine, le Canada (s'agissant des organisations non publiques soumises à la Loi Canadienne sur la Protection des Renseignements Personnels et les Documents Electroniques), la Suisse, les Îles Féroé, Guernesey, Israël, l'Île de Man, Jersey et la Nouvelle-Zélande) et, dans chaque cas, le transfert est par conséquent considéré comme fournissant un niveau adéquat de protection des données du point de vue de la loi européenne sur la protection des données. D'autres destinataires peuvent être situés dans des pays qui n'assurent pas un niveau de protection adéquat du point de vue de la loi européenne sur la protection des données. Nous prendrons toutes les mesures nécessaires pour veiller à ce que les transferts en dehors de l'EEE soient protégés de manière adéquate, conformément à la loi applicable sur la protection des données. En ce qui concerne les transferts vers des pays qui n'assurent pas un niveau adéquat de protection des données, nous baserons le transfert sur des garanties appropriées, telles que les clauses types de protection des données adoptées par la Commission Européenne ou par une autorité de contrôle, les codes de conduite approuvés en parallèle d'engagements contraignants et dotés de force obligatoire du destinataire, ou des mécanismes de certification approuvés en parallèle d'engagements contraignants et dotés de force obligatoire du destinataire. Vous pouvez demander une copie de ces garanties appropriées en nous contactant comme indiqué à la Section 7 ci-dessous.</p>

<p><b>4.Durée de Conservation - Combien de temps conservons-nous vos données personnelles ?</b></p>
<p>Vos données personnelles seront conservées aussi longtemps que nécessaire pour vous fournir les services demandés. Dès lors que vous aurez résilié la relation contractuelle qui nous lie / que le jeu-concours aura été effectué ou que vous aurez mis fin à votre relation avec nous, nous supprimerons vos données personnelles de nos systèmes et de nos fichiers et/ou prendrons des mesures pour les anonymiser correctement, de sorte que vous ne puissiez plus être identifié à partir de ces données (à moins que nous soyons tenus de conserver vos informations pour respecter les obligations légales ou réglementaires auxquelles la Société Kao est soumise – ex. : à des fins fiscales).</p>
<p>Nous pouvons conserver vos coordonnées et les données sur l'intérêt que vous portez à nos produits ou services pour une durée supérieure, si la Société Kao est autorisée à vous envoyer des contenus marketing. En outre, nous pouvons être dans l'obligation, conformément à la loi applicable, de conserver certaines de vos données personnelles pour une période de 10 ans suivant l'exercice fiscal concerné. Nous pouvons également conserver vos données personnelles après la résiliation de notre relation contractuelle, si vos données personnelles sont nécessaires pour nous conformer à d'autres lois applicables, ou si nous avons besoin de vos données personnelles pour constater, exercer ou défendre un  droit en justice, uniquement sur la base d'un besoin légitime. Dans la mesure du possible, nous limiterons le traitement de vos données personnelles à ces finalités après la résiliation de notre relation contractuelle.
</p>


 <p><b>5. Vos Droits - De quels droits bénéficiez-vous et comment pouvez-vous les faire valoir ?</b></p>

<p><b>Droit de retirer votre consentement</b> : Si vous avez donné votre consentement concernant certaines activités de collecte, de traitement ou d'utilisation de vos données personnelles (notamment s'agissant de la réception de communications marketing direct via courrier électronique, SMS/MMS, fax et téléphone), vous pouvez retirer ce consentement à tout moment avec effet pour l'avenir. Un tel retrait n'affectera pas la légalité du traitement avant le retrait du consentement. Veuillez nous contacter comme indiqué à la Section 7 ci-dessous pour retirer votre consentement. [En outre, vous pouvez vous opposer à l'utilisation de vos données personnelles à des fins marketing sans encourir de frais autres que les coûts de transmission fixés selon les tarifs de base.]</p>

<p><b>Autres droits en matière de protection des données et de la vie privée</b> : Conformément à la loi applicable sur la protection des données, vous êtes en droit de : (i) demander l'accès à vos données personnelles ; (ii) demander la rectification de vos données personnelles ; (iii) demander l'effacement de vos données personnelles ; (iv) demander la limitation de traitement de vos données personnelles ; (v) demander la portabilité des données ; et/ou (vi) vous opposer au traitement de vos données personnelles (y compris vous opposer au profilage).</p>

<p>Sachez que les droits mentionnés ci-dessus peuvent être limités par la loi nationale applicable sur la protection des données. Vous trouverez ci-dessous davantage d'informations sur vos droits lorsque le RGPD s'applique :</p>

                        <ul>
<li><p>Droit de demander l'accès à vos données personnelles : Vous pouvez obtenir auprès de nous la confirmation que les données personnelles vous concernant sont ou ne sont pas traitées et, lorsque c’est le cas, de demander l'accès aux données personnelles. Ces informations auxquelles vous pouvez accéder comprennent, entre autres, les finalités de traitement, les catégories de données personnelles concernées et les destinataires ou catégories de destinataires à qui ont été ou seront divulguées les données personnelles. Cependant, il ne s’agit pas d’un droit absolu et les intérêts d'autres personnes physiques peuvent restreindre votre droit d'accès.<br />
Vous pouvez avoir le droit d'obtenir gratuitement une copie des données personnelles en cours de traitement. Pour obtenir des exemplaires supplémentaires, nous pouvons vous facturer des frais raisonnables qui tiennent compte des coûts administratifs.</p></li>
<li><p>Droit de demander la rectification : Vous pouvez obtenir la rectification des données personnelles inexactes vous concernant. Selon les finalités de traitement, vous êtes en droit de demander à ce que les données à caractère personnel incomplètes soient complétées, notamment en fournissant une déclaration complémentaire.</p></li>
<li><p>Droit de demander l'effacement (droit à l’oubli) : Dans certaines circonstances, vous pouvez obtenir de notre part l’effacement des données personnelles vous concernant et i nous pouvons avoir l'obligation d'effacer ces données personnelles.</p></li>
<li><p>Droit de demander la limitation du traitement : Dans certaines circonstances, vous pouvez obtenir de notre part la limitation du traitement de vos données personnelles. Dans ce cas, les données respectives seront marquées et nous ne pourrons les traiter que pour certaines finalités.</p></li>
<li><p>Droit de demander la portabilité des données : Dans certaines circonstances, vous pouvez avoir droit de recevoir les données personnelles vous concernant, que vous nous avez fournies, dans un format structuré, couramment utilisé et lisible par machine et vous pouvez avoir le droit de transmettre ces données à une autre entité sans que nous nous y opposions.</p></li>
</ul>
                  
                <div style="max-width: 800px; background-color: #CCC; margin: auto">
                    <p><b>Droit d'opposition :</b></p>
<p><b>Dans certaines circonstances, vous pouvez avoir le droit de vous opposer à tout moment, pour des motifs liés à votre situation particulière, au traitement de vos données personnelles et nous pouvons être tenus de ne plus traiter vos données personnelles. Il est possible qu'un tel droit d'opposition s'applique plus particulièrement si nous collectons et traitons vos données à caractère personnel à des fins de profilage, afin de mieux comprendre l'intérêt que vous portez à nos produits et services, ou à des fins de marketing direct.</b></p>
<p><b>Si vous disposez d'un droit d'opposition et que vous exercez ce droit, nous ne procèderons plus au traitement de vos données personnelles pour de telles finalités. Vous pouvez exercer ce droit en nous contactant comme indiqué à la Section 7 ci-dessous.</b></p>
<p><b>Il est possible qu'un tel droit d'opposition n'existe pas, notamment si le traitement de vos données personnelles est nécessaire pour prendre certaines mesures préalables à la conclusion d'un contrat, ou pour exécuter un contrat d'ores et déjà conclu.</b></p>
                    </div>

<p>Pour exercer vos droits, veuillez nous contacter comme indiqué à la Section 7 ci-dessous. Vous êtes également en droit de déposer une réclamation auprès de l’autorité compétente de contrôle de la protection des données.</p>

<p><b>6. Cookies et autres technologies de suivi</b></p>

<p>Ce Site Web utilise des cookies et d'autres technologies de suivi. </p>

<p>Kao peut être amené à enregistrer vos interactions avec nos publicités, nos sites web, emails ou autres applications que nous fournissons avec Clickstream Data and Cookies. « Clickstream Data » est un enregistrement de ce sur quoi vous cliquez pendant que vous surfez sur ce site web. Ces données nous informent sur le type d’ordinateur et le logiciel de navigation que vous utilisez, ainsi que l’adresse du site qui vous a mené à ce site web. </p>

<p>Les « Cookies » sont de petits fichiers textes placés sur votre ordinateur par un site web dans le but de faciliter et d’améliorer votre communication et vos interactions avec le site en question, en mémorisant vos préférences et en recueillant des informations globales (c.-à-d. qui ne vous identifient pas personnellement). De nombreux site web, dont le nôtre, ont recours aux cookies à ces fins.</p> 

<p>Le site web peut également inclure des cookies mis en place par des tiers, notamment Google, ce qui 1) nous aide à mesurer la performance du site web, 2) nous permet de partager avec vous des informations pertinentes et des publicités en fonction de vos visites précédentes ainsi que celles des autres utilisateurs sur ce site Internet quand vous êtes sur le web, et 3) nous aide à mesurer vos interactions avec les publicités Kao que vous voyez sur d’autres sites web. Enfin, Kao aura recours aux données Google des publicités basées sur les centres d’intérêt ou aux données d’audience de tiers, telles que l’âge, le sexe et les centres d’intérêt, combinées à nos propres données, afin de produire des publicités qui vous sont pertinentes en fonction de votre profil démographique et de vos intérêts.</p> 

<p>Spécifiquement, ce site web a mis en place les fonctionnalités de Google Analytics suivantes pour développer nos publicités à travers le web : le Remarketing (pour vous montrer des publicités sur des sites Internet), Google Display Network Impression Reporting, le DoubleClick Campaign Manager Integration et le Google Analytics Demographics and Interest Reporting.  </p>

<p>Vous pouvez désactiver l’affichage de publicités de Google Analytics et personnaliser les publicités du Réseau Display de Google en règlant les paramètres publicitaires ou en utilisant le module de désactivation de Google Analytics. Vous trouverez plus d’information sur Google Analytics par ici. </p>

<p>Vous pouvez arrêter ou restreindre l’installation de cookies sur votre ordinateur ou les éliminer de votre navigateur en ajustant les préférences de votre navigateur et les paramètres des modules de votre navigateur, auquel cas vous pouvez continuer à utiliser notre site web mais cela peut interférer avec certaines de ses fonctionnalités.</p>

<p><b>Google Analytics</b></p>

<p>Ce site Internet a recours à Google Analytics, un service d’analyse internet fourni par Google, Inc. (« Google). Google Analytics utilise des « cookies », des fichiers textes placés sur votre ordinateur, pour aider le site web à analyser la manière dont les visiteurs utilisent le site. L’information générée par le cookie sur votre utilisation du site web sera transmise à et stockée par Google sur des serveurs aux Etats-Unis. </p>
<p>Si l’anonymisation de l’IP est active sur ce site web, votre adresse IP sera tronquée au sein des Etats Membre de l’Union Européenne ou tout autre participant à l’accord sur l’Espace Economique Européen. Uniquement dans des cas exceptionnels, la totalité de l’adresse IP sera d’abord transférée à un serveur Google aux Etats-Unis et tronqués là-bas. L’anonymisation de l’IP est active sur ce site web. </p>
<p>Google utilisera ces informations pour le compte de l’opérateur de ce site web dans le but d’évaluer votre utilisation du site web, de compiler des rapports sur l’activité du site pour les exploitants du site web et pour leur fournir d’autres services relatifs à l’activité du site web et l’utilisation d’Internet. </p>
<p>L’adresse IP, que votre navigateur transmet dans le cadre de Google Analytics, ne sera associée à aucune autre donnée détenue par Google. Vous pouvez refuser l’utilisation des cookies en choisissant les paramètres adaptés dans votre navigateur, notez cependant que si vous faites cela, vous pourriez ne pas avoir accès à toutes les fonctionnalités de ce site web.</p>
<p>Vous pouvez également ne pas adhérer au suivi de Google Analytics pour le futur en téléchargeant et en installant le module Google Analytics Opt-Out Browser Addon pour votre navigateur actuel : 
tools.google.com/dlpage/gaoptout. </p>
                        <p>Comme alternative au module du navigateur et en particulier pour les navigateurs mobiles, veuillez cliquer sur le lien suivant pour définir un cookie de désactivation. Ce cookie de désactivation empêche la détection par Google Analytics sur ce site Web. <a href="/privacy/?google-analytics-opt-out=true">http://www.biore.fr/privacy/?google-analytics-opt-out=true</a></p>

<p><b>Adobe Analytics</b></p>

<p>Ce site web a également recours à Adobe Analytics, un service d’analyse internet proposé par Adobe Systems Software Ireland Limited (“Adobe”). Adobe Analytics utilise des « cookies », des fichiers textes archivés dans votre ordinateur qui permettent d’analyser votre utilisation du site web. Si les informations générées par le cookie concernant l’utilisation du site web est transmis à un serveur Adobe, les paramètres garantissent l’anonymat de l’adresse IP avant la géolocalisation, et le remplacement par une adresse IP générique avant l’archivage. Pour le compte de l’exploitant de ce site web, Adobe va utiliser ces informations pour évaluer l’utilisation du site par les visiteurs, compiler des rapports sur les activités du site, et fournir à l’exploitant du site d’autres services relatifs à l’activité du site web et à l’utilisation d’Internet. L’adresse IP envoyée par votre navigateur dans le cadre de l’utilisation d’Adobe Analytics ne sera associée à aucune autre donnée d’Adobe. Vous pouvez empêcher l’archivage des cookies en ajustant votre logiciel de navigation en conséquence. Cependant, veuillez noter que si vous faites cela, vous pourriez ne pas avoir accès à toutes les fonctionnalités de ce site. De plus, vous pouvez empêcher l’enregistrement par Adobe des données générées par les cookies et relatives à votre utilisation du site (y compris votre adresse IP) ainsi que le traitement de ces données par Adobe en utilisant le plugin de navigateur disponible sur le lien suivant. A télécharger et installer : http://www.adobe.com/privacy/opt-out.html </p>

<p><b>7. Questions et Coordonnées</b></p>

<p>Pour toute question relative à cette Politique de Confidentialité, ou si vous souhaitez exercer vos droits comme indiqué ci-dessus à la Section 5, veuillez nous contacter aux coordonnées suivantes : Kao France Sarl, 11 rue Louis Philippe, 92522 Neuilly-sur-Seine Cedex.  Pour plus d’informations sur vos droits, veuillez visiter www.kao.com/global/en/EU-Data-Subject-Request/.</p>

<p><b>8. Modifications de la présente Politique de Confidentialité </b></p>
<p>Nous pouvons actualiser la présente Politique de Confidentialité de temps à autre en réponse aux évolutions des exigences juridiques, réglementaires ou opérationnelles. Nous vous notifierons de toute modification, y compris la date de son entrée en vigueur, en actualisant la date de « Dernière révision » susmentionnée, ou de toute autre manière requise en vertu de la loi applicable. Votre utilisation continue de notre Site Web après l'entrée en vigueur de telles mises à jour vaudra acceptation de ces modifications. Si vous n'acceptez pas les mises à jour apportées à la présente Politique de Confidentialité, vous êtes invité à cesser d'utiliser notre Site Web
</p>


                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
