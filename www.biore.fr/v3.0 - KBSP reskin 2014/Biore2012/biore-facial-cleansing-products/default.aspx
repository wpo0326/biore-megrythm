﻿<%@ Page Title="Libérez vos pores ! – Une peau nette en pleine santé – Afficher tous les produits |  Bior&eacute;&reg;" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.our_products.Default" %>
<%@ Register Src="~/BazaarVoice/Controls/BVScripts.ascx" TagPrefix="uc1" TagName="BVScripts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Les produits Bioré® nettoient et exfolient en douceur pour une peau nette en pleine santé ! Découvrez la gamme de produits Bioré®" name="description" />
    <meta content="peau nette, Bioré®, libérez vos pores, charbon, bicarbonate de sodium, bicarbonate de soude, patchs purifiants, nettoyage de peau, peau mixte, peau grasse, imperfections, points noirs" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>
    <meta name="viewport" content="width=device-width">
    <uc1:BVScripts runat="server" ID="BVScripts" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="titleh1">
                <h1>Nos produits</h1>
                <div id="responseRule"></div>
                <p>Essentiels pour notre peau, les pores peuvent pourtant s’obstruer et devenir très (et souvent trop !) visibles. Pire encore, les pores dilatés entraînent l’apparition de vilains points noirs et d’imperfections cutanées…</p></br>
                <p>Prenez le problème à la racine en nettoyant votre peau en profondeur !</p></br>
                <p>Super efficaces pour nettoyer les pores, nos produits existent en formats gels, masques, exfoliants, eaux micellaires ou patchs pour vous permettre de retrouver une peau nette et saine.</p>
            </div>

            
             <div id="takeitalloffProds" class="prodList">
                <h2 class="pie roundedCorners takeitalloff">un geste et <span>c’est parti !</span>  
                    <span></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
 
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-cleansing-micellar-water.png" alt="" />
                        <asp:Literal ID="litBSMicellarWater" runat="server" />
                        <h3>Micellair Reinigingswater <br />met Baking Soda</h3>
                        <p>Verwijdert make-up, reinigt tot diep in de poriën en balanceert de huid, zonder uit te drogen.</p>
                        <a href="../take-it-all-off/baking-soda-cleansing-micellar-water" id="details-baking-soda-cleansing-micellar-water">details ></a>
                    </li>-->

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-cleansing-micellar-water.png" alt="" />
                        <asp:Literal ID="litCharcoalMicellarWater" runat="server" />
                        <h3>Eau Micellaire au Charbon</h3>
                        <p>Démaquille parfaitement, nettoie les pores en profondeur et absorbe l’excès de sébum, sans dessécher la peau</p>
                        <a href="../take-it-all-off/charcoal-cleansing-micellar-water" id="details-charcoal-cleansing-micellar-water">details ></a>
                    </li>
                </ul>
            </div>

            <div id="deepCleansingProds" class="prodList">
                <h2 class="pie roundedCorners deepCleansing"> bye bye <span>les impuretés !</span><br />
                    <span class="arrow"></span>
                </h2>
                <ul>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CHARCOAL_100X235.png" alt="" />
                        <h3>Gel Nettoyant en <br />Profondeur au Charbon</h3>
                        <p>Nettoie 2x mieux* en profondeur & purifie naturellement<br /><br />* qu’un nettoyant classique</p>
                        <div id="BVRRInlineRating-deep-pore-charcoal-cleanser" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/deep-pore-charcoal-cleanser" id="A1">details ></a>
                    </li>
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreMinimizer.png" alt="" />
                        <h3>Gel Exfoliant Réducteur <br />
                      de Pores au Charbon</h3>
                        <p>Exfolie & resserre visiblement les pores </p>
                        <div id="BVRRInlineRating-charcoal-pore-minimizer" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/charcoal-pore-minimizer" id="details-charcoal-pore-minimizer">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/SelfHeatingMask.png" alt="" style="margin:0 0 2px 44px;"/>
                        <h3>Masque Auto-Chauffant <br />1 Minute Chrono au Charbon</h3>
                        <p>Se réchauffe au contact de l’eau et purifie les pores en seulement 1 minute</p>
                        <div id="BVRRInlineRating-self-heating-one-minute-mask" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/self-heating-one-minute-mask" id="A2">details ></a>
                    </li>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/Baking_Soda_Cleanser_small.png" alt="" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:8pt;"></span> Gel Nettoyant Exfoliant Doux au Bicarbonate de Sodium</h3>
                        <p>Désobstrue les pores & exfolie tout en douceur</p>
                        <div id="BVRRInlineRating-baking-soda-pore-cleanser" class="ratingCategory"></div>
                        <a href="../baking-soda-for-combination-skin/baking-soda-pore-cleanser" id="A3">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-cleansing-scrub-small.png" alt="" />
                        <h3><span style="font-family: ProximaNova-Bold, Arial;color:yellow;font-size:8pt;"></span> Poudre Exfoliante en Profondeur au Bicarbonate de Sodium</h3>
                        <p>Gomme en douceur & nettoie intensément</p>
                        <div id="BVRRInlineRating-baking-soda-cleansing-scrub" class="ratingCategory"></div>
                        <a href="../baking-soda-for-combination-skin/baking-soda-cleansing-scrub" id="A4">details ></a>
                    </li>             
                    
                </ul>
            </div>

<div id="complexionClearingProds" class="prodList">
                <h2 class="pie roundedCorners bigPores">halte aux <span>spots !</span><br />
                    <span class="arrow"></span>
                </h2>
                <ul>
                   <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-anti-pimple_100X235.png" alt="" />
                        <h3>Gel Nettoyant <br />Anti-Imperfections au Charbon</h3>
                        <p>Absorbe l’excès de sébum et élimine les boutons</p>
                        <div id="Div1" class="ratingCategory"></div>
                        <a href="../halte-aux-spots/charcoal-anti-pimple-clearing-cleanser" id="A5">details ></a>
                    </li>   
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-anti-pimple_100X235.png" alt="" />
                        <h3>Exfoliant <br />Anti-Imperfections au Charbon</h3>
                        <p>Élimine les impuretés et l’excès de sébum qui provoquent les boutons</p>
                        <div id="Div2" class="ratingCategory"></div>
                        <a href="../halte-aux-spots/charcoal-anti-pimple-scrub" id="A6">details ></a>
                    </li> 

                
                </ul>
            </div>

            <div id="murtProds" class="prodList">
                <h2 class="pie roundedCorners murt">fini <span>les points noirs !</span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltra.png" alt="" />
                        <h3>Patchs Ultra Purifiants</h3>
                        <p>Élimine 2x plus d’impuretés incrustées</p>
                        <div id="Div3" class="ratingCategory"></div>
                        <a href="../pore-strips/deep-cleansing-pore-strips-ultra" id="A7">details ></a>
                    </li>
		   <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" />
                        <h3>Patchs Purifiants au Charbon</h3>
                        <p>Désobstrue instantanément les pores en éliminant impuretés, excès de sébum & points noirs</p><p>
3x moins de sébum
</p>
                        <div id="BVRRInlineRating-charcoal-pore-strips-regular" class="ratingCategory"></div>
                        <a href="../pore-strips/charcoal-pore-strips#regular" id="details-deep-cleansing-pore-strips-charcoal">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.png" alt="" />
                        <h3>Assortiment de 14 Patchs Purifiants</h3>
                        <p>Désobstrue instantanément les pores en éliminant impuretés, excès de sébum & points noirs</p>
                        <div id="BVRRInlineRating-pore-strips-combo" class="ratingCategory"></div>
                        <a href="../pore-strips/pore-strips-combo" id="details-deep-cleansing-pore-strips-combo">details ></a>
                    </li>

               
                    
                </ul>
            </div>

           

        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
