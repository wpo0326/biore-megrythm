﻿<%@ Page Title="Libérez vos pores ! – Une peau nette en pleine santé – Afficher tous les produits |  Bior&eacute;&reg;" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.our_products.Default" %>
<%@ Register Src="~/BazaarVoice/Controls/BVScripts.ascx" TagPrefix="uc1" TagName="BVScripts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Les produits Bioré® nettoient et exfolient en douceur pour une peau nette en pleine santé ! Découvrez la gamme de produits Bioré®" name="description" />
    <meta content="peau nette, Bioré®, libérez vos pores, charbon, bicarbonate de sodium, bicarbonate de soude, patchs purifiants, nettoyage de peau, peau mixte, peau grasse, imperfections, points noirs" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>
    <meta name="viewport" content="width=device-width">
    <uc1:BVScripts runat="server" ID="BVScripts" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="titleh1">
                <h1>Nos produits</h1>
                <div id="responseRule"></div>
                <p>Les pores sont à l’origine de toutes les imperfections de la peau – débarrassez-vous de vos problèmes de peau une fois pour toutes en la nettoyant tous les jours. Nos produits super efficaces pour nettoyer les pores existent en format gel, masque, exfoliant ou en patchs purifiants pour que votre peau reste nette et en plein santé.</p>
            </div>

            

            <div id="deepCleansingProds" class="prodList">
                <h2 class="pie roundedCorners deepCleansing"> <span>Au Charbon !</span><br />Pour peaux normales à grasses
                    <span class="arrow"></span>
                </h2>
                <ul>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CHARCOAL_100X235.png" alt="" />
                        <h3>Gel Nettoyant en <br />Profondeur </h3>
                        <p>Nettoie 2x mieux* en profondeur & purifie naturellement<br /><br />* qu’un nettoyant classique</p>
                        <div id="BVRRInlineRating-deep-pore-charcoal-cleanser" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/deep-pore-charcoal-cleanser" id="A1">details ></a>
                    </li>
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreMinimizer.png" alt="" />
                        <h3>Gel Exfoliant Réducteur <br />
                      de Pores</h3>
                        <p>Exfolie & resserre visiblement les pores</p>
                        <div id="BVRRInlineRating-charcoal-pore-minimizer" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/charcoal-pore-minimizer" id="details-charcoal-pore-minimizer">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/SelfHeatingMask.jpg" alt="" style="margin:0 0 2px 44px;"/>
                        <h3>Masque Auto-Chauffant <br />1 Minute Chrono</h3>
                        <p>Se réchauffe au contact de l’eau et purifie les pores en seulement 1 minute</p>
                        <div id="BVRRInlineRating-self-heating-one-minute-mask" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/self-heating-one-minute-mask" id="A2">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" class="cps-custom"/>
                        <h3>Patchs Purifiants</h3>
                        <p>Désobstrue instantanément les pores en éliminant impuretés, excès de sébum & points noirs</p><p>
3x moins de sébum
</p>
                        <div id="BVRRInlineRating-charcoal-pore-strips-regular" class="ratingCategory"></div>
                        <a href="../pore-strips/charcoal-pore-strips#regular" id="details-deep-cleansing-pore-strips-charcoal">details ></a>
                    </li>
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/makeupRemovingTowelettes.png" alt="" />
                        <h3>Daily Makeup Removing Towelettes</h3>
                        <p>Remove stubborn waterproof makeup and clean pores.</p>
                        <a href="../charcoal-for-oily-skin/make-up-removing-towelettes" id="details-make-up-removing-towelettes">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/4In1DetoxifyingCleanser.png" alt="" />
                        <h3>Pore Detoxifying Foam Cleanser</h3>
                        <p>Clean, tone, and stimulate with our self-foaming formula.</p>
                        <a href="../charcoal-for-oily-skin/pore-detoxifying-foam-cleanser" id="details-pore-detoxifying-foam-cleanser">details ></a>
                    </li>  -->  
                </ul>
            </div>

            <div id="murtProds" class="prodList">
                <h2 class="pie roundedCorners murt"><span>Patchs purifants</span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.png" alt="" />
                        <h3>Assortiment de 14 Patchs Purifiants</h3>
                        <p>Désobstrue instantanément les pores en éliminant impuretés, excès de sébum & points noirs</p>
                        <div id="BVRRInlineRating-pore-strips-combo" class="ratingCategory"></div>
                        <a href="../pore-strips/pore-strips-combo" id="details-deep-cleansing-pore-strips-combo">details ></a>
                    </li>
		   <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" />
                        <h3>Patchs Purifiants</h3>
                        <p>Désobstrue instantanément les pores en éliminant impuretés, excès de sébum & points noirs</p><p>
3x moins de sébum
</p>
                        <div id="BVRRInlineRating-charcoal-pore-strips-regular" class="ratingCategory"></div>
                        <a href="../pore-strips/charcoal-pore-strips#regular" id="details-deep-cleansing-pore-strips-charcoal">details ></a>
                    </li>
                    
                    <!-- <li>
                        <img src="../images/ourProducts/products/small/combinationSkinBalancingCleanser.png" alt="" />
                        <h3>COMBINATION SKIN BALANCING CLEANSER</h3>
                        <p>Get an even, all-over clean for the complex needs of combination skin.</p>
                        <a href="../charcoal-for-oily-skin/combination-skin-balancing-cleanser" id="details-combination-skin-balancing-cleanser">details ></a>
                    </li>
                    <li class="taller" id="dailyCleansingCloths">
                        <img src="../images/ourProducts/products/small/dailyCleansingCloths.png" alt="" />
                        <h3>DAILY CLEANSING CLOTHS</h3>
                        <p>Wipe away dirt to reveal healthy, glowing skin.</p>
                        <a href="../charcoal-for-oily-skin/daily-cleansing-cloths" id="details-daily-cleansing-cloths">details ></a>
                    </li>
                    
                    <li>
                        <img src="../images/ourProducts/products/small/4In1DetoxifyingCleanser.png" alt="" />
                        <h3>4-IN-1 DETOXIFYING CLEANSER</h3>
                        <p>Revitalize and refresh for healthier-looking skin.</p>
                        <a href="../charcoal-for-oily-skin/4-in-1-detoxifying-cleanser" id="details-4-in-1-detoxifying-cleanser">details ></a>
                    </li>
                     <li>
                        <img src="../images/ourProducts/products/small/steamActivatedCleanser.jpg" alt="" />
                        <h3>STEAM ACTIVATED CLEANSER</h3>
                        <p>Harness the power of pore-opening steam for a truly deep clean.</p>
                        <a href="../charcoal-for-oily-skin/steam-activated-cleanser" id="details-steam-activated-cleanser">details ></a>
                    </li> -->
                </ul>
            </div>
            
            <div id="bigPoresProds" class="prodList">
                <h2 class="pie roundedCorners bigPores"><span>Au Bicarbonate de Sodium !</span><br />Pour peaux mixtes
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img id="dontBeDirtyProducts" src="../images/ourProducts/products/small/Baking_Soda_Cleanser_small.png" alt="" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:8pt;"></span> Gel Nettoyant Exfoliant Doux</h3>
                        <p>Désobstrue les pores & exfolie tout en douceur</p>
                        <div id="BVRRInlineRating-baking-soda-pore-cleanser" class="ratingCategory"></div>
                        <a href="../baking-soda-for-combination-skin/baking-soda-pore-cleanser" id="A1">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-cleansing-scrub-small.png" alt="" />
                        <h3><span style="font-family: ProximaNova-Bold, Arial;color:yellow;font-size:8pt;"></span> Poudre Exfoliante en Profondeur</h3>
                        <p>Gomme en douceur & nettoie intensément</p>
                        <div id="BVRRInlineRating-baking-soda-cleansing-scrub" class="ratingCategory"></div>
                        <a href="../baking-soda-for-combination-skin/baking-soda-cleansing-scrub" id="A1">details ></a>
                    </li>             
                </ul>
            </div>

            <!--<div id="complexionClearingProds" class="prodList">
                <h2 class="pie roundedCorners complexionClearing">acne&rsquo;s <span>outta here!<sup>&reg;</sup></span>
                    <span></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishFightingIceCleanser.png" alt="" />
                        <h3>BLEMISH FIGHTING<br />ICE CLEANSER</h3>
                        <p>With Salicylic acid, this formula cleans pores of dirt, oil, and makeup.</p>
                        <div id="BVRRInlineRating-blemish-fighting-ice-cleanser" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/blemish-fighting-ice-cleanser" id="details-blemish-fighting-ice-cleanser">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/ACS_Tube.jpg" alt="" />
                        <h3>ACNE CLEARING<br />SCRUB</h3>
                        <p>Defeat deep-down dirt and oil in 2 days.</p>
                        <div id="BVRRInlineRating-acne-clearing-scrub" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/acne-clearing-scrub" id="details-acne-clearing-scrub">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishTreatingAstringentToner.png" alt="" />
                        <h3>BLEMISH FIGHTING ASTRINGENT</h3>
                        <p>Help prevent breakouts with an oil-free toner.</p>
                        <div id="BVRRInlineRating-blemish-fighting-astringent-toner" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/blemish-fighting-astringent-toner" id="details-blemish-fighting-astringent">details ></a>
                    </li>
                </ul>
            </div>-->

        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
