﻿using BVSeoSdkDotNet.Config;
using BVSeoSdkDotNet.Content;
using BVSeoSdkDotNet.Model;
using System;
using System.Configuration;
using System.Web;

namespace Biore.BazaarVoice
{
    public static class BVUtils
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Request"></param>
        /// <param name="category"></param>
        /// <returns></returns>
        public static string GetProductId(HttpRequest Request, out string category)
        {
            // For this site it is true that: product page url = Product Id.
            string id = Request.Url.Segments[Request.Url.Segments.Length - 1].Replace("/", "");

            string categoryPages = ConfigurationManager.AppSettings["BV_ProductCategories"];
            category = string.Empty;
            if (id.Equals("default.aspx") && Request.Url.PathAndQuery.Contains("biore-facial-cleansing-products"))
            {
                category = "biore-facial-cleansing-products";
            }

            return id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Request"></param>
        /// <param name="productId"></param>
        /// <param name="sBvOutputSummary"></param>
        /// <param name="sBvOutputReviews"></param>
        /// <returns></returns>
        public static bool GetBVSEO(HttpRequest Request, string productId, out string sBvOutputSummary, out string sBvOutputReviews)
        {
            bool Ok = true;
            try
            {
                BVConfiguration bvConfig = new BVSdkConfiguration();

                bvConfig.addProperty(BVClientConfig.CLOUD_KEY, ConfigurationManager.AppSettings["BV_CLOUD_KEY"]);
                bvConfig.addProperty(BVClientConfig.BV_ROOT_FOLDER, ConfigurationManager.AppSettings["BV_ROOT_FOLDER"]); //adjust this for each locale
                bvConfig.addProperty(BVClientConfig.STAGING, ConfigurationManager.AppSettings["BV_STAGING"]);
                bvConfig.addProperty(BVClientConfig.EXECUTION_TIMEOUT, ConfigurationManager.AppSettings["BV_EXECUTION_TIMEOUT"]);

                //Prepare pageURL and SubjectID/ProductID values.	
                String subjectID = productId;
                String pageURL = Request.Url.ToString();

                //http://seo-stg.bazaarvoice.com/bioreus-09666f5b5257b313b23e72d0ceccf9bf/Main_Site-en_US/reviews/product/1/shea-beauty-oil.htm
                //Set BV Parameters that are specific to the page and content type.
                BVParameters bvParam = new BVParameters();
                bvParam.UserAgent = Request.Headers.Get("User-Agent");
                bvParam.BaseURI = pageURL.Contains("?") ? pageURL.Substring(0, pageURL.IndexOf("?")) : pageURL;
                bvParam.PageURI = Request.Url.ToString(); //this value is used to extract the page number from bv URL parameters
                bvParam.ContentType = new BVContentType(BVContentType.REVIEWS);
                bvParam.SubjectType = new BVSubjectType(BVSubjectType.PRODUCT);
                bvParam.SubjectId = subjectID;

                //Get content and place into strings, then output into the injection divs.
                BVUIContent bvOutput = new BVManagedUIContent(bvConfig);
                sBvOutputSummary = bvOutput.getAggregateRating(bvParam);  //getAggregateRating delivers the AggregateRating section only
                sBvOutputReviews = bvOutput.getReviews(bvParam);  //getReviews delivers the review content with pagination only
            }
            catch (Exception)
            {
                sBvOutputSummary = sBvOutputReviews = string.Empty;
                Ok = false;
            }

            return Ok;
        }
    }
}