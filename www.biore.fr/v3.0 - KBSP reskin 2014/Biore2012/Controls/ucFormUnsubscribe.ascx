﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="KaoBrands.FormStuff.ucFormUnsubscribe" Codebehind="ucFormUnsubscribe.ascx.cs" %>

<div id="unsubscribe">
		<div id="container">
			<div id="main-content">
				<div id="optOut">
				    <asp:Panel ID="OptinForm" runat="server">
					    <h1>Unsubscribe</h1>
					    <p>If you'd no longer like to hear about the latest news, promotions and contests, just
						    enter your e-mail address below.</p>
					    <p class="req">required<span class="required">*</span></p>
					    <div class="tbin">
						    <div class="input-container">
							    <asp:Label ID="emailLbl" runat="server" AssociatedControlID="Email" Text="Email*" />
							    <asp:TextBox ID="Email" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
							    <div class="ErrorContainer">
							    <asp:RequiredFieldValidator ID="EmailValidator1" Display="Dynamic" runat="server"
								    ErrorMessage="Please enter your Email Address." ControlToValidate="Email" EnableClientScript="true"
								    SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
							    <asp:RegularExpressionValidator ID="EmailValidator2" Display="Dynamic" runat="server"
								    ErrorMessage="Please enter a valid Email Address." ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
								    ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RegularExpressionValidator>								
							    <asp:RegularExpressionValidator ID="EmailValidator3" runat="server" Display="Dynamic"
								    ErrorMessage="Please limit the entry to 50 characters."
								    ValidationExpression="^[\s\S]{0,50}$" ControlToValidate="Email" SetFocusOnError="true"
								    CssClass="errormsg"></asp:RegularExpressionValidator>
							    <asp:RegularExpressionValidator ID="EmailValidator4" runat="server" Display="Dynamic"
								    ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Email."
								    ValidationExpression="^[^<>]+$" ControlToValidate="Email" EnableClientScript="true"
								    SetFocusOnError="true" CssClass="errormsg" ></asp:RegularExpressionValidator>
							    </div>
							    <!--end Required Field Validator Container-->
						    </div>
						    
						    <div class="input-container">
							   <asp:Label ID="Label1" class="whyRadio" runat="server" AssociatedControlID="whyRadio">
	                                Please indicate why you are choosing to unsubscribe from Bior&eacute;<sup>&reg;</sup> Skincare communications
                                </asp:Label>
                                <div class="clear"></div>
                                <asp:RadioButtonList ID="whyRadio" class="whyRadioList" runat="server">
	                                <asp:ListItem Value="No longer using or interested in Biore(R) Skincare product">No longer using or interested in Bior&eacute;<sup>&reg;</sup> Skincare product</asp:ListItem>
	                                <asp:ListItem>Not enough samples, coupons, or promotions</asp:ListItem>
	                                <asp:ListItem>Communications too frequent</asp:ListItem>
	                                <asp:ListItem>Communications not relevant to my interests</asp:ListItem>
	                                <asp:ListItem>None of these</asp:ListItem>
                                </asp:RadioButtonList>
							    <div class="ErrorContainer"></div>
						    </div>
						    
					    </div>
					    <div id="submit-container">
						    <p>
							    <asp:Button UseSubmitBehavior="true" ID="submit" Text="Unsubscribe ›" runat="server" CssClass="submit buttonLink" />
						    </p>
					    </div>
				    </asp:Panel>
				    <asp:Panel ID="OptinFormSuccess" runat="server">
					    <h1>We're sorry to see you go...</h1>
					    <p>Your request has been sent and we&rsquo;ll go ahead and remove your e-mail address from our list.</p>
					    <p>If you change your mind, you can always re-subscribe right here on our website.</p>
					    <p><a href="<%= ResolveClientUrl("~/") %>">Return home</a>.</p>
				    </asp:Panel>
				    <asp:Panel ID="OptinFormFailure" runat="server">
					    <h1>We're Sorry...</h1>
					    <p>There has been a problem with your form submission.</p>
					    <p>Please go back and <a href="javascript: history.back(-1)">try again</a>.</p>
				    </asp:Panel>
				</div> <%-- end #optOut --%>
			</div> <%-- end #main-content --%>
			<div id="unsubscribe-img">
			</div>
		</div> <%-- end #container --%>
	</div>

