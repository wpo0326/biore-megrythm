﻿<%@ Page Title="Error | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.error._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Bioré® Skincare – Error" name="description" />

    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <div id="content">
<br/>
<br/>
<br/>
                    <h1>ERREUR</h1>
                    <p>Nous sommes désolés. La page demandée n’a pas pu être trouvée ou une erreur a été détectée.</p>
                    <div class="promoButton"><a href="http://biore.fr">RETOUR À LA PAGE D’ACCUEIL <span class="arrow">&rsaquo;</span></a></div>
                 </div>
                 <div id="photo">
                    <img src="<%= System.Configuration.ConfigurationManager.AppSettings["path"] %>images/utilityPages/polaroid.jpg" alt="" />
                 </div>
                 <div class="clear"></div>
            </div>
        </div>
    </div>
</asp:Content>