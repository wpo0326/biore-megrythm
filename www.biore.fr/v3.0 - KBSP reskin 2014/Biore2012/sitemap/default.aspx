﻿<%@ Page Title="PLAN DU SITE " MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.sitemap._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="PLAN DU SITE" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1 style="text-align:center;">PLAN DU SITE</h1>
                 <div id="responseRule"></div>
                 <div id="linkList">
                     <div id="prodLinks">
                         <ul class="col" id="col1">
                            <li><a href="../biore-facial-cleansing-products">Nos Produits <span class="arrow">&rsaquo;</span></a></li>
                            <li>
                                <h2 class="pie deepCleansing"><span class="subNavHeadlines"><span class="greenBold">Au Charbon ! </span>Pour peaux normales à grasses</span></h2>
                                <ul>
                                    <li><a href="../charcoal-for-oily-skin/deep-pore-charcoal-cleanser"><!--<span class="new">New</span>-->Gel Nettoyant en Profondeur au Charbon</a></li>
                                    <li><a href="../charcoal-for-oily-skin/charcoal-pore-minimizer">Gel Exfoliant Réducteur de Pores</a></li>
                                    <li><a href="../charcoal-for-oily-skin/self-heating-one-minute-mask">Masque Auto-Chauffant 1 Minute Chrono</a></li>
                                </ul>

                                <h2 class="pie takeitAllOff"><span class="subNavHeadlines"><span class="subNavHeadlines">un geste et <span class="purpleBold">c’est parti </span>!</span></h2>
                                <ul>
                                    <!--<li class="purpleHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/take-it-all-off/baking-soda-cleansing-micellar-water") %>" id="topnav-baking-soda-cleansing-micellar-water">Micellair Reinigingswater met Baking Soda</a></li>-->
                                    <li class="purpleHover"><a href="<%= VirtualPathUtility.ToAbsolute("~/take-it-all-off/charcoal-cleansing-micellar-water") %>" id="topnav-charcoal-cleansing-micellar-water">Eau Micellaire au Charbon</a></li> 
                                </ul>
                                

                            </li>
                        </ul>
                        <ul class="col" id="col2">
                            <li>
                                <h2 class="pie complexionClearing"><span class="subNavHeadlines"><span class="tealBold">Au Bicarbonate de Sodium ! </span>Pour peaux mixtes</span></h2>
                                <ul>
                                    <li><a href="../baking-soda-for-combination-skin/baking-soda-pore-cleanser">GEL NETTOYANT EXFOLIANT DOUX</a></li>
                                    <li><a href="../baking-soda-for-combination-skin/baking-soda-cleansing-scrub">POUDRE EXFOLIANTE EN PROFONDEUR</a></li>
                                </ul>
                                <h2 class="pie murt"><span class="subNavHeadlines"><span class="redBold">Patchs purifants</span></h2>
                                <ul>
                                    <li><a href="../charcoal-for-oily-skin/charcoal-pore-strips">PATCHS PURIFIANTS AU CHARBON</a></li>
                                    <li><a href="../pore-strips/pore-strips-combo">ASSORTIMENT DE 14 PATCHS PURIFIANTS</a></li>
                                </ul>
                                <ul>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <ul class="col noPadding" id="col3">
                        <!--<li>
                            <a href="../biore-facial-cleansing-products/charcoal.aspx">What's New <span class="arrow">&rsaquo;</span></a>
                        </li>-->
                        <!--<li>
                            <a href="../pore-care/">Pore Care<span class="arrow">&rsaquo;</span></a>
                        </li>-->
                        <!--<li><a href="../email-newsletter-sign-up">Sign Me Up <span class="arrow">&rsaquo;</span></a></li>-->
                        <li><a href="../where-to-buy-biore">OÙ TROUVER BIORÉ ? <span class="arrow">&rsaquo;</span></a></li>
                        <!--<li><a href="../biore-videos">Watch It Work <span class="arrow">&rsaquo;</span></a></li>-->
                        <li><a href="../about-us">À PROPOS DE BIORÉ <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaocareers.com/" target="_blank">Kao <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../contact-us">NOUS CONTACTER <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../legal/">MENTIONS LÉGALES <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../privacy/">POLITIQUE DE CONFIDENTIALITÉ <span class="arrow">&rsaquo;</span></a></li>
                     </ul>
                 </div>
                 <div id="photo"></div>
            </div>
        </div>
    </div>
</asp:Content>