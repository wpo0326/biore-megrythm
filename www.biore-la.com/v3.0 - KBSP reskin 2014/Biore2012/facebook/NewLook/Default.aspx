﻿<%@ Page Title="" Language="C#" MasterPageFile="~/facebook/FBContentPageMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Biore2012.facebook.NewLook.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div id="fb-root">
	</div>

	<script type="text/javascript">
		var extJsHost_connect_facebook_net = (("https:" == document.location.protocol) ? "https://" : "http://");
		window.fbAsyncInit = function() {
			FB.init({
				appId: '<%= System.Configuration.ConfigurationManager.AppSettings["newlookFBAppId"] %>', // App ID
				//channelURL : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
				status: true, // check login status
				cookie: true, // enable cookies to allow the server to access the session
				oauth: true, // enable OAuth 2.0
				xfbml: true  // parse XFBML
			});

			// Additional initialization code here
			FB.Canvas.setAutoGrow();
			FB.Canvas.scrollTo(0, 0);
		};

		// Load the SDK Asynchronously
		(function(d) {
			var js, id = 'facebook-jssdk'; if (d.getElementById(id)) { return; }
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = extJsHost_connect_facebook_net + "connect.facebook.net/en_US/all.js";
			d.getElementsByTagName('head')[0].appendChild(js);
		} (document));
	</script>
	<div class="newLook">
		<h1>Allow us to freshen up (our look) a bit</h1>
		<div id="newLookCopy">
			<h2>New Look</h2>
			<p>Say goodbye to green&mdash;we're stepping up the style of all our Bior&eacute;<sup>&reg;</sup>
				Skincare products with fresh white and blue hues! We designed our clean new look
				to match what's inside: the same innovative formulas that keep your complexion feeling
				fresh and clean.</p>
			<a href="<%=System.Configuration.ConfigurationManager.AppSettings["path"] %>biore-skincare" class="linkBtn" target="_blank">Take a Peek ></a>
		</div>
		<div id="proveIt">
			<h2 id="proveItHdr">Keep earning rewards</h2>
			<p>And while our look's getting an upgrade, you'll still be able to earn rewards with
				<a href="http://www.facebook.com/bioreskin?sk=app_205787372796203" target="_blank">Prove It!&trade; Reward Points</a> on Facebook! Sign
				up today to start racking up points<br />and redeem them for coupons, free<br />products,
				plus, more to come!</p>
			<a href="http://www.facebook.com/bioreskin?sk=app_205787372796203" class="linkBtn" target="_blank">Start Earning Now ></a>
		</div>
		<div id="cleanSkin">
			<h2>Join the clean skin movement</h2>
			<p>Sign up for our newsletter to be the first to hear all about the latest offers and
				promos from Bior&eacute;<sup>&reg;</sup> Skincare. </p>
			<a href="<%=System.Configuration.ConfigurationManager.AppSettings["path"] %>email-newsletter-sign-up" class="linkBtn" target="_blank">Sign me up ></a>
		</div>
		<h3 id="logoTagline">Bior&eacute;<sup>&reg;</sup>. Get your best clean.</h3>
	</div>
</asp:Content>
