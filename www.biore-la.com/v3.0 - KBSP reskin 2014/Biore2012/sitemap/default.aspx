﻿<%@ Page Title="Sitemap | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.sitemap._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
    <style type="text/css">
        #linkList {overflow:visible}
        #footer {clear:both;}
        .utility #photo {display:none;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1>Mapa del sitio </h1>
                 <div id="linkList">
                     <div id="prodLinks">
                         <ul class="col" id="col1">
                            <li><a href="../biore-facial-cleansing-products">Nuestros productos <span class="arrow">&rsaquo;</span></a></li>
                            <li>
                                <h2 class="pie deepCleansing"><span class="subNavHeadlines"><span class="greenBold">para todo tipo de piel</span></span></h2>
                                <ul>
									<li><a href="/dont-be-dirty/baking-soda-pore-cleanser">Baking Soda Limpiador de Poros con bicarbonato de sodio</a></li>
                                            <li><a href="/dont-be-dirty/baking-soda-cleansing-scrub">Baking Soda Limpiador en Polvo con bicarbonato de sodio</a></li>
                                            <li><a href="/dont-be-dirty/deep-pore-charcoal-cleanser">Limpiador Profundo de Poros con Carbón Natural</a></li>
                                            <li><a href="/dont-be-dirty/self-heating-one-minute-mask">Mascarilla térmica con carbón natural</a></li>
                                            <li><a href="/dont-be-dirty/pore-unclogging-scrub">EXFOLIADOR DESINCRUSTANTE</a></li>
                                            <li><a href="/dont-be-dirty/make-up-removing-towelettes">Toallitas Desmaquillantes</a></li>
										    <li><a href="/dont-be-dirty/charcoal-bar">Barra Limpiadora de Poros Con Carbón Natural</a></li>
										    <li><a href="/back-off-big-pores/charcoal-pore-minimizer">Limpiador Minimizador de Poros Con Carbón Natural</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="col" id="col2">
                            <li>
                                <h2 class="pie complexionClearing"><span class="subNavHeadlines"><span class="orangeBold">para piel con granos y espinillas</span></h2>
                                <ul>
                                    <li><a href="/acnes-outta-here/baking-soda-acne-scrub">EXFOLIADOR CON BAKING SODA PARA PIEL CON GRANOS Y ESPINILLAS</a></li>
                                            <li><a href="/acnes-outta-here/charcoal-acne-scrub">EXFOLIADOR CON CARBÓN NATURAL PARA PIEL CON GRANOS Y ESPINILLAS</a></li>
                                            <li><a href="/acnes-outta-here/charcoal-acne-clearing-cleanser">LIMPIADOR CON CARBÓN NATURAL PARA PIEL CON GRANOS Y ESPINILLAS</a></li>
                                            <li><a href="/acnes-outta-here/blemish-fighting-astringent-toner">Astringente para Piel con granos y espinillas</a></li>
                                            <li><a href="/acnes-outta-here/acne-clearing-scrub">EXFOLIADOR DIARIO PARA PIEL con granos y espinillas</a></li>
                                            <li><a href="/acnes-outta-here/blemish-fighting-ice-cleanser">GEL LIMPIADOR REFRESCANTE PARA PIEL con granos y espinillas</a></li> 
                                </ul>
                                
                                <h2 class="pie murt"><span class="subNavHeadlines"><span class="redBold">para piel con puntos negros</span></span></h2>
                                <ul>
                                     <li><a href="/breakup-with-blackheads/pore-strips#ultra">BANDAS ULTRA DE LIMPIEZA PROFUNDA</a></li>
                                            <li><a href="/breakup-with-blackheads/pore-strips#regular">BANDAS DE LIMPIEZA PROFUNDA</a></li>
                                            <li><a href="/breakup-with-blackheads/warming-anti-blackhead-cleanser">Limpiador Térmico Para Piel Con Puntos Negros</a></li>
                                            <li><a href="/breakup-with-blackheads/pore-strips#combo">bandas de limpieza combo</a></li>
											<li><a href="/breakup-with-blackheads/charcoal-pore-strips#regular">Bandas de Limpieza Profunda Con Carbón Natural</a></li>
                                </ul>
                                <ul>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <ul class="col noPadding" id="col3">
                        <li>
                            <a href="/">Lo nuevo <span class="arrow">&rsaquo;</span></a>
                  
                        </li>
                        <li>
                            <a href="../pore-care/">CUIDADO DE LOS POROS <span class="arrow">&rsaquo;</span></a>
                  
                        </li>
                        <!--<li><a href="../email-newsletter-sign-up">Registrate <span class="arrow">&rsaquo;</span></a></li>-->
                        <li><a href="../where-to-buy-biore">D&oacute;nde Comprar <span class="arrow">&rsaquo;</span></a></li>
                        <!--<li><a href="../biore-videos">Watch It Work <span class="arrow">&rsaquo;</span></a></li>-->
                        <li><a href="../about-us">Acerca de Bior&eacute;<sup>&reg;</sup> Skincare <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaobrands.com/" target="_blank">Kao USA Inc. <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../contact-us">Contactanos <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../legal">Legal <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="/privacy/" target="_blank">Política de privacidad <span class="arrow">&rsaquo;</span></a></li>
                     </ul>
                 </div>
                 <div id="photo"></div>
            </div>
        </div>
    </div>
</asp:Content>