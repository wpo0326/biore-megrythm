﻿<%@ Page Title="Sitemap | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.sitemap._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1>Mapa del sitio </h1>
                 <div id="linkList">
                     <div id="prodLinks">
                         <ul class="col" id="col1">
                            <li><a href="../biore-facial-cleansing-products">Nuestros productos <span class="arrow">&rsaquo;</span></a></li>
                            <li>
                                <h2 class="pie deepCleansing"><span class="subNavHeadlines"><span class="greenBold">para todo tipo de piel</span></span></h2>
                                <ul>
                                    <li><a href="../dont-be-dirty/deep-pore-charcoal-cleanser"><!--<span class="new">New</span>-->Limpiador Profundo de Poros con Carbón Natural</a></li>
                                    <li><a href="../dont-be-dirty/self-heating-one-minute-mask">Mascarilla Térmico con Carbón Natural</a></li>
                                    <li><a href="../dont-be-dirty/pore-unclogging-scrub">EXFOLIADOR DESINCRUSTANTE</a></li>
                                    <li><a href="../dont-be-dirty/combination-skin-balancing-cleanser">Gel Limpiador Equilibrante Para Piel Mixta</a></li>
                                    <li><a href="../dont-be-dirty/daily-cleansing-cloths">Toallitas Desmaquillantes</a></li>
                                    <li><a href="../dont-be-dirty/make-up-removing-towelettes">Toallitas Desmaquillantes</a></li>
                                    <li><a href="../dont-be-dirty/pore-detoxifying-foam-cleanser">Espuma Limpiador Desintoxicante</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="col" id="col2">
                            <li>
                                <h2 class="pie complexionClearing"><span class="subNavHeadlines"><span class="orangeBold">para piel propensa al acne</span></h2>
                                <ul>
                                    <li><a href="../acnes-outta-here/blemish-fighting-astringent-toner">Astringente para Piel Propensa al Acne</a></li>
                                    <li><a href="../acnes-outta-here/acne-clearing-scrub">Exfoliador Diario para Piel Propensa al Acne</a></li>
                                    <li><a href="../acnes-outta-here/blemish-fighting-ice-cleanser">Gel Limpiador Refrescante para piel propensa al acne</a></li>
                                </ul>
                                <h2 class="pie murt"><span class="subNavHeadlines"><span class="redBold">para piel con puntos negros</span></span></h2>
                                <ul>
                                    <li><a href="../breakup-with-blackheads/pore-strips#ultra">BANDAS ULTRA DE LIMPIEZA PROFUNDA</a></li>
                                    <li><a href="../breakup-with-blackheads/pore-strips#regular">BANDAS DE LIMPIEZA PROFUNDA</a></li>
                                    <li><a href="../breakup-with-blackheads/warming-anti-blackhead-cleanser">Limpiador Térmico Para Piel Con Puntos Negros</a></li>
                                    <li><a href="../breakup-with-blackheads/pore-strips#combo">Bandas de limpieza combo</a></li>
                                </ul>
                                <ul>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <ul class="col noPadding" id="col3">
                        <li>
                            <a href="../biore-facial-cleansing-products/charcoal.aspx">Lo nuevo <span class="arrow">&rsaquo;</span></a>
                  
                        </li>
                        <li>
                            <a href="../pore-care/">Pore Care <span class="arrow">&rsaquo;</span></a>
                  
                        </li>
                        <li><a href="../email-newsletter-sign-up">Registrate <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../where-to-buy-biore">D&oacute;nde Comprar <span class="arrow">&rsaquo;</span></a></li>
                        <!--<li><a href="../biore-videos">Watch It Work <span class="arrow">&rsaquo;</span></a></li>-->
                        <li><a href="../about-us">Acerca de Bior&eacute;<sup>&reg;</sup> Skincare <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaobrands.com/" target="_blank">Kao Brands Company <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../contact-us">Contactanos <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaobrands.com/legal.asp" target="_blank">Legal <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../privacy">Política de privacidad <span class="arrow">&rsaquo;</span></a></li>
                     </ul>
                 </div>
                 <div id="photo"></div>
            </div>
        </div>
    </div>
</asp:Content>