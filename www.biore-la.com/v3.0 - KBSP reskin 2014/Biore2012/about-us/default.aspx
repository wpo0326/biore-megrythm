﻿<%@ Page Title="Acerca de Bior&eacute;&reg; Skincare | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.about_us._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Learn more about Bioré® Skincare" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div class="centeringDiv">
                <div id="content">
                    <div class="titleh1">
                        <h1>Acerca de Bior&eacute;<sup>&reg;</sup> Skincare</h1>
                        
                        <div id="responseRule"></div>
                    </div>
                     <p>Pon la raíz de todos los problemas de la piel en la mira, exfoliando cada semana y limpiando todos los días. Nuestros poderosos productos limpiadores de poros están disponibles en forma de líquido, espuma, exfoliador y banda para mantener tu piel limpia y saludable.</p>
                    <img src="../images/about/aboutBG.jpg" alt="" id="Img1" />
                   
                    <!--<p class="boldBlue">Bior&eacute;<sup>&reg;</sup> Skincare products are available at select food, drug and mass&ndash;merchant stores</p>-->
                </div>
            </div>
        </div>
    </div>
</asp:Content>

