﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.pore_care.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Conceptos básicos de cuidado de los poros | Bioré® Skincare" name="title" />
    <meta content="Clear Blemishes & Clogged Pores with a Pore Care Regimen from Bioré® Skincare." name="description" />
    <meta content="How to clean clogged pores, how to clean pores, clear blemishes, how to clear blemishes, blemish clearing" name="keywords" />

    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/poreCare.css")
        .Render("~/css/combinedprorecare_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="floodlightPixels" runat="server">
   <!-- header scripts here -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div class="centeringDiv">
                <div id="content">
                    <div class="titleh1">
                        <h1>Conceptos básicos de cuidado de los poros</h1>
                        <div class="ruleBGa"></div>
                        <div class="responseRule"></div>

                    </div>
                    <div class="rowCont">
                    	<div class="row1"> 
		                    <div class="whyCare"><h4>¿POR QUÉ PREOCUPARTE?</h4><h2>Mantener los poros limpios y saludables</h2><p>es importante para mantener una piel de aspecto saludable, ya que un adulto promedio tiene aproximadamente 200,000 poros faciales.</p></div>
                                
		                </div>
                       <div class="row2">
                            <div class="col1"><h4>LA PREGUNTA ES:</h4><h2>¿Pueden los poros<br />tapados causar acné e imperfecciones?</h2><p><b>¿Realmente? ¡Sí! </b>La suciedad y la grasa se acumulan alrededor de los poros, formando residuos que pueden tapar los folículos cutáneos. Si las bacterias llegan hasta el área bloqueada y se multiplican, esto puede causar inflamación y provocar brotes e imperfecciones. La hinchazón y el enrojecimiento pueden persistir en forma de barros, espinillas y zonas de la piel enrojecidas e inflamadas.</p></div>
		                    <div class="col2"><img src="../images/poreCare/poreCare2.jpg" alt="" id="Img2" /></div>
		                </div>
                        <div class="row3">
                            <div class="col1 colDesktop"><img src="../images/poreCare/poreCare3.jpg" alt="" id="Img3" /></div>
		                    <div class="col2"><h4>LA SOLUCIÓN:</h4><h2>Limpia, tonifica, elimina.</h2><p>Nuestro régimen contra las imperfecciones trabaja de maravilla. Solo selecciona un limpiador, un astringente y una banda de limpieza profunda para obtener una piel limpia.<br /><br />Si tienes acné, el ácido salicílico es uno de los mejores ingredientes para combatirlo. Ayuda a reducir el sebo y limita el crecimiento bacteriano. Al provocar que la capa superior de la piel se hinche, suavice y luego pele, el ácido salicílico corrige el desprendimiento anormal de las células. En casos de acné más leves, ayuda a destapar los poros para sanar y prevenir lesiones. Sorprendente.</p></div>
                             <div class="col1 colMobile"><img src="../images/poreCare/poreCare3.jpg" alt="" id="Img5" /></div>
		                </div>
                        <div style="clear:both;"></div>
                        <div id="Tips10">
                            <p class="largeNumber">10</p>
                            <p class="tipsFor green">CONSEJOS DEL</p>
                            <!--<p class="clearSkin">DR. WESTLEY PARA</p>-->
                            <p class="clearSkin2 green">LIMPIAR LA PIEL:</p>
                            <div class="ruleBG"></div>
                        </div>
		                <div class="bullets">	
                            <h3 class="green">• Siga una rutina para el cuidado de los poros.</h3>
	                        <h3>• Limpie la piel cuidadosamente con un limpiador suave.</h3>
	                        <h3 class="green">• Retire toda la suciedad y el maquillaje.</h3>
	                        <h3>• Lávese el rostro dos veces por día, especialmente después de una actividad física.</h3>
	                        <h3 class="green">• Evite exfoliar la piel en exceso o lavarla demasiadas veces.</h3>
	                        <h3>• No apriete, rasque, quite o frote las espinillas. Al hacerlo, puede provocar infecciones o generar cicatrices.</h3>
	                        <h3 class="green">• Evite tocarse la cara con las manos.</h3>
	                        <h3>• Evite cremas y cosméticos oleosos.</h3>
	                        <h3 class="green">• Utilice fórmulas a base de agua o no comedogénicas.</h3>
	                        <h3>• No se exponga al sol. Los rayos ultravioleta pueden empeorar el acné. Gravemente.</h3>
		                </div>
	                </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
    <%--<h1>pore care - scripts</h1>--%>
</asp:Content>
