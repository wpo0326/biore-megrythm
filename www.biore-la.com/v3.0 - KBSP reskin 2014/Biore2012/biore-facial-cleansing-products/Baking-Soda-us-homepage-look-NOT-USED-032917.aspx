﻿<%@ Page Title="Baking Soda Scrub and Baking Soda Cleanser | Bioré® Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Baking-Soda.aspx.cs" Inherits="Biore2012.biore_facial_cleansing_products.Charcoal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta name="description" content="Deep clean pores and gently exfoliate with our Baking Soda Scrub and Baking Soda Cleanser. Try Baking Soda Cleansing Scrub and Baking Soda Pore Cleanser from Bioré® Skincare." />
    <meta content="Charcoal Mask, Charcoal Face Wash, charcoal" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/baking-soda-page-2017.css")
        .Add("~/css/flexslider.css")
        .Add("~/css/UniversPro67.css")
        .Render("~/CSS/combinedbakingsoda_#.css")
    %>
    <meta name="viewport" content="width=device-width">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main1">
    <div class="home-content">
        <section id="section-2017-interim">
            <div class="top-baking-soda-2017"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/baking-soda-bg-blue-matte-122216.png") %>" alt="baking soda bubbles" /></div>
            <div class="welcome-module">
                <div class="header-2017">
                    <div class="acne-header-2017">      
                        <h2>
                            <span class="acnes">¡ADIOS A LOS GRANOS!</span>
                            <!--<span class="outtahere">A LOS GRANOS!</span>-->
                            <!--<span class="oneporetime">One Pore At A Time.</span>-->
                        </h2>
                    </div>
                    <div class="acne-solutions-2017">
                        <div class="acne-solutions-text-2017"><span class="solutions1">CON EL PODER DEL</span>
                                                            <span class="solutions2">bicarbonato de sodio</span>
                                                            <span class="solutions3">y del</span>
                                                            <span class="solutions4">carbón</span></div>
                        <div class="acne-solutions-img-2017"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/acne-solutions-badge-spanish.png") %>" alt="Acne Solutions" /></div>

                    </div>
                </div>
                <div class="acne-products-2017">
                    <div class="baking-soda-2017">
                        <div class="baking-soda-img-2017"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/baking-soda-acne-products-122216.png") %>" alt="Baking Soda Acne Scrub" />
                            <div class="violator"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/acne-products-new-violator-122216.png") %>" alt="NEW!" /></div>
                        </div>
                        <div class="baking-soda-text-2017">
                            <h3>PIEL MÁS LIMPIA EN SOLO <span>2</span> DÍAS.</h3>
                            <p>Limpia profundamente y exfolia con suavidad con el bicarbonato de sodio natural y la capacidad combativa contra las imperfecciones del ácido salicílico.
                                <!--<a href="/en-US/acnes-outta-here/baking-soda-acne-scrub"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/acne-products-button.png") %>" alt="button"/></a>-->
              
             


                            </p>
                        </div>
                    </div>

                    <div class="charcoal-2017">
                        <div class="charcoal-text-2017">
                            <h3>PIEL MÁS LIMPIA EN SOLO <span>2</span> DÍAS</h3>
                            <p>Limpia profundamente o exfolia mientras absorbe el exceso de grasa con el carbón natural y la capacidad combativa contra las imperfecciones del ácido salicílico.
                                <!--<a href="/en-US/acnes-outta-here/charcoal-acne-scrub"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/acne-products-button.png") %>" alt="button"/></a>-->
                            </p>

                        </div>
                        <div class="charcoal-img-2017"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/charcoal-acne-products-122216.png") %>" alt="Charcoal Acne Clearing Cleanser and Charcoal Acne Scrub" />
                            <div class="violator"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/acne-products-new-violator-122216.png") %>" alt="NEW!" /></div>
                        </div>
                    </div>
                </div>

                 <div class="hide-mobile"></div>
                <!--<div class="scrollDownHldr"> 
                    <p class="scrollDown">SCROLL DOWN</p>
                        <div id="arrowBounce">
                            <a href="#section-01"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/arrowDown.png") %>" class="section-down-arrow"/></a>
                        </div>

                </div>-->
            </div>
            
            <!--<div class="scroll-dots">
		        <a href="#section-2017-interim"><span class="scroll-dot enable"></span></a>
                <a href="#section-01"><span class="scroll-dot"></span></a>
                <a href="#section-02"><span class="scroll-dot"></span></a>
                <a href="#section-03"><span class="scroll-dot"></span></a>
                <a href="#section-04"><span class="scroll-dot"></span></a>
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>-->
             <div class="bottom-charcoal-2017"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/charcoal-bg-top-panel-122216.png") %>" alt="charcoal dust" /></div>
        </section> 
    </div>
  
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
