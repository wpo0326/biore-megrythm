﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Biore2012.BLL;
using Biore2012.DAL;
using System.Configuration;
using System.Web.UI.HtmlControls;
using KaoBrands.FormStuff;
using KAOForms;

namespace Biore2012.FormConfig
{
    public partial class ContactUs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Manually register the event-handling method for the  
            // ServerValidate event of the CustomValidator control.
            // FNameValidator3.ServerValidate +=
            // new ServerValidateEventHandler(this.TextBoxLengthValidator);


            Page.Header.DataBind();

            // Grab masterpage, set appropriate body classes
            Site myMaster = (Site)this.Master;
            myMaster.bodyClass += " forms";

            if (Request.Url.ToString().Contains("email-newsletter-sign-up")) myMaster.bodyClass += " signUp";
            else if (Request.Url.ToString().Contains("contact-us")) myMaster.bodyClass += " contactUs";

            // This turns on and off the Question Pro Include for this page...
            ((Panel)Page.Master.FindControl("panel_Question_Pro")).Visible = false;

            if (!IsPostBack)
            {

                ContactFormSuccess.Visible = false;
                ContactFormFailure.Visible = false;

                CheckHookID();

                //litContactImage.Text = System.Configuration.ConfigurationManager.AppSettings["imageprefix"]; +"forms/contactFormSalonPool.jpg";
                DateTime dt = DateTime.Now;
                BioreUtils ut = new BioreUtils();

                ut.queueNumber(dt.Year - 100, dt.Year - 13, "Año", yyyy); //Build Year
                ut.queueNumber(1, 13, "Mes", mm); //Build Day
                ut.queueNumber(1, 32, "Día", dd); //Build Month
            }
            else
            {
                Page.Validate();
                string strEntryReturn = "";
                string strMailReturn = "";

                if (Page.IsValid)
                {

                    ContactForm.Visible = false;
                    //int validEntry = -1;

                    //you must flip month and day here because when the culture is set to Mexico (es-MX), day comes before month.
                    //System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
                    //Convert.ToDateTime(string, culture);
                    string DOB = dd.SelectedValue + "/" + mm.SelectedValue + "/" + yyyy.SelectedValue;

                    //*** PROCESS AS POSSIBLE OPTIN (LEAVE BLANK IF NO OPTIN VALUE) --creates new "member" and adds to event log.
                    //*** THEN SEND EMAIL TO CRS

                    string strFName = FName.Text;
                    string strLName = LName.Text;
                    string strEmail = Email.Text;
                    string strEmailRetype = RetypeEmail.Text;
                    string strStreet1 = Address1.Text;
                    string strStreet2 = Address2.Text;
                    string strAddress3 = Address3.Text;
                    string strCity = City.Text;
                    string strProvince = State.SelectedItem.ToString();
                    string strPostalCode = PostalCode.Text;
                    string strCountry = Country.SelectedValue.ToString();
                    string strGender = Gender.SelectedValue.ToString();
                    string strDOBdd = dd.SelectedValue;
                    string strDOBmo = mm.SelectedValue;
                    string strDOByr = yyyy.SelectedValue;
                    string strDOB = DOB;
                    string strPhone = Phone.Text;
                    string strMobilePhone = "";
                    string strComment = QuestionComment.Text;

                    string strEventCode = System.Configuration.ConfigurationManager.AppSettings["EventCode_contact_us"].ToString();
                    int intEventID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["EventID_contact_us"]);
                    int intSiteID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SiteID"]);

                    string strURL = Request.RawUrl == null ? "" : Request.RawUrl.ToString();
                    string strRemoteAddr = Request.ServerVariables["REMOTE_ADDR"] == null ? "" : Request.ServerVariables["REMOTE_ADDR"].ToString();
                    string strUserAgent = Request.ServerVariables["HTTP_USER_AGENT"] == null ? "" : Request.ServerVariables["HTTP_USER_AGENT"].ToString(); ;

                    string strContactContent = "";
                    string strContactVia = "";
                    string strContactFreq = "";

                    //CRS-specific variables
                    string strCrs_Opt_In_Out = "0";
                    string strCrs_Age = GetCRSAge(Convert.ToDateTime(DOB));
                    string strCrs_Website = System.Configuration.ConfigurationManager.AppSettings["crs_Website"].ToString();  //johnfrieda.com
                    string strCrs_Email = System.Configuration.ConfigurationManager.AppSettings["crs_EmailAddress"].ToString();  //the CRS "to" address
                    string strCrs_Subject = "Contact from " + strCrs_Website;
                    string strCrs_ContactFrom = "contact@" + strCrs_Website;

                    string strHookID = hfHookID.Value;

                    //bool blnNewsletter = optin.Checked ? true : false;
                    //bool blnSamples = sampleRequest.Checked ? true : false;

                    //string strNewsletterReturn = "";
                    //string strSamplesReturn = "";                    

                    //INSERT NEWSLETTER OPT-IN VALUES IF USER OPTED IN.
                    //if (blnNewsletter)
                    //{
                    //    strContactContent = "samples,contests,sweepstakes,other info,";
                    //    strContactVia = "email,direct mail,";
                    //    strContactFreq = "as needed,";
                    //}

                    //PROCESS ENTRY.
                    strEntryReturn = CGOptin.ProcessOptIn(intSiteID, intEventID, strFName, strLName, strEmail, strDOBdd, strDOBmo, strDOByr,
                                              strStreet1, strStreet2, strProvince, strCity, strPostalCode, strCountry, strPhone, strMobilePhone,
                                              strGender, strURL, strRemoteAddr, strUserAgent, strEventCode,
                                              strContactContent, strContactVia, strContactFreq, strURL.Replace("http://", ""), strComment, strHookID);


                    //SEND EMAIL TO CRS.

                    //mail for support rep
                    strMailReturn = CGMail.MultiPartMimeContact(strEmail, strCrs_ContactFrom, strCrs_Email, strCrs_Subject,
                                            strFName, strLName, strCrs_Age, strStreet1, strStreet2, strCity, strProvince,
                                            strPostalCode, strCountry, strPhone, strComment, strCrs_Website, strCrs_Opt_In_Out);

                }

                if (strEntryReturn.IndexOf("TRUE") != -1 && strMailReturn == "TRUE")
                {
                    ContactFormSuccess.Visible = true;
                    ContactDetails.Visible = false;
                    ContactForm.Visible = false;
                    Master.Page.Title = "John Frieda&reg; Hair Care - Contact Us - Confirmation";
                }
                else
                {
                    //Oops!
                    litError.Text = "<span style=\"color:#C00\">An error has occurred!<br />" + strEntryReturn + "<br /><br />" + strMailReturn + "</span>";
                    ContactFormFailure.Visible = true;
                    ContactDetails.Visible = false;
                    ContactForm.Visible = false;
                }
            }
        }

        protected void DOBValidate(object source, ServerValidateEventArgs args)
        {
            try
            {

                int Day = int.Parse(dd.SelectedValue.ToString());
                int Month = int.Parse(mm.SelectedValue.ToString());
                int Year = int.Parse(yyyy.SelectedValue.ToString());

                DateTime Test = new DateTime(Year, Month, Day);

                //Are they 13 years of age or older?
                if (Test > DateTime.Now.AddYears(-13))
                {
                    args.IsValid = false;
                    //redirect to sorry page
                    //Response.Redirect("sorry.aspx");
                }
                else
                {
                    args.IsValid = true;
                }

            }
            catch (FormatException Arg)
            {
                string error = Arg.ToString();
                // item not selected (couldn't convert int)
                args.IsValid = false;
            }
            catch (ArgumentOutOfRangeException Arg)
            {
                string error = Arg.ToString();
                // invalid date (31 days in February)
                args.IsValid = false;
            }
        }


        protected string GetCRSAge(DateTime birthday)
        {
            string strReturn = "";

            // set current time
            DateTime now = DateTime.Today;

            // get the year difference
            int years = now.Year - birthday.Year;

            // subtract another year if birthday hasn't yet occurred in same year
            if (now.Month < birthday.Month || (now.Month == birthday.Month && now.Day < birthday.Day))
                --years;

            strReturn = years.ToString();
            //Response.Write(strReturn);
            return strReturn;
        }

        public void CheckHookID()
        {
            //Find hook id passed in query string, if available.  This is a means by which we can track promo source (e.g. Facebook)
            string strHookIDRequest = Request.QueryString["hook"] == null ? System.Configuration.ConfigurationManager.AppSettings["HookID"].ToString() : Request.QueryString["hook"].ToString();

            hfHookID.Value = strHookIDRequest;
        }

    }

}
