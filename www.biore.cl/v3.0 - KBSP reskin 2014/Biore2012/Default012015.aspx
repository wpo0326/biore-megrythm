﻿<%@ Page Title="Obtén un rostro limpio | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Obtén una piel saludable y radiante con productos Bioré® Skincare" name="description" />
    <meta content="Skincare, rostro limpio" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/homepage.css")
        .Add("~/css/flexslider.css")
        .Render("~/CSS/combinedhome_#.css")
    %>
    <meta name="viewport" content="width=device-width">

    <script type="text/javascript">
         $(function() {
             $('.fma1Find a').click(
                 function (e) {
                     e.preventDefault();

                     var axel = Math.random() + "";
                     var a = axel * 10000000000000;
                     var strScript = "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore551;ord='" + a + "'?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"                     
                                + "<noscript>"
                                + "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore551;ord=1?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"
                                + "</noscript>";

                     $('body').prepend(strScript);
                     var target = $(this).attr('target');
                     var uri = $(this).attr('href');

                     setTimeout(function () {
                         if (target) {
                             window.open(uri, '_blank');
                         } else {
                             window.location = uri;
                         }
                     }, 1000);
                     
                 });
         })
    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div class="flexslider"  id="theater" >
            <ul class="slides">
              <!--<li id="theaterItem1" class="theaterHolder">
                <div class="fma1Product"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/slider-cl-01.png") %>" alt="" /></div>
                <div class="fma1headline">
                    <span class="fma1Intro">CONOCE <span class="fma1Char">EL CARBÓN</span></span><br />
                    <span class="fma1New">LA NUEVA ARMA SECRETA</span><br />
                    <span class="fma1From">DE LOS EXPERTOS DE BIORÉ® SKINCARE</span><br />
                    <div class="fma1FindWrapper">
                        <div class="fma1Find">
                            <a href="<%= VirtualPathUtility.ToAbsolute("~/biore-facial-cleansing-products/charcoal.aspx") %>">Obtén más información</a>
                        </div>
                    </div>
                    </div>
               </li>-->
                <li id="theaterItem1" class="theaterHolder">
                    
                    <div><img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/slider-cl-02.png") %>" alt="" /></div>
       

                </li>
               <!--<li id="theaterItem3" class="theaterHolder">        
                    <div class="fma2Headline">Start Earning Your</div><br />
                        <div class="fma2HeadlineTwo">Way to <span class="fma2Great">Great Rewards!</span></div>
                              <div class="fma2Bubbles"><div class="fma2Prove"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/fmas/fma2Prove.png") %>" alt="" /></div></div>
                                <div class="fma2HowWrap">
                                  <div class="fma2row">
                                      <div id="fma2How">How?</div>
                                      <div id="fma2List">
                                          <div>&#8226; Earn <b>Prove It!&reg;</b> Reward Points.</div>
                                          <div>&#8226; Redeem.</div>
                                          <div>&#8226; Celebrate.</div>
                                      </div>
                                  </div>
                                  <div id="mobileFMA" class="fma2row">
                                      <div id="fma2ListM">
                                          <div>&#8226; Earn <b>Prove It!&reg;</b> Reward Points.</div>
                                          <div>&#8226; Redeem. &#8226; Celebrate.</div>
                                      </div>
                                  </div>
                              </div>
                      <a target="_blank" href="https://apps.facebook.com/proveitrewards/"><div class="fma2Face">Start Earning On Facebook</div></a>
                 </li>-->             
            </ul>
            <div class="centeringDiv" id="pagerWrapper"><div id="pager"></div></div>
        </div>
        
    </div>
  
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/jquery.flexslider.min.js")
        .Render("~/js/combinedhome_#.js")
    %>
</asp:Content>