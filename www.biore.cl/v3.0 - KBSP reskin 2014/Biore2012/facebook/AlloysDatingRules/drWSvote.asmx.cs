﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;

namespace Biore2012.facebook.AlloysDatingRules
{
	/// <summary>
	/// Summary description for drWSvote
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	[System.Web.Script.Services.ScriptService]
	public class drWSvote : System.Web.Services.WebService
	{

		[WebMethod]
		//[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public string addVote(string drID, string modifier)
		{
			string status;
			string connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbConnection"].ConnectionString;
				// Add to UpVotes or DownVotes based on modifier
				using (SqlConnection con = new SqlConnection(connString))
				{

					SqlCommand cmd = con.CreateCommand();
					cmd.CommandText = "sp201202FBDatingRulesTopTenChangeVote";
					cmd.CommandType = CommandType.StoredProcedure;

					cmd.Parameters.Add("@ID", SqlDbType.VarChar);
					cmd.Parameters["@ID"].Value = drID;
					cmd.Parameters.Add("@modifier", SqlDbType.Int);
					cmd.Parameters["@modifier"].Value = Int32.Parse(modifier);
					cmd.Parameters.Add("@Status", SqlDbType.Int);
					cmd.Parameters["@Status"].Direction = ParameterDirection.Output;

					using (SqlDataAdapter myAdapt = new SqlDataAdapter(cmd))
					{
						con.Open();
						
						try
						{
							cmd.ExecuteNonQuery();
							status = cmd.Parameters["@Status"].Value.ToString();
						}
						catch (Exception ex)
						{
							// DB call failed
							status = "error: " + "SQL ERROR: " + ex.Message.ToString();
						}
					}
				}
			return status;
		}
	}
}
