﻿<%@ Page Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Learn more about Bioré® Skincare" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
    <style type="text/css">
        h4{
            padding:10px 0 10px 0px;
            font-weight:bold
        }
        #privacyinfo {
            padding-bottom:20px
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div class="centeringDiv">
                <div id="content">
                    <div class="titleh1">
                        <h1>Politica de privacidad</h1>
                        <div id="responseRule"></div>
                    </div>
                    <div id="privacyinfo">
                         En Kao, respetamos tu privacidad y tomamos medidas razonables para protegerla. Esta política se aplica únicamente al sitio web donde aparece y no se aplica a otros sitios web o servicios de Kao USA Inc. Al acceder a este sitio web o utilizarlo, estás de acuerdo y das tu consentimiento para que utilicemos tu información en la forma descrita en esta política.
                        <h4>FECHA DE VIGENCIA</h4>
                        Esta política de privacidad está vigente desde su fecha de actualización más reciente, la cual fue el 14 de marzo de 2012. 
                        <h4>QUÉ INFORMACIÓN SOBRE TI RECOPILAMOS</h4>
                        <h4>Información de identificación personal</h4>
                        Este sitio web está estructurado de manera que, en general, puedas visitarlo sin tener que revelar información de identificación personal. Una vez que hayas elegido proporcionarnos dicha información, puedes tener la tranquilidad de que solamente será utilizada de conformidad con los principios expuestos en este documento. <br />
                        <br />
                        En algunas páginas web de Kao USA Inc., puedes interactuar con formularios de ingreso de datos para comunicarte con nosotros, suscribirte a comunicaciones y otros eventos promocionales. Los tipos de información de identificación personal que se recopilan en estas páginas pueden incluir tu nombre, dirección postal y dirección de correo electrónico. También podremos pedirte que nos proporciones, de manera voluntaria, información sobre tus intereses personales o profesionales, datos demográficos, experiencias con nuestros productos y preferencias de contacto.<br />
                        <br />
                       No recopilamos información de identificación personal, a menos que tú nos la proporciones. Si nos envías un correo electrónico o respondes a preguntas presentadas en este sitio web, nos estarás revelando dicha información en forma voluntaria para que podamos utilizarla en la forma descrita en esta política. No nos envíes información de identificación personal si no quieres que tengamos esa información en nuestra base de datos.
                        <h4>Información de desempeño</h4>
                       Kao USA Inc. podrá registrar tu interacción con nuestros anuncios, sitios web, correos electrónicos y otras aplicaciones que proveemos, a través de Clickstream Data y cookies. Los “Clickstream Data” son un registro de lo que seleccionas al navegar por Internet. Estos datos pueden decirnos el tipo de computadora y software de navegación que usas, así como la dirección del sitio web desde el cual te enlazaste al sitio. Esta información podrá recopilarse y almacenarse en un servidor de sitio web, como el nuestro. 
                        Las “cookies” son pequeños archivos de texto que un sitio web coloca en tu computadora con el fin de facilitar y mejorar tu comunicación e interacción con el sitio web y para recopilar información agregada. Muchos sitios web, como el nuestro, utilizan cookies para estos fines. Podrás detener o limitar la colocación de cookies en tu computadora, o borrarlas de tu navegador, si ajustas las preferencias del navegador de Internet y los valores de ajuste de los complementos del navegador; en este caso, podrás seguir utilizando nuestro sitio web, pero esto también podría interferir con parte de su funcionalidad.
                        <h4>Lo que NO recopilamos</h4>
                        Kao USA Inc. no intenta recopilar información confidencial, como serían números de Seguro Social o de tarjetas de crédito. Asimismo, Kao USA no tiene la intención de recopilar información de identificación personal de niños menores de 13 años. Kao USA mantendrá procedimientos para asegurar que la información de identificación personal de niños menores de 13 años sea recopilada únicamente con el consentimiento expreso del padre o tutor del niño. Si un niño menor de 13 años ha provisto a Kao USA información de identificación personal sin dicho consentimiento, Kao USA solicitará al  padre o tutor del niño que se comunique con Kao USA a través de la página Contacto para informarnos de tal suceso. Kao USA hará un esfuerzo razonable para eliminar dicha información de nuestra base de datos.
                        <h4>CÓMO USAMOS TU INFORMACIÓN</h4>
                        Kao USA utiliza tu información para entender tus necesidades y proveer mejores productos y servicios. Kao USA podrá combinar tus datos personales y conductuales para personalizar tu experiencia en el sitio web, personalizar futuras comunicaciones y enviarte ofertas personalizadas. En ocasiones, Kao USA también podrá utilizar tu información para comunicarse contigo con fines de investigación de mercado o para proporcionar información de mercadotecnia que consideremos sería de tu particular interés. Siempre te daremos la oportunidad de excluirte de estas comunicaciones.<br />
                        <br />
                        Kao USA podrá compartir la información de identificación personal que proporciones en Internet con otras divisiones o empresas afiliadas de Kao USA. <br />
                        <br />
                        En ocasiones, podrá recopilarse información de identificación personal tuya en representación de Kao USA y de un tercero que se identificará en el momento de recopilar la información. En estos casos, tu información de identificación personal podrá proveerse tanto a Kao USA como al tercero. Aunque Kao USA usará tu información de identificación personal tal como se describe en esta política, el tercero la utilizará en la forma descrita en su propia política de privacidad. Por lo tanto, deberás revisar tales políticas antes de proporcionar tu información de identificación personal. Kao USA no será responsable por las acciones de dichos terceros.<br />
                        <br />
                        Kao USA podrá permitir a sus proveedores y subcontratistas tener acceso a tu información de identificación personal, pero únicamente en relación con los servicios que suministran a Kao USA. No tienen autorización de Kao USA para utilizar la información para su propio beneficio. Kao USA podrá divulgar información de identificación personal según lo requieran las leyes o los procesos legales.<br />
                        <br />
                        Kao USA podrá divulgar información de identificación personal para investigar sospechas de fraude, hostigamiento u otras infracciones de leyes, reglas o reglamentos o de los términos, condiciones o políticas del sitio web. <br />
                        <br />
                        En caso de una venta, fusión, liquidación, disolución o adquisición de Kao USA o de una unidad de negocios de Kao USA, la información recopilada por Kao USA sobre ti podrá venderse o transferirse de otra manera. Sin embargo, esto solamente sucederá si la parte que adquiere la información conviene en utilizar la información de identificación personal de una manera esencialmente similar a los usos descritos en esta política. 
                        <h4>CONTENIDO Y MENSAJES PERSONALIZADOS</h4>
                        Consideramos que el contenido, los mensajes y la publicidad son más relevantes y valiosos para ti si se basan en tus intereses, necesidades y características demográficas. Por lo tanto, podremos combinar tu información de identificación personal con información agregada que hemos recopilado para proporcionarte contenido, mensajes y publicidad específicos para ti, basados en tus actividades anteriores en Internet o en la información provista. Por ejemplo, si tus actividades en nuestro sitio web han mostrado tu interés por productos para el cuidado del cabello, podremos presentarse más anuncios sobre productos para el cuidado del cabello que sobre otros productos por los que no has demostrado interés o con los que no has interactuado en el sitio web. Aunque podremos utilizar estos perfiles para personalizar lo que te presentamos, seguimos manejando y protegiendo tu información de identificación personal tal como se especifica en esta política.
                        <h4>TU OPCIÓN</h4>
                        Kao USA no usará o compartirá la información que nos proporciones por Internet de maneras que no estén relacionadas con las descritas anteriormente, sin antes informártelo y darte una opción.
                        <h4>SEGURIDAD</h4>
                        Kao USA se compromete a tomar medidas razonables para garantizar la seguridad de la información que recopilamos. Para evitar el acceso no autorizado, mantener la exactitud de los datos y asegurar el uso apropiado de la información de identificación personal, hemos implementado procedimientos físicos, electrónicos y administrativos apropiados para proteger y asegurar la información de identificación personal que recopilamos por Internet. Si te proporcionamos un medio directo para mantener tu información en nuestra base de datos, como sería una aplicación de “centro de perfil”, tú serás responsable de tomar precauciones razonables para proteger tus datos de acceso, como serían tu identificación y contraseña. Kao USA no será responsable por violaciones a la seguridad que resulten de datos de acceso perdidos o robados. 
                        <h4>NO CONFIDENCIAL</h4>
                        Toda comunicación o material que nos transmitas por correo electrónico o de otra manera, por ejemplo, datos, preguntas, comentarios, sugerencias o similares, serán tratados como no confidenciales y no exclusivos. Excepto en la medida expresamente especificada en esta política, todo lo que transmitas o publiques podrá ser utilizado por nosotros para cualquier fin, como serían, entre otros, reproducción, divulgación, transmisión, publicación, difusión o exhibición. Convienes de manera expresa en que tendremos la libertad de utilizar todas las ideas, conceptos, conocimientos o técnicas contenidos en las comunicaciones que nos envíes, sin remuneración y para cualquier fin, incluidos, entre otros, desarrollo, manufactura y comercialización de productos y servicios utilizando dicha información. Asimismo, confirmas que todo archivo de medios que nos envíes (por ejemplo, fotografías, videos, etc.) son de tu propiedad y que tienes autorización de las personas que aparecen en dichos archivos para ceder los derechos a Kao USA. 
                        <h4>ACCESO Y CORRECCIÓN DE LA INFORMACIÓN; EXCLUSIÓN VOLUNTARIA</h4>
                        Kao USA mantendrá procedimientos razonables para que las personas tengan acceso a su información de identificación personal y preferencias. Kao USA corregirá toda información que sea incorrecta o incompleta, o permitirá que cambies tu nivel de consentimiento individual. Podrás hacerlo <a href="http://www.biore.com.mx/contact-us" target="">comunicándote con nosotros</a> con los detalles relevantes de tu solicitud. 
                        Si deseas que dejemos de usar tu información de identificación personal y que la eliminemos de nuestra lista de usuarios activos, <a href="http://www.biore.com.mx/contact-us" target="">comunícate con nosotros</a> con los detalles necesarios para procesar tu solicitud. Procesaremos y respetaremos dichas solicitudes en un plazo razonable después de recibirlas. Sin embargo, aunque podemos eliminar tu información de identificación personal de nuestra lista de usuarios activos, no seremos responsables de eliminar tu información de identificación personal de las listas de terceros a los cuales hayamos  proporcionado tu información de conformidad con lo dispuesto en esta política, como sería el caso de socios comerciales. 
                        <h4>COMENTARIOS Y PREGUNTAS</h4>
                        Si tienes comentarios o preguntas sobre nuestra política de privacidad o tu información de identificación personal, usa nuestra página <a href="http://www.biore.com.mx/contact-us" target="">comunicáte con nosotros.</a> 
                        <h4>ESTADOS UNIDOS DE AMÉRICA</h4>
                        Este sitio web y nuestras bases de datos relacionadas se mantienen en los Estados Unidos de América. Al utilizar el sitio web, das tu consentimiento de manera libere y específica para que recopilemos y almacenemos tu información en los Estados Unidos y que utilicemos tu información de la manera especificada en esta política. 
                        <h4>CAMBIOS A LA POLÍTICA DE PRIVACIDAD</h4>
                        Kao USA se reserva el derecho de modificar y actualizar esta política o sus prácticas comerciales relacionadas en cualquier momento, a nuestra discreción. Todos los cambios relevantes serán publicados aquí como referencia. Sin embargo, Kao USA no hará cambios relevantes a esta política, como sería la manera en que divulga la información de identificación personal, sin antes darte la oportunidad de excluirte voluntariamente de dichos cambios en la forma de uso. 
                       
                 </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
