﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Biore2012.BLL;
using System.Text;
using System.Web.UI.HtmlControls;

//add id to sidebar for GA event tracking  -MCT

namespace Biore2012.our_products
{
    public partial class ProductDetail : RoutablePage
    {

        public string floodlightIframeSource;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            // Grab masterpage, set appropriate body classes
            Site myMaster = (Site)this.Master;
            myMaster.bodyClass += " ourProducts productDetail";
            
            // turn off floodlight pixels for the majority of the detail pages
            myMaster.FindControl("floodlightPixels").Visible = false;

            // configure prove it link for mobile
            //BioreUtils bu = new BioreUtils();
            //bu.configureProveItLinks(myMaster.isMobile, proveItPromoLink);

            // Fetch product based on route used to reach this page.
            Product theProduct = new Product();
            String pid = "";
            if (Routing.Value("pid") != null)
            {
                pid = Routing.Value("pid").ToString();
                // Make sure we aren't showing a pore strip page.
                if (pid == "deep-cleansing-pore-strips-combo" ||
                        pid == "deep-cleansing-pore-strips-ultra" ||
                        pid == "deep-cleansing-pore-strips")
                    Response.Redirect(VirtualPathUtility.ToAbsolute("~/deep-cleansing-product-family/pore-strips"));
                else
                    theProduct = ProductDAO.GetProduct(pid);
            }
            else
            {
                if (Request.Url.ToString().Contains("deep-cleansing-product-family/pore-strips"))
                {
                    // turn on floodlight pixels for pore strips, and set iframe source
                    myMaster.FindControl("floodlightPixels").Visible = true;
                    floodlightIframeSource = "http://fls.doubleclick.net/activityi;src=1418070;type=biore797;cat=biore776;ord=";
                    
                    // We're on the porestrips family page.
                    thePoreStripNavPanel.Visible = true;
                    theProduct = ProductDAO.GetProduct("deep-cleansing-pore-strips");
                    PoreStripProduct theRegularProduct = PoreStripProductDAO.GetProduct("deep-cleansing-pore-strips");
                    PoreStripProduct theUltraProduct = PoreStripProductDAO.GetProduct("deep-cleansing-pore-strips-ultra");
                    PoreStripProduct theComboProduct = PoreStripProductDAO.GetProduct("deep-cleansing-pore-strips-combo");

                    // Write the JSON for all three to the page.
                    thePoreStripsJSON.Text = "var productRegular ="
                                            + JSONHelper.Serialize<PoreStripProduct>(theRegularProduct)
                                            + ";\n var productUltra ="
                                            + JSONHelper.Serialize<PoreStripProduct>(theUltraProduct)
                                            + ";\n var productCombo ="
                                            + JSONHelper.Serialize<PoreStripProduct>(theComboProduct)
                                            + ";";
                }
                else
                    // If a user accidentally lands on the product detail page without a route.
                    theProduct = ProductDAO.GetProduct("combination-skin-balancing-cleanser");
            }
            
            // turn on floodlight pixels for skin balancing cleanser, and set iframe source
            if (theProduct.ProductRefName == "combination-skin-balancing-cleanser") {
                myMaster.FindControl("floodlightPixels").Visible = true;
                floodlightIframeSource = "http://fls.doubleclick.net/activityi;src=1418070;type=biore797;cat=biore972;ord=";
            }

            // turn on floodlight pixels for skin balancing cleanser, and set iframe source
            if (theProduct.ProductRefName == "blemish-fighting-ice-cleanser")
            {
                myMaster.FindControl("floodlightPixels").Visible = true;
                floodlightIframeSource = "http://fls.doubleclick.net/activityi;src=1418070;type=biore797;cat=blemi198;ord=";
            }

            // Add meta tagging.
            myMaster.Page.Title = theProduct.MetaTitle;

            HtmlGenericControl theMetaDescription = (HtmlGenericControl)Master.FindControl("head").FindControl("metaDescription");
            theMetaDescription.Attributes.Add("content", theProduct.MetaDescription);

            HtmlGenericControl theMetaKeywords = (HtmlGenericControl)Master.FindControl("head").FindControl("metaKeywords");
            theMetaKeywords.Attributes.Add("content", theProduct.MetaKeywords);

            HtmlMeta theMetaFBImage = (HtmlMeta)Master.FindControl("fbImage");
            theMetaFBImage.Attributes.Add("content", "http://" + Request.Url.Host + VirtualPathUtility.ToAbsolute(theProduct.SocialImage));


            // Populate category, write body classes to style according to category
            String productCategory = theProduct.Category;
            theCategory.Text = productCategory;
            if (productCategory.ToLower() == "deep cleansing")
            {
                myMaster.bodyClass += " deepCleansing";
                theCategoryClass.Text = "deepCleansing";
            }
            else if (productCategory.ToLower() == "complexion clearing")
            {
                myMaster.bodyClass += " complexionClearing";
                theCategoryClass.Text = "complexionClearing";
            }
            else if (productCategory.ToLower() == "make-up removing")
            {
                myMaster.bodyClass += " murt";
                theCategoryClass.Text = "murt";
            }


            // New and old image logic, including new product case

            if (myMaster.isMobile) { theNewImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.NewLookImage).Replace("large", "mobile"); }
            else { theNewImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.NewLookImage); }

            if (theProduct.OldLookImage == "") oldNewControlPanel.Visible = false;
            else
            {
                if (myMaster.isMobile)
                {
                    theOldImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.OldLookImage).Replace("large", "mobile");
                }
                else
                {
                    theOldImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.OldLookImage);
                }
            }
            if (theProduct.BuyNowURL == "") { theBuyNowLink.Visible = false; }


            // Plug in data from the product
            theName.Text = theProduct.Name;
            thePromoCopy.Text = theProduct.PromoCopy;
            theDescription.Text = theProduct.Description;
            theBuyNowLink.NavigateUrl = theProduct.BuyNowURL;
            theWhatItIs.Text = theProduct.WhatItIs;
            theIngredients.Text = theProduct.ProductDetails;
            theHowItWorks.Text = theProduct.HowItWorks;
            theCautions.Text = theProduct.Cautions;
            theSidebarHeader.Text = theProduct.SideBarHeader;
            theSidebarCopy.Text = theProduct.SideBarCopy;

            // Is there a fun facts area?
            if (theProduct.FunFacts == "") theFunFactsPanel.Visible = false;
            else
            {
                theFunFacts.Text = theProduct.FunFacts;
            }


            // Is there a video?
            if (theProduct.Video == "") theVideoPanel.Visible = false;
            else
            {
                theVideoLink.NavigateUrl = VirtualPathUtility.ToAbsolute(theProduct.Video);
                string theVideoParams = string.Empty;
                if (theProduct.VideoDuration != "") theVideoParams = "videoParams::448|252|" + theProduct.VideoDuration;
                else theVideoParams = "videoParams::448|252|160";
                theVideoLink.Attributes.Add("rel", theVideoParams);
                theVideoStillImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.VideoStillImage);
            }


            // Associated Products
            List<SideBarProduct> SideBarProductsList = theProduct.SideBarProductCollection;
            theSideBarProducts.DataSource = SideBarProductsList;
            theSideBarProducts.DataBind();


            // Draw Like button if not on a mobile device
            if (myMaster.isMobile == false)
            {
                buildLikeBtn(theProduct.LikeURL);
                buildPlusBtn(theProduct.LikeURL);
                myMaster.FindControl("plusScript").Visible = true;

            }
        }

        protected void sidebar_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;
            ListView lv = (ListView)sender;

            List<SideBarProduct> myData = (List<SideBarProduct>)lv.DataSource;

            SideBarProduct theSideBarProduct = myData[dataItem.DataItemIndex];

            HyperLink theSideBarProductLink = (HyperLink)e.Item.FindControl("theSideBarProductLink");
            theSideBarProductLink.NavigateUrl = VirtualPathUtility.ToAbsolute(theSideBarProduct.ProductURL);

            Image theSideBarProductImage = (Image)e.Item.FindControl("theSideBarProductImage");
            theSideBarProductImage.ImageUrl = VirtualPathUtility.ToAbsolute(theSideBarProduct.Image);

            Literal theSideBarProductName = (Literal)e.Item.FindControl("theSideBarProductName");
            theSideBarProductName.Text = theSideBarProduct.ProductName;
            theSideBarProductLink.ID = theSideBarProduct.ProductName.Replace(' ', '-'); //used for event tracking

            HtmlControl theSideBarListItem = (HtmlControl)e.Item.FindControl("theSideBarListItem");
            string className = theSideBarProduct.ClassName;
            if (className != "") { theSideBarListItem.Attributes.Add("class", className); }

        }
        private void buildLikeBtn(string likeURL)
        {
            // Build FB like code.
            StringBuilder likeBuilder = new StringBuilder();
            likeBuilder.Append("<div class=\"fb-like\" data-href=\"");
            likeBuilder.Append("http://" + Request.Url.Host + VirtualPathUtility.ToAbsolute(likeURL));
            likeBuilder.Append("\" data-send=\"false\" data-layout=\"button_count\" data-width=\"100\" data-show-faces=\"false\"></div>");
            fbLike.Text = likeBuilder.ToString();
        }

        private void buildPlusBtn(string likeURL)
        {
            // Build Google plus code.
            StringBuilder plusBuilder = new StringBuilder();
            plusBuilder.Append("<g:plusone size=\"medium\" annotation=\"none\" href=\"");
            plusBuilder.Append("http://" + Request.Url.Host + VirtualPathUtility.ToAbsolute(likeURL));
            plusBuilder.Append("\"></g:plusone>");
            plusButton.Text = plusBuilder.ToString();
        }
    }
}


