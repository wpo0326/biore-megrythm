﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Biore2012.where_to_buy
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //The map splash is no longer used, so hide it and show the store finder by default.
            mapHolder.Visible = false;
            storeFinder.Visible = true;

            Site myMaster = (Site)this.Master;
            myMaster.bodyClass += " whereToBuy";

            string strCountry = Request.QueryString["c"] == null ? "" : Request.QueryString["c"].ToString();

             GetDefaultRetailer(strCountry);

        }

        protected void GetDefaultRetailer(string strCountry)
        {
            mapHolder.Visible = false;
            storeFinder.Visible = true;
            mapInstructions.InnerHtml = "Selecciona un país de la lista para encontrar una tienda cerca de ti.";
            //show selected country's list of retailers, hide the rest.
            //make sure to replace spaces in names with an underscore for the ID reference.
            //activate country list item and expand appropriate continent region by assigning active classes.
            switch (strCountry)
            {
                //Mexico
                case "Mexico":
                    AddGenericClass(MexicoHolder, "open");
                    Mexico.Visible = true;
                    break;

                //Central America
                case "Costa Rica":
                    AddGenericClass(CentroAmericaHolder, "open");
                    AddAnchorClass(costa_rica_anchor, "selected");
                    Costa_Rica.Visible = true;
                    break;
                 case "Dominican Republic":
                    AddGenericClass(CentroAmericaHolder, "open");
                    AddAnchorClass(dominican_republic_anchor, "selected");
                    Dominican_Republic.Visible = true;
                    break;
                case "El Salvador":
                    AddGenericClass(CentroAmericaHolder, "open");
                    AddAnchorClass(el_salvador_anchor, "selected");
                    El_Salvador.Visible = true;
                    break;
                case "Guatemala":
                    AddGenericClass(CentroAmericaHolder, "open");
                    AddAnchorClass(guatemala_anchor, "selected");
                    Guatemala.Visible = true;
                    break;
                case "Honduras":
                    AddGenericClass(CentroAmericaHolder, "open");
                    AddAnchorClass(honduras_anchor, "selected");
                    Honduras.Visible = true;
                    break;
                case "Nicaragua":
                    AddGenericClass(CentroAmericaHolder, "open");
                    AddAnchorClass(nicaragua_anchor, "selected");
                    Nicaragua.Visible = true;
                    break;
                case "Panama":
                    AddGenericClass(CentroAmericaHolder, "open");
                    AddAnchorClass(panama_anchor, "selected");
                    Panama.Visible = true;
                    break;
                
                //South America
                case "Chile":
                    AddGenericClass(SudaAmericaHolder, "open");
                    AddAnchorClass(chile_anchor, "selected");
                    Chile.Visible = true;
                    break;
                case "Colombia":
                    AddGenericClass(SudaAmericaHolder, "open");
                    AddAnchorClass(colombia_anchor, "selected");
                    Colombia.Visible = true;
                    break;
                case "Peru":
                    AddGenericClass(SudaAmericaHolder, "open");
                    AddAnchorClass(peru_anchor, "selected");
                    Peru.Visible = true;
                    break;
                case "Uruguay":
                    AddGenericClass(SudaAmericaHolder, "open");
                    AddAnchorClass(uruguay_anchor, "selected");
                    Uruguay.Visible = true;
                    break;
                case "Venezuela":
                    AddGenericClass(SudaAmericaHolder, "open");
                    AddAnchorClass(venezuela_anchor, "selected");
                    Venezuela.Visible = true;
                    break;
                case "Brazil":
                    AddGenericClass(SudaAmericaHolder, "open");
                    AddAnchorClass(brazil_anchor, "selected");
                    Brazil.Visible = true;
                    break;
                case "Ecuador":
                    AddGenericClass(SudaAmericaHolder, "open");
                    AddAnchorClass(ecuador_anchor, "selected");
                    Ecuador.Visible = true;
                    break;
                case "Paraguay":
                    AddGenericClass(SudaAmericaHolder, "open");
                    AddAnchorClass(paraguay_anchor, "selected");
                    Paraguay.Visible = true;
                    break;

                default:
                    AddGenericClass(MexicoHolder, "open");
                    Mexico.Visible = true;
                    break;

            }

        }

        protected void AddGenericClass(System.Web.UI.HtmlControls.HtmlGenericControl e, string classname)
        {
            // add css class
            e.Attributes["class"] = String.Join(" ", e
                       .Attributes["class"]
                       .Split(' ')
                       .Except(new string[] { "", classname })
                       .Concat(new string[] { classname })
                       .ToArray()
               );
        }

        protected void RemoveGenericClass(System.Web.UI.HtmlControls.HtmlGenericControl e, string classname)
        {
            //remove css class
            e.Attributes["class"] = String.Join(" ", e
                      .Attributes["class"]
                      .Split(' ')
                      .Except(new string[] { "", classname })
                      .ToArray()
              );
        }

        protected void AddAnchorClass(System.Web.UI.HtmlControls.HtmlAnchor e, string classname)
        {
            // add css class
            e.Attributes["class"] = String.Join(" ", e
                       .Attributes["class"]
                       .Split(' ')
                       .Except(new string[] { "", classname })
                       .Concat(new string[] { classname })
                       .ToArray()
               );
        }

        protected void RemoveAnchorClass(System.Web.UI.HtmlControls.HtmlAnchor e, string classname)
        {
            //remove css class
            e.Attributes["class"] = String.Join(" ", e
                      .Attributes["class"]
                      .Split(' ')
                      .Except(new string[] { "", classname })
                      .ToArray()
              );
        }
    }
}
