﻿<%@ Page Title="Where To Buy | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.where_to_buy._default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Quickly find where to buy your favorite Bioré® products" name="description" />
    <meta content="where to buy Bioré products, find Bioré retailers" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/whereToBuy.css")
        .Add("~/css/productDetail.css")
        .Render("~/css/combinedbuy_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <h1>Dónde Comprar</h1>
                <h2 id="mapInstructions" runat="server">Selecciona un país en el mapa para encontrar una tienda cerca de ti</h2>
                <asp:Panel ID="mapHolder" runat="server">
                    <div id="wtbMap">
                        <div id="wtbMapDropDown">
                            <div id="ourProductsBg" class="pie"></div>
                            <div id="wtbMapDropDownWrapper">
                                <ul>
                                    <li><a href="/where-to-buy-biore/Mexico/">México</a></li>
                                    <li><a href="/where-to-buy-biore/Chile/">Chile</a></li>
                                    <li><a href="/where-to-buy-biore/Costa Rica/">Costa Rica</a></li>
                                    <li><a href="/where-to-buy-biore/El Salvador/">El Salvador</a></li>
                                    <li><a href="/where-to-buy-biore/Guatemala/">Guatemala</a></li>
                                    <li><a href="/where-to-buy-biore/Honduras/">Honduras</a></li>
                                    <li><a href="/where-to-buy-biore/Nicaragua/">Nicaragua</a></li>
                                    <li><a href="/where-to-buy-biore/Colombia/">Colombia</a></li>
                                   <!-- <li><a href="/where-to-buy-biore/Peru/">Perú</a></li>-->
                                    <li><a href="/where-to-buy-biore/Panama/">Panama</a></li>
                                    <!--<li><a href="/where-to-buy-biore/Uruguay/">Uruguay</a></li>-->
                                    <li><a href="/where-to-buy-biore/Venezuela/">Venezuela</a></li>
                                    <!--<li><a href="/where-to-buy-biore/Brazil/">Brazil</a></li>-->
                                    <li><a href="/where-to-buy-biore/Ecuador/">Ecuador</a></li>
                                    <!--<li><a href="/where-to-buy-biore/Paraguay/">Paraguay</a></li>-->
                                    <li><a href="/where-to-buy-biore/Dominican Republic/">República Dominicana</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="storeFinder" runat="server">
                    <div id="countryNav">
                        <div id="productInfo" class="col floatRight">
                            <div id="MexicoHolder" class="contentHolder" runat="server">
                                <h3 class="pie"><a href="/where-to-buy-biore/Mexico/">México</a>
                                    <!--<span class="pie circle"></span>-->
                                </h3>
                                <div class="collapsibleContent hide">
                                </div>
                            </div>
                            <div id="CentroAmericaHolder" class="contentHolder" runat="server">
                                <h3 class="pie">Centroamérica / el Caribe
                                    <span class="pie circle"></span>
                                </h3>
                                <div class="collapsibleContent hide">
                                    <ul>
                                        <li><a id="costa_rica_anchor" href="/where-to-buy-biore/Costa Rica/" runat="server" class="">Costa Rica</a></li>
                                        <li><a id="dominican_republic_anchor" href="/where-to-buy-biore/Dominican Republic/" runat="server" class="">República Dominicana</a></li>
                                        <li><a id="el_salvador_anchor" href="/where-to-buy-biore/El Salvador/" runat="server" class="">El Salvador</a></li>
                                        <li><a id="guatemala_anchor" href="/where-to-buy-biore/Guatemala/" runat="server" class="">Guatemala</a></li>
                                        <li><a id="honduras_anchor" href="/where-to-buy-biore/Honduras/" runat="server" class="">Honduras</a></li>
                                        <li><a id="nicaragua_anchor" href="/where-to-buy-biore/Nicaragua/" runat="server" class="">Nicaragua</a></li>
                                        <li><a id="panama_anchor" href="/where-to-buy-biore/Panama/" runat="server" class="">Panama</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div id="SudaAmericaHolder" class="contentHolder" runat="server">
                                <h3 class="pie">Sudamérica
                                    <span class="pie circle"></span>
                                </h3>
                                <div class="collapsibleContent hide">
                                    <ul>
                                        <!--<li><a id="brazil_anchor" href="/where-to-buy-biore/Brazil/" runat="server" class="">Brazil</a></li>-->
                                        <li><a id="chile_anchor" href="/where-to-buy-biore/Chile/" runat="server" class="">Chile</a></li>
                                        <li><a id="colombia_anchor" href="/where-to-buy-biore/Colombia/" runat="server" class="">Colombia</a></li>
                                        <li><a id="ecuador_anchor" href="/where-to-buy-biore/Ecuador/" runat="server" class="">Ecuador</a></li>
                                        <!--<li><a id="paraguay_anchor" href="/where-to-buy-biore/Paraguay/" runat="server" class="">Paraguay</a></li>-->
                                        <!--<li><a id="peru_anchor" href="/where-to-buy-biore/Peru/" runat="server" class="">Perú</a></li>-->
                                        <!--<li><a id="uruguay_anchor" href="/where-to-buy-biore/Uruguay/" runat="server" class="">Uruguay</a></li>-->
                                        <li><a id="venezuela_anchor" href="/where-to-buy-biore/Venezuela/" runat="server" class="">Venezuela</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div id="logoHolder">
                        <div id="countries">
                            <div id="Mexico" runat="server" visible="false">
                                <ul>
                                    <li>Bodega Aurrer&aacute;</li>
                                    <!-- <li>Bodega Comercial Mexicana </li>-->
                                    <!-- <li>Benavides  FASA</li> -->
                                    <li>Casa Ley</li>
                                    <li>Chedraui</li>
                                    <li>Comercial Mexicana</li>
                                    <li>DAX</li>
                                    <li>Farmacias Benavides</li>
                                    <li>Farmacias Del Ahorro</li>
                                    <li>Farmacias Guadalajara</li>
                                    <li>Futurama</li>
                                    <li>Grupo Rivera</li>
                                    <li>HEB</li>
                                    <li>Mercado Soriana</li>
                                    <!-- <li>Mi Bodega Aurrera </li>-->
                                    <!-- <li>Mega </li> -->
                                    <!--<li>Sanborns</li> -->
                                    <li>Soriana</li>
                                    <li>Superama</li>
                                    <li>Walmart Super Center</li>
                                </ul>
                            </div>

                            <div id="Chile" runat="server" visible="false">
                                <ul>
                                    <li>Cruz Verde</li>
                                    <li>Farmacias Ahumada</li>
                                    <li>Jumbo</li>
                                    <!-- <li>Líder Hyper</li> -->
                                    <li>Maicao</li>
                                    <li>Montserrat</li>
                                    <li>Salco Brand</li>
                                    <li>Tottus</li>
                                    <!-- <li>Unimarc</li> -->
                                </ul>
                            </div>

                            <div id="Costa_Rica" runat="server" visible="false">
                                <ul>
                                    <li>Almacenes Siman</li>
                                    <li>AutoMercado</li>
                                    <li>Mas X Menos</li>
                                    <li>Walmart Supercenter</li>
                                </ul>
                            </div>

                            <div id="El_Salvador" runat="server" visible="false">
                                <ul>
                                    <li>Almacenes Siman</li>
                                    <li>La Despensa de Don Juan</li>
                                    <li>Prisma Moda</li>
                                    <li>Sanborns</li>
                                    <li>SEARS</li>
                                    <li>Super Selectos</li>
                                    <li>Walmart Supercenter</li>
                                </ul>
                            </div>

                            <div id="Guatemala" runat="server" visible="false">
                                <ul>
                                    <li>Almacenes Siman</li>
                                    <li>Belliance</li>
                                    <li>La Torre</li>
                                    <!-- <li>Maxi Despensa </li> -->
                                    <li>Meykos</li>
                                    <li>Paiz</li>
                                    <li>Walmart Supercenter</li>
                                </ul>
                            </div>

                            <div id="Honduras" runat="server" visible="false">
                                <ul>
                                    <li>Maxi Despensa</li>
                                    <li>Paiz</li>
                                    <li>Walmart Supercenter</li>
                                </ul>
                            </div>

                            <div id="Nicaragua" runat="server" visible="false">
                                <ul>
                                    <li>Almacenes Siman </li>
                                    <li>La Union </li>
                                </ul>
                            </div>

                            <div id="Colombia" runat="server" visible="false">
                                <ul>
                                    <li>Almacenes 14 </li>
                                    <li>Krika</li>
                                    <li>Fedco</li>
                                    <li>Locatel</li>
                                    <li>Laskin</li>

                                </ul>
                            </div>

                            <div id="Peru" runat="server" visible="false">
                                <ul>
                                    <li>Bajo Construcción </li>
                                </ul>
                            </div>

                            <div id="Panama" runat="server" visible="false">
                                <ul>
                                    <li>	Machetazo   	</li>
                                    <li>	Rey                 	</li>
                                    <li>	Super 99   	</li>
                                    <li>	Super Xtra	</li>
                                    <li>	Riba Smith       	</li>
                                    <li>	Kosher 	</li>
                                    <li>	Super Carnes 	</li>
                                    <li>	Arrocha           	</li>
                                    <li>	Metro               	</li>
                                    <li>	Pharma           	</li>
                                    <li>	Javillo                        	</li>
                                    <li>	Gonzalez Revilla	</li>
                                    <li>	El Costo              	</li>
                                    <li>	El Titan                	</li>
                                    <li>	PriceSmart 	</li>
                                    <li>	Super Baru	</li>


                                </ul>
                            </div>

                            <div id="Uruguay" runat="server" visible="false">
                                <ul>
                                    <li>Bajo Construcción </li>
                                </ul>
                            </div>

                            <div id="Venezuela" runat="server" visible="false">
                                <ul>
                                     <li>	Plan Suárez	</li>
                                    <li>	Rattan	</li>
                                    <li>	Sigo	</li>
                                    <li>	Unicasa	</li>
                                    <li>	Premium	</li>
                                    <li>	Batata	</li>
                                    <li>	Central Madeirense	</li>
                                    <li>	Exelsior Gama	</li>
                                    <li>	Víveres Cándido	</li>
                                    <li>	Euromarket	</li>
                                    <li>	Plaza's	</li>
                                    <li>	Farmatodo	</li>
                                    <li>	Locatel	</li>
                                    <li>	Farmahorro	</li>
                                    <li>	Saas	</li>
                                    <li>	Provemed	</li>
                                    <li>	Lider	</li>
                                    <li>	Casa Tokio	</li>
                                    <li>	Americas	</li>
                                    <li>	Obssesion	</li>
                                    <li>	Farmatencion	</li>
                                    <li>	Farmared	</li>
                                    <li>	Diademas	</li>
                                    <li>	Todofertas	</li>
                                    <li>	Super Enne	</li>
                                    <li>	Centro 99	</li>
                                    <li>	Don Lolo	</li>
                                    <li>	Sarela	</li>
                                    <li>	Dulcinea	</li>
                                    <li>	Casa Hernandez	</li>
                                    <li>	Fung y Hung	</li>

                                </ul>
                            </div>

                            <div id="Brazil" runat="server" visible="false">
                                <ul>
                                    <li>Bajo Construcción </li>
                                </ul>
                            </div>

                            <div id="Ecuador" runat="server" visible="false">
                                <ul>
                                    <li>	Super Maxi	</li>
                                    <li>	Coral Rio	</li>
                                    <li>	Fybeca	</li>
                                    <li>	BurbujasXpres	</li>

                                </ul>
                            </div>

                            <div id="Paraguay" runat="server" visible="false">
                                <ul>
                                    <li>Bajo Construcción </li>
                                </ul>
                            </div>

                            <div id="Dominican_Republic" runat="server" visible="false">
                                <ul>
                                    <li>	Grupo Ramos	</li>
                                    <li>	Centro Cuesta Nacional	</li>
                                    <li>	Farmax	</li>
                                    <li>	Carol	</li>
                                    <li>	Independent Stores	</li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

<%=SquishIt.Framework.Bundle .JavaScript()
    .Add("~/js/swfobject.min.js")
    .Add("~/js/jquery.ba-hashchange.min.js")
    .Add("~/js/jquery.touchwipe.min.js")
    .Add("~/js/productDetail.js")
    .Render("~/js/combineddetail_#.js")
%>

</asp:Content> 

