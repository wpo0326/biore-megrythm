﻿<%@ Page Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="NewsletterForm.aspx.cs"
    Inherits="Biore2012.FormConfig.NewsletterForm" %>
<%@ Register TagPrefix="skm" Namespace="skmValidators" Assembly="skmValidators" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%#SquishIt.Framework.Bundle .Css()
        .Add("~/css/formConfig.css")
        .Render("~/css/combinedform_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow">
            </div>
            <div class="centeringDiv">
                <div class="responsive contact">
                    <div id="contactFormWrap">
                        <div id="BrandImageContainer" class="png-fix">
                            <img class="png-fix" src="images/forms/signUpPhoto.jpg" style="border-width: 0px;" />
                        </div>
                        <div id="ContactFormContainer">
                            <asp:Panel ID="ContactDetails" runat="server">
                                <!-- Header -->
                                <div id="formHeader">
                                    <h1 id="ctl00_ContentPlaceHolder1_ucForm_PageHeader">
                                        ¡Únete al movimiento por una piel maravillosa todos los días!
                                    </h1>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="OptinForm" runat="server">
                                <!-- Description -->
                                <div class="DescriptionContainer png-fix">
                                    <h2>
                                        Gracias por interesarte en Bioré® Skincare</h2>
                                    Por favor ingresa la siguiente información para conocer nuestras últimas promociones, concursos y noticias.
                                </div>
                                <!-- From Fields -->
                                <div class="FormContainer">
                                    <p class="req png-fix">
                                        <em>Obligatorio*</em></p>
                                    <div class="MemberInfoContainer png-fix">
                                        <div class="NameWrapper">
                                            <div class="FirstNameContainer Question">
                                                <asp:Label ID="FNameLbl" runat="server" Text="Nombre*" AssociatedControlID="FName"></asp:Label>
                                                <asp:TextBox ID="FName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="FNameValidator1" runat="server" ErrorMessage="Por favor, introduzca su nombre de pila"
                                                        ControlToValidate="FName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="FNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your First Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="FName" EnableClientScript="true"
                                                        SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="LastNameContainer Question">
                                                <asp:Label ID="LNameLbl" runat="server" Text="Apellido*" AssociatedControlID="LName"></asp:Label>
                                                <asp:TextBox ID="LName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="LNameValidator1" runat="server" ErrorMessage="Por favor, introduzca su apellido."
                                                        ControlToValidate="LName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="LNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Last Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="LName" SetFocusOnError="true"
                                                        Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="EmailWrapper">
                                            <div class="EmailContainer Question">
                                                <asp:Label ID="EmailLbl" runat="server" Text="Correo electrónico*" AssociatedControlID="Email"></asp:Label>
                                                <asp:TextBox ID="Email" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="EmailValidator1" runat="server" ErrorMessage="Por favor, introduzca su correo electrónico"
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="EmailValidator2" runat="server" ErrorMessage="Please enter a valid Email Address."
                                                        ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div class="PhoneContainer Question">
                                            <asp:Label ID="MobilePhoneLbl" runat="server" Text="Teléfono móvil" AssociatedControlID="MobilePhone"></asp:Label>
                                            <asp:TextBox ID="MobilePhone" MaxLength="50" runat="server" />
                                            <div class="ErrorContainer">
                                                <asp:RegularExpressionValidator ID="v_MobilePhone" runat="server" ErrorMessage="Your Mobile Phone is not required, but please do not use special characters in the Mobile Phone field."
                                                    ValidationExpression="^[^<>]+$" ControlToValidate="MobilePhone" EnableClientScript="true"
                                                    SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                <%-- The following RegEx Validators is for US type numbers.  IMPORTANT: 
								 Please test against your user base before using as you may need to modify it for other formats!  --%>
                                                <%--<asp:RegularExpressionValidator ID="PhoneValidator2" runat="server"
								ErrorMessage="<br/>Your Home Phone is not required, but please enter only valid characters."
								ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"
								ControlToValidate="Phone" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg phone"></asp:RegularExpressionValidator>--%>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelAddress" class="AddressContainer">
                                            <div class="AddressWrapper">
                                                <div class="Address1Container Question">
                                                    <asp:Label ID="Address1Lbl" runat="server" Text="Dirección 1*" AssociatedControlID="Address1"></asp:Label>
                                                    <asp:TextBox ID="Address1" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="Address1Validator1" runat="server" ErrorMessage="Por favor, introduzca su dirección."
                                                            ControlToValidate="Address1" EnableClientScript="true" SetFocusOnError="true"
                                                            Display="Dynamic" CssClass="errormsg" />
                                                        <asp:RegularExpressionValidator ID="Address1Validator2" runat="server" ErrorMessage="Please do not use special characters in the Address 1 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address1" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="Address2Container Question">
                                                    <asp:Label ID="Address2Lbl" runat="server" Text="Dirección 2" AssociatedControlID="Address2"></asp:Label>
                                                    <asp:TextBox ID="Address2" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="Address2Validator1" runat="server" ErrorMessage="Please do not use special characters in the Address 2 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address2" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="CityContainer Question">
                                                    <asp:Label ID="CityLbl" runat="server" Text="Ciudad*" AssociatedControlID="City"></asp:Label>
                                                    <asp:TextBox ID="City" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="CityValidator1" runat="server" ErrorMessage="Por favor, introduzca su ciudad."
                                                            ControlToValidate="City" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                            CssClass="errormsg" />
                                                        <asp:RegularExpressionValidator ID="CityValidator2" runat="server" ErrorMessage="Please do not use special characters in the City field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="City" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="StateContainer Question">
                                                <asp:Label ID="StateLbl" runat="server" Text="Estado*" AssociatedControlID="State"></asp:Label>
                                                <asp:DropDownList ID="State" runat="server">
                                                    <asp:ListItem Value="">Selecciona</asp:ListItem>
                                                    <asp:ListItem Value="Aguascalientes">Aguascalientes</asp:ListItem>
                                                    <asp:ListItem Value="Baja California">Baja California</asp:ListItem>
                                                    <asp:ListItem Value="Baja California Sur">Baja California Sur</asp:ListItem>
                                                    <asp:ListItem Value="Campeche">Campeche</asp:ListItem>
                                                    <asp:ListItem Value="Chiapas">Chiapas</asp:ListItem>
                                                    <asp:ListItem Value="Chihuahua">Chihuahua</asp:ListItem>
                                                    <asp:ListItem Value="Coahuila">Coahuila</asp:ListItem>
                                                    <asp:ListItem Value="Colima">Colima</asp:ListItem>
                                                    <asp:ListItem Value="Durango">Durango</asp:ListItem>
                                                    <asp:ListItem Value="Guanajuato">Guanajuato</asp:ListItem>
                                                    <asp:ListItem Value="Guerrero">Guerrero</asp:ListItem>
                                                    <asp:ListItem Value="Hidalgo">Hidalgo</asp:ListItem>
                                                    <asp:ListItem Value="Jalisco">Jalisco</asp:ListItem>
                                                    <asp:ListItem Value="Mexico State">Mexico State</asp:ListItem>
                                                    <asp:ListItem Value="Michoacán">Michoacán</asp:ListItem>
                                                    <asp:ListItem Value="Morelos">Morelos</asp:ListItem>
                                                    <asp:ListItem Value="Nayarit">Nayarit</asp:ListItem>
                                                    <asp:ListItem Value="Nuevo León">Nuevo León</asp:ListItem>
                                                    <asp:ListItem Value="Oaxaca">Oaxaca</asp:ListItem>
                                                    <asp:ListItem Value="Puebla">Puebla</asp:ListItem>
                                                    <asp:ListItem Value="Querétaro">Querétaro</asp:ListItem>
                                                    <asp:ListItem Value="Quintana Roo">Quintana Roo</asp:ListItem>
                                                    <asp:ListItem Value="San Luis Potosí">San Luis Potosí</asp:ListItem>
                                                    <asp:ListItem Value="Sinaloa">Sinaloa</asp:ListItem>
                                                    <asp:ListItem Value="Sonora">Sonora</asp:ListItem>
                                                    <asp:ListItem Value="Tabasco">Tabasco</asp:ListItem>
                                                    <asp:ListItem Value="Tamaulipas">Tamaulipas</asp:ListItem>
                                                    <asp:ListItem Value="Tlaxcala">Tlaxcala</asp:ListItem>
                                                    <asp:ListItem Value="Veracruz">Veracruz</asp:ListItem>
                                                    <asp:ListItem Value="Yucatán">Yucatán</asp:ListItem>
                                                    <asp:ListItem Value="Zacatecas">Zacatecas</asp:ListItem>
                                                    <asp:ListItem Value="Otro">Otro / Ninguno</asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="StateValidator" runat="server" ErrorMessage="Por favor, introduzca el estado."
                                                        ControlToValidate="State" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div class="PostalCodeContainer Question">
                                            <asp:Label ID="PostalCodeLbl" runat="server" Text="Código postal*" AssociatedControlID="PostalCode"></asp:Label>
                                            <asp:TextBox ID="PostalCode" runat="server" MaxLength="20" CssClass="inputTextBox"></asp:TextBox>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="PostalCodeValidator1" runat="server" ErrorMessage="Por favor, introduzca el código postal."
                                                    ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true"
                                                    Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                
						                        <asp:RegularExpressionValidator ID="PostalCodeValidator2" runat="server" Display="Dynamic"
							        ErrorMessage="<br />Please enter a valid Postal Code." ValidationExpression="^(\d{5}|\d{5}\-\d{4})$"
							        ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true"
							        CssClass="errormsg" ForeColor="#aa0000"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                        <div class="CountryContainer Question">
                                            <asp:Label ID="CountryLbl" runat="server" Text="País&nbsp;*" AssociatedControlID="Country"></asp:Label>
                                            <asp:DropDownList ID="Country" runat="server">
								                <asp:ListItem Value="">Seleccione...</asp:ListItem>
								                <asp:ListItem Value="MEX">Mexico</asp:ListItem>
                                                <asp:ListItem Value="CHIL">Chile</asp:ListItem>
                                                <asp:ListItem Value="CORI">Costa Rica</asp:ListItem>
                                                <asp:ListItem Value="ELSA">El Salvador</asp:ListItem>
                                                <asp:ListItem Value="GUAT">Guatemala</asp:ListItem>
                                                <asp:ListItem Value="HOND">Honduras</asp:ListItem>
                                                <asp:ListItem Value="NICA">Nicaragua</asp:ListItem>
                                                <asp:ListItem Value="CLBA">Colombia</asp:ListItem>
                                                <asp:ListItem Value="PERU">Peru</asp:ListItem>
                                                <asp:ListItem Value="PANA">Panama</asp:ListItem>
                                                <asp:ListItem Value="URUG">Uruguay</asp:ListItem>
                                                <asp:ListItem Value="VENE">Venezuela</asp:ListItem>
                                                <asp:ListItem Value="BRAZ">Brazil</asp:ListItem>
                                                <asp:ListItem Value="ECUA">Ecuador</asp:ListItem>
                                                <asp:ListItem Value="PARA">Paraguay</asp:ListItem>
                                                <asp:ListItem Value="DMRP">Dominican Republic</asp:ListItem>
							                </asp:DropDownList>
                                            <div class="ErrorContainer">
							                    <asp:RequiredFieldValidator ID="CountryValidator" runat="server" ErrorMessage="Por favor seleccione su País."
								                ControlToValidate="Country" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>                  </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelGender" class="GenderContainer Question">
                                            <asp:Label ID="GenderLbl" runat="server" Text="Sexo*" AssociatedControlID="Gender"></asp:Label>
                                            <asp:DropDownList ID="Gender" runat="server">
                                                <asp:ListItem Value="">Selecciona</asp:ListItem>
                                                <asp:ListItem Value="F">Mujer</asp:ListItem>
                                                <asp:ListItem Value="M">Hombre</asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="GenderValidator" runat="server" ErrorMessage="Por favor, introduzca su sexo."
                                                    ControlToValidate="Gender" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="BirthdayContainer Question">
                                            <asp:Label ID="DOBLbl" runat="server" Text="Fecha de nacimiento*" AssociatedControlID="yyyy"></asp:Label>
                                            <asp:DropDownList ID="yyyy" runat="server" CssClass="inputSelectBDay year">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="mm" runat="server" CssClass="inputSelectBDay month">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="dd" runat="server" CssClass="inputSelectBDay day">
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:CustomValidator ID="DOBValidator" runat="server" ErrorMessage="Por favor, introduzca su fecha de nacimiento."
                                                    OnServerValidate="DOBValidate" EnableClientScript="false" Display="Dynamic" CssClass="errormsg validdate"></asp:CustomValidator>
                                                <asp:RequiredFieldValidator ID="yyyyValidator" runat="server" ErrorMessage="Por favor, introduzca su año."
                                                    ControlToValidate="yyyy" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg year"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="mmValidator" runat="server" ErrorMessage="Por favor, introduzca su mes."
                                                    ControlToValidate="mm" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg month"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="ddValidator" runat="server" ErrorMessage="Por favor, introduzca su día."
                                                    ControlToValidate="dd" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg day"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="seperator png-fix">
                                        </div>
                                    </div>
                                              
                                    <div id="ctl00_ContentPlaceHolder1_ucForm_QuestionPanel" class="QuestionContainer">
                        			            
                                        <div class="MarketingQuestionContainer">
				                            <div class="CustomQuestiondropdown Question">
					                            <label for="Biore_mkt_q1" id="Biore_mkt_q1LBL" class="dropdownLabel">¿Cuántos productos para el cuidado de la piel usas a diario?*</label>
					                              <asp:DropDownList ID="Biore_mkt_q1" runat="server" CssClass="dropDownAnswer">
					                                <asp:ListItem Value="">Selecciona...</asp:ListItem>
						                            <asp:ListItem value="1">1</asp:ListItem>
						                            <asp:ListItem value="2">2</asp:ListItem>
						                            <asp:ListItem value="3">3</asp:ListItem>
						                            <asp:ListItem value="4">4</asp:ListItem>
						                            <asp:ListItem value="5 o más">5 o más</asp:ListItem>
					                              </asp:DropDownList>
					                              <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="v_q1" runat="server" ControlToValidate="Biore_mkt_q1" ErrorMessage="Por favor, conteste todas las preguntas necesarias." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
				                            </div>
				                            <div class="CustomQuestiondropdown Question">
					                            <label for="Biore_mkt_q7" id="Label1" class="dropdownLabel">Le presto más atención/doy más tiempo al cuidado de mi piel que otras personas.*</label>
					                            
					                                <asp:DropDownList ID="Biore_mkt_q7" runat="server" CssClass="dropDownAnswer">
					                                   <asp:ListItem Value="">Selecciona...</asp:ListItem>
						                                <asp:ListItem value="Totalmente de acuerdo">Totalmente de acuerdo</asp:ListItem>
						                                <asp:ListItem value="Un poco de acuerdo">Un poco de acuerdo</asp:ListItem>
						                                <asp:ListItem value="Ni de acuerdo ni en desacuerdo">Ni de acuerdo ni en desacuerdo</asp:ListItem>
						                                <asp:ListItem value="Un poco en desacuerdo">Un poco en desacuerdo</asp:ListItem>
						                                <asp:ListItem value="Totalmente en desacuerdo">Totalmente en desacuerdo</asp:ListItem>
					                                </asp:DropDownList>
					                                <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="v_Biore_mkt_q7" runat="server" ControlToValidate="Biore_mkt_q7" ErrorMessage="Por favor, conteste todas las preguntas necesarias." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                                </div>

				                            </div>
				                            <div class="CustomQuestiondropdown Question">
					                            <label for="Biore_mkt_q8" id="Biore_mkt_q8LBL" class="dropdownLabel">Estoy dispuesta a pagar más por productos para la piel que realmente deseo.*</label>
					                            <asp:DropDownList ID="Biore_mkt_q8" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem Value="">Selecciona...</asp:ListItem>
						                            <asp:ListItem value="Totalmente de acuerdo">Totalmente de acuerdo</asp:ListItem>
					                                <asp:ListItem value="Un poco de acuerdo">Un poco de acuerdo</asp:ListItem>
					                                <asp:ListItem value="Ni de acuerdo ni en desacuerdo">Ni de acuerdo ni en desacuerdo</asp:ListItem>
					                                <asp:ListItem value="Un poco en desacuerdo">Un poco en desacuerdo</asp:ListItem>
					                                <asp:ListItem value="Totalmente en desacuerdo">Totalmente en desacuerdo</asp:ListItem>
					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
						                            <asp:RequiredFieldValidator ID="v_Biore_mkt_q8" runat="server" ControlToValidate="Biore_mkt_q8" ErrorMessage="Por favor, conteste todas las preguntas necesarias." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
					                            </div>
				                            </div>
				                            <div class="CustomQuestiondropdown Question">
					                            <label for="Biore_mkt_q2" id="Biore_mkt_q2LBL" class="dropdownLabel">¿Qué tan probable es que recomiendes tus productos para la piel favoritos a tus amistades?*</label>
					                            <asp:DropDownList ID="Biore_mkt_q2" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem Value="">Selecciona...</asp:ListItem>
						                            <asp:ListItem value="Muy probable">Muy probable</asp:ListItem>
						                            <asp:ListItem value="Un poco probable">Un poco probable</asp:ListItem>
						                            <asp:ListItem value="Un poco improbable">Un poco improbable</asp:ListItem>
						                            <asp:ListItem value="Para nada probable">Para nada probable</asp:ListItem>
					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
						                            <asp:RequiredFieldValidator ID="v_Biore_mkt_q2" runat="server" ControlToValidate="Biore_mkt_q2" ErrorMessage="Por favor, conteste todas las preguntas necesarias." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
					                            </div>
				                            </div>
				                            <div class="CustomQuestioncheckbox Question trait413 cbVert noneToggle1">
				                                <label for="Biore_mkt_q3" id="Biore_mkt_q3LBL" class="checkboxLabel">¿Qué productos de Bioré<sup>&trade;</sup> usas actualmente? (selecciona todos los que correspondan)*</label>
				                                <asp:CheckBoxList ID="Biore_mkt_q3" RepeatDirection="Vertical" runat="server" RepeatLayout="Flow" CssClass="checkboxAnswer">
							                        <asp:ListItem Value="Limpiador Equilibrante para Piel Mixta de Bioré®">Limpiador Equilibrante para Piel Mixta de Bioré&lt;sup&gt;&reg;&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Limpiador Activado por Vapor de Bioré®">Limpiador Activado por Vapor de Bioré&lt;sup&gt;&reg;&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Toallitas Desmaquillantes de Bioré®">Toallitas Desmaquillantes de Bioré&lt;sup&gt;&reg;&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Limpiador Desintoxicante 4 en 1 de Bioré®">Limpiador Desintoxicante 4 en 1 de Bioré&lt;sup&gt;&reg;&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Toallitas para Limpieza Diaria de Bioré®">Toallitas para Limpieza Diaria de Bioré&lt;sup&gt;&reg;&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Limpiador Refrescante Anti-Acné de Bioré®">Limpiador Refrescante Anti-Acné de Bioré&lt;sup&gt;&reg;&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Exfoliante para Poros Obstruidos de Bioré®">Exfoliante para Poros Obstruidos de Bioré&lt;sup&gt;&reg;&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Limpiador Térmico para Puntos Negros de Bioré®">Limpiador Térmico para Puntos Negros de Bioré&lt;sup&gt;&reg;&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Astringente Anti-Acné de Bioré®">Astringente Anti-Acné de Bioré&lt;sup&gt;&reg;&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Bandas Ultra de Limpieza Profunda de Bioré®">Bandas Ultra de Limpieza Profunda de Bioré&lt;sup&gt;&reg;&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Bandas de Limpieza Profunda de Bioré®">Bandas de Limpieza Profunda de Bioré&lt;sup&gt;&reg;&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Bandas de Limpieza Profunda Combinadas de Bioré®">Bandas de Limpieza Profunda Combinadas de Bioré&lt;sup&gt;&reg;&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Ninguno de los anteriores">Ninguno de los anteriores</asp:ListItem>
							                    </asp:CheckBoxList>
				         <span class="cbinput"></span>
				         
					                            <div class="ErrorContainer">
						                            <skm:CheckBoxListValidator ID="HairAppliancesLblVal" ControlToValidate="Biore_mkt_q3"
                                                    runat="server" Display="Dynamic" CssClass="errormsg" ErrorMessage="¿Qué productos de Bioré<sup>&trade;</sup> usas actualmente?" />
					                            </div>
				                            </div>
				                            <!--<div class="CustomQuestiondropdown Question">
					                            <label for="Biore_mkt_q4" id="Biore_mkt_q4LBL" class="dropdownLabel">¿Qué marca de productos faciales usas más a menudo? (selecciona una)*</label>
					                            
					                            <asp:DropDownList ID="Biore_mkt_q4" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem Value="">Selecciona...</asp:ListItem>
						                            <asp:ListItem value="Aveeno">Aveeno&#174;</asp:ListItem>
						                            <asp:ListItem value="Avon®">Avon&#174;</asp:ListItem>

						                            <asp:ListItem value="Biore">Bior&#233;&#174; products</asp:ListItem>
						                            <asp:ListItem value="Cetaphil">Cetaphil&#174;</asp:ListItem>
						                            <asp:ListItem value="Clean and Clear">Clean &amp; Clear&#174;</asp:ListItem>
						                            <asp:ListItem value="Clearasil">Clearasil&#174;</asp:ListItem>
						                            <asp:ListItem value="Clinique®">Clinique&#174;</asp:ListItem>

						                            <asp:ListItem value="Dove">Dove&#174;</asp:ListItem>
						                            <asp:ListItem value="Garnier Nutritioniste">Garnier Nutritioniste</asp:ListItem>
						                            <asp:ListItem value="L'Oreal">L' Oreal&#174;</asp:ListItem>
						                            <asp:ListItem value="Mary Kay">Mary Kay&#174;</asp:ListItem>
						                            <asp:ListItem value="Neutrogena">Neutrogena&#174;</asp:ListItem>
						                            <asp:ListItem value="Noxzema">Noxzema&#174;</asp:ListItem>
                                                    <asp:ListItem value="Olay">Olay&#174;</asp:ListItem>
						                            <asp:ListItem value="ProActiv Solutions">ProActiv&#174; Solutions</asp:ListItem>
						                            <asp:ListItem value="St. Ives">St. Ives&#174;</asp:ListItem>
						                            <asp:ListItem value="Store Brand">Store Brand</asp:ListItem>
						                            <asp:ListItem value="Other Department Store brand">Other Department Store brand</asp:ListItem>

						                            <asp:ListItem value="Otro">Otro</asp:ListItem>
						                            <asp:ListItem Value="Ninguno de los anteriores">Ninguno de los anteriores</asp:ListItem>
					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
						                             <asp:RequiredFieldValidator ID="v_Biore_mkt_q4" runat="server" ControlToValidate="Biore_mkt_q4" ErrorMessage="Por favor, conteste todas las preguntas necesarias." Display="Dynamic" CssClass="errormsg" Enabled="false"></asp:RequiredFieldValidator>
					                            </div>
				                            </div>-->
				                            <div class="CustomQuestioncheckbox Question trait419 cbVert noneToggle2">
					                            <label for="Biore_mkt_q9" id="Biore_mkt_q9LBL" class="checkboxLabel">¿Cuál de los siguientes tipos de productos usas generalmente? (selecciona todos los que correspondan)*</label>
					                           <asp:CheckBoxList ID="Biore_mkt_q9" RepeatDirection="Vertical" runat="server" RepeatLayout="Flow" CssClass="checkboxAnswer">
							                        <asp:ListItem>Limpiador líquido/en crema/en gel</asp:ListItem>
							                        <asp:ListItem>Toallitas limpiadoras</asp:ListItem>
							                        <asp:ListItem>Crema/loción facial hidratante</asp:ListItem>
							                        <asp:ListItem>Crema/loción facial hidratante con protección solar</asp:ListItem>
							                        <asp:ListItem>Exfoliante facial</asp:ListItem>
							                        <asp:ListItem>Toner / Astringente</asp:ListItem>
							                        <asp:ListItem>Tratamiento para el acné/marcas de acné</asp:ListItem>
							                        <asp:ListItem>Productos para el tratamiento de la decoloración de la piel (daño por el sol)</asp:ListItem>
							                        <asp:ListItem>Gel / crema / tratamiento para los ojos</asp:ListItem>
							                        <asp:ListItem>Crema nocturna / hidratante nocturno</asp:ListItem>
							                        <asp:ListItem>Desmaquillante</asp:ListItem>
							                        <asp:ListItem>Ninguno de los anteriores</asp:ListItem>
							                    </asp:CheckBoxList>
							                    
				         <span class="cbinput"></span>
					                            <div class="ErrorContainer">
						                            <skm:CheckBoxListValidator ID="v_Biore_mkt_q9" ControlToValidate="Biore_mkt_q9"
                                                    runat="server" Display="Dynamic" CssClass="errormsg" ErrorMessage="Por favor, conteste todas las preguntas necesarias." />
					                            </div>
					                           
				                            </div>
				                            <div class="CustomQuestioncheckbox Question trait415 cbVert noneToggle3">
					                            <label for="Biore_mkt_q5" id="Biore_mkt_q5LBL" class="checkboxLabel">¿Cuáles de las siguientes características te interesaría en un producto facial? (selecciona todos los que correspondan)*</label>
					                            
					                            <asp:CheckBoxList ID="Biore_mkt_q5" RepeatDirection="Vertical" runat="server" RepeatLayout="Flow" CssClass="checkboxAnswer">
							                        <asp:ListItem>Conveniencia</asp:ListItem>
							                        <asp:ListItem>Antienvejecimiento</asp:ListItem>
							                        <asp:ListItem>Limpieza profunda</asp:ListItem>
							                        <asp:ListItem>Suave / piel sensible</asp:ListItem>
							                        <asp:ListItem>Para piel propensa al acné</asp:ListItem>
							                        <asp:ListItem>Para piel mixta</asp:ListItem>
							                        <asp:ListItem>Desmaquillante / mejorar el maquillaje</asp:ListItem>
							                        <asp:ListItem>Ninguna de los anteriores</asp:ListItem>
							                    </asp:CheckBoxList>					                    
				         <span class="cbinput"></span>
					                            <div class="ErrorContainer">
						                            <skm:CheckBoxListValidator ID="v_Biore_mkt_q5" ControlToValidate="Biore_mkt_q5"
                                                    runat="server" Display="Dynamic" CssClass="errormsg" ErrorMessage="Por favor, conteste todas las preguntas necesarias." />
					                            </div>

				                            </div>
				                            <div class="CustomQuestioncheckbox Question trait416 cbVert noneToggle4">
					                            <label for="Biore_mkt_q6" id="Biore_mkt_q6LBL" class="checkboxLabel">¿Cuál de los siguientes problemas dirías que te preocupa mucho? (Si es el caso) (selecciona todos los que correspondan)*</label>
					                            
					                            
					                            <asp:CheckBoxList ID="Biore_mkt_q6" RepeatDirection="Vertical" runat="server" RepeatLayout="Flow" CssClass="checkboxAnswer">
							                        <asp:ListItem>Acné</asp:ListItem>
							                        <asp:ListItem>Manchas de envejecimiento</asp:ListItem>
							                        <asp:ListItem>Puntos negros</asp:ListItem>
							                        <asp:ListItem>Imperfecciones / espinillas</asp:ListItem>
							                        <asp:ListItem>Marcas / cicatrices de acné</asp:ListItem>
							                        <asp:ListItem>Patas de gallo alrededor de los ojos</asp:ListItem>
							                        <asp:ListItem>Ojeras</asp:ListItem>
							                        <asp:ListItem>Líneas muy profundas / arrugas</asp:ListItem>
							                        <asp:ListItem>Piel mixta</asp:ListItem>
							                        <asp:ListItem>Vello facial</asp:ListItem>
							                        <asp:ListItem>Decoloración de la piel del rostro</asp:ListItem>
							                        <asp:ListItem>Líneas delgadas / arrugas</asp:ListItem>
							                        <asp:ListItem>Falta de firmeza</asp:ListItem>
							                        <asp:ListItem>Poros grandes / agrandados</asp:ListItem>
							                        <asp:ListItem>Áreas grasosas / brillantes</asp:ListItem>
							                        <asp:ListItem>Ojos hinchados</asp:ListItem>
							                        <asp:ListItem>Enrojecimiento / Rosácea</asp:ListItem>
							                        <asp:ListItem>Piel delicada</asp:ListItem>
							                        <asp:ListItem>Daño por el sol</asp:ListItem>
							                        <asp:ListItem>Textura irregular de la piel</asp:ListItem>
							                        <asp:ListItem>Tono irregular de la piel</asp:ListItem>
							                        <asp:ListItem>Ninguno de los anteriores</asp:ListItem>
							                    </asp:CheckBoxList>                        
				         <span class="cbinput"></span>
					                            <div class="ErrorContainer">
						                            <skm:CheckBoxListValidator ID="v_Biore_mkt_q6" ControlToValidate="Biore_mkt_q6"
                                                    runat="server" Display="Dynamic" CssClass="errormsg" ErrorMessage="Por favor, conteste todas las preguntas necesarias." />
					                            </div>
				                            </div>
				                            <div class="CustomQuestiondropdown Question trait420">
					                            <label for="Biore_mkt_q10" id="Biore_mkt_q10LBL" class="dropdownLabel">¿Cómo describirías tu tipo de piel? (selecciona una)*</label>
                                                
                                                <asp:DropDownList ID="Biore_mkt_q10" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem Value="">Selecciona...</asp:ListItem>
						                            <asp:ListItem>Reseca</asp:ListItem>
						                            <asp:ListItem>Normal</asp:ListItem>

						                            <asp:ListItem>Grasosas</asp:ListItem>
						                            <asp:ListItem>Mixta Normal a Grasosas</asp:ListItem>
						                            <asp:ListItem>Mixta Normal a Reseca</asp:ListItem>
						                            <asp:ListItem>Mixta Grasosas-Reseca</asp:ListItem>

					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
						                             <asp:RequiredFieldValidator ID="v_Biore_mkt_q10" runat="server" ControlToValidate="Biore_mkt_q10" ErrorMessage="Por favor, conteste todas las preguntas necesarias." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
					                            </div>
				                            </div>
			                            </div>
		                            </div>
           
                                    <div class="OptinContainer">
                                        <div class="seperator png-fix"></div>
	                                    <p class="privacy">
		                                    Tu privacidad es importante para nosotros. Puedes confiar en que Kao USA Inc. usará la información personal de acuerdo a nuestra  <a href="http://www.kaobrands.com/privacy_policy.asp"
			                                    target="_blank">Política de privacidad.</a>.
	                                    </p>

	                                    <div class="CurrentSiteOptinContainer">
	                                        <ul>
		                                        <li>
		                                            <asp:CheckBox ID="bioreoptin" runat="server" />
			                                        <asp:Label ID="bioreoptinLabel" runat="server" Text="Sí, avísenme de nuevos productos y ofertas de Bioré® Skincare" AssociatedControlID="bioreoptin" CssClass="siteOptinChkbox" />

			                                        <div class="ErrorContainer">
                                    			        <skm:CheckBoxValidator ID="cbv_bioreOptin" runat="server" ControlToValidate="bioreoptin" ErrorMessage="Si desea convertirse en un Biore® Miembro, por favor, marque Sí!" CssClass="errormsg" Display="Dynamic"></skm:CheckBoxValidator>
				                                    </div>

		                                        </li>
	                                        </ul>
	                                    </div>
                                        
                                        <!--<div id="ctl00_ContentPlaceHolder1_ucForm_ucFormOptin_PanelMultiOptin" class="MultiBrandOptinContainer">
                                    			            
		                                    <ul>            
			                                    <li id="optinToAll">
			                                        <asp:CheckBox ID="MultiOptin" runat="server" CssClass="multiOptin" />
			                                        <asp:Label ID="MultiOptinLabel" runat="server" Text="Yes, I'd like to receive emails and newsletters from other great products from Kao Brands Company:" AssociatedControlID="MultiOptin" />
			        
			                                    </li>			

			                                    <li id="ctl00_ContentPlaceHolder1_ucForm_ucFormOptin_curelListItem">
			                                        <asp:CheckBox ID="cureloptin" runat="server" />
			                                <asp:Label ID="cureloptinLabel" runat="server" Text="Cur&eacute;l<sup>&reg;</sup></a> Skincare's full line of hand, foot and body moisturizers delivers freedom from dry skin." AssociatedControlID="cureloptin"
				                                CssClass="brandLabels" />
			                         
			                                    </li>
			                                    <li id="ctl00_ContentPlaceHolder1_ucForm_ucFormOptin_jergensListItem">
			                                        <asp:CheckBox ID="jergensoptin" runat="server" />
			                                        <asp:Label ID="jergensoptinLabel" runat="server" Text="Jergens<sup>&reg;</sup></a> Skincare collection of moisturizers delivers a natural glow, smooth and firm skin and an allure that captivates." AssociatedControlID="jergensoptin"
				                                CssClass="brandLabels" />

			                                    </li>

			                                    <li>
				                                    <p class="privDisclaimer">Before submitting your information, please view our <a
					                                    href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">Privacy Policy</a>.</p>
			                                    </li>
		                                    </ul>
                                    	
		                                    </div>	-->	

                                    </div> 

                                    <div id="submit-container" class="SubmitContainer png-fix">
                                        <asp:Button ID="Button1" runat="server" CssClass="submit buttonLink png-fix" Text="ENVIAR ›" OnClick="Step1_ButtonClick"  />
                                    </div>
                                    <asp:HiddenField ID="hfHookID" runat="server" />
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="OptinFormResults" runat="server">
                                 <div class="ConfrimationContainer"><h2>¡Gracias!</h2><p>Gracias por interesarte en Bioré® Skincare.</p></div>  

                            </asp:Panel>
                            <asp:Panel ID="OptinError" runat="server">
                                <h2>Lo sentimos...</h2>
                                
                                Lo sentimos. No se encontró la página solicitada o hubo un error al mostrarla. <a href="javascript: back();">
                                            Inicio</a>.  <asp:Literal ID="litError" runat="server"></asp:Literal>
                            </asp:Panel>
                            <div class="FormBottom png-fix">
                            </div>
                            <!-- Disclaimer -->
                            <div id="DisclaimerContainer" class="png-fix">
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">

                    </script>

                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script type="text/javascript">
        $(function () {

            if ($(".signUp").length) {
                toggle("noneToggle1");
                toggle("noneToggle2");
                toggle("noneToggle3");
                toggle("noneToggle4");
            }

            $("#ctl00_ContentPlaceHolder1_cureloptin, #ctl00_ContentPlaceHolder1_jergensoptin").click(function () {
                if ($('#ctl00_ContentPlaceHolder1_cureloptin:checked').val()
                    || $('#ctl00_ContentPlaceHolder1_jergensoptin:checked').val()) {
                    $('#ctl00_ContentPlaceHolder1_MultiOptin').attr("checked", "true");
                }
                else { $('#ctl00_ContentPlaceHolder1_MultiOptin').removeAttr("checked"); }
            });
            $("#ctl00_ContentPlaceHolder1_MultiOptin").click(function () {
                if ($('#ctl00_ContentPlaceHolder1_MultiOptin:checked').val()) {
                    $("#ctl00_ContentPlaceHolder1_cureloptin").attr("checked", "true");
                    $("#ctl00_ContentPlaceHolder1_jergensoptin").attr("checked", "true");
                } else {
                    $("#ctl00_ContentPlaceHolder1_cureloptin").removeAttr("checked");
                    $("#ctl00_ContentPlaceHolder1_jergensoptin").removeAttr("checked");
                }
            });
        });


        function toggle(className) {
            var checkToggle = $("." + className + " input:last");
            checkToggle.click(function () {
                if ($("#" + checkToggle.attr('id') + ":checked").val()) {
                    $("." + className + " input").each(function () {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).attr("checked", false);
                            $(this).attr('disabled', 'disabled');
                        }
                    });
                } else {
                    $("." + className + "  input").each(function () {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).removeAttr('disabled');
                        }
                    });
                }
            });
        }
    </script>

</asp:Content>
