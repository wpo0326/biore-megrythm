﻿<%@ Page Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs"
    Inherits="Biore2012.FormConfig.ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%#SquishIt.Framework.Bundle .Css()
        .Add("~/css/formConfig.css")
        .Render("~/css/combinedform_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow">
            </div>
            <div class="centeringDiv">
                <div class="responsive contact">
                    <div id="contactFormWrap">
                        <div id="BrandImageContainer" class="png-fix">
                            <img class="png-fix" src="images/forms/contactUsPhoto.jpg" style="border-width: 0px;" />
                        </div>
                        <div id="ContactFormContainer">
                            <asp:Panel ID="ContactDetails" runat="server">
                                <!-- Header -->
                                <div id="formHeader">
                                    <h1 id="ctl00_ContentPlaceHolder1_ucForm_PageHeader">
                                        Contactanos
                                    </h1>
                                </div>
                                <!-- Description -->
                                <div class="DescriptionContainer png-fix">
                                    <h2>
                                        Por Correo</h2>
                                    <p>
                                        Departamento del cuidado de consumidor<br>
                                        Kao USA Inc.<br>
                                        2535 Spring Grove Ave<br>
                                        Cincinnati, OH 45214</p>
                                    
                                    <h2>
                                        Internet</h2>
                                    <p>
                                        Llena el siguiente formulario.</p>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="ContactForm" runat="server">
                                <!-- From Fields -->
                                <div class="FormContainer">
                                    <p class="req png-fix">
                                        <em>Obligatorio*</em></p>
                                    <div class="MemberInfoContainer png-fix">
                                        <div class="NameWrapper">
                                            <div class="FirstNameContainer Question">
                                                <asp:Label ID="FNameLbl" runat="server" Text="Nombre*" AssociatedControlID="FName"></asp:Label>
                                                <asp:TextBox ID="FName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="FNameValidator1" runat="server" ErrorMessage="Por favor, introduzca su nombre de pila"
                                                        ControlToValidate="FName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="FNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your First Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="FName" EnableClientScript="true"
                                                        SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="LastNameContainer Question">
                                                <asp:Label ID="LNameLbl" runat="server" Text="Apellido*" AssociatedControlID="LName"></asp:Label>
                                                <asp:TextBox ID="LName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="LNameValidator1" runat="server" ErrorMessage="Por favor, introduzca su apellido."
                                                        ControlToValidate="LName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="LNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Last Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="LName" SetFocusOnError="true"
                                                        Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="EmailWrapper">
                                            <div class="EmailContainer Question">
                                                <asp:Label ID="EmailLbl" runat="server" Text="Correo electrónico*" AssociatedControlID="Email"></asp:Label>
                                                <asp:TextBox ID="Email" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="EmailValidator1" runat="server" ErrorMessage="Por favor, introduzca su correo electrónico"
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="EmailValidator2" runat="server" ErrorMessage="Por favor, introduzca sólo caracteres válidos en el campo de correo electrónico"
                                                        ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="ConfirmEmailContainer Question">
                                                <asp:Label ID="RetypeEmailLbl" runat="server" Text="Confirma tu correo electrónico*" AssociatedControlID="RetypeEmail"></asp:Label>
                                                <asp:TextBox ID="RetypeEmail" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="RetypeValidator1" runat="server" ErrorMessage="Por favor, confirma tu correo electrónico."
                                                        ControlToValidate="RetypeEmail" EnableClientScript="true" SetFocusOnError="true"
                                                        Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="RetypeValidator2" Text="Make sure you retyped the Email correctly."
                                                        ControlToValidate="RetypeEmail" ControlToCompare="Email" EnableClientScript="true"
                                                        SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelAddress" class="AddressContainer">
                                            <div class="AddressWrapper">
                                                <div class="Address1Container Question">
                                                    <asp:Label ID="Address1Lbl" runat="server" Text="Dirección 1*" AssociatedControlID="Address1"></asp:Label>
                                                    <asp:TextBox ID="Address1" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="Address1Validator1" runat="server" ErrorMessage="Por favor, introduzca la dirección de la calle."
                                                            ControlToValidate="Address1" EnableClientScript="true" SetFocusOnError="true"
                                                            Display="Dynamic" CssClass="errormsg" />
                                                        <asp:RegularExpressionValidator ID="Address1Validator2" runat="server" ErrorMessage="Please do not use special characters in the Address 1 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address1" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="Address2Container Question">
                                                    <asp:Label ID="Address2Lbl" runat="server" Text="Dirección 2" AssociatedControlID="Address2"></asp:Label>
                                                    <asp:TextBox ID="Address2" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="Address2Validator1" runat="server" ErrorMessage="Please do not use special characters in the Address 2 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address2" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="Address3Container Question">
                                                    <asp:Label ID="Address3Lbl" runat="server" Text="Dirección 3" AssociatedControlID="Address3"></asp:Label>
                                                    <asp:TextBox ID="Address3" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="Address3Validator1" runat="server" ErrorMessage="Please do not use special characters in the Address 3 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address3" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="CityContainer Question">
                                                    <asp:Label ID="CityLbl" runat="server" Text="Ciudad*" AssociatedControlID="City"></asp:Label>
                                                    <asp:TextBox ID="City" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="CityValidator1" runat="server" ErrorMessage="Por favor, introduzca su ciudad."
                                                            ControlToValidate="City" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                            CssClass="errormsg" />
                                                        <asp:RegularExpressionValidator ID="CityValidator2" runat="server" ErrorMessage="Please do not use special characters in the City field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="City" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="StateContainer Question">
                                                <asp:Label ID="StateLbl" runat="server" Text="Estado*" AssociatedControlID="State"></asp:Label>
                                                <asp:DropDownList ID="State" runat="server">
                                                    <asp:ListItem Value="">Selecciona</asp:ListItem>
                                                    <asp:ListItem Value="Aguascalientes">Aguascalientes</asp:ListItem>
                                                    <asp:ListItem Value="Baja California">Baja California</asp:ListItem>
                                                    <asp:ListItem Value="Baja California Sur">Baja California Sur</asp:ListItem>
                                                    <asp:ListItem Value="Campeche">Campeche</asp:ListItem>
                                                    <asp:ListItem Value="Chiapas">Chiapas</asp:ListItem>
                                                    <asp:ListItem Value="Chihuahua">Chihuahua</asp:ListItem>
                                                    <asp:ListItem Value="Coahuila">Coahuila</asp:ListItem>
                                                    <asp:ListItem Value="Colima">Colima</asp:ListItem>
                                                    <asp:ListItem Value="Durango">Durango</asp:ListItem>
                                                    <asp:ListItem Value="Guanajuato">Guanajuato</asp:ListItem>
                                                    <asp:ListItem Value="Guerrero">Guerrero</asp:ListItem>
                                                    <asp:ListItem Value="Hidalgo">Hidalgo</asp:ListItem>
                                                    <asp:ListItem Value="Jalisco">Jalisco</asp:ListItem>
                                                    <asp:ListItem Value="Mexico State">Mexico State</asp:ListItem>
                                                    <asp:ListItem Value="Michoacán">Michoacán</asp:ListItem>
                                                    <asp:ListItem Value="Morelos">Morelos</asp:ListItem>
                                                    <asp:ListItem Value="Nayarit">Nayarit</asp:ListItem>
                                                    <asp:ListItem Value="Nuevo León">Nuevo León</asp:ListItem>
                                                    <asp:ListItem Value="Oaxaca">Oaxaca</asp:ListItem>
                                                    <asp:ListItem Value="Puebla">Puebla</asp:ListItem>
                                                    <asp:ListItem Value="Querétaro">Querétaro</asp:ListItem>
                                                    <asp:ListItem Value="Quintana Roo">Quintana Roo</asp:ListItem>
                                                    <asp:ListItem Value="San Luis Potosí">San Luis Potosí</asp:ListItem>
                                                    <asp:ListItem Value="Sinaloa">Sinaloa</asp:ListItem>
                                                    <asp:ListItem Value="Sonora">Sonora</asp:ListItem>
                                                    <asp:ListItem Value="Tabasco">Tabasco</asp:ListItem>
                                                    <asp:ListItem Value="Tamaulipas">Tamaulipas</asp:ListItem>
                                                    <asp:ListItem Value="Tlaxcala">Tlaxcala</asp:ListItem>
                                                    <asp:ListItem Value="Veracruz">Veracruz</asp:ListItem>
                                                    <asp:ListItem Value="Yucatán">Yucatán</asp:ListItem>
                                                    <asp:ListItem Value="Zacatecas">Zacatecas</asp:ListItem>
                                                    <asp:ListItem Value="Otro">Otro / Ninguno</asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="StateValidator" runat="server" ErrorMessage="Por favor, introduzca el estado."
                                                        ControlToValidate="State" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="PostalCodeContainer Question">
                                            <asp:Label ID="PostalCodeLbl" runat="server" Text="Código postal*" AssociatedControlID="PostalCode"></asp:Label>
                                            <asp:TextBox ID="PostalCode" runat="server" MaxLength="20" CssClass="inputTextBox"></asp:TextBox>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="PostalCodeValidator1" runat="server" ErrorMessage="Por favor, introduzca el código postal."
                                                    ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true"
                                                    Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="PostalCodeValidator2" runat="server" Display="Dynamic"
							        ErrorMessage="<br />Por favor, introduzca el código postal." ValidationExpression="^(\d{5}|\d{5}\-\d{4})$"
							        ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true"
							        CssClass="errormsg" ForeColor="#aa0000"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                        <div class="CountryContainer Question">
                                            <asp:Label ID="CountryLbl" runat="server" Text="País&nbsp;*" AssociatedControlID="Country"></asp:Label>
                                            <asp:DropDownList ID="Country" runat="server">
								                <asp:ListItem Value="">Seleccione...</asp:ListItem>
								                <asp:ListItem Value="MEX">Mexico</asp:ListItem>
                                                <asp:ListItem Value="CHIL">Chile</asp:ListItem>
                                                <asp:ListItem Value="CORI">Costa Rica</asp:ListItem>
                                                <asp:ListItem Value="ELSA">El Salvador</asp:ListItem>
                                                <asp:ListItem Value="GUAT">Guatemala</asp:ListItem>
                                                <asp:ListItem Value="HOND">Honduras</asp:ListItem>
                                                <asp:ListItem Value="NICA">Nicaragua</asp:ListItem>
                                                <asp:ListItem Value="CLBA">Colombia</asp:ListItem>
                                                <asp:ListItem Value="PERU">Peru</asp:ListItem>
                                                <asp:ListItem Value="PANA">Panama</asp:ListItem>
                                                <asp:ListItem Value="URUG">Uruguay</asp:ListItem>
                                                <asp:ListItem Value="VENE">Venezuela</asp:ListItem>
                                                <asp:ListItem Value="BRAZ">Brazil</asp:ListItem>
                                                <asp:ListItem Value="ECUA">Ecuador</asp:ListItem>
                                                <asp:ListItem Value="PARA">Paraguay</asp:ListItem>
                                                <asp:ListItem Value="DMRP">Dominican Republic</asp:ListItem>
							                </asp:DropDownList>
							               <div class="ErrorContainer">
							                    <asp:RequiredFieldValidator ID="CountryValidator" runat="server" ErrorMessage="Por favor seleccione su País."
								                ControlToValidate="Country" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>                  </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelGender" class="GenderContainer Question">
                                            <asp:Label ID="GenderLbl" runat="server" Text="Sexo*" AssociatedControlID="Gender"></asp:Label>
                                            <asp:DropDownList ID="Gender" runat="server">
                                                <asp:ListItem Value="">Selecciona</asp:ListItem>
                                                <asp:ListItem Value="F">Mujer</asp:ListItem>
                                                <asp:ListItem Value="M">Hombre</asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="GenderValidator" runat="server" ErrorMessage="Por favor, introduzca su sexo."
                                                    ControlToValidate="Gender" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="BirthdayContainer Question">
                                            <asp:Label ID="DOBLbl" runat="server" Text="Fecha de nacimiento*" AssociatedControlID="yyyy"></asp:Label>
                                            <asp:DropDownList ID="yyyy" runat="server" CssClass="inputSelectBDay year">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="mm" runat="server" CssClass="inputSelectBDay month">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="dd" runat="server" CssClass="inputSelectBDay day">
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:CustomValidator ID="DOBValidator" runat="server" ErrorMessage="Por favor, introduzca su fecha de nacimiento."
                                                    OnServerValidate="DOBValidate" EnableClientScript="false" Display="Dynamic" CssClass="errormsg validdate"></asp:CustomValidator>
                                                <asp:RequiredFieldValidator ID="yyyyValidator" runat="server" ErrorMessage="Por favor, introduzca su año."
                                                    ControlToValidate="yyyy" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg year"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="mmValidator" runat="server" ErrorMessage="Por favor, introduzca su mes."
                                                    ControlToValidate="mm" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg month"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="ddValidator" runat="server" ErrorMessage="Por favor, introduzca su día ."
                                                    ControlToValidate="dd" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg day"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="PhoneContainer Question">
                                            <%--<label for="Phone">Phone</label>--%>
                                            <asp:Label ID="PhoneLbl" runat="server" Text="Teléfono" AssociatedControlID="Phone"></asp:Label>
                                            <asp:TextBox ID="Phone" MaxLength="50" runat="server" />
                                            <div class="ErrorContainer">
                                                <asp:RegularExpressionValidator ID="PhoneValidator1" runat="server" ErrorMessage="Por favor, introduzca su Teléfono."
                                                    ValidationExpression="^[^<>]+$" ControlToValidate="Phone" EnableClientScript="true"
                                                    SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                <%-- The following RegEx Validators is for US type numbers.  IMPORTANT: 
								 Please test against your user base before using as you may need to modify it for other formats!  --%>
                                                <%--<asp:RegularExpressionValidator ID="PhoneValidator2" runat="server"
								ErrorMessage="<br/>Your Home Phone is not required, but please enter only valid characters."
								ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"
								ControlToValidate="Phone" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg phone"></asp:RegularExpressionValidator>--%>
                                            </div>
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelComment" class="CommentContainer Question">
                                            <asp:Label ID="QuestionCommentLbl" runat="server" Text="Pregunta / Comentario*" AssociatedControlID="QuestionComment"></asp:Label>
                                            <asp:TextBox ID="QuestionComment" runat="server" TextMode="MultiLine" />
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="QuestionCommentValidator" runat="server" ErrorMessage="Por favor completa: Pregunta / Comentario."
                                                    ControlToValidate="QuestionComment" EnableClientScript="true" SetFocusOnError="true"
                                                    Display="Dynamic" CssClass="errormsg questioncomment" />
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                    </div>
                                    <div id="ctl00_ContentPlaceHolder1_ucForm_QuestionPanel" class="QuestionContainer">
                                    </div>
                                    <div id="submit-container" class="SubmitContainer png-fix">
                                        <asp:Button ID="Button1" runat="server" CssClass="submit buttonLink png-fix" Text="enviar contacto ›" />
                                    </div>
                                    <asp:HiddenField ID="hfHookID" runat="server" />
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="ContactFormSuccess" runat="server">
                                <div id="contactSuccess">
                                    <h2>
                                        ¡Gracias!</h2>
                                    <p>
                                        Gracias por contactarnos. Un miembro del equipo de Atención a Clientes se pondrá en contacto contigo tan pronto como sea posible. Para más información, únete al movimiento para una piel maravillosa todos los días haciendo <a href="email-newsletter-sign-up">clic aquí click here</a>.</p>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="ContactFormFailure" runat="server">
                                <div id="contactFailure">
                                    <h2>
                                        Lo sentimos...</h2>
                                    <p>
                                        Lo sentimos. No se encontró la página solicitada o hubo un error al mostrarla. <a href="javascript: back();">
                                            Inicio</a>.</p>
                                    <asp:Literal ID="litError" runat="server"></asp:Literal>
                                </div>
                            </asp:Panel>
                            <div class="FormBottom png-fix">
                            </div>
                            <!-- Disclaimer -->
                            <div id="DisclaimerContainer" class="png-fix">
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">

                    </script>

                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script type="text/javascript">
        $(function () {
            if ($(".signUp").length) {
                toggle("noneToggle1");
                toggle("noneToggle2");
                toggle("noneToggle3");
                toggle("noneToggle4");
            }
        });
        function toggle(className) {
            var checkToggle = $("." + className + " input:last");
            checkToggle.click(function () {
                if ($("#" + checkToggle.attr('id') + ":checked").val()) {
                    $("." + className + " input").each(function () {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).attr("checked", false);
                            $(this).attr('disabled', 'disabled');
                        }
                    });
                } else {
                    $("." + className + "  input").each(function () {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).removeAttr('disabled');
                        }
                    });
                }
            });
        }
    </script>

</asp:Content>
