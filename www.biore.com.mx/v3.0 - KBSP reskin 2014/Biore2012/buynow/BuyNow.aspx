<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BuyNow.aspx.cs" Inherits="Biore2012.buy_now.BuyNow" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><asp:Literal ID="litProductName1" runat="server"></asp:Literal></title>
    <link rel="Stylesheet" href="/en-US/css/global.css" />
    <style type="text/css">
        body {
            background-color: #FFF !important;
            overflow-x: hidden;
            height: 100%;
        }
    </style>
    <style type="text/css">
        body {
            background: white;
            font: 12px arial,helvetica,sans-serif;
        }
    </style>
    <script type="text/javascript" src='<%= VirtualPathUtility.ToAbsolute("~/js/jquery-1.8.1.min.js") %>'></script>
    <script type="text/javascript"
src="http://content.channelintelligence.com/scripts/ykb_PopupWindow.js"></script>
    <script type="text/javascript"
src="http://content.channelintelligence.com/scripts/cii_embeddedfunctions.asp"></script>


    <script language="JavaScript" type="text/JavaScript">
    <!--
    function submitShopOnline() {
        document.frmFindLoc.submit();
    }

    //var submitInterval = setInterval("checkSubmit", 100);

    function checkSubmit() {
        if (document.forms[0].Submit()) {

            document.getElementById("loadingStatus").innerHTML = "<img id=\"animateStatus\" src='/en-US/images/buynow/_global/loading.gif' width=\"50\" height=\"50\" alt=\"Loading Results...\" title=\"Loading Results...\" /><br />Loading Results...";
            document.getElementById("loadingStatus").style.display = "block";
            //the following time out function resolves IE not animating the image.  Stupid IE.  -MCT
            setTimeout('document.images["animateStatus"].src = "/en-US/images/buynow/_global/loading.gif"', 200);
        }

        return false;
    }

    function clearLoadingImage() {
        var frms = document.getElementsByTagName("form");

        var sels;
        for (var i = 0; i < frms.length; i++) {
            sels = frms[i].getElementsByTagName("hidden");   // loop through the selects in a similar manner  } 
            //alert(i);
        }

        document.getElementById("loadingStatus").style.display = "none";
    }



    //-->
    </script>
</head>
<body>
    <form id="BuyNow" runat="server">
        <div id="boxcontainer-buynow">

            <div id="product-results-content" style="min-height: 500px; padding: 20px">
                <div id="ci-product-description">
                    <!--<h2 style="margin-top: 0;">
                        <asp:Literal ID="litProductName2" runat="server" /></h2>-->

                    <div id="ci-product-image">
                        <asp:Literal ID="litProductImage" runat="server" />
                    </div>

                    <div id="ci-store-locator">
                        <!--<div id="ci-globe"><img src="/images/buynow/WTB-globe.png" width="62" height="65" alt="store locator globe" class="images" /></div>-->
                        <div id="ci-zip-txt-container">
                            <h3>Prefer to shop in a store near you?</h3>
                            <div id="zip-txt">Enter your ZIP code to find in a store near you.</div>

                            <div id="zipText">
                                <asp:TextBox ID="zip" runat="server" MaxLength="5" Text="Enter Zip Code" CssClass="ciText" /></div>
                            <div id="zipButton">
                                <asp:Button ID="zipBtn" runat="server" Text="Submit" CssClass="zipButton" /></div>
                            <asp:HiddenField ID="hfProductSku" runat="server" />
                            <asp:HiddenField ID="hfProductRefName" runat="server" />
                            <div class="clearFloat"></div>
                            <asp:RequiredFieldValidator ID="rfZip" runat="server" ErrorMessage="Zip code required." ControlToValidate="zip" InitialValue="Enter Zip Code" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:Literal ID="litReturnToOnlineRetailer" runat="server"></asp:Literal>
                        </div>
                        <div class="clearFloat"></div>
                    </div>
                </div>
                <asp:Panel ID="OnlineRetailers" runat="server">
                    <div class="ci-results-placeholder">
                        <div class="ci-results">
                            <!-- CII:START -->
                            <script language="javascript" type="text/javascript">
                                //NOTE:  IE8 bug with form tag.  may need to consider iframe.
                                <asp:Literal ID="litCIBuyNow" runat="server"></asp:Literal>
                                //-->
                            </script>
                            <!-- CII:END -->
                        </div>
                    </div>
                    <div class="clearFloat"></div>
                </asp:Panel>
                <asp:Panel ID="BrickAndMortarRetailers" runat="server">
                    <div class="ci-results-placeholder">
                        <div class="ci-results">
                            <div id="divResults">
                                <div id="loadingStatus" style="width: 200px; height: 100px; margin: 180px auto; text-align: center; font-size: 10px; font-family: Arial, Helvetica, sans-serif">
                                    <img id="animateStatus" src="/en-US/images/buynow/loading.gif" width="50" height="50" alt="Loading Results..." title="Loading Results..." /><br />
                                    Loading Results...</div>

                                <script language="JavaScript1.2" type="text/javascript">
                                        <!--
    cii_EmbedPinpointer('kao');
    //-->
                                </script>
                                <div class="clearFloat"></div>
                            </div>
                            <div class="clearFloat"></div>
                        </div>
                    </div>
                    <script language="javascript" type="text/javascript">
                        $(function() {
                            clearLoadingImage();
                        });
                    </script>
                </asp:Panel>
            </div>
            <div class="clear"></div>
        </div>
    </form>
     <script type="text/javascript">
         if (typeof jQuery == 'undefined') {
             var source = '<%= VirtualPathUtility.ToAbsolute("~/js/jquery-1.6.4.min.js") %>';
            document.write(unescape("%3Cscript src='" + source + "' type='text/javascript'%3E%3C/script%3E"));
        }
    </script>
    <script type="text/javascript" src="<%= VirtualPathUtility.ToAbsolute("~/js/tracking.js") %>"></script>
    <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
        try {
            var firstTracker = _gat._getTracker("UA-385129-27"); // Global Tag
            firstTracker._setDomainName("none");
            firstTracker._trackPageview();
            var secondTracker = _gat._getTracker("UA-385129-22"); // Biore MX Tag
            secondTracker._trackPageview();
        } catch (err) { }
    </script>
</body>
</html>


