﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="porespectivesRules.aspx.cs" Inherits="Biore2012.porespectivesRules" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedrules_#.css")
    %>
    
    <style type="text/css">
        .utility #mainContent, #mainContent 
        {
            height:auto;
        }
        
        #privacyinfo li {
    list-style: square none;
    margin-left: 1em;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
        <div id="mainContent">
            <!--<div id="shadow"></div>-->
            <div class="centeringDiv">
                <!--<div id="polaroid"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/forms/signUpPhoto.jpg") %>" alt="" /></div>-->
                <div id="content">
                    <div class="titleh1">
                    <h1>Bioré® Skincare Caribbean Back-to-School 2018 Sweepstakes
</h1>
                        <div id="responseRule"></div>
                    <h2>
Official Rules</h2>
		</div>
                    <div id="privacyinfo">
                    <p><b>NO PURCHASE NECESSARY TO ENTER OR WIN. A PURCHASE DOES NOT IMPROVE YOUR CHANCES OF WINNING. </b></p>

<p><b>This Sweepstakes may only be entered in or from Aruba, Bahamas, Barbados, Bermuda, Curacao, Grand Cayman, Guyana, Jamaica, St. Lucia, and Trinidad & Tobago; entries originating from any other jurisdiction are not eligible for entry. This sweepstakes is governed exclusively by the laws of the United States. You are not authorized to participate in the sweepstakes if you are not located within Aruba, Bahamas, Barbados, Bermuda, Curacao, Grand Cayman, Guyana, Jamaica, St. Lucia, and Trinidad & Tobago.  
</b></p>

                    <p><strong>1. Eligibility.</strong> Participation open only to legal residents of <b>Aruba, Bahamas, Barbados, Bermuda, Curacao, Grand Cayman, Guyana, Jamaica, St. Lucia, and Trinidad & Tobago (each an “Eligible Country”)</b>, who are 13 or older as of date of entry.  Void outside <b>Aruba, Bahamas, Barbados, Bermuda, Curacao, Grand Cayman, Guyana, Jamaica, St. Lucia, and Trinidad & Tobago</b>, and where prohibited, taxed or restricted by law.  Employees, officers and directors of Sponsor and its parent companies, subsidiaries, affiliates, partners, advertising and promotion agencies, manufacturers or distributors of Sweepstakes materials and their immediate families (parents, children, siblings, spouse) or members of the same household (whether related or not) of such employees, officers, and directors are not eligible to enter.  Sweepstakes may only be entered in or from <b>Aruba, Bahamas, Barbados, Bermuda, Curacao, Grand Cayman, Guyana, Jamaica, St. Lucia, and Trinidad & Tobago</b>, and entries originating from any other jurisdiction are not eligible for entry.  All federal, state and local laws and regulations apply.</p>
                                        
                    <p><strong>2. How to Enter.</strong> Between 9:01 AM Pacific Time (“PT”) and 11:59 PM PT on August 8, 2018 (“Sweepstakes Period”), eligible entrants must enter via Facebook as described below:</p>
a.	Log in to or create your Facebook account and follow @biorecaribbean.</br></br>
b.	See Back-to-School Contest post on August 8, 2018 with instructions posted by Sponsor [https://www.facebook.com/biorecaribbean] (“Graphic Post”) </br></br>
c.	Watch the video graphic and identify under which shell the pore strip is hidden. Comment under the Graphic Post, answering “Where is the pore strip?” Tag a friend.  Include your name and Eligible Country of residence <b>(“Entry”)</b>.</br></br>

<p>You will automatically receive one (1) Entry into the Sweepstakes drawing for the country specified in the Entry.</p>

<p>Limit one (1) entry in Sweepstakes per person, per Facebook account. No automated entry devices and/or programs permitted. All entries become the sole exclusive property of the Sponsor and receipt of entries will not be acknowledged or returned. Delivery of prizes required a street address (no P.O. Boxes). Sponsor is not responsible for lost, late, illegible, stolen, incomplete, invalid, unintelligible, misdirected, postage-due, technically corrupter or garbled entries or mail, which will be disqualified, or for problems of any kind whether mechanical, human or electronic. Only fully completed entry forms are eligible. Proof of submission will not be deemed to be proof of receipt by Sponsor. By entering the Sweepstakes, entrants fully and unconditionally agree to be bound by these rules, which will be final and binding in all matters relating to the Sweepstakes.</p>

<p><strong>3. Random Drawings/Odds. </strong> One (1) winner will be selected in a random drawing from all eligible entries received during the Sweepstakes Period for each of the Eligible Countries: <b>Aruba, Bahamas, Barbados, Bermuda, Curacao, Grand Cayman, Guyana, Jamaica, St. Lucia, and Trinidad & Tobago</b>.  The drawings will be held on or about August 9, 2018. Odds of winning depend on the number of eligible entries received for the drawing per country.  By entering the Sweepstakes, entrants fully and unconditionally agree to be bound by these rules and the decisions of the Sponsor, which will be final and binding in all matters relating to the Sweepstakes.</p>

<p><strong>4.	Prizes.</strong> The first Entry drawn for each Eligible Country will receive the Prize which consists of the following:</p>

<p>1 ea. Bioré® Charcoal Cleansing Micellar Water, 1 ea. Bioré® Charcoal Acne Clearing Cleanser, 1 ea. Bioré® Charcoal Acne Scrub, 1 ea. Bioré® Deep Cleansing Charcoal Pore Strips, 1 ea. Bioré® Blemish Fighting Astringent. $8.25 USD average retail value per item; $41.25 USD average retail value per prize). Local retail value of prize may vary by country.</p>

<p>Prizes are non-transferable.  No substitutions or cash redemptions.  In the case of unavailability of any prize, Sponsor reserves the right to substitute a prize of equal or greater value.  Prize Winners will not receive difference between actual and approximate retail value set forth in these Official Rules. All taxes and unspecified expenses are the responsibility of winners. </p>

<p><strong>5. Prize Delivery. </strong> Receiving a prize is contingent upon compliance with these Official Rules.  Winner will be notified via Facebook within 24 hours after the drawing.  Except where prohibited, the winner must respond to the Facebook post from Sponsor within three (3) days of the date notice or attempted notice is sent in order to claim the prize. Winner will be required to provide a valid mailing address to Sponsor to which the prize can be delivered.  Sponsor retains the right to confirm any winner’s eligibility to participate and will disqualify any winner whose mailing address does not correspond to the country identified in the Entry. If a winner cannot be contacted and does not provide a valid mailing address within 3 days of the Sponsor’s notice/attempted notice or if prize is returned as undeliverable, the winner forfeits his/her prize and the prize will be awarded to the next qualified entrant for that drawing. Upon Prize forfeiture, no compensation will be given.  By entering the Sweepstakes, entrants fully and unconditionally agree to be bound by these rules which will be final and binding in all matters relating to the Sweepstakes.  Allow four (4) to six (6) weeks for delivery. Sponsor not responsible if the Prize cannot be delivered due to an incorrect mailing address or for lost or misdirected shipments.</p>

<p><strong>6.	Conditions.</strong> All federal, state and local taxes are the sole responsibility of the winners. Participation in Sweepstakes and acceptance of prize constitutes winner’s permission for Sponsor to use his/her name, address (city and state), likeness, photograph, picture, portrait, voice, biographical information and/or any statements made by each winner regarding the Sweepstakes or Sponsor for advertising and promotional purposes without notice or additional compensation, except where prohibited by law. By participating, entrants and winner agree to release and hold harmless Sponsor, its advertising and promotion agencies and their respective parent companies, subsidiaries, affiliates, partners, representatives, agents, successors, assigns, employees, officers and directors (collectively, the “Released Entities”), from any and all liability, for loss, harm, damage, injury, cost or expense whatsoever including without limitation, property damage, personal injury and/or death which may occur in connection with, preparation for, travel to, or participation in Sweepstakes, or possession, acceptance and/or use or misuse of prize or participation in any Sweepstakes-related activity and for any claims based on publicity rights, defamation or invasion of privacy and merchandise delivery.  Sponsor is not responsible if Sweepstakes cannot take place or if any prize cannot be awarded due to travel cancellations, delays or interruptions due to acts of God, acts of war, natural disasters, weather or acts of terrorism.  Entrants who do not comply with these Official Rules, or attempt to interfere with this Sweepstakes in any way shall be disqualified.  There is no purchase or sales presentation required to participate. A purchase does not increase odds of winning.   </p>

<p><strong>7.	Additional Terms.</strong> In case of dispute as to the identity of any entrant, Entry will be declared made by the authorized account holder of the email address submitted at the time of entry. “Authorized Account Holder” is defined as the natural person who is assigned an email address by an Internet access provider, online service provider, or other organization (e.g. business, educational, institutional, etc.) responsible for assigning email addresses or the domain associated with the submitted email address. Any Potential Winner may be requested to provide Sponsor with proof that such winner is the authorized account holder of the email address associated with the winning entry. Any other attempted form of entry is prohibited; no automatic, programmed, robotic or similar means of entry are permitted. Sponsor, its affiliates, partners and promotion and advertising agencies are not responsible for technical, hardware, software, telephone or other communication malfunctions, errors or failures of any kind, lost or unavailable network connections, web site, internet, or ISP availability, unauthorized human intervention, traffic congestion, incomplete or inaccurate capture of entry information (regardless of cause) or failed, incomplete, garbled, jumbled or delayed computer transmissions which may limit one’s ability to enter the Sweepstakes, including any injury or damage to participant’s or any other person’s computer relating to or resulting from participating in this Sweepstakes or downloading any materials in this Sweepstakes. </p>

<p>Sponsor reserves the right, in its sole discretion, to cancel, terminate, modify, extend or suspend this Sweepstakes should (in its sole discretion) virus, bugs, non-authorized human intervention, fraud or other causes beyond its control corrupt or affect the administration, security, fairness or proper conduct of the Sweepstakes. In such case, Sponsor may select the winner(s) from all eligible entries received prior to and/or after (if appropriate) the action taken by Sponsor. Sponsor reserves the right, at its sole discretion, to disqualify any individual it finds, in its sole discretion, to be tampering with the entry process or the operation of the Sweepstakes or web site.  Sponsor may prohibit an entrant from participating in the Sweepstakes or winning a prize if, in its sole discretion, it determines that said entrant is attempting to undermine the legitimate operation of the Sweepstakes by cheating, hacking, deception, or other unfair playing practices (including the use of automated quick entry programs) or intending to annoy, abuse, threaten or harass any other entrants or Sponsor representatives.
<i><b>CAUTION: ANY ATTEMPT BY AN ENTRANT TO DELIBERATELY DAMAGE ANY WEB SITE OR UNDERMINE THE LEGITIMATE OPERATION OF THE SWEEPSTAKES MAY BE A VIOLATION OF CRIMINAL AND CIVIL LAWS AND SHOULD SUCH AN ATTEMPT BE MADE, THE SPONSOR RESERVES THE RIGHT TO SEEK DAMAGES FROM ANY SUCH PERSON TO THE FULLEST EXTENT PERMITTED BY LAW.</b>,</i></p>

<p><strong>8.	Limitation of Liability; Disclaimer of Warranties.</strong> IN NO EVENT WILL THE RELEASED ENTITIES BE RESPONSIBLE OR LIABLE FOR ANY DAMAGES OR LOSSES OF ANY KIND, INCLUDING DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL OR PUNITIVE DAMAGES ARISING OUT OF YOUR ACCESS TO AND USE OF THE SERVICE AND/OR THE SWEEPSTAKES, DOWNLOADING FROM AND/OR PRINTING MATERIAL DOWNLOADED FROM ANY WEBSITES ASSOCIATES WITH THE SWEEPSTAKES. IN NO EVENT SHALL THE RELEASED ENTITIES’ TOTAL LIABILITY TO YOU FOR ALL DAMAGES, LOSSES, OR CAUSES OF ACTION EXCEED $10. WITHOUT LIMITING THE FOREGOING, THIS SWEEPSTAKES AND ALL PRIZES ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT. SOME JURISDICTIONS MAY NOT ALLOW THE LIMITATIONS OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES OR EXCLUSION OF IMPLIED WARRANTIES SO SOME OF THE ABOVE LIMITATIONS OR EXCLUSIONS MAY NOT APPLY TO YOU. CHECK YOUR LOCAL LAWS FOR ANY RESTRICTIONS OR LIMITATIONS REGARDING THESE LIMITATIONS OR EXCLUSIONS.</p>

<p><strong>9.	Use of Data.</strong> Any entry information collected from the Sweepstakes form shall be used only in a manner consistent with these Official Rules and with the http://www.moltonbrown.com/store/privacy-policy.  By participating in the Sweepstakes, entrants hereby agree to Sponsor's collection and usage of their personal information in accordance with Sponsor's privacy policy and acknowledge that they have read and accepted Sponsor's privacy policy located at http://www.biore.com.mx/privacy/.</p>

<p><strong>10. List of Winners. </strong> To obtain a list of winners, send a self-addressed, stamped envelope by September 8, 2018 to: Chinelo Entertainment S.A. de C.V., Malaga Norte 18, Colonia Extremadura Insurgentes, Delegacion Benito Juarez, D.F. 03740 MEXICO.</p> 
 
<p><strong>11. Sponsor</strong> Kao USA Inc., 2535 Spring Grove Avenue, Cincinnati, Ohio 45214. Administrator:  Chinelo Entertainment S.A. de C.V., Malaga Norte 18, Colonia Extremadura Insurgentes, Delegacion Benito Juarez, D.F. 03740 MX.</p>

<p><b>This Sweepstakes is in no way sponsored, endorsed or administered by, or associated with Facebook, LLC. Information is being provided to the Sponsor (defined below), not to Facebook. Any questions, comments or complaints regarding the Sweepstakes must be directed to the Sponsor and not to Facebook. Your participation in this Sweepstakes must at all times comply with all applicable Facebook terms of service. In the event of any violation of such terms of service, the Sponsor may, in its sole and absolute discretion, disqualify you from the Sweepstakes.</b></p>

                 </div>
		</div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
