﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="porespectivesRules.aspx.cs" Inherits="Biore2012.porespectivesRules" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedrules_#.css")
    %>
    
    <style type="text/css">
        .utility #mainContent, #mainContent 
        {
            height:auto;
        }
        
        #privacyinfo li {
    list-style: square none;
    margin-left: 1em;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
        <div id="mainContent">
            <!--<div id="shadow"></div>-->
            <div class="centeringDiv">
                <!--<div id="polaroid"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/forms/signUpPhoto.jpg") %>" alt="" /></div>-->
                <div id="content">
                    <div class="titleh1">
                    <h1>The “New Ambassador for Bioré Skincare in the Caribbean” Contest
</h1>
                        <div id="responseRule"></div>
                    <h2>
Official Rules</h2>
		</div>
                    <div id="privacyinfo">
                    <p><b>NO PURCHASE NECESSARY TO ENTER OR WIN. A PURCHASE DOES NOT IMPROVE YOUR CHANCES OF WINNING. </b></p>

<p><b>Contest may only be entered in or from the countries of Antigua, Aruba, Bahamas, Barbados, Bermuda, Cayman Islands, Curaçao, Guyana, Haiti, Jamaica, St. Lucia, St. Maarten, and Trinidad & Tobago. Entries originating from any other jurisdiction are not eligible for entry.  This Contest is governed exclusively by the laws of the United States.  You are not authorized to participate in the Contest if you are not located within Antigua, Aruba, Bahamas, Barbados, Bermuda, Cayman Islands, Curaçao, Guyana, Haiti, Jamaica, St. Lucia, St. Maarten, and Trinidad & Tobago. By participating in the Contest, entrant fully and unconditionally agrees to and accepts these Official Rules and the decisions of the Sponsor, which are final and binding in all matters related to the Contest.  
</b></p>

                    <p><strong>1. Eligibility.</strong> Participation in the New Face of Bioré Skincare (“Contest”) is open only to legal residents of Antigua, Aruba, Bahamas, Barbados, Bermuda, Cayman Islands, Curaçao, Guyana, Haiti, Jamaica, St. Lucia, St. Maarten, and Trinidad & Tobago who are between the ages of fifteen (15) and thirty (30) at the time of entry and who did not purchase any equipment or products for purposes of entering the Contest.  Employees of Kao USA Inc., Chinelo Estudio Creativo S.A. de C.V. and any of their respective related companies, parents, subsidiaries, affiliates, and agents and any agencies or other companies involved in the development or execution of the Contest or production or distribution of Contest materials, as well as the immediate family (spouse, parents, siblings, and children) and household members of each such employee are not eligible.  The Contest is subject to all applicable federal, state, and local laws and regulations.  The Contest is void where prohibited.  </p>
                                         
                    <p><strong>2. Start/End Dates.</strong> Entry period begins at 9:00am Daylight Savings Time ("DST") on October 2, 2017 and ends at 05:00pm DST on October 13, 2017 (“Contest Period”). </p>
                 
                    <p><strong>3. How to Enter.</strong> To enter the Contest, eligible entrants should upload one (1) 15 to 20 second video to Bioré Caribbean Facebook page as a video comment (the “Video”) (i) stating their first name and country; (ii) identifying their favorite Bioré Skincare product (“Contest Theme”); (iii) stating why they should be selected the Bioré Ambassador for the Caribbean; and (iv) including the hashtag #BioreCaribbeanContest.</p>

<p>Entry into the Contest is by this process only.  Failure to hashtag the submitted Video with #BioreCaribbeanContest will result in an invalid entry and the Video will not be judged as part of the Contest. </p>

<p>All Videos must comply with the entry requirements above and Video Content Restrictions (as defined below), as determined by Sponsor in its sole and absolute discretion.  Sponsor reserves the right to cancel or modify this Contest in the event an insufficient number of entries are received that meet the judging criteria as set out in paragraph 6 of these Official Rules.  </p>

<p><b>By entering, each entrant warrants and represents the following with respect to his/her Video: (a) entrant is the sole and exclusive owner of the Video; (b) the Video will not infringe on any rights of any third party, including but not limited to copyright, trademark, privacy, publicity and/or any other intellectual property right; and (c) the Video complies with the Video Content Restrictions (defined below). </b></p>

<p>Sponsor is not responsible for lost, late, illegible, stolen, incomplete, invalid, unintelligible, postage-due, misdirected, technically corrupted or garbled entries, which will be disqualified, or for problems of any kind whether mechanical, human or electronic.  </p>

<p><strong>4. Video Content Restrictions.</strong> By entering the Contest, each entrant agrees that his or her Video conforms to the Video Content Restrictions as defined below and that Sponsor, in its sole discretion, may remove any Video and disqualify an entrant from the Contest if it believes, in its sole discretion, that the entrant’s Video fails to conform to these Video Content Restrictions: </p>

<ul>
<li><p>The Video must not contain material that violates or infringes another’s rights, including but not limited to copyright, trademark, privacy, publicity or any other intellectual property rights;</p></li> 
<li><p>The Video must not disparage Sponsor, any other person or party affiliated with the promotion and administration of this Contest, or competitors of Sponsor;</p></li>
<li><p>The Video must not contain brand names or trademarks of any entity (including, but not limited to, those of any Competitor) other than the Sponsor’s Marks (defined below), which entrant has a limited license to use for the sole purpose of creating and uploading a Video in this Contest;</p></li>
<li><p>The Video must not contain material that is inappropriate, indecent, obscene hateful, tortious, defamatory, slanderous or libelous;</p></li>
<li><p>The Video must not contain material that promotes bigotry, racism, hatred or harm against any group or individual or promotes discrimination based on race, gender, religion, nationality, disability, sexual orientation or age; and</p></li>
<li><p>The Video must not contain material that is unlawful, in violation of or contrary to the laws or regulations in any jurisdiction where Video is created.</p></li>
</ul>
<p>
Entrants acknowledge that other entrants may have used ideas and/or concepts in their Video that may have similarities to ideas or concepts included in another entrant’s Video, and entrants understand and agree that they shall not in any way be entitled to any compensation because of any such similarities.  Sponsor’s decisions are final and binding in all matters relating to this Contest, including interpretation and application of these Official Rules, and no correspondence will be entered into.</p>


<p><strong>5. Authorization.</strong> BY ENTERING THIS CONTEST THROUGH THE UPLOAD OF A VIDEO TO THE FACEBOOK APPLICATION WITH THE HASHTAG #BIORECARIBBEANCONTEST, ENTRANT IRREVOCABLY AUTHORIZES AND ACNKOWLEDGES HIS/HER CONSENT WITH THE VIDEO/IMAGE CONTAINED THEREIN BEING UPLOADED IN THE BIORE’S FACEBOOK GALLERY DURING A 6 MONTH TERM COUNTED FROM THE CLOSING DATE OF RELEVANT CONTEST (October 13, 2017). In any case, Bioré will include relevant mechanisms for participants to request that their image be taken down from the mentioned gallery.  Sponsor grants eligible entrants a limited, revocable, non-sublicensable, license to use Sponsor’s name, trademarks and logos (collectively, “Sponsor’s Marks”) for the sole purpose of creating and submitting the Video for review and assessment in this Contest.  Entrants are not permitted to make any further use of Sponsor’s Marks for any purpose whatsoever.  In addition, entrants recognize that all right, title, and interest in Sponsor’s Marks as well as all derivative works created using Sponsor’s Marks shall vest exclusively to the Sponsor, and entrant agrees that he/she has not and will not take any action that might harm or adversely affect such rights.  No right, title, or interest in and to the Sponsor’s Marks except for the limited license granted to entrant in these Official Rules is transferred or created. Each entrant further acknowledges and agrees he/she shall do nothing to challenge the validity or enforceability of, or otherwise interfere with, Sponsor’s Marks in any forum.  Entrants agree that the use of Sponsor’s Marks is permitted only for the purpose of making a Video for entry in this Contest, and that any use of Sponsor’s Marks (whether in the Video or otherwise) beyond this scope infringes the rights of Sponsor and will result in irreparable harm to Sponsor.</p>

<p><strong>6. Judging Panel.</strong> All valid Videos received during the Contest Entry Period will be judged.  The Videos will be judged between October 16, 2017 and October 20, 2017 at 312 Plum Street, Cincinnati, OH 45201, USA by a panel of qualified judges determined by Sponsor in its sole and absolute discretion (“Judging Panel”).  The Judging Panel will judge all of the eligible Videos based on the following judging criteria (“Judging Panel Criteria”): </p>
<ul>
<li><p>Creativity/Originality (33%) </p></li>
<li><p>Fit with Contest Theme (33%) </p></li>
<li><p>Fit with Brand Identity (34%)</p></li>
</ul>

<p>In the event of a tie, Sponsor, in its sole discretion, will determine the Potential Winner based on the Video that received the highest score in the Fit with Brand Identity category.</p>

<p>Subject to verification and compliance with these Official Rules, the highest scoring Video will be deemed the Winner.  This is a game of skill and chance plays no part in determining the Potential Winner. </p>

<p><strong>7. Potential Winner Prize.</strong> ONE (1) PRIZE:  The Grand Prize Winner will win a cash prize valued at USD$2,000 (to be paid by check or direct deposit, at Sponsor’s sole discretion); the opportunity to participate in a professional photo & video shoot for the Bioré Skincare brand during the 2017 calendar year and, at Sponsor’s discretion, be featured in Caribbean region promotional materials and Caribbean social media channel(s) for the Bioré Skincare brand (Sponsor is under no obligation to use any assets derived from the professional photo/video shoot for commercial purposes); and a year’s supply of Bioré Skincare products including 12 Bioré Charcoal Acne Clearing Cleansers, 12 Bioré Charcoal Acne Scrubs, and 12 boxes of 6 count Bioré Deep Cleansing Charcoal Pore Strips).  Approximate Retail Value (“ARV”): $4,742 USD.  Winner will not receive difference between actual and approximate retail value. </p>

<p>FOR SAKE OF CLARITY, ENTRANTS UNDERSTAND AND AGREE THAT EVEN IF ENTRANT SUBMITS A PRIZE WINNING VIDEO, SPONSOR IS UNDER NO OBLIGATION TO USE THE WINNING VIDEO IN ANY MANNER OR FOR ANY PURPOSE.  ENTRANTS FURTHER UNDERSTAND AND AGREE THAT SPONSOR IS UNDER NO OBLIGATION TO USE ANY ASSETS DERIVED FROM THE PROFESSIONAL PHOTO/VIDEO SHOOT FOR COMMERCIAL PURPOSES.
</p>

<p><strong>8. Notification.</strong> The potential winner be contacted by Sponsor via a comment on the Potential Winner’s Video and will be required to sign and return a Confirmation of Eligibility, Liability/Publicity Release and/or rights transfer document, as required and directed by the Sponsor, within seven (7) calendar days of prize notification (i.e. on or before October 27, 2017).  If any Potential Winner cannot be contacted within seven (7) calendar days of first notification attempt, if any prize or prize notification is returned as undeliverable, if any Potential Winner rejects his/her prize or in the event of noncompliance with these Official Rules or any other requirements of the Sponsor, that potential Winner will forfeit his/her prize and that prize and will be awarded to Video with the next highest score, as judged by the Judging Panel in accordance with the Judging Panel Criteria. </p>

<p><strong>9. Conditions.</strong> Sponsor shall not be liable or responsible in the event any entrant or winner’s Video is not used for any reason.  Any and all federal, state and local taxes are the sole responsibility of the winner.  Participation in Contest and acceptance of any prize constitutes each winner’s permission for Sponsor to use his/her name, address (city, state, country), likeness, video, photograph, picture, portrait, voice, biographical information, as described in paragraph 5.  By participating, entrants and winner agrees to release, indemnify, waive, discharge, absolve and hold harmless Sponsor, its partners and promotion and advertising agencies and each of their respective parent companies, subsidiaries, affiliates, partners, representatives, agents, successors, assigns, employees, officers and directors (collectively, the “Released Entities”), from any and all liability, for loss, harm, damage, injury, cost or expense whatsoever including without limitation, property damage, personal injury and/or death which may occur in connection with, preparation for, travel to, or participation in Contest, or possession, acceptance and/or use or misuse of prize or participation in any Contest-related activity and for any claims based on publicity rights, defamation, invasion of privacy, copyright infringement, trademark infringement, moral rights infringement or any other intellectual property-related cause of action.  Entrants who do not comply with these Official Rules, or attempt to interfere with this Contest in any way shall be disqualified. Sponsor is not responsible if Contest cannot take place or if any prize cannot be awarded due to travel cancellations, delays or interruptions due to acts of God, acts of war, natural disasters, weather or acts of terrorism. If any Winner cannot participate in a prize or any part of a prize for any reason then that prize will be void and no compensation will be payable. </p>

<p><strong>10. Additional terms.</strong> The Sponsor's decision is final and binding on all matters relating to this Contest and no correspondence will be entered into.  Any attempted form of entry other than as set forth above is prohibited. The Released Entities are not responsible for technical, hardware, software, telephone or other communications malfunctions, errors or failures of any kind, lost or unavailable network connections, web site, Internet, or ISP availability, unauthorized human intervention, traffic congestion, incomplete or inaccurate capture of entry information (regardless of cause) or failed, incomplete, garbled, jumbled or delayed computer transmissions which may limit one’s ability to enter the Contest, including any injury or damage to participant’s or any other person’s computer relating to or resulting from participating in this Contest or downloading any materials in this Contest.  Sponsor reserves the right, in its sole discretion, to cancel, terminate, modify, extend or suspend this Contest should (in its sole discretion) virus, bugs, non-authorized human intervention, fraud or other causes beyond its control corrupt or affect the administration, security, fairness or proper conduct of the Contest. Sponsor reserves the right, at its sole discretion, to disqualify or refuse to provide a prize to any individual it finds, in its sole discretion, to have tampered with the entry process or the operation of the Contest, provided false or misleading details, or behaved in a fraudulent or dishonest manner or otherwise than in accordance with these terms and conditions or the spirit of the Contest.  Sponsor may prohibit an entrant from participating in the Contest or winning a prize if, in its sole discretion, it determines that said entrant is attempting to undermine the legitimate operation of the Contest by cheating, hacking, deception, or other unfair playing practices (including the use of automated quick entry programs) or intending to annoy, abuse, threaten or harass any other entrants or Sponsor representatives.  Sponsor also reserves the right to request that a Potential Winner provides proof of his/her identity, residency and/or age prior to awarding a prize.  Proof of identification, residency and/or age considered suitable for verification is at the discretion of the Sponsor.  In the event that a Potential Winner cannot provide suitable proof, he/she will forfeit the applicable prize and no substitute will be offered. </p> 
 
<p>CAUTION: ANY ATTEMPT BY AN ENTRANT TO DELIBERATELY DAMAGE ANY WEB SITE OR UNDERMINE THE LEGITIMATE OPERATION OF THE CONTEST MAY BE A VIOLATION OF CRIMINAL AND CIVIL LAWS AND SHOULD SUCH AN ATTEMPT BE MADE, THE SPONSOR RESERVES THE RIGHT TO SEEK DAMAGES FROM ANY SUCH PERSON TO THE FULLEST EXTENT PERMITTED BY LAW.</p>

<p><strong>11. Limitation of Liability.</strong> IN NO EVENT WILL THE RELEASED ENTITIES BE RESPONSIBLE OR LIABLE FOR ANY DAMAGES OR LOSSES OF ANY KIND, INCLUDING DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL OR PUNITIVE DAMAGES ARISING OUT OF ENTRANTS' PARTICIPATION IN THE CONTEST, DOWNLOADING FROM AND/OR PRINTING MATERIAL DOWNLOADED FROM ANY WEBSITES ASSOCIATED WITH THE CONTEST, OR AS THE RESULT OF ACCEPTING OR USING ANY PRIZE.  </p>

<p><strong>12. Disputes; Governing Law.</strong> THESE OFFICIAL RULES AND THE INTERPRETATION OF ITS TERMS SHALL BE GOVERNED BY AND CONSTRUED IN ACCORDANCE WITH THE LAWS OF OHIO, U.S.A. AND ENTRANTS SUBMIT TO THE JURISDICTION OF THE COURTS OF OHIO, U.S.A. FOR ANY PROCEEDINGS IN CONNECTION WITH THE CONTEST AND / OR THESE OFFICIAL RULES.</p>

<p><strong>13. Use of Data.</strong> Sponsor will be collecting personal data about entrants online, in accordance with its privacy policy. Please review the Sponsor’s privacy policy at http://www.biore.com/privacy/.  By participating in the Contest, entrants hereby agree to Sponsor’s collection and usage of their personal information and acknowledge that they have read and accepted Sponsor’s privacy policy.</p>



<p><strong>14. Sponsor:</strong> Kao USA Inc., 2535 Spring Grove Ave, Cincinnati, OH 45214, USA.  Administrator:  Chinelo Estudio Creativo S.A. de C.V., Malaga Norte 18, Colonia Extremadura Insurgentes, Delegacion Benito Juarez, D.F. 03740 MX.</p>

<p>This Contest is in no way sponsored, endorsed or administered by, or associated with Facebook.</p>

<p>Abbreviated Rules:</p>

<p>NO PURCHASE NECESSARY. Open to legal residents of Antigua, Aruba, Bahamas, Barbados, Bermuda, Cayman Islands, Curaçao, Guyana, Haiti, Jamaica, St. Lucia, St. Maarten, and Trinidad & Tobago ages 15-30. Contest ends October 13, 2017. To enter, upload one (1) 15 to 20 second video to Bioré Caribbean Facebook page as a video comment (the “Video”) (i) stating their first name and country; (ii) identifying their favorite Bioré Skincare product (“Contest Theme”); (iii) stating why they should be selected the Bioré Ambassador for the Caribbean; and (iv) including the hashtag #BioreCaribbeanContest.</p>
<p>
All valid Videos will be judged.  For Official Rules, including prize descriptions, visit http://biore.com.mx/biorecaribbeancontestrules.  Void where prohibited.</p>

                 </div>
		</div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
