﻿<%@ Page Title="About Bior&eacute;&reg; Skincare | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="/Site.Master" AutoEventWireup="true" CodeBehind="iframe-test.aspx.cs" Inherits="Biore2012.iframetest._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Learn more about Bioré® Skincare" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>

    <style type="text/css">
        .iframeWrapper {
	position: relative;
	padding-bottom: 100%;
	padding-top: 25px;
	height: 0;
}
.iframeWrapper iframe {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div class="centeringDiv">
                <div id="content">
                    <div class="iframeWrapper">
                        <iframe src="http://www.guilsys-networks.com/biore" id="iframetest" width="1024" height="840" frameborder="0" allowfullscreen="allowfullscreen" scrolling="no"></iframe>
                     </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

