﻿<%@ Page Title="Obtén un rostro limpio | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Obtén una piel saludable y radiante con productos Bioré® Skincare" name="description" />
    <meta content="Skincare, rostro limpio" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/homepage.css")
        .Add("~/css/flexslider.css")
        .Add("~/css/UniversPro67.css")
        .Render("~/CSS/combinedhome_#.css")
    %>
    <meta name="viewport" content="width=device-width">

    <script type="text/javascript">
         $(function() {
             $('.fma1Find a').click(
                 function (e) {
                     e.preventDefault();

                     var axel = Math.random() + "";
                     var a = axel * 10000000000000;
                     var strScript = "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore551;ord='" + a + "'?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"                     
                                + "<noscript>"
                                + "<iframe src=\"http:\/\/4133684.fls.doubleclick.net\/activityi;src=4133684;type=biore530;cat=biore551;ord=1?\" width=\"1\" height=\"1\" frameborder=\"0\" style=\"display:none\"></iframe>"
                                + "</noscript>";

                     $('body').prepend(strScript);
                     var target = $(this).attr('target');
                     var uri = $(this).attr('href');

                     setTimeout(function () {
                         if (target) {
                             window.open(uri, '_blank');
                         } else {
                             window.location = uri;
                         }
                     }, 1000);
                     
                 });
         })
    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main1">
    <div class="home-content">
            <section id="section-2017-interim">
                <div class="top-baking-soda-2017">
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/top-right-bubbles.png") %>" alt="top right bubbles" /></div>
                <div class="welcome-module clearfix">
					<div style="position: absolute; margin: 70px auto 0; text-align: center; width: 100%; cursor: pointer;"><a href="/biore-facial-cleansing-products/"><img src="images/spacer.gif" height="510" width="300" /></a></div>
                     <div class="header-2017">
                        <div class="acne-header-2017">
                            <h2>
                                <span class="acnes foamparty">Kiss Your <span class="cateye">Contour-<br />Cat-Eye-Lip-Kit</span></span>
                              
                            </h2>
                            <h2 style="margin-top: 14px;">
                                <span class="acnessmall">Goodbye!</span>

                            </h2>
                        </div>
                       
                    </div>

                    <div class="left-text">
                        <div>
                           <span class="new">New </span> Bior&eacute;<sup>&reg;</sup><br />Cleansing<br />Micellar Waters

                        </div>

                        <!--<div class="allInOneCleansing">All-in-One Cleansing!</div>-->

                    </div>

                     <div class="right-text">
                         <div>
                         All-in-one cleanser<br />and makeup remover
                              <span class="waterproof"> Your Pores<br />(And Pillowcase)<br />Will Thank You</span>

                         </div>

                     </div>

                     <div class="footer-2017">
                        <div class="acne-footer-2017">
                           <!-- *Garnier Skin Active Micellar Water All-In-1 Waterproof, the leading Micellar Water according to Neilsen Scanning Data, 52 Weeks Ending April 2018-->
                        </div>
                    </div>


                    <div class="hide-mobile"></div>
                    <!--<div class="scrollDownHldr">
                        <p class="scrollDown">SCROLL DOWN</p>
                        <div id="arrowBounce">
                            <a href="#section-2018-products">
                                <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/arrowDown.png") %>" class="section-down-arrow" /></a>
                        </div>

                    </div>-->
                </div>

               <!-- <div class="scroll-dots">
                    <a href="#section-2017-interim"><span class="scroll-dot enable"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot"></span></a>
                    <a href="#section-01"><span class="scroll-dot"></span></a>
                    <a href="#section-04"><span class="scroll-dot"></span></a>
                    <a href="#section-05"><span class="scroll-dot"></span></a>
                    <a href="#section-03"><span class="scroll-dot"></span></a>
                    <a href="#section-02"><span class="scroll-dot"></span></a>
                </div>-->
                <div class="bottom-charcoal-2017">
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/bottom-left-bubbles.png") %>" alt="bottom left bubbles" /></div>
            </section>
        
           <!-- <section class="home-section" id="section-2018-products">
                <h4>Piel más limpia en solo 2 días. Con limpiadores de imperfecciones Bioré con bicarbonato de sodio y carbón natural.</h4>
                <div class="container3">

                    <div class="prodList">
                        
                        <ul>
                            <li class="productListSpacer">
                                <img src="images/ourProducts/products/small/baking-soda-acne-scrub.png" alt="" />
                                <h3>EXFOLIADOR CON BAKING SODA PARA PIEL CON GRANOS Y ESPINILLAS</h3>
                                <p>Con bicarbonato de sodio natural y ácido salicílico, esta crema exfoliante limpia profundamente y exfolia con suavidad <a href="/acnes-outta-here/baking-soda-acne-scrub" class="buynowbtn"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/acne-products-button.png") %>" alt="button"/></a></p>

                                
                                
                            <li class="productListSpacer">
                                <img src="images/ourProducts/products/small/charcoal-acne-scrub.png" alt="" />
                                <h3>EXFOLIADOR CON CARBÓN NATURAL PARA PIEL CON GRANOS Y ESPINILLAS</h3>
                                <p>Con carbón natural y ácido salicílico, exfolia la suciedad que causa imperfecciones y absorbe el exceso de grasitud
 <a href="/acnes-outta-here/charcoal-acne-scrub" class="buynowbtn"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/acne-products-button.png") %>" alt="button"/></a></p>

                    
                            </li>
                            <li class="productListSpacer">
                                <img src="images/ourProducts/products/small/charcoal-acne-clearing-cleanser.png" alt="" />
                                <h3>LIMPIADOR CON CARBÓN NATURAL PARA PIEL CON GRANOS Y ESPINILLAS</h3>
                                <p>Con carbón natural y ácido salicílico, limpia profundamente, penetra en los poros y absorbe el exceso de grasitud <a href="/acnes-outta-here/charcoal-acne-clearing-cleanser" class="buynowbtn"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/acne-products-button.png") %>" alt="button"/></a></p>

                         
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="scroll-dots">
                    <a href="#section-2017-interim"><span class="scroll-dot"></span></a>
                    <a href="#section-2018-products"><span class="scroll-dot enable"></span></a>
                </div>
            </section>-->
    </div>
  
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/jquery.flexslider.min.js")
        .Render("~/js/combinedhome_#.js")
    %>
</asp:Content>