﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Biore2012.BLL;
using System.Text;
using System.Threading;
using System.Web.UI.HtmlControls;

//New Floodlight tags from KBS replaced Enlighten Floodlight tags 6/10/13 -MCT
//Add alt tag rules 6/12/13 -MCT
//Add Micellar Water products - 8/8/18 -MCT

namespace Biore2012.our_products
{
    public partial class ProductDetail : RoutablePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            // Grab masterpage, set appropriate body classes
            Site myMaster = (Site)this.Master;
            myMaster.bodyClass += " ourProducts productDetail";

            // configure prove it link for mobile
            BioreUtils bu = new BioreUtils();
            //bu.configureProveItLinks(myMaster.isMobile, proveItPromoLink);

            // Fetch product based on route used to reach this page.
            Product theProduct = new Product();
            String pid = "";
            if (Routing.Value("pid") != null)
            {
                pid = Routing.Value("pid").ToString();
                // Make sure we aren't showing a pore strip page.
                if (pid == "deep-cleansing-pore-strips-combo" ||
                        pid == "deep-cleansing-pore-strips-ultra" ||
                        pid == "deep-cleansing-pore-strips-ultra-combo" ||
                        pid == "deep-cleansing-pore-strips")
                    Response.Redirect(VirtualPathUtility.ToAbsolute("~/breakup-with-blackheads/pore-strips"));
                else
                    theProduct = ProductDAO.GetProduct(pid);
            }
            else
            {
                if (Request.Url.ToString().Contains("breakup-with-blackheads/pore-strips"))
                {
                    // We're on the porestrips family page.
                    thePoreStripNavPanel.Visible = true;
                    theProduct = ProductDAO.GetProduct("deep-cleansing-pore-strips");
                    PoreStripProduct theRegularProduct = PoreStripProductDAO.GetProduct("deep-cleansing-pore-strips");
                    PoreStripProduct theUltraProduct = PoreStripProductDAO.GetProduct("deep-cleansing-pore-strips-ultra");
                    PoreStripProduct theComboProduct = PoreStripProductDAO.GetProduct("deep-cleansing-pore-strips-combo");
                    PoreStripProduct theUltraComboProduct = PoreStripProductDAO.GetProduct("deep-cleansing-pore-strips-ultra-combo");

                    // Write the JSON for all three to the page.
                    thePoreStripsJSON.Text = "var productRegular ="
                                            + JSONHelper.Serialize<PoreStripProduct>(theRegularProduct)
                                            + ";\n var productUltra ="
                                            + JSONHelper.Serialize<PoreStripProduct>(theUltraProduct)
                                            + ";\n var productCombo ="
                                            + JSONHelper.Serialize<PoreStripProduct>(theComboProduct)
                                             + ";\n var productUltraCombo ="
                                            + JSONHelper.Serialize<PoreStripProduct>(theUltraComboProduct)
                                            + ";";
                }
                else
                    // If a user accidentally lands on the product detail page without a route.
                    theProduct = ProductDAO.GetProduct("combination-skin-balancing-cleanser");
            }


            // Add meta tagging.
            myMaster.Page.Title = theProduct.MetaTitle;

            HtmlGenericControl theMetaDescription = (HtmlGenericControl)Master.FindControl("head").FindControl("metaDescription");
            theMetaDescription.Attributes.Add("content", theProduct.MetaDescription);

            HtmlGenericControl theMetaKeywords = (HtmlGenericControl)Master.FindControl("head").FindControl("metaKeywords");
            theMetaKeywords.Attributes.Add("content", theProduct.MetaKeywords);

           HtmlMeta theMetaFBImage = (HtmlMeta)Master.FindControl("fbImage");
            theMetaFBImage.Attributes.Add("content", "http://" + Request.Url.Host + VirtualPathUtility.ToAbsolute(theProduct.SocialImage));


            // Populate category, write body classes to style according to category
            String productCategory = theProduct.Category.Trim();
            String productCategoryLeft = theProduct.CategoryLeft == null ? "" : theProduct.CategoryLeft.Trim();
            String productCategoryRight = theProduct.CategoryRight == null ? "" : theProduct.CategoryRight.Trim();

            if (productCategoryLeft.Length > 0 || productCategoryRight.Length > 0)
            {
                if (productCategoryLeft.Length > 0)
                {
                    theCategoryLeft.Text = productCategoryLeft;
                }
                else
                {
                    theCategoryLeft.Visible = false;
                }

                if (productCategoryRight.Length > 0)
                {
                    theCategoryRight.Text = productCategoryRight;
                }
                else
                {
                    theCategoryRight.Visible = false;
                }
            }
            else
            {
                theCategoryLeft.Text = productCategory;
                theCategoryRight.Visible = false;
            }


            // ATAM OLD
            //if (productCategory.ToLower() == "deep cleansing")
            //{
            //    myMaster.bodyClass += " deepCleansing";
            //    theCategoryClass.Text = "deepCleansing";
            //}
            //else if (productCategory.ToLower() == "complexion clearing")
            //{
            //    myMaster.bodyClass += " complexionClearing";
            //    theCategoryClass.Text = "complexionClearing";
            //}
            //else if (productCategory.ToLower() == "make-up removing")
            //{
            //    myMaster.bodyClass += " murt";
            //    theCategoryClass.Text = "murt";
            //}

            //ATAM NEW

            string strBVLocaleColorCode = "7668redes-green-en_CA";
            if (productCategory.ToLower() == "dont be dirty")
            {
                myMaster.bodyClass += " dontbedirty";
                theCategoryClass.Text = "dontbedirty";
            }
            else if (productCategory.ToLower() == "acne outta here")
            {
                myMaster.bodyClass += " acneouttahere";
                theCategoryClass.Text = "acneouttahere";
                strBVLocaleColorCode= "7668redes-orange-en_CA";
            }
            else if (productCategory.ToLower() == "breakup with black heads")
            {
                myMaster.bodyClass += " breakupwithblackheads";
                theCategoryClass.Text = "breakupwithblackheads";
                strBVLocaleColorCode= "7668redes-magento-en_CA";
            }
            else if (productCategory.ToLower() == "back off big pores")
            {
                myMaster.bodyClass += " backoffbigpores";
                theCategoryClass.Text = "backoffbigpores";
                strBVLocaleColorCode = "7668redes-magento-en_CA";
            }
            else if (productCategory.ToLower() == "take it all off") //micellar water
            {
                myMaster.bodyClass += " takeitalloff";
                theCategoryClass.Text = "takeitalloff";
                strBVLocaleColorCode = "7668redes-magento-en_CA";
            }


            // New and old image logic, including new product case

            if (myMaster.isMobile) { theNewImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.NewLookImage).Replace("large", "mobile"); }
            else { theNewImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.NewLookImage); }

            if (theProduct.OldLookImage == "") oldNewControlPanel.Visible = false;
            else
            {
                if (myMaster.isMobile)
                {
                    theOldImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.OldLookImage).Replace("large", "mobile");
                }
                else
                {
                    theOldImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.OldLookImage);
                }
            }
            if (theProduct.BuyNowURL == "") { theBuyNowLink.Visible = false; }


            // Plug in data from the product
            // Plug in data from the product
            theNamePrefix.Text = theProduct.NamePrefix == null ? "" : theProduct.NamePrefix;

            string ProductName = theProduct.Name == null ? "" : theProduct.Name.Trim();
            string ProductNameLine1 = theProduct.ProductNameLine1 == null ? "" : theProduct.ProductNameLine1.Trim();
            string ProductNameLine2 = theProduct.ProductNameLine2 == null ? "" : theProduct.ProductNameLine2.Trim();
            string ProductNameLine3 = theProduct.ProductNameLine3 == null ? "" : theProduct.ProductNameLine3.Trim();

            if (ProductNameLine1.Trim().Length == 0 && ProductNameLine2.Length == 0 && ProductNameLine3.Length == 0)
            {
                ProductNameL1.Text = ProductName.Replace("\n", "");
                ProductNameL1.Text = ProductNameL1.Text.Trim();
            }
            else
            {
                if (ProductNameLine1.Trim().Length > 0)
                {
                    ProductNameL1.Text = ProductNameLine1.Replace("\n", "");
                }
                else
                {
                    ProductNameL1.Visible = false;
                }

                if (ProductNameLine2.Trim().Length > 0)
                {
                    ProductNameL2.Text = ProductNameLine2.Replace("\n", "");
                }
                else
                {
                    ProductNameL2.Visible = false;
                }

                if (ProductNameLine3.Trim().Length > 0)
                {
                    ProductNameL3.Text = ProductNameLine3.Replace("\n", "");
                }
                else
                {
                    ProductNameL3.Visible = false;
                }

            }


            thePromoCopy.Text = theProduct.PromoCopy;
            theDescription.Text = theProduct.Description;
            //theBuyNowLink.NavigateUrl = theProduct.BuyNowURL;

            if (theProduct.BuyNowURL.Trim().Length <= 0)
            {
                divBuyNowBox.Visible = false;
            }
            else
            {
                if (theProduct.ProductRefName == "deep-cleansing-pore-strips")
                {
#if DEBUG
                    buyNowIFrame.Attributes["src"] = "/BuyNow.aspx?RefName=pore-strips#regular";
#else
    buyNowIFrame.Attributes["src"] = "/en-US/BuyNow.aspx?RefName=pore-strips#regular";
#endif

                }
                else
                {
#if DEBUG
                    buyNowIFrame.Attributes["src"] = "/BuyNow.aspx?RefName=" + theProduct.ProductRefName;
#else
    buyNowIFrame.Attributes["src"] = "/en-US/BuyNow.aspx?RefName=" + theProduct.ProductRefName;
#endif
                }
            }

            theWhatItIs.Text = theProduct.WhatItIs;
            theIngredients.Text = theProduct.ProductDetails;
            theHowItWorks.Text = theProduct.HowItWorks;
            theCautions.Text = theProduct.Cautions;
            theSidebarHeader.Text = theProduct.SideBarHeader;
            theSidebarCopy.Text = theProduct.SideBarCopy;

            // Is there a fun facts area?
            //if (theProduct.FunFacts == "") theFunFactsPanel.Visible = false;
           // else
            //{
                //theFunFacts.Text = theProduct.FunFacts;
            //}

            // Are product reviews active?
            //Ratings and Review initialization block - check if enabled
            if (System.Configuration.ConfigurationManager.AppSettings["RatingsAndReviews"].ToString() == "true")
            {
                //R&R IS TURNED ON
                String submitLink = Request.Url.Host + System.Configuration.ConfigurationManager.AppSettings["path"] + "/FormConfig/Ratings-And-Reviews/Default.aspx";
                submitLink = "http://" + submitLink.Replace("//", "/");

                litReviewsSummary.Text = "<div id=\"BVRRSummaryContainer\"></div>";
                litBVSubmitLink.Text = "<script type=\"text/javascript\">$BV.configure(\"global\", { submissionContainerUrl: \"" + submitLink + "\" });</script>";
                litBVInvoke.Text = "<script type=\"text/javascript\" id=\"BVUI\">$BV.ui(\"rr\", \"show_reviews\", {productId: \"" + theProduct.ProductRefName + "\", doShowContent: function() { productDetail.showReviewsTab(1);}});</script>";
                //litReviewsTab.Text = "<div id=\"BVRRContainer\"></div>";


                // Check if we're using the staging R&R server
                if (System.Configuration.ConfigurationManager.AppSettings["BV_UseBVStagingServer"].ToString() == "true")
                {
                    //R&R pointing to staging
                    //litBVAPIPath.Text = "<script type=\"text/javascript\" src=\"//biore.ugc.bazaarvoice.com/bvstaging/static/" + System.Configuration.ConfigurationManager.AppSettings["BV_LocaleKey"].ToString() + "/bvapi.js\"></script>";


                    litBVAPIPath.Text = "<script type=\"text/javascript\" src=\"//biore.ugc.bazaarvoice.com/bvstaging/static/" + strBVLocaleColorCode + "/bvapi.js\"></script>";

                    //    litReadAllReviews.Text = "<div id=\"BVRRLinkContainer\"><a href=\"http://reviews.biore.ca/bvstaging/" + System.Configuration.ConfigurationManager.AppSettings["BV_LocaleKey"].ToString() + "/" + p.ProductRefName + "/reviews.htm\">Read all " + p.ProductLongName + " reviews</a></div>";

                }
                else
                {
                    // R&R pointing to production.
                    litBVAPIPath.Text = "<script type=\"text/javascript\" src=\"//biore.ugc.bazaarvoice.com/static/" + strBVLocaleColorCode + "/bvapi.js\"></script>";
                    //     litReadAllReviews.Text = "<div id=\"BVRRLinkContainer\"><a href=\"http://reviews.biore.ca/" + System.Configuration.ConfigurationManager.AppSettings["BV_LocaleKey"].ToString() + "/" + p.ProductRefName + "/reviews.htm\">Read all " + p.ProductLongName + " reviews</a></div>";
                }
            }
            else
            {
                theProductReviewsPanel.Visible = false;
            }




            // Is there a video?
            if (theProduct.Video == "") theVideoPanel.Visible = false;
            else
            {
                theVideoLink.NavigateUrl = VirtualPathUtility.ToAbsolute(theProduct.Video);
                string theVideoParams = string.Empty;
                if (theProduct.VideoDuration != "") theVideoParams = "videoParams::448|252|" + theProduct.VideoDuration;
                else theVideoParams = "videoParams::448|252|160";
                theVideoLink.Attributes.Add("rel", theVideoParams);
                theVideoStillImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.VideoStillImage);
            }


            // Associated Products
            List<SideBarProduct> SideBarProductsList = theProduct.SideBarProductCollection;
            theSideBarProducts.DataSource = SideBarProductsList;
            theSideBarProducts.DataBind();


            // Draw Like button if not on a mobile device
            if (myMaster.isMobile == false)
            {
                buildLikeBtn(theProduct.LikeURL);
                //buildPlusBtn(theProduct.LikeURL);
                myMaster.FindControl("plusScript").Visible = true;

            }
        }

        protected void sidebar_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;
            ListView lv = (ListView)sender;

            List<SideBarProduct> myData = (List<SideBarProduct>)lv.DataSource;

            SideBarProduct theSideBarProduct = myData[dataItem.DataItemIndex];

            HyperLink theSideBarProductLink = (HyperLink)e.Item.FindControl("theSideBarProductLink");
            theSideBarProductLink.NavigateUrl = VirtualPathUtility.ToAbsolute(theSideBarProduct.ProductURL);

            Image theSideBarProductImage = (Image)e.Item.FindControl("theSideBarProductImage");
            theSideBarProductImage.ImageUrl = VirtualPathUtility.ToAbsolute(theSideBarProduct.Image);

            Literal theSideBarProductName = (Literal)e.Item.FindControl("theSideBarProductName");
            theSideBarProductName.Text = theSideBarProduct.ProductName;

            HtmlControl theSideBarListItem = (HtmlControl)e.Item.FindControl("theSideBarListItem");
            string className = theSideBarProduct.ClassName;
            if (className != "") { theSideBarListItem.Attributes.Add("class", className); }

        }
        private void buildLikeBtn(string likeURL)
        {
            // Build FB like code.
            StringBuilder likeBuilder = new StringBuilder();
            likeBuilder.Append("<div class=\"fb-like\" data-href=\"");
            likeBuilder.Append("http://" + Request.Url.Host + VirtualPathUtility.ToAbsolute(likeURL));
            likeBuilder.Append("\" data-send=\"false\" data-layout=\"button_count\" data-width=\"100\" data-show-faces=\"false\"></div>");
            fbLike.Text = likeBuilder.ToString();
        }

        private void buildPlusBtn(string likeURL)
        {
            // Build Google plus code.
            StringBuilder plusBuilder = new StringBuilder();
            plusBuilder.Append("<g:plusone size=\"medium\" annotation=\"none\" href=\"");
            plusBuilder.Append("http://" + Request.Url.Host + VirtualPathUtility.ToAbsolute(likeURL));
            plusBuilder.Append("\"></g:plusone>");
            plusButton.Text = plusBuilder.ToString();
        }
    }
}












/*namespace Biore2012.our_products
{
    public partial class ProductDetail : RoutablePage
    {

        public string floodlightIframeSource;
        public string floodlightDescription;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            //BWR-78, BWR-79, BWR-80
            if (HttpContext.Current.Request.Path.Contains("complexion-clearing-products") || HttpContext.Current.Request.Path.Contains("deep-cleansing-products") || HttpContext.Current.Request.Path.Contains("make-up-removing-products") || HttpContext.Current.Request.Path.Contains("deep-cleansing-product-family"))
                Response.Redirect("~/biore-facial-cleansing-products");

            if (HttpContext.Current.Request.Path.Contains("deep-pore-charcoal-cleanser"))
            {
                spanNamePrefix.Attributes.Add("class", "spanNamePrefixDeepPoreCharcoalCleanser");
            }
            else if (HttpContext.Current.Request.Path.Contains("self-heating-one-minute-mask"))
            {
                spanNamePrefix.Attributes.Add("class", "spanNamePrefixSelfHeatingOneMinuteMask");
            }
            else
            {
                // class="spanNamePrefix" 
                spanNamePrefix.Attributes.Add("class", "spanNamePrefix");
            }

            StringBuilder sb = new StringBuilder();

            // Grab masterpage, set appropriate body classes
            Site myMaster = (Site)this.Master;
            myMaster.bodyClass += " ourProducts productDetail";
            
            // turn off floodlight pixels for the majority of the detail pages
            myMaster.FindControl("floodlightPixels").Visible = false;

            // configure prove it link for mobile
            //BioreUtils bu = new BioreUtils();
            //bu.configureProveItLinks(myMaster.isMobile, proveItPromoLink);

            // Fetch product based on route used to reach this page.
            Product theProduct = new Product();
            String pid = "";
            if (Routing.Value("pid") != null)
            {
                pid = Routing.Value("pid").ToString();
                // Make sure we aren't showing a pore strip page.
                if (pid == "deep-cleansing-pore-strips-combo" ||
                    pid == "deep-cleansing-pore-strips-ultra" ||
                    pid == "deep-cleansing-pore-strips")
                    Response.Redirect(VirtualPathUtility.ToAbsolute("~/deep-cleansing-product-family/pore-strips"));
                else
                {
                    try
                    {
                        theProduct = ProductDAO.GetProduct(pid);
                    }
                    catch (Exception ex)
                    {

                        if (ex is HttpException)
                        {
                            var httpEx = ex as HttpException;

                            switch (httpEx.GetHttpCode())
                            {
                                case 404:
                                    Response.Redirect("~/404/");
                                    break;

                                // others if any

                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            else
            {
                if (Request.Url.ToString().Contains("breakup-with-blackheads/pore-strips"))
                {
                    // turn on floodlight pixels for pore strips, and set iframe source
                    myMaster.FindControl("floodlightPixels").Visible = true;
                    floodlightIframeSource = "http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=deepc455;ord=";
                    floodlightDescription = "pore strips (regular, ultra and combo)";
                    
                    // We're on the porestrips family page.
                    thePoreStripNavPanel.Visible = true;
                    theProduct = ProductDAO.GetProduct("deep-cleansing-pore-strips");
                    PoreStripProduct theRegularProduct = PoreStripProductDAO.GetProduct("deep-cleansing-pore-strips");
                    PoreStripProduct theUltraProduct = PoreStripProductDAO.GetProduct("deep-cleansing-pore-strips-ultra");
                    PoreStripProduct theComboProduct = PoreStripProductDAO.GetProduct("deep-cleansing-pore-strips-combo");

                    // Write the JSON for all three to the page.
                    thePoreStripsJSON.Text = "var productRegular ="
                                            + JSONHelper.Serialize<PoreStripProduct>(theRegularProduct)
                                            + ";\n var productUltra ="
                                            + JSONHelper.Serialize<PoreStripProduct>(theUltraProduct)
                                            + ";\n var productCombo ="
                                            + JSONHelper.Serialize<PoreStripProduct>(theComboProduct)
                                            + ";";
                }
                else
                    // If a user accidentally lands on the product detail page without a route.
                    theProduct = ProductDAO.GetProduct("combination-skin-balancing-cleanser");
            }
            
            // turn on floodlight pixels for acne clearing scrub, and set iframe source
            if (theProduct.ProductRefName == "acne-clearing-scrub")
            {
                myMaster.FindControl("floodlightPixels").Visible = true;
                floodlightIframeSource = "http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=compl814;ord=";
                floodlightDescription = theProduct.ProductRefName;
            }

            // turn on floodlight pixels for pore unclogging scrub, and set iframe source
            if (theProduct.ProductRefName == "pore-unclogging-scrub")
            {
                myMaster.FindControl("floodlightPixels").Visible = true;
                floodlightIframeSource = "http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=deepc349;ord=";
                floodlightDescription = theProduct.ProductRefName;
            }

            // turn on floodlight pixels for combination skin balancing cleanser, and set iframe source
            if (theProduct.ProductRefName == "combination-skin-balancing-cleanser")
            {
                myMaster.FindControl("floodlightPixels").Visible = true;
                floodlightIframeSource = "http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=deepc661;ord=";
                floodlightDescription = theProduct.ProductRefName;
            }

            // turn on floodlight pixels for steam activated cleanser, and set iframe source
            if (theProduct.ProductRefName == "steam-activated-cleanser")
            {
                myMaster.FindControl("floodlightPixels").Visible = true;
                floodlightIframeSource = "http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=deepc128;ord=";
                floodlightDescription = theProduct.ProductRefName;
            }

            // turn on floodlight pixels for 4-in-1 detoxificating cleanser, and set iframe source
            if (theProduct.ProductRefName == "4-in-1-detoxificating-cleanser")
            {
                myMaster.FindControl("floodlightPixels").Visible = true;
                floodlightIframeSource = "http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=deepc945;ord=";
                floodlightDescription = theProduct.ProductRefName;
            }

            // turn on floodlight pixels for daily cleansing cloths, and set iframe source
            if (theProduct.ProductRefName == "daily-cleansing-cloths")
            {
                myMaster.FindControl("floodlightPixels").Visible = true;
                floodlightIframeSource = "http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=deepc379;ord=";
                floodlightDescription = theProduct.ProductRefName;
            }

            // turn on floodlight pixels for blemish fighting ice cleanser, and set iframe source
            if (theProduct.ProductRefName == "blemish-fighting-ice-cleanser")
            {
                myMaster.FindControl("floodlightPixels").Visible = true;
                floodlightIframeSource = "http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=compl040;ord=";
                floodlightDescription = theProduct.ProductRefName;
            }

            // turn on floodlight pixels for blemish treating astringent, and set iframe source
            if (theProduct.ProductRefName == "blemish-treating-astringent-toner")
            {
                myMaster.FindControl("floodlightPixels").Visible = true;
                floodlightIframeSource = "http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=compl168;ord=";
                floodlightDescription = theProduct.ProductRefName;
            }

            // turn on floodlight pixels for warming anti-blackhead cleanser, and set iframe source
            if (theProduct.ProductRefName == "warming-anti-blackhead-cleanser")
            {
                myMaster.FindControl("floodlightPixels").Visible = true;
                floodlightIframeSource = "http://4133684.fls.doubleclick.net/activityi;src=4133684;type=biore530;cat=compl687;ord=";
                floodlightDescription = theProduct.ProductRefName;
            }


        

            // Add meta tagging.
            myMaster.Page.Title = theProduct.MetaTitle;

            HtmlGenericControl theMetaDescription = (HtmlGenericControl)Master.FindControl("head").FindControl("metaDescription");
            theMetaDescription.Attributes.Add("content", theProduct.MetaDescription);

            HtmlGenericControl theMetaKeywords = (HtmlGenericControl)Master.FindControl("head").FindControl("metaKeywords");
            theMetaKeywords.Attributes.Add("content", theProduct.MetaKeywords);

            HtmlMeta theMetaFBImage = (HtmlMeta)Master.FindControl("fbImage");
            theMetaFBImage.Attributes.Add("content", "http://" + Request.Url.Host + VirtualPathUtility.ToAbsolute(theProduct.SocialImage));


            //  ATAM this needs to be done in sidebar_ItemDataBound
            //String CategoryArrowSmallProduct = theProduct.CategoryArrowSmallProduct == null ? "" : theProduct.CategoryArrowSmallProduct.Trim();
            //if (CategoryArrowSmallProduct.Length > 0)
            //{
            //    //div
                
            //}
            //else
            //{
                
            //}

            // Populate category, write body classes to style according to category
            String productCategory = theProduct.Category.Trim();
            String productCategoryLeft = theProduct.CategoryLeft == null ? "" : theProduct.CategoryLeft.Trim();
            String productCategoryRight = theProduct.CategoryRight == null ? "" : theProduct.CategoryRight.Trim();

            if (productCategoryLeft.Length > 0 || productCategoryRight.Length > 0)
            {
                if ( productCategoryLeft.Length > 0)
                {
                    theCategoryLeft.Text = productCategoryLeft;
                }
                else
                {
                    theCategoryLeft.Visible = false;
                }

                if (productCategoryRight.Length > 0)
                {
                    theCategoryRight.Text = productCategoryRight;
                }
                else
                {
                    theCategoryRight.Visible = false;
                }
            }
            else
            {
                theCategoryLeft.Text = productCategory;
                theCategoryRight.Visible = false;
            }
            

            // ATAM OLD
            //if (productCategory.ToLower() == "deep cleansing")
            //{
            //    myMaster.bodyClass += " deepCleansing";
            //    theCategoryClass.Text = "deepCleansing";
            //}
            //else if (productCategory.ToLower() == "complexion clearing")
            //{
            //    myMaster.bodyClass += " complexionClearing";
            //    theCategoryClass.Text = "complexionClearing";
            //}
            //else if (productCategory.ToLower() == "make-up removing")
            //{
            //    myMaster.bodyClass += " murt";
            //    theCategoryClass.Text = "murt";
            //}
            
            //ATAM NEW
            if (productCategory.ToLower() == "dont be dirty")
            {
                myMaster.bodyClass += " dontbedirty";
                theCategoryClass.Text = "dontbedirty";
            }
            else if (productCategory.ToLower() == "acne outta here")
            {
                myMaster.bodyClass += " acneouttahere";
                theCategoryClass.Text = "acneouttahere";
            }
            else if (productCategory.ToLower() == "breakup with black heads")
            {
                myMaster.bodyClass += " breakupwithblackheads";
                theCategoryClass.Text = "breakupwithblackheads";
            }


            // New and old image logic, including new product case

            if (myMaster.isMobile)
            {
                theNewImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.NewLookImage).Replace("large", "mobile");
            }
            else
            {
                theNewImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.NewLookImage);

                //string sPath = System.Configuration.ConfigurationManager.AppSettings["path"];
                //string sProductImage = VirtualPathUtility.ToAbsolute(theProduct.NewLookImage);

                //if (sProductImage.StartsWith("/") && !sProductImage.StartsWith("/en-us"))
                //{
                //    sProductImage = sProductImage.Remove(0, 1);
                //}

                //theNewImage.ImageUrl = sPath + sProductImage; //  VirtualPathUtility.ToAbsolute(theProduct.NewLookImage);
            }

            //Display alt tag and make it title case for SEO requirements 6/12/13 -MCT
            Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(theNewImage.AlternateText = theProduct.ProductRefName.Replace("-", " "));

            if (theProduct.OldLookImage == "") oldNewControlPanel.Visible = false;
            else
            {
                if (myMaster.isMobile)
                {
                    theOldImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.OldLookImage).Replace("large", "mobile");
                }
                //else
                //{
                //    string sPath = System.Configuration.ConfigurationManager.AppSettings["path"];
                //    string sProductImage = VirtualPathUtility.ToAbsolute(theProduct.OldLookImage);

                //    if (sProductImage.StartsWith("/") && !sProductImage.StartsWith("/en-us"))
                //    {
                //        sProductImage = sProductImage.Remove(0, 1);
                //    }

                //    theOldImage.ImageUrl = sPath + sProductImage; //  VirtualPathUtility.ToAbsolute(theProduct.NewLookImage);
                //}
                else
                {
                    theOldImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.OldLookImage);
                }
            }
            if (theProduct.BuyNowURL == "") { theBuyNowLink.Visible = false; }


            // Plug in data from the product
            theNamePrefix.Text = theProduct.NamePrefix == null ? "" : theProduct.NamePrefix;

            string ProductName = theProduct.Name == null ? "" : theProduct.Name.Trim();
            string ProductNameLine1 = theProduct.ProductNameLine1 == null ? "" : theProduct.ProductNameLine1.Trim();
            string ProductNameLine2 = theProduct.ProductNameLine2 == null ? "" : theProduct.ProductNameLine2.Trim();
            string ProductNameLine3 = theProduct.ProductNameLine3 == null ? "" : theProduct.ProductNameLine3.Trim();

            if (ProductNameLine1.Trim().Length == 0 && ProductNameLine2.Length == 0 && ProductNameLine3.Length == 0)
            {
                ProductNameL1.Text = ProductName.Replace("\n", "");
                ProductNameL1.Text = ProductNameL1.Text.Trim();
            }
            else
            {
                if (ProductNameLine1.Trim().Length > 0)
                {
                    ProductNameL1.Text = ProductNameLine1.Replace("\n", "");                    
                }
                else
                {
                    ProductNameL1.Visible = false;
                }

                if (ProductNameLine2.Trim().Length > 0)
                {
                    ProductNameL2.Text = ProductNameLine2.Replace("\n", "");
                }
                else
                {
                    ProductNameL2.Visible = false;
                }

                if (ProductNameLine3.Trim().Length > 0)
                {
                    ProductNameL3.Text = ProductNameLine3.Replace("\n", "");
                }
                else
                {
                    ProductNameL3.Visible = false;
                }

            }


            thePromoCopy.Text = theProduct.PromoCopy;
            theDescription.Text = theProduct.Description;
            //theBuyNowLink.NavigateUrl = theProduct.BuyNowURL;
            
            if (theProduct.BuyNowURL.Trim().Length <= 0)
            {
                divBuyNowBox.Visible = false;
            }
            else
            {
                if (theProduct.ProductRefName == "deep-cleansing-pore-strips")
                {
#if DEBUG
                    buyNowIFrame.Attributes["src"] = "/BuyNow.aspx?RefName=pore-strips#regular";
#else
    buyNowIFrame.Attributes["src"] = "/en-US/BuyNow.aspx?RefName=pore-strips#regular";
#endif

                }
                else
                {
#if DEBUG
                    buyNowIFrame.Attributes["src"] = "/BuyNow.aspx?RefName=" + theProduct.ProductRefName;
#else
    buyNowIFrame.Attributes["src"] = "/en-US/BuyNow.aspx?RefName=" + theProduct.ProductRefName;
#endif
                }
            }

            theWhatItIs.Text = theProduct.WhatItIs;
            theIngredients.Text = theProduct.ProductDetails;
            theHowItWorks.Text = theProduct.HowItWorks;
            theCautions.Text = theProduct.Cautions;
            theSidebarHeader.Text = theProduct.SideBarHeader;
            theSidebarCopy.Text = theProduct.SideBarCopy;

            // Is there a fun facts area?
            if (theProduct.FunFacts == "") theFunFactsPanel.Visible = false;
            else
            {
                theFunFacts.Text = theProduct.FunFacts;
            }


            // Is there a video?
            if (theProduct.Video == "") theVideoPanel.Visible = false;
            else
            {
                theVideoLink.NavigateUrl = VirtualPathUtility.ToAbsolute(theProduct.Video);
                string theVideoParams = string.Empty;
                if (theProduct.VideoDuration != "") theVideoParams = "videoParams::448|252|" + theProduct.VideoDuration;
                else theVideoParams = "videoParams::448|252|160";
                theVideoLink.Attributes.Add("rel", theVideoParams);
                theVideoStillImage.ImageUrl = VirtualPathUtility.ToAbsolute(theProduct.VideoStillImage);
            }


            // Associated Products
            List<SideBarProduct> SideBarProductsList = theProduct.SideBarProductCollection;
            theSideBarProducts.DataSource = SideBarProductsList;
            theSideBarProducts.DataBind();


            // Draw Like button if not on a mobile device
            if (myMaster.isMobile == false)
            {
                buildLikeBtn(theProduct.LikeURL);
                buildPlusBtn(theProduct.LikeURL);
                myMaster.FindControl("plusScript").Visible = true;

            }
        }

        protected void sidebar_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;
            ListView lv = (ListView)sender;

            List<SideBarProduct> myData = (List<SideBarProduct>)lv.DataSource;

            SideBarProduct theSideBarProduct = myData[dataItem.DataItemIndex];

            HyperLink theSideBarProductLink = (HyperLink)e.Item.FindControl("theSideBarProductLink");
            theSideBarProductLink.NavigateUrl = VirtualPathUtility.ToAbsolute(theSideBarProduct.ProductURL);

            Image theSideBarProductImage = (Image)e.Item.FindControl("theSideBarProductImage");
            theSideBarProductImage.ImageUrl = VirtualPathUtility.ToAbsolute(theSideBarProduct.Image);

            Literal theSideBarProductName = (Literal)e.Item.FindControl("theSideBarProductName");
            theSideBarProductName.Text = theSideBarProduct.ProductName;

            HtmlControl theSideBarListItem = (HtmlControl)e.Item.FindControl("theSideBarListItem");
            string className = theSideBarProduct.ClassName;
            if (className != "") { theSideBarListItem.Attributes.Add("class", className); }

            HtmlControl divarrowSmallProduct = (HtmlControl)e.Item.FindControl("divarrowSmallProduct");
            string arrowSmallProductClassName = "arrowSmallProduct";

            if (theSideBarProduct.CategoryArrowSmallProduct != null && theSideBarProduct.CategoryArrowSmallProduct.Trim().Length > 0)
            {
                arrowSmallProductClassName = theSideBarProduct.CategoryArrowSmallProduct.Trim();

            }
                
            divarrowSmallProduct.Attributes.Add("class", arrowSmallProductClassName);

        }
        private void buildLikeBtn(string likeURL)
        {
            // Build FB like code.
            StringBuilder likeBuilder = new StringBuilder();
            likeBuilder.Append("<div class=\"fb-like\" data-href=\"");
            likeBuilder.Append("http://" + Request.Url.Host + VirtualPathUtility.ToAbsolute(likeURL));
            likeBuilder.Append("\" data-send=\"false\" data-layout=\"button_count\" data-width=\"100\" data-show-faces=\"false\"></div>");
            fbLike.Text = likeBuilder.ToString();
        }

        private void buildPlusBtn(string likeURL)
        {
            // Build Google plus code.
            StringBuilder plusBuilder = new StringBuilder();
            plusBuilder.Append("<g:plusone size=\"medium\" annotation=\"none\" href=\"");
            plusBuilder.Append("http://" + Request.Url.Host + VirtualPathUtility.ToAbsolute(likeURL));
            plusBuilder.Append("\"></g:plusone>");
            plusButton.Text = plusBuilder.ToString();
        }
    }
}*/


