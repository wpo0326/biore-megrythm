﻿<%@ Page Title="Libre tus poros - Obtén una piel más limpia y de aspecto más saludable - Explora todos los productos | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.our_products.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Los productos Bioré® Skincare limpian y exfolian con delicadeza, dejando tu piel más limpia y con un aspecto más saludable. Conoce la línea completa de productos Bioré® Skincare." name="description" />
    <meta content="Piel limpia, productos de Bioré®" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>
    <meta name="viewport" content="width=device-width">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="intro">
                <h1>Nuestros productos</h1>
                <div id="responseRule"></div>
                <p>Pon la raíz de todos los problemas de la piel en la mira, exfoliando cada semana y limpiando todos los días. Nuestros poderosos productos limpiadores de poros están disponibles en forma líquida, de espuma, exfoliador y banda para mantener tu piel limpia y con aspecto saludable.</p>
            </div>

			<div id="deepCleansingProds" class="prodList">
                <h2 class="pie roundedCorners deepCleansing"><span>para todo tipo de piel</span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img id="dontBeDirtyProducts" src="../images/ourProducts/products/small/Baking_Soda_Cleanser_small.png" alt="" />
                        <h3><!--<span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:8pt;">NUEVO</span> -->Baking Soda Limpiador de Poros con bicarbonato de sodio</h3>
                        <p>Limpia profundamente y exfolia la piel reseca y escamosa.</p>
                        <a href="../dont-be-dirty/baking-soda-pore-cleanser" id="A1">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-cleansing-scrub-small.png" alt="" />
                        <h3><!--<span style="font-family: ProximaNova-Bold, Arial;color:yellow;font-size:8pt;">NUEVO</span> -->Baking Soda Limpiador en Polvo con bicarbonato de sodio</h3>
                        <p>Limpia profundamente sin exfoliar de más.</p>
                        <a href="../dont-be-dirty/baking-soda-cleansing-scrub" id="A2">details ></a>
                    </li>
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalBar.png" alt="" />
                        <h3>Barra Limpiadora de Poros Con Carbón Natural</h3>
                        <p>Fórmula de doble acción que exfolia y limpia los poros profundamente.</p>
                        <a href="../dont-be-dirty/charcoal-bar" id="A3">details ></a>
                    </li>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CHARCOAL_100X235.png" alt="" />
                        <h3>Limpiador Profundo de Poros con Carbón Natural</h3>
                        <p>Fórmula con carbón que actúa como imán para extraer la suciedad y la grasa, dejando la piel con una suavidad que se siente.</p>
                        <a href="../dont-be-dirty/deep-pore-charcoal-cleanser" id="A4">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/SHM_100X235.png" alt="" />
                        <h3>Mascarilla térmica con carbón natural</h3>
                        <p>Se calienta al entrar en contacto con la piel para eliminar la suciedad y la grasa, dejando la piel limpia y con una tersura que se siente.</p>
                        <a href="../dont-be-dirty/self-heating-one-minute-mask" id="A5">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/poreUncloggingScrub.png" alt="" />
                        <h3>EXFOLIADOR DESINCRUSTANTE</h3>
                        <p>Alisa y destapa la piel, enfocándose en la suciedad y la grasa.</p>
                        <a href="../dont-be-dirty/pore-unclogging-scrub" id="topnav-pore-unclogging-scrub">details ></a>
                    </li>
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/combinationSkinBalancingCleanser.png" alt="" />
                        <h3>Gel Limpiador Equilibrante Para Piel Mixta</h3>
                        <p>Equilibra la humedad de la piel y resuelve los problemas de la piel mixta.</p>
                        <a href="../dont-be-dirty/combination-skin-balancing-cleanser" id="details-combination-skin-balancing-cleanser">details ></a>
                    </li>-->
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/dailyCleansingCloths.png" alt="" />
                        <h3>Toallitas Diarias de Limpieza Profunda de Poros</h3>
                        <p>Elimina la suciedad, grasa y maquillaje con una tela exfoliadora.</p>
                        <a href="../dont-be-dirty/daily-cleansing-cloths" id="A4">details ></a>
                    </li>-->
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/makeupRemovingTowelettes.png" alt="" style="margin-left:13%;" />
                        <h3>Toallitas Desmaquillantes</h3>
                        <p>Elimina el tenaz maquillaje a prueba de agua y limpia los poros.</p>
                        <a href="../dont-be-dirty/make-up-removing-towelettes" id="details-make-up-removing-towelettes">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreMinimizer.png" alt="" />
                        <h3>Limpiador Minimizador de Poros Con Carbón Natural</h3>
                        <p>Exfolia y atrapa la suciedad para dejar los poros visiblemente más pequeños.</p>
                        <div id="BVRRInlineRating-charcoal-pore-minimizer" class="ratingCategory"></div>
                        <a href="../back-off-big-pores/charcoal-pore-minimizer" id="details-charcoal-pore-minimizer">details ></a>
                    </li>
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/4In1DetoxifyingCleanser.png" alt="" />
                        <h3>Espuma Limpiador Desintoxicante</h3>
                        <p>Limpia, tonifica y estimula con nuestra fórmula autoespumante.</p>
                        <a href="../dont-be-dirty/pore-detoxifying-foam-cleanser" id="details-pore-detoxifying-foam-cleanser">details ></a>
                    </li>    -->
                </ul>
            </div>
            
            
            <div id="complexionClearingProds" class="prodList">
                <h2 class="pie roundedCorners complexionClearing"><span>para piel con granos y espinillas</span>
                    <span></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-acne-cleansing-foam.png" alt="" style="bottom:155px;" />
                        <h3>ESPUMA LIMPIADOR CON BAKING SODA PARA PIEL CON GRANOS Y ESPINILLAS</h3>
                        <p>Gentle foam targets acne & deep cleans pores.</p>
                        <div id="BVRRInlineRating-baking-soda-acne-cleansing-foam" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/baking-soda-acne-cleansing-foam" id="A6">details ></a>
                    </li>
                <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-acne-scrub.png" alt="" style="bottom:155px;" />
                        <h3>EXFOLIADOR CON BAKING SODA PARA PIEL CON GRANOS Y ESPINILLAS</h3>
                        <p>Destapa los poros y equilibra la piel para reducir los brotes.</p>
                        <div id="BVRRInlineRating-baking-soda-acne-scrub" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/baking-soda-acne-scrub" id="details-baking-soda-acne-scrub">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-acne-scrub.png" alt="" style="bottom:155px;" />
                        <h3>EXFOLIADOR CON CARBÓN NATURAL PARA PIEL CON GRANOS Y ESPINILLAS</h3>
                        <p>Elimina suavemente la suciedad que causa las imperfecciones y absorbe el exceso de grasa para ayudar a eliminar los brotes.</p>
                        <div id="BVRRInlineRating-charcoal-acne-scrub" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/charcoal-acne-scrub" id="details-charcoal-acne-scrub">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-acne-clearing-cleanser.png" alt="" style="bottom: 172px; width: 86px;" />
                        <h3>LIMPIADOR CON CARBÓN NATURAL PARA PIEL CON GRANOS Y ESPINILLAS</h3>
                        <p>Limpia profundamente, penetra los poros y absorbe la grasa para ayudar a detener los brotes.</p>
                        <div id="BVRRInlineRating-charcoal-acne-clearing-cleanser" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/charcoal-acne-clearing-cleanser" id="details-charcoal-acne-clearing-cleanser">details ></a>
                    </li>
                
                
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishFightingIceCleanser.png" alt="" />
                        <h3>GEL LIMPIADOR REFRESCANTE PARA PIEL con granos y espinillas</h3>
                        <p>Esta fórmula, que contiene ácido salicílico, elimina la suciedad, grasa y maquillaje de los poros.</p>
                        <a href="../acnes-outta-here/blemish-fighting-ice-cleanser" id="details-blemish-fighting-ice-cleanser">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/acneClearingScrub.png" alt="" />
                        <h3>EXFOLIADOR DIARIO PARA PIEL con granos y espinillas</h3>
                        <p>Vence a la suciedad y grasa profundas en solo 2 días.</p>
                        <a href="../acnes-outta-here/acne-clearing-scrub" id="details-acne-clearing-scrub">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishTreatingAstringentToner.png" alt="" />
                        <h3>Astringente para Piel con granos y espinillas</h3>
                        <p>Ayuda a evitar brotes con un tonificador sin aceites.</p>
                        <a href="../acnes-outta-here/blemish-fighting-astringent-toner" id="details-blemish-fighting-astringent">details ></a>
                    </li>
                </ul>
            </div>

            



            <div id="murtProds" class="prodList">
                <h2 class="pie roundedCorners murt"><span>para piel con puntos negros</span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltra.png" alt="" />
                        <h3>BANDAS ULTRA DE LIMPIEZA PROFUNDA</h3>
                        <p>Destruye las espinillas y elimina 2 veces más suciedad profunda.</p>
                        <a href="../breakup-with-blackheads/pore-strips#ultra" id="details-deep-cleansing-pore-strips-ultra">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStrips.png" alt="" />
                        <h3>BANDAS DE LIMPIEZA PROFUNDA</h3>
                        <p>Destapa los poros con un poder parecido al de un imán. Elimina al instante semanas de suciedad acumulada.</p>
                        <a href="../breakup-with-blackheads/pore-strips#regular" id="details-deep-cleansing-pore-strips">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/warmingAntiBlackheadCleanser.png" alt="" />
                        <h3>Limpiador Térmico Para Piel Con Puntos Negros</h3>
                        <p>Destruye la suciedad y la grasa con una fórmula calentadora de microesferas. </p>
                        <a href="../breakup-with-blackheads/warming-anti-blackhead-cleanser" id="details-deep-cleansing-pore-strips-combo">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.png" alt="" />
                        <h3>bandas de limpieza combo</h3>
                        <p>Destapa los poros con un poder parecido al de un imán en tu nariz, mentón, mejillas y frente.</p>
                        <a href="../breakup-with-blackheads/pore-strips#combo" id="details-deep-cleansing-pore-strips-combo">details ></a>
                    </li>
                    
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" />
                        <h3>Bandas de Limpieza Profunda Con Carbón Natural</h3>
                        <p>Destapa los poros y extrae el exceso de grasa para la limpieza más profunda.</p>
                        <div id="BVRRInlineRating-charcoal-pore-strips-regular" class="ratingCategory"></div>
                        <a href="../breakup-with-blackheads/charcoal-pore-strips#regular" id="details-deep-cleansing-pore-strips-charcoal">details ></a>
                    </li>
                    
                    
                    <!-- <li>
                        <img src="../images/ourProducts/products/small/combinationSkinBalancingCleanser.png" alt="" />
                        <h3>COMBINATION SKIN BALANCING CLEANSER</h3>
                        <p>Get an even, all-over clean for the complex needs of combination skin.</p>
                        <a href="../dont-be-dirty/combination-skin-balancing-cleanser" id="details-combination-skin-balancing-cleanser">details ></a>
                    </li>
                    <li class="taller" id="dailyCleansingCloths">
                        <img src="../images/ourProducts/products/small/dailyCleansingCloths.png" alt="" />
                        <h3>DAILY CLEANSING CLOTHS</h3>
                        <p>Wipe away dirt to reveal healthy, glowing skin.</p>
                        <a href="../dont-be-dirty/daily-cleansing-cloths" id="details-daily-cleansing-cloths">details ></a>
                    </li>
                    
                    <li>
                        <img src="../images/ourProducts/products/small/4In1DetoxifyingCleanser.png" alt="" />
                        <h3>4-IN-1 DETOXIFYING CLEANSER</h3>
                        <p>Revitalize and refresh for healthier-looking skin.</p>
                        <a href="../dont-be-dirty/4-in-1-detoxifying-cleanser" id="details-4-in-1-detoxifying-cleanser">details ></a>
                    </li>
                     <li>
                        <img src="../images/ourProducts/products/small/steamActivatedCleanser.jpg" alt="" />
                        <h3>STEAM ACTIVATED CLEANSER</h3>
                        <p>Harness the power of pore-opening steam for a truly deep clean.</p>
                        <a href="../dont-be-dirty/steam-activated-cleanser" id="details-steam-activated-cleanser">details ></a>
                    </li> -->
                </ul>
            </div>
            
            
            
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
