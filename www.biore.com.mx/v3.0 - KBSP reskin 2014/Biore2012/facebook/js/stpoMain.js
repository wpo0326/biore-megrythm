﻿(function($, undefined) {
	var socialSharing = {
		openWindow: function(url) {
			window.open(url, 'feedDialog', 'toolbar=0,status=0,width=580,height=400');
			return false;
		},
		createFBShareLink: function() {
			// Reference for the Feed Dialog:
			// http://developers.facebook.com/docs/reference/dialogs/feed/
			var url = 'http://www.facebook.com/dialog/feed?app_id=' + FBVars.fbAppId +
					'&link=' + FBVars.fbShareUrl +
					(FBVars.fbSharePicPath !== '' ? '&picture=' + FBVars.fbSharePicPath : '') +
					(FBVars.nameLine !== '' ? '&name=' + encodeURIComponent(FBVars.nameLine) : '') +
					(FBVars.capt !== '' ? '&caption=' + encodeURIComponent(FBVars.capt) : '') +
					(FBVars.desc !== '' ? '&description=' + encodeURIComponent(FBVars.desc) : '') +
					'&redirect_uri=' + FBVars.baseURL + 'PopupClose.html' +
					'&display=popup';

			return url;
		}
	};

	window.stpoMain = {
		init: function() {
			if ($('.tabContainer').length) {
				stpoMain.setupLaunchDayTabs();
			}
			if ($('.quizHolder').length) {
				stpoMain.setupQuiz();
			}
			if ($('.videoLink').length) {
				stpoMain.setupVideo();
			}
			if ($('.phUserLikesPre10am').length) {
				goToPage('/Biore2012/facebook/StoryToPoreOver/TabView/#phUserLikesPre10am');
			}
			if ($('.phPostLaunchDayContent').length && !$('.AuthorPartnershipInterim').length) {
				goToPage('/Biore2012/facebook/StoryToPoreOver/TabView/#phPostLaunchDayContent');
			}
			if ($('.AuthorPartnershipInterim').length) {
				goToPage('/Biore2012/facebook/StoryToPoreOver/TabView/#AuthorPartnershipInterim');
			}
			stpoMain.setupEvents();
		},
		setupEvents: function() {
			// capture event on FB Share and popup Share Dialog
			$('.fbShare').on('click', function(e) {
				var url = socialSharing.createFBShareLink();
				socialSharing.openWindow(url);

				e.preventDefault();
			});

			$('#howDoesItWorkPopup').on('click', stpoMain.openHiWPopup);
			$('#getTips').on('click', stpoMain.openHiWPopup);
			$('.howDoesItWorkPopup .stpoHowItWorksPopupClose').on('click', stpoMain.closeHiWPopup);
			$('.howDoesItWorkPopupOverlay').on('click', stpoMain.closeHiWPopup);

			$('.downloadPanel a').on('click', function(e) {
				var dlItemName = $(this).attr('href').split('/').pop();
				goToPage('/Biore2012/facebook/StoryToPoreOver/BookDownload/#' + dlItemName);
			});

		},
		openHiWPopup: function(e) {
			$('.howDoesItWorkPopupOverlay').fadeIn(300);
			$('.howDoesItWorkPopup').fadeIn(600);
			e.preventDefault();
		},
		closeHiWPopup: function(e) {
			$('.howDoesItWorkPopupOverlay').fadeOut(300);
			$('.howDoesItWorkPopup').fadeOut(300);
			e.preventDefault();
		},
		flashPrefix: '../../flash/',
		setupVideo: function() {
			$('.videoLink').ENLvideo({
				videoContainerClass: 'videoHolder',
				openVideoElsewhere: true,
				hideVideoLink: false,
				flashPrefix: "../../flash/",
				flashVideoPrefix: "../facebook/StoryToPoreOver/",
				videoWidth: 384,
				videoHeight: 288,
				callback: function() {
					if ($('a.defaultVid').length) {
						$('.videoHolder').html('').css('background-image', 'none');
					}
					if (!$(this).hasClass('defaultVid')) {  /* then the default overlay link wasn't the one clicked */
						$('.selected').removeClass('selected');
						$(this).addClass('selected');
					}
				}
			});
		},
		tabs: ['.tab1000am', '.tab0100pm', '.tab0300pm'],
		setupLaunchDayTabs: function() {
			var tabToShow = 0;
			tabToShow = $('.tab').not('.preunlock').length - 1;
			switch (tabToShow) {
				case 0:
					goToPage('/Biore2012/facebook/StoryToPoreOver/TabView/#tabPanel1000am');
					break;
				case 1:
					goToPage('/Biore2012/facebook/StoryToPoreOver/TabView/#tabPanel0100pm');
					break;
				case 2:
					goToPage('/Biore2012/facebook/StoryToPoreOver/TabView/#tabPanel0300pm');
					break;
				default:
					break;
			}

			$(".tabContainer")
				.easytabs({
					animate: false,
					defaultTab: stpoMain.tabs[tabToShow],
					updateHash: false
				})
				.bind('easytabs:before', stpoMain.easytabsBeforeTabChange)
				.bind('easytabs:after', stpoMain.swapDisclaimers);
			$('.tab').on('click', '.lock', function(e) {
				$(this).siblings('a').eq(0).click();
			});
			stpoMain.swapDisclaimers(undefined, $(stpoMain.tabs[tabToShow] + ' a'));
		},
		easytabsBeforeTabChange: function(e, $tab) {
			var i, inTabs = false, clonedVid;
			// Check Lock state
			if ($tab.parent().hasClass("preunlock")) {
				$tab.siblings(".lock").animate({ top: -3 }, 100, function() { $(this).animate({ top: 2 }, 100); });
				// TESTING! if following is commented out
				return false;  // disable the "future/locked" tabs
			}
			goToPage('/Biore2012/facebook/StoryToPoreOver/TabView/' + $tab.attr('href'));
		},
		easytabsAfterTabChange: function(e, $tab) {
			// if ($.browser.msie && $.browser.version.substring(0, 1) < 9) {
			// $tab.closest(".tabsHolder").find("css3-container").css("z-index", 1);
			// $tab.closest(".tabsHolder").find(".tab").css("z-index", 2);
			// $tab.closest(".tabsHolder").find("css3-container.active").css("z-index", 11);
			// $tab.closest(".tabsHolder").find(".active").css("z-index", 15);
			// }
		},
		swapDisclaimers: function(e, $tab) {
			if (typeof $tab !== 'undefined' && $tab.attr('href') === '#tabPanel0300pm') {
				$('.disctabPanel1000am').hide();
				$('.disctabPanel0300pm').show();
			} else {
				$('.disctabPanel1000am').show();
				$('.disctabPanel0300pm').hide();
			}
		},
		setupQuiz: function() {
			if ($('.pan100Quiz').length) {
				$('.hide').hide();
			}
			$('.stpoQuizNextBtn').on('click', stpoMain.quizNext);
			$('.stpoQuizPrevBtn').on('click', stpoMain.quizPrev);
			$('.stpoQuizSubmitBtn').on('click', stpoMain.quizSubmit);
		},
		quizAnswers: [],
		quizNext: function(e) {
			var $curQ = $('.question.current'),
				curQindex = $curQ.index(),
				$nxtQ = $('.question').eq(curQindex + 1),
				$ans = $('.question.current input:checked');

			e.preventDefault();

			// Validation
			$('.error').remove();
			if (!$ans.length) {
				stpoMain.quizError($curQ);
				return;
			}

			stpoMain.quizAnswers[curQindex] = $ans.closest('li').index();

			$curQ.removeClass('current');
			if (curQindex <= 1) {
				$('.questionCounter .currentQ').text(curQindex + 2);
				$curQ.animate({ left: '-105%' }, 500);
				$nxtQ.animate({ left: 0 }, 500, function() {
					$(this).addClass('current');
				});
				$('.stpoQuizPrevBtn').css({ 'visibility': 'visible' });
				if (curQindex + 1 === 2) {
					$('.stpoQuizNextBtn').css({ 'visibility': 'hidden' });
					$('.stpoQuizSubmitBtn').show();
				}
			}

			$('.quizHolder').trigger('quizNext');
		},
		quizPrev: function(e) {
			var $curQ = $('.question.current'),
				curQindex = $curQ.index(),
				$prvQ = $('.question').eq(curQindex - 1),
				$ans = $('.question.current input:checked');

			e.preventDefault();

			// Validation
			$('.error').remove();
			if (!$ans.length) {
				stpoMain.quizError($curQ);
				return;
			}

			stpoMain.quizAnswers[curQindex] = $ans.closest('li').index();

			$curQ.removeClass('current');
			if (curQindex >= 1) {
				$('.questionCounter .currentQ').text(curQindex);
				$curQ.animate({ left: '105%' }, 500);
				$prvQ.animate({ left: 0 }, 500, function() {
					$(this).addClass('current');
				});
				$('.stpoQuizNextBtn').css({ 'visibility': 'visible' });
				if (curQindex - 1 <= 0) {
					$('.stpoQuizPrevBtn').css({ 'visibility': 'hidden' });
				}
				$('.stpoQuizSubmitBtn').hide();
			}

			$('.quizHolder').trigger('quizPrev');
		},
		quizError: function($errQ) {
			$errQ.append($('<p>').text('Please answer the question.').addClass('error'));
		},
		quizSubmit: function(e) {
			var $curQ = $('.question.current'),
				curQindex = $curQ.index(),
				$result = $('.result');

			$('.quizHolder').on('quizNext', function() {
				// Handle submit logic
				$curQ.animate({ left: '-105%' }, 500);
				$result.animate({ left: 0 }, 500, stpoMain.showQuizResults);
				stpoMain.calcResults();
			});

			$('.stpoQuizNextBtn').click();

			e.preventDefault();
		},
		showQuizResults: function() {
			$(this).addClass('current');
			$('.quizNav').hide();
			$('.stpoQuizSubmitBtn').hide();
			$('.quizQuestionsHdr').hide();
			$('.questions').addClass('results');
			$('.quizAnswerHdr').show();
			if ($('.proveIt').length) { $('.proveIt').show(); }
		},
		calcResults: function() {
			var countA = 0, countB = 0, countC = 0, i;
			if (stpoMain.quizAnswers.length === 3) {
				for (i = 0; i < 3; i++) {
					countA += stpoMain.quizAnswers[i] === 0 ? 1 : 0;
					countB += stpoMain.quizAnswers[i] === 1 ? 1 : 0;
					countC += stpoMain.quizAnswers[i] === 2 ? 1 : 0;
				}

				if (countA >= 2) {
					// Love for Love
					stpoMain.showQuizResultsAnswerA();
					return;
				} else if (countB >= 2) {
					// Family First
					stpoMain.showQuizResultsAnswerB();
					return;
				} else if (countC >= 2) {
					// Friends Forever
					stpoMain.showQuizResultsAnswerC(false);
					return;
				}
			}
			// Show default (there was some other result (i.e. 1,1,1))
			// Friends Forever
			stpoMain.showQuizResultsAnswerC(true);
		},
		showQuizResultsAnswerA: function(isDefault) {
			$('.quizAnswerHdr p').hide().filter('.loveForLove').show();
			$('.result .bookReco').hide().filter('.bookRecoLove').show();
			goToPage('/Biore2012/facebook/StoryToPoreOver/QuizResults/Answers_A/loveForLove/');
		},
		showQuizResultsAnswerB: function() {
			$('.quizAnswerHdr p').hide().filter('.familyFirst').show();
			$('.result .bookReco').hide().filter('.bookRecoFam').show();
			goToPage('/Biore2012/facebook/StoryToPoreOver/QuizResults/Answers_B/familyFirst/');
		},
		showQuizResultsAnswerC: function() {
			$('.quizAnswerHdr p').hide().filter('.friendsForever').show();
			$('.result .bookReco').hide().filter('.bookRecoFriend').show();
			if (isDefault) {
				goToPage('/Biore2012/facebook/StoryToPoreOver/QuizResults/Answers_Default/friendsForever/');
			} else {
				goToPage('/Biore2012/facebook/StoryToPoreOver/QuizResults/Answers_C/friendsForever/');
			}
		}
	};

	window.goToPage = function(pg) {
		firstTracker._trackPageview(pg);
		secondTracker._trackPageview(pg);
	};

	$(function() {
		stpoMain.init();
	});
})(jQuery);