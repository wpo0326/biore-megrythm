﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Biore2012.facebook
{
	public partial class FBContentPageMaster : System.Web.UI.MasterPage
	{

        private string _bodyClass;
        public string bodyClass
        {
            get { return _bodyClass; }
            set { _bodyClass = value; } 
        }

		// This code is in place to allow IE to keep its Cookies and Sessions despite being
		// set on an iFrame in Facebook.  This only affect certain versions & users with specific
		// privacy settings.
		protected override void OnPreRender(EventArgs e)
		{
			Response.AppendHeader("P3P", "CP=\"CAO CUR OUR\"");
			base.OnPreRender(e);
		}

		protected void Page_Load(object sender, EventArgs e)
		{

		}
	}
}
