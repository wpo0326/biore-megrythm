﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Facebook;

namespace Biore2012.facebook.CSBCCoupon
{
	public partial class Default : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            FBContentPageMaster myMaster = (FBContentPageMaster)this.Master;
            myMaster.bodyClass += " csbc";

			if (IsPostBack)
			{
				setDisplayState("pageliked");
			}
			else
			{
				string signedReqJson = "";
				FacebookSignedRequest signedReqObj = new FacebookSignedRequest();

				decodeFBSignedRequest(ref signedReqJson, ref signedReqObj);

				//if there is no signedRequest, kick to error page
				if (Session["signedRequestObj"] == null)
				{
					setDisplayState("notfacebook");
					return;
				}

				if (signedReqObj != null && signedReqObj.page.liked)
				{
					setDisplayState("pageliked");
				}
				else
				{
					setDisplayState("likeoverlay");
				}
			}
		}

		private void setDisplayState(string state)
		{
			panelLikeOverlay.Visible = false;
			panelUserLikes.Visible = false;
			panelNotFacebook.Visible = false;

			// For Dev purposes to force a state (must comment out the switch statement)
			//panelUserLikes.Visible = true;

			switch (state)
			{
				case "likeoverlay":
					panelLikeOverlay.Visible = true;
					break;
				case "pageliked":
					panelUserLikes.Visible = true;
					break;
				case "notfacebook":
					panelNotFacebook.Visible = true;
					break;
				default:
					panelLikeOverlay.Visible = true;
					break;
			}
		}

		private static void decodeFBSignedRequest(ref string signedReqJson, ref FacebookSignedRequest signedReqObj)
		{
			signedReqJson = FBUtils.decodeSignedReq();
			signedReqObj = FBUtils.serializeSignedRequestJSON<FacebookSignedRequest>(signedReqJson);

			FBUtils.setSignedRequestSession(signedReqJson, signedReqObj);
		}
	}
}
