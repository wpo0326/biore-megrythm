﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="KaoBrands.FormStuff.ucFormConfig" Codebehind="ucFormConfig.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ucFormMemberInfo" Src="~/Controls/ucFormMemberInfo.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ucFormOptin" Src="~/Controls/ucFormOptin.ascx" %>
<asp:Panel ID="PageContainer" cssClass="responsive noForm" runat="server">
<div id="contactFormWrap">

     <!-- Header --> 
    <div id="signUpHeaderBg">
        <div id="formHeader" class="archer-book">
            <h1 id="PageHeader" runat="server">
                <div class="divLitHeaderText"><asp:Literal ID="LitHeaderText" runat="server"></asp:Literal></div>
                <div class="divLitHeaderText2"><asp:Literal ID="LitHeaderText2" runat="server"></asp:Literal></div>
            </h1> 
        </div>
        <!-- Description --> 
        <asp:Panel ID="DescriptionContainer" CssClass="DescriptionContainer png-fix" runat="server"> 
            <asp:Literal ID="LitDescriptionText" runat="server"></asp:Literal>  
        </asp:Panel>  

         <div id="BrandImageContainer" class="png-fix">
             <span class="BrandImage1"><asp:Image ID="Image1" AlternateText="test" runat="server" Visible="false" CssClass="png-fix"/></span>
             <span class="BrandImage2"><asp:Image ID="Image2" AlternateText="test" runat="server" Visible="false" CssClass="png-fix"/></span>
             <span class="BrandImage3"><asp:Image ID="Image3" AlternateText="test" runat="server" Visible="false" CssClass="png-fix"/></span>
             <span class="BrandImage4"><asp:Image ID="Image4" AlternateText="test" runat="server" Visible="false" CssClass="png-fix"/></span>
             <span class="BrandImage5"><asp:Image ID="Image5" AlternateText="test" runat="server" Visible="false" CssClass="png-fix"/></span>
             <span class="BrandImage6"><asp:Image ID="Image6" AlternateText="test" runat="server" Visible="false" CssClass="png-fix"/></span>
             <span class="BrandImage7"><asp:Image ID="Image7" AlternateText="test" runat="server" Visible="false" CssClass="png-fix"/></span>
             <span class="BrandImage8"><asp:Image ID="Image8" AlternateText="test" runat="server" Visible="false" CssClass="png-fix"/></span>
	     </div>
    </div> <!-- End signUpHeaderBg -->
	
	<div id="ContactFormContainer">
	     
        <!-- Form Fields --> 
        <asp:Panel ID="PanelForm" CssClass="FormContainer" runat="server">              
            <!-- <p class="req png-fix"><em>Required*</em></p> -->
            <uc1:ucFormMemberInfo ID="ucFormMemberInfo" runat="server" />                    
            <asp:Panel ID="QuestionPanel" cssClass="QuestionContainer" runat="server">            
            </asp:Panel>
           <uc2:ucFormOptin ID="ucFormOptin" runat="server" />
            <div id="submit-container" class="SubmitContainer png-fix">
                <asp:Button UseSubmitBehavior="true" ID="submit" Text="" runat="server" CssClass="submit buttonLink png-fix" />
            </div>
        </asp:Panel>    
              
        <div class="FormBottom png-fix"></div>
                  
        <!-- Disclaimer --> 
        <div id="DisclaimerContainer" class="png-fix">
            <asp:Literal ID="LitDisclaimer" runat="server"></asp:Literal>  
        </div>
        
	</div>
</div>
<script type="text/javascript">
    <asp:Literal runat="server" id="equalHeightVars"></asp:Literal>
    jQuery( document ).ready(function( $ ) {
        if($('.ConfrimationContainer').length != 0){ $('#BrandImageContainer').hide();$(".signUp #mainContent").css("min-height","650px");}
    });    
</script>
</asp:Panel>