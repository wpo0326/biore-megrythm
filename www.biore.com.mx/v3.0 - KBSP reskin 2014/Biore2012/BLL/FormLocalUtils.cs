﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KAOForms;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Configuration;

namespace Biore2012.BLL
{
    public class FormLocalUtils
    {
        public bool MaxCapLimitExceeded(int EventID, int Cap)
        {
            ConsumerInfoDAO ciDAO = new ConsumerInfoDAO();
            EventCountData ecData = new EventCountData();

            ecData = ciDAO.getEventCount(EventID);
            if (ecData.CurrentCount >= Cap)
            {
                return true;

            }
            if (ecData.MaxCap > 0 && ecData.CurrentCount >= ecData.MaxCap)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void DisableControlOnClick(WebControl control, string clientFunction, string controlValue)
        {

            StringBuilder sb = new StringBuilder(128);
            sb.Append("if(typeof(Page_ClientValidate)=='function'){");
            sb.Append("if(!Page_ClientValidate()){return false;}}");
            sb.Append("this.disabled=true;");
            if (!String.IsNullOrEmpty(clientFunction))
            {
                sb.AppendFormat("this.value='{0}';", controlValue);
            }
            if (!String.IsNullOrEmpty(clientFunction))
            {
                sb.AppendFormat("if(typeof({0})=='function'){{{0}()}};", clientFunction);
            }
            sb.AppendFormat("{0};", control.Page.GetPostBackEventReference(control));

            //add javascript to execute when button is clicked
            control.Attributes.Add("onclick", sb.ToString());

        }

        public Control clearCurrentPageValues(Control Root)
        {


            foreach (Control Ctl in Root.Controls)
            {
                if (Ctl is TextBox)
                {
                    ((TextBox)(Ctl)).Text = string.Empty;
                }

                if (Ctl is RadioButtonList)
                {
                    ((RadioButtonList)(Ctl)).ClearSelection();
                }

                if (Ctl is DropDownList)
                {
                    ((DropDownList)(Ctl)).ClearSelection();
                }

                if (Ctl is CheckBoxList)
                {
                    ((CheckBoxList)(Ctl)).ClearSelection();
                }

                Control FoundCtl = clearCurrentPageValues(Ctl);


            }

            return null;

        }

        public Control findControlRecursive(Control Root, string Id)
        {

            if (Root.ID == Id)

                return Root;

            foreach (Control Ctl in Root.Controls)
            {

                Control FoundCtl = findControlRecursive(Ctl, Id);

                if (FoundCtl != null)

                    return FoundCtl;

            }

            return null;

        }

        public bool sendReportPingEmail(string fName, string lName, string email, string formID, string memID)
        {
            EmailDAO eDAO = new EmailDAO();
            string crsWebsite = System.Configuration.ConfigurationManager.AppSettings["crs_Website"];

            StringBuilder emailMessage = new StringBuilder();
            emailMessage.Append("CRS Website: " + crsWebsite + "\r\n");
            emailMessage.Append("Form Submitted: " + formID + "\r\n");
            emailMessage.Append("First Name: " + fName + "\r\n");
            emailMessage.Append("Last Name: " + lName + "\r\n");
            emailMessage.Append("Email: " + email + "\r\n");
            emailMessage.Append("Member ID: " + memID + "\r\n");

            try
            {
                eDAO.sendSMTPEmail("KaoProdSupport@enlighten.com", "salontouroptin@" + crsWebsite, "JF Salon Tour Form Submit Notification - CRS Website: " + crsWebsite, emailMessage.ToString());

            }
            catch (Exception ex)
            {
                sendReportPingEmailFailLog(fName, lName, email, formID, memID, ex.Message.ToString());
                return false;
            }

            return true;
        }

        private void sendReportPingEmailFailLog(string fName, string lName, string email, string formID, string memID, string error)
        {
            StringBuilder myString = new StringBuilder(2000);
            string crsWebsite = System.Configuration.ConfigurationManager.AppSettings["crs_Website"];
            int CurrentSiteID = int.Parse(System.Configuration.ConfigurationManager.AppSettings["SiteID"]);
            myString.Append("Error sending sendReportPingEmail \r\n");
            myString.Append("Error: " + error + "\r\n");
            myString.Append("CRS Website: " + crsWebsite + "\r\n");
            myString.Append("Form Submitted: " + formID + "\r\n");
            myString.Append("First Name: " + fName + "\r\n");
            myString.Append("Last Name: " + lName + "\r\n");
            myString.Append("Email: " + email + "\r\n");
            myString.Append("Member ID: " + memID + "\r\n");

            LogEntry myLog = new LogEntry();
            myLog.Priority = 1;
            myLog.EventId = -(CurrentSiteID + 55);
            myLog.Message = myString.ToString();
            Logger.Write(myLog);
            myLog = null;
        }

        public void SalonTourAgentRedirect(HttpResponse response, HttpRequest request)
        {
            string flagCheck = System.Configuration.ConfigurationManager.AppSettings["SalonTour_Check"];
            //If the SalonTour_Check appkey is True and the Agent is invalid, then redirect.
            if (!string.IsNullOrEmpty(flagCheck) &&
                flagCheck.ToLower().Equals("true") &&
                !validSalonTourAgent(response, request))
            {
                response.Redirect("~/SalonTour2011/FormViewRefused.aspx");
            }

        }

        private bool validSalonTourAgent(HttpResponse response, HttpRequest request)
        {
            string userAgent = request.UserAgent;
            bool valid = false;

            //Get Web App Settings
            string iPad1 = System.Configuration.ConfigurationManager.AppSettings["SalonTour_iPad1"];
            string iPad2 = System.Configuration.ConfigurationManager.AppSettings["SalonTour_iPad2"];
            string iPad3 = System.Configuration.ConfigurationManager.AppSettings["SalonTour_iPad3"];
            string iPad4 = System.Configuration.ConfigurationManager.AppSettings["SalonTour_iPad4"];
            string iPadNot1 = System.Configuration.ConfigurationManager.AppSettings["SalonTour_iPadNot1"];
            string iPadNot2 = System.Configuration.ConfigurationManager.AppSettings["SalonTour_iPadNot2"];

            string chrome1 = System.Configuration.ConfigurationManager.AppSettings["SalonTour_Chrome1"];
            string chrome2 = System.Configuration.ConfigurationManager.AppSettings["SalonTour_Chrome2"];
            string chrome3 = System.Configuration.ConfigurationManager.AppSettings["SalonTour_Chrome3"];
            string chrome4 = System.Configuration.ConfigurationManager.AppSettings["SalonTour_Chrome4"];
            string chrome5 = System.Configuration.ConfigurationManager.AppSettings["SalonTour_Chrome5"];

            //ipad check mobile app only
            if (userAgent.Contains(iPad1) &&
                userAgent.Contains(iPad2) &&
                userAgent.Contains(iPad3) &&
                userAgent.Contains(iPad4) &&
                !userAgent.Contains(iPadNot1) &&
                !userAgent.Contains(iPadNot2))
            {
                valid = true;
            }

            //chrome on windows check XP or Win7
            if (userAgent.Contains(chrome1) &&
                userAgent.Contains(chrome2) &&
                userAgent.Contains(chrome3) &&
                userAgent.Contains(chrome4) &&
                userAgent.Contains(chrome5))
            {
                valid = true;
            }

            return valid;
        }

        public void queueDateNumber(int min, int max, string defaultVal, DropDownList DDL)
        {
            /// <summary>
            /// Method to bind ranged numbers to DropDownLists
            /// </summary>

            bindSurveyDDList(defaultVal, "", DDL); //Create default value

            if (!DDL.ID.ToLower().Contains("yyyy")) //Days/Month increment to max value
            {
                for (int i = min; i < max; i++)
                {
                    bindSurveyDDList(i.ToString(), i.ToString(), DDL);
                }
            }
            else
            {
                for (int i = max; i > min; i--) //Year decrements to min value
                {
                    bindSurveyDDList(i.ToString(), i.ToString(), DDL);
                }
            }

        }

        public string getCBAnswers(CheckBoxList cbList)
        {
            StringBuilder selectedItems = new StringBuilder();
            string answerString = string.Empty;

            foreach (ListItem li in cbList.Items)
            {
                if (li.Selected)
                {
                    selectedItems.Append(li.Value);
                    selectedItems.Append(",");
                }
            }

            answerString = selectedItems.ToString();

            if (!string.IsNullOrEmpty(answerString))
            {
                answerString = answerString.Remove(answerString.Length - 1);
            }

            return answerString;
        }

        //from KAO Global Utils.cs
        public void bindSurveyDDList(string text, string value, DropDownList DDL)
        {
            /// <summary>
            /// Method to bind values/numbers to DropDownLists
            /// </summary>
            /// 
            ListItem li = new ListItem();
            li.Text = text;
            li.Value = value;
            DDL.Items.Add(li);
            li = null;
        }
    }

}
