﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KAOForms;
using System.Data;
using System.Web.Security;

// NOTE: This is NOT the same as the ConsumerInfoUtils in GlobalBase2010!!!
namespace Biore2012.BLL
{
	public class ConsumerInfoUtils
	{
		public static MemberData getCIMemberInfo(int memberID)
		{
			MemberData mData = new MemberData();
			DataSet ds = ConsumerInfoUtilsDAO.getMemberInfo(memberID);

			if (ds != null && ds.Tables[0].Rows.Count > 0)
			{
				DataRow rs = ds.Tables[0].Rows[0];

				mData.FirstName = rs["FName"].ToString();
				mData.LastName = rs["LName"].ToString();
				mData.Email = rs["Email"].ToString();
				mData.Address1 = rs["Addr1"].ToString();
				mData.Address2 = rs["Addr2"].ToString();
				mData.Address3 = rs["Addr3"].ToString();
				mData.City = rs["City"].ToString();
				mData.State = rs["RegionID"].ToString();
				mData.PostalCode = rs["PostalCode"].ToString();
				mData.Gender = rs["Gender"].ToString();
				mData.DOB = rs["DOB"].ToString();
				mData.Phone = rs["HomePhone"].ToString();
			}
			return mData;
		}

		public static List<UserAnswer> getCITraitValues(int memberID)
		{
			List<UserAnswer> traitsList = new List<UserAnswer>();
			DataSet ds = ConsumerInfoUtilsDAO.getTraitValues(memberID);

			if (ds != null && ds.Tables[0].Rows.Count > 0)
			{
				foreach (DataRow row in ds.Tables[0].Rows)
				{
					traitsList.Add(new UserAnswer
					{

						traitID = row["TraitID"].ToString(),
						value = row["TraitValue"].ToString()
					});
				}
			}

			return traitsList;
		}

		public static MemberData GetMemData(string email, out int memId)
		{
			memId = GetMemId(email);
			if (memId != -1)
			{
				MemberData memData = getCIMemberInfo(memId);
				return memData;
			}
			return null;
		}

		public static MemberData GetMemData(int memId)
		{
			if (memId != -1)
			{
				MemberData memData = getCIMemberInfo(memId);
				return memData;
			}
			return null;
		}

		public static int GetMemId(string email)
		{
			int memId = -1;
			DataSet ds = ConsumerInfoUtilsDAO.getMemberID(email);

			if (ds != null && ds.Tables[0].Rows.Count > 0)
			{
				foreach (DataRow row in ds.Tables[0].Rows)
				{
					if (row["MemberID"] != null)
					{
						bool memIdValid = Int32.TryParse(row["MemberID"].ToString(), out memId);
					}
				}
			}
			return memId;
		}
	}

}