﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KAOForms;
using megrhythm.BLL;
using megrhythm.DAL;

//Add GDPR compliance checkbox per brand request, store as traitid megryhthm_uk_GDPR 6/18/18 -MCT

namespace megrhythm.Contact_Us
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
		{
			// Manually register the event-handling method for the  
			// ServerValidate event of the CustomValidator control.
			// FNameValidator3.ServerValidate +=
			// new ServerValidateEventHandler(this.TextBoxLengthValidator);

			Master.Page.Title = "Contact Us - MegRhythm";
				
			if (!IsPostBack)
			{
				
				ContactFormThanks.Visible = false;
				ContactFormError.Visible = false;


                CheckHookID();

				//litContactImage.Text = System.Configuration.ConfigurationManager.AppSettings["imageprefix"]; +"forms/contactFormSalonPool.jpg";
				DateTime dt = DateTime.Now;
				Utils ut = new Utils();

				ut.queueNumber(dt.Year - 100, dt.Year - 13, "Year", yyyy); //Build Year
				ut.queueNumber(1, 13, "Mon", mm); //Build Day
				ut.queueNumber(1, 32, "Day", dd); //Build Month
			}
		}

        protected void Contact_Click(object sender, EventArgs e)
        {

            Page.Validate();
            string strEntryReturn = "";
            string strMailReturn = "";
            string strTraitReturn = "";

            if (Page.IsValid && FrstNme.Text.Trim() == "") //honeypot check
            {

                ContactFormPanel.Visible = false;
                //int validEntry = -1;
                string DOB = mm.SelectedValue + "/" + dd.SelectedValue + "/" + yyyy.SelectedValue;

                //*** PROCESS AS POSSIBLE OPTIN (LEAVE BLANK IF NO OPTIN VALUE) --creates new "member" and adds to event log.
                //*** THEN SEND EMAIL TO CRS

                string strFName = FName.Text;
                string strLName = LName.Text;
                string strEmail = Email.Text;
                //string strEmailRetype = RetypeEmail.Text;
                string strStreet1 = Address1.Text;
                string strStreet2 = Address2.Text;
                //string strStreet3 = Address3.Text;
                string strCity = City.Text;
                string strProvince = "";
                string strPostalCode = PostalCode.Text;
                string strCountry = "United Kingdom";
                string strGender = Gender.SelectedValue.ToString() == "F" || Gender.SelectedValue.ToString() == "M" ? Gender.SelectedValue.ToString() : "";
                string strDOBdd = dd.SelectedValue;
                string strDOBmo = mm.SelectedValue;
                string strDOByr = yyyy.SelectedValue;
                string strDOB = DOB;
                DateTime testDate;
                bool blnIsDOBDate = DateTime.TryParse(DOB, out testDate);
                string strPhone = Phone.Text;
                string strMobilePhone = "";
                string strComment = QuestionComment.Text;
                string strUPC_Code = "";
                string strMFG_Code = "";

                string strEventCode = System.Configuration.ConfigurationManager.AppSettings["EventCode_contact_us"].ToString();
                int intEventID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["EventID_contact_us"]);
                int intSiteID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SiteID"]);

                string strURL = Request.RawUrl == null ? "" : Request.RawUrl.ToString();
                string strRemoteAddr = Request.ServerVariables["REMOTE_ADDR"] == null ? "" : Request.ServerVariables["REMOTE_ADDR"].ToString();
                string strUserAgent = Request.ServerVariables["HTTP_USER_AGENT"] == null ? "" : Request.ServerVariables["HTTP_USER_AGENT"].ToString(); ;

                string strContactContent = "";
                string strContactVia = "";
                string strContactFreq = "";

                //CRS-specific variables
                string strCrs_Opt_In_Out = "0";
                string strCrs_Age = "";
                if (blnIsDOBDate)
                {
                    strCrs_Age = (GetCRSAge(Convert.ToDateTime(DOB)));
                }
                string strCrs_Website = System.Configuration.ConfigurationManager.AppSettings["crs_Website"].ToString();  //johnfrieda.com
                string strCrs_Email = System.Configuration.ConfigurationManager.AppSettings["crs_EmailAddress"].ToString();  //the CRS "to" address
                string strCrs_Subject = "Contact from " + strCrs_Website;
                string strCrs_ContactFrom = "contact@" + strCrs_Website;

                int intGDPRID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["traitID_GDPR"]);

                string strHookID = hfHookID.Value;

                //GDPR acknowledment
                string strGDPR = bioreGDPR.Checked ? "I agree" : "I disagree";

                //bool blnNewsletter = optin.Checked ? true : false;
                //bool blnSamples = sampleRequest.Checked ? true : false;

                //string strNewsletterReturn = "";
                //string strSamplesReturn = "";                    

                //INSERT NEWSLETTER OPT-IN VALUES IF USER OPTED IN.
                //if (blnNewsletter)
                //{
                //    strContactContent = "samples,contests,sweepstakes,other info,";
                //    strContactVia = "email,direct mail,";
                //    strContactFreq = "as needed,";
                //}

                //PROCESS ENTRY.
                strEntryReturn = CGOptin.ProcessOptIn(intSiteID, intEventID, strFName, strLName, strEmail, strDOBdd, strDOBmo, strDOByr,
                                          strStreet1, strStreet2, strProvince, strCity, strPostalCode, strPhone, strMobilePhone,
                                          strGender, strURL, strRemoteAddr, strUserAgent, strEventCode,
                                          strContactContent, strContactVia, strContactFreq, strURL.Replace("http://", ""), strComment, strHookID);

                //Process GDPR
                strTraitReturn = CGOptin.ProcessTraits(Convert.ToInt32(strEntryReturn.Replace("TRUE", "")), intEventID, strRemoteAddr, strUserAgent, strURL, intGDPRID, strGDPR);

                //SEND EMAIL TO CRS.

                //mail for support rep
                strMailReturn = CGMail.MultiPartMimeContact(strEmail, strCrs_ContactFrom, strCrs_Email, strCrs_Subject,
                                        strFName, strLName, strCrs_Age, strStreet1, strStreet2, strCity, strProvince,
                                        strPostalCode, strCountry, strPhone, strComment, strUPC_Code, strMFG_Code, strCrs_Website, strCrs_Opt_In_Out);

                if (strEntryReturn.IndexOf("TRUE") != -1 && strMailReturn == "TRUE")
                {
                    ContactFormThanks.Visible = true;
                    ContactFormPanel.Visible = false;
                    Master.Page.Title = "MegRhythm&reg; - Contact Us - Confirmation";
                }
                else
                {
                    //Oops!
                    litError.Text = "<span style=\"color:#C00\">An error has occurred.<br />" + strEntryReturn + "<br /><br />" + strMailReturn + "</span>";
                    ContactFormError.Visible = true;
                    ContactFormPanel.Visible = false;
                }

            }

           
        }

        protected void DOBValidate(object source, ServerValidateEventArgs args)
        {
            try
            {
                if (dd.SelectedValue.ToString() != "" && mm.SelectedValue.ToString() != "" && yyyy.SelectedValue.ToString() != "")
                {
                    int Day = int.Parse(dd.SelectedValue.ToString());
                    int Month = int.Parse(mm.SelectedValue.ToString());
                    int Year = int.Parse(yyyy.SelectedValue.ToString());

                    DateTime Test = new DateTime(Year, Month, Day);

                    //Are they 13 years of age or older?
                    if (Test > DateTime.Now.AddYears(-13))
                    {
                        args.IsValid = false;
                        //redirect to sorry page
                        //Response.Redirect("sorry.aspx");
                    }
                    else
                    {
                        args.IsValid = true;
                    }
                }
                else
                {
                    //nothing selected, so let it pass since it's optional now.
                    args.IsValid = true;

                }

            }
            catch (FormatException Arg)
            {
                string error = Arg.ToString();
                // item not selected (couldn't convert int)
                args.IsValid = false;
            }
            catch (ArgumentOutOfRangeException Arg)
            {
                string error = Arg.ToString();
                // invalid date (31 days in February)
                args.IsValid = false;
            }
        }


        protected string GetCRSAge(DateTime birthday)
        {
            string strReturn = "";

            // set current time
            DateTime now = DateTime.Today;

            // get the year difference
            int years = now.Year - birthday.Year;

            // subtract another year if birthday hasn't yet occurred in same year
            if (now.Month < birthday.Month || (now.Month == birthday.Month && now.Day < birthday.Day))
                --years;

            strReturn = years.ToString();
            //Response.Write(strReturn);
            return strReturn;
        }

        public void CheckHookID()
        {
            //Find hook id passed in query string, if available.  This is a means by which we can track promo source (e.g. Facebook)
            string strHookIDRequest = Request.QueryString["hook"] == null ? System.Configuration.ConfigurationManager.AppSettings["HookID"].ToString() : Request.QueryString["hook"].ToString();

            hfHookID.Value = strHookIDRequest;
        }
      
    }
}