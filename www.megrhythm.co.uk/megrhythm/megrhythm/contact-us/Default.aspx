﻿<%@ Page Title="Contact Us" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="megrhythm.Contact_Us.Default" %>
<%@ Register TagPrefix="skm" Namespace="skmValidators" Assembly="skmValidators" %>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">

    <div class="main-container imgback">
        <div class="wrapper internal clearfix">
            <div id="wrapper-body">
                
                <section>
                    <h1>Contact Us</h1>

                    <div class="contactText">
                            <h2>By Mail</h2>
                            <p>
                                MegRhythm Consumer Relations<br />
                                130 Shaftesbury Avenue<br />
                                London W1D 5EU<br /></p>

                            <h2> By Phone</h2>
                            <p>
                                <span>Phone: 0800 0086421</span>.<br />
                                Fax: 020 7851 9888.</p>

                            <h2>Online</h2>  
                   
                            <p>Fill out the form <span class="contactDirection"></span>.</p>
                     </div>

                    <asp:Panel ID="ContactFormPanel" runat="server">
                        <div id="contactFormLeftContainer">
                            <div class="EmailWrapper">
                                <div><h3>What is your e-mail-address?</h3>
                                </div>
                                <div class="leftField">
                                    <asp:Label ID="lblEmail" runat="server" AssociatedControlID="Email">Email Address*:</asp:Label>
                                    <asp:RequiredFieldValidator ForeColor="Red" ID="rf_email" runat="server" ErrorMessage="required<br />" Display="Dynamic" ControlToValidate="Email"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rf_email2" runat="server" Display="Dynamic"
                                        ErrorMessage="<br />Please enter a valid Email Address." ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true"></asp:RegularExpressionValidator>
                                    <asp:TextBox ID="Email" runat="server" CssClass="textboxes" required="required" MaxLength="150"></asp:TextBox>

                                </div>

                                <div class="rightField">
                                    <asp:Label ID="RetypeEmailLbl" runat="server" Text="Re-type Email*" AssociatedControlID="RetypeEmail"></asp:Label>
                                    <asp:RequiredFieldValidator ID="RetypeValidator1" runat="server" ErrorMessage="Please re-type your Email Address.<br />"
								    ControlToValidate="RetypeEmail" EnableClientScript="true" SetFocusOnError="true"
								    Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
							    <asp:CompareValidator ID="RetypeValidator2" Text="Make sure you retyped the Email correctly.<br />"
								    ControlToValidate="RetypeEmail" ControlToCompare="Email" EnableClientScript="true"
								    SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" runat="server" />
							    <asp:TextBox ID="RetypeEmail" runat="server" required="required" MaxLength="150" CssClass="textboxes"></asp:TextBox>
							

                                </div>
                            </div>

                            <div class="clearfix"></div>
                               
                            <div class="NameWrapper">
                                <div><h3>Would you like to tell us more about you?</h3>
                                    <p>For example: Last Name, First Name </p>
                                </div>
                                <div class="leftField">
                                    <div class="mmSweet">
                                        <asp:Label ID="lblFrstNme" runat="server" AssociatedControlID="FrstNme">First Name*:</asp:Label>
                                        <asp:TextBox ID="FrstNme" runat="server" CssClass="textboxes"></asp:TextBox>
                                    </div>

                                    <asp:Label ID="lblFName" runat="server" AssociatedControlID="FName">First Name:</asp:Label>
                                
                                    <asp:TextBox ID="FName" runat="server" CssClass="textboxes" MaxLength="50"></asp:TextBox>
                                </div>

                                <div class="rightField">
                                    <asp:Label ID="lblLName" runat="server" AssociatedControlID="LName">Last Name:</asp:Label>
                              
                                    <asp:TextBox ID="LName" runat="server" CssClass="textboxes" MaxLength="50"></asp:TextBox>

                                </div>
                            </div>
                            <div class="clearfix"></div>

                            
                            <div class="leftField">
                                <asp:Label ID="Address1Lbl" runat="server" Text="Address 1" AssociatedControlID="Address1"></asp:Label>
                               
							<asp:RegularExpressionValidator ID="Address1Validator2" runat="server" ErrorMessage="Please do not use special characters in the Address 1 field.<br />"
								ValidationExpression="^[^<>]+$" ControlToValidate="Address1" EnableClientScript="true"
								SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
							<asp:TextBox ID="Address1" runat="server" MaxLength="50" CssClass="textboxes"></asp:TextBox>
							
                            </div>

                            <div class="rightField">
                                <asp:Label ID="Address2Lbl" runat="server" Text="Address 2" AssociatedControlID="Address2"></asp:Label>
                                <asp:RegularExpressionValidator ID="Address2Validator1" runat="server" ErrorMessage="Please do not use special characters in the Address 2 field.<br />"
								ValidationExpression="^[^<>]+$" ControlToValidate="Address2" EnableClientScript="true"
								SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
							<asp:TextBox ID="Address2" runat="server" MaxLength="50" CssClass="textboxes"></asp:TextBox>
							

                            </div>

                            <div class="clearfix"></div>

                            <div class="leftField">
                                <asp:Label ID="CityLbl" runat="server" Text="City" AssociatedControlID="City"></asp:Label>
                              
					
							<asp:TextBox ID="City" runat="server" MaxLength="50" CssClass="textboxes"></asp:TextBox>
							
                            </div> 

                             <div class="rightField">
                                 <div class="localeContainer">
                                    <asp:Label ID="PostalCodeLbl" runat="server" Text="Postal Code" AssociatedControlID="PostalCode"></asp:Label>
                                    
							    <asp:TextBox ID="PostalCode" runat="server" MaxLength="20" CssClass="textboxes"></asp:TextBox>
							    
							    <%-- The following RegEx Validators is for US Zip/Postal codes.  IMPORTANT: 
								     Please test against your user base before using as you may need to modify it for other formats!  --%>
							    <%--<asp:RegularExpressionValidator ID="PostalCodeValidator2" runat="server" 
								    ErrorMessage="Please enter a valid 5-digit ZIP code." ValidationExpression="^\d{5}(-\d{4})$" 
								    ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true" 
								    Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>--%>
                                </div>
                             </div>

                            <div class="clearfix"></div>

                            <div class="leftField">
                                <asp:Label ID="lblPhone" runat="server" AssociatedControlID="Phone">Phone</asp:Label>
                               
                                <asp:TextBox ID="Phone" runat="server" CssClass="textboxes" MaxLength="14"></asp:TextBox>
                            </div>

                            <div class="rightField">
                                <asp:Label ID="GenderLbl" runat="server" Text="Gender" AssociatedControlID="Gender"></asp:Label>
                               
							<asp:DropDownList ID="Gender" runat="server">
								<asp:ListItem Value="">Select</asp:ListItem>
								<asp:ListItem Value="F">Female</asp:ListItem>
								<asp:ListItem Value="M">Male</asp:ListItem>
							</asp:DropDownList>
							
                                
                            </div>

                           <div class="clearfix"></div>

                            <div class="leftField">
                                <asp:Label ID="DOBLbl" runat="server" Text="Birthdate" AssociatedControlID="yyyy"></asp:Label>
                                <asp:CustomValidator ID="DOBValidator" runat="server" ErrorMessage="Please enter a valid date.<br />"
								OnServerValidate="DOBValidate" EnableClientScript="false" Display="Dynamic" CssClass="errormsg validdate"></asp:CustomValidator>
							
							<asp:DropDownList ID="yyyy" runat="server" CssClass="inputSelectBDay"></asp:DropDownList>
							<asp:DropDownList ID="mm" runat="server" CssClass="inputSelectBDay"></asp:DropDownList>
							<asp:DropDownList ID="dd" runat="server" CssClass="inputSelectBDay"></asp:DropDownList>
							
                            </div>

                            <div class="clearfix"></div>

                        <div class="clearfix"></div>

                        <div id="contactFormRightContainer">

                            <div class="QuestionWrapper">
                                
                                <div><h3>How can we help you?</h3></div>

                                <asp:Label ID="lblQuestionComment" runat="server" AssociatedControlID="QuestionComment">Comments*:</asp:Label>
                                <asp:RequiredFieldValidator ID="rfQuestionComment" runat="server" ErrorMessage="Please enter your comment/question.<br />"
								    ControlToValidate="QuestionComment" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="QuestionComment" runat="server" TextMode="MultiLine"></asp:TextBox>

                                <asp:HiddenField ID="hfHookID" runat="server" />

                                <div class="optincontainer">                                    
                                            <ul>
		                                            <li>
		                                                <asp:CheckBox ID="bioreGDPR" runat="server" />
			                                            <asp:Label ID="bioreoptinLabel" runat="server" Text="* Yes, I would like to be contacted by MegRhythm regarding my request above. I agree to the use of my personal data according to our <a href='/privacy/' target='_blank' style='text-decoration:underline'>privacy policy</a>." AssociatedControlID="bioreGDPR" CssClass="siteOptinChkbox" />

			                                            <div class="ErrorContainer">
                                    			            <skm:CheckBoxValidator ID="cbv_bioreGDPR" runat="server" ControlToValidate="bioreGDPR" ErrorMessage="*Required" CssClass="errormsg" Display="Dynamic"></skm:CheckBoxValidator>
				                                        </div>

		                                            </li>
                                            </ul>
                                        </div>
                           
                                <asp:Button ID="submitform" runat="server" Text="Submit Form" CssClass="applyNow" OnClick="Contact_Click" />
                                <div id="process_message" runat="server">
                                    <p><em><strong>Processing your submission...</strong></em></p>
                                </div>

                            </div>

                            <br class="clearBoth" />
                            <br />

                        </div>
                        <br class="clearBoth" />
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ContactFormThanks" runat="server">

                        <h1>Thank you!  Your message has been sent.  Someone will be contacting you shortly.</h1>

                    </asp:Panel>
                    <asp:Panel ID="ContactFormError" CssClass="shortformContent" runat="server">
                        <h2>An error occurred.  Please try again later.  If this message persists, call 0800 0086421.</h2>
                        <asp:Literal ID="litError" runat="server"></asp:Literal>
                    </asp:Panel>


                </section>

            </div>
        </div>

    </div>

       <script type="text/javascript">
           $(document).ready(function () {
               $('.help-tip').click(function () {
                   if ($(this).find('p').hasClass('helpTipOn')) {
                       $(this).find('p').removeClass('helpTipOn');
                   }
                   else {
                       $(this).find('p').addClass('helpTipOn');

                   }
                   return false;
               });
           });
    </script>
</asp:Content>

