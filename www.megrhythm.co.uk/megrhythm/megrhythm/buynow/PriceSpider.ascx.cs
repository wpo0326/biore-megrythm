﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace megrhythm.buynow
{
    public partial class PriceSpider : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string strSku = "";

            string urlPath = Request.Url.GetLeftPart(UriPartial.Path);

            Uri pageURI = new Uri(Request.Url.OriginalString);
            string urlType = pageURI.Segments[1].Replace("/", "");
            string urlResult = pageURI.Segments[2].Replace("/", "");

            

            using (StreamReader sr = new StreamReader(Server.MapPath("~/buynow/PriceSpider.json")))
            {
                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Spider> products = serializer.Deserialize<List<Spider>>(sr.ReadToEnd());

                foreach (var product in products)
                {
                    if (product.productType == urlType && product.productName.ToLower() == urlResult)
                    {
                        strSku = product.productSku;
                        break;
                    }

                }

            }

            System.Web.UI.WebControls.Literal litPriceSpiderButtonHldr = null;
            Literal ctl = (Literal)this.Page.Master.FindControl("MainContent").FindControl("litPriceSpiderButton");      //Parent.Page.FindControl("MainContent_litPriceSpiderButton");

            while (true)
            {
                //Response.Write("we got this far" + ctl);
                litPriceSpiderButtonHldr = (System.Web.UI.WebControls.Literal)ctl.FindControl("litPriceSpiderButton");
                if (litPriceSpiderButtonHldr != null && strSku !="")
                {
                    litPriceSpiderButtonHldr.Text = "<div class=\"ps-widget\" ps-sku=\"" + strSku + "\"></div>";
                    //Response.Write("foo");
                }
                //else { Response.Write("meh"); }
              
                break;
            }
            

            

        }
    }

    public class Spider
    {
        public string productType;
        public string productName;
        public string productSku;
    }
}