﻿<%@ Page Title="Privacy Policy" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="megrhythm.privacy.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <meta name="description" content="Privacy policy for Ban 24 Hour Odor and Wetness Protection with roll-ons and invisible solids">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="main-container imgback">
        <div class="wrapper internal clearfix">
            <div id="wrapper-body">

                <section>
                    <h1>Privacy Policy</h1>

                    <p>
                        <b>Last revised May 9<sup>th</sup>, 2018</b>
                    </p>
                    <p>
                        MegRhythm, 130 Shaftesbury Avenue, London, W1D 5EU ("<b>Kao Company</b>"
    or "<b>we</b>" or "<b>our</b>") and each of its
affiliates and subsidiaries in the EMEA region collectively, the "<b>Kao Group</b>") takes data privacy seriously. This Privacy
Policy informs the users of<a href="http://www.megrhythm.co.uk">www.megrhythm.co.uk</a>  and any
    other Kao Company-owned websites or mobile applications on which this
    Privacy Policy is displayed ("<b>Website</b>") how we, as
controller within the meaning of the General Data Protection Regulation ("<b>GDPR</b>") collect and process the personal data and other
    information of such users in connection with their usage of the Website.
                    </p>
                    <p>
                        Note that other Kao Group websites or mobile apps may be governed by other
    privacy policies.
                    </p>
                    <p>
                        <b>1. </b>
                        <b>Categories of Personal Data and Processing Purposes -        <em>What personal data do we process about you and why?</em>
                        </b>
                    </p>
                    <p>
                        <b>1.1 Metadata</b>
                    </p>
                    <p>
                        You may use the Website without providing any personal data about you. In
    this case, we will collect only the following metadata that result from
    your usage of the Website: browser type and version, operating system and
    interface, website from which you are visiting us (referrer URL),
    webpage(s) you are visiting on our Website, date and time of accessing our
    Website, and internet protocol (IP) address.
                    </p>
                    <p>
                        Your IP address will be used to enable your access to our Website. Once the
    IP address is no longer necessary for this purpose, we will shorten your IP
    address by removing the last octet of your IP address. The metadata,
    including the shortened IP address, will be used to improve the quality and
    services of our Website and services by analysing the usage behaviour of
    our users.
                    </p>
                    <p>
                        <b>1.2 Account</b>
                    </p>
                    <p>
                        If you create an account on our Website you will be asked to provide the
    following personal data about you: name, gender (salutation), date of
    birth, postal address, email address, telephone number, selected password
    for your account, payment details, invoicing and delivery address and your
    preferences in receiving marketing from us (voluntary). We process such
    personal data for purposes of account administration, answering your
    queries or information requests, providing desired products or services,
    providing you with marketing materials where you have provided consent for
    us to do so, to the extent permitted by applicable law, analysing your
    interests for marketing purposes, improving our Website according to usage
    patterns, and for technical administration or other purposes to which you
    have agreed.
                    </p>
                    <p>
                        <b>1.3 Product Orders</b>
                    </p>
                    <p>
                        If you order a product via our Website we collect and process the following
    personal data about you: name, gender (salutation), postal address, email
    address, telephone number, payment details, invoicing and delivery address,
    type and amount of product, purchase price, order date, order status,
    product returns, customer care requests, and your preferences in receiving
    marketing from us (voluntary). We process such personal data for purposes
    of carrying out the contractual relationship and the product order,
    providing customer care services, compliance with legal obligations,
    defending, establishing and exercising legal claims, providing you with
    marketing materials where you have provided consent for us to do so, to the
    extent permitted by applicable law, and analysing your interests for
    marketing purposes.
                    </p>
                    <p>
                        <b>1.4 Competitions</b>
                    </p>
                    <p>
                        If you participate in a competition, we collect and process the following
    personal data about you: name, gender (salutation), postal address, email
    address, telephone number and selection as winner. We process such personal
    data for purposes of carrying out the competition, informing the winner,
    delivering the prize to the winner, carrying out the event, and providing
    you with marketing materials where you have provided us consent to do so,
    to the extent permitted by applicable law, and analysing your interests for
    marketing purposes.
                    </p>
                    <p>
                        <b>1.5 Newsletter</b>
                    </p>
                    <p>
                        If you request to receive our newsletter, we collect and process the
    following personal data about you: name, address, date of birth and email
    address, and your preferences in receiving marketing from us via emails,
    SMS or postal mails (voluntary). We process such personal data for purposes
    of providing the newsletter and other marketing materials to the extent
    permitted by applicable law and where you have provided us consent to do
    so, and analysing your interests for marketing purposes.
                    </p>
                    <p>
                        <b>1.6 Contact Us</b>
                    </p>
                    <p>
                        On our website, we offer you the opportunity to contact us via a contact
    form. For this we need the following personal data from you: name, email,
    telephone number, address (optional), date of birth (optional) and gender
    (optional). The personal data that you provide us in the context of this
    contact request will only be used to answer your inquiry and for the
    technical administration thereof. The transfer to third parties does not
    take place. Your personal data will be deleted as soon as we have processed
    your request or you revoke the consent you have given.
                    </p>
                    <p>
                        <u></u>
                    </p>
                    <p>
                        <b>2. Processing Basis and Consequences -
        <em>What is the legal justification for processing your personal data
            and what happens if you choose not to provide it?
        </em>
                        </b>
                    </p>

                    <ul>
 
                        <li>
                        We rely on the following legal grounds for the collection, processing, and
    use of your personal data:
                    </li>
                    <li>
                        your consent to the processing of your data for one or more specific
    purposes;
                    </li>
                    <li>
                        the processing is necessary for the performance of a contract to which
    you are a party or to take steps at your request prior to entering into a
    contract;
                    </li>
                    <li>
                        the processing is necessary for compliance with a legal obligation to
    which we are subject;
                    </li>
                    <li>
                        the processing is necessary for the purposes of the legitimate interests
    pursued by us or by a third party, except where your interests or
    fundamental rights and freedoms do not override those interests;
                    </li>
                    <li>
                        the provision of your personal data is required by a statutory or
    contractual obligation. The provision of your personal data is necessary to
    enter into a contract with us or to receive our services/products as
    requested by you. The provision of your personal data is voluntary for you.
                    </li>
                    </ul>
                    <p>
                        Not providing your personal data may result in disadvantages for you, for
    example, you may not be able to receive certain products and services.
    However, unless otherwise specified, not providing your personal data will
    not result in legal consequences for you.
                    </p>
                    <p>
                        <b>3. Categories of Recipients and International Transfers -
        <em>Who do we transfer your personal data to and where are they
            located?
        </em>
                        </b>
                    </p>
                    <p>
                        We may transfer your personal data to third parties for the processing
    purposes described above as follows:<b></b>
                    </p>
                    <ul>
                        <li>
                            <b>Within the Kao Company: </b>
                            Our parent entity, the Kao Corporation, in Japan and each of its
        affiliates and subsidiaries (each affiliate or subsidiary including us
referred to as "<b>Kao Company</b>"; collectively, the "<b>Kao Group</b>") within the global        <a href="http://www.kaocareers.com/#brands">Kao Group</a> may receive
        your personal data as necessary for the processing purposes described
        above. Depending on the categories of personal data and the purposes
        for which the personal data has been collected, different internal
        departments within the Kao Company may receive your personal data. For
        example, our IT department may have access to your account data, and
        our eCommerce and sales departments may have access to your account
        data or data relating to product orders. Moreover, other departments
        within the Kao Company may have access to certain personal data about
        you on a need to know basis, such as the legal department, the finance
        department or internal auditing.
                        </li>
                        <li>
                            <b>With data processors:</b>
                            Certain third parties, whether affiliated or unaffiliated, may receive
        your personal data to process such data under appropriate instructions
        ("<b>Processors</b>") as necessary for the processing
        purposes described above, such as website service providers, order
        fulfilment providers, customer care providers, marketing service
        providers, IT support service providers, and other service providers
        who support us in maintaining our commercial relationship with you. The
        Processors will be subject to contractual obligations to implement
        appropriate technical and organizational security measures to safeguard
        the personal data, and to process the personal data only as instructed.
                        </li>
                        <li>
                            <b>Other recipients:</b>
                            We may transfer - in compliance with applicable data protection law -
        personal data to law enforcement agencies, governmental authorities,
        judicial authorities, legal counsel, external consultants, or business
        partners. In case of a corporate merger or acquisition, personal data
        may be transferred to the third parties involved in the merger or
        acquisition. We will not disclose your personal data to third parties
        for advertising or marketing purposes or for any other purposes without
        your permission.
                        </li>
                    </ul>
                    <p>
                        Any access to your personal data is restricted to those individuals that
    have a need-to-know in order to fulfill their job responsibilities.
                    </p>
                    <p>
                        <b>UNITED STATES OF AMERICA</b>
                    </p>
                    <p>
                        The website and our related databases are maintained in the United States
    of America. By using the website, you freely and specifically give us your
    consent to collect and store, your information in the United States and to
    use your information as specified within this policy.
                    </p>
                    <p>
                        The transfer certified under the EU-U.S. Privacy Shield is thereby
    recognized as providing an adequate level of data protection from a
    European data protection law perspective. Other recipients might be located
    in countries which do not adduce an adequate level of protection from a
    European data protection law perspective. We will take all necessary
    measures to ensure that transfers out of the EEA are adequately protected
    as required by applicable data protection law. With respect to transfers to
    countries not providing an adequate level of data protection, we will base
    the transfer on appropriate safeguards, such as standard data protection
    clauses adopted by the European Commission or by a supervisory authority,
    approved codes of conduct together with binding and enforceable commitments
    of the recipient, or approved certification mechanisms together with
    binding and enforceable commitments of the recipient. You can ask for a
    copy of such appropriate safeguards by contacting us as set out in Section
    7 below.
                    </p>
                    <p>
                        <b>4. Retention Period - <em>How long do we keep your personal data?</em>
                        </b>
                    </p>
                    <p>
                        Your personal data will be retained as long as necessary to fulfil the
    purposes we collected it for, including for the purposes of satisfying any
    legal, accounting or reporting requirements. To determine the appropriate
    retention period for personal data, we consider the amount, nature, and
    sensitivity of the personal data, the potential risk of harm from
    unauthorised use or disclosure of your personal data, the purposes for
    which we process your personal data and whether we can achieve those
    purposes through other means, and the applicable legal requirements.
                    </p>
                    <p>
                        <b>5. Your Rights -        <em>What rights do you have and how can you assert your rights?</em>
                        </b>
                    </p>
                    <p>
                        <b>Right to withdraw your consent:</b>
                        If you have declared your consent regarding certain collecting, processing
    and use of your personal data (in particular, regarding the receipt of
    direct marketing communication via email, telephone/SMS and postal), you
    can withdraw this consent at any time with immediate effect. Such a
    withdrawal will not affect the lawfulness of the processing prior to the
    consent withdrawal. Please contact us as stated in Section 7 below to
    withdraw your consent. Further, you can object to the use of your personal
    data for the purposes of marketing without incurring any costs other than
    the transmission costs in accordance with the basic tariffs.
                    </p>
                    <p>
                        <b>Additional data privacy rights:</b>
                        Pursuant to applicable data protection law, you may have the right to: (i)
    request access to your personal data; (ii) request rectification of your
    personal data; (iii) request erasure of your personal data; (iv) request
    restriction of processing of your personal data; (v) request data
    portability; and/or (vi) object to the processing of your personal data
    (including objection to profiling).
                    </p>
                    <p>
                        Please note that these aforementioned rights might be limited under the
    applicable local data protection law. Below please find further information
    on your rights to the extent that the GDPR applies:
                    </p>
                    <ul>
                        <li>
                            <b>Right to request access to your personal data: </b>
                            You may have the right to obtain from us confirmation as to whether or
        not personal data concerning you is being processed, and, where that is
        the case, to request access to the personal data. This access
        information includes – inter alia – the purposes of the processing, the
        categories of personal data concerned, and the recipients or categories
        of recipient to whom the personal data have been or will be disclosed.
        However, this is not an absolute right and the interests of other
        individuals may restrict your right of access.
                        </li>
                    </ul>
                    <p>
                        You may have the right to obtain a copy of the personal data undergoing
    processing free of charge. For further copies requested by you, we may
    charge a reasonable fee based on administrative costs.
                    </p>
                    <ul>
                        <li>
                            <b>Right to request rectification: </b>
                            You may have the right to obtain from us the rectification of
        inaccurate personal data concerning you. Depending on the purposes of
        the processing, you may have the right to have incomplete personal data
        completed, including by means of providing a supplementary statement.
                        </li>
                        <li>
                            <b>Right to request erasure (right to be forgotten): </b>
                            Under certain circumstances, you may have the right to obtain from us
        the erasure of personal data concerning you and we may be obliged to
        erase such personal data.
                        </li>
                        <li>
                            <b>Right to request restriction of processing: </b>
                            Under certain circumstances, you may have the right to obtain from us
        restriction of processing your personal data. In such case, the
        respective data will be marked and may only be processed by us for
        certain purposes.
                        </li>
                        <li>
                            <b>Right to request data portability: </b>
                            Under certain circumstances, you may have the right to receive the
        personal data concerning you, which you have provided to us, in a
        structured, commonly used and machine-readable format and you may have
        the right to transmit those data to another entity without hindrance
        from us.
                        </li>
                        <li>
                            <b>Right to object: </b>
                            Under certain circumstances, you may have the right to object, on
        grounds relating to your particular situation, at any time to the
        processing of your personal data by us and we can be required to no
        longer process your personal data. Such right to object may especially
        apply if we collect and process your personal data for profiling
        purposes in order to better understand your interests in our products
        and services or for direct marketing. If you have a right to object and
        you exercise this right, your personal data will no longer be processed
        for such purposes by us. You may exercise this right by contacting us
        as stated in Section 7 below. Such a right to object may, in
        particular, not exist if the processing of your personal data is
        necessary to take steps prior to entering into a contract or to perform
        a contract already concluded. If you no longer want to receive direct
        marketing via email, telephone/SMS, and postal, you need to withdraw
        your consent as explained at the start of Section 5.
                        </li>
                    </ul>
                    <p>
                        To exercise your rights, please contact us as stated under Section 7 below.
    You also have the right to lodge a complaint with the competent data
    protection supervisory authority.
                    </p>
                    <p>
                        <b>6. Cookies and other tracking technologies</b>
                    </p>
                    <p>
                        This Website uses cookies and other tracking technologies.
                    </p>
                    <p>
                        Kao may record your interactions with our advertisements, our web sites,
    emails or other applications we provide using Clickstream Data and Cookies.
    “Clickstream Data” is a recording of what you click on while browsing this
    web site. This data can tell us the type of computer and browsing software
    you use and the address of the web site from which you linked to this web
    site.
                    </p>
                    <p>
                        “Cookies” are small text files that are placed on your computer by a web
    site for the purpose of facilitating and enhancing your communication and
    interaction with that web site, remembering your preferences and collecting
    aggregate (i.e., not personally identifiable) information. Many web sites,
    including ours, use cookies for these purposes.
                    </p>
                    <p>
                        The web site may also include cookies set by third parties, including
    Google, which 1) helps us measure the performance of the web site, 2)
    allows us to share relevant information and advertising with you based on
    your and other visitors’ past visits to this web site when you surf the
    web, and 3) helps us measure your interactions with any Kao advertising you
    see on other sites. Finally, Kao will use data from Google’s Interest-based
    advertising or third-party audience data, such as age, gender and
    interests, together with our own data, to deliver relevant advertising to
    you based upon your demographic profile and interests. Specifically, this
    web site has implemented the following Google Analytics features to support
    our advertising across the web: Remarketing (to show you ads on sites
    across the Internet), Google Display Network Impression Reporting, the
    DoubleClick Campaign Manager Integration, and Google Analytics Demographics
    and Interest Reporting.
                    </p>
                    <p>
                        You can opt-out of Google Analytics for Display Advertising and customize
    Google Display Network ads by using the Ads Settings, or by using the
    Google Analytics opt-out Browser add-on. You can find more information
    about Google Analytics here.
                    </p>
                    <p>
                        As an alternative to the browser add-on and especially for mobile browsers,
    please click on the following link to set an opt-out cookie. This opt-out
    cookie prevents detection by Google Analytics within this website.
    <a href="http://www.megrhythm.co.uk/privacy/?google-analytics-opt-out=true">http://www.megrhythm.co.uk/privacy/?google-analytics-opt-out=true
    </a>
                        
                    </p>
                    <p>
                        You may stop or restrict the placement of cookies on your computer or flush
    them from your browser by adjusting your web browser preferences and
    browser plug-in settings, in which case you may still use our web site, but
    it may interfere with some of its functionality.
                    </p>
                    <p>
                        <b>Cookies on our Site</b>
                    </p>
                    <p>
                        We're giving you this information as part of our initiative to comply with
    recent legislation, and to make sure we're honest and clear about your
    privacy when using out website. We know you'd expect nothing less from us,
    and please be assured that we're working on a number of other privacy and
    cookie-related improvements to the website.
                    </p>
                    <p>
                        Here's a list of the main cookies we use and what we use them for.
                    </p>


                    <table class="cookieTbl">
                    <tr>
                        <td><b>cookie and status</b></td><td><b>cookie name</b></td><td><b>party</b></td><td><b>purpose</b></td>
                    </tr>
                    <tr>
                        <td>google analytics</td><td>utma</td><td>1st</td><td>google analytics helps us see how
    visitors get to navigate around our website, allowing us to constantly
    improve the site</td>
                    </tr>
                    <tr>
                        <td>(optional)</td><td>_utmb</td><td></td><td rowspan="3">the data collected is<br /><br />anonymous (which adverts you might have seen, which browser you’re
    using, the date/time of your visit, demographic information and which pagespseudonymous (your ip address).<br /><br /> aggregated and anonymous data can be shared with 3rd parties in the form of
    market place /industry vertical performance insight and benchmarking.<br /><br /> to manage google analytics cookies see
    https://tools.google.com/dlpage/gaoptout/ (opens in a new window – please
    note that we’re not responsible for the content of external websites).</td>
                    </tr>
                    <tr>
                        <td></td><td>_utmc</td><td></td>
                    </tr>
                        <tr>
                        <td></td><td>_utmz</td><td></td>
                    </tr>
                        <tr>
                        <td>new relic</td><td>jsessionid</td><td>3rd</td><td>new relic enables performance
    monitoring on our sites to ensure you, the customer, have the best
    experience possible when engaging with our brand.</td>
                    </tr>

                        <tr>
                        <td>(optional)</td><td></td><td></td><td>the data collected is<br /><br /> anonymous (which adverts you might have seen, which browser you’re using,
    the date/time of your visit, demographic information and which pages you
    viewed) or<br /><br />pseudonymous (your ip address).<br /><br /> aggregated and anonymous data can be shared with 3rd parties in the form of
    market place /industry vertical performance insight and benchmarking.</td>
                    </tr>
                        
                </table>
            
                   
                  
                  
                    <p>
                        <b>Kao “share” tools</b>
                    </p>
                    <p>
                        If you take the opportunity to 'share' Kao content with friends through
    social networks – such as Facebook and Twitter - you may be sent cookies
    from these websites. We don't control the setting of these cookies, so we
    suggest you check the third-party websites for more information about their
    cookies and how to manage them.
                    </p>
                    <p>
                        <b>Managing Cookies</b>
                    </p>
                    <p>
                        If cookies aren't enabled on your computer, it will mean that some of the
    site may not function appropriately providing a poor experience. You will,
    however, still be able to view some content on our website if it's just a
    bit of research you're after. If you are interested in managing cookies on
    your computer, then reference our how to enable cookies guide here.
                    </p>
                    <p>
                        <b>Further information about cookies</b>
                    </p>
                    <p>
                        If you'd like to learn more about cookies in general and how to manage
    them, visit <a href="http://www.aboutcookies.org">www.aboutcookies.org</a>
                        (opens in a new window – please note that we’re not responsible for the
    content of external websites).
                    </p>
                    <p>
                        <b>HOW WE USE YOUR INFORMATION</b>
                    </p>
                    <p>
                        Kao uses your information to understand your needs and provide better
    products and services. Kao may use data collected from you to personalize
    your web site experience, tailor future communications, and send you
    targeted offers as described below. Occasionally, Kao may use your
    personally identifiable information to contact you for market research or
    to provide you with marketing information we think would be of particular
    interest. We will always give you the opportunity to opt out of receiving
    such contacts.
                    </p>
                    <p>
                        Kao may share the personally identifiable information you provide with
    other Kao divisions or affiliates, provided that, such other divisions or
    affiliates have privacy practices that are similar to those set forth in
    this policy.
                    </p>
                    <p>
                        Kao may permit its vendors and subcontractors to access your personally
    identifiable information, but they are only permitted to do so in
    connection with services they are performing for Kao. They are not
    authorized by Kao to use your personally identifiable information for their
    own benefit. Kao may disclose personally identifiable information as
    required by law or legal process.
                    </p>
                    <p>
                        Kao may disclose personally identifiable information to investigate
    suspected fraud, harassment or other violations of any law, rule or
    regulation, or the terms or policies for the web site.
                    </p>
                    <p>
                        In the event of a sale, merger, liquidation, dissolution, reorganization or
    acquisition of Kao, or a Kao business unit, information Kao has collected
    about you may be sold or otherwise transferred. However, this will only
    happen if the party acquiring the information agrees to use personally
    identifiable information in a manner which is substantially similar to the
    uses described in this policy.
                    </p>
                    <p>
                        Competitions, contests and other promotions may set forth additional uses
    of your personally identifiable information in connection with such
    promotions.
                    </p>
                    <p>
                        <b>THIRD PARTY COLLECTION AND SHARING</b>
                    </p>
                    <p>
                        At times, personally identifiable information may be collected from you on
    behalf of Kao and a third party who is identified at the time of
    collection. This may include co-branded promotions. In such instances, your
    personally identifiable information may be provided to both Kao and such
    third party. While Kao will use your personally identifiable information as
    set forth in this policy, such third party will use your personally
    identifiable information as set forth in their own privacy policy. Such
    third parties may collect information regarding your activities over time
    and across different web sites. Therefore, you should review such policies
    prior to providing your personally identifiable information. Kao is not
    responsible for the actions of such third parties.
                    </p>
                    <p>
                        <b>TARGETED CONTENT AND MESSAGING</b>
                    </p>
                    <p>
                        We believe that content, messages and advertising are more relevant and
    valuable to you when they are based upon your interests, needs and
    demographics. Therefore, we may deliver content, messages and advertising
    specifically to you that are based upon your prior activities on our web
    sites and information provided to us or gathered as described in this
    policy. For example, if you have previously expressed an interest in hair
    care products through your activities on our web site, we may deliver more
    information to you about hair care products than other products for which
    you have not expressed an interest or interacted with on the web site.
    While we may use this information to tailor what we deliver to you, we will
    still handle and secure your personally identifiable information as set
    forth in this policy.
                    </p>
                    <p>
                        Through the use of the Google services (described above), the cookies we
    place on this web site, and cookies placed by third parties on this web
    site or through other sites you visit on the internet, we may cause
    advertisements and content to appear on this web site and elsewhere on the
    internet based upon your activities over time and across different web
    sites. In most instances, Kao is simply a content provider and does not
    directly possess such behavioural information about your online activities.
    You are able to limit such targeted advertising by setting your browser to
    block third party cookies or by visiting
    <a href="http://www.aboutads.info/consumers">www.aboutads.info/consumers
    </a>
                        to learn more about such advertising practices and to exercise options with
respect to such practices at    <a href="http://www.aboutads.info/choices">www.aboutads.info/choices</a>.
                    </p>
                    <p>
                        We do not respond to or honor “do not track” (a/k/a DNT) signals or similar
    mechanisms transmitted by web browsers.
                    </p>
                    <p>
                        <b>7. Questions and Contact Information</b>
                    </p>
                    <p>
                        Should you have any questions or concerns regarding our website or privacy
policy, please email us using the ‘Contact Us’ link on our website.    
                    </p>
                    <p>
                        Alternatively, we can be contacted by telephone +44(0)800 1070 853 (our
    lines are open 9am-5pm (UK Time) Monday to Friday,
                    </p>
                    <p>
                        The postal address for MegRhythm is:
                    </p>
                    <p>
                        Customer Services Team<br />
                    
                        Kao (UK) Ltd<br />
                    
                        130 Shaftesbury Avenue<br />
                    
                        London<br />
                    
                        W1D 5EU<br />

                        United Kingdom
                    </p>
                    <p>
                        For further information and statutory rights, please go to
    <a href="http://www.kao.com/global/en/EU-Data-Subject-Request/">http://www.kao.com/global/en/EU-Data-Subject-Request/
    </a>
                    </p>
                    <p>
                        <b>8. Changes to this Privacy Policy</b>
                    </p>
                    <p style="margin-bottom: 0">
                        We may update this Privacy Policy from time to time in response to changing
    legal, regulatory or operational requirements. We will notify you of any
    such changes, including when they will take effect, by updating the "Last
    revised" date above or as otherwise required by applicable law. Your
    continued use of our Website after any such updates take effect will
    constitute acceptance of those changes. If you do not accept updates to
    this Privacy Policy, you should stop using our Website.
                    </p>
                   
            
                </section>

            </div>
        </div>

    </div>
</asp:Content>
