﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.XPath;

namespace megrhythm.where_to_buy
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            XmlDataSource xdsSource = new XmlDataSource();
            xdsSource.ID = "XmlDataSource1";
            xdsSource.DataFile = "/where-to-buy/wheretobuy.xml";
            xdsSource.XPath = "WTBLinks/Links";

            lvRetailer.DataSource = xdsSource;
            lvRetailer.DataBind();
        }

        protected void lvRetailer_ItemDataBound(object sender, ListViewItemEventArgs e)
        {

            IXPathNavigable navigableDataItem = (IXPathNavigable)((ListViewDataItem)e.Item).DataItem;
            XPathNavigator theNavigator = navigableDataItem.CreateNavigator();

            String linkText = theNavigator.SelectSingleNode("LinkText").Value;
            String storeLinkHref = theNavigator.SelectSingleNode("StoreLink").Value;
            String onlineLinkHref = theNavigator.SelectSingleNode("OnlineLink").Value;
            String titleText = theNavigator.SelectSingleNode("TitleText").Value;
            String fileName = theNavigator.SelectSingleNode("LinkImage/FileName").Value;
            String target = theNavigator.SelectSingleNode("Target").Value;

            //This way allows both titles and alt text
            HyperLink imglink = (HyperLink)e.Item.FindControl("hlRetailer");
            HyperLink storelocatorlink = (HyperLink)e.Item.FindControl("locatorLink");
            HyperLink onlinelink = (HyperLink)e.Item.FindControl("onlineLink");
            if (storeLinkHref != "")
            {
                storelocatorlink.NavigateUrl = storeLinkHref;
                storelocatorlink.Visible = true;
            }
            if (onlineLinkHref != "")
            {
                onlinelink.NavigateUrl = onlineLinkHref;
                onlinelink.Visible = true;
            }
            imglink.NavigateUrl = HttpContext.Current.Request.Url.ToString() + "#";
            imglink.Text = linkText;
            imglink.ToolTip = Server.HtmlDecode(titleText).Replace("&reg;", "®").Replace("&trade;", "™");
            //imglink.Target = target;

            imglink.Attributes.Add("style", "background: url(/images/where-to-buy/" + fileName + ") top left no-repeat;");



        }
    }
}