﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="roll-ons.ascx.cs" Inherits="megrhythm.Content.controls.roll_ons" %>

<div class="prodContainer clearfix">
    <div class="prodList">
        <h2>roll-ons</h2>
        <!--<h3>Glides on clear:  no messy white residue</h3>-->

        <ul class="product-nav-rollons">
            <li><a href="/roll-ons/unscented/" class="unscented">unscented</a><span class="tic"></span></li>
            <li><a href="/roll-ons/regular/" class="regular">regular</a><span class="tic"></span></li>
            <li><a href="/roll-ons/powder-fresh/" class="powder-fresh">powder fresh</a><span class="tic"></span></li>
            <li><a href="/roll-ons/satin-breeze/" class="satin-breeze">satin breeze<sup>&reg;</sup></a><span class="tic"></span></li>
            <li><a href="/roll-ons/fresh-cotton/" class="fresh-cotton">fresh cotton</a><span class="tic"></span></li>
            <li><a href="/roll-ons/simply-clean/" class="simply-clean">simply clean</a><span class="tic"></span></li>
        </ul>
    </div>
</div>
<div class="clearfix"></div>
<script type="text/javascript">
    //windows styles nav show active sub-section
    if (location.pathname.split("/")[1] != "" && location.pathname.split("/")[1] == "roll-ons") {
        if (location.pathname.split("/")[2] != "")
            $('.product-nav-rollons li a[href^="/roll-ons/' + location.pathname.split("/")[2] + '"]').next().addClass('active');
    }
</script>
