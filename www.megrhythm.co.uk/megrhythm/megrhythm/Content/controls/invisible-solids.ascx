﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="invisible-solids.ascx.cs" Inherits="megrhythm.Content.controls.invisible_solids" %>

<div class="prodContainer clearfix">
    <div class="prodList">
        <h2>invisible-solids</h2>
       <!--<h3>24-hour odor and wetness protection</h3>-->

        <ul class="product-nav-invisiblesolids">
            <li><a href="/invisible-solids/unscented/" class="unscented">unscented</a><span class="tic"></span></li>
            <li><a href="/invisible-solids/shower-fresh/" class="shower-fresh">shower fresh<sup>&reg;</sup></a><span class="tic"></span></li>
            <li><a href="/invisible-solids/powder-fresh/" class="powder-fresh">powder fresh</a><span class="tic"></span></li>
            <li><a href="/invisible-solids/sweet-simplicity/" class="sweet-simplicity">sweet simplicty</a><span class="tic"></span></li>
            <li><a href="/invisible-solids/simply-clean/" class="simply-clean">simply clean</a><span class="tic"></span></li>
        </ul>
    </div>
</div>
<div class="clearfix"></div>
<script type="text/javascript">
    //windows styles nav show active sub-section
    if (location.pathname.split("/")[1] != "" && location.pathname.split("/")[1] == "invisible-solids") {
        if (location.pathname.split("/")[2] != "")
            $('.product-nav-invisiblesolids li a[href^="/invisible-solids/' + location.pathname.split("/")[2] + '"]').next().addClass('active');
    }
</script>