﻿<%@ Page Title="Über Bioré | Bioré" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.about_us._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Leer meer over Bioré® Huidverzorging " name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent" class="aboutBlueBG">
            <div class="centeringDiv">
                <div id="content" class="aboutBlueTxt">
                    <div class="titleh1">
                        <h1>Über Bioré</h1>
                        <div id="responseRule"></div>
                    </div>
                    <div class="about-info">     
                        
                        <p><span>Entdecke Bioré – den Experten für freie Poren!</span></p>

                        <p>Verstopfte Poren sind die Hauptursache von Hautunreinheiten. </p> 
<p>Die hochwirksamen Produkte von Bioré  mit Backpulver und Aktivkohle befreien deine Poren von Schmutz und Talg und helfen dir somit Hautunreinheiten und Mitesser effektiv zu bekämpfen. </p>  
<p>Ob als Waschgel, Peeling, Maske oder Clear-Up Strips – Bioré reinigt deine Poren in der Tiefe und sorgt für ein erfrischtes und ebenmäßigeres Hautbild. </p>

                        <p><span>Bioré – free your pores!</span></p>
                                           
                       
                    </div>
                    <img src="../images/about/aboutBG.png" alt="" id="Img1" />                    
                </div>
            </div>
        </div>
    </div>
</asp:Content>

