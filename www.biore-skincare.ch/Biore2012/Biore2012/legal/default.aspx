﻿<%@ Page Title="Legal | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master"
    Language="C#" AutoEventWireup="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
    <style type="text/css">
        #content p
        {
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="about">
        <div id="main">
            <div id="mainContent">
                <div class="centeringDiv">
                    <div id="content">
                        <div class="titleh1">
                            <h1>
                                Rechtliche Hinweise</h1>
                            <div id="responseRule">
                            </div>
                        </div>
                        <p>© 2018 Guhl Ikebana GmbH. Alle Rechte vorbehalten.</p>
                
                <p>Bei der Nutzung dieser Website verstehst und akzeptierst du Folgendes:
                <br /><br />
                Die in dieser Website enthaltenen Informationen (die „Seite“) die eine beliebige Dienstleistung oder ein beliebiges Produkt betreffen sind nur in den Vereinigten Staaten von Amerika anwendbar und ausschließlich für deinen persönlichen, nicht kommerziellen Nutzen bestimmt. Alle Titel, Namen und Graphiken die in Zusammenhang mit dieser Website genutzt werden sind Eigentum der Firma Kao oder deren Lizenznehmer, ausgenommen im Falle gegenteiliger Bestimmungen. Alle Informationen dieser Website sind urheberrechtlich geschütztes Eigentum der Firma Kao oder deren Lizenznehmer. Vervielfältigungen, Publikationen oder Verteilung jeglicher Informationen dieser Website sind strengstens verboten, wenn diese über das zur Nutzung dieser Website erforderliche Maß hinausgehen. Hiervon ausgenommen ist die Tatsache, dass der Nutzer berechtigt ist, für persönlichen und/oder nicht geschäftlichen Nutzen einen Download vorzunehmen und eine Kopie zu machen.
Informationen zu den Produkten und Dienstleistungen der Firma Kao unterliegen Änderungen ohne vorhergehende Mitteilung. Manche Produkte und Dienstleistungen sind in bestimmten Regionen nicht erhältlich. Die in der Website enthaltenen Informationen dienen ausschließlich der allgemeinen Information. Auf der Website ausgesprochene Ansichten und Sichtweisen entsprechen nicht unbedingt den Ansichten und Sichtweisen der Firma Kao. Vermerkte Produkte und/oder Firmen dürfen nicht als Befürwortung oder Aussage der Zugehörigkeit für/zu diesen Produkten und/oder Firmen verstanden werden.

                <br /><br />
               Die in der Website enthaltenen Informationen dienen ausschließlich der allgemeinen Information, sie stellen keinen Ersatz für medizinisch professionelle Beurteilungen, Diagnose oder Behandlung dar. Das Vertrauen in die Angaben dieser Website geschieht auf eigenes Risiko. Solltest du Fragen oder Anliegen zu medizinischen oder gesundheitlichen Belangen haben, muss ein Arzt aufgesucht werden.
                <br /><br />
                Der Gebrauch von Suchmaschinen, Robottern, Datamining oder sonstigen automatischen Mitteln oder Programmen zum Katalogisieren, Herunterladen, Speichern oder Reproduzieren und der Distribution des Inhalts dieser Website ist untersagt. Du bist im Rahmen dieser Bestimmungen und Bedingungen nicht befugt, Tätigkeiten auszuführen, mit dem Ziel diese Website oder jeglichen Nutzen des Nutzers dieser Website, zu unterbrechen oder zu beeinflussen, inklusive und in unbegrenzter Weise via Überlastungstools, welche diese Website « fluten », « Mailbombing » oder « Crashing » vornehmen, die Sicherheit oder die Authentifizierungsmaßnahmen der Nutzer umgehen oder versuchen, die dir zugestandene, begrenzte Genehmigung und den Zugang zu hintergehen. Du bist nicht befugt, Teile dieser Website in andere Websites einzufügen oder beliebige andere Websites mit einer beliebigen Seite dieser Website zu verlinken, außer mit der Homepage. Du bist nicht befugt, den Nutzen oder den Zugang zu dieser Website ohne unsere explizite vorhergehende Genehmigung zu veräußern.</p>
                
                <p>Der Inhalt dieser Website wurde mit angemessener Sorgfalt verfasst damit gewährleistet ist, dass der Inhalt korrekt und aktuell ist. Kao gewährt jedoch keinerlei Zusicherungen und Gewährleistungen, weder in Hinsicht auf die Genauigkeit oder Vollständigkeit des Inhalts der Website noch hinsichtlich des Inhalts der Websites die auf Basis eines Ist-Zustandes geliefert werden. Kao garantiert nicht, dass die auf der Website zur Verfügung gestellten Mittel ununterbrochen funktionieren oder fehlerfrei sind, oder dass Fehler korrigiert werden oder dass der betroffene Server frei von Viren oder sonstigen schädlichen Komponenten ist. Kao übernimmt unbegrenzt keine Haftung, weder für die Nutzung dieser Website noch für den Inhalt und die eventuell darin enthaltenen Fehler. Alle impliziten oder expliziten Garantien werden hiermit zurückgewiesen (inklusive jedoch unbegrenzt Garantien zu Gebrauchstauglichkeit und Tauglichkeit für eine spezielle Anwendung).</p>
                
                <p>Diese Website kann Verknüpfungen oder Hinweise zu anderen Websites beinhalten, Kao übernimmt jedoch keine Verantwortung hinsichtlich des Inhalts dieser anderen Websites und kann für beliebige Schäden, die durch derartige Inhalte verursacht werden könnten, nicht zur Rechenschaft gezogen werden. Links und Verknüpfungen zu anderen Seiten werden überwiegend als Dienstleistung an den Nutzer dieser Website angesehen.
                <br /><br />
                Kao nimmt dein Persönlichkeitsrecht und die Vertraulichkeit deiner persönlichen Daten sehr ernst. Konsultiere unsere Datenschutzrichtlinien für ergänzende Informationen zu der Art und Weise wie wir deine gesammelten Informationen nutzen.</a>.
                <br /><br />
                Weder Kao, seine Händler, Geschäftspartner oder Vertreter, noch beliebige Dritte, die in der Kreation, Produktion oder Verwaltung dieser Website involviert sind (zusammengefasst die “Kao Parteien”), können für beliebige indirekte, zufällige, abzuleitende, spezielle oder sträfliche Schäden beliebiger Art, die auf den Nutzen der Website zurückzuführen sind, zur Verantwortung gezogen werden. Dies betrifft auch jegliche Dienstleistung die auf der Website angeboten wird, selbst wenn die betroffene Partei über die Möglichkeit derartiger Schäden informiert wurde.
                <br /><br />
                In keinem Fall darf die Summe der gesamten Verbindlichkeiten der Kao Parteien dir gegenüber für sämtliche Schäden, Verletzungen, Verluste und Ursachen (ob in Verträgen, Delikten oder sonstigem) die aus deiner Nutzung der Website hervorgehen, folgende Beträge überschreiten: (a) Den durch dich an Kao gezahlten Betrag, wenn vorhanden, in Bezug auf deine Nutzung der Website oder (b) $100.
                <br /><br />
                Diese Website stellt eine Dienstleistung an deren Nutzer dar. Kao behält sich das Recht vor, den Inhalt der Website jederzeit aus beliebigen Grund und ohne vorherige Mitteilung zu löschen, ändern oder ergänzen, inklusive Änderungen der vorliegenden Klauseln. Wir nehmen derartige Änderungen vor indem wir diese auf der Website veröffentlichen. Du solltest die Website regelmäßig auf Änderungen überprüfen. Mit deinem fortlaufenden Zugang zu der Website nachdem diese Änderungen vorgenommen wurden, bestätigst du die Akzeptanz der Änderungen.</p>
                
                <p>Diese Vereinbarung unterliegt der Gesetzgebung des Staates Delaware unter Ausschluss des Kollisionsrechts. Du bestätigst hiermit, dass für jegliche Streitigkeit, die in Zusammenhang mit vorliegender Vereinbarung, der Website oder beliebigen Kao Produkten oder Dienstleistungen steht, die Gerichtsbarkeiten des Staates Delaware ausschließlich zuständig sind und du willigst unwiderruflich ein, dich diesen Gerichtsbarkeiten zu beugen, sollte eine beliebige Bestimmung vorliegender Vereinbarung als rechtswidrig, lückenhaft, oder aus irgendeinem Grunde als nicht erzwingbar erachtet werden, dann sollte diese Bestimmung als nicht zu vorliegender Vereinbarung zugehörig angesehen werden und die Gültigkeit und Erzwingbarkeit aller anderen Bestimmungen nicht beeinträchtigen.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
