﻿<%@ Page Title="Die richtige Gesichtsreinigungfür dein – die Produkte von Bioré" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.our_products.Default" %>
<%@ Register Src="~/BazaarVoice/Controls/BVScripts.ascx" TagPrefix="uc1" TagName="BVScripts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Bioré Hautpflegeprodukte für normale bis fettige Haut oder Mischhaut befreien deine Haut von Unreinheiten und Mitessern. Entdecke alle Produkte." name="description" />
    <meta content="Gesichtsreinigung, Hautprobleme bekämpfen, Hauttyp, Hautunreinheiten beseitigen, Hautbild, was hilft gegen unreine Haut?, Pickel, Aktivkohle, Backpulver, Produkte gegen Pickel" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>
    <meta name="viewport" content="width=device-width">
    <uc1:BVScripts runat="server" ID="BVScripts" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="titleh1">
                <h1>Unsere Produkte</h1>
                <div id="responseRule"></div>
                <!--<p>Verstopfte Poren sind die Hauptursache von Hautunreinheiten.</p>
                <br />-->
                <p>Unsere hochwirksamen, poren-reinigenden Produkte befreien deine Poren von Schmutz und Talg und helfen dir somit Hautunreinheiten und Mitesser effektiv zu bekämpfen. Ob als Waschgel, Peeling, Maske oder Clear-Up Strip – Bioré reinigt deine Poren in der Tiefe und sorgt für ein erfrischtes und ebenmäßigeres Hautbild.</p>
               
            </div>

            

            <div id="deepCleansingProds" class="prodList"> 
                <h2 class="pie roundedCorners deepCleansing">hallo <span>reine haut</span>! 
                    <span class="arrow"></span>
                </h2>
                <ul>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CHARCOAL_100X235.png" alt="" />
                        <h3>Porentiefes Waschgel <br />mit Aktivkohle</h3>
                        <p>tiefenreinigt 2x gründlicher als ein gewöhnliches Waschgel & erfrischt ganz natürlich</p>
                        <div id="BVRRInlineRating-deep-pore-charcoal-cleanser" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/deep-pore-charcoal-cleanser" id="A1">details ></a>
                    </li>
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreMinimizer.png" alt="" />
                        <h3>Poren verkleinerndes <br />Peeling mit Aktivkohle</h3>
                        <p>befreit von Hautschüppchen & verfeinert das Hautbild sichtbar</p>
                        <div id="BVRRInlineRating-charcoal-pore-minimizer" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/charcoal-pore-minimizer" id="details-charcoal-pore-minimizer">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/SelfHeatingMask.jpg" alt="" style="margin:0 0 2px 44px;"/>
                        <h3>1-Minute-Thermomaske</h3>
                        <p>Selbsterwärmend. Reinigt Poren 2,5x gründlicher* in nur einer Minute.</p>
                        <div id="BVRRInlineRating-self-heating-one-minute-mask" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/self-heating-one-minute-mask" id="A2">details ></a>
                    </li>
                   <li class="productListSpacer">
                        <img id="dontBeDirtyProducts" src="../images/ourProducts/products/small/Baking_Soda_Cleanser_small.png" alt="" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:8pt;"></span> Porenreinigendes Waschgel mit Backpulver</h3>
                        <p>Reinigt die Poren und entfernt sanft Hautschüppchen </p>
                        <div id="BVRRInlineRating-baking-soda-pore-cleanser" class="ratingCategory"></div>
                        <a href="../baking-soda-for-combination-skin/baking-soda-pore-cleanser" id="A3">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-cleansing-scrub-small.png" alt="" />
                        <h3><span style="font-family: ProximaNova-Bold, Arial;color:yellow;font-size:8pt;"></span> Puderpeeling mit Backpulver</h3>
                        <p>tiefenreinigt & befreit sanft von Hautschüppchen</p>
                        <div id="BVRRInlineRating-baking-soda-cleansing-scrub" class="ratingCategory"></div>
                        <a href="../baking-soda-for-combination-skin/baking-soda-cleansing-scrub" id="A4">details ></a>
                    </li>             
                </ul>
            </div>
            
            <div id="complexionClearingProds" class="prodList">
                <h2 class="pie roundedCorners bigPores">bye, bye <span>pickel</span>!
                    <span class="arrow"></span>
                </h2>
                <ul>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-anti-pimple_100X235.png" alt="" />
                        <h3><span style="font-family: ProximaNova-Bold, Arial;color:yellow;font-size:8pt;"></span> Charcoal Anti-Pimple Clearing Cleanser</h3>
                        <p>lorem ipsum lorem ipsum</p>
                        <div id="Div2" class="ratingCategory"></div>
                        <a href="../bye-bye-pickel/charcoal-anti-pimple-clearing-cleanser" id="A6">details ></a>
                    </li>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-anti-pimple_100X235.png" alt="" />
                        <h3><span style="font-family: ProximaNova-Bold, Arial;color:yellow;font-size:8pt;"></span> Baking Soda Anti-Pimple Cleansing Foam</h3>
                        <p>lorem ipsum lorem ipsum</p>
                        <div id="Div1" class="ratingCategory"></div>
                        <a href="../bye-bye-pickel/baking-soda-anti-pimple-cleansing-foam" id="A5">details ></a>
                    </li>       
                    
                </ul>
            </div>

<div id="murtProds" class="prodList">
                <h2 class="pie roundedCorners murt">mach schluss mit <span>mitessern</span>!
                    <span class="arrow"></span>
                </h2>
                <ul>

                       <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" class="cps-custom"/>
                        <h3>Tiefenreinigende Clear-Up Strips<br /> mit Aktivkohle</h3>
                        <p>Entzieht überschüssigen Talg. Mit natürlicher Aktivkohle.</p>
                        <div id="Div1" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/charcoal-pore-strips#regular" id="A5">details ></a>
                    </li>-->
                    
		   <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" />
                        <h3>Tiefenreinigende Clear-Up <br />Strips mit Aktivkohle</h3>
                        <p>Entzieht überschüssigen Talg. Mit natürlicher Aktivkohle.</p>
                        <div id="BVRRInlineRating-charcoal-pore-strips-regular" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/charcoal-pore-strips#regular" id="details-deep-cleansing-pore-strips-charcoal">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.png" alt="" />
                        <h3>Duo gegen Mitesser <br />für Gesicht & Nase</h3>
                        <p>Ideal für die T-Zone!</p>
                        <div id="BVRRInlineRating-pore-strips-combo" class="ratingCategory"></div>
                        <a href="../pore-strips/pore-strips-combo" id="details-deep-cleansing-pore-strips-combo">details ></a>
                    </li>

                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltra.png" alt="" />
                        <h3>Ultra Deep Cleansing Pore Strips Ultra</h3>
                        <p>lorem ipsum</p>
                        <div id="Div3" class="ratingCategory"></div>
                        <a href="../pore-strips/pore-strips-ultra" id="A7">details ></a>
                    </li>
                    
                    
                </ul>
            </div>

       

        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
