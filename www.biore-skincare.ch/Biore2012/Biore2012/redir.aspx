﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="redir.aspx.cs" Inherits="Biore2012.redir" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    If your browser doesn't redirect automatically, click <a href="<asp:Literal ID='litRedirectUrl' runat='server' />">here</a>.
    </div>
    </form>
    
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        //Biore Tag
        ga('create', 'UA-385129-4', 'auto');
        ga('send', 'pageview');

        //KAO Global
        ga('create', 'UA-385129-27', 'auto', { 'name': 'KAOGlobal' });
        ga('KAOGlobal.send', 'pageview');

    </script>

</body>
</html>
