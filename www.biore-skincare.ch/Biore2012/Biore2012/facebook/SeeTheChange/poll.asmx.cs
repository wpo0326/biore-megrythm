﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using Biore2012.BLL;

namespace Biore2012.facebook.SeeTheChange
{
    /// <summary>
    /// Summary description for rsgpPoll
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class poll : System.Web.Services.WebService
    {

        [WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<Poll> addPollVote(string pollAnsID)
        {
            List<Poll> poll = new List<Poll>();
            int pollID = 0;
            bool pollIsInt = Int32.TryParse(pollAnsID, out pollID);

            // Only execute the code if the pollID is 0 - 3
            if (pollIsInt && pollID >= 0 && pollID <= 3)
            {
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbConnection"].ConnectionString;
                // Add to Votes for Answer ID passed

                try
                {
                    using (SqlConnection con = new SqlConnection(connString))
                    {
                        con.Open();

                        SqlCommand cmd = con.CreateCommand();
                        cmd.CommandText = "sp201207SeeTheChangeFBPagePollVote";
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@answerID", SqlDbType.VarChar);
                        cmd.Parameters["@answerID"].Value = pollAnsID;

                        SqlDataReader reader;
                        reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Poll p = new Poll();
                                p.AnswerID = reader.GetString(0);
                                p.Votes = reader.GetInt32(1).ToString();
                                poll.Add(p);
                            }
                        }
                        else
                        {
                            //Console.WriteLine("No rows found.");
                        }
                        reader.Close();
                    }
                }
                catch (Exception ex)
                {
                    // DB call failed
                    //status = "error: " + "SQL ERROR: " + ex.Message.ToString();
                    Poll p = new Poll();
                    poll = new List<Poll>();

                    p.AnswerID = "-1";
                    p.Votes = ex.Message.ToString();
                    poll.Add(p);
                }
            }
            return poll;
        }

        [WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<Poll> getPollResults()
        {
            DataSet ds = new DataSet();
            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbConnection"].ConnectionString;
            // Get the Poll Results
            try
            {
                using (SqlConnection con = new SqlConnection(connString))
                {

                    SqlCommand cmd = con.CreateCommand();
                    cmd.CommandText = "sp201207SeeTheChangeFBPagePollGetResults";
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataAdapter myAdapt = new SqlDataAdapter(cmd))
                    {
                        con.Open();

                        myAdapt.Fill(ds, "Poll");
                    }
                }
            }
            catch (Exception ex)
            {
                // DB call failed
                //status = "error: " + "SQL ERROR: " + ex.Message.ToString();
            }

            List<Poll> pollResults = new List<Poll>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Poll poll = new Poll();
                poll.AnswerID = row["AnswerID"].ToString();
                poll.Votes = row["Votes"].ToString();
                pollResults.Add(poll);
            }
            return pollResults;
        }
    }
}
