﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteNoHeader.Master" AutoEventWireup="true" CodeBehind="PostLaunchForm.aspx.cs" Inherits="Biore2012.facebook.StoryToPoreOver.PostLaunchForm" %>
<%@ Register TagPrefix="uc1" TagName="ucForm" Src="~/Controls/ucFormConfig.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <%#SquishIt.Framework.Bundle .Css()
        .Add("~/css/formConfig.css")
    	.Add("~/facebook/globalFBCSS/StoryToPoreOverStyles.css")
		.Render("~/facebook/globalFBCSS/combinedform_#.css")
    %>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="floodlightPixels" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div id="fb-root"></div>
	<script>
		window.fbAsyncInit = function() {
			FB.init({
				appId: '<%= System.Configuration.ConfigurationManager.AppSettings["AuthorPartnershipFBAppId"] %>', // App ID
				//channelURL : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
				status: true, // check login status
				cookie: true, // enable cookies to allow the server to access the session
				oauth: true, // enable OAuth 2.0
				xfbml: true  // parse XFBML
			});

			// Additional initialization code here
			FB.Canvas.setAutoGrow();
			FB.Canvas.scrollTo(0, 0);

			(function initFBUtils() {
				if (typeof fbUtils === 'object') {
					fbUtils.initEvents();  //set up FB Event Listeners
					fbUtils.getLoginState(); //Get the current Login State
				} else {
					setTimeout(initFBUtils, 200);
				}
			})();
		};
	    // Load the SDK Asynchronously
		(function(d) {
			var js, id = 'facebook-jssdk'; if (d.getElementById(id)) { return; }
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = "//connect.facebook.net/en_US/all.js";
			d.getElementsByTagName('head')[0].appendChild(js);        } (document));
    </script>
	<div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <uc1:ucForm ID="ucForm" runat="server" />
                <%-- Extra form fields for custom form --%>
                <asp:Panel ID="panAddFormFields" runat="server" CssClass="addFormFields">
					<div class="AddPhotoContainer Question CustomQuestiondropdown">
						<label>Add your photo*</label>
						<a href="#" class="selectFromFB">Select from your Facebook Albums</a>
						<div class="fbPhotoPreview">
							<img src="images/stpoPicUploadBoxInnerShadow.png" alt="" class="maskedImage" />
							<img src="images/stpoPicUploadBox-InnerMask.png" alt="mask" class="mask" />
						</div>
						<asp:HiddenField ID="hidAddPhoto" runat="server" />
						<%-- This block is reserved for generating the duplicate error which is triggered by the server side --%>
						<div class="ErrorContainer">
							<asp:TextBox runat="server" ID="valCheck" Visible="false" Text="checkString" />
							<asp:RequiredFieldValidator ID="reqValCheck" runat="server" CssClass="errormsg" ErrorMessage="Image is required for submission."
								ControlToValidate="valCheck" EnableClientScript="true" SetFocusOnError="true"></asp:RequiredFieldValidator>
						</div>
					</div>
					<div class="BooksReadContainer Question CustomQuestiondropdown">
						<asp:Label AssociatedControlID="ddBooksRead" ID="lblBooksRead" runat="server">How many books do you read each month?*</asp:Label>
						<asp:DropDownList ID="ddBooksRead" runat="server" CssClass="ddBooksRead">
							<asp:ListItem Text="Select" Value="" />
							<asp:ListItem Text="0-1" Value="0to1" />
							<asp:ListItem Text="2-4" Value="2to4" />
							<asp:ListItem Text="5 or more" Value="5orMore" />
						</asp:DropDownList>
						<div class="ErrorContainer">
							<asp:RequiredFieldValidator ID="valDDBooksRead" runat="server" ControlToValidate="ddBooksRead" CssClass="errormsg" ErrorMessage="Please answer all required questions."></asp:RequiredFieldValidator>
						</div>
					</div>
					<div class="BookClubMemerContainer Question CustomQuestiondropdown">
						<asp:Label AssociatedControlID="ddBookClub" ID="lblBookClub" runat="server">Are you currently a member of a book club?*</asp:Label>
						<asp:DropDownList ID="ddBookClub" runat="server" CssClass="ddBookClub">
							<asp:ListItem Text="Select" Value="" />
							<asp:ListItem Text="Yes" Value="Yes" />
							<asp:ListItem Text="No" Value="No" />
						</asp:DropDownList>
						<div class="ErrorContainer">
							<asp:RequiredFieldValidator ID="valDDBookClub" runat="server" ControlToValidate="ddBookClub" CssClass="errormsg" ErrorMessage="Please answer all required questions."></asp:RequiredFieldValidator>
						</div>
					</div>
					<div class="AvailableContainer Question CustomQuestiondropdown">
						<asp:Label AssociatedControlID="ddAvailable" ID="lblAvailable" runat="server">If chosen, are you available on December 5, 2012?*</asp:Label>
						<asp:DropDownList ID="ddAvailable" runat="server" CssClass="ddAvailable">
							<asp:ListItem Text="Select" Value="" />
							<asp:ListItem Text="Yes" Value="Yes" />
							<asp:ListItem Text="No" Value="No" />
						</asp:DropDownList>
						<div class="ErrorContainer">
							<asp:RequiredFieldValidator ID="valDDAvailable" runat="server" ControlToValidate="ddAvailable" CssClass="errormsg" ErrorMessage="Please answer all required questions."></asp:RequiredFieldValidator>
						</div>
					</div>
					<div class="WhyYouContainer Question CustomQuestiondropdown"></div>
						<asp:Label AssociatedControlID="txtWhyYou" ID="lblWhyYou" runat="server">In your own words, tell us what makes you the perfect book club candidate:*<br />(1,000 character max)*</asp:Label>
						<asp:TextBox Rows="7" Columns="45" TextMode="MultiLine" ID="txtWhyYou" CssClass="txtWhyYou" runat="server" />
						<div class="ErrorContainer">
							<asp:RequiredFieldValidator ID="valTXTWhyYou" runat="server" ControlToValidate="txtWhyYou" CssClass="errormsg" ErrorMessage="Please answer all required questions."></asp:RequiredFieldValidator>
						</div>
                </asp:Panel>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
<script type="text/javascript">
    $(function() {
        if ($(".biore_us_201210_AthrPrtnLaunchDayCFinalSweeps5M2DBX67").length) {
            toggle("noneToggle1");
            toggle("noneToggle2");
            toggle("noneToggle3");
            toggle("noneToggle4");
        }
    });
    function toggle(className) {
        var checkToggle = $("." + className + " input:last");
        checkToggle.click(function() {
            if ($("#" + checkToggle.attr('id') + ":checked").val()) {
                $("." + className + " input").each(function() {
                    if ($(this).attr('id') != checkToggle.attr('id')) {
                        $(this).attr("checked", false);
                        $(this).attr('disabled', 'disabled');
                    }
                });
            } else {
                $("." + className + "  input").each(function() {
                    if ($(this).attr('id') != checkToggle.attr('id')) {
                        $(this).removeAttr('disabled');
                    }
                });
            }
        });
    }
</script>
    <asp:Panel ID="MoveFileUpload" runat="server" Visible="false">
        <script type="text/javascript">
        	$(".QuestionContainer").prepend($(".addFormFields"));
			FBVars = {
				fbAppId: '<%= System.Configuration.ConfigurationManager.AppSettings["AuthorPartnershipFBAppId"] %>',
				FBShareUrl: '<%= System.Configuration.ConfigurationManager.AppSettings["FBAuthorPartnershipPageLink"] %>',
				thumbPath: '<%= System.Configuration.ConfigurationManager.AppSettings["FBAuthorPartnershipImageUrl"] %>',
				baseURL: '<%= System.Configuration.ConfigurationManager.AppSettings["FBAuthorPartnershipBaseUrl"]%>'
			};
    </script>
		<div class="selectPhotoWrap">
			<h3>Select a Photo</h3>
			<div class="albumWrap">
				<p>Change album <select id="fbAlbum"><option value="">Profile Pictures</option></select></p>
				<div class="imageWrap">
				</div>
			</div>
			<div class="actions">
				<a href="#" id="cancelSelectFBImage">Cancel</a>
				<a href="#" id="selectFBImage" class="selectFBImageBtn">Select</a>
			</div>
		</div>
		<div class="howDoesItWorkPopupOverlay"></div>
    </asp:Panel>
	<%=SquishIt.Framework.Bundle .JavaScript()
		.Add("~/facebook/js/fbUtils.js")
		.Render("~/facebook/js/stpoFBJS_#.js")
	%>        
</asp:Content>
