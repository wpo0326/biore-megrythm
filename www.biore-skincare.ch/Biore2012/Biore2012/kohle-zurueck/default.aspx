﻿<%@ Page Title="Legal | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master"
    Language="C#" AutoEventWireup="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
    <style type="text/css">
        #content p
        {
            text-align: left;
        }
        
        #topNav, .centeringDiv ul {
        display: none;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="about">
        <div id="main">
            <div id="mainContent">
                <div class="centeringDiv">
                    <div id="content">
                        <div class="titleh1">
                            <p><br/><br/>
                                <strong>Teilnahmebedingungen und Datenschutzhinweise für die Geld-zurück-Aktion „Kohle zurück“</strong><p>
                            <!--<div id="responseRule">
                            </div>-->
                        </div>
                        
                        <!--<div class="guaranteeImg"><img src="/images/forms/biore-guarantee-packshots-sm.png" alt="Biore products" /></div>-->
                       

<p>Die nachfolgenden Bedingungen regeln die Teilnahme an der Geld-zurück-Aktion „Kohle zurück“ der Marke Bioré (Microsite http://kohle-zurueck.biore.de), sowie die Erhebung und Nutzung der von den Teilnehmern erhobenen oder mitgeteilten Daten. Eine Teilnahme an der Aktion „Kohle zurück“ ist ausschließlich zu den hier aufgeführten Teilnahmebedingungen möglich. Bitte lies diese Nutzungsbedingungen sorgfältig durch. Die Nutzungsbedingungen enthalten wichtige Informationen. Bitte drucke die Nutzungsbedingungen aus und behalte eine Kopie für Deine Unterlagen. Mit der Teilnahme an der Aktion „Kohle zurück“ akzeptiert der Teilnehmer diese Teilnahmebedingungen:</p>

<p><strong>Veranstalter und Realisator</strong></p>

<p> 1. Die Aktion wird von der Guhl Ikebana GmbH, Pfungstädter Str. 98, 64297 Darmstadt, Deutschland, Telefon +49 (0)800-7307310 (DE), +43 (0)800-221770 (AT), +41 (0)800 890760 (CH) E-Mail: Produktberatung@Biore.com, veranstaltet. </p>

<p>2. Mit der Abwicklung der Aktion (Sichten, Kontrollieren und manuelle Erfassung der eingesandten Unterlagen und Daten sowie die Anweisung der Überweisung) wurde der Lettershop Emirat AG, Elisabethplatz 1, 80796 München, Deutschland beauftragt (Realisator). </p>

<p>3. Fragen zu der Aktion sind an den Veranstalter, Fragen hinsichtlich der erhobenen Daten an den Realisator zu richten. </p>

<p><strong>Teilnahmebedingungen der Aktion</strong></p>

<p> 1. Die Aktion endet mit dem Einsendeschluss am 31.10.2018. Es gilt das Datum des Poststempels.</p>

<p>2. Teilnahmeberechtigt sind Personen, die Ihren Wohnsitz in Deutschland, Österreich oder Schweiz haben und die zum Zeitpunkt der Teilnahme das 18. Lebensjahr vollendet haben. Mitarbeiter der Guhl Ikebana GmbH sowie deren Angehörige i.S.v. §15 AO, Lebenspartner und Mitarbeiter von Kooperationspartnern, die mit der Erstellung oder Abwicklung des Aktion beschäftigt sind oder waren, sind von der Teilnahme ausgeschlossen. </p>

<p>3. Bedingung für die Inanspruchnahme der Geld-zurück-Garantie ist eine Begründung der Unzufriedenheit von mindestens 15 Wörtern in vollständigen Sätzen. Der Original-Kaufbeleg muss zusammen mit dem abgetrennten pinken „Kohle zurück-Sticker“ des Aktionsproduktes und den Bankdaten für die Rückerstattung eingesandt werden. Anhand des Datums auf dem Kaufbeleg muss klar ersichtlich sein, dass der Produktkauf nicht länger als 4 Wochen zurückliegt. Die Unterlagen sind an die folgende Adresse zu senden: </p>

<p>Bioré<br />Kennwort: „Kohle zurück“<br />Postfach 400202<br />D-80702 München, Deutschland</p>

<p>4. Die Teilnahme an der Aktion „Kohle zurück“ findet ausschließlich postalisch (offline) statt. Eine Rücksendung des Kaufbelegs ist aus technischen Gründen nicht möglich. <strong>Es ist nur eine einmalige Teilnahme pro Haushalt mit nur einem unserer für die Aktion ausgezeichneten Bioré-Produkte möglich</strong>. Es können nur ausreichend frankierte, vollständige und gut lesbare Einsendungen berücksichtigt werden.</p>

<p>5. Es wird der tatsächliche Kaufpreis erstattet, wie auf dem Kaufbeleg ausgewiesen, zuzüglich der einmaligen Portokosten für deine Einsendung. Der Betrag wird innerhalb von 6 Wochen auf das angegebene Bankkonto erstattet. Eine Barauszahlung ist nicht möglich.</p>

<p>6. Mit der Teilnahme an der Geld-zurück-Garantie werden die Teilnahmebedingungen akzeptiert. Bioré behält sich das Recht vor, diese Teilnahmebedingungen zu ändern.</p>

<p>7. Bioré behält sich das Recht vor, Teilnehmer bei Verstoß gegen diese Teilnahmebedingungen, insbesondere bei unerlaubter offensichtlicher Mehrfacheinlösung, und/oder aufgrund von falschen Angaben, Manipulationen oder der Verwendung unerlaubter Hilfsmittel von der Geld-zurück-Garantie auszuschließen und die Rückerstattung zu verweigern. Der Rechtsweg ist ausgeschlossen. </p>

<p><strong>Gewährleistungsausschluss</strong></p>

<p> 1. Der Veranstalter behält sich das Recht vor, die Aktion zu jedem Zeitpunkt ohne Vorankündigung aus wichtigem Grund abzubrechen. Im Falle eines Abbruchs der Aktion aus wichtigem Grund wird der Veranstalter die Teilnehmer unverzüglich hierüber informieren. Ein Abbruch aus wichtigem Grund kann insbesondere erfolgen, wenn aus technischen Gründen eine ordnungsgemäße Durchführung der Aktion nicht mehr gewährleistet werden kann. </p>

<p>2. Die Verfügbarkeit und Funktion der Aktion kann vom Veranstalter nicht gewährleistet werden. Die Aktion kann aufgrund von äußeren Umständen und Zwängen beendet werden, ohne dass hieraus Ansprüche der Teilnehmer gegenüber dem Veranstalter entstehen.</p>

<p>3. Der Veranstalter haftet ferner nicht für verloren gegangene, verzögerte, fehlgeleitete, beschädigte oder nicht zugestellte Beiträge/Inhalte, fehlerhafte Kontodaten oder unzureichende Frankierung.</p>

<p><strong>Datenschutz</strong></p>

<p>1. Der Realisator erhebt und nutzt die Daten der Teilnehmer nur soweit dies gesetzlich erlaubt ist oder die Teilnehmer dazu einwilligen. Die bereitgestellten Informationen werden - sofern nicht ausdrücklich anders ausgewiesen – nur für die Durchführung der Aktion und die Überweisung des Rückerstattungsbetrags vom Veranstalter verwendet.</p>
<p> 2. Die Teilnehmer willigen durch ihre Teilnahme an der Aktion zur Erhebung, Speicherung und Nutzung folgender Daten ein, die der Gestaltung, Durchführung und Abwicklung der Aktion dienen: Name und Kontodaten. Die persönlichen Daten der Teilnehmer beim Veranstalter werden im Anschluss an die Beendigung der Aktion und dessen Abwicklung gelöscht.  
<p>3. Auf schriftliche oder in Textform mitgeteilte Anforderung hin, können die Teilnehmer vom Realisator jederzeit Auskunft darüber verlangen, welche personenbezogenen Daten gespeichert sind, können deren Berichtigung sowie Löschung verlangen und Datennutzungseinwilligungen widerrufen.</p>

<p><strong>Haftungsausschluss</strong></p>

<p> 1. Für eine Haftung des Veranstalters auf Schadensersatz gelten unbeschadet der sonstigen gesetzlichen Anspruchsvoraussetzungen folgende Haftungsausschlüsse und Haftungsbegrenzungen:</p>  

<p>2. Der Veranstalter haftet unbeschränkt, soweit die Schadensursache auf Vorsatz oder grober Fahrlässigkeit beruht. Ferner haftet der Veranstalter für die leicht fahrlässige Verletzung von wesentlichen Pflichten, deren Verletzung die Erreichung des Vertragszwecks gefährdet, oder für die Verletzung von Pflichten, deren Erfüllung die ordnungsgemäße Durchführung des Gewinnspiels überhaupt erst ermöglichen und auf deren Einhaltung die Vertragspartner regelmäßig vertrauen. In diesem Fall haftet der Veranstalter jedoch nur für den vorhersehbaren, vertragstypischen Schaden. Der Veranstalter haftet nicht für die leicht fahrlässige Verletzung anderer als der in den vorstehenden Sätzen genannten Pflichten. </p><p> 3. Soweit die Haftung des Veranstalters ausgeschlossen oder beschränkt ist, gilt dies auch für die persönliche Haftung von Arbeitnehmern, Vertretern und Erfüllungsgehilfen des Veranstalters.</p>

<p><strong>Sonstiges</strong></p>
<p>1. Der Rechtsweg ist ausgeschlossen. Es ist ausschließlich das Recht der Bundesrepublik Deutschland anwendbar. Sollten einzelne dieser Teilnahmebedingungen ungültig sein oder werden, bleibt die Gültigkeit der übrigen Teilnahmebedingungen hiervon unberührt. Diese Teilnahmebedingungen können jederzeit von Guhl Ikebana GmbH geändert werden.</p>

<p> 2. Aufgrund der besseren Lesbarkeit wird in den Teilnahmebedingungen nur die männliche Form verwendet. Die weibliche Form ist selbstverständlich immer mit eingeschlossen.</p>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
