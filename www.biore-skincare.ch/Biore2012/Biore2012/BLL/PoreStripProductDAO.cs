﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.IO;
using System.Web.Caching;

namespace Biore2012.BLL
{
    public class PoreStripProductDAO
    {
        public static PoreStripProduct GetProduct(string ProductRefName)
        {
            PoreStripProduct fileContent = (PoreStripProduct)HttpContext.Current.Cache["PoreStripProductFile-" + ProductRefName];
            if (fileContent == null)
            {
                string src = System.Configuration.ConfigurationManager.AppSettings["XMLDataPath"] + "\\" + ProductRefName + ".xml";

                XmlSerializer mySerializer = new XmlSerializer(typeof(PoreStripProduct), new XmlRootAttribute("Product"));
                FileStream myFileStream = new FileStream(src, FileMode.Open, FileAccess.Read);
                try
                {
                    fileContent = (PoreStripProduct)mySerializer.Deserialize(myFileStream);
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    myFileStream.Close();
                }

                HttpContext.Current.Cache.Insert("PoreStripProductFile-" + ProductRefName, fileContent, new CacheDependency(src));
            }
            return fileContent;
        }
    }
}
