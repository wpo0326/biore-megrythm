﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.IO;
using System.Web.Caching;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace Biore2012.BLL
{
    public class ProveItCodeDAO
    {
        public static ProveItCode GetProveItCode(int memberId, int proveItCampaignId)
        {
            ProveItCode thisProveItCode = new ProveItCode();
            try
            {
                String conStr = System.Configuration.ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString;
                SqlConnection myConn = null;
                SqlDataReader reader;
                myConn = new SqlConnection(conStr);
                myConn.Open();

                SqlCommand cmd = new SqlCommand("ProveItCodes_Update", myConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@MemberID", memberId));
                cmd.Parameters.Add(new SqlParameter("@CampaignID", proveItCampaignId));

                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //Console.WriteLine("{0}\t{1}", reader.GetInt32(0), reader.GetString(1));
                        //thisProveItCode.returnStatus = reader.GetInt32(0);
                        thisProveItCode.returnStatusMessage = reader.GetString(0);
                        thisProveItCode.proveItCode = reader.GetString(1);
                    }
                }
                else
                {
                    //Console.WriteLine("No rows found.");
                }
                reader.Close();
                myConn.Close();
            }
            catch (Exception)
            {
                throw new ArgumentException("Error executing stored procedure.");
            }
            return thisProveItCode;
        }
    }
}