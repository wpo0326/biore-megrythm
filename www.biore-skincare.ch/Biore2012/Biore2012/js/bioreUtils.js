﻿/****************************************************************
Utils JavaScript Document for Biore

Global Utils JS document contains JavaScript functions 
that are used throughout the site.tpj.noConflict();
      
*****************************************************************/
var tpj = jQuery;
var mobileNavToggle = false;
// tpj.noConflict();

tpj(function() {
    global.init();
});

var global = {
    resize: null,
    flashPrefix: '../flash/',
    resize: null,
    sendVideoTrack: null,
    init: function () {

        //check cookie for pop-up
        // if no cookie
        //var value = $.cookie('cookieaccept');
        //alert(value);
        if (!$.cookie('cookieaccept')) {
            //$("#cookiepop").show();
            $(".cdp-cookies-boton-cerrar").click(function () {
                $("#cookiepop").hide();
                // set the cookie for 24 hours
                var date = new Date();
                date.setTime(date.getTime() + 24 * 60 * 60 * 1000);
                $.cookie('cookieaccept', true, { expires: date, path: '/' });
            });
        }

        // if homepage, start cycle
        if (tpj(".home").length) {
            global.startCycle();
        }

        // call scrollto to hide address bar on mobile devices on page load
        if (tpj(".mobile").length) {
            setTimeout(function() {
                window.scrollTo(0, 1);
            }, 500);
        }

        var config = {
            over: global.dropdownOver, // function = onMouseOver callback (REQUIRED)    
            timeout: 200, // number = milliseconds delay before onMouseOut
            out: global.dropdownOut,
            sensitivity: 2,
            interval: 0,
            timeout: 0// function = onMouseOut callback (REQUIRED)    
        };

        
        tpj("#theaterItem3 > div.ContenTop.clearfix > div.mainContent > div.btnLearnMore > a > img").hover(function () {
            tpj(this).attr('src', '../en-US/images/home/cta-btn-hover.png');
        }, function () {
            tpj(this).attr('src', '../en-US/images/home/cta-btn.png');
        });
        
        // hover event for our products dropdown in global nav
        tpj("#topnavItem1").hoverIntent(config);

        tpj("#ourProductsBlock a").click(function() {
            global.dropdownOut();
        });

        tpj("#burgerShot").click(function () {
            global.burgerShotToggle();
        });

        // click event for Our Products headings at less than 625 width
        tpj(".prodList h2").click(global.ourProductsHeadingClick);

        // global click event for scrolling up to top
        tpj('.scrollPage').click(function(e) {
            e.preventDefault();
            var elementClicked = tpj(this).attr("href");
            var destination = tpj(elementClicked).offset().top;
            tpj("html:not(:animated),body:not(:animated)").animate({ scrollTop: destination - 20 }, 500);
        });

        // video link
        tpj("a.videoLink").removeAttr("target");

        tpj("a.videoLink").click(function(e) {
            e.preventDefault();
            var vidParams = tpj(this).attr("rel").split("::")[1].split("|");
            var posterImg = tpj(this).find("img").attr("src");
            global.playVideo(tpj(this).attr("href"), tpj(this).parent().attr("id"), vidParams[0], vidParams[1], vidParams[2], global.flashPrefix + "FlashVideoPlayer_scale.swf", posterImg);
        });

        if (Modernizr.touch) {
            tpj("body").addClass("touch");
        }
        else { tpj("body").addClass("notouch"); }

        // check for screen size, and remove selective click events if bigger than mobile size
        global.addRemoveClickEvents();
        tpj(window).resize(function() {
            clearTimeout(global.resize);
            global.resize = setTimeout(function() {
                global.addRemoveClickEvents();
            }, 300);
        });
    },
    ourProductsHeadingClick: function() {
        global.showHideContent(this, tpj(".prodList"), tpj(this).parent(), true);
    },
    addRemoveClickEvents: function() {
        tpj(".prodList h2").unbind("click");
        if (tpj(window).width() < 1500) {
            tpj(".prodList h2").bind("click", global.ourProductsHeadingClick);
        }
    },
    dropdownOver: function() {
        var windowWidth = global.getWindowWidth();
        if (windowWidth > 768) {
            tpj("a.ourProductTopNav").removeClass("whiteTopNav");
            tpj("a.ourProductTopNav").addClass("navBlue");
            tpj("#ourProductsBlock").addClass("open").animate({ "filter": "alpha(opacity=100)", "opacity": 1 }, 300, function() {
                if (tpj(".ie").length) {
                    tpj("#ourProductsBlock").get(0).style.removeAttribute("filter");
                }
            });
        }
    },
    dropdownOut: function() {
        var windowWidth = global.getWindowWidth();
        if (windowWidth > 768) {
            tpj("a.ourProductTopNav").addClass("whiteTopNav");
            tpj("a.ourProductTopNav").removeClass("navBlue");
            tpj("#ourProductsBlock").stop().removeClass("open").css({ "filter": "alpha(opacity=0)", "opacity": 0 });   
        }
    },
    /*
    burgerShotToggle: function() {
        var windowWidth = global.getWindowWidth();
        if (windowWidth < 600 && mobileNavToggle == false) {
            tpj("#topnav").animate({
                right: 0
            }, 1000, function () {
                mobileNavToggle = true;
            });

        } else if (windowWidth < 600 && mobileNavToggle == true) {
            tpj("#topnav").animate({
                right: -180
            }, 1000, function () {
                mobileNavToggle = false;
            });            
        }
    },*/
    startCycle: function() {
        tpj('.flexslider').flexslider({
            animation: "fade",
            controlsContainer: "#pager",
            directionNav: false,
            slideshowSpeed: 10000,
            start: function(slider) {
                global.removeIEFilter(slider);
            },
            after: function(slider) {
                global.removeIEFilter(slider);
            }
        });
    },
    // used on our products and product detail to expand / collapse content
    showHideContent: function(element, elementToRemoveClass, elementToAddClass, scrollToTop) {
        var open = false;
        if (tpj(elementToAddClass).hasClass("open")) {
            open = true;
        }
        tpj(elementToRemoveClass).removeClass("open");
        var scrollTopPos = tpj(element).offset().top;
        if (scrollToTop) {
            tpj("html:not(:animated),body:not(:animated)").animate({ scrollTop: scrollTopPos - 10 }, 500);
        }
        if (!open) {
            tpj(elementToAddClass).addClass("open").removeClass("hide");
        }
    },
    removeIEFilter: function(slider) {
        if (tpj(".ie").length) {
            tpj(slider).find("li").each(function() {
                this.style.removeAttribute("filter");
            });
        }
    },
    getWindowWidth: function() {
        var windowHeight = tpj(window).height();
        var windowWidth;
        tpj("body.ie").css({ "height": windowHeight, "overflow": "hidden" });
        if (tpj.browser.safari || tpj.browser.msie) {
            windowWidth = document.body.clientWidth;
        }
        else {
            windowWidth = window.innerWidth;
        }
        tpj("body.ie").removeAttr("style");
        return windowWidth;
    },

    playVideo: function(videoURL, vidDivID, vidWidth, vidHeight, vidDuration, playerURL, posterImg) {
        // SWFplayer and ContainerDiv sizes are dynamic based on vidWidth & vidHeight
        if (videoURL == "") { return false; }

        /*****************************************/
        /* IMPORTANT PARAMS TO CONFIGURE - START */
        /*****************************************/
        var flashVideoPlayerURL = playerURL;
        if (playerURL == null || playerURL == "") { flashVideoPlayerURL = global.flashPrefix + "FlashVideoPlayer_scale.swf"; }

        var videoURLForFlashPlayer = videoURL;
        var flash1_contentVersion = "9.0.115"; // Flash 9.0.115 or higher required for H.264 video codec support

        // Add 22px for the height of the control bar in the Flash player, edit if the control bar height changes in SWF
        var flashPlayerControlHeight = 22;

        // CHOOSE SOME DEFAULT HEIGHTS AND WIDTHS IF THEY ARE NOT EXPLICITLY DEFINED IN CALL TO playVideo
        var defaultVideoWidth = "518";
        var defaultVideoHeight = "310";
        /*****************************************/
        /* IMPORTANT PARAMS TO CONFIGURE - END */
        /*****************************************/

        // test function variables
        if (vidWidth == "") { vidWidth = defaultVideoWidth; }
        if (vidHeight == "") { vidHeight = defaultVideoHeight; }
        if (vidDuration == "") { vidDuration = "9999"; }
        if (playerURL == null || playerURL == "") { playerURL = global.flashPrefix + "FlashVideoPlayer_scale.swf"; }

        //Check if video is larger than the viewport, if so, resize video proportionally to fit
        var viewportY = tpj(".videoHolder").height();
        var viewportX = tpj(".videoHolder").width();

        vidWidth = parseInt(vidWidth);
        vidHeight = parseInt(vidHeight);
        if (vidWidth > viewportX || vidHeight > viewportY) {
            var vidPorportions = vidWidth / vidHeight;
            var orgW = vidWidth;
            var orgH = vidHeight;
            if (vidHeight > viewportY) {
                vidHeight = viewportY;
                vidWidth = orgW > orgH ? vidHeight * vidPorportions : vidHeight / vidPorportions;
                if (vidWidth > viewportX) {
                    vidWidth = viewportX;
                    vidHeight = orgW > orgH ? vidWidth / vidPorportions : vidWidth * vidPorportions;
                }
            } else {
                vidWidth = viewportX;
                vidHeight = orgW > orgH ? vidWidth / vidPorportions : vidWidth * vidPorportions;
                if (vidHeight > viewportY) {
                    vidHeight = viewportY;
                    vidWidth = orgW > orgH ? vidHeight * vidPorportions : vidHeight / vidPorportions;
                }
            }
        }

        var playerWidth = parseInt(vidWidth);

        // Add the height of the control bar in the Flash player
        var playerHeight = parseInt(vidHeight) + flashPlayerControlHeight;
        global.createVideoObject(flash1_contentVersion, vidDivID, videoURL, vidWidth, vidHeight, vidDuration, flashVideoPlayerURL, playerWidth, playerHeight, posterImg);

        // this is for after the video is embedded, making it re-playable
        if (global.detectAndroid()) {
            tpj("video").live("click", function() {
                this.play();
            });
        }
    },

    createNonFlashContent: function(vidDivID) {
        //Non-Flash Content for Video
        var nonflashVideoContentPart1 = "To see this video you need JavaScript enabled and the latest version of Flash.  You can also use a browser that supports H.264 HTML5 Video.";
        var nonflashVideoContentPart2LinkText = "Click here";
        var nonflashVideoContentPart2LinkUrl = "http://www.adobe.com/go/getflashplayer"
        var nonflashVideoContentPart2Text = " to go to the Adobe Flash download center.";

        var noflashDiv = tpj("<div></div>").attr("id", vidDivID + "noflash").addClass("no-flash");
        var noflashP1 = tpj("<p></p>").text(nonflashVideoContentPart1);
        var noflashP2link = tpj("<a></a>").attr({ href: nonflashVideoContentPart2LinkUrl, target: "_blank" }).text(nonflashVideoContentPart2LinkText);
        var noflashP2 = tpj("<p></p>").text(nonflashVideoContentPart2Text);
        noflashP2.prepend(noflashP2link);
        noflashDiv.append(noflashP1).append(noflashP2);
        return noflashDiv;
    },

    createVideoObject: function(flash1_contentVersion, vidDivID, videoURL, vidWidth, vidHeight, vidDuration, playerURL, playerWidth, playerHeight, posterImg) {
        tpj("#" + vidDivID).html(global.createNonFlashContent(vidDivID));
        if (global.supports_html5_h264_video()) {
            //EMBED HTML5 Video Player
            // NOTE that iPhone OS2 does not support HTML5 Video
            var videoattributes = {};
            videoattributes.src = videoURL;
            videoattributes.poster = posterImg;
            videoattributes.width = vidWidth;
            videoattributes.height = vidHeight;
            videoattributes.controls = "true";
            videoattributes.autoplay = "true";
            var video = tpj("<video></video>").attr(videoattributes);
            tpj("#" + vidDivID).html(video);
            tpj(video).gVideo();
            // the below line works for ios and android 3.0 and higher
            tpj(video).get(0).play();
            // this line is for android 2.2 and under
            setTimeout(function() { tpj(video).get(0).play(); }, 50);
            // add tracking to video play
            tpj(video).bind("timeupdate", global.trackVideoTime);
            tpj("#" + vidDivID).parent().width(vidWidth);
        }
        else if (swfobject.hasFlashPlayerVersion(flash1_contentVersion)) {
            //EMBED SWFObject Flash Player
            var flashvars = {};
            flashvars.videoUrl = videoURL;
            flashvars.videoWidth = vidWidth;
            flashvars.videoHeight = vidHeight;
            flashvars.videoDuration = vidDuration;
            var flashparams = {};
            flashparams.menu = "false";
            flashparams.scalemode = "noscale";
            flashparams.quality = "high";
            flashparams.wmode = "opaque";
            flashparams.bgcolor = "#3E3C3C";
            var flashattributes = {};
            swfobject.embedSWF(playerURL, vidDivID, playerWidth, playerHeight, flash1_contentVersion, null, flashvars, flashparams, flashattributes);
        }
    },

    supports_html5_h264_video: function() {
        //if Android, return true
        if (global.detectAndroid()) return true;
        //if browser can't play video tag at all (IE), return false:
        if (!document.createElement('video').canPlayType) { return false; }
        //if it can, check for mp4 type:
        else {
            var v = document.createElement("video");
            return !!v.canPlayType('video/mp4');
        }
    },

    detectAndroid: function() {
        var Android = ['android'];
        var userAgent = navigator.userAgent.toLowerCase();
        for (var i = 0; i < Android.length; i++) {
            if (userAgent.indexOf(Android[i]) != -1) {
                return true;
            }
        }
        return false;
    },

    trackVideoTime: function() {
        var curTime = this.currentTime.toFixed(0);
        var vidLength = this.duration.toFixed(0);
        var eventType = "VideoProgress_";
        var videoURL = this.src;
        if (
            curTime == vidLength * .9 ||
            curTime == vidLength * .75 ||
            curTime == vidLength * .5 ||
            curTime == vidLength * .25
        ) {
            eventType += ((curTime / vidLength) * 100).toString();
            // use timeout so when it hits this multiple times within a second, it will only call tracker once
            clearTimeout(global.sendVideoTrack);
            global.sendVideoTrack = setTimeout(function() {
                goToPage('VideoPlayerEvent/VideoProgress_' + eventType + '/' + videoURL + '/');
            }, 1000);
        }
    }
}

/***********************************************************************
validateOnBlur
Assists the .Net form validation by checking if the form element
passed is valid and setting the error span to block if it isn't
		
Page_Validators is a .Net array of the validators on the page
		
This function requires .Net validation to be enabled and the
Display: Dyanamic attribute to be used on the validators
***********************************************************************/
function validateOnBlur(Page_Validators, tElement) {
    for (var i = 0; i < Page_Validators.length; i++) {
        //ValidatorValidate(Page_Validators[i], null, null);
        if (Page_Validators[i].id.indexOf(tElement.attr("id")) != -1) {
            if (!Page_Validators[i].isvalid) {
                var errorSpan = (Page_Validators[i].id.substring(Page_Validators[i].id.length - 1)) - 1;
                tElement.siblings("span").eq(errorSpan).css("display", "block");
                tElement.siblings("span:visible").css("display", "block");
            }
        }
    }
}


/* Flash Tracking */
function riaTrack(trackingParam) {
    goToPage(trackingParam);
}

function goToPage(pg) {
    firstTracker._trackPageview(pg);
    secondTracker._trackPageview(pg);
}

/* Querystrings */
function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function getQuerystring(key, default_) {
    if (default_ == null) default_ = "";
    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
    var qs = regex.exec(window.location.href);
    if (qs == null)
        return default_;
    else
        return qs[1];
}

/* Modernizr 2.0.6 (Custom Build) | MIT & BSD
* Build: http://www.modernizr.com/download/#-touch-teststyles-prefixes
*/
; window.Modernizr = function(a, b, c) { function y(a, b) { return !! ~("" + a).indexOf(b) } function x(a, b) { return typeof a === b } function w(a, b) { return v(m.join(a + ";") + (b || "")) } function v(a) { j.cssText = a } var d = "2.0.6", e = {}, f = b.documentElement, g = b.head || b.getElementsByTagName("head")[0], h = "modernizr", i = b.createElement(h), j = i.style, k, l = Object.prototype.toString, m = " -webkit- -moz- -o- -ms- -khtml- ".split(" "), n = {}, o = {}, p = {}, q = [], r = function(a, c, d, e) { var g, i, j, k = b.createElement("div"); if (parseInt(d, 10)) while (d--) j = b.createElement("div"), j.id = e ? e[d] : h + (d + 1), k.appendChild(j); g = ["&shy;", "<style>", a, "</style>"].join(""), k.id = h, k.innerHTML += g, f.appendChild(k), i = c(k, a), k.parentNode.removeChild(k); return !!i }, s, t = {}.hasOwnProperty, u; !x(t, c) && !x(t.call, c) ? u = function(a, b) { return t.call(a, b) } : u = function(a, b) { return b in a && x(a.constructor.prototype[b], c) }; var z = function(c, d) { var f = c.join(""), g = d.length; r(f, function(c, d) { var f = b.styleSheets[b.styleSheets.length - 1], h = f.cssRules && f.cssRules[0] ? f.cssRules[0].cssText : f.cssText || "", i = c.childNodes, j = {}; while (g--) j[i[g].id] = i[g]; e.touch = "ontouchstart" in a || j.touch.offsetTop === 9 }, g, d) } ([, ["@media (", m.join("touch-enabled),("), h, ")", "{#touch{top:9px;position:absolute}}"].join("")], [, "touch"]); n.touch = function() { return e.touch }; for (var A in n) u(n, A) && (s = A.toLowerCase(), e[s] = n[A](), q.push((e[s] ? "" : "no-") + s)); v(""), i = k = null, e._version = d, e._prefixes = m, e.testStyles = r; return e } (this, this.document);