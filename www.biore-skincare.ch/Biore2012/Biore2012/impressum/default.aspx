﻿<%@ Page Title="Privacy | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master"
    Language="C#" AutoEventWireup="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
    <style type="text/css">
        #content p
        {
            text-align: left;
            margin: 0 auto 40px 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="about">
        <div id="main">
            <div id="mainContent">
                <div class="centeringDiv">
                    <div id="content">
                        <div class="titleh1">
                            <h1>
                                Impressum</h1>
                            <div id="responseRule">
                            </div>
                        </div>


                        <h2>
                            GUHL IKEBANA GMBH<br />
                            A KAO GROUP COMPANY</h2><br />
                        <h2>
                            ADRESSE</h2>
                        <p>
                            Pfungstädterstr. 98<br />
                            D – 64297 Darmstadt</p>
                        <h2>
                            TELEFON/FAX</h2>
                        <p>
                            Tel.: 0800-730 7310 (D)<br />
                            Fax: +49 (0)6151 3960 778</p>
                        <h2>
                            E-MAIL</h2>
                        <p>
                            <a href="mailto:Produktberatung@biore.com" target="_blank">Produktberatung@biore.com</a></p>
                        <h2>
                            GESCHÄFTSFÜHRER</h2>
                        <p>
                            Eric Brockhus, Michael Cramer, Joseph Workman</p>
                        <h2>
                            HANDELSREGISTER</h2>
                        <p>
                            Amtsgericht Darmstadt, HRB 7210</p>
                        <h2>
                            UMSATZSTEUER-IDENTIFIKATIONSNUMMER</h2>
                        <p>
                            DE136611941</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
