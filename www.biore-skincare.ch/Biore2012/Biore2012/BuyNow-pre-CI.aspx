<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BuyNow-pre-CI.aspx.cs" Inherits="Biore2012.buy_now.BuyNow" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" type="text/css" href="~/css/ProximaNovaA.css" />
    <title></title>
    <style type="text/css">
        body {background:white; font: 12px arial,helvetica,sans-serif; }
        h1 { color:#22244a; font-size:16px; text-transform:uppercase; margin-bottom: 25px; }
		a.buyNow {
            /* background: white url(/en-US/images/products/grayArrow.gif) center right no-repeat; */
            background: url(../images/ourProducts/productDetail/BuyNowBtnModal.gif) no-repeat;
            width:111px;
            height:24px;
            color: #22244A;
            float: left;
            font-size: 14px;
            line-height: 14px;
            margin: 10px 10px 5px 0;
            padding: 12px 0 0 15px;
            text-align: left;
            text-decoration: none;
            text-transform: uppercase;
            font-family: ProximaNova-Bold;
		}

            a.buyNow:hover {
                background-position: 0px -34px;
            }

        #productHead {
            text-align: center;
        }
        #productImageContainer {
            float: left;
            width: 200px;
            margin: 0px 0px 0px -50px;
        }
        #productRetailerContainer {
            float:left; 
            width:495px; 
            text-align:center;
            margin-left: 77px;
        }
        .buyNowModalBox {
            float:left;
            width: 111px;
            margin: 0px 0px 50px 50px;
        }

    </style>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ListView ID="ListView1" runat="server" DataSourceID="XmlDataSource1" OnItemDataBound="OnItemDataBound">
            <LayoutTemplate>
                <div runat="server" id="itemPlaceholder"></div>
            </LayoutTemplate>
            <ItemTemplate>
                
                <div id="productImageContainer">
                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# "/en-US/images/ourProducts/products/large-buynow/" + XPath("ImageCollection/Item[Purpose=&#39;large-buynow&#39;]/FileName") %>' />
                </div>
                <div id="productRetailerContainer">
                    <div id="productHead"><h1><%#XPath("ProductShortName") %></h1></div>
                        
                        <asp:Repeater ID="Repeater1" runat="server" DataSource='<%# XPathSelect("LinkData/*") %>' OnItemDataBound="RepeaterOnItemDataBound">
                            
                                <ItemTemplate>
                                    <div class="buyNowModalBox">
                                    <asp:HyperLink ID="RetailerLogo" runat="server" ImageUrl='<%# "/en-US/images/buynow/" + XPath("Image") %>' NavigateUrl='<%# XPath("Href") %>' Target="_blank" style="margin-right:40px;" ImageAlign="AbsMiddle" ToolTip='<%# XPath("Retailer") %>'  />
                                    <asp:HyperLink ID="RetailerLink" runat="server" NavigateUrl='<%# XPath("Href") %>' Target="_blank" CssClass="buyNow" ToolTip='<%# XPath("Retailer") %>'>Buy&nbsp;Now</asp:HyperLink>

                			        <!-- <div style="clear:both; margin-top:20px;"></div> -->
                                    </div> 
                                </ItemTemplate>
                           
                        </asp:Repeater>
                        
                	<div style="clear:both;"></div>
                </div>
                <div style="clear:both;"></div>
            </ItemTemplate>
        </asp:ListView>
        <asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="/en-US/biore-facial-cleansing-products/xml/buynow.xml"></asp:XmlDataSource>
    </form>
    <script type="text/javascript">
        if (typeof jQuery == 'undefined') {
            var source = '<%= VirtualPathUtility.ToAbsolute("~/js/jquery-1.6.4.min.js") %>';
            document.write(unescape("%3Cscript src='" + source + "' type='text/javascript'%3E%3C/script%3E"));
        }
    </script>
    <script type="text/javascript" src="<%= VirtualPathUtility.ToAbsolute("~/js/tracking.js") %>"></script>
    <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
        try {
            var firstTracker = _gat._getTracker("UA-385129-27"); // Global Tag
            firstTracker._setDomainName("none");
            firstTracker._trackPageview();
            var secondTracker = _gat._getTracker("UA-385129-4"); // Biore Tag
            secondTracker._trackPageview();
        } catch (err) { }
    </script>
</body>
</html>
