﻿<%@ Page Title="Bioré – der Experte für freie Poren. | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Free your pores! Erlebe mit Bioré die tiefenreinigende Kraft von Backpulver und Aktivkohle. Für reine Haut und feine Poren." name="description" />
    <meta content="skincare, clean face" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/UniversPro67.css")
        .Add("~/css/homepage.css")
        .Add("~/css/flexslider.css")
        .Render("~/CSS/combinedhome_#.css")
    %>
    <meta name="viewport" content="width=device-width">

    <style type="text/css">
        /**
       * Fade-move animation for second dialog
       */

        /* at start */
        .zoom-anim-dialog {
            opacity: 0;
            -webkit-transition: all 0.2s ease-out;
            -moz-transition: all 0.2s ease-out;
            -o-transition: all 0.2s ease-out;
            transition: all 0.2s ease-out;
            -webkit-transform: translateY(-20px) perspective( 600px ) rotateX( 10deg );
            -moz-transform: translateY(-20px) perspective( 600px ) rotateX( 10deg );
            -ms-transform: translateY(-20px) perspective( 600px ) rotateX( 10deg );
            -o-transform: translateY(-20px) perspective( 600px ) rotateX( 10deg );
            transform: translateY(-20px) perspective( 600px ) rotateX( 10deg );
        }

        /* animate in */
        .mfp-ready .zoom-anim-dialog {
            opacity: 1;
            -webkit-transform: translateY(0) perspective( 600px ) rotateX( 0 );
            -moz-transform: translateY(0) perspective( 600px ) rotateX( 0 );
            -ms-transform: translateY(0) perspective( 600px ) rotateX( 0 );
            -o-transform: translateY(0) perspective( 600px ) rotateX( 0 );
            transform: translateY(0) perspective( 600px ) rotateX( 0 );
        }

        /* animate out */
        .mfp-removing .zoom-anim-dialog {
            opacity: 0;
            -webkit-transform: translateY(-10px) perspective( 600px ) rotateX( 10deg );
            -moz-transform: translateY(-10px) perspective( 600px ) rotateX( 10deg );
            -ms-transform: translateY(-10px) perspective( 600px ) rotateX( 10deg );
            -o-transform: translateY(-10px) perspective( 600px ) rotateX( 10deg );
            transform: translateY(-10px) perspective( 600px ) rotateX( 10deg );
        }

        /* Dark overlay, start state */
        .mfp-bg {
            opacity: 0;
            -webkit-transition: opacity 0.3s ease-out;
            -moz-transition: opacity 0.3s ease-out;
            -o-transition: opacity 0.3s ease-out;
            transition: opacity 0.3s ease-out;
        }
        /* animate in */
        .mfp-ready.mfp-bg {
            opacity: 0.8;
        }
        /* animate out */
        .mfp-removing.mfp-bg {
            opacity: 0;
        }
    </style>
    <script type="text/javascript" src="<%= VirtualPathUtility.ToAbsolute("~/js/homepage.js") %>"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            //$('.scroll-dots a').click(function (event) {
            //    event.preventDefault();
            //    var target = $(this).attr('id');
            //    console.log(target);
            //});

            //Event snippet for Kaufen Coop on http://www.biore.de/biore-skincare: Please do not remove.
            //Place this snippet on pages with events that you’re tracking.
            //Creation date: 11/20/2018

            $(".onlineKaufen").on("click", function(e) {
                e.preventDefault(); // --> if this handle didn't run first, this wouldn't work
                gtag('event', 'conversion', {
                    'allow_custom_scripts': true,
                    'send_to': 'DC-3838934/bior20/onlin0+standard'
                });
                window.location.href = $(this).attr('href');
            });

            $(".manor").on("click", function(e) {
                e.preventDefault(); // --> if this handle didn't run first, this wouldn't work
                gtag('event', 'conversion', {
                    'allow_custom_scripts': true,
                    'send_to': 'DC-3838934/bior20/filia00+standard'
                });
                window.location.href = $(this).attr('href');
            });

            $(".coop").on("click", function(e) {
                e.preventDefault(); // --> if this handle didn't run first, this wouldn't work
                gtag('event', 'conversion', {
                    'allow_custom_scripts': true,
                    'send_to': 'DC-3838934/bior20/filia0+standard'
                });
                window.location.href = $(this).attr('href');
            });

            $(".productInfo").on("click", function(e) {
                e.preventDefault(); // --> if this handle didn't run first, this wouldn't work
                gtag('event', 'conversion', {
                    'allow_custom_scripts': true,
                    'send_to': 'DC-3838934/bior20/produ0+standard'
                });
                window.location.href = $(this).attr('href');
            });


            //Event snippet for Visit Landingpage on www.biore-skincare.ch: Please do not remove.
            //Place this snippet on pages with events you’re tracking. 
            //Creation date: 11/12/2018
            gtag('event', 'conversion', {
                'allow_custom_scripts': true,
                'send_to': 'DC-3838934/bior20/visit0+standard'
            });


        });
    </script>
    

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main1">

    </div>

    <div class="home-content">
        <section class="home-section iex" id="section-01">

            <div class="welcome-module">
                
                <a href="http://www.biore.de/biore-facial-cleansing-products/" class="productInfo">
                    <img src="/images/homepage/biore-ch-products.jpg" alt="Biore Switzerland" class="productImg" />
                </a>

                <!--<h2 class="white-headline">Online und in ausgewählten Filialen erhältlich</h2>-->
                
            </div>

            <div class="btnHldr">
                        <a class="onlineKaufen" href="https://www.manor.ch/de/b/perfume/bioré?utm_source=biore&utm_medium=biore_website&utm_campaign=kam_1810_biore_perfume_d&utm_content=001" target="manor"><img src="/images/homepage/new-jetzt-online-kaufen.png" alt="jetzt online kaufen" class="btnImg" /></a>
                    </div>

                    <div class="btnHldr">
                        <a class="coop" href="https://www.coop.ch/de/services/standorte-und-oeffnungszeiten.city.html" target="coop"><img src="/images/homepage/coop-city-in-deiner.png" alt="coop city filiale" class="btnImg" /></a>
                        <a class="manor" href="https://www.manor.ch/de/store-finder?utm_source=biore&utm_medium=biore_website_storefinder&utm_campaign=kam_1810_biore_perfume_d&utm_content=001" target="manor"><img src="/images/homepage/manor-in-deiner.png" alt="manor filiale" class="btnImg" /></a>
                    </div>

         
          
        </section>
        
       <!-- <section class="home-section" id="section-05">
                    <div class="divot"><img src="/images/homepage/v-divot.png" alt="divot" /></div>
               

                    <div class="btnHldr">
                        <a href="https://www.manor.ch/de/b/perfume/bioré" target="manor"><img src="/images/homepage/jetzt-online-kaufen.png" alt="jetzt online kaufen" class="btnImg" /></a>
                    </div>

                    <div class="btnHldr">
                        <a href="https://www.coop.ch/de/services/standorte-und-oeffnungszeiten.city.html" target="coop"><img src="/images/homepage/coop-city-filiale.png" alt="coop city filiale" class="btnImg" /></a>
                        <a href="https://www.manor.ch/de/store-finder" target="manor"><img src="/images/homepage/manor-filiale.png" alt="manor filiale" class="btnImg" /></a>
                    </div>

                    <div class="btnHldrLogos">
                        <a href="https://www.coopathome.ch/de/search/?text=biore" target="retailer"><img src="/images/homepage/coop.png" alt="Coop" class="btnImgLogos" /></a>
                        <a href="https://www.coopathome.ch/de/search/?text=biore" target="retailer"><img src="/images/homepage/coop-city.png" alt="Coop City" class="btnImgLogos" /></a>
                        <a href="https://www.manor.ch/de/b/perfume/bior%C3%A9" target="retailer"><img src="/images/homepage/manor-logo.png" alt="Manor logo" class="btnImgLogos" /></a>
                    </div>

                    <div class="btnHldr">
                        <a href="http://www.biore.fr/biore-skincare"><img src="/images/homepage/decouvrez-biore.png" alt="Decouvrez Biore" class="btnImg" /></a>
                    </div>
          
        </section>-->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/jquery.flexslider.min.js")
        .Render("~/js/combinedhome_#.js")
    %>
</asp:Content>

