﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.Xml;
using System.Collections;

namespace Biore2012.BLL
{
    [System.Web.Script.Services.ScriptService]
    [XmlRoot("Product")]
    public class Product 
    {
        /// <summary>
        /// ProductRefName is the system name used as a primary key for joining data
        /// </summary>
        [XmlElement("ProductRefName")]
        public string ProductRefName;

        /// <summary>
        /// NewLookImage is the path to the new packaging product shot
        /// </summary>
        [XmlElement("NewLookImage")]
        public string NewLookImage;

        /// <summary>
        /// OldLookImage is the path to the old packaging product shot
        /// </summary>
        [XmlElement("OldLookImage")]
        public string OldLookImage;

        /// <summary>
        /// SocialImage is the path to the social packaging product shot
        /// </summary>
        [XmlElement("SocialImage")]
        public string SocialImage;

        /// <summary>
        /// Category identifies product category 
        /// </summary>
        [XmlElement("Category")]
        public string Category;

        /// <summary>
        /// Category identifies product category 
        /// </summary>
        [XmlElement("CategoryLeft")]
        public string CategoryLeft;


        // ATAM do not need in product bc moved to sideBarProducts 
        /// <summary>
        /// Category identifies product category arrow class
        /// </summary>
        //////////[XmlElement("CategoryArrowSmallProduct")]
        //////////public string CategoryArrowSmallProduct;

        /// <summary>
        /// Category identifies product category 
        /// </summary>
        [XmlElement("CategoryRight")]
        public string CategoryRight;

        [XmlElement("ProductNameLine1")]
        public string ProductNameLine1;

        [XmlElement("ProductNameLine2")]
        public string ProductNameLine2;

        [XmlElement("ProductNameLine3")]
        public string ProductNameLine3;

        /// <summary>
        /// AwardBadge
        /// </summary>
        [XmlElement("AwardBadge")]
        public AwardBadge AwardBadge;

        /// <summary>
        /// NamePrefix
        /// </summary>
        [XmlElement("NamePrefix")]
        public string NamePrefix { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [XmlElement("Name")]
        public string Name { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [XmlElement("PromoCopy")]
        public string PromoCopy { get; set; }

        /// <summary>
        /// 
        /// </summary>        
        [XmlElement("Description")]
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>        
        [XmlElement("BuyNowURL")]
        public string BuyNowURL { get; set; }

        /// <summary>
        /// 
        /// </summary>        
        [XmlElement("LikeURL")]
        public string LikeURL { get; set; }

        /// <summary>
        /// 
        /// </summary>        
        [XmlElement("WhatItIs")]
        public string WhatItIs { get; set; }

        /// <summary>
        /// 
        /// </summary>        
        [XmlElement("ProductDetails")]
        public string ProductDetails { get; set; }

        /// <summary>
        /// 
        /// </summary>        
        [XmlElement("FunFacts")]
        public string FunFacts { get; set; }

        /// <summary>
        /// 
        /// </summary>        
        [XmlElement("HowItWorks")]
        public string HowItWorks { get; set; }

        /// <summary>
        /// 
        /// </summary>        
        [XmlElement("Cautions")]
        public string Cautions { get; set; }

        /// <summary>
        /// 
        /// </summary>        
        [XmlElement("Video")]
        public string Video { get; set; }

        /// <summary>
        /// 
        /// </summary>        
        [XmlElement("VideoStillImage")]
        public string VideoStillImage { get; set; }

        /// <summary>
        /// 
        /// </summary>        
        [XmlElement("VideoDuration")]
        public string VideoDuration { get; set; }


        /// <summary>
        /// 
        /// </summary>        
        [XmlElement("SideBarHeader")]
        public string SideBarHeader { get; set; }

        /// <summary>
        /// 
        /// </summary>        
        [XmlElement("SideBarCopy")]
        public string SideBarCopy { get; set; }


        /// <summary>
        /// A collection of Sidebar products
        /// </summary>
        [System.Xml.Serialization.XmlArray("SideBarProducts")]
        [System.Xml.Serialization.XmlArrayItem("SideBarProduct", typeof(SideBarProduct))]
        public List<SideBarProduct> SideBarProductCollection;

        /// <summary>
        /// 
        /// </summary>        
        [XmlElement("MetaTitle")]
        public string MetaTitle { get; set; }

        /// <summary>
        /// 
        /// </summary>        
        [XmlElement("MetaDescription")]
        public string MetaDescription { get; set; }

        /// <summary>
        /// 
        /// </summary>        
        [XmlElement("MetaKeywords")]
        public string MetaKeywords { get; set; }

        /// <summary>
        /// 
        /// </summary>        
        [XmlElement("AdSense")]
        public string AdSense { get; set; }

        //[System.Xml.Serialization.XmlArray("XMLData")]
        //[System.Xml.Serialization.XmlArrayItem("Item", typeof(XMLData))]
        ////public ArrayList listXMLData;
        //public List<XMLData> XMLDataCollection;

        //[System.Xml.Serialization.XmlArray("RelatedProducts")]
        //[System.Xml.Serialization.XmlArrayItem("Product", typeof(String))]
        //public List<string> RelatedProductsCollection;

        

        public Product() 
        {
            SideBarProductCollection = new List<SideBarProduct>();
            //LinkDataCollection = new List<LinkBase>();
            //ContentDataCollection = new List<ContentBase>();
            //RelatedProductsCollection = new List<string>();
            //XMLDataCollection = new List<XMLData>();
            //PromotionsCollection = new List<PromoPlacement>();
            //ImageCollection = new List<ImageBase>();
            //PageBaseData = new PageBase();
  
        } 
    }
}
