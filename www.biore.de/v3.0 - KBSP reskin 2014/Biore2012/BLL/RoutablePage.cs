﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Biore2012.BLL
{
    public class RoutablePage : Page, IRoutablePage
    {
        public RoutingHelper Routing { get; set; }

        public RoutablePage()
        {
            Routing = new RoutingHelper();
        }
    }
}
