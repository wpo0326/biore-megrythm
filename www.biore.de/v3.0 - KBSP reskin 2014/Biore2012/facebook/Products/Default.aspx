﻿<%@ Page Title="" Language="C#" MasterPageFile="~/facebook/FBContentPageMaster.Master"
    AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Biore2012.facebook.Products.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="fb-root">
    </div>

    <%--<script type="text/javascript">
		var extJsHost_connect_facebook_net = (("https:" == document.location.protocol) ? "https://" : "http://");
		window.fbAsyncInit = function() {
			FB.init({
				appId: '<%= System.Configuration.ConfigurationManager.AppSettings["productsFBAppId"] %>', // App ID
				//channelURL : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
				status: true, // check login status
				cookie: true, // enable cookies to allow the server to access the session
				oauth: true, // enable OAuth 2.0
				xfbml: true  // parse XFBML
			});

			// Additional initialization code here
			FB.Canvas.setAutoGrow();
			FB.Canvas.scrollTo(0, 0);
		};

		// Load the SDK Asynchronously
		(function(d) {
			var js, id = 'facebook-jssdk'; if (d.getElementById(id)) { return; }
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = extJsHost_connect_facebook_net + "connect.facebook.net/en_US/all.js";
			d.getElementsByTagName('head')[0].appendChild(js);
		} (document));
	</script>--%>

    <div class="products">
        <div>
            <div class="logoContainer">
                <img src="images/BioreLogo.png" alt="Biore Logo" />
            </div>
            <div class="titleContainer">
                <img src="images/powerToYourPores.png" alt="Power to your pores" />
                <img src="images/bannerText.png" alt="From our newly redesigned product line to our updated website, see why Bioré Skincare is the new face of clean." />
            </div>
            <div class="productContainer">
                <img src="images/threeProducts.png" alt="Three Products Bioré" />
            </div>
            <div class="productDescriptions">
                <div class="firstColumn">
                    <img src="images/dontBeDirty.png" alt="don't be dirty" />
                    <p>
                        Draw out dirt and oil with our unique line of pore-cleansing products, including NEW Bioré<sup>&reg;</sup> Deep Pore Charcoal Cleanser and Self Heating One Minute Mask.
                    </p>
                </div>
                <div class="secondColumn">
                    <img src="images/breakUp.png" alt="break up with blackheads" />
                    <p>Banish blackheads and free your pores with our Deep Cleansing Pore Strips and Warming Anti-Blackhead Cleanser.</p>
                </div>
                <div class="thirdColumn">
                    <img src="images/acnesOutta.png" class="acneouttahere" alt="acnes outta here" />
                    <p>
                        Break the breakout cycle with our powerful Blemish Fighting Astringent, Blemish Fighting Ice Cleanser, and Acne Clearing Scrub.   
                    </p>
                </div>
            </div>
            <div class="buttonContainer">
                <a href="http://www.biore.com/en-US/biore-skincare">
                    <img src="images/visitNowBtn.png" alt="Visit http://www.biore.com/en-US/biore-skincare to view our entire product line and see which products are right for you." /></a>
            </div>

            <%--<a href="<%=System.Configuration.ConfigurationManager.AppSettings["path"] %>biore-skincare"
                target="_blank" id="visitUs">Visit us at www.biore.com/en-US/biore-skincare to learn
				more about our products.</a> <a href="<%=System.Configuration.ConfigurationManager.AppSettings["path"] %>past-biore-favorites/"
                    target="_blank" id="updatedProds"><span>Can't find your favorite?</span> Check out
					our updated lineup <span>&raquo;</span></a>--%>
        </div>
    </div>
</asp:Content>
