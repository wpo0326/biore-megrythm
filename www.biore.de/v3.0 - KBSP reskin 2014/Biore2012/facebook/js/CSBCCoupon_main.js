﻿$(function() {
	global.init();
});

var global = {
	flashPrefix: '../../flash/',
	sendVideoTrack: null,
	init: function() {
		// video link
		$("a.videoLink").removeAttr("target");

		$("a.videoLink").click(function(e) {
			e.preventDefault();
			var vidParams = $(this).attr("rel").split("::")[1].split("|");
			var posterImg = $(this).find("img").attr("src");
			global.playVideo($(this).attr("href"), $(this).parent().attr("id"), vidParams[0], vidParams[1], vidParams[2], global.flashPrefix + "FlashVideoPlayer_scale.swf", posterImg);
		});
	},

	playVideo: function(videoURL, vidDivID, vidWidth, vidHeight, vidDuration, playerURL, posterImg) {
		// SWFplayer and ContainerDiv sizes are dynamic based on vidWidth & vidHeight
		if (videoURL == "") { return false; }

		/*****************************************/
		/* IMPORTANT PARAMS TO CONFIGURE - START */
		/*****************************************/
		var flashVideoPlayerURL = playerURL;
		if (playerURL == null || playerURL == "") { flashVideoPlayerURL = global.flashPrefix + "FlashVideoPlayer_scale.swf"; }

		var videoURLForFlashPlayer = videoURL;
		var flash1_contentVersion = "9.0.115"; // Flash 9.0.115 or higher required for H.264 video codec support

		// Add 22px for the height of the control bar in the Flash player, edit if the control bar height changes in SWF
		var flashPlayerControlHeight = 22;

		// CHOOSE SOME DEFAULT HEIGHTS AND WIDTHS IF THEY ARE NOT EXPLICITLY DEFINED IN CALL TO playVideo
		var defaultVideoWidth = "448";
		var defaultVideoHeight = "252";
		/*****************************************/
		/* IMPORTANT PARAMS TO CONFIGURE - END */
		/*****************************************/

		// test function variables
		if (vidWidth == "") { vidWidth = defaultVideoWidth; }
		if (vidHeight == "") { vidHeight = defaultVideoHeight; }
		if (vidDuration == "") { vidDuration = "9999"; }
		if (playerURL == null || playerURL == "") { playerURL = global.flashPrefix + "FlashVideoPlayer_scale.swf"; }

		//Check if video is larger than the viewport, if so, resize video proportionally to fit
		var viewportY = $(".videoHolder").height();
		var viewportX = $(".videoHolder").width();

		vidWidth = parseInt(vidWidth);
		vidHeight = parseInt(vidHeight);
		if (vidWidth > viewportX || vidHeight > viewportY) {
			var vidPorportions = vidWidth / vidHeight;
			var orgW = vidWidth;
			var orgH = vidHeight;
			if (vidHeight > viewportY) {
				vidHeight = viewportY;
				vidWidth = orgW > orgH ? vidHeight * vidPorportions : vidHeight / vidPorportions;
				if (vidWidth > viewportX) {
					vidWidth = viewportX;
					vidHeight = orgW > orgH ? vidWidth / vidPorportions : vidWidth * vidPorportions;
				}
			} else {
				vidWidth = viewportX;
				vidHeight = orgW > orgH ? vidWidth / vidPorportions : vidWidth * vidPorportions;
				if (vidHeight > viewportY) {
					vidHeight = viewportY;
					vidWidth = orgW > orgH ? vidHeight * vidPorportions : vidHeight / vidPorportions;
				}
			}
		}

		var playerWidth = parseInt(vidWidth);

		// Add the height of the control bar in the Flash player
		var playerHeight = parseInt(vidHeight) + flashPlayerControlHeight;
		global.createVideoObject(flash1_contentVersion, vidDivID, videoURL, vidWidth, vidHeight, vidDuration, flashVideoPlayerURL, playerWidth, playerHeight, posterImg);

		// this is for after the video is embedded, making it re-playable
		if (global.detectAndroid()) {
			$("video").live("click", function() {
				this.play();
			});
		}
	},

	createNonFlashContent: function(vidDivID) {
		//Non-Flash Content for Video
		var nonflashVideoContentPart1 = "To see this video you need JavaScript enabled and the latest version of Flash.";
		var nonflashVideoContentPart2LinkText = "Click here";
		var nonflashVideoContentPart2LinkUrl = "http://www.adobe.com/go/getflashplayer"
		var nonflashVideoContentPart2Text = " to go to the Adobe Flash download center.";

		var noflashDiv = $("<div></div>").attr("id", vidDivID + "noflash").addClass("no-flash");
		var noflashP1 = $("<p></p>").text(nonflashVideoContentPart1);
		var noflashP2link = $("<a></a>").attr({ href: nonflashVideoContentPart2LinkUrl, target: "_blank" }).text(nonflashVideoContentPart2LinkText);
		var noflashP2 = $("<p></p>").text(nonflashVideoContentPart2Text);
		noflashP2.prepend(noflashP2link);
		noflashDiv.append(noflashP1).append(noflashP2);
		return noflashDiv;
	},

	createVideoObject: function(flash1_contentVersion, vidDivID, videoURL, vidWidth, vidHeight, vidDuration, playerURL, playerWidth, playerHeight, posterImg) {
		$("#" + vidDivID).html(global.createNonFlashContent(vidDivID));
		if (swfobject.hasFlashPlayerVersion(flash1_contentVersion)) {
			//EMBED SWFObject Flash Player
			var flashvars = {};
			flashvars.videoUrl = videoURL;
			flashvars.videoWidth = vidWidth;
			flashvars.videoHeight = vidHeight;
			flashvars.videoDuration = vidDuration;
			var flashparams = {};
			flashparams.menu = "false";
			flashparams.scalemode = "noscale";
			flashparams.quality = "high";
			flashparams.wmode = "opaque";
			flashparams.bgcolor = "#3E3C3C";
			var flashattributes = {};
			swfobject.embedSWF(playerURL, vidDivID, playerWidth, playerHeight, flash1_contentVersion, null, flashvars, flashparams, flashattributes);
		}
	},

	detectAndroid: function() {
		var Android = ['android'];
		var userAgent = navigator.userAgent.toLowerCase();
		for (var i = 0; i < Android.length; i++) {
			if (userAgent.indexOf(Android[i]) != -1) {
				return true;
			}
		}
		return false;
	},

	trackVideoTime: function() {
		var curTime = this.currentTime.toFixed(0);
		var vidLength = this.duration.toFixed(0);
		var eventType = "VideoProgress_";
		var videoURL = this.src;
		if (
            curTime == vidLength * .9 ||
            curTime == vidLength * .75 ||
            curTime == vidLength * .5 ||
            curTime == vidLength * .25
        ) {
			eventType += ((curTime / vidLength) * 100).toString();
			// use timeout so when it hits this multiple times within a second, it will only call tracker once
			clearTimeout(global.sendVideoTrack);
			global.sendVideoTrack = setTimeout(function() {
				goToPage('VideoPlayerEvent/VideoProgress_' + eventType + '/' + videoURL + '/');
			}, 1000);
		}
	}
}

/* Flash Tracking */
function riaTrack(trackingParam) {
	goToPage(trackingParam);
}

function goToPage(pg) {
	dcsMultiTrack('DCS.dcsuri', pg, 'DCS.dcsqry', '', 'WT.ti', pg);
	firstTracker._trackPageview(pg);
	secondTracker._trackPageview(pg);
}

function getQuerystring(key, default_) {
	if (default_ == null) default_ = "";
	key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
	var qs = regex.exec(window.location.href);
	if (qs == null)
		return default_;
	else
		return qs[1];
}
