﻿<%@ Page Title="" Language="C#" MasterPageFile="~/facebook/FBContentPageMaster.Master"
	AutoEventWireup="true" CodeBehind="TopTenDatingRules.aspx.cs" Inherits="Biore2012.facebook.AlloysDatingRules.TopTenDatingRules" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<meta property="fb:admins" content="1064275431,25700203,1584985879" />
	<meta property="fb:app_id" content="<%= System.Configuration.ConfigurationManager.AppSettings["datingrulesFBAppId"] %>" />
	<script>
		var FBVars = {}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div id="fb-root">
	</div>

	<script type="text/javascript">
		var extJsHost_connect_facebook_net = (("https:" == document.location.protocol) ? "https://" : "http://");
		window.fbAsyncInit = function() {
			FB.init({
				appId: '<%= System.Configuration.ConfigurationManager.AppSettings["datingrulesFBAppId"] %>', // App ID
				//channelURL : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
				status: true, // check login status
				cookie: true, // enable cookies to allow the server to access the session
				oauth: true, // enable OAuth 2.0
				xfbml: true  // parse XFBML
			});

			// Additional initialization code here
			FB.Canvas.setAutoGrow();
			FB.Canvas.scrollTo(0, 0);
		};

		// Load the SDK Asynchronously
		(function(d) {
			var js, id = 'facebook-jssdk'; if (d.getElementById(id)) { return; }
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = extJsHost_connect_facebook_net + "connect.facebook.net/en_US/all.js";
			d.getElementsByTagName('head')[0].appendChild(js);
		} (document));
	</script>

	<script>!function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = "//platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } } (document, "script", "twitter-wjs");</script>

	<div class="datingRules topTenPg">
		<div id="topNav">
			<a href="Default.aspx" class="btn videoFeeds">Videos and Feeds</a>
			<a href="TopTenDatingRules.aspx" class="btn topTen current">Top Ten Dating Rules</a>
		</div>
		<div id="top">
			<h1 class="ir">Top 10 Dating Rules</h1>
			<p class="instruction">Vote for your favorite <strong>Dating Rules</strong> below&mdash;and share them with your friends.
				See which rules are getting the most votes and have made it into the top 10!</p>
			<p class="displayBtns">
				<a href="#" id="showTopTen">Show <span>Top Ten</span></a>
				<a href="#" id="showAll">Show <span>All</span></a></p>
			<h2 class="ir">Click below to cast your vote</h2>
		</div>
		<div id="grid">
			<asp:ListView runat="server" ID="lvGrid" OnItemDataBound="lvGrid_ItemDataBound">
				<LayoutTemplate>
					<ul>
						<div runat="server" id="itemPlaceholder"></div>
					</ul>
				</LayoutTemplate>
				<ItemTemplate>
					<li class="gridDouble">
						<div class="gridItem">
							<asp:HyperLink ID="hlGridItem" CssClass="gridItemLink" runat="server"></asp:HyperLink>
							<div class="screenGridItem"></div>
							<div class="overlay" id="dvOverlay" runat="server">
								<a href="#" class="overlayClose">X</a>
								<h3 class="ir">Cast Your Vote!</h3>
								<asp:Image ID="imgOverlay" runat="server" />
								<p class="ir voteDesc"><asp:Literal ID="litDescription" runat="server" /></p>
								<div class="voteShare">
									<asp:HyperLink ID="hlLuvIt" CssClass="luvit" runat="server" NavigateUrl="#" Text="Luv It!" />
									<asp:HyperLink ID="hlShare" CssClass="shareBtn" runat="server" NavigateUrl="#" Text="Share" />
									<asp:HyperLink ID="hlMeh" CssClass="meh" runat="server" NavigateUrl="#" Text="Meh." />
								</div>
								<h4 class="voted">Thanks for voting!</h4>
								<p class="voted">Now check out the rest of the Dating Rules.</p>
								<a href="#" class="overlayClose" style="top:640px;">Close X</a>
							</div>							
						<asp:Literal ID="litCenterBlockText" runat="server" />
						</div>
						<asp:Literal ID="litLastLI" runat="server" />
				</ItemTemplate>
				<AlternatingItemTemplate>						
						<div class="gridItem">
							<asp:HyperLink ID="hlGridItem" CssClass="gridItemLink" runat="server"></asp:HyperLink>
							<div class="screenGridItem"></div>
							<div class="overlay" id="dvOverlay" runat="server">
								<a href="#" class="overlayClose">X</a>
								<h3 class="ir">Cast Your Vote!</h3>
								<asp:Image ID="imgOverlay" runat="server" />
								<p class="ir voteDesc"><asp:Literal ID="litDescription" runat="server" /></p>
								<div class="voteShare">
									<asp:HyperLink ID="hlLuvIt" CssClass="luvit" runat="server" NavigateUrl="#" Text="Luv It!" />
									<asp:HyperLink ID="hlShare" CssClass="shareBtn" runat="server" NavigateUrl="#" Text="Share" />
									<asp:HyperLink ID="hlMeh" CssClass="meh" runat="server" NavigateUrl="#" Text="Meh." />
								</div>
								<h4 class="voted">Thanks for voting!</h4>
								<p class="voted">Now check out the rest of the Dating Rules.</p>
								<a href="#" class="overlayClose" style="top:640px;">Close X</a>
							</div>
						</div>
					</li>
				</AlternatingItemTemplate>
			</asp:ListView>
		</div>
	</div>
	<%=SquishIt.Framework.Bundle.JavaScript()
		.Add("~/facebook/js/jquery-1.6.4.min.js")
		.Add("~/facebook/AlloysDatingRules/js/ttUtils.js")
		.Render("~/facebook/AlloysDatingRules/js/combinedTopTen_#.js")
	%><script>
	  	FBVars = {
	  		fbAppId: '<%= System.Configuration.ConfigurationManager.AppSettings["datingrulesFBAppId"] %>',
	  		FBShareUrl: '<%= System.Configuration.ConfigurationManager.AppSettings["FBShareURL"] %>',
	  		FBSharePicPath: '<%= System.Configuration.ConfigurationManager.AppSettings["FBSharePicURL"] %>',
	  		baseURL: '<%= System.Configuration.ConfigurationManager.AppSettings["ServerBaseUrl"]%>'
	  	};
		(window.JSON && typeof JSON.stringify === 'function' && typeof JSON.parse === 'function') || 
			document.write(unescape("%3Cscript src='" + 'js/json2.min.js' + "'%3E%3C/script%3E"));</script>
</asp:Content>
