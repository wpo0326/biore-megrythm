﻿<%@ Page Title="Free Your Pores – Get Clean, Healthy Skin – Browse All Products  | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.our_products.Default" %>
<%@ Register Src="~/BazaarVoice/Controls/BVScripts.ascx" TagPrefix="uc1" TagName="BVScripts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Bior&eacute;&reg; Skincare products cleanse and gently exfoliate, giving you healthy, clean skin! See the complete Bior&eacute;&reg; Skincare product line." name="description" />
    <meta content="Clear Skin, Biore&reg; Products" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>
    <meta name="viewport" content="width=device-width">
    <uc1:BVScripts runat="server" ID="BVScripts" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="titleh1">
                <h1>Our Products</h1>
                <div id="responseRule"></div>
                <p>Target the root of all skin problems &mdash; by stripping weekly and cleansing daily.  Our powerful, pore-cleansing products come in liquid, foam, scrub, and strip forms to keep your skin clean and healthy.</p>
            </div>

            

            <div id="deepCleansingProds" class="prodList">
                <h2 class="pie roundedCorners deepCleansing"> <span>Charcoal</span> for<br />Normal / Oily Skin
                    <span class="arrow"></span>
                </h2>
                <ul>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CHARCOAL_100X235.png" alt="" />
                        <h3>Deep Pore<br />Charcoal Cleanser</h3>
                        <p>Charcoal formula acts as a magnet to draw out dirt and oil for a tingly clean.</p>
                        <div id="BVRRInlineRating-deep-pore-charcoal-cleanser" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/deep-pore-charcoal-cleanser" id="A1">details ></a>
                    </li>
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreMinimizer.png" alt="" />
                        <h3>Charcoal<br />
                      Pore Minimizer</h3>
                        <p>Exfoliates and sucks up gunk for visibly smaller pores.</p>
                        <div id="BVRRInlineRating-charcoal-pore-minimizer" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/charcoal-pore-minimizer" id="details-charcoal-pore-minimizer">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/SelfHeatingMask.jpg" alt="" style="margin:0 0 2px 44px;"/>
                        <h3>Self Heating<br />One Minute Mask</h3>
                        <p>Heats on contact to remove dirt and oil for tingly-smooth, clean skin.</p>
                        <div id="BVRRInlineRating-self-heating-one-minute-mask" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/self-heating-one-minute-mask" id="A2">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" class="cps-custom"/>
                        <h3>Deep Cleansing<br />Charcoal Pore Strips</h3>
                        <p>Unclogs pores &amp; draws out excess oil for the deepest clean.</p>
                        <div id="BVRRInlineRating-charcoal-pore-strips-regular" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/charcoal-pore-strips#regular" id="details-deep-cleansing-pore-strips-charcoal">details ></a>
                    </li>
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/makeupRemovingTowelettes.png" alt="" />
                        <h3>Daily Makeup Removing Towelettes</h3>
                        <p>Remove stubborn waterproof makeup and clean pores.</p>
                        <a href="../charcoal-for-oily-skin/make-up-removing-towelettes" id="details-make-up-removing-towelettes">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/4In1DetoxifyingCleanser.png" alt="" />
                        <h3>Pore Detoxifying Foam Cleanser</h3>
                        <p>Clean, tone, and stimulate with our self-foaming formula.</p>
                        <a href="../charcoal-for-oily-skin/pore-detoxifying-foam-cleanser" id="details-pore-detoxifying-foam-cleanser">details ></a>
                    </li>  -->  
                </ul>
            </div>

            <div id="murtProds" class="prodList">
                <h2 class="pie roundedCorners murt"><span>Pore Strips</span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.png" alt="" />
                        <h3>Deep Cleansing Pore<br />Strips Combo</h3>
                        <p>Unclog pores with magnet-like power on your nose, chin, cheeks, and forehead.</p>
                        <div id="BVRRInlineRating-pore-strips-combo" class="ratingCategory"></div>
                        <a href="../pore-strips/pore-strips-combo" id="details-deep-cleansing-pore-strips-combo">details ></a>
                    </li>
		   <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" />
                        <h3>Deep Cleansing<br />Charcoal Pore Strips</h3>
                        <p>Unclogs pores &amp; draws out excess oil for the deepest clean.</p>
                        <div id="BVRRInlineRating-charcoal-pore-strips-regular" class="ratingCategory"></div>
                        <a href="../charcoal-for-oily-skin/charcoal-pore-strips#regular" id="details-deep-cleansing-pore-strips-charcoal">details ></a>
                    </li>
                    
                    <!-- <li>
                        <img src="../images/ourProducts/products/small/combinationSkinBalancingCleanser.png" alt="" />
                        <h3>COMBINATION SKIN BALANCING CLEANSER</h3>
                        <p>Get an even, all-over clean for the complex needs of combination skin.</p>
                        <a href="../charcoal-for-oily-skin/combination-skin-balancing-cleanser" id="details-combination-skin-balancing-cleanser">details ></a>
                    </li>
                    <li class="taller" id="dailyCleansingCloths">
                        <img src="../images/ourProducts/products/small/dailyCleansingCloths.png" alt="" />
                        <h3>DAILY CLEANSING CLOTHS</h3>
                        <p>Wipe away dirt to reveal healthy, glowing skin.</p>
                        <a href="../charcoal-for-oily-skin/daily-cleansing-cloths" id="details-daily-cleansing-cloths">details ></a>
                    </li>
                    
                    <li>
                        <img src="../images/ourProducts/products/small/4In1DetoxifyingCleanser.png" alt="" />
                        <h3>4-IN-1 DETOXIFYING CLEANSER</h3>
                        <p>Revitalize and refresh for healthier-looking skin.</p>
                        <a href="../charcoal-for-oily-skin/4-in-1-detoxifying-cleanser" id="details-4-in-1-detoxifying-cleanser">details ></a>
                    </li>
                     <li>
                        <img src="../images/ourProducts/products/small/steamActivatedCleanser.jpg" alt="" />
                        <h3>STEAM ACTIVATED CLEANSER</h3>
                        <p>Harness the power of pore-opening steam for a truly deep clean.</p>
                        <a href="../charcoal-for-oily-skin/steam-activated-cleanser" id="details-steam-activated-cleanser">details ></a>
                    </li> -->
                </ul>
            </div>
            
            <div id="bigPoresProds" class="prodList">
                <h2 class="pie roundedCorners bigPores"><span>Baking Soda</span><br />for Combination Skin
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img id="dontBeDirtyProducts" src="../images/ourProducts/products/small/Baking_Soda_Cleanser_small.png" alt="" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:8pt;">New</span> Baking soda Pore Cleanser</h3>
                        <p>Deep cleans & exfoliates dry, flaky skin.</p>
                        <div id="BVRRInlineRating-baking-soda-pore-cleanser" class="ratingCategory"></div>
                        <a href="../baking-soda-for-combination-skin/baking-soda-pore-cleanser" id="A1">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-cleansing-scrub-small.png" alt="" />
                        <h3><span style="font-family: ProximaNova-Bold, Arial;color:yellow;font-size:8pt;">New</span> Baking soda Cleansing scrub</h3>
                        <p>Deep clean without over-scrubbing.</p>
                        <div id="BVRRInlineRating-baking-soda-cleansing-scrub" class="ratingCategory"></div>
                        <a href="../baking-soda-for-combination-skin/baking-soda-cleansing-scrub" id="A1">details ></a>
                    </li>             
                </ul>
            </div>

            <!--<div id="complexionClearingProds" class="prodList">
                <h2 class="pie roundedCorners complexionClearing">acne&rsquo;s <span>outta here!<sup>&reg;</sup></span>
                    <span></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishFightingIceCleanser.png" alt="" />
                        <h3>BLEMISH FIGHTING<br />ICE CLEANSER</h3>
                        <p>With Salicylic acid, this formula cleans pores of dirt, oil, and makeup.</p>
                        <div id="BVRRInlineRating-blemish-fighting-ice-cleanser" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/blemish-fighting-ice-cleanser" id="details-blemish-fighting-ice-cleanser">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/ACS_Tube.jpg" alt="" />
                        <h3>ACNE CLEARING<br />SCRUB</h3>
                        <p>Defeat deep-down dirt and oil in 2 days.</p>
                        <div id="BVRRInlineRating-acne-clearing-scrub" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/acne-clearing-scrub" id="details-acne-clearing-scrub">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishTreatingAstringentToner.png" alt="" />
                        <h3>BLEMISH FIGHTING ASTRINGENT</h3>
                        <p>Help prevent breakouts with an oil-free toner.</p>
                        <div id="BVRRInlineRating-blemish-fighting-astringent-toner" class="ratingCategory"></div>                        
                        <a href="../acnes-outta-here/blemish-fighting-astringent-toner" id="details-blemish-fighting-astringent">details ></a>
                    </li>
                </ul>
            </div>-->

        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
