﻿<?xml version="1.0" encoding="utf-8" ?>
<Product>
  <ProductRefName>blemish-treating-astringent</ProductRefName>
  <NewLookImage>~/images/ourProducts/products/large/blemishTreatingAstringent.png</NewLookImage>
  <OldLookImage>~/images/ourProducts/products/large-old/blemishTreatingAstringentOld.png</OldLookImage>
  <SocialImage>~/images/ourProducts/products/social/blemishTreatingAstringent.png</SocialImage>
  <AwardBadge>
    <AwardImageURL></AwardImageURL>
    <VerticalOffset></VerticalOffset>
    <HorizontalOffset></HorizontalOffset>
  </AwardBadge>
  <Category>Complexion Clearing</Category>
  <PromoCopy></PromoCopy>
  <Name>Blemish Fighting Astringent</Name>
  <Description>Salicylic acid acne treatment. Helps treat blemishes and reduce surface shine for clearer skin.</Description>
  <BuyNowURL>http://www.drugstore.com/biore-blemish-treating-astringent/qxp388179?catid=182998&amp;aid=328279&amp;aparam=biore_bs_bn</BuyNowURL>
  <LikeURL>~/social/permalinks/blemish-treating-astringent/</LikeURL>
  <WhatItIs>
    <![CDATA[
    <h4>Got blemishes? Get treatment.</h4>
    <p>This power-packed astringent formula goes beyond just cleaning and actually helps treat blemishes and helps prevent future breakouts. Use it on its own or as a follow-up to Blemish Fighting Ice Cleanser or Pore Unclogging Scrub. Plus, it's the perfect pre-treatment to give your Pore Strip maximum power. And since we love your skin and want to treat it nicely, we made our astringent gentle enough to use every day.</p>
    <ul>
      <li>Oil-Free</li>
    </ul>
    ]]>
  </WhatItIs>
  <ProductDetails>
    <![CDATA[
    <h4>INGREDIENTS</h4>
    <div class="activeIngredient">
        <h5>Active Ingredient:</h5>
        <p>Salicylic Acid 2%</p><span class="dots"></span>
    </div>
    <div class="purpose">
        <h5>Purpose</h5>
        <p>Acne Treatment</p>
    </div>
    <div class="clear"></div>
    <p><strong>Use:</strong> For the treatment of acne</p>
    <p>STORE AT ROOM TEMPERATURE</p>
    <h5>Inactive Ingredients:</h5>
    <p>WATER, ALCOHOL DENAT., ISOPROPYL ALCOHOL, HAMAMELIS VIRGINIANA (WITCH HAZEL) WATER, GLYCERIN, ALCOHOL, MENTHOL, BENZOPHENONE-4, POLYSORBATE 60, CITRIC ACID, SODIUM CITRATE, SODIUM HYDROXIDE, DISODIUM EDTA, FRAGRANCE, BLUE 1, RED 4, EXT. VIOLET 2.</p>
    <h4>NET CONTENTS</h4>
    <p>8.0 FL OZ (236 mL)</p>
    <p>Distributed by<br/>Kao USA Inc.<br/>Cincinnati, OH 45214<br/>&copy; 2013<br/>Reg. U.S. Pat. &amp; Tm. Off.<br/>Made in U.S.A.<br/><br/>16991-0-400</p>
    ]]>
  </ProductDetails>
  <FunFacts></FunFacts>
  <HowItWorks>
    <![CDATA[
    <ul>
      <li>On the surface, this astringent formula instantly removes oil, dirt, and make-up as it cuts down on surface shine.</li>
      <li>Meanwhile, deep down, it helps clear skin by treating blemishes and helping to prevent new breakouts.</li>
    </ul>
    <h4>How to use</h4>
    <ul>
      <li>For optimal results: Cleanse skin thoroughly before applying astringent. Moisten cotton ball or pad and pat over face and neck. Note: Because too much drying of the skin may occur, start with 1 application daily, then gradually increase to 2 or 3 times daily, if needed or as directed by a doctor. If bothersome dryness or peeling occurs, reduce application to once a day or every other day.</li>
    </ul>
    ]]>
  </HowItWorks>
  <Cautions>
    <![CDATA[
    <ul>
      <li>Flammable: keep away from fire or flame.</li>
      <li>For external use only. Using other topical acne medications at the same time or right after use of this product may increase dryness or irritation of the skin. If this occurs, only one medication should be used unless directed by a doctor.</li>
      <li>When using this product, avoid contact with eyes. If contact occurs, flush thoroughly with water.</li>
      <li>Keep out of reach of children. If swallowed, get medical help or contact a Poison Control Center right away.</li>
    </ul>
    ]]>
  </Cautions>
  <Video></Video>
  <VideoStillImage></VideoStillImage>
  <VideoDuration></VideoDuration>
  <SideBarHeader>Works great with:</SideBarHeader>
  <SideBarCopy></SideBarCopy>
  <SideBarProducts>
    <SideBarProduct>
      <ProductName>Pore Unclogging Scrub</ProductName>
      <Image>~/images/ourProducts/products/small/poreUncloggingScrub.jpg</Image>
      <ProductURL>~/charcoal-for-oily-skin/pore-unclogging-scrub</ProductURL>
      <ClassName></ClassName>
    </SideBarProduct>
    <SideBarProduct>
      <ProductName>Blemish Fighting Ice Cleanser</ProductName>
      <Image>~/images/ourProducts/products/small/blemishFightingIceCleanser.jpg</Image>
      <ProductURL>~/acnes-outta-here/blemish-fighting-ice-cleanser</ProductURL>
      <ClassName></ClassName>
    </SideBarProduct>
    <SideBarProduct>
      <ProductName>Deep Cleansing Pore Strips Combo</ProductName>
      <Image>~/images/ourProducts/products/small/deepCleansingPoreStripsCombo.jpg</Image>
      <ProductURL>~/deep-cleansing-product-family/pore-strips#combo</ProductURL>
      <ClassName></ClassName>
    </SideBarProduct>
  </SideBarProducts>
  <MetaTitle><![CDATA[Get Clear Skin - Blemish Treating Astringent | Bior&eacute;&reg; Skincare ]]></MetaTitle>
  <MetaDescription>Treat blemishes and get clear skin! Learn more about Blemish Treating Astringent from Bioré® Skincare.</MetaDescription>
  <MetaKeywords>clear skin, treat blemishes, astringent</MetaKeywords>
</Product>