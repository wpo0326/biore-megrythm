﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Biore2012.biore_facial_cleansing_products
{
    public partial class BlemishFightingTeam : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Grab masterpage, set appropriate body classes
            Site myMaster = (Site)this.Master;
            myMaster.bodyClass += " ourProducts productDetail complexionClearing";

            // Draw Like button if not on a mobile device
            if (myMaster.isMobile == false)
            {
                myMaster.FindControl("plusScript").Visible = true;

            }
        }
       
    }
}