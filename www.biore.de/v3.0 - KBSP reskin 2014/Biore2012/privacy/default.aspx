﻿<%@ Page Title="Datenschutzrichtlinien | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master"
    Language="C#" AutoEventWireup="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
    <style type="text/css">
        #content p {
            text-align: left;
        }
        #content ul {
            margin: auto auto auto 8em;
        }
        #content ul li {
            list-style: square;
        }

            #content ul li p {
                margin-left: 0;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="about">
        <div id="main">
            <div id="mainContent">
                <div class="centeringDiv">
                    <div id="content">
                        <div class="titleh1">
                            <h1 class="privacy">
                                Datenschutzrichtlinien</h1>
                            <div id="responseRule">
                            </div>
                        </div>
                        <p>
                           DATENSCHUTZERKLÄRUNG
                        </p>
                        
<p><b>Zuletzt am 09.05.2018 überarbeitet</b></p>

<p>Die Guhl Ikebana GmbH, a Kao Group Company, Pfungstädter Straße 98, 64297 Darmstadt  („<b>Kao Unternehmen</b>“ oder „<b>wir</b>“ oder „<b>unser</b>“) und jedes seiner verbundenen Unternehmen und Tochterunternehmen in der EMEA-Region, zusammen der „<b>Kao-Gruppe</b>“ nehmen den Datenschutz ernst. Diese Datenschutzerklärung gibt den Nutzern der https://www.biore.de  („Webseite“) und aller anderen Webseiten oder mobilen Apps des Kao-Unternehmens, die auf diese Datenschutzerklärung verweisen, Auskunft darüber, wie wir als Verantwortlicher im Sinne der Datenschutz-Grundverordnung („DSGVO“) personenbezogene Daten und andere Daten solcher Nutzer im Zusammenhang mit ihrer Nutzung der Webseite erheben und verarbeiten.
    </p>

<p>Beachte bitte, dass andere Webseiten oder mobile Apps der Kao-Gruppe anderen Datenschutzrichtlinien unterliegen können.</p>

<p><b>1. Kategorien personenbezogener Daten und Verarbeitungszwecke - <i>Welche personenbezogenen Daten verarbeiten wir von Dir und weshalb?</i></b></p>

<p><b>1.1 Metadaten</b><br />
Du kannst die Webseite benutzen, ohne detaillierte personenbezogene Daten über dich anzugeben. In einem solchen Fall werden wir nur die folgenden Metadaten erheben, die sich aus einer Benutzung der Webseite ergeben: Browsertyp und -version, Betriebssystem und Schnittstelle, Webseite, von der du auf unsere Webseite verlinkt wurdest (Referral-URL), Internetseite(n), die du auf unserer Webseite besuchst, Datum und Uhrzeit, an dem du auf unsere Webseite zugreifst sowie die Internetprotokoll-(IP)-Adresse.
</p>

<p><b>1.2  Gewinnspiele</b><br />
Wenn du an einem Gewinnspiel teilnimmst, erheben wir die folgenden personenbezogenen Daten über dich: Name, Postanschrift, E-Mail-Adresse, , Datum der Teilnahme, Auswahl als Gewinner, Preis, Antwort auf eine Gewinnspielfrage. Wir verarbeiten solche personenbezogenen Daten zur Durchführung des Gewinnspiels, der Benachrichtigung des Gewinners, der Lieferung des Preises an den Gewinner, der Durchführung der Veranstaltung, dem Bereitstellen von Marketingmaterialien im gesetzlich zulässigen Umfang.</p>

<p><b>1.3 Kontaktformular</b><br />
Auf unsere Webseite bieten wir dir die Möglichkeit uns mittels Kontaktformular zu kontaktieren. Dazu benötigen wir folgende personenbezogene Daten von dir: Vorname, Nachname, Email-Adresse, Postanschrift, Geschlecht, Geburtsdatum und die Telefonnummer. Die personenbezogenen Daten, die du uns im Rahmen dieser Kontaktanfrage zur Verfügung stellst, werden nur für die Beantwortung deiner Anfrage/Kontaktaufnahme und für die damit verbundene technische Administration verwendet. Die Weitergabe an Dritte findet nicht statt. Deine personenbezogenen Daten werden nach der Bearbeitung deiner Anfrage gelöscht oder sobald du die hier erteilte Einwilligung zur Speicherung widerrufst.  </p>


<p><b>1.4 Google Analytics</b><br />
Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf deinem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch dich ermöglichen. Die durch den Cookie erzeugten Informationen über deine Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert.</p>

<p>Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite, wird deine IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Die IP-Anonymisierung ist auf dieser Website aktiv. </p>

<p>Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen.</p>

<p>Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt. Du kannst die Speicherung der Cookies durch eine entsprechende Einstellung deiner Browser-Software verhindern; wir weisen dich jedoch darauf hin, dass du in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen kannst. </p>

<p>Du kannst darüber hinaus die Erfassung der durch das Cookie erzeugten und auf deine Nutzung der Website bezogenen Daten (inkl. deiner IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem du das unter dem folgenden Link verfügbare Browser-Plugin herunterlädst und installierst: tools.google.com/dlpage/gaoptout. </p>

<p>Alternativ zum Browser-Plugin und insbesondere für mobile Browser klicke bitte auf den folgenden Link, um ein Opt-Out-Cookie zu setzen. Dieses Opt-Out-Cookie verhindert die Erfassung durch Google Analytics innerhalb dieser Website. <a href="/privacy/?google-analytics-opt-out=true">/http://www.biore.de/privacy/?google-analytics-opt-out=true</a></p>

<p><b>1.6 Adobe Analytics</b><br />
Auf dieser Website kommt außerdem Adobe Analytics, ein Webanalysedienst der Adobe Systems Software Ireland Limited („Adobe“), zum Einsatz. Adobe Analytics verwendet sog. “Cookies”, Textdateien, die auf deinem Computer gespeichert werden und die eine Analyse der Benutzung der Webseite durch Sie ermöglichen. Werden die durch den Cookie erzeugten Informationen über die Benutzung der Website an einen Server von Adobe übermittelt, dann ist durch die Einstellungen gewährleistet, dass die IP-Adresse vor der Geolokalisierung anonymisiert und vor Speicherung durch eine generische IP-Adresse ersetzt wird. Im Auftrag des Betreibers dieser Website wird Adobe diese Informationen benutzen, um die Nutzung der Website durch die Nutzer auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Adobe Analytics von deinem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Adobe zusammengeführt. Du kannst die Speicherung der Cookies durch eine entsprechende Einstellung deiner Browser-Software verhindern. Wir weisen dich jedoch darauf hin, dass du in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich nutzen kannst. Du kannst darüber hinaus die Erfassung der durch das Cookie erzeugten und auf deine Nutzung der Website bezogenen Daten (inkl. deiner IP-Adresse) an Adobe sowie die Verarbeitung dieser Daten durch Adobe verhindern, indem du das unter dem folgenden Link verfügbare Browser-Plug-In herunterlädst und installierst: http://www.adobe.com/privacy/opt-out.html</p>

<p><b>2. Verarbeitungsgrundlagen und Rechtsfolgen - <i>Auf welcher Rechtsgrundlage erfolgt die Verarbeitung deiner personenbezogenen Daten und was passiert, wenn du die personenbezogenen Daten nicht angeben willst</i></b><br />
Das Unternehmen stützt sich bei der Verarbeitung personenbezogener Daten auf folgende Rechtsgrundlagen: </p>

     <ul><li><p>Ihre Zustimmung zur Verarbeitung Ihrer Daten zu einem oder mehreren spezifischen Zwecken.</p></li></ul>

<p><b>3. Kategorien von Empfängern und internationale Übermittlungen - <i>An wen übermitteln wir deine personenbezogenen Daten und wo befinden sie sich?</i></b></p>

<p>Wir können Ihre personenbezogenen Daten unter Umständen zu den oben beschriebenen Verarbeitungszwecken wie folgt an Dritte übermitteln:</p>
    <ul>
        <li><p><b>innerhalb der Kao-Gruppe</b>: unser Mutterunternehmen, die Kao Corporation in Japan und jedes ihrer verbundenen Unternehmen und Tochterunternehmen (jedes verbundene Unternehmen oder Tochterunternehmen, einschließlich uns, nachstehend als „<b>Kao Unternehmen</b>“; zusammen als der „<b>Kao-Gruppe</b>“ bezeichnet) innerhalb der globalen Kao-Gruppe können unter Umständen deine personenbezogenen Daten zu den oben beschriebenen Verarbeitungszwecken erhalten. Je nach Kategorien personenbezogener Daten und Zwecke, für welche die personenbezogenen Daten erhoben wurden, können interne Abteilungen innerhalb des Kao-Unternehmens deine personenbezogenen Daten erhalten. Darüber hinaus können Abteilungen innerhalb des Kao-Unternehmens Zugriff auf personenbezogene Daten über dich auf einer Need-To-Know-Basis haben, wie z.B. die Rechtsabteilung.</p></li>
        <li><p><b>an Auftragsverarbeiter</b>: Bestimmte Dritte, unabhängig davon, ob es sich dabei um verbundene Unternehmen handelt oder nicht, können deine personenbezogenen Daten zur Verarbeitung solcher Daten gemäß entsprechender Anweisungen („<b>Auftragsverarbeiter</b>“), wie es für die oben beschriebenen Verarbeitungszwecke notwendig ist, erhalten, wie z.B. Anbieter von Webseiten Auftragsabwicklungen, Kundendienst-Dienstleistungen, Marketingdienstleistungen, IT-Unterstützungsdienstleistungen und andere Dienstleister, die uns bei der Aufrechterhaltung unserer Geschäftsbeziehung mit dir unterstützen. Die Datenverarbeiter werden vertraglich verpflichtet, geeignete technische und organisatorische Maßnahmen zur Sicherung der personenbezogenen Daten umzusetzen und die personenbezogenen Daten nur nach Anweisung zu verarbeiten. Für unser Webangebot setzen wir folgende Dienstleister ein: CG Marketing Communication, <a href="http://www.cgmarketingcommunications.com" target="privacy">www.cgmarcom.com</a>, Parasol Island GmbH, https://www.parasol-island.com sowie HeberLink GmbH, https://heberlink.de</p></li>
        <li><p><b>Sonstige Empfänger</b>: Wir können personenbezogene Daten - gemäß des anwendbaren Datenschutzgesetzes - an Strafverfolgungsbehörden, Regierungs- und Justizbehörden, Rechtsbeistände, externe Berater oder Geschäftspartner übermitteln. Bei Unternehmensakquisitionen und -zusammenschlüssen können personenbezogene Daten ggf. an Dritte übermittelt werden, die an der/dem Unternehmensakquisition oder -zusammenschluss beteiligt sind. Wir werden deine personenbezogenen Daten Dritten ohne deine Zustimmung nicht zu Werbe- oder Marketing- oder sonstigen Zwecken offenlegen.</p></li>
    </ul>

<p>Es haben nur solche Personen Zugang zu deinen personenbezogenen Daten, die Kenntnis von diesen Informationen haben müssen, um Ihre beruflichen Aufgaben zu erfüllen.</p>

<p><b>Internationale Übermittlungen</b>: Die personenbezogenen Daten, die wir von dir erheben und empfangen, können an Empfänger übermittelt und von diesen verarbeitet werden, die sich innerhalb oder außerhalb des Europäischen Wirtschaftsraums („<b>EWR</b>“) befinden. Einige der Empfänger außerhalb des EWR sind nach dem EU-US Privacy Shield zertifiziert und andere befinden sich in Ländern mit bestehenden Angemessenheitsentscheidungen. In jedem Fall wird bei der Übermittlung ein aus europäischer Datenschutzperspektive anerkanntes und angemessenes Datenschutzniveau gewährleistet. Andere Empfänger können sich in Ländern befinden, die kein angemessenes Datenschutzniveau aus europäischer Datenschutzperspektive gewährleisten. Wir werden alle erforderlichen Vorkehrungen treffen, um sicherzustellen, dass bei Übermittlungen in Länder außerhalb des EWR alle datenschutzrechtlich notwendigen Sicherheitsmaßnahmen getroffen werden. Hinsichtlich der Übermittlungen in Länder, die kein angemessenes Datenschutzniveau gewährleisten, werden wir geeignete Sicherheitsvorkehrungen bei der Übermittlung treffen, wie zum Beispiel Standard-Datenschutzklauseln der Europäischen Kommission oder einer Überwachungsbehörde, zugelassene Verhaltenskodizes zusammen mit verbindlichen und durchsetzbaren Verpflichtungen des Empfängers oder zugelassene Zertifizierungsmechanismen zusammen mit verbindlichen und durchsetzbaren Verpflichtungen des Empfängers. Du kannst uns wie unter Abschnitt 7 unten angegeben kontaktieren und eine Kopie solcher angemessenen Sicherheitsvorkehrungen verlangen.</p>

<p><b>4.Aufbewahrungsfrist - Wie lange bewahren wir deine personenbezogenen Daten auf?</b><br />
Deine personenbezogenen Daten werden so lange aufbewahrt, wie es erforderlich ist, um dir die gewünschten Dienstleistungen und Produkte bereitzustellen. Sobald deine Vertragsbeziehung mit uns beendet ist (bspw. deine Kontaktanfrage beantwortet wurde, das Gewinnspiel an dem du teilgenommen hast beendet ist) oder du auf andere Weise deine Beziehung mit uns beendest, werden wir deine personenbezogenen Daten aus unseren Systemen und Aufzeichnungen entfernen und/oder diese ordnungsgemäß anonymisieren, so dass du nicht mehr anhand dieser identifiziert werden kannst (es sei denn, wir müssen deine Daten auch weiterhin behalten, um gesetzliche oder behördliche Vorschriften einzuhalten, denen das Kao Unternehmen unterliegt --z.B. aus steuerlichen Gründen).<br />
Wir bewahren deine Kontaktdaten und Ihr manifestiertes Interesse an unseren Produkten und Dienstleistungen unter Umständen für längere Zeit auf, wenn du eingewilligt hast, dass dir das Kao Unternehmen Marketingmaterialien zusenden darf. Wir könnten auch nach anwendbarem Recht dazu verpflichtet sein, bestimmte personenbezogene Daten für eine Dauer von 10 Jahren nach dem entsprechenden Steuerjahr aufzubewahren. Wir können deine personenbezogenen Daten nach Beendigung des Vertragsverhältnisses aufbewahren, wenn deine personenbezogenen Daten für die Einhaltung anderer anwendbarer Gesetze notwendig sind oder wenn wir deine personenbezogenen Daten nur auf einer Need-To-Know-Basis benötigen, um einen Rechtsanspruch zu begründen, auszuüben oder zu verteidigen. Soweit möglich, werden wir die Verarbeitung deiner personenbezogenen Daten zu solch beschränkten Zwecken nach Beendigung des Vertragsverhältnisses einschränken.

</p>


<p><b>5.	Deine Rechte - <i>Welche Rechte hast du und wie kannst du deine Rechte geltend machen?</i></b><br />
<b>Recht auf Widerruf der Einwilligung</b>: Wenn du deine Einwilligung zur Erhebung, Verarbeitung und Verwendung deiner personenbezogenen Daten erteilt hast (insbesondere zum Erhalt von Direktmarketingmitteilungen per E-Mail, SMS/WhatsApp und Telefon), kannst Du diese Einwilligung jederzeit mit Wirkung für die Zukunft widerrufen. Ein solcher Widerruf hat keinen Einfluss auf die Rechtmäßigkeit der Verarbeitung vor dem Widerruf der Einwilligung. Bitte kontaktiere uns gemäß Abschnitt 7 unten, wenn du deine Einwilligung widerrufen möchtest.</p>

<p><b>Zusätzliche Datenschutzrechte</b>: Nach anwendbarem Datenschutzrecht hast Du unter Umständen das Recht auf: (i) Auskunft über deine personenbezogenen Daten; (ii) Berichtigung deiner personenbezogenen Daten; (iii) Löschung deiner personenbezogenen Daten; (iv) Einschränkung der Verarbeitung deiner personenbezogenen Daten; (v) Übertragbarkeit der Daten und/oder (vi) Widerspruch gegen die Verarbeitung deiner personenbezogenen Daten (einschließlich des Widerspruchs gegen eine Profilerstellung).  </p>

<p>Bitte beachte, dass diese vorgenannten Rechte im Rahmen des anwendbaren lokalen Datenschutzgesetzes eingeschränkt sein könnten. Nachstehend findest du nähere Informationen zu deinen Rechten im Rahmen der DSGVO:</p>

                        <ul>
<li><p><b>Recht auf Auskunft über deine personenbezogenen Daten</b>: Dir steht unter Umständen das Recht zu, von uns eine Bestätigung darüber zu erhalten, ob deine personenbezogenen Daten verarbeitet werden, und in einem solchen Fall Auskunft über deine personenbezogenen Daten zu verlangen. Zu deinem Recht auf Auskunft gehören unter anderem die Verarbeitungszwecke, die Kategorien betroffener personenbezogener Daten und die Empfänger oder Kategorien von Empfängern, denen die personenbezogenen Daten offengelegt wurden oder noch offengelegt werden. Dies ist jedoch kein absolutes Recht, und die Interessen anderer Personen könnten ggf. dein Auskunftsrecht einschränken.<br />
Dir könnte unter Umständen das Recht zustehen, eine kostenlose Kopie der verarbeiteten personenbezogenen Daten zu erhalten. Für weitere von dir angeforderte Kopien können wir unter Umständen eine angemessene Gebühr berechnen, die auf den Verwaltungskosten basiert.
</p></li>

<li><p><b>Recht auf Berichtigung</b>: Dir könnte unter Umständen das Recht zustehen, von uns die Berichtigung unrichtiger personenbezogener Daten über dich zu verlangen. Abhängig von den Verarbeitungszwecken könnte dir das Recht zustehen, unvollständige personenbezogene Daten zu vervollständigen, einschließlich durch das Bereitstellen einer ergänzenden Erklärung.</p></li>

<li><p><b>Recht auf Löschung (Recht auf Vergessen werden)</b>: Unter bestimmten Umständen könnte dir das Recht zustehen, von uns die Löschung der dir betreffenden personenbezogene Daten zu verlangen, und wir könnten verpflichtet sein, solche personenbezogenen Daten zu löschen.</p></li>

<li><p><b>Recht auf Einschränkung der Verarbeitung</b>: Unter Umständen steht dir das Recht zu, von uns eine Einschränkung der Verarbeitung deiner personenbezogenen Daten zu verlangen. In diesem Fall werden die betreffenden Daten gekennzeichnet und dürfen von uns nur zu bestimmten Zwecken verarbeitet werden.</p></li>

<li><p><b>Recht auf Übertragbarkeit der Daten</b>: Unter Umständen könnte dir das Recht zustehen, die von dir zur Verfügung gestellten personenbezogene Daten in einem strukturierten, üblichen und maschinenlesbaren Format zu erhalten und diese Daten von unserer Seite ungehindert an ein drittes Unternehmen zu übermitteln.</p></li>
                            </ul>
                        <div style="max-width: 800px; background-color: #CCC; margin: auto">
                            <p><b>Widerspruchsrecht : </b><br />

                            Unter bestimmten Umständen steht dir das Recht zu, aus Gründen, die sich aus deiner besonderen Situation ergeben, jederzeit der Verarbeitung deiner personenbezogenen Daten zu widersprechen, und wir sind verpflichtet, die Verarbeitung deiner personenbezogenen Daten einzustellen. Ein solches Widerspruchsrecht kann vor allem dann bestehen, wenn wir deine personenbezogenen Daten für Direktmarketingzwecke erheben und verarbeiten oder dazu, ein Nutzerprofil zu erstellen, um somit einen besseren Überblick über deine Interessen an unseren Produkten und Dienstleistungen zu erhalten.</p>

                            <p>Wenn du ein Widerspruchsrecht hast und dieses Recht ausübst, werden deine personenbezogenen Daten von uns nicht mehr für solche Zwecke verarbeitet. Du kannst von diesem Recht Gebrauch machen, indem du uns wie in Abschnitt 7 beschrieben kontaktierst.</p>

                            <p>Ein solches Widerspruchsrecht kann insbesondere dann bestehen, wenn die Verarbeitung deiner personenbezogenen Daten notwendig ist, um während der Vertragsverhandlung erforderliche Schritte zu unternehmen oder um den Vertrag mit dir zu erfüllen.</p>

                            <p>Wenn du nicht länger Direktmarketingmaterial über E-Mail, SMS/MMS, Fax und Telefon erhalten möchtest, musst du deine Einwilligung wie oben beschrieben widerrufen (Kontakt wie in Abschnitt 7 beschrieben).</p>
                      
                         </div>

                        <p>Für die Ausübung Ihrer Rechte kontaktieren Sie uns bitte wie im Abschnitt 7 beschrieben. Sie haben auch das Recht, eine Beschwerde bei der zuständigen Datenschutzaufsichtsbehörde einzureichen.</p>

<p><b>6.  Cookies und andere Tracking-Technologien</b><br />
Wir geben dir diese Informationen als Teil unseres Bestrebens, neuen Gesetzgebungen zu entsprechen, und um sicherzustellen, dass wir ehrlich und klar über deine Privatsphäre bei dem Besuch unserer Website kommunizieren. Wir wissen, dass du nichts anderes von uns erwarten würdest, und du kannst sicher sein, dass wir an einer Reihe anderer Privatsphäre und Cookie-bezogener Verbesserungen an der Webseite arbeiten.</p> 

                        <p>Hier ist eine Liste der wichtigsten von uns verwendeten Cookies und wofür wir sie verwenden: </p>

                        <ul>
                            <li>
                                	<p>Google Analytics Cookie (_utma, _utmb, _utmc, _utmz) </p>
                            </li>
                            <li><p>Zweck: Google Analytics hilft uns dabei, zu sehen, wie Besucher auf unsere Website gelangen und wie sie sich dort bewegen. Das hilft uns dabei, unsere Website konstant zu verbessern.</p></li>

                        </ul>

                        <p>Die gesammelten Daten sind</p>
                        <ul>
                            <li><p>anonym (welche Werbeanzeigen du gesehen haben könntest, welchen Web-Browser du nutzt, Datum/Uhrzeit deines Besuchs auf der Website, demographische Informationen und welche anderen Seiten du dir angesehen hast) oder </p></li>
                            <li><p>pseudonym (deine IP Addresse). </p></li>
                        </ul>
                        <p>Aggregierte und anonymisierte Daten können mit dritten Parteien in Form von vertikaler Leistungseinsicht und Benchmarking gemeinsam genutzt werden. </p>

                        <p><b>Facebook (Facebook Connect, Facebook Costum Audience)</b><br />
Zweck: Wir verwenden Plugins des sozialen Netzwerkes facebook.com, welches von der Facebook Inc., 1601 S. California Ave, Palo Alto, CA 94304, USA betrieben wird („Facebook“). Den Link zur Datenschutzerklärung von Facebook findest du hier: Datenschutzhinweise von Facebook. </p>

<p><b>DoubleClick (Id,)</b><br />
Zweck: Dieses Cookie sorgt dafür, dass du Angebote/Werbeanzeigen von Kao auf sorgfältig ausgewählten Seiten Dritter sehen kannst, nachdem due unsere Website verlassen hast. Für weitere Informationen, oder falls du dieses Cookie deaktivieren möchtest, ohne andere optionale Cookies zu entfernen, besuche bitte diesen Link: https://www.google.com/settings/ads/onweb#display_optout </p>

<p><b>CookieAcceptance (CookieAcceptance)</b><br />
Dieses Cookie zeichnet auf, wenn du die Verwendung von Cookies auf dieser Website akzeptiert hast. Es enthält/speichert keine weiteren Informationen. Dieses Cookie verbleibt auf deinem Computer, nachdem du die Website verlassen habst. </p>

<p><b>“Share” Tools</b><br />
 Wenn du die Möglichkeit nutzt Kao Inhalte über Soziale Medien – wie Facebook und Twitter – zu “teilen”, können dir Cookies dieser Seiten zugesendet werden. Wir haben keine Kontrolle über die Einstellung dieser Cookies, und empfehlen dir daher, die Websites von Drittanbietern zu überprüfen, um weitere Informationen über deren Cookies und darüber, wie diese zu verwalten sind, zu erhalten. </p>

<p><b>Instrumente zum Teilen von Inhalten</b><br />
 Wenn du die Möglichkeit in Anspruch nimmst, Inhalte von Kao mit deinen Freunden über soziale Medien wie z.B. Facebook oder Twitter zu teilen, kann es sein, dass diese Websites dir Cookies schicken. Wir kontrollieren die Cookie Einstellungen dieser Seiten nicht, deshalb empfehlen wir dir dich über den Einsatz von und den Umgang mit Cookies dieser Webseiten zu informieren. </p>

<p><b>Verwalten von Cookies</b><br />
 Wenn Cookies auf deinem Computer nicht aktiviert sind, wird das dazu führen, dass ein Teil der Website nicht korrekt angezeigt wird oder funktioniert. Du wirst jedoch noch in der Lage sein, einige Inhalte auf unserer Website zu sehen.</p> 

<p><b>Weitere Informationen über Cookies</b><br />
 Wenn du mehr über Cookies im Allgemeinen und darüber, wie man sie verwaltet, erfahren möchtest, besuche www.aboutcookies.org (öffnet in neuem Fenster – bitte beachte, dass wir für die Inhalte externer Websites nicht verantwortlich sind). </p>


<p><b>7. Fragen und Kontaktinformationen</b><br />
Solltest du Fragen zu dieser Datenschutzrichtlinie haben oder deine Rechte gemäß Abschnitt 5 oben ausüben wollen, so kontaktiere uns bitte unter:  </p> 

<p>Guhl Ikebana GmbH, a Kao Group Company <br />
Pfungstädter Straße 98<br />
64297 Darmstadt<br />
Aus Deutschland: 0800 7307310<br />
Aus der Schweiz: 0800 221770<br />
Aus Österreich: 0800 890760</p>

<p>Du kannst zudem über die globale Webseite mit uns Kontakt aufnehmen: www.kao.com/global/en/EU-Data-Subject-Request</p>

<p>>Der Datenschutzbeauftragte des Kao Unternehmens für Deutschland ist erreichbar unter: Steffen Tracz, Christian Adolphy Consulting, Neuberg 4a, 65193 Wiesbaden, Germany, steffen.tracz@ca-c.de</p>

<p><b>8.    Änderungen an der Datenschutzerklärung </b><br />
Wir können diese Datenschutzrichtlinie von Zeit zu Zeit ändern, um auf Änderungen von gesetzlichen, behördlichen oder betriebsbedingten Anforderungen zu reagieren. Wir werden dich über solche Änderungen in Kenntnis setzen, einschließlich darüber, wann sie in Kraft treten werden, indem wir das obige „letzte Überarbeitungsdatum” aktualisieren oder wie es anderweitig gesetzlich vorgeschrieben ist. Deine weitere Benutzung der Webseite nach Inkrafttreten solcher Aktualisierungen kommt einer Annahme dieser Änderungen gleich. Solltest du keine Aktualisierungen dieser Datenschutzrichtlinie annehmen, so solltest du die Benutzung der Webseite unterlassen.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
