﻿<%@ Page Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs"
    Inherits="Biore2012.FormConfig.ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%#SquishIt.Framework.Bundle .Css()
        .Add("~/css/formConfig.css")
        .Render("~/css/combinedform_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow">
            </div>
            <div class="centeringDiv">
                <div class="responsive contact">
                    <div id="contactFormWrap">
                        <div id="BrandImageContainer" class="png-fix">
                            <img class="png-fix" src="images/forms/contactUsPhoto.jpg" style="border-width: 0px;" />
                        </div>
                        <div id="ContactFormContainer">
                            <asp:Panel ID="ContactDetails" runat="server">
                                <!-- Header -->
                                <div id="formHeader">
                                    <h1 id="ctl00_ContentPlaceHolder1_ucForm_PageHeader">Contact Us
                                    </h1>
                                    <div id="responseRule" style="width: 400px; margin: 5px 5px 45px;"></div>
                                </div>
                                <!-- Description -->
                                <div class="DescriptionContainer png-fix">
                                    <h2>By Mail</h2>
                                    <p>
                                        Kao (UK) Limited<br />
                                        130 Shaftesbury Avenue<br />
                                        London W1D 5EU<br />
                                    </p>
                                    <h2>By Phone</h2>
                                    <p>
                                        <span>Phone: 0800 1070 853</span>.<br />
                                        Fax: 020 7851 9888.
                                    </p>
                                    <h2>Online</h2>
                                    <p>
                                        Fill out the form below.
                                    </p>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="ContactForm" runat="server">
                                <!-- From Fields -->
                                <div class="FormContainer">
                                    <p class="req png-fix">
                                        <em>Required*</em>
                                    </p>
                                    <div class="MemberInfoContainer png-fix">
                                        <div class="NameWrapper">
                                            <div class="FirstNameContainer Question">
                                                <asp:Label ID="FNameLbl" runat="server" Text="Vorname*" AssociatedControlID="FName"></asp:Label>
                                                <asp:TextBox ID="FName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>

                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Bitte tragen Sie Ihren Vornamen ein."
                                                        ControlToValidate="FName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your First Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="FName" EnableClientScript="true"
                                                        SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="LastNameContainer Question">
                                                <asp:Label ID="LNameLbl" runat="server" Text="Nachname*" AssociatedControlID="LName"></asp:Label>
                                                <asp:TextBox ID="LName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>

                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Bitte tragen Sie Ihren Nachname ein."
                                                        ControlToValidate="LName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Last Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="LName" SetFocusOnError="true"
                                                        Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="EmailWrapper">
                                            <div class="EmailContainer Question">
                                                <asp:Label ID="EmailLbl" runat="server" Text="E-mail*" AssociatedControlID="Email"></asp:Label>
                                                <asp:TextBox ID="Email" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>

                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Bitte tragen Sie Ihre E-Mail Adresse ein."
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Please enter a valid Email Address."
                                                        ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="ConfirmEmailContainer Question">
                                                <asp:Label ID="RetypeEmailLbl" runat="server" Text="Wiederholung E-Mail*" AssociatedControlID="RetypeEmail"></asp:Label>
                                                <asp:TextBox ID="RetypeEmail" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>

                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Bitte überprüfen Sie Ihre E-Mail Adresse, so dass beide E-Mail Adressen übereinstimmen."
                                                        ControlToValidate="RetypeEmail" EnableClientScript="true" SetFocusOnError="true"
                                                        Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="CompareValidator1" Text="Make sure you retyped the Email correctly."
                                                        ControlToValidate="RetypeEmail" ControlToCompare="Email" EnableClientScript="true"
                                                        SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelAddress" class="AddressContainer">
                                            <div class="AddressWrapper">
                                                <div class="Address1Container Question">
                                                    <asp:Label ID="Address1Lbl" runat="server" Text="Straße/Hausnummer" AssociatedControlID="Address1"></asp:Label>
                                                    <asp:TextBox ID="Address1" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>


                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Bitte geben Sie Strasse und Hausnummer."
                                                            ControlToValidate="Address1" EnableClientScript="true" SetFocusOnError="true"
                                                            Display="Dynamic" CssClass="errormsg" Enabled="false" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Please do not use special characters in the Address 1 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address1" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="Address2Container Question">
                                                    <asp:Label ID="Address2Lbl" runat="server" Text="Straße/Hausnummer 2" AssociatedControlID="Address2"></asp:Label>
                                                    <asp:TextBox ID="Address2" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>


                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Please do not use special characters in the Address 2 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address2" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="Address3Container Question">
                                                    <asp:Label ID="Address3Lbl" runat="server" Text="Straße/Hausnummer 3" AssociatedControlID="Address3"></asp:Label>
                                                    <asp:TextBox ID="Address3" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>

                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="Please do not use special characters in the Address 3 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address3" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="CityContainer Question">
                                                    <asp:Label ID="CityLbl" runat="server" Text="Stadt" AssociatedControlID="City"></asp:Label>
                                                    <asp:TextBox ID="City" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>

                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Bitte geben Sie Ihre Stadt."
                                                            ControlToValidate="City" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" Enabled="false" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="Please do not use special characters in the City field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="City" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="PostalCodeContainer Question">
                                            <asp:Label ID="PostalCodeLbl" runat="server" Text="Postleitzahl" AssociatedControlID="PostalCode"></asp:Label>
                                            <asp:TextBox ID="PostalCode" runat="server" MaxLength="20" CssClass="inputTextBox"></asp:TextBox>

                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Bitte geben Sie Postleitzahl ein."
                                                    ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true"
                                                    Display="Dynamic" CssClass="errormsg" Enabled="false"></asp:RequiredFieldValidator>
                                                <%-- The following RegEx Validators is for US Zip/Postal codes.  IMPORTANT: 
								 Please test against your user base before using as you may need to modify it for other formats!  --%>
                                                <%--<asp:RegularExpressionValidator ID="PostalCodeValidator2" runat="server" 
								ErrorMessage="Please enter a valid 5-digit ZIP code." ValidationExpression="^\d{5}(-\d{4})$" 
								ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true" 
								Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>--%>
                                            </div>
                                        </div>
                                        <div class="CountryContainer Question">
                                            <asp:Label ID="CountryLbl" runat="server" Text="Land" AssociatedControlID="Country"></asp:Label>
                                            <asp:DropDownList ID="Country" runat="server">
                                                <asp:ListItem Value="1">Deutschland</asp:ListItem>
                                                <asp:ListItem Value="2">Österreich</asp:ListItem>
                                                <asp:ListItem Value="3">Schweiz</asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="CountryValidator" runat="server" ErrorMessage="Bitte wählen Sie Ihr Land."
                                                    ControlToValidate="Country" EnableClientScript="true" SetFocusOnError="true"
                                                    Display="Dynamic" CssClass="errormsg" Enabled="false"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelGender" class="GenderContainer Question">
                                            <asp:Label ID="GenderLbl" runat="server" Text="Geschlecht" AssociatedControlID="Gender"></asp:Label>
							                <asp:DropDownList ID="Gender" runat="server">
								                <asp:ListItem Value="">Auswählen</asp:ListItem>
								                <asp:ListItem Value="F">Frau</asp:ListItem>
								                <asp:ListItem Value="M">Herr</asp:ListItem>
							                </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="GenderValidator" runat="server" ErrorMessage="Please enter your Gender."
                                                    ControlToValidate="Gender" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="BirthdayContainer Question">
                                            <asp:Label ID="DOBLbl" runat="server" Text="Geburtsdatum" AssociatedControlID="yyyy"></asp:Label>
							                <asp:DropDownList ID="yyyy" runat="server" CssClass="inputSelectBDay"></asp:DropDownList>
							                <asp:DropDownList ID="mm" runat="server" CssClass="inputSelectBDay"></asp:DropDownList>
							                <asp:DropDownList ID="dd" runat="server" CssClass="inputSelectBDay"></asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:CustomValidator ID="DOBValidator" runat="server" ErrorMessage="Please enter a valid date."
                                                    OnServerValidate="DOBValidate" EnableClientScript="false" Display="Dynamic" CssClass="errormsg validdate"></asp:CustomValidator>
                                                <asp:RequiredFieldValidator ID="yyyyValidator" runat="server" ErrorMessage="Please enter your Birth Year."
                                                    ControlToValidate="yyyy" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg year"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="mmValidator" runat="server" ErrorMessage="Please enter your Birth Month."
                                                    ControlToValidate="mm" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg month"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="ddValidator" runat="server" ErrorMessage="Please enter your Birth Day."
                                                    ControlToValidate="dd" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg day"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="PhoneContainer Question">
                                            <%--<label for="Phone">Phone</label>--%>
                                            <asp:Label ID="PhoneLbl" runat="server" Text="Telefon" AssociatedControlID="Phone"></asp:Label>
							<asp:TextBox ID="Phone" MaxLength="50" runat="server" />
							
                                            <div class="ErrorContainer">
                                              <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ErrorMessage="Your Phone is not required, but please do not use special characters in the Phone field."
								ValidationExpression="^[^<>]+$" ControlToValidate="Phone" EnableClientScript="true"
								SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" ></asp:RegularExpressionValidator>
                                                <%-- The following RegEx Validators is for US type numbers.  IMPORTANT: 
								 Please test against your user base before using as you may need to modify it for other formats!  --%>
                                                <%--<asp:RegularExpressionValidator ID="PhoneValidator2" runat="server"
								ErrorMessage="<br/>Your Home Phone is not required, but please enter only valid characters."
								ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"
								ControlToValidate="Phone" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg phone"></asp:RegularExpressionValidator>--%>
                                            </div>
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelComment" class="CommentContainer Question">
                                           <asp:Label ID="QuestionCommentLbl" runat="server" Text="Fragen/Kommentare*" 
								AssociatedControlID="QuestionComment"></asp:Label>
							<asp:TextBox ID="QuestionComment" runat="server" TextMode="MultiLine" />
							
                                            <div class="ErrorContainer">
                                               <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
								ErrorMessage="Please complete: Question / Comment." ControlToValidate="QuestionComment" 
								EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg questioncomment" />
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                    </div>
                                    <div id="ctl00_ContentPlaceHolder1_ucForm_QuestionPanel" class="QuestionContainer">
                                    </div>
                                    <div id="submit-container" class="SubmitContainer png-fix">
                                        <asp:Button runat="server" CssClass="submit buttonLink png-fix" Text="SUBMIT ›" />
                                    </div>
                                    <asp:HiddenField ID="hfHookID" runat="server" />
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="ContactFormSuccess" runat="server">
                                 <div id="formHeader">
                                    <h1 id="ctl00_ContentPlaceHolder1_ucForm_PageHeader">Contact Us
                                    </h1>
                                    <div id="responseRule" style="width: 400px; margin: 5px 5px 45px;"></div>
                                </div>
                                <div id="contactSuccess">
                                  <h2>Danke schön!</h2>
					<p>Vielen Dank für Ihre E-Mail. Sie werden in Kürze eine Antwort von uns erhalten.</p>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="ContactFormFailure" runat="server">
                                 <div id="fromHeader">
                                    <h1 id="ctl00_ContentPlaceHolder1_ucForm_PageHeader">Contact Us
                                    </h1>
                                    <div id="responseRule" style="width: 400px; margin: 5px 5px 45px;"></div>
                                </div>
                                <div id="contactFailure">
                                   <h2>Wir bitten um Entschuldigung...</h2>
					<p>Es gab einen Fehler beim Versuch, zu verarbeiten. Bitte versuchen Sie es später erneut.</p>
                                    <asp:Literal ID="litError" runat="server"></asp:Literal>
                                </div>
                            </asp:Panel>
                            <div class="FormBottom png-fix">
                            </div>
                            <!-- Disclaimer -->
                            <div id="DisclaimerContainer" class="png-fix">
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">

                    </script>

                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script type="text/javascript">
        $(function () {
            if ($(".signUp").length) {
                toggle("noneToggle1");
                toggle("noneToggle2");
                toggle("noneToggle3");
                toggle("noneToggle4");
            }
        });
        function toggle(className) {
            var checkToggle = $("." + className + " input:last");
            checkToggle.click(function () {
                if ($("#" + checkToggle.attr('id') + ":checked").val()) {
                    $("." + className + " input").each(function () {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).attr("checked", false);
                            $(this).attr('disabled', 'disabled');
                        }
                    });
                } else {
                    $("." + className + "  input").each(function () {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).removeAttr('disabled');
                        }
                    });
                }
            });
        }
    </script>

</asp:Content>
