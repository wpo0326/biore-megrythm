﻿<%@ Page Title="About Bior&eacute;&reg; Skincare | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.about_us._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Learn more about Bioré® Skincare" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div class="centeringDiv">
                <div id="content">
                    <div class="titleh1">
                        <h1>About Bior&eacute;<sup>&reg;</sup> Skincare</h1>
                        <div id="responseRule"></div>
                    </div>
                    <div class="about-info">                        
                        <p>Bior&eacute;<sup>&reg;</sup> Skincare targets the root of all skin problems—the evil clogged pore. So when it comes to dirt and oil we take a no nonsense approach to fight smarter, not harder. Our scrub, liquid, powder, strip, mask and bar cleansing products go straight to the source of those pesky pores for deeply clean, beautiful skin that’s ready for anything.</p>
                    </div>
                    <img src="../images/about/aboutBG.png" alt="" id="Img1" />                    
                </div>
            </div>
        </div>
    </div>
</asp:Content>

