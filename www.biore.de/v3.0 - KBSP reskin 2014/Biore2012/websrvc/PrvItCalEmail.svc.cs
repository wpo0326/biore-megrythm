﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Collections.Generic;
using KAOForms;

namespace Biore2012.websrvc
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PrvItCalEmail
    {
        // Add [WebGet] attribute to use HTTP GET
        [OperationContract]
        [WebGet]
        public string Send(string email, string code)
        {
            string status = "error";
            if (!String.IsNullOrEmpty(email) && !String.IsNullOrEmpty(code))
            {

                String fromEmail = "";
                String fromName = "";

                EmailDAO eDAO = new EmailDAO();
                List<ExTargetAttribute> attList = new List<ExTargetAttribute>();

                //create firstname attribute
                ExTargetAttribute item1 = new ExTargetAttribute();
                item1.Name = "UniqueCodeStr";
                item1.Value = code;
                attList.Add(item1);

                try
                {
                    DefaultValues dVaules = DefaultFormDAO.GetDefaultValues();
                    string etClientID = dVaules.BrandSiteData.Biore.ETClientID;
                    bool sendStatusGood = eDAO.sendETEmail(fromEmail, fromName, email, email, "", attList, "BioreProveItCalendarEmailWithDataExt", etClientID);
                    if (sendStatusGood) status = "success";
                }
                catch (Exception ex)
                {
                    string error = ex.ToString();
                    throw;
                }
            }
            return status;
        }
    }
}
