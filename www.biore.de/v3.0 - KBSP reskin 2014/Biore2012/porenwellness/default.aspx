﻿<%@ Page Title="Legal | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master"
    Language="C#" AutoEventWireup="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
    <style type="text/css">
        #content p
        {
            text-align: left;
        }
        
        #topNav, .centeringDiv ul {
        display: none;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="about">
        <div id="main">
            <div id="mainContent">
                <div class="centeringDiv">
                    <div id="content">
                        <div class="titleh1">
                            <p><br/><br/>
                                <strong>Teilnahmebedingungen und Datenschutzhinweise für das Instagram-Gewinnspiel „Porenwellness“</strong><p>
                            <!--<div id="responseRule">
                            </div>-->
                        </div>
                        <p>Die nachfolgenden Bedingungen regeln die Teilnahme an dem Gewinnspiel „Porenwellness“ der Marke Bioré auf Instagram sowie die Erhebung und Nutzung der von den Teilnehmern erhobenen oder mitgeteilten Daten. Eine Teilnahme am Gewinnspiel ist ausschließlich zu den hier aufgeführten Teilnahmebedingungen möglich. Mit der Teilnahme am Gewinnspiel „Porenwellness“ akzeptiert der Teilnehmer diese Teilnahmebedingungen:</p>
                 
                <p><strong>Veranstalter und Realisator</strong></p>

                  <p>1. Das Gewinnspiel wird von der GUHL Ikebana GmbH, a Kao Group Company, Pfungstädter Str. 98, 64297 Darmstadt, Telefon + 49 (0)800-7307310 (DE), +43 (0)800-221770 (AT), E-Mail: Produktberatung@Biore.com, veranstaltet.  </p>

                <p>2. Der technische Realisator des Gewinnspiels ist Häberlein & Mauerer AG, Franz-Joseph-Straße 1, 80801 München – Germany, Telefon: 089-38108160, E-Mail: bioresocial@haebmau.de.</p>

                <p>3.  Sämtliche Fragen, Kommentare oder Beschwerden zum Gewinnspiel sind an den Veranstalter zu richten.</p>

                <p><strong>Teilnahmebedingungen des Gewinnspiels</strong></p>
                 	
               <p>1.	Die Aktion findet vom 01.03.2017 bis 30.06.2017 statt. Einsendeschluss ist der 30.06.2017 um 23.59 Uhr. Es gilt das Datum und die Uhrzeit des Eintrags in der Datenbank.
<p>2.	Teilnahmeberechtigt sind Personen, die Ihren Wohnsitz in Deutschland oder Österreich haben, das 18. Lebensjahr vollendet haben und dem Instagram Account @biore_de folgen. Mitarbeiter der Guhl Ikebana GmbH sowie deren Angehörige i.S.v. §15 AO, Lebenspartner und Mitarbeiter von Kooperationspartnern, die mit der Erstellung oder Abwicklung des Gewinnspiels beschäftigt sind oder waren, sind von der Teilnahme ausgeschlossen.</p>
<p>3.	Teilnehmer der Aktion ist diejenige Person, die die Teilnahme-Bedingungen erfüllt. Zudem müssen die notwendigen Einverständniserklärungen zur Datenspeicherung erteilt werden.</p>
<p>4.	Der Teilnahmebeitrag besteht in dem Posten/Teilen eines Selfies zum Thema Bioré-Porenpflege/ Porenwellness auf Instagram unter der korrekten Markierung mit den beiden Hashtags #freeyourpores #porenwellness. Voraussetzung ist, dass der Teilnehmer dem Bioré Instagram-Account @biore_de folgt und der Instagram Account des Teilnehmers öffentlich sichtbar gemacht ist. Das Gewinnspiel steht in keiner Verbindung zu Instagram oder Facebook und wird in keiner Weise von Instagram oder Facebook gesponsert, unterstützt oder organisiert. </p>
<p>5.	Mit dem Upload/Post seines Fotos/Selfies stimmt der Teilnehmer zu, dass dieses im Zusammenhang mit dem Gewinnspiel und der Marke Bioré auf Instagram und Facebook veröffentlicht werden darf. Der Teilnehmer versichert das Einverständnis aller Personen, die auf dem hochgeladenen Foto abgebildet sind, sowie alle damit verbunden Rechte im Vorfeld eingeholt zu haben. Dies gilt auch auf Fotos die unter Verwendung der Regram/Repost-Funktion für das Gewinnspiel genutzt werden. Beiträge zur Teilnahme am Gewinnspiel dürfen zudem keine Beleidigungen, falsche Tatsachen, Wettbewerbs-, Marken- oder Urheberrechtsverstöße enthalten. Der Teilnehmer kann der Veröffentlichung seines Fotos jederzeit schriftlich unter Produktberatung@biore.com widersprechen.</p>
<p>6.	Unter allen Teilnehmern werden folgende Preise per Zufall verlost:</p>
<div style="margin-left:30px"><p>-	1 exklusives Wellness-Wochenende für 2 Personen inkl. je einer professionellen Gesichtsbehandlung im Wert von mindestens 500 Euro </p>
<p>-	sowie 50 Bioré Produktsets bestehend aus jeweils 1 Porentiefen Waschgel mit Aktivkohle, einer Packung 1-Minute Thermomaske mit Aktivkohle, einer Packung tiefenreinigende Clear-Up Strips mit Aktivkohle, 1 Porenreinigendem Waschgel mit Backpulver und einem Puderpeeling mit Backpulver.</p> </div>
<p>Der im Gewinnspiel angegebene Wert der Reise bezieht sich auf die teuerste Reisezeit und unterliegt somit, je nach Zeitpunkt der Zurverfügungstellung und Durchführung der Reise, saisonal bedingten Abweichungen. Ein Ausgleich einer möglichen Wertdifferenz ist ausgeschlossen. Für An-und Abreise werden 2 Tickets für die Deutsche Bahn (2. Klasse) vom Veranstalter gestellt. Aufgrund von unvorhersehbaren Ereignissen, höherer Gewalt oder Streik behält sich der Veranstalter vor, das Transportmittel auch kurzfristig zu ändern bzw. die Reise abzusagen oder umzubuchen. Zusätzliche Kosten, welche auf der Reise entstehen (Minibar, Telefon etc.) werden vom Veranstalter nicht übernommen.</p>
<p>7.	Die Teilnahme an dem Gewinnspiel erfolgt unentgeltlich. Jeder Teilnehmer darf nur einmal teilnehmen. Mehrfach-Teilnahmen werden nicht berücksichtigt. Die Gewinnspielteilnahme ist unabhängig vom Kauf einer Ware. Der Erwerb einer Ware oder die Inanspruchnahme einer Dienstleistung erhöhen die Gewinnchancen nicht.</p> 
<p>8.	Bei einem Verstoß gegen diese Teilnahmebedingungen behält sich die Guhl Ikebana GmbH das Recht vor, Personen vom Gewinnspiel auszuschließen.</p>

<p><strong>Gewinnbenachrichtigung, keine Barauszahlungen</strong></p>
<p>1.	Die Teilnehmer werden per Direktnachricht an ihre Instagram Accounts kontaktiert und um Bestätigung gebeten. Ebenfalls werden ihnen dort die notwendigen, folgenden Schritte zur weiteren nicht-öffentlichen Kommunikation / Kontaktaufnahme mitgeteilt. Jegliche weitere Kommunikation zwischen dem Veranstalter/Realisator und dem Gewinner im Anschluss findet in nicht-öffentlichen, individuellen Nachrichten statt. </p>
<p>2.	Die Gewinner müssen sich innerhalb von 7 Werktagen melden und ihre vollständigen Namen, Adressdaten und Geburtsdatum mitteilen. Bestätigt ein Teilnehmer die Annahme des Gewinns nicht innerhalb einer Frist von 7 Tagen verfällt der Gewinn.</p>
<p>3.	Das Produktpaket wird an die vom Teilnehmer übermittelte postalische Anschrift verschickt. Ist eine Zustellung nicht möglich oder sollten die angegebenen Kontaktmöglichkeiten fehlerhaft sein (z.B. ungültige Emailadressen oder postalische Anschriften), wird das Produktpaket an den Veranstalter zurückgeschickt und der Gewinn verfällt. Der Veranstalter ist nicht verpflichtet richtige Adressen auszuforschen. Die Nachteile die sich aus der Angabe fehlerhafter Kontaktdaten ergeben, gehen zu Lasten der Teilnehmer.</p>
<p>4.	Eine Auszahlung des Gewinns in bar, in Sachwerten, dessen Tausch oder Übertragung auf andere Personen ist nicht möglich. Ein Teilnehmer kann auf den Gewinn verzichten. In diesem Fall rückt an seine Stelle der nächste Teilnehmer in der Gewinnerrangfolge nach. </p>

<p><strong>Gewährleistungsausschluss</strong></p>
<p>1.	Der Veranstalter behält sich das Recht vor, das Gewinnspiel zu jedem Zeitpunkt ohne Vorankündigung aus wichtigem Grund abzubrechen. Im Falle eines Abbruchs der Aktion aus wichtigem Grund wird der Veranstalter die Teilnehmer unverzüglich hierüber informieren. Ein Abbruch aus wichtigem Grund kann insbesondere erfolgen, wenn aus technischen Gründen eine ordnungsgemäße Durchführung der Aktion nicht mehr gewährleistet werden kann. Der Veranstalter übernimmt keine Gewähr für entgangene Teilnahmechancen durch technisch bedingte Verbindungsstörungen o.ä.</p>
<p>2.	Die Verfügbarkeit und Funktion des Gewinnspiels auf Instagram kann vom Veranstalter nicht gewährleistet werden kann. Das Gewinnspiel kann aufgrund von äußeren Umständen und Zwängen beendet oder entfernt werden, ohne dass hieraus Ansprüche der Teilnehmer gegenüber dem Veranstalter entstehen.</p>
<p>3.	Der Veranstalter haftet ferner nicht für verloren gegangene, verzögerte, fehlgeleitete, beschädigte oder nicht zugestellte Beiträge/Inhalte, aufgrund von die elektronische Kommunikation störenden technischen Schwierigkeiten oder sonstigen Ursachen, wie sie bei Instagram oder einem Internet Provider auftreten können.</p>

<p><strong>Rechte</strong></p>
<p>1.	Mit dem Absenden oder Hochladen von Inhalten, insbesondere Bildern oder Beiträgen im Rahmen der Teilnahme am Gewinnspiel, erklären die Teilnehmer, dass die Inhalte frei von Rechten Dritter sind, also entweder von ihnen stammen oder sie die Einwilligung der Urheber sowie der auf Bildern und Videos abgebildeten Personen eingeholt haben. </p>
<p>2.	Der Veranstalter ist berechtigt hochgeladene/geteilte Inhalte abzulehnen, wenn die Inhalte nach seiner sachgerechten Einschätzung rechtswidrig sind oder gegen die guten Sitten verstoßen.</p> 
<p>3.	Die Teilnehmer stellen den Veranstalter von allen Ansprüchen Dritter frei, die aufgrund der den Teilnehmern bereitgestellten Inhalte entstanden sind. Sie erklären sich bereit, den Veranstalter in jeder zumutbaren Form bei der Abwehr dieser Ansprüche zu unterstützen.</p>
<p>4.	Mit dem Einsenden seines Bildes tritt der Teilnehmer zudem seine Rechte an diesem Bild an die Guhl Ikebana GmbH ab. Diese nimmt die Übertragung der Rechte an.</p>

<p><strong>Datenschutz</strong></p>
<p>1.	Der Veranstalter erhebt und nutzt die Daten der Teilnehmer nur soweit dies gesetzlich erlaubt ist oder die Teilnehmer dazu einwilligen. Die bereitgestellten Informationen werden – sofern nicht ausdrücklich anders ausgewiesen – nur für die Durchführung des Gewinnspiels und den Versand des Produktpaketes, vom Veranstalter und Realisator verwendet. Eine darüberhinausgehende Weitergabe der Teilnehmerdaten an Dritte erfolgt nur, soweit dies für die Durchführung des Gewinnspiels oder die Übermittlung der Gewinne erforderlich ist, zum Beispiel im Rahmen der Versendung der Gewinne.</p>
<p>2.	Die Teilnehmer willigen durch ihre Teilnahme am Gewinnspiel zur Erhebung, Speicherung und Nutzung folgender Daten ein, die der Gestaltung, Durchführung und Abwicklung des Gewinnspiels dienen: Name, Adresse, Geburtsdatum, E-Mail, Instagram User Name. Die persönlichen Daten der Teilnehmer werden im Anschluss an die Beendigung des Gewinnspiels und deren Abwicklung gelöscht. </p>
<p>3.	Auf schriftliche oder in Textform mitgeteilte Anforderung hin, können die Teilnehmer vom Veranstalter jederzeit Auskunft darüber verlangen, welche personenbezogenen Daten bei dem Veranstalter gespeichert sind, können deren Berichtigung sowie Löschung verlangen und Datennutzungseinwilligungen widerrufen.</p>

<p><strong>Haftungsausschluss</strong></p>
<p>1.	Für eine Haftung des Veranstalters auf Schadensersatz gelten unbeschadet der sonstigen gesetzlichen Anspruchsvoraussetzungen folgende Haftungsausschlüsse und Haftungsbegrenzungen: </p>
<p>2.	Der Veranstalter haftet unbeschränkt, soweit die Schadensursache auf Vorsatz oder grober Fahrlässigkeit beruht. Ferner haftet der Veranstalter für die leicht fahrlässige Verletzung von wesentlichen Pflichten, deren Verletzung die Erreichung des Vertragszwecks gefährdet, oder für die Verletzung von Pflichten, deren Erfüllung die ordnungsgemäße Durchführung des Gewinnspiels überhaupt erst ermöglichen und auf deren Einhaltung die Vertragspartner regelmäßig vertrauen. In diesem Fall haftet der Veranstalter jedoch nur für den vorhersehbaren, vertragstypischen Schaden. Der Veranstalter haftet nicht für die leicht fahrlässige Verletzung anderer als der in den vorstehenden Sätzen genannten Pflichten.</p> 
<p>3.	Die vorstehenden Haftungsbeschränkungen gelten nicht bei Verletzung von Leben, Körper und Gesundheit, für einen Mangel nach Übernahme von Beschaffenheitsgarantien für die Beschaffenheit eines Produktes und bei arglistig verschwiegenen Mängeln. Die Haftung nach dem Produkthaftungsgesetz bleibt unberührt. </p>
<p>4.	Soweit die Haftung des Veranstalters ausgeschlossen oder beschränkt ist, gilt dies auch für die persönliche Haftung von Arbeitnehmern, Vertretern und Erfüllungsgehilfen des Veranstalters.</p>

<p><strong>Sonstiges</strong></p>
<p>1.	Der Rechtsweg ist ausgeschlossen. Es ist ausschließlich das Recht der Bundesrepublik Deutschland anwendbar. Sollten einzelne dieser Teilnahmebedingungen ungültig sein oder werden, bleibt die Gültigkeit der übrigen Teilnahmebedingungen hiervon unberührt. Diese Teilnahmebedingungen können jederzeit von Guhl Ikebana geändert werden.</p>
<p>2.	Aufgrund der besseren Lesbarkeit wird in den Teilnahmebedingungen nur die männliche Form verwendet. Die weibliche Form ist selbstverständlich immer mit eingeschlossen.</p>


                 </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
