﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Biore2012.facebook.permalinks.self_heating_one_minute_mask
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Status = "301 Moved Permanently";
            Response.AddHeader("Location", VirtualPathUtility.ToAbsolute("~/dont-be-dirty/self-heating-one-minute-mask"));
        }
    }
}
