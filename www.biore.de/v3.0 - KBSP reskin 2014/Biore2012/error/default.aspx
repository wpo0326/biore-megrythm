﻿<%@ Page Title="Error | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.error._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Bioré® Skincare – Error" name="description" />

    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <div id="content">
                    <h1>Seite konnte nicht gefunden werden</h1>
                    <p>Es tut uns leid. Die von dir gewünschte Seite konnte nicht gefunden werden oder es ist ein Fehler aufgetreten. </p>
                    <div class="promoButton"><a href="<%= System.Configuration.ConfigurationManager.AppSettings["path"] %>">Home <span class="arrow">&rsaquo;</span></a></div>
                 </div>
                 <div id="photo">
                    <img src="<%= System.Configuration.ConfigurationManager.AppSettings["path"] %>images/utilityPages/polaroid.png" alt="" />
                 </div>
                 <div class="clear"></div>
            </div>
        </div>
    </div>
</asp:Content>