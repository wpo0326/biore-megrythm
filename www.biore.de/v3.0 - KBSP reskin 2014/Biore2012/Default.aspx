﻿<%@ Page Title="Bioré – der Experte für freie Poren. | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Free your pores! Erlebe mit Bioré die tiefenreinigende Kraft von Backpulver und Aktivkohle. Für reine Haut und feine Poren." name="description" />
    <meta content="skincare, clean face" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/UniversPro67.css")
        .Add("~/css/homepage.css")
        .Add("~/css/flexslider.css")
        .Render("~/CSS/combinedhome_#.css")
    %>
    <meta name="viewport" content="width=device-width">

    <style type="text/css">
        /**
       * Fade-move animation for second dialog
       */

        /* at start */
        .zoom-anim-dialog {
            opacity: 0;
            -webkit-transition: all 0.2s ease-out;
            -moz-transition: all 0.2s ease-out;
            -o-transition: all 0.2s ease-out;
            transition: all 0.2s ease-out;
            -webkit-transform: translateY(-20px) perspective( 600px ) rotateX( 10deg );
            -moz-transform: translateY(-20px) perspective( 600px ) rotateX( 10deg );
            -ms-transform: translateY(-20px) perspective( 600px ) rotateX( 10deg );
            -o-transform: translateY(-20px) perspective( 600px ) rotateX( 10deg );
            transform: translateY(-20px) perspective( 600px ) rotateX( 10deg );
        }

        /* animate in */
        .mfp-ready .zoom-anim-dialog {
            opacity: 1;
            -webkit-transform: translateY(0) perspective( 600px ) rotateX( 0 );
            -moz-transform: translateY(0) perspective( 600px ) rotateX( 0 );
            -ms-transform: translateY(0) perspective( 600px ) rotateX( 0 );
            -o-transform: translateY(0) perspective( 600px ) rotateX( 0 );
            transform: translateY(0) perspective( 600px ) rotateX( 0 );
        }

        /* animate out */
        .mfp-removing .zoom-anim-dialog {
            opacity: 0;
            -webkit-transform: translateY(-10px) perspective( 600px ) rotateX( 10deg );
            -moz-transform: translateY(-10px) perspective( 600px ) rotateX( 10deg );
            -ms-transform: translateY(-10px) perspective( 600px ) rotateX( 10deg );
            -o-transform: translateY(-10px) perspective( 600px ) rotateX( 10deg );
            transform: translateY(-10px) perspective( 600px ) rotateX( 10deg );
        }

        /* Dark overlay, start state */
        .mfp-bg {
            opacity: 0;
            -webkit-transition: opacity 0.3s ease-out;
            -moz-transition: opacity 0.3s ease-out;
            -o-transition: opacity 0.3s ease-out;
            transition: opacity 0.3s ease-out;
        }
        /* animate in */
        .mfp-ready.mfp-bg {
            opacity: 0.8;
        }
        /* animate out */
        .mfp-removing.mfp-bg {
            opacity: 0;
        }
    </style>
    <script type="text/javascript" src="<%= VirtualPathUtility.ToAbsolute("~/js/homepage.js") %>"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            //$('.scroll-dots a').click(function (event) {
            //    event.preventDefault();
            //    var target = $(this).attr('id');
            //    console.log(target);
            //});
        });
    </script>

    <script type="text/javascript">
    
    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main1">
        <!-- instagram pop -->
        <!--<div id="popup-dialog" class="zoom-anim-dialog mfp-hide">
            <div class="pop-text">
                <h3>GEWINNE<br />Wellness für deine Poren!</h3>
                <div class="pop-subtext">
                    <p>Zeig‘ uns wie du deine Poren mit Bioré verwöhnst und gewinne 1 exklusives Wellness-Wochenende für 2 Personen inkl. professioneller Gesichtsbehandlung oder 1 von 50 Bioré Produktsets!</p>

                    <p>Und so nimmst du teil:</p>

                    <ul>
                        <li>Folge unserem Instagram Kanal @biore_de</li>
                        <li>Lade dein schönstes Bioré Porenpflege- Foto auf Instagram hoch und versehe es mit #freeyourpores #porenwellness</li>
                    </ul>

                    <p><b><a href="/porenwellness/" target="_blank">Bitte beachte unsere Teilnahmebedingungen</a></b></p>
                </div>
          </div>
      </div>-->
    </div>

    <div class="home-content">
        <section class="home-section iex" id="section-01">
            <!--<div class="newicon">
                <img src="/images/homepage/new-top-icon.png" alt="new">
            </div>-->
            <div class="animation overwritte">
                <div class="animation-button">

                    <a href="https://www.youtube.com/embed/1oO2gBM8JGU?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm  fancybox.iframe">
                        <img src="/images/homepage/top-play-btn.png" alt="video play button" />
                    </a>

                </div>
            </div>
            <div class="welcome-module">
                <!--<video autoplay="autoplay" loop="loop" muted="muted" preload="auto" id="welcomeVideo">
                    <source src="<%= VirtualPathUtility.ToAbsolute("~/video/homepage/shayzilla.mp4") %>" type="video/mp4"/>
                    <source src="<%= VirtualPathUtility.ToAbsolute("~/video/homepage/shayzilla.ogv") %>" type="video/ogg"/>
                    <source src="<%= VirtualPathUtility.ToAbsolute("~/video/homepage/shayzilla.webm") %>" type="video/webm"/>
                </video>-->
                <div id="welcomeImage" class="hide-mobile show" style="height: 100%">
                    <img class="ïmg-less" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/charcoal-top-model-bg.jpg") %>" />
                </div>
                <div id="welcomeImage" class="hide-desktop">
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/charcoal-top-model-bg.jpg") %>" />
                </div>
                <div class="welcome-module-content">
                    <h1>ENTDECKE BIORÉ - DEN<br />
                        EXPERTEN FÜR FREIE POREN</h1>
                    <div class="centeringDiv alignCenter">
                        <h2 class="white-headline">BEFREIT POREN.</h2>
                        <h3 class="orange-headline">BEKÄMPFT PICKEL.</h3>
                        <h4 class="sub-headline">BIORÉ MIT AKTIVKOHLE UND BACKPULVER<br />
                            FÜR REINERE HAUT IN NUR 2 TAGEN.</h4>
                    </div>
                    <!--<p class="welcome-module-line1">UND DIE TIEFENREINIGENDE KRAFT VON</p>
                    <p class="welcome-module-line2">BACKPULVER UND AKTIVKOHLE</p>-->

                </div>
                <div class="hide-mobile">
                </div>
                <div class="bubbles-bg hide-mobile">

                    <!-- <div class="bubbles-charcoal-nosestrips">
                         <img class="products-responsive" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/charcoal-nosestrips.png") %>"/>
                     </div>-->
                    <div class="bubbles-charcoal-active">
                        <div class="newicon">
                            <img src="/images/homepage/new-top-icon.png" alt="new">
                        </div>
                        <a href="/bye-bye-pickel/charcoal-anti-pimple-clearing-cleanser"><img class="products-responsive" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/anti-pickel-charcoal.png") %>" /></a>
                    </div>

                    <div class="bubbles-charcoal-bakingsoda">
                        <a href="/bye-bye-pickel/baking-soda-anti-pimple-cleansing-foam"><img class="products-responsive" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/anti-pickel-baking-soda.png") %>" /></a>
                    </div>

                </div>
                <p class="scrollDown">SCROLL DOWN</p>
                <div id="arrowBounce" class="hide-mobile">
                    <a href="#section-06">
                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/arrowDown.png") %>" class="section-down-arrow" /></a>
                </div>
            </div>
            <div class="scroll-dots">
                <a href="#section-01"><span class="scroll-dot enable"></span></a>
                <a href="#section-06"><span class="scroll-dot"></span></a>
                <a href="#section-07"><span class="scroll-dot"></span></a>
                <a href="#section-02"><span class="scroll-dot"></span></a>
                <a href="#section-03"><span class="scroll-dot"></span></a>
                <a href="#section-04"><span class="scroll-dot"></span></a>
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
            <!--<div class="white-shadow hide-mobile"></div>-->
        </section>
        <section class="home-section" id="section-06">
            <div class="bg-left"></div>
            <div class="bg-right"></div>
            <div class="container3">
                <div class="container2">
                    <div class="container1">
                        <div class="headline">
                            <h4>SO LONG SPOTS WITH THE BLACK FACIAL GEL!</h4>
                        </div>
                        <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/embed/ffRpOxRF6Wg?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                <a href="https://www.youtube.com/embed/ffRpOxRF6Wg?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2-2018.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2-2018.gif") %>">
                                </a>
                                <div class="animation-skin-container">
                                    <div class="animation-skin">
                                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/06_anti_pickel-charcoal.png") %>" /></div>
                                </div>
                                <div class="animation-play-btn">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>" />
                                </div>
                            </div>
                        </div>
                        <div class="product">
                            <p>CHECK OUT HOW TO GET CLEARER SKIN IN JUST 2 DAYS WITH THE CHARCOAL ANTI-BLEMISH CLEARING CLEANSER</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="scroll-dots">
                <a href="#section-01"><span class="scroll-dot"></span></a>
                <a href="#section-06"><span class="scroll-dot enable"></span></a>
                <a href="#section-07"><span class="scroll-dot"></span></a>
                <a href="#section-02"><span class="scroll-dot"></span></a>
                <a href="#section-03"><span class="scroll-dot"></span></a>
                <a href="#section-04"><span class="scroll-dot"></span></a>
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section" id="section-07">
            <div class="bg-left"></div>
            <div class="bg-right"></div>
            <div class="container3">
                <div class="container2">
                    <div class="container1">
                        <div class="headline">
                            <h4>SAY FAREWELL TO SPOTS WITH A BIORÉ FOAM PARTY!</h4>
                        </div>
                        <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/embed/ffRpOxRF6Wg?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                <a href="https://www.youtube.com/embed/ffRpOxRF6Wg?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section3-2018.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section3-2018.gif") %>">
                                </a>
                                <div class="animation-skin-container">
                                    <div class="animation-skin">
                                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/07_baking_soda_anti-pickel.png") %>" /></div>
                                </div>
                                <div class="animation-play-btn">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>" />
                                </div>
                            </div>
                        </div>
                        <div class="product">
                            <p>LEARN HOW TO CLEAR BREAKOUTS WITH OUR FLUFFY BAKING SODA ANTI-BLEMISH CLEANSING FOAM</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="scroll-dots">
                <a href="#section-01"><span class="scroll-dot"></span></a>
                <a href="#section-06"><span class="scroll-dot"></span></a>
                <a href="#section-07"><span class="scroll-dot enable"></span></a>
                <a href="#section-02"><span class="scroll-dot"></span></a>
                <a href="#section-03"><span class="scroll-dot"></span></a>
                <a href="#section-04"><span class="scroll-dot"></span></a>
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section" id="section-02">
            <div class="bg-left"></div>
            <div class="bg-right"></div>
            <div class="container3">
                <div class="container2">
                    <div class="container1">
                        <div class="headline">
                            <h4>SCHAUMPARTY FÜR DEINE POREN!</h4>
                        </div>
                        <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/embed/qc1pVpXQPgY?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                <a href="https://www.youtube.com/embed/qc1pVpXQPgY?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section3.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section3.gif") %>">
                                </a>
                                <div class="animation-skin-container">
                                    <div class="animation-skin">
                                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/01_baking_soda_cleansing_scrub.png") %>" /></div>
                                </div>
                                <div class="animation-play-btn">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>" />
                                </div>
                            </div>
                        </div>
                        <div class="product">
                            <p>ENTDECKE WIE DAS PUDERPEELING MIT BACKPULVER FUNKTIONIERT</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="scroll-dots">
                <a href="#section-01"><span class="scroll-dot"></span></a>
                <a href="#section-06"><span class="scroll-dot"></span></a>
                <a href="#section-07"><span class="scroll-dot"></span></a>
                <a href="#section-02"><span class="scroll-dot enable"></span></a>
                <a href="#section-03"><span class="scroll-dot"></span></a>
                <a href="#section-04"><span class="scroll-dot"></span></a>
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section" id="section-03">
            <div class="bg-left"></div>
            <div class="bg-right"></div>
            <div class="container3">
                <div class="container2">
                    <div class="container1">
                        <div class="headline">
                            <h4>ENTZIEHT DEN SCHMUTZ WIE EIN MAGNET</h4>
                        </div>
                        <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/embed/ffRpOxRF6Wg?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                <a href="https://www.youtube.com/embed/ffRpOxRF6Wg?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section2.gif") %>">
                                </a>
                                <div class="animation-skin-container">
                                    <div class="animation-skin">
                                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/03_charcoal_pore_strips.png") %>" /></div>
                                </div>
                                <div class="animation-play-btn">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>" />
                                </div>
                            </div>
                        </div>
                        <div class="product">
                            <p>ERFAHRE WIE DER TIEFENREINIGENDE CLEAR-UP STRIP MIT AKTIVKOHLE ANGEWENDET WIRD</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="scroll-dots">
                <a href="#section-01"><span class="scroll-dot"></span></a>
                <a href="#section-06"><span class="scroll-dot"></span></a>
                <a href="#section-07"><span class="scroll-dot"></span></a>
                <a href="#section-02"><span class="scroll-dot"></span></a>
                <a href="#section-03"><span class="scroll-dot enable"></span></a>
                <a href="#section-04"><span class="scroll-dot"></span></a>
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section" id="section-04">
            <div class="bg-left"></div>
            <div class="bg-right"></div>
            <div class="container3">
                <div class="container2">
                    <div class="container1">
                        <div class="headline">
                            <h4>JETZT WIRD'S HEIß IM KAMPF GEGEN SCHMUTZ</h4>
                        </div>
                        <div class="animation">
                            <div class="animation-container">
                                <div class="animation-border"><a href="https://www.youtube.com/embed/ySL9f0MyogM?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe"></a></div>
                                <a href="https://www.youtube.com/embed/ySL9f0MyogM?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm">
                                    <img class="gif-preload" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section4.gif") %>">
                                    <img class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-origin="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/animation2.png") %>" data-animate="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section4.gif") %>">
                                </a>
                                <div class="animation-skin-container">
                                    <div class="animation-skin">
                                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/03_self-heating-mask.png") %>" /></div>
                                </div>
                                <div class="animation-play-btn">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>" />
                                </div>
                            </div>
                        </div>
                        <div class="product">
                            <p>LERNE WIE DU DIE 1-MINUTE-THERMOMASKE ANWENDEST</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="scroll-dots">
                <a href="#section-01"><span class="scroll-dot"></span></a>
                <a href="#section-06"><span class="scroll-dot"></span></a>
                <a href="#section-07"><span class="scroll-dot"></span></a>
                <a href="#section-02"><span class="scroll-dot"></span></a>
                <a href="#section-03"><span class="scroll-dot"></span></a>
                <a href="#section-04"><span class="scroll-dot enable"></span></a>
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>
        </section>
        <section class="home-section" id="section-05">
            <div class="bg-left"></div>
            <div class="bg-right"></div>
            <div class="container3">
                <div class="container2">
                    <div class="container1">
                        <div class="headline">
                            <h4>Mach Schluss mit der Maskerade!<br />
                                Befrei deine Poren und zeig dein reines Gesicht.</h4>
                        </div>

                        <div class="animation">
                            <a href="https://www.youtube.com/embed/1oO2gBM8JGU?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm fancybox.iframe">
                                <span style="width: 100%; height: 650px; position: absolute; top: 0; left: 0; z-index: 300; display: block;"></span>
                            </a>

                            <div class="animation-container">
                                <img id="imganim" class="gif-holder" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/gif/section5.gif") %>" alt="Mach Schluss mit der Maskerade!" />

                                <!--<div class="animation-skin-container">
                                    <div class="animation-skin"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/01_baking_soda_cleansing_scrub.png") %>"/></div>
                                </div>-->
                                <!--<div class="animation-play-btn">                                    
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/playbtn.png") %>"/>
                                </div>-->
                            </div>
                        </div>
                        <div class="product">
                            <p>...Finde uns auf</p>
                            <div class="social-container">
                                <div class="facebook"><a href="https://www.facebook.com/biorede/" target="_blank">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/facebook.png") %>" alt="facebook" /></a></div>
                                <div class="instagram"><a href="https://www.instagram.com/biore_de/" target="_blank">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/instagram.png") %>" alt="instagram" /></a></div>
                                <div class="youtube"><a href="https://www.youtube.com/channel/UCpFuYv-kk5cXbC_be362B0Q" target="_blank">
                                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/youtube.png") %>" alt="youtube" /></a></div>
                            </div>
                            <p>#freeyourpores</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="scroll-dots">
                <a href="#section-01"><span class="scroll-dot"></span></a>
                <a href="#section-06"><span class="scroll-dot"></span></a>
                <a href="#section-07"><span class="scroll-dot"></span></a>
                <a href="#section-02"><span class="scroll-dot"></span></a>
                <a href="#section-03"><span class="scroll-dot"></span></a>
                <a href="#section-04"><span class="scroll-dot"></span></a>
                <a href="#section-05"><span class="scroll-dot enable"></span></a>
            </div>
        </section>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/jquery.flexslider.min.js")
        .Render("~/js/combinedhome_#.js")
    %>
</asp:Content>

