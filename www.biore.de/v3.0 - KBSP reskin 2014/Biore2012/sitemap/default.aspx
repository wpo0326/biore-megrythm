﻿<%@ Page Title="Sitemap  Bioré Gesichtsreinigung" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.sitemap._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Entdecke die Bioré Website" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1 style="text-align:center;">Sitemap</h1>
                 <div id="responseRule"></div>
                 <div id="linkList">
                     <div id="prodLinks">
                         <ul class="col" id="col1">
                            <li><a href="../biore-facial-cleansing-products">Unsere Produkte <span class="arrow">&rsaquo;</span></a></li>
                            <li>
                                <h2 class="pie deepCleansing"><span class="subNavHeadlines"><span class="greenBold">Aktivkohle</span> für normale bis fettige Haut</span></h2>
                                <ul>
                                    <li><a href="../charcoal-for-oily-skin/deep-pore-charcoal-cleanser"><!--<span class="new">New</span>-->Porentiefes Waschgel mit Aktivkohle</a></li>
                                    <li><a href="../charcoal-for-oily-skin/self-heating-one-minute-mask">1-Minute-Thermomaske</a></li>
                                    <li><a href="../charcoal-for-oily-skin/charcoal-pore-minimizer">Poren verkleinerndes Peeling mit Aktivkohle</a></li>
                                    <li><a href="../charcoal-for-oily-skin/charcoal-pore-strips#regular">Tiefenreinigende Clear-Up Strips mit Aktivkohle</a></li>
                                    <!--<li><a href="../charcoal-for-oily-skin/combination-skin-balancing-cleanser">Combination Skin Balancing Cleanser</a></li>
                                    <li><a href="../charcoal-for-oily-skin/daily-cleansing-cloths">Daily Deep Pore Cleansing Cloths</a></li>
                                    <li><a href="../charcoal-for-oily-skin/make-up-removing-towelettes">Daily Makeup Removing Towelettes</a></li>
                                    <li><a href="../charcoal-for-oily-skin/pore-detoxifying-foam-cleanser">Pore Detoxifying Foam Cleanser</a></li>-->
                                </ul>
                            </li>
                        </ul>
                        <ul class="col" id="col2">
                            <li>
                                <h2 class="pie complexionClearing"><span class="subNavHeadlines"><span class="tealBold">
Backpulver</span> für trockene bis fettige Mischhaut</span></h2>
                                <ul>
                                    <li><a href="../baking-soda-for-combination-skin/baking-soda-pore-cleanser">Porenreinigendes Waschgel mit Backpulver</a></li>
                                    <li><a href="../charcoal-for-oily-skin/baking-soda-cleansing-scrub">Puderpeeling mit Backpulver</a></li>
                                    <!--<li><a href="../acnes-outta-here/blemish-fighting-ice-cleanser">Blemish Fighting Ice Cleanser</a></li>-->
                                </ul>
                                <h2 class="pie murt"><span class="subNavHeadlines"><span class="redBold">Clear-up Strips</span></h2>
                                <ul>
                                	<li><a href="../charcoal-for-oily-skin/charcoal-pore-strips#regular">Tiefenreinigende Clear-Up Strips mit Aktivkohle</a></li>
                                    <li><a href="../pore-strips/pore-strips-combo">Duo gegen Mitesser für Gesicht & Nase</a></li>
                                    
                                    <!--<li><a href="../pore-strips/pore-strips-ultra">Ultra Deep Cleansing Pore Strips</a></li>
                                    <li><a href="../pore-strips/pore-strips">Deep Cleansing Pore Strips</a></li>
                                    <li><a href="../pore-strips/warming-anti-blackhead-cleanser">Warming Anti-Blackhead Cleanser</a></li>-->
                                    
                                </ul>
                                <ul>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <ul class="col noPadding" id="col3">
                        <!--<li>
                            <a href="../biore-facial-cleansing-products/charcoal.aspx">What's New <span class="arrow">&rsaquo;</span></a>
                  
                        </li>
                        <li>
                            <a href="../pore-care/">Pore Care<span class="arrow">&rsaquo;</span></a>
                  
                        </li>
                        <li><a href="../email-newsletter-sign-up">Sign Me Up <span class="arrow">&rsaquo;</span></a></li>-->
                        <!--<li><a href="../where-to-buy-biore">"Store Finder" <span class="arrow">&rsaquo;</span></a></li>-->
                        <!--<li><a href="../biore-videos">Watch It Work <span class="arrow">&rsaquo;</span></a></li>-->
                        <li><a href="../about-us">Über Bioré <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaobrands.com/" target="_blank">Kao <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../contact-us">Kontakt <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaobrands.com/legal.asp" target="_blank">Rechtliche Hinweise <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">Datenschutzrichtlinien&nbsp;<span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../impressum">Impressum <span class="arrow">&rsaquo;</span></a></li>
                     </ul>
                 </div>
                 <div id="photo"></div>
            </div>
        </div>
    </div>
</asp:Content>