﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Biore2012.unsubscribe.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <style type="text/css">
        .loader-section, .email-unsubscribe {
            display:none;
        }
    </style>
     <link rel="stylesheet" type="text/css" href="<%= VirtualPathUtility.ToAbsolute("~/css/formConfig.css") %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="floodlightPixels" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="optOut" class="responsive centeringDiv">
                        <asp:Literal ID="litError" runat="server"></asp:Literal>

                        <asp:Panel ID="OptinForm" runat="server" CssClass="MemberInfoContainer">

                            <h1>Unsubscribe</h1>
                            <br /><br />
                            <p>
                                If you'd no longer like to hear about the latest news, promotions and contests, just
							enter your e-mail address below.
                            </p>
                                  

                            <!--<div class="parsys colpar">
                                <div class="loader-section"> 
                                    <div class="g-Text l-Text">
                                        <hr class="loader" />
                                    </div>
                                </div>
                                <div class="email-unsubscribe">
                                    <div class="section">
                                        <div class="unsubscribe-message">
                                            <p class="cmn-richtext">
                                                <span class="common_inplace_editor">
                                                    <span>Do you really want to unsubscribe from the following e-mail address?
                                                    </span>
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="common_text_item parbase section">
                                        <div class="unsubscribe-email">
                                            <p class="cmn-richtext">
                                                <span>{EMAIL}</span>  
                                            </p>
                                        </div>
                                    </div>
                                    <div class="parbase section common_button_pack">
                                        <div class="opt-al_c is-gutter--m g-ButtonP l-ButtonP js-uniformHeight" data-uniform-target="g-ButtonP__list__item">
                                            <ul class="g-ButtonP__list l-ButtonP__list">

                                                <li class="g-ButtonP__list__item l-ButtonP__list__item">
                                                    <div class="common_button_unit">
                                                        <div class="g-ButtonUnit l-ButtonUnit unsubscribe-yes is-colorset--1 is-size--m">
                                                            <a href="#" class="g-ButtonUnit__link l-ButtonUnit__link js-anchorLink"><span class="cmn-richtext">
                                                                <span class="parbase common_inplace_editor_nolink text common_inplace_editor">
                                                                    <span>Yes, unsubscribe<br />

                                                                    </span>
                                                                </span>
                                                            </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="g-ButtonP__list__item l-ButtonP__list__item">
                                                    <div class="common_button_unit">
                                                        <div class="g-ButtonUnit l-ButtonUnit is-colorset--1 is-size--m">
                                                            <a href="#" target="_self" id="common_button_unit_20180602023223814_x5wn" class="g-ButtonUnit__link l-ButtonUnit__link js-anchorLink">
                                                                <span class="parbase common_inplace_editor_nolink text common_inplace_editor">
                                                                    <span>No, do not unsubscribe<br />

                                                                    </span>
                                                                </span>
                                                        
                                                            </a>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="hidden section">
                                        <input id="new_form_contentPageKeyHash" name="contentPageKeyHash" value="8A8FB9E52C31783A6EC95773CFDF0FB01F7E7BF1" type="hidden">
                                    </div>
                                    <div class="hidden section">
                                        <input id="new_form_emailIdKeyHash" name="emailIdKeyHash" value="F5139E779477A63A2863F7F8EE42246C6F56B290" type="hidden">
                                    </div>
                                    <div class="hidden section">
                                        <input id="new_form_optInKeyHash" name="optInKeyHash" value="7D3CC7BD609178A36EEEA4CD63C1BA315F67D5A5" type="hidden">
                                    </div>
                                    <div class="common_text_item parbase section">
                                        <div class="g-Text l-Text opt-al_c error" id="error-400">
                                            <p class="cmn-richtext">
                                                <span class="parbase text common_inplace_editor">
                                                    <span>Unfortunately, we could not confirm your e-mail address.<br />

                                                    </span>
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="common_text_item parbase section">
                                        <div class="g-Text l-Text opt-al_c error" id="error-500">
                                            <p class="cmn-richtext">
                                                <span class="parbase text common_inplace_editor">
                                                    <span>We can not process your request at the moment. Please try again later.
                                                    </span>
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <p>&nbsp;</p>
                            <p style="font-style:italic">required<span class="required">*</span><br /><br />

                            </p>
                            <div class="tbin">
                                <div class="input-container">
                                    <asp:Label ID="emailLbl" runat="server" AssociatedControlID="Email" Text="Email*" Font-Bold="true" />
                                    <asp:TextBox ID="Email" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="EmailValidator1" Display="Dynamic" runat="server"
                                        ErrorMessage="<br />Please enter your Email Address." ControlToValidate="Email" EnableClientScript="true"
                                        SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="EmailValidator2" Display="Dynamic" runat="server"
                                        ErrorMessage="Please enter a valid Email Address." ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RegularExpressionValidator>
                                    <asp:RegularExpressionValidator ID="EmailValidator3" runat="server" Display="Dynamic"
                                        ErrorMessage="Please limit the entry to 50 characters."
                                        ValidationExpression="^[\s\S]{0,50}$" ControlToValidate="Email" SetFocusOnError="true"
                                        CssClass="errormsg"></asp:RegularExpressionValidator>
                                    <asp:RegularExpressionValidator ID="EmailValidator4" runat="server" Display="Dynamic"
                                        ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Email."
                                        ValidationExpression="^[^<>]+$" ControlToValidate="Email" EnableClientScript="true"
                                        SetFocusOnError="true" CssClass="errormsg"></asp:RegularExpressionValidator>
                                    <!--end Required Field Validator Container-->
                                </div>
                            </div>

                            <div id="contactFormWrap">
                                <div id="submit-container" class="SubmitContainer png-fix">
                                    <asp:Button UseSubmitBehavior="true" ID="submit" Text="Unsubscribe" runat="server" CssClass="submit buttonLink" />
                                
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="OptinFormSuccess" runat="server">
                            <h1>We're sorry to see you go...</h1>
                            <p>Your request has been sent and we&rsquo;ll go ahead and remove your e-mail address from our list.</p>
                            <p>If you change your mind, you can always re-subscribe right here on our website.</p>
                            <p><a href="<%= ResolveClientUrl("~/") %>">Return home</a>.</p>
                        </asp:Panel>
                        <asp:Panel ID="OptinFormFailure" runat="server">
                            <h1>We're Sorry...</h1>
                            <p>There has been a problem with your form submission.</p>
                            <p>Please go back and <a href="javascript: history.back(-1)">try again</a>.</p>
                        </asp:Panel>
                    </div>
                    <%-- end #optOut --%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
