﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Biore2012.DAL;

namespace Biore2012.unsubscribe
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.AppendHeader("Access-Control-Allow-Origin", "*");

            OptinFormSuccess.Visible = false;
            OptinFormFailure.Visible = false;

            int intSiteID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SiteID"]);
            string strIPAddr = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            string strBrowserInfo = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];
            string strURL = HttpContext.Current.Request.ServerVariables["URL"];

            string strResultData = "";
            string strHybrisReturn = "";
            string strEmail = "";
            string strDateTimeZulu = DateTime.UtcNow.ToString("s") + "+0000"; //DateTime.Now.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");

            if (Request.QueryString["email"] != null)
            {
                Email.Text = Server.UrlDecode(Request.QueryString["email"]);
            }

            if (IsPostBack)
            {
                OptinForm.Visible = false;
                strEmail = Email.Text;

                //Submit
                strResultData = CGOptin.ProcessOptOut(intSiteID, strIPAddr, strBrowserInfo, Email.Text, strURL);  //TODO:  shut down once final list pull is transferred to Hybris

                strHybrisReturn = Hybris.JsonPost("{ \"id\":\"" + strEmail + "\",\"timestamp\":\"" + strDateTimeZulu + "\",\"idOrigin\":\"CONSUMER_INFO\",\"contactType\":\"REGD\",\"addressLine1\":\"\",\"addressLine2\":\"\",\"addressLine3\":\"\",\"city\":\"\",\"state\":\"\",\"country\":\"US\",\"postCode\":\"\",\"emailAddress\":\"" + strEmail + "\",\"emailOptin\":\"N\",\"firstName\":\"\",\"gender\":\"\",\"lastName\":\"\",\"phoneNumber\":\"\",\"phoneOptin\":\"N\",\"mobileNumber\":\"\",\"mobileOptin\":\"N\",\"title\":\"\",\"dateOfBirth\":\"\",\"ageGroup\":\"\",\"frequencyOfSubscription\":\"0\",\"channelSource\":\"WEB\",\"marketingLocationId\":\"store_gb\",\"activitySource\":\"\",\"subscriptionPreference\":\"\",\"language\":\"EN\",\"action\":\"newsletter\",\"marketingArea\":\"BIORE_CA\"}");

                if (strHybrisReturn == "TRUE")  //process consumerinfo silently as it is no longer relevant and email may not be found there.
                {
                    OptinFormSuccess.Visible = true;
                    
                }
                else
                {
                    OptinFormFailure.Visible = true;
                    litError.Text = "<!-- Errors: ConsumerInfo:" + strResultData + " - Hybris: " + strHybrisReturn + " -->";
                }
            }
        }
    }
}