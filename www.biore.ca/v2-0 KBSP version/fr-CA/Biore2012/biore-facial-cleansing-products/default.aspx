﻿<%@ Page Title="Libérez vos pores – Obtenez une peau propre et saine – Découvrez tous les produits | Soins pour la peau BioréMD" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.our_products.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Les soins pour la peau BioréMD nettoient et exfolient doucement pour une peau propre et saine! Découvrez la gamme complète des soins pour la peau BioréMD." name="description" />
    <meta content="Clear Skin, Biore&reg; Products" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>
    <meta name="viewport" content="width=device-width">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="intro">
                <h1>Nos produits</h1>
                <div id="responseRule"></div>
                <p>Ciblez tous les problèmes de peau à la source – avec un nettoyage quotidien et un usage hebdomadaire des bandes pour les pores. Nos puissants nettoyants pour pores sont offerts sous forme de liquide, de masque désincrustant et de bandes pour une peau saine et nette.</p>
            </div>

            <div id="takeitalloffProds" class="prodList">
                <h2 class="pie roundedCorners takeitalloff">Juste enlevez <span>tout</span>
                    <span></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
 
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-cleansing-micellar-water.png" alt="" />
                        <asp:Literal ID="litBSMicellarWater" runat="server" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:9pt;">Nouveau</span> Eau micellaire<br />nettoyante au bicarbonate de soude</h3>
                        <p>Démaquille, nettoie les pores en profondeur et rafraîchit</p>
                        <a href="../take-it-all-off/baking-soda-cleansing-micellar-water" id="details-baking-soda-cleansing-micellar-water">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-cleansing-micellar-water.png" alt="" />
                        <asp:Literal ID="litCharcoalMicellarWater" runat="server" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:9pt;">Nouveau</span> Eau micellaire<br />nettoyante au charbon</h3>
                        <p>Démaquille, nettoie les pores en profondeur et rafraîchit</p>
                        <a href="../take-it-all-off/charcoal-cleansing-micellar-water" id="details-charcoal-cleansing-micellar-water">details ></a>
                    </li>
                </ul>
            </div>

            <div id="complexionClearingProds" class="prodList">
                <h2 class="pie roundedCorners complexionClearing">Fini <span>l’acné!<sup>MC</sup></span>
                    <span></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                   

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-acne-cleansing-foam.png" alt="" />
                        <asp:Literal ID="litBSAcneFoam" runat="server" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:9pt;">Nouveau</span> Mousse nettoyante <br />anti-acné <br />au bicarbonate de soude</h3>
                        <p>Désincruste délicatement la peau pour combattre l'acné.</p>
                        <a href="../acnes-outta-here/baking-soda-acne-cleansing-foam" id="details-baking-soda-acne-cleansing-foam">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-acne-scrub.png" alt="" />
                        <asp:Literal ID="litBSAcneScrub" runat="server" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:9pt;">Nouveau</span> Exfoliant anti-acné <br />au bicarbonate de soude</h3>
                        <p>Exfolie délicatement la peau pour contrer les boutons.</p>
                        <a href="../acnes-outta-here/baking-soda-acne-scrub" id="details-baking-soda-acne-scrub">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-acne-clearing-cleanser.png" alt="" />
                        <asp:Literal ID="litCharcoalAcneCleanser" runat="server" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:9pt;">Nouveau</span> Nettoyant anti-acné <br />au charbon</h3>
                        <p>Lave la peau à fond pour prévenir les boutons.</p>
                        <a href="../acnes-outta-here/charcoal-acne-clearing-cleanser" id="details-charcoal-acne-clearing-cleanser">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-acne-scrub.png" alt="" />
                        <asp:Literal ID="litCharcoalAcneScrub" runat="server" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:9pt;">Nouveau</span> Exfoliant anti-acné <br />au charbon</h3>
                        <p>Absorbe l’excès de sébum pour contrer les boutons.</p>
                        <a href="../acnes-outta-here/charcoal-acne-scrub" id="details-charcoal-acne-scrub">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishFightingIceCleanser.png" alt="" />
                        <asp:Literal ID="litBlemishIceCleanser" runat="server" />
                        <h3>Nettoyant glacé <br />anti-acné</h3>
                        <p>À base d’acide salicylique, cette formule libère les pores des impuretés, des résidus huileux et du maquillage.</p>
                        <a href="../acnes-outta-here/blemish-fighting-ice-cleanser" id="details-blemish-fighting-ice-cleanser">details ></a>
                    </li>
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/acneClearingScrub.png" alt="" />
                        <asp:Literal ID="litAcneClearingScrub" runat="server" />
                        <h3>Gommage nettoyant<br /> anti-acné</h3>
                        <p>Vient à bout des impuretés incrustées et des résidus huileux en deux jours.</p>
                        <a href="../acnes-outta-here/acne-clearing-scrub" id="details-acne-clearing-scrub">details ></a>
                    </li>-->
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishTreatingAstringentToner.png" alt="" />
                        <asp:Literal ID="litBlemishAstringent" runat="server" />
                        <h3>Astringent anti-acné</h3>
                        <p>Aidez à prévenir les boutons avec cet astringent sans huile.</p>
                        <a href="../acnes-outta-here/blemish-fighting-astringent-toner" id="details-blemish-fighting-astringent">details ></a>
                    </li>
                </ul>
            </div>

            <div id="deepCleansingProds" class="prodList">
                <h2 class="pie roundedCorners deepCleansing">Pensez <span>propre!<sup>MC</sup></span>
                    <span class="arrow"></span>
                </h2>
                <ul>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blue-agave-baking-soda-instant-warming-clay-mask.png" alt="" />
                         <asp:Literal ID="litBABSWarmingClayMask" runat="server" />
                        <h3>Agave Bleu + Bicarbonate de Soude Masque Chauffant Instantané à L’argile de Biore</h3>
                        <p>Se réchauffe au contact de la peau et purifie les pores en une minute seulement.</p>
                        <a href="../dont-be-dirty/blue-agave-baking-soda-instant-warming-clay-mask">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-whipped-mask.png" alt="" />
                         <asp:Literal ID="litCharcoalWhippedMask" runat="server" />
                        <h3>Masque détox onctueux et purifiant au charbon de Biore</h3>
                        <p>Avec du charbon naturel, reconnu pour sa capacité à déloger et à extraire les impuretés.</p>
                        <a href="../dont-be-dirty/charcoal-whipped-mask">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blue-agave-baking-soda-whipped-mask.png" alt="" />
                         <asp:Literal ID="litBABSWhippedMask" runat="server" />
                        <h3>Masque détox onctueux et nourrissant au agave bleu + bicarbonate de soude de Biore</h3>
                        <p>Avec de l’agave bleu, econnu pour ses propriétés apaisantes et revitalisante.</p>
                        <a href="../dont-be-dirty/baking-soda-pore-cleanser">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/two-step-charcoal-pore-kit.png" alt="" />
                         <asp:Literal ID="litCharcoalTwoStepKit" runat="server" />
                        <h3>Ensemble pour les pores à 2 étapes au charbon de Biore</h3>
                        <p>Enlevez ENCORE MIEUX la saleté, le sébum et les points noirs grâce à ce duo parfait au charbon</p>
                        <a href="../dont-be-dirty/two-step-charcoal-pore-kit">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blue-agave-baking-soda-balancing-pore-cleanser.png" alt="" />
                         <asp:Literal ID="litBABSBalancingPoreCleanser" runat="server" />
                        <h3>Nettoyant équilibrant au agave bleu + bicarbonate de soude pour les pores de Biore</h3>
                        <p>Nettoyage des pores en profondeur et exfoliation en douceur.</p>
                        <a href="../dont-be-dirty/blue-agave-baking-soda-balancing-pore-cleanser">details ></a>
                    </li>

                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-cleansing-scrub.png" alt="" />
                         <asp:Literal ID="litBakingSodaPoreCleanser" runat="server" />
                        <h3>Nettoyant exfoliant au bicarbonate de soude</h3>
                        <p>Une fois activé à l’eau, il nettoie la peau en profondeur et l’exfolie en douceur.</p>
                        <a href="../dont-be-dirty/baking-soda-cleansing-scrub" id="A4">details ></a>
                    </li>

                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-pore-cleanser.png" alt="" />
                         <asp:Literal ID="litBakingSodaCleansingScrub" runat="server" />
                        <h3>NETTOYANT AU BICARBONATE DE SOUDE POUR LES PORES</h3>
                        <p>Il pénètre les pores et exfolie la peau en douceur.</p>
                        <a href="../dont-be-dirty/baking-soda-pore-cleanser" id="A5">details ></a>
                    </li>
                	
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CHARCOAL_100X235.png" alt="" />
                         <asp:Literal ID="litDeepPoreCharcoalCleanser" runat="server" />
                        <h3>Nettoyant en profondeur au charbon pour les pores</h3>
                        <p>La formule au charbon attire les impuretés et les résidus huileux comme un aimant, laissant la peau propre et tonifiée.</p>
                        <a href="../dont-be-dirty/deep-pore-charcoal-cleanser" id="A1">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalBar.png" alt="" />
                         <asp:Literal ID="litCharcoalBar" runat="server" />
                        <h3>Pain désincrustant au charbon pour les pores</h3>
                        <p>Idéal pour visage et corps.</p>
                        <a href="../dont-be-dirty/charcoal-bar" id="A1">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/SHM_100X235.png" alt="" />
                        <asp:Literal ID="litSelfHeatingOneMinuteMask" runat="server" />
                        <h3>Masque une minute auto-chauffant</h3>
                        <p>Se réchauffe au contact de la peau et purifie les pores en une minute seulement.</p>
                        <a href="../dont-be-dirty/self-heating-one-minute-mask" id="A2">details ></a>
                    </li>
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/4In1DetoxifyingCleanser.png" alt="" />
                        <asp:Literal ID="litPoreFoamCleanser" runat="server" />
                        <h3>Mousse nettoyante revitalisante pour les pores</h3>
                        <p>Nettoyez, tonifiez et stimulez votre peau avec notre formule moussante.</p>
                        <a href="../dont-be-dirty/pore-detoxifying-foam-cleanser" id="details-pore-detoxifying-foam-cleanser">details ></a>
                    </li> -->
                   

                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/dailyPurifyingScrub.jpg" alt="" />
                        <asp:Literal ID="litPurifyingScrub" runat="server" />
                        <h3>DAILY PURIFYING SCRUB</h3>
                        <p>Delivers fresh, glowing skin by exfoliating and purifying </p>
                        <a href="../deep-cleansing-products/daily-purifying-scrub">details ></a>
                    </li>-->

                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/combinationSkinBalancingCleanser.png" alt="" />
                        <asp:Literal ID="litComboSkinCleanser" runat="server" />
                        <h3>Nettoyant équilibrant peau mixte</h3>
                        <p>3 Équilibre l’hydratation de la peau et traite la peau mixte.</p>
                        <a href="../dont-be-dirty/combination-skin-balancing-cleanser" id="details-combination-skin-balancing-cleanser">details ></a>
                    </li>-->
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/dailyCleansingCloths.png" alt="" />
                        <h3>Daily Deep Pore Cleansing Cloths</h3>
                        <p>Wipe away dirt, oil and makeup with an exfoliating cloth.</p>
                        <a href="../dont-be-dirty/daily-cleansing-cloths" id="A4">details ></a>
                    </li>-->
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/makeupRemovingTowelettes.png" alt="" />
                        <asp:Literal ID="litMakeUpTowelettes" runat="server" />
                        <h3>Lingettes démaquillantes quotidiennes</h3>
                        <p>Éliminent le maquillage hydrofuge tenace et nettoient les pores.</p>
                        <a href="../dont-be-dirty/make-up-removing-towelettes" id="details-make-up-removing-towelettes">details ></a>
                    </li>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/poreUncloggingScrub.png" alt="" />
                        <asp:Literal ID="litPoreUncloggingScrub" runat="server" />
                        <h3>Nettoyant granuleux pour les pores</h3>
                        <p>Adoucit et désobstrue la peau en ciblant les impuretés et les résidus huileux.</p>
                        <a href="../dont-be-dirty/pore-unclogging-scrub" id="A3">details ></a>
                    </li>
                       
                </ul>
            </div>



            <div id="murtProds" class="prodList">
                <h2 class="pie roundedCorners murt">Adieu <span>points noirs!<sup>MC</sup></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltra.png" alt="" />
                        <asp:Literal ID="litPoreStripsUltra" runat="server" />
                        <h3>Bandes de nettoyage en profondeur ultra <!--(boîtes de 6)--></h3>
                        <p>Triomphez des points noirs et éliminez deux fois plus d’impuretés tenaces.*</p>
                        <a href="../breakup-with-blackheads/deep-cleansing-pore-strips-ultra" id="details-deep-cleansing-pore-strips-ultra">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStrips.png" alt="" />
                        <asp:Literal ID="litPoreStrips" runat="server" />
                        <h3>Bandes de nettoyage en profondeur <!--(boîtes de 8 et de 14)--></h3>
                        <p>Désobstruent les pores en agissant comme un aimant, éliminant instantanément la saleté accumulée pendant des semaines. </p>
                        <a href="../breakup-with-blackheads/deep-cleansing-pore-strips" id="details-deep-cleansing-pore-strips">details ></a>
                    </li>
                    
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.png" alt="" />
                        <asp:Literal ID="litPoreStripsCombo" runat="server" />
                        <h3>Bandes de nettoyage en profondeur – emballage assorti <!--(boîtes de 14)--></h3>
                        <p>Désobstruent les pores du nez, du menton, des joues et du front en attirant les impuretés comme un aimant.</p>
                        <a href="../breakup-with-blackheads/deep-cleansing-pore-strips-combo" id="details-deep-cleansing-pore-strips-combo">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" />
                        <asp:Literal ID="litCharcoalStrips" runat="server" />
                        <h3>Bandes de nettoyage en profondeur des pores au charbon <!--(boîtes de 14)--></h3>
                        <p>Désobstruez vos pores et voyez 3x moins de sébum après un seul usage</p>
                        <a href="../breakup-with-blackheads/charcoal-pore-strips" id="details-deep-cleansing-charcoal-pore-strips">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsFace.png" alt="" />
                        <asp:Literal ID="litPoreStripsFace" runat="server" />
                        <h3>Bandes de nettoyage en profondeur pour les pores du visage<!--(boîtes de 14)--></h3>
                        <p>Désobstruez les pores de votre menton, de vos joues et de votre front.</p>
                        <a href="../breakup-with-blackheads/deep-cleansing-pore-strips-face" id="details-deep-cleansing-pore-strips-face">details ></a>
                    </li>
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/warmingAntiBlackheadCleanser.png" alt="" />
                        <asp:Literal ID="litBlackheadCleanser" runat="server" />
                        <h3>Nettoyant chauffant en crème anti­points noirs</h3>
                        <p>Détruisez les impuretés et les résidus huileux grâce à cette formule chauffante à microbilles. </p>
                        <a href="../breakup-with-blackheads/warming-anti-blackhead-cleanser" id="details-deep-cleansing-blackhead-cleanser">details ></a>
                    </li>-->
                    
                    
                    <!-- <li>
                        <img src="../images/ourProducts/products/small/combinationSkinBalancingCleanser.png" alt="" />
                        <h3>COMBINATION SKIN BALANCING CLEANSER</h3>
                        <p>Get an even, all-over clean for the complex needs of combination skin.</p>
                        <a href="../dont-be-dirty/combination-skin-balancing-cleanser" id="details-combination-skin-balancing-cleanser">details ></a>
                    </li>
                    <li class="taller" id="dailyCleansingCloths">
                        <img src="../images/ourProducts/products/small/dailyCleansingCloths.png" alt="" />
                        <h3>DAILY CLEANSING CLOTHS</h3>
                        <p>Wipe away dirt to reveal healthy, glowing skin.</p>
                        <a href="../dont-be-dirty/daily-cleansing-cloths" id="details-daily-cleansing-cloths">details ></a>
                    </li>
                    
                    <li>
                        <img src="../images/ourProducts/products/small/4In1DetoxifyingCleanser.png" alt="" />
                        <asp:Literal ID="litRevitalizingCleanser" runat="server" />
                        <h3>4-IN-1 DETOXIFYING CLEANSER</h3>
                        <p>Revitalize and refresh for healthier-looking skin.</p>
                        <a href="../dont-be-dirty/4-in-1-detoxifying-cleanser" id="details-4-in-1-detoxifying-cleanser">details ></a>
                    </li>
                     <li>
                        <img src="../images/ourProducts/products/small/steamActivatedCleanser.jpg" alt="" />
                        <asp:Literal ID="litSteamCleanser" runat="server" />
                        <h3>STEAM ACTIVATED CLEANSER</h3>
                        <p>Harness the power of pore-opening steam for a truly deep clean.</p>
                        <a href="../dont-be-dirty/steam-activated-cleanser" id="details-steam-activated-cleanser">details ></a>
                    </li> -->
                </ul>
            </div>
            <div style="font-size:9px; line-height:10px; margin:20px 0;">* Les bandes de nettoyage en profondeur pour les pores Ultra de Bior&eacute;<sup>MD</sup> délogent 2 fois plus d’impuretés qui obstruent les pores que les bandes de nettoyage en profondeur pour les pores Ultra de Bior&eacute;<sup>MD</sup> </div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
