﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Biore2012.BLL;
using Biore2012.DAL;
using System.Xml;

namespace Biore2012.our_products {
    public partial class Default : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e)
        {
            Site myMaster = (Site)this.Master;
            myMaster.bodyClass += " ourProducts";

            //check to see if ratings/reviews are active before proceeding.
            if (System.Configuration.ConfigurationManager.AppSettings["RatingsAndReviews"].ToLower() == "true")
            {
                //update the XML feed if necessary.
                XmlDocument xmlDoc = BazaarvoiceDAO.GetBVAPIFeed();

                //now build the ratings for each hard-coded product.

                //Acne's Outta Here
                litBSAcneFoam.Text = BazaarvoiceDAO.GetRatingsByProductID("baking-soda-acne-cleansing-foam");
                litBSAcneScrub.Text = BazaarvoiceDAO.GetRatingsByProductID("baking-soda-acne-scrub");
                litCharcoalAcneCleanser.Text = BazaarvoiceDAO.GetRatingsByProductID("charcoal-acne-clearing-cleanser");
                litCharcoalAcneScrub.Text = BazaarvoiceDAO.GetRatingsByProductID("charcoal-acne-scrub");
                litBlemishIceCleanser.Text = BazaarvoiceDAO.GetRatingsByProductID("blemish-fighting-ice-cleanser");
                litAcneClearingScrub.Text = BazaarvoiceDAO.GetRatingsByProductID("acne-clearing-scrub");
                litBlemishAstringent.Text = BazaarvoiceDAO.GetRatingsByProductID("blemish-fighting-astringent-toner");

                //Don't Be Dirty
                litBakingSodaPoreCleanser.Text = BazaarvoiceDAO.GetRatingsByProductID("baking-soda-pore-cleanser");
                litBakingSodaCleansingScrub.Text = BazaarvoiceDAO.GetRatingsByProductID("baking-soda-cleansing-scrub");
                litDeepPoreCharcoalCleanser.Text = BazaarvoiceDAO.GetRatingsByProductID("deep-pore-charcoal-cleanser");
                litSelfHeatingOneMinuteMask.Text = BazaarvoiceDAO.GetRatingsByProductID("self-heating-one-minute-mask");
                litPoreFoamCleanser.Text = BazaarvoiceDAO.GetRatingsByProductID("pore-detoxifying-foam-cleanser");
                litComboSkinCleanser.Text = BazaarvoiceDAO.GetRatingsByProductID("combination-skin-balancing-cleanser");
                litMakeUpTowelettes.Text = BazaarvoiceDAO.GetRatingsByProductID("make-up-removing-towelettes");
                litPoreUncloggingScrub.Text = BazaarvoiceDAO.GetRatingsByProductID("pore-unclogging-scrub");
                litCharcoalBar.Text = BazaarvoiceDAO.GetRatingsByProductID("charcoal-bar");

                litBABSWarmingClayMask.Text = BazaarvoiceDAO.GetRatingsByProductID("blue-agave-baking-soda-instant-warming-clay-mask");
                litCharcoalWhippedMask.Text = BazaarvoiceDAO.GetRatingsByProductID("charcoal-whipped-mask");
                litBABSWhippedMask.Text = BazaarvoiceDAO.GetRatingsByProductID("blue-agave-baking-soda-whipped-mask");
                litCharcoalTwoStepKit.Text = BazaarvoiceDAO.GetRatingsByProductID("two-step-charcoal-pore-kit");
                litBABSBalancingPoreCleanser.Text = BazaarvoiceDAO.GetRatingsByProductID("blue-agave-baking-soda-balancing-pore-cleanser");

                //Breakup with Blackheads
                litPoreStripsUltra.Text = BazaarvoiceDAO.GetRatingsByProductID("deep-cleansing-pore-strips-ultra");
                litPoreStrips.Text = BazaarvoiceDAO.GetRatingsByProductID("deep-cleansing-pore-strips");
                litPoreStripsCombo.Text = BazaarvoiceDAO.GetRatingsByProductID("deep-cleansing-pore-strips-combo");
                litBlackheadCleanser.Text = BazaarvoiceDAO.GetRatingsByProductID("warming-anti-blackhead-cleanser");
                litCharcoalStrips.Text = BazaarvoiceDAO.GetRatingsByProductID("charcoal-pore-strips");
                litPoreStripsFace.Text = BazaarvoiceDAO.GetRatingsByProductID("deep-cleansing-pore-strips-face");

                //Take it all Off
                litBSMicellarWater.Text = BazaarvoiceDAO.GetRatingsByProductID("baking-soda-cleansing-micellar-water");
                litCharcoalMicellarWater.Text = BazaarvoiceDAO.GetRatingsByProductID("charcoal-cleansing-micellar-water");

            }
        }
    }
}
