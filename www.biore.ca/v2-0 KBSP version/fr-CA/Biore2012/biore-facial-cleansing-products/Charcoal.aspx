﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Charcoal.aspx.cs" Inherits="Biore2012.biore_facial_cleansing_products.Charcoal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Purifiez vos pores grâce à notre masque et notre nettoyant pour le visage au charbon. Essayez le nettoyant en profondeur au charbon pour les pores et le masque une minute auto-chauffant des soins pour la peau BioréMD." />
    <meta content="Charcoal Mask, Charcoal Face Wash, charcoal" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/charcoalLanding.css")
        .Render("~/css/combinednew_#.css")
    %>
    <meta name="viewport" content="width=device-width">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">
<script type="text/javascript" src="http://o2.eyereturn.com/?site=2687&amp;page=biore_charcoal_FR"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main" class="charcoalLanding">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
                <div class="charcoalHeaderSpacer"></div>
                <h1><img src="<%= VirtualPathUtility.ToAbsolute("~/images/charcoalLanding/charcoalHeader.png") %>" border="0" alt="" /></h1>
                
            <div id="charcoalProdWrap">
                <div id="charDescript">
                    <!--<div id="charPaint"></div>-->
                    <h2 class="archer-book">Vraiment?<br /><span class="charGrey archer-bold">Le charbon?</span></h2>
                    <p>Oui, le charbon! Comme un aimant, le charbon attire et capture les impuretés de la peau, puis les déloge en nettoyant à fond. Pour vous faire profiter de ses vertus exceptionnelles, Bioré a intégré cet ingrédient à deux nouveautés : <br /><br />
                        les<b> NOUVELLES  <span class="charGreeen">bandes de nettoyage en profondeur au charbon pour les pores</span></b> et notre <b><span class="charGreeen">pain désincrustant au charbon pour les pores.</p>
                </div>
                <div id="charProducts"></div>
                <div style="width:285px; float:right; text-align:right; margin-top:0px;">                
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/breakup-with-blackheads/charcoal-pore-strips#regular") %>"><div class="charTry" style="margin-top:0px; width:130px;padding:7px 0px 7px 13px">Essayez maintenant</div></a>
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/dont-be-dirty/charcoal-bar") %>"><div class="charTry" style="margin-top:0px; width:120px;padding:7px 0px 7px 13px">Essayez maintenant</div></a>
                </div>
                <div style="clear:both;"></div>
                <br /><br /><br />
            </div>
            
            <!--<div class="charList archer-bold">
                <br />
                <span class="charGreeen">Bienfaits du pain de savon au charbon</span><br /><br />

                <span class="charbullet"></span>Exfolie doucement la peau et nettoie les pores à fond, délogeant la saleté et le sébum accumulés pour laisser une intense sensation de propreté<br />
                <br />
                <span class="charGreeen">Bienfaits des bandes au charbon</span><br /><br />
                <span class="charbullet"></span>3X moins de sébum en un seul usage<br />
                
            </div>-->

            <!--<div id="charTryNowWrap">
                <div id="charTryProdOne">
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/dont-be-dirty/charcoal-bar") %>"><div class="charTry">Essayez maintenant</div></a>
                </div>
                <div id="charTryProdTwo">
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/breakup-with-blackheads/charcoal-pore-strips#regular") %>"><div class="charTry">Essayez maintenant</div></a>
                </div>
            </div>-->
           
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
