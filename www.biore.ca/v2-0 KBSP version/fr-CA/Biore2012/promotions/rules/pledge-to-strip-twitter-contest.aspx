﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="ratings-and-review-contest.aspx.cs" Inherits="Biore2012.promotions.rules.ratings_and_review_contest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedrules_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <!--<div id="polaroid"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/forms/signUpPhoto.jpg") %>" alt="" /></div>-->
                <div id="content">
                    <h1>CONCOURS UNIVERSITAIRE TWITTER ADHÉSION À LA BANDE DE BIORE<sup style="font-size:20px; line-height:40px;">MD</sup> RÈGLEMENT OFFICIEL DU CONCOURS</h1>

<h2 style="text-transform:uppercase;">CE CONCOURS EST RÉSERVÉ AUX RÉSIDENTS DU CANADA ET EST RÉGI PAR LES LOIS CANADIENNES</h2>
<p>&nbsp;</p>
                    <p><strong>1.	PÉRIODE DU CONCOURS</strong>  </p>
                    <p>&nbsp;</p><p>Le concours universitaire Twitter Adhésion à la bande de BioréMD (le « concours ») débute le 8 septembre 2015 à 9 h, heure de l’Est (HE), et prend fin le 26 novembre 2015 à 23 h (HE), tel que précisé plus particulièrement pour chaque université dans le tableau de la section 3 (la « période du concours »).</p>
                    <p>&nbsp;</p>

                    <p><strong>2.	ADMISSIBILITÉ</strong></p>
                    <p>&nbsp;</p>
                    <p>Le concours s’adresse aux résidents du Canada qui ont 17 ans ou plus le 8 septembre 2015, à l’exception des dirigeants, des administrateurs, des employés, des représentants et des mandataires (et des personnes domiciliées à la même adresse, qu’elles soient apparentées ou non) de Kao Canada inc. (le « commanditaire »), de Campus Intercept Inc. et de leurs sociétés mères, filiales, sociétés affiliées, fournisseurs de prix et agences de publicité ou de promotion respectifs ainsi que toute entité qui participe à la création, à la production, à la mise en œuvre, à l’administration ou à la réalisation du concours ou qui agit à titre de juge de celui-ci (collectivement les « parties au concours »). </p>
                    <p>&nbsp;</p>

                    <p><strong>3.	COMMENT PARTICIPER</strong></p>
                    <p>&nbsp;</p>

                        <p>AUCUN ACHAT REQUIS. Le tableau ci-dessous dresse la liste des campus universitaires participants (les « campus »), où se trouvera une machine distributrice Bioré aux dates prévues ci-dessous (les « dates »).</p>
                        
                  <table border="1" cellspacing="15" cellpadding="0">
                          <tr bgcolor="#B9B9B9">
                            <td style="padding:5px;"><p><strong>Emplacement du campus</strong></p></td>
                            <td style="padding:5px;"><p><strong>Date</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Mohawk College </strong></p></td>
                            <td><p><strong>Du 8&nbsp;septembre&nbsp;2015   au 1er&nbsp;octobre&nbsp;2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Université McGill</strong></p></td>
                            <td><p><strong>Du 8&nbsp;septembre&nbsp;2015 au 1er&nbsp;octobre&nbsp;2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Seneca College </strong></p></td>
                            <td><p><strong>Du 8&nbsp;septembre&nbsp;2015 au 1er&nbsp;octobre&nbsp;2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Université de Toronto à   Mississauga</strong></p></td>
                            <td><p><strong>Du 8&nbsp;septembre&nbsp;2015 au 1er&nbsp;octobre&nbsp;2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Université de Windsor </strong></p></td>
                            <td><p><strong>Du 8&nbsp;septembre&nbsp;2015 au 1er&nbsp;octobre&nbsp;2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Université Queen's</strong></p></td>
                            <td><p><strong>Du 6&nbsp;octobre&nbsp;2015 au   29&nbsp;octobre&nbsp;2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Université d&rsquo;Ottawa </strong></p></td>
                            <td><p><strong>Du   6&nbsp;octobre&nbsp;2015 au 29&nbsp;octobre&nbsp;2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Université</strong><strong> York</strong></p></td>
                            <td><p><strong>Du   6&nbsp;octobre&nbsp;2015 au 29&nbsp;octobre&nbsp;2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>George Brown College</strong></p></td>
                            <td><p><strong>Du   6&nbsp;octobre&nbsp;2015 au 29&nbsp;octobre&nbsp;2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Université de Waterloo </strong></p></td>
                            <td><p><strong>Du   6&nbsp;octobre&nbsp;2015 au 29&nbsp;octobre&nbsp;2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Centennial College </strong></p></td>
                            <td><p><strong>Du 3&nbsp;novembre&nbsp;2015 au   26&nbsp;novembre&nbsp;2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>University of Western Ontario</strong></p></td>
                            <td><p><strong>Du   3&nbsp;novembre&nbsp;2015 au 26&nbsp;novembre&nbsp;2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Fanshawe College </strong></p></td>
                            <td><p><strong>Du   3&nbsp;novembre&nbsp;2015 au 26&nbsp;novembre&nbsp;2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Université Brock</strong></p></td>
                            <td><p><strong>Du   3&nbsp;novembre&nbsp;2015 au 26&nbsp;novembre&nbsp;2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Université Wilfrid Laurier</strong></p></td>
                            <td><p><strong>Du   3&nbsp;novembre&nbsp;2015 au 26&nbsp;novembre&nbsp;2015</strong></p></td>
                          </tr>
                        </table>
<p>&nbsp;</p>
                        
                        
                        
<p>&nbsp;</p>
                        <p>Pour vous inscrire au concours, rendez-vous à la machine distributrice Bioré de votre campus à la date indiquée. La machine distributrice à écran tactile vous fournira un code numérique unique (le « code »). Diffusez un message sur votre compte personnel Twitter (votre « compte ») qui comprend ce qui suit : i) la mention « @biorécanada » (la « mention »); ii) les mots-clics #adhèreàlabande, #[nom du campus] (p. ex. #Carleton) et #[code] (p. ex. #1234) (les « mots-clics ») (collectivement le « message »).</p>
                        <p>&nbsp;</p>
                  

                    

                  <p>Pour être admissible, votre message :</p>
                    <p>&nbsp;</p>

                    <div style="margin-left:50px">
                    i.	doit être conforme aux exigences particulières d’inscription mentionnées à la section 5;<br>
                    ii.	doit provenir d’un compte public;<br>
                    iii.	doit comprendre la mention et les mots-clics requis; <br>
                    iv.	doit être conforme aux modalités de service et aux règlements de Twitter, consultables sur www.twitter.com;<br>
                    v. 	ne doit pas être supprimé avant le 2 décembre 2015 à 12 h (HE), une fois qu’il a été diffusé. 
                  </div>
                    <p>&nbsp;</p>
                    <p>
Le commanditaire peut, à sa seule et entière discrétion, rejeter et déclarer inadmissible tout message qui n’est pas conforme aux règles édictées ci-dessus. Le message et l’inscription, telle qu’elle est définie ci-dessous, sont respectivement réputés nuls et invalides dans le cas où le message : i) est diffusé ou reçu en dehors de la période du concours, conformément au présent règlement; ii) n’est pas diffusé sur un compte public; iii) ne comprend pas la mention et les mots-clics requis; iv) n’est pas conforme aux modalités de service ou aux règles de Twitter; v) n’est pas conforme aux exigences d’inscription définies ci-dessous, à la section 5 (de l’avis du commanditaire, à sa seule et entière discrétion); ou vi) a été supprimé à tout moment après sa diffusion et avant le 2 décembre 2015 à 12 h (HE).</p><br />
                    

                    <p>Le participant admissible (voir ci-dessus) qui diffuse un message conforme au règlement officiel du concours (le « règlement ») a droit à une (1) inscription (l’« inscription »), à la seule et entière discrétion du commanditaire. 
<br><br>
Pour participer à ce concours, vous devez disposer d’un compte valide. Si vous n’avez pas de compte, rendez-vous sur www.twitter.com et créez gratuitement votre compte Twitter conformément aux directives.
<br><br>
Les tarifs normaux de messagerie texte et de transfert de données s’appliquent dans le cas où le participant diffuse son message au moyen d’un appareil mobile sans fil. Les fournisseurs de services sans fil peuvent imposer des frais supplémentaires pour l’envoi et la réception de chaque message texte standard. Veuillez communiquer avec votre fournisseur de services pour vous informer sur les forfaits et les tarifs avant d’utiliser votre appareil mobile pour participer au concours.
</p>
                    <p>&nbsp;</p>

                    <p><strong>4.	LIMITE ET VÉRIFICATION</strong></p>
                    <p>&nbsp;</p><p>Un (1) seul message et une (1) seule inscription par compte sont admis pendant la période du concours. Le commanditaire peut, à sa seule et entière discrétion, disqualifier un participant au concours et déclarer invalides son message et son inscription s’il s’avère que ce participant a soit : i) diffusé plus d’un (1) message par personne ou par compte pendant la période du concours; ii) obtenu plus d’une (1) inscription par personne ou par compte; iii) utilisé (ou tenté d’utiliser) plusieurs noms, identités, adresses de courriel ou comptes pour s’inscrire au concours. Est interdite l’utilisation (et la tentative d’utilisation) de plusieurs noms, identités, adresses de courriel ou comptes ainsi que l’utilisation de systèmes automatisés ou robotisés, de scripts, de macros ou de tout autre système ou programme en vue de s’inscrire au concours ou d’y participer autrement ou d’en perturber le déroulement, sous peine de disqualification par le commanditaire. Les renonciataires (définis ci-dessous) ne sont pas responsables des messages tardifs, perdus, égarés, retardés, incomplets ou irrecevables (lesquels sont tous nuls). 
<br><br>
Tous les messages peuvent faire l’objet d’une vérification par le commanditaire en tout temps. Le commanditaire se réserve le droit, à sa seule et entière discrétion, d’exiger une preuve d’identité ou d’admissibilité (sous une forme qu’il juge acceptable, notamment une pièce d’identité avec photo délivrée par un gouvernement) : i) aux fins de vérification de l’admissibilité du participant au concours; ii) aux fins de vérification de l’admissibilité ou de la légitimité d’une inscription ou d’un message dans le cadre du présent concours; iii) pour toute autre motif jugé nécessaire par le commanditaire, à sa seule et entière discrétion, dans le but d’administrer le présent concours conformément au règlement. Le défaut de fournir la preuve exigée en temps voulu, à la satisfaction du commanditaire, pourrait entraîner une disqualification, à sa seule et entière discrétion. Aux fins du présent concours, les seules dates et heures utilisées pour déterminer la validité des inscriptions sont celles du ou des serveurs utilisés pour le concours.
<br><br>
AUCUNE PERSONNE NI AUCUN ABONNÉ, PEU IMPORTE LE CAS OU LES CIRCONSTANCES, NE PEUT OBTENIR PLUS D’INSCRIPTIONS QUE LE NOMBRE MAXIMAL PERMIS SELON LE RÈGLEMENT.
</p>
                    <p>&nbsp;</p>

                    <p><strong>5.	EXIGENCES D’INSCRIPTION </strong></p>
                    <p>&nbsp;</p>
                    <p>PAR LA DIFFUSION D’UN MESSAGE, VOUS RECONNAISSEZ SA CONFORMITÉ À TOUTES LES MODALITÉS DU PRÉSENT RÈGLEMENT ET AUX MODALITÉS DE SERVICE DE TWITTER. LES RENONCIATAIRES (DÉFINIS CI-DESSOUS) N’ASSUMENT AUCUNE RESPONSABILITÉ JURIDIQUE EN CE QUI A TRAIT À L’UTILISATION DE TOUT MESSAGE QUE VOUS DIFFUSEZ, DE MÊME DANS L’ÉVENTUALITÉ OÙ IL S’AVÉRERAIT SUBSÉQUEMMENT QUE VOUS AVEZ ENFREINT UNE DISPOSITION DU PRÉSENT RÈGLEMENT. 
<br><br>
Par sa participation au concours et la diffusion d’un message, chaque participant accepte d’être lié par le présent règlement officiel et son interprétation par le commanditaire, et il garantit et déclare en outre que le contenu diffusé dans son message (à l’exclusion de la mention et des mots-clics) (collectivement son « contenu ») : 
</p>
                    <p>&nbsp;</p>
                    <p>a) est de son cru et qu’il possède tous les droits nécessaires à l’égard de son contenu pour s’inscrire au concours;
<br><br>
b)	ne contrevient à aucune loi;
<br><br>
c)	ne contient aucune référence ou ressemblance à une tierce partie identifiable, à moins d’avoir obtenu son consentement et celui de son parent ou tuteur légal si cette personne est mineure dans sa province ou son territoire de résidence; 
<br><br>
d)	ne donnera lieu à aucune réclamation pour infraction pour atteinte à la vie privée ou à la publicité, et ne violera aucun droit ni intérêt d’une tierce partie, ni ne donnera lieu à aucune réclamation de paiement, quelle qu’elle soit;
 <br><br>
e)	n’est pas diffamatoire, notamment à l’endroit d’une entreprise, ni pornographique ou obscène et ne contient et ne comprend aucun des éléments suivants ni n’y fait allusion, de quelque façon que ce soit : la nudité; la consommation d’alcool, de drogues ou de tabac; une activité ou une insinuation à caractère sexuel explicite; une expression ou un symbole grossier, vulgaire ou injurieux; la caractérisation méprisante d’un groupe, notamment ethnique, racial, sexuel ou religieux; un contenu qui avalise, tolère ou aborde des activités, des conduites ou des comportements illégaux, inappropriés ou risqués, quels qu’ils soient; des renseignements personnels, y compris notamment le nom, le numéro de téléphone ou l’adresse (physique ou électronique) d’une personne; un message commercial, une comparaison ou une sollicitation à l’égard de produits ou de services autres que ceux du commanditaire; un produit d’une tierce partie ou une marque de commerce, une marque ou un logo autre que ceux du commanditaire; une conduite ou une activité contraire au présent règlement; tout autre contenu de nature inappropriée, inadéquate ou injurieuse ou susceptible de l’être, de l’avis du commanditaire, à sa seule et entière discrétion.
</p>
                    
                    <p>&nbsp;</p> 

                    <p>Par sa participation au concours et la diffusion d’un message, chaque participant : i) sans déroger aux modalités de service de Twitter, accorde au commanditaire, à perpétuité, une licence non exclusive pour publier, afficher, reproduire, modifier, corriger ou utiliser autrement son message, en tout ou en partie, à des fins de publicité ou de promotion du concours ou pour tout autre motif; ii) renonce à tous les droits moraux rattachés à son message en faveur du commanditaire; iii) exonère les parties au concours et chacun de leurs mandataires, employés, administrateurs, successibles et ayants droit (collectivement, les « renonciataires ») de toute responsabilité relativement à des réclamations fondées sur les droits publicitaires, la diffamation, l’atteinte à la vie privée, la violation de droits d’auteur, la violation de marques de commerce ou toute cause d’action liée à la propriété intellectuelle. Pour plus de certitude, le commanditaire se réserve le droit, à sa seule et entière discrétion, de modifier, de corriger ou d’invalider un message en tout temps pendant le concours, s’il reçoit une plainte relativement à celui-ci ou pour quelque motif que ce soit. Dans un tel cas, ou s’il estime, à sa seule et entière discrétion, qu’un message n’est pas conforme au règlement, il se réserve le droit d’invalider le message (et l’inscription correspondante) et de disqualifier le participant visé, à tout moment et pour quelque motif que ce soit. Les messages ne font PAS l’objet d’une évaluation par un jury.</p>
                    <p>&nbsp;</p>

                    <p><strong>6. 	LE PRIX</strong> </p>
                    <p>&nbsp;</p>
                    <p>Quinze (15) prix BioréMD (les « prix ») sont offerts. Chacun de ces prix consiste en un (1) an d’approvisionnement gratuit en bandes de nettoyage pour les pores de BioréMD.
                    <br><br>
Un (1) prix est attribué par campus. Chaque prix totalise une valeur au détail approximative de 70,00 $ CA. Les caractéristiques et attributs de chaque élément d’un prix, à l’exception de ce qui est décrit ci-dessus, sont à la seule et entière discrétion du commanditaire. Le prix doit être accepté tel quel et il n’est ni transférable, ni cessible, ni monnayable (sauf autorisation du commanditaire, à sa seule et entière discrétion). Aucune substitution n’est autorisée, à moins que le commanditaire n’en décide autrement. Le commanditaire se réserve le droit, à sa seule et entière discrétion, de remplacer le prix, en tout ou en partie, par un prix de valeur égale ou supérieure, ce qui peut comprendre, entre autres, la remise d’une somme en argent.
</p>
                    <p>&nbsp;</p>

                    <p><strong>7. 	LE TIRAGE DES PRIX</strong></p>
                    <p>&nbsp;</p>
                    <p>Le tableau ci-dessous présente les dates de tirage (chacune étant une « date de tirage ») pour chaque campus :</p>
                    
                    <table border="1" cellspacing="15" cellpadding="0">
                      <tr bgcolor="#B4B4B4">
                        <td style="padding:5px;"><p><strong>Emplacement du campus</strong></p></td>
                        <td style="padding:5px;"><p><strong>Date de tirage</strong></p></td>
                      </tr>
                      <tr>
                        <td><p><strong>Mohawk College </strong><br>
                          <strong>Université McGill</strong><br>
                          <strong>Seneca College </strong><br>
                          <strong>Université de Toronto à Mississauga</strong><br>
                          <strong>Université de Windsor</strong></p></td>
                        <td><p><strong>Le mardi 6&nbsp;octobre&nbsp;2015</strong></p></td>
                      </tr>
                      <tr>
                        <td><p><strong>Université</strong><strong> Queen's</strong><br>
                          <strong>Université</strong><strong> d&rsquo;Ottawa </strong><br>
                          <strong>Université</strong><strong> York</strong><br>
                          <strong>George   Brown College</strong><br>
                          <strong>Université</strong><strong> de Waterloo</strong></p></td>
                        <td><p><strong>Le mardi 3&nbsp;november&nbsp;2015</strong></p></td>
                      </tr>
                      <tr>
                        <td><p><strong>Centennial   College </strong><br>
                          <strong>University   of Western Ontario</strong><br>
                          <strong>Fanshawe   College </strong><br>
                          <strong>Université</strong><strong> Brock</strong><br>
                          <strong>Université</strong><strong> Wilfrid Laurier</strong></p></td>
                        <td><p><strong>Le mardi 1er&nbsp;décembre&nbsp;2015</strong></p></td>
                      </tr>
                    </table>
<p>&nbsp;</p>
                    

                    <p>Un tirage au sort aura lieu à la date de tirage ou vers celle-ci, à environ 14 h (HE), à Toronto (Ontario), parmi toutes les inscriptions valides reçues, conformément au présent règlement (selon ce que détermine le commanditaire, à sa seule et entière discrétion), afin de sélectionner le gagnant potentiel pour chaque campus. Les chances de gagner un prix dépendent du nombre d’inscriptions admissibles reçues conformément au présent règlement (selon ce que détermine le commanditaire, à sa seule et entière discrétion).
<br><br>
Le commanditaire ou son représentant désigné tentera à au moins trois (3) reprises de joindre le gagnant potentiel au moyen d’un message Twitter diffusé sur son compte @biorécanada. En plus de dévoiler l’identité du gagnant potentiel, ce message invitera ce dernier à « répondre directement » au commanditaire dans les cinq (5) jours ouvrables suivant la date du tirage. Dans le cas où le gagnant potentiel ne peut être joint, pour quelque motif que ce soit, dans les cinq (5) jours ouvrables suivant la date du tirage, ou dans le cas où le gagnant potentiel enfreint autrement le présent règlement, il sera disqualifié (et sera du même coup déchu de son droit au prix) et, dès lors, le commanditaire se réserve le droit, à sa seule et entière discrétion, de sélectionner aléatoirement un autre gagnant potentiel parmi les inscriptions admissibles restantes (auquel cas les dispositions précitées dans la présente section s’appliquent au nouveau gagnant potentiel, avec les adaptations nécessaires).
<br><br>
AVANT DE POUVOIR ÊTRE OFFICIELLEMENT DÉCLARÉ GAGNANT DU PRIX, le gagnant potentiel doit : a) répondre correctement à une question réglementaire d’arithmétique, dans un délai convenu, sans aide mécanique ou autre, soit par téléphone ou par courriel (au choix du commanditaire), à un moment qui convient aux deux parties, ou au moyen du formulaire de déclaration et d’exonération de responsabilité; b) signer et transmettre au commanditaire, dans les délais déterminés par le commanditaire (ou son représentant désigné), le formulaire de déclaration et d’exonération de responsabilité, par lequel le gagnant (entre autres) : i) confirme qu’il respecte le présent règlement; ii) déclare qu’il accepte le prix tel quel; iii) exonère les renonciataires de toute responsabilité relativement au concours, à sa participation au concours, à la remise d’un prix et à l’utilisation, appropriée ou non, du prix, en tout ou en partie; iv) consent à la publication, à la reproduction ou à l’utilisation à d’autres fins de ses nom, adresse, voix, déclarations relatives au concours, photo ou autre image le représentant, sans autre avis ni contrepartie, dans toute publicité ou annonce du commanditaire ou pour son compte, sous quelque forme que ce soit, notamment dans la presse écrite, par radiodiffusion, par télédiffusion ou par Internet. Dans le cas où le gagnant potentiel est mineur dans sa province ou son territoire de résidence, le commanditaire peut exiger que son parent ou tuteur légal ratifie le formulaire d’exonération de responsabilité. Le commanditaire peut, à sa seule et entière discrétion, disqualifier le gagnant potentiel (qui est du même coup déchu de son droit au prix) et choisir aléatoirement un autre gagnant potentiel parmi les inscriptions admissibles restantes (auquel cas les dispositions de cette section s’appliquent au nouveau gagnant potentiel, avec les adaptations nécessaires) dans le cas où le gagnant potentiel : a) répond incorrectement à la question réglementaire d’arithmétique; b) omet de transmettre les documents requis dûment remplis conformément aux exigences précitées; ou c) n’est pas en mesure d’accepter le prix, pour quelque motif que ce soit.
</p>
                    <p>&nbsp;</p>

                    <p><strong>8.	CONDITIONS GÉNÉRALES</strong></p>
                    <p>&nbsp;</p>
                    <p>Par sa participation au concours, le participant reconnaît qu’il se conforme au règlement officiel et il accepte d’être lié par celui-ci, y compris les conditions d’admissibilité. En outre, chaque participant exonère les renonciataires des modes d’action, causes d’action, poursuites, dettes, conventions, contrats, réclamations et exigences (y compris les honoraires et frais juridiques), de quelque type que ce soit, notamment les réclamations éventuelles du participant ou de ses administrateurs, héritiers, successibles ou ayants droit, fondées sur la négligence, la rupture de contrat, la violation fondamentale et la responsabilité en cas de dommages corporels ou matériels ou de décès découlant de l’inscription du participant au concours, de son acceptation ou de son utilisation du prix ou qui s’y rattachent, de même qu’il les exonère de toute responsabilité découlant du concours ou du prix ou qui s’y rattache, en matière de dommages corporels ou matériels. Le concours est assujetti à l’ensemble de la réglementation fédérale, provinciale et municipale applicable. Les décisions du commanditaire (ou de son représentant désigné, lorsqu’il est dûment mandaté à cette fin) relativement à tous les aspects de ce concours sont péremptoires, sans appel et exécutoires pour tous les participants, y compris notamment les décisions concernant la validité ou la disqualification des participants, des messages ou des inscriptions. LE COMMANDITAIRE PEUT, EN TOUT TEMPS ET À SA SEULE ET ENTIÈRE DISCRÉTION, DISQUALIFIER TOUTE PERSONNE QUI, DE SON AVIS, ENFREINT LE RÈGLEMENT OFFICIEL POUR QUELQUE MOTIF QUE CE SOIT. 
<br><br>
Les renonciataires ne peuvent être tenus responsables dans l’un ou l’autre des cas suivants : i) une défaillance technique ou un autre problème lié au réseau, aux combinés, à l’équipement ou aux lignes de téléphone, aux systèmes informatiques en ligne, aux serveurs, aux fournisseurs d’accès, à l’équipement informatique, aux plateformes de médias sociaux ou aux logiciels; ii) un problème de transmission d’un message Twitter ou d’une inscription ou d’un autre renseignement envoyé, capté ou enregistré, pour quelque motif que ce soit; iii) une combinaison des éventualités ci-dessus. 
<br><br>
En cas de différend quant à l’identité de l’auteur d’un message Twitter, le commanditaire se réserve le droit de considérer que l’auteur du message est le titulaire autorisé du compte d’où provient le message. Le « titulaire autorisé du compte » désigne la personne qui possède le compte Twitter visé (selon les registres de Twitter). Le participant peut être contraint de fournir une preuve (sous une forme que le commanditaire juge acceptable) attestant qu’il est le titulaire autorisé du compte associé au message Twitter en question.
<br><br>
Le commanditaire se réserve le droit, à sa seule et entière discrétion, sous réserve de l’approbation donnée par la Régie (définie ci-dessous) au Québec, s’il y a lieu, de supprimer, de modifier ou de suspendre ce concours (ou d’en modifier le règlement), de quelque façon que ce soit, en cas d’erreur, de problème technique, de virus informatique, de bogue, de trafiquage, d’intervention non autorisée, de fraude, de défaillance technique ou de toute autre situation indépendante de sa volonté raisonnable qui entraverait le bon déroulement du concours comme prévu dans le présent règlement. Toute tentative de nuire à l’administration légitime de ce concours, de quelque façon que ce soit (de l’avis du commanditaire, à sa seule et entière discrétion), constitue une infraction au droit civil et pénal, et le commanditaire se réserve le droit, le cas échéant, d’intenter des recours en dommages-intérêts, dans toute la mesure permise par la loi. Le commanditaire se réserve le droit, à sa seule et entière discrétion, d’annuler, de modifier ou de suspendre ce concours ou d’en modifier le règlement, de quelque façon que ce soit, sans préavis ni obligation, en cas d’accident ou de toute erreur, notamment d’impression ou d’administration, ou pour tout autre motif. Sans restreindre la généralité de ce qui précède, le commanditaire se réserve le droit, à sa seule et entière discrétion, de demander au gagnant potentiel de répondre à une autre question réglementaire selon ce qu’il jugera approprié et les circonstances, ou pour se conformer aux lois applicables.
<br><br>
Le commanditaire se réserve le droit, à sa seule et entière discrétion et sans préavis, de modifier les dates et les délais prévus dans le présent règlement, en tout ou en partie, dans la mesure nécessaire pour vérifier si un participant, son message ou son inscription satisfait au règlement, pour composer avec des problèmes techniques ou d’autre nature ou pour s’adapter à toute autre circonstance qu’il juge, à sa seule et entière discrétion, préjudiciable à la bonne administration du concours, conformément au règlement, ou pour tout autre motif.
</p>
                   
                    
                    <p>&nbsp;
                        </p>

                    <p><strong>9.	RÉSIDENTS DU QUÉBEC</strong> </p>
                    <p>&nbsp;</p>
                    <p>Tout différend quant à la tenue ou l’organisation d’un concours publicitaire peut être soumis à la Régie des alcools, des courses et des jeux (la « Régie »), pour qu’il soit tranché. Tout litige afférent à la remise d’un prix peut être soumis à la Régie seulement dans le but d’aider les parties à s’entendre. </p>
                    <p>&nbsp;</p>
                    <p><strong>10.	CONSENTEMENT À L’UTILISATION DE RENSEIGNEMENTS PERSONNELS</strong></p>
                    <p>&nbsp;</p>
                    <p>Par son inscription au concours, chaque participant consent expressément à ce que le commanditaire, ses mandataires et ses représentants divulguent, utilisent et conservent les renseignements personnels fournis dans son formulaire de demande, et ce strictement aux fins de l’administration de l’offre et en conformité avec la politique de confidentialité du commanditaire (consultable à : http://www.biore.ca/fr-CA/privacy/), sauf décision contraire du participant.</p>
                    <p>&nbsp;</p>
                    <p><strong>11. 	AUTRES CONDITIONS</strong> </p>
                    <p>&nbsp;</p>
                    <p>Sous réserve des limites imposées par la loi, les conditions énoncées dans la version anglaise du présent règlement prévalent en cas de divergence ou d’incompatibilité entre celles-ci et les affirmations ou autres énoncés que pourrait comporter tout autre élément afférent au concours, y compris, entre autres, la version française du présent règlement et la publicité aux points de vente, à la télévision, dans des publications imprimées ou en ligne. </p>
                    <p>&nbsp;</p>  

                </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>

