﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


namespace Biore2012.DAL
{
    public class CGOptin
    {
        public static String ProcessOptIn(Int32 SiteID, Int32 EventID, string FirstName, string LastName, string Email,
                                           string Dobdd, string Dobmo, string Dobyr, string Street1, string Street2,
                                           string Province, string City, string PostalCode, string PrimaryLanguage, 
                                           string HomePhone, string MobilePhone,
                                           string Gender, string URL, string RemoteAddr, string UserAgent, string EventCode,
                                           string ContactContent, string ContactVia, string ContactFreq, string PageURL,
                                           string OpenEnded, string HookID)
        {

            string strReturn = "FALSE";
            //Get connection string from Web.Config

            string connStr = ConfigurationManager.ConnectionStrings["ConsumerInfo"].ConnectionString;
            SqlConnection sqlConn = new SqlConnection(connStr);

            try
            {
                sqlConn.Open();
                SqlCommand sqlCmd = new SqlCommand("spEntryForm", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.AddWithValue("@SiteID", SiteID);//1103
                sqlCmd.Parameters.AddWithValue("@EventID", EventID);//71103
                sqlCmd.Parameters.AddWithValue("@FName", FirstName);
                sqlCmd.Parameters.AddWithValue("@LName", LastName);
                sqlCmd.Parameters.AddWithValue("@Email", Email);
                sqlCmd.Parameters.AddWithValue("@Gender", Gender);
                sqlCmd.Parameters.AddWithValue("@DOB", Dobmo + "/" + Dobdd + "/" + Dobyr); //DBNull.Value
                sqlCmd.Parameters.AddWithValue("@Addr1", Street1);
                sqlCmd.Parameters.AddWithValue("@Addr2", Street2);
                sqlCmd.Parameters.AddWithValue("@City", City);
                sqlCmd.Parameters.AddWithValue("@PostalCode", PostalCode);
                sqlCmd.Parameters.AddWithValue("@PrimaryLanguageID", PrimaryLanguage);
                sqlCmd.Parameters.AddWithValue("@HomePhone", HomePhone);
                sqlCmd.Parameters.AddWithValue("@MobilePhone", MobilePhone);
                sqlCmd.Parameters.AddWithValue("@IPAddr", RemoteAddr);
                sqlCmd.Parameters.AddWithValue("@BrowserInfo", UserAgent);
                sqlCmd.Parameters.AddWithValue("@RegionID", Province);
                sqlCmd.Parameters.AddWithValue("@EventCode", EventCode); //jfsignup_ca_en  //jf_rasample_ca_en
                sqlCmd.Parameters.AddWithValue("@Message", "entry: " + EventCode);
                sqlCmd.Parameters.AddWithValue("@ContactContent", ContactContent); //samples,contests,sweepstakes,other info,
                sqlCmd.Parameters.AddWithValue("@ContactVia", ContactVia); //email,
                sqlCmd.Parameters.AddWithValue("@ContactFreq", ContactFreq); //as needed,
                sqlCmd.Parameters.AddWithValue("@URL", PageURL); //"www.johnfrieda.ca/event/sampleRequest.aspx"
                sqlCmd.Parameters.AddWithValue("@OpenEnded", OpenEnded);
                sqlCmd.Parameters.AddWithValue("@HookID", HookID); //either 0 or the brand-specific hook from tblHooks

                //the following params are required but are either not user-specific or are pre-defined.

                sqlCmd.Parameters.AddWithValue("@ParentMemberID", 0);
                sqlCmd.Parameters.AddWithValue("@MemberID", 0);
                sqlCmd.Parameters.AddWithValue("@Email_alt", "");
                sqlCmd.Parameters.AddWithValue("@LoginPW", "");
                sqlCmd.Parameters.AddWithValue("@NickName", "");
                sqlCmd.Parameters.AddWithValue("@Member_URL", "");
                sqlCmd.Parameters.AddWithValue("@Title", "");
                sqlCmd.Parameters.AddWithValue("@MName", "");
                sqlCmd.Parameters.AddWithValue("@Addr3", "");
                sqlCmd.Parameters.AddWithValue("@CountryID", ConfigurationManager.AppSettings["site_CountryID"]);
                sqlCmd.Parameters.AddWithValue("@SecondaryLanguageID", "");
                sqlCmd.Parameters.AddWithValue("@WorkPhone", "");
                // output params

                SqlParameter outParam1 = sqlCmd.Parameters.Add("iError", SqlDbType.Int, 4);
                outParam1.Direction = ParameterDirection.Output;

                SqlParameter outParam2 = sqlCmd.Parameters.Add("ErrDesc", SqlDbType.VarChar, 50);
                outParam2.Direction = ParameterDirection.Output;

                SqlParameter outParam3 = sqlCmd.Parameters.Add("_sp_MemberID", SqlDbType.Int, 4);
                outParam3.Direction = ParameterDirection.Output;

                sqlCmd.ExecuteNonQuery();

                int intError = Convert.ToInt32(outParam1.Value);
                string strError = outParam2.Value.ToString();
                int intMemberID = Convert.ToInt32(outParam3.Value);

                //check for errors reported by stored procedure
                if (strError == "Success")
                    strReturn = "TRUE" + intMemberID.ToString();
                else
                    strReturn = strError + "<br />" + intError.ToString() + "<br />" + intMemberID.ToString();

            }
            catch (Exception ex)
            {
                strReturn = ex.ToString();
            }
            finally
            {

                sqlConn.Close();
            }

            return strReturn;
        }

        public static String ProcessTraits(Int32 MemberID, Int32 EventId, string RemoteAddr, string UserAgent,
                                            string URL, int TraitId, string TraitValue)
        {
            string strReturn = "FALSE";
            //Get connection string from Web.Config

            string connStr = ConfigurationManager.ConnectionStrings["ConsumerInfo"].ConnectionString;
            SqlConnection sqlConn = new SqlConnection(connStr);

            try
            {
                sqlConn.Open();
                SqlCommand sqlCmd = new SqlCommand("spMemberTrait", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.AddWithValue("@_sp_MemberID", MemberID);
                sqlCmd.Parameters.AddWithValue("@EventId", EventId);
                sqlCmd.Parameters.AddWithValue("@HookId", ConfigurationManager.AppSettings["HookID"].ToString());
                sqlCmd.Parameters.AddWithValue("@IPAddr", RemoteAddr);
                sqlCmd.Parameters.AddWithValue("@SiteID", ConfigurationManager.AppSettings["SiteID"].ToString());
                sqlCmd.Parameters.AddWithValue("@BrowserInfo", UserAgent);
                sqlCmd.Parameters.AddWithValue("@URL", URL);
                sqlCmd.Parameters.AddWithValue("@TraitID", TraitId);
                sqlCmd.Parameters.AddWithValue("@TraitValue", TraitValue);

                // output params

                SqlParameter outParam1 = sqlCmd.Parameters.Add("iError", SqlDbType.Int, 4);
                outParam1.Direction = ParameterDirection.Output;

                SqlParameter outParam2 = sqlCmd.Parameters.Add("ErrDesc", SqlDbType.VarChar, 50);
                outParam2.Direction = ParameterDirection.Output;

                sqlCmd.ExecuteNonQuery();

                int intError = Convert.ToInt32(outParam1.Value);
                string strError = outParam2.Value.ToString();

                //check for errors reported by stored procedure
                if (strError == "Success")
                    strReturn = "TRUE";
                else
                    strReturn = strError + "<br />" + intError.ToString();
            }
            catch (Exception ex)
            {
                strReturn = ex.ToString();
            }
            finally
            {
                sqlConn.Close();
            }

            return strReturn;
        }

        public static String ProcessOptOut(int SiteID, string IPAddr, string BrowserInfo, string Email, string URL)
        {
            string strReturn = "FALSE";

            string connStr = ConfigurationManager.ConnectionStrings["ConsumerInfo"].ConnectionString;
            SqlConnection sqlConn = new SqlConnection(connStr);

            try
            {
                sqlConn.Open();
                SqlCommand sqlCmd = new SqlCommand("spOptOut", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.AddWithValue("@SiteID", SiteID);
                sqlCmd.Parameters.AddWithValue("@IPAddr", IPAddr);
                sqlCmd.Parameters.AddWithValue("@BrowserInfo", BrowserInfo);
                sqlCmd.Parameters.AddWithValue("@Email", Email.ToLower());
                sqlCmd.Parameters.AddWithValue("@ContactContent", "unsubscribe");
                sqlCmd.Parameters.AddWithValue("@ContactVia", "unsubscribe");
                sqlCmd.Parameters.AddWithValue("@ContactFreq", "unsubscribe");
                sqlCmd.Parameters.AddWithValue("@URL", URL);

                //output params
                SqlParameter outParam1 = sqlCmd.Parameters.Add("iError", SqlDbType.Int, 4);
                outParam1.Direction = ParameterDirection.Output;

                SqlParameter outParam2 = sqlCmd.Parameters.Add("ErrDesc", SqlDbType.VarChar, 50);
                outParam2.Direction = ParameterDirection.Output;

                sqlCmd.ExecuteNonQuery();

                int intError = Convert.ToInt32(outParam1.Value);
                string strError = outParam2.Value.ToString();

                //check for errors reported by stored procedure
                if (strError == "Success")
                    strReturn = "TRUE";
                else
                    strReturn = strError + "<br />" + intError.ToString();
            }
            catch (Exception ex)
            {
                strReturn = ex.ToString();
            }
            finally
            {
                sqlConn.Close();
            }

            return strReturn;

        }

        public static Int32 checkSampleCount(string strEventID)
        {
            int intReturn = 0;

            string connStr = System.Configuration.ConfigurationManager.ConnectionStrings["ConsumerInfo"].ConnectionString;
            SqlConnection sqlConn = new SqlConnection(connStr);

            try
            {
                sqlConn.Open();
                SqlCommand sqlComm = new SqlCommand("select COUNT(distinct memberid) from tbleventlog where EventID=" + strEventID, sqlConn);
                //SqlCommand sqlComm = new SqlCommand("select isnull(count(eventlogid),0) as EntryCount from tblEventLog where EventID=" + strEventID, sqlConn);
                intReturn = (int)sqlComm.ExecuteScalar();
            }
            catch { }
            finally { sqlConn.Close(); }

            return intReturn;
        }

        public static Int32 checkMemberSubscribed(string Email, string Message, string SiteID)
        {
            int intReturn = 1;

            string connStr = System.Configuration.ConfigurationManager.ConnectionStrings["consumerInfo"].ConnectionString;
            SqlConnection sqlConn = new SqlConnection(connStr);

            try
            {
                sqlConn.Open();
                //SqlCommand sqlComm = new SqlCommand("select count(distinct Message) from tblEventLog a inner join tblmembers b on a.memberid = b.memberid where b.email = '" + Email + "' and a.message = '" + Message + "'", sqlConn);
                SqlCommand sqlComm = new SqlCommand("select COUNT(distinct a.memberid) from tblMemberInterests a inner join tblMembers b on b.MemberID = a.MemberID where b.Email = '" + Email + "' and a.SiteID ='" + SiteID + "' and a.ContactVia like '%email%'", sqlConn);
                intReturn = (int)sqlComm.ExecuteScalar();
            }
            catch { }
            finally { sqlConn.Close(); }

            return intReturn;
        }

        public static Boolean EmailIsValid(string Email)
        {
            Boolean blnReturn = false;

            string pattern = @"^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            System.Text.RegularExpressions.Match match = Regex.Match(Email, pattern, RegexOptions.IgnoreCase);

            if (match.Success)
                blnReturn = true;

            return blnReturn;

        }

        public static Boolean EventHasStarted(string strEventID)
        {
            bool blnReturn = false;
            string strStartDate = "";

            string connStr = System.Configuration.ConfigurationManager.ConnectionStrings["consumerInfo"].ConnectionString;
            SqlConnection sqlConn = new SqlConnection(connStr);

            try
            {
                sqlConn.Open();
                SqlCommand sqlComm = new SqlCommand("select top 1 dtEventStart from tblEvents where eventID = " + strEventID, sqlConn);
                strStartDate = sqlComm.ExecuteScalar().ToString();

                if (DateTime.Now >= Convert.ToDateTime(strStartDate))
                    blnReturn = true;

            }
            catch { }
            finally { sqlConn.Close(); }

            return blnReturn;
        }

        public static Boolean EventHasEnded(string strEventID)
        {
            bool blnReturn = false;
            string strEndDate = "";

            string connStr = System.Configuration.ConfigurationManager.ConnectionStrings["consumerInfo"].ConnectionString;
            SqlConnection sqlConn = new SqlConnection(connStr);

            try
            {
                sqlConn.Open();
                SqlCommand sqlComm = new SqlCommand("select top 1 dtEventEnd from tblEvents where eventID = " + strEventID, sqlConn);
                strEndDate = sqlComm.ExecuteScalar().ToString();

                if (DateTime.Now >= Convert.ToDateTime(strEndDate))
                    blnReturn = true;

            }
            catch { }
            finally { sqlConn.Close(); }

            return blnReturn;
        }

    }

    

}
