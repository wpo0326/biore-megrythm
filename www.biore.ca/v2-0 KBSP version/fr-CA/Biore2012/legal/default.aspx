﻿<%@ Page Title="Privacy | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.sitemap._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id=“mainContent” style=“min-height:730px”>
            <div id="shadow"></div>
            <div class="centeringDiv">
            <br /><br />
                 <h1>Avis juridique</h1>
                 <h2>© 2017 Kao Canada Inc. ("Kao"). Tous droits réservés.  </h2><br />
                 <p>EN UTILISANT LE PRÉSENT SITE WEB, VOUS COMPRENEZ ET ACCEPTEZ CE QUI SUIT :</p><br />
                 
                 <p>Les renseignements contenus dans le présent site Web (le « site ») concernant tout produit et service ne s'appliquent qu'au Canada et sont destinés à un usage personnel et non commercial. Tous les titres, noms et éléments graphiques utilisés dans le site sont des marques de commerce de Kao ou de ses concédants, à moins d'indication contraire. La reproduction, la republication et la distribution de tout élément figurant dans ce site à des fins autres que pour vous permettre de consulter le site sont strictement interdites, exception faite que l'utilisateur peut, pour des raisons personnelles et non commerciales, les télécharger et en faire une copie papier. Les renseignements relatifs aux produits et services de Kao peuvent être modifiés sans préavis. Certains produits et services peuvent ne pas être offerts dans certaines régions. Les renseignements contenus dans le présent site sont fournis à titre informatif seulement. Les points de vue et opinions exprimés sur ce site ne sont pas nécessairement ceux de Kao. Toute mention de produits ou d’entreprises ne doit en aucun cas être considérée comme un endossement desdits produits ou entreprises ou comme un énoncé d’affiliation.</p><br />
                
                 <p>Les renseignements présentés sur le présent site sont fournis à titre informatif et ne peuvent en aucun cas se substituer à un avis, diagnostic ou traitement médical professionnel. L’utilisation des renseignements fournis sur le site est entièrement à vos propres risques. Si vous avez des questions concernant des aspects médicaux ou de santé, communiquez avec un professionnel de la santé. </p><br />

<p>Vous ne pouvez utiliser aucun moteur de recherche Web, robot, technique de forage de données ni aucun autre dispositif ou procédé automatique pouvant permettre de cataloguer, télécharger, enregistrer ou autrement reproduire, stocker ou distribuer le contenu du site. Vous ne pouvez prendre aucune mesure visant à interférer avec le site, l’interrompre ou interrompre l’utilisation par d’autres usagers incluant, sans s’y limiter, en surchargeant, inondant, bombardant de courriels ou paralysant le site, contournant les mesures de sécurité ou d’authentification des utilisateurs ou tentant de dépasser les privilèges d’autorisation et d’accès accordés en vertu des présentes modalités. Vous ne pouvez intégrer des portions du site à l’intérieur d’un autre site ou établir des liens entre un autre site et toute page du présent, exception faite de la page d’accueil. Vous ne pouvez revendre à un tiers l’utilisation du site et son accès sans notre consentement écrit préalable.</p><br />

<p>TOUTES LES PRÉCAUTIONS ONT ÉTÉ PRISES POUR S'ASSURER QUE LE CONTENU DU SITE EST EXACT ET À JOUR. Kao NE FAIT AUCUNE REPRÉSENTATION OU GARANTIE QUANT À L'EXACTITUDE OU AU CARACTÈRE COMPLET DU SITE OU DE SON CONTENU, QUI SONT FOURNIS EN L’ÉTAT. Kao NE GARANTIT PAS QUE L'USAGE DU SITE SE FERA SANS INTERRUPTION OU SANS ERREUR, QUE LES DÉFECTUOSITÉS, LE CAS ÉCHÉANT, SERONT CORRIGÉES, OU QUE LE SITE OU LE SERVEUR QUI LE MET À VOTRE DISPOSITION EST OU SERA EXEMPT D'ERREUR, ACCESSIBLE, ININTERROMPU, OU EXEMPT DE VIRUS, DE DISPOSITIFS DOMMAGEABLES OU D'AUTRES COMPOSANTES. Kao NE PEUT ÊTRE TENUE RESPONSABLE DE L’UTILISATION DU SITE, Y COMPRIS, SANS S’Y LIMITER, DU CONTENU ET DE TOUTE ERREUR. Kao EXCLUT TOUTE GARANTIE EXPLICITE OU IMPLICITE (NOTAMMENT DE QUALITÉ MARCHANDE, D'ABSENCE DE CONTREFAÇON ET D’ADÉQUATION À UN USAGE PARTICULIER).</p><br />

<p>Le présent site peut contenir des liens ou des références à d’autres sites Web. Kao ne peut toutefois être tenue responsable du contenu desdits sites ni des dommages découlant des contenus en question. Les liens vers d’autres sites sont fournis aux utilisateurs uniquement pour des raisons de commodité.</p><br />

<p>Kao se soucie de la protection de votre vie privée. Pour obtenir de plus amples renseignements à ce sujet, consultez notre Politique de confidentialité.</p>
<p>Ni Kao, ni ses agents, ses sociétés affiliées, représentants ou toute autre partie participant à la création, à la production ou à l'administration du site (collectivement les « parties de Kao ») ne peuvent être tenus responsables des dommages indirects, accessoires, consécutifs, spéciaux ou exemplaires résultant de l’utilisation du site ou de tout produit ou service offert sur le site, et cela, même si lesdites parties ont été informées de la possibilité de tels dommages. </p> <br />

<p>La responsabilité globale des parties de Kao concernant les dommages, blessures, pertes ou causes d’action (que ce soit de manière contractuelle, délictuelle ou autre) résultant de l'utilisation du site ne peut en aucun cas dépasser : (a) le montant que vous avez versé, s’il y a lieu, à Kao pour utiliser le site ou (b) 100 $.</p><br />

<p>Le présent site est fourni à titre de service à ses visiteurs. Kao se réserve le droit de supprimer, modifier ou amender le contenu du site en tout temps, y compris modifier les présentes modalités, pour quelque raison, et cela, sans préavis. Les changements seront publiés sur le site. Consultez celui-ci régulièrement afin de prendre connaissance de tout changement apporté. En continuant d'accéder au site après de tels changements, vous indiquez de ce fait votre acceptation des changements en question.</p><br />

<p>Les présentes modalités sont régies par les lois de l'Ontario et du Canada et sont interprétées en vertu de telles lois, sans donner effet à aucun principe de conflit de lois. Vous acceptez que tout litige lié à l’utilisation de ce site ou de tout produit ou service de Kao soit soumis exclusivement à des tribunaux fédéraux ou provinciaux au Canada et vous acceptez irrévocablement de le soumettre à la juridiction des tribunaux compétents. Si une disposition des présentes modalités devait être illégale, nulle et non avenue ou, pour n'importe quel motif, non exécutoire, une telle disposition serait alors jugée dissociable des présentes et n'aurait aucun effet sur la validité ou le caractère exécutoire de toute disposition restante. </p><br /><br />


                 
            </div>
        </div>
    </div>
</asp:Content>