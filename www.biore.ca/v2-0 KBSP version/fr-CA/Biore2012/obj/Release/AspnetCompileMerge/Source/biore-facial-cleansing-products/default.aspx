﻿<%@ Page Title="Exfoliate &amp; Get a Clean Face - Browse All Products  | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.our_products.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Get clean skin: Bioré® Skincare products cleanse and gently exfoliate, giving you healthy, radiant skin! See the complete Bioré® Skincare product line." name="description" />
    <meta content="exfoliate" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">
<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: Biore Product Page
URL of the webpage where the tag is expected to be placed: http://www.biore.com/en-US/biore-facial-cleansing-products/
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 02/01/2012
-->
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="http://fls.doubleclick.net/activityi;src=1418070;type=biore797;cat=biore083;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="http://fls.doubleclick.net/activityi;src=1418070;type=biore797;cat=biore083;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="intro">
                <h1>Our Products</h1>
                <p>Get a deep clean so you're ready 24/7&mdash;exfoliate and cleanse your skin daily with our invigorating cleansing products, plus pore strip weekly for a complete Bior&eacute;<sup>&reg;</sup> clean.</p>
            </div>

            <div id="complexionClearingProds" class="prodList">
                <h2 class="pie complexionClearing">acne's <span>outta here!<sup>&trade;</sup></span></h2>
                <ul>
                    <li>
                        <img src="../images/ourProducts/products/small/blemishFightingIceCleanser.png" alt="" />
                        <h3>BLEMISH FIGHTING<br />ICE CLEANSER</h3>
                        <p>Stay in control, help stop blemishes before they start.</p>
                        <a href="../complexion-clearing-products/blemish-fighting-ice-cleanser" id="details-blemish-fighting-ice-cleanser">details ></a>
                    </li>
                    <li>
                        <img src="../images/ourProducts/products/small/acneClearingScrub.png" alt="" />
                        <h3>ACNE CLEARING<br />SCRUB</h3>
                        <p>Get clearer, healthier skin in just two days.</p>
                        <a href="../complexion-clearing-products/acne-clearing-scrub" id="details-acne-clearing-scrub">details ></a>
                    </li>
                    <li>
                        <img src="../images/ourProducts/products/small/blemishTreatingAstringentToner.png" alt="" />
                        <h3>BLEMISH TREATING ASTRINGENT</h3>
                        <p>Go beyond clean to help treat blemishes with salicylic acid.</p>
                        <a href="../complexion-clearing-products/blemish-treating-astringent-toner" id="details-blemish-treating-astringent">details ></a>
                    </li>
                </ul>
            </div>

            <div id="deepCleansingProds" class="prodList">
                <h2 class="pie deepCleansing">don't be <span>dirty<sup>&trade;</sup></span></h2>
                <ul>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CHARCOAL_100X235.png" alt="" />
                        <h3>Deep Pore<br />Charcoal Cleanser</h3>
                        <p>Get an even, all-over clean for the complex needs of combination skin.</p>
                        <a href="../deep-cleansing-products/combination-skin-balancing-cleanser" id="A1">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/SHM_100X235.png" alt="" />
                        <h3>Self Healing<br />One Minute Mask</h3>
                        <p>Get an even, all-over clean for the complex needs of combination skin.</p>
                        <a href="../deep-cleansing-products/combination-skin-balancing-cleanser" id="A2">details ></a>
                    </li>
                    <li>
                        <img src="../images/ourProducts/products/small/poreUncloggingScrub.png" alt="" />
                        <h3>PORE UNCLOGGING SCRUB</h3>
                        <p>Target pore-clogging dirt and oil for a clear complexion.</p>
                        <a href="../deep-cleansing-products/pore-unclogging-scrub" id="A3">details ></a>
                    </li>
                    <li>
                        <img src="../images/ourProducts/products/small/combinationSkinBalancingCleanser.png" alt="" />
                        <h3>COMBINATION SKIN BALANCING CLEANSER</h3>
                        <p>Get an even, all-over clean for the complex needs of combination skin.</p>
                        <a href="../deep-cleansing-products/combination-skin-balancing-cleanser" id="details-combination-skin-balancing-cleanser">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/dailyCleansingCloths.png" alt="" />
                        <h3>DAILY CLEANSING CLOTHS</h3>
                        <p>Wipe away dirt to reveal healthy, glowing skin.</p>
                        <a href="../deep-cleansing-products/daily-cleansing-cloths" id="A4">details ></a>
                    </li>
                    <li>
                        <img src="../images/ourProducts/products/small/makeupRemovingTowelettes.png" alt="" />
                        <h3>MAKE-UP REMOVING TOWELETTES</h3>
                        <p>Remove waterproof mascara better than the leading towelette.</p>
                        <a href="../make-up-removing-products/make-up-removing-towelettes" id="details-make-up-removing-towelettes">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/4In1DetoxifyingCleanser.png" alt="" />
                        <h3>4-IN-1 DETOXIFYING CLEANSER</h3>
                        <p>Revitalize and refresh for healthier-looking skin.</p>
                        <a href="../deep-cleansing-products/4-in-1-detoxifying-cleanser" id="details-4-in-1-detoxifying-cleanser">details ></a>
                    </li>    
                </ul>
            </div>



            <div id="murtProds" class="prodList">
                <h2 class="pie murt">break up with <span>blackheads<sup>&trade;</sup></span></h2>
                <ul>
                    <li>
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltra.png" alt="" />
                        <h3>DEEP CLEANSING<br />PORE STRIPS ULTRA</h3>
                        <p>Amp up the Pore Strip with tingling ingredients like tea tree oil, menthol and witch hazel.</p>
                        <a href="../deep-cleansing-product-family/pore-strips#ultra" id="details-deep-cleansing-pore-strips-ultra">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStrips.png" alt="" />
                        <h3>DEEP CLEANSING<br />PORE STRIPS</h3>
                        <p>Remove a week's worth of build-up in minutes.</p>
                        <a href="../deep-cleansing-product-family/pore-strips" id="details-deep-cleansing-pore-strips">details ></a>
                    </li>
                    <li>
                        <img src="../images/ourProducts/products/small/warmingAntiBlackheadCleanser.png" alt="" />
                        <h3>Warming Anti-Blackhead<br />Cleanser</h3>
                        <p>Strip more than just your nose with Pore Strips for your cheeks, chin and forehead.</p>
                        <a href="../deep-cleansing-product-family/pore-strips#combo" id="details-deep-cleansing-pore-strips-combo">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.png" alt="" />
                        <h3>Deep Cleansing Pore<br />Strips Combo</h3>
                        <p>Target pore-clogging dirt and oil for a clear complexion.</p>
                        <a href="../deep-cleansing-products/pore-unclogging-scrub" id="details-pore-unclogging-scrub">details ></a>
                    </li>
                    
                    
                    <!-- <li>
                        <img src="../images/ourProducts/products/small/combinationSkinBalancingCleanser.png" alt="" />
                        <h3>COMBINATION SKIN BALANCING CLEANSER</h3>
                        <p>Get an even, all-over clean for the complex needs of combination skin.</p>
                        <a href="../deep-cleansing-products/combination-skin-balancing-cleanser" id="details-combination-skin-balancing-cleanser">details ></a>
                    </li>
                    <li class="taller" id="dailyCleansingCloths">
                        <img src="../images/ourProducts/products/small/dailyCleansingCloths.png" alt="" />
                        <h3>DAILY CLEANSING CLOTHS</h3>
                        <p>Wipe away dirt to reveal healthy, glowing skin.</p>
                        <a href="../deep-cleansing-products/daily-cleansing-cloths" id="details-daily-cleansing-cloths">details ></a>
                    </li>
                    
                    <li>
                        <img src="../images/ourProducts/products/small/4In1DetoxifyingCleanser.png" alt="" />
                        <h3>4-IN-1 DETOXIFYING CLEANSER</h3>
                        <p>Revitalize and refresh for healthier-looking skin.</p>
                        <a href="../deep-cleansing-products/4-in-1-detoxifying-cleanser" id="details-4-in-1-detoxifying-cleanser">details ></a>
                    </li>
                     <li>
                        <img src="../images/ourProducts/products/small/steamActivatedCleanser.jpg" alt="" />
                        <h3>STEAM ACTIVATED CLEANSER</h3>
                        <p>Harness the power of pore-opening steam for a truly deep clean.</p>
                        <a href="../deep-cleansing-products/steam-activated-cleanser" id="details-steam-activated-cleanser">details ></a>
                    </li> -->
                </ul>
            </div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
