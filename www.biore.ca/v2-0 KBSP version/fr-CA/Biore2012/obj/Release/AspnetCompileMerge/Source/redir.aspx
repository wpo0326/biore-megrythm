﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="redir.aspx.cs" Inherits="Biore2012.redir" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    If your browser doesn't redirect automatically, click <a href="<asp:Literal ID='litRedirectUrl' runat='server' />">here</a>.
    </div>
    </form>
    
    <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
        try {
            var firstTracker = _gat._getTracker("UA-385129-27"); // Global Tag
            firstTracker._setDomainName("none");
            firstTracker._trackPageview();
            var secondTracker = _gat._getTracker("UA-385129-4"); // Biore Tag
            secondTracker._trackPageview();
        } catch (err) { }
    </script>
</body>
</html>
