﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Biore2012.BLL;

namespace Biore2012.Forms.RatingsAndReviews
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            initErrorMessages();
            AuthUtils au = new AuthUtils();
            if (au.CheckIfAuthenticated() == false)
            {
                Response.Redirect("Login.aspx");
            }

            literalBackButton.Text = "<a href=\"" + Request.QueryString["return"] + "\" class=\"buttonLink\">Back</a><br />";


        }

        private void initErrorMessages()
        {
            reqCurrPassword.Text = "<p>Veuillez entrer votre mot de passe actuel.</p>";
            regCurrLengthPassword.Text = "<p>Le mot de passe actuel doit compter au moins six (6) caractères.</p>";
            regCurrPassword.Text = "<p>Veuillez entrer un mot de passe contenant des lettres, des chiffres ou les caractères suivants : !@#$%^*-=+?,_.</p>";
            reqNewPass.Text = "<p>Veuillez entrer votre nouveau mot de passe.</p>";
            regLenNewPass.Text = "<p>Le nouveau mot de passe doit compter au moins six (6) caractères</p>";
            regNewPass.Text = "<p>Veuillez entrer un mot de passe contenant des lettres, des chiffres ou les caractères suivants : !@#$%^*-=+?,_.</p>";
            reqPasswordConfirm.Text = "<p>Veuillez confirmer votre nouveau mot de passe.</p>";
            cmpPasswordConfirm.Text = "<p>Les deux champs du nouveau mot de passe doivent être identiques.</p>";
            //Forced ServSide message for wrong current password
            reqPassCheck.Text = "<p>Le mot de passe actuel est incorrect.</p>";
        }

        protected void change_Click(object sender, EventArgs e)
        {
            //is current password right?
            if (Membership.ValidateUser(HttpContext.Current.User.Identity.Name, textCurrent.Text))
            {
                if (textNew.Text != textNew2.Text)
                {
                    // new password fields must match
                    Page.Validate();

                }
                else if (textNew.Text.Length < 6)
                {
                    // new password must be at least 6 characters long.
                    Page.Validate();
                }
                else
                {
                    // Okay, let's change it.
                    //change password
                    MembershipUser u;
                    u = Membership.GetUser(HttpContext.Current.User.Identity.Name, false);
                    u.ChangePassword(textCurrent.Text, textNew.Text);
                    //show confirmation
                    formPanel.Visible = false;
                    confirmationPanel.Visible = true;
                }
            }
            else
            {
                // Hey wait the current PW is wrong...
                userpass.Text = "";
                Page.Validate();
                userpass.Text = "userpass";
            }
        }
    }
}
