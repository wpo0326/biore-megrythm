﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Net.Mail;
using KAOForms;
using Biore2012.DAL;

namespace Biore2012.Forms.RatingsAndReviews
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            initErrorMessages();

            if (Request.QueryString["return"] != null && Request.QueryString["return"] != "")
            {
                literalLoginButton.Text = "<a href=\"Login.aspx?return=Default.aspx?" + Server.UrlEncode(Request.QueryString["return"]) + "\" class=\"buttonLink\">Ouvrir une session</a>";
            }
            else
            {
                literalLoginButton.Text = "<a href=\"Login.aspx\" class=\"buttonLink\">Ouvrir une session</a>";
            }

        }
        private void initErrorMessages()
        {
            reqEmail.Text = "Veuillez entrer votre adresse de courriel.";
            regEmail.Text = "Veuillez entrer votre adresse de courriel valide.";
            //Forced ServSide message 
            reqUsername.Text = "<p>Nous n'avons pas cette adresse e-mail dans nos dossiers. S'il vous plaît essayez de nouveau.</p>";
        }
        protected void reset_Click(object sender, EventArgs e)
        {
            if (System.Web.Security.Membership.FindUsersByEmail(forgotPasswordUsername.Text).Count == 0)
            {
                // This username / Nickname is not in our database. 
                username.Text = ""; //force reqUsername validator
                Page.Validate();
                username.Text = "username";
            }
            else
            {
                String siteName = System.Configuration.ConfigurationManager.AppSettings.Get("siteName");
                String fromEmail = System.Configuration.ConfigurationManager.AppSettings.Get("resetPasswordFromEmail");
                String fromName = System.Configuration.ConfigurationManager.AppSettings.Get("resetPasswordFromName");
                String SMTPServer = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPServer");
                String loginLink = Request.Url.Host + System.Configuration.ConfigurationManager.AppSettings["path"] + "/FormConfig/Ratings-And-Reviews/Login.aspx";
                loginLink += "?return=|||" + Request.Url.Host + System.Configuration.ConfigurationManager.AppSettings["path"];
                loginLink = "http://" + loginLink.Replace("//", "/");
                loginLink = loginLink.Replace("|||", "http://");

                // Fetch user
                MembershipUser u;
                u = System.Web.Security.Membership.GetUser(forgotPasswordUsername.Text, false);

                if (u.IsLockedOut == false)
                {
                    String newPassword = string.Empty;
                    // http://msdn.microsoft.com/en-us/library/system.web.security.sqlmembershipprovider.generatepassword%28v=VS.90%29.aspx
                    // The standard SqlMembershipProvider.GeneratePassword Method that ResetPassword calls would make a generated password that only
                    //  contains alphanumeric characters and the following punctuation marks: !@#$%^&*()_-+=[{]};:<>|./?
                    // Unfortunately we do not allow some characters such as < & >.  Also some end users have trouble finding some of these special
                    //  punctuation marks so we will override the GeneratePassword Method in ExtendedSqlMembershipProvider.cs Class and configure to 
                    //  use this in the Web.config
                    newPassword = u.ResetPassword();

                    //send email
                    //Response.Write("Look here " + newPassword + " " + u.Email);
                    String emailBody = "Votre mot de passe Bioré<sup>MD</sup>a été modifié: " + newPassword + "<br/><br/>Vous pouvez maintenant ouvrir une session à l’aide de votre nouveau mot de passe: <a href=\"" + loginLink + "\">" + loginLink + "</a><br/><br/>Merci.<br/><br/>" + siteName;
                    String emailBodyPlain = "Votre mot de passe Bioré<sup>MD</sup>a été modifié: " + newPassword + "\n\nVous pouvez maintenant ouvrir une session à l’aide de votre nouveau mot de passe: " + loginLink + "\n\nMerci.\n\n" + siteName;
                    /* EmailDAO emDAO = new EmailDAO();

                    
                     List<ExTargetAttribute> etAttributes = new List<ExTargetAttribute>();

                     ExTargetAttribute newAttribute = new ExTargetAttribute();
                     newAttribute.Name = "Password";
                     newAttribute.Value = newPassword;
                     etAttributes.Add(newAttribute);

                     ExTargetAttribute anotherNewAttribute = new ExTargetAttribute();
                     anotherNewAttribute.Name = "LoginURL";
                     anotherNewAttribute.Value = loginLink;
                     etAttributes.Add(anotherNewAttribute);

                     emDAO.sendEmail(fromEmail, fromName, u.Email, u.Email, "", etAttributes, "JFPasswordReset");
                     */

                    //// SMTP EMAIL SEND BLOCK
                    /*MailMessage message = new MailMessage(
                      fromEmail,
                      u.Email,
                      "Your John Frieda(R) Password Has Been Reset",
                      emailBody);
                    SmtpClient client = new SmtpClient(SMTPServer, 25);
                    message.IsBodyHtml = true;
                    client.Send(message);
                    */

                    CGMail.MultiPartMime(fromEmail, fromName, u.Email, u.Email, "Votre mot de passe Bioré<sup>MD</sup>a été modifié", emailBodyPlain, emailBody);


                    formPanel.Visible = false;
                    confirmationPanel.Visible = true;
                }
                else
                {
                    formPanel.Visible = false;
                    lockedOutPanel.Visible = true;
                }
            }
        }
    }
}
