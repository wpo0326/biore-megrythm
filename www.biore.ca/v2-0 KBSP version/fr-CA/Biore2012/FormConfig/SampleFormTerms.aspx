﻿<%@ Page Title="Bior&eacute;&reg; Skincare | Rules" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SampleFormTerms.aspx.cs" Inherits="Biore2012.FormConfig.SampleTerms" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Bioré® rules" name="description" />
    <meta content="" name="keywords" />
 <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedrules_#.css")
    %>
    <style type="text/css">

        

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <div id="content-sample-form-terms">
                    <h2>
        Conditions de <a name="_Hlk405884201">l’offre promotionnelle d’échantillons d’essai de produits au charbon</a> de Bioré<sup>MD</sup> (les « conditions
        »). À LIRE ATTENTIVEMENT.
    </h2>
<p>
    <strong>L’offre est réservée aux résidents du Canada et EST RÉGIE par les lois du Canada.</strong>
</p>
<p>
    <strong>A. Aperçu</strong>
</p>
<p>
    L’offre promotionnelle d’échantillons d’essai <a name="_Hlk405884271">de produits </a>au charbon de Bioré<sup>MD</sup> (l’<strong>« offre »</strong>) est
    commanditée par Kao Canada inc. (le <strong>« commanditaire »</strong>). Aucun achat n’est exigé pour y participer. Les renseignements que vous divulguez
    ne sont utilisés qu’aux fins de la gestion de l’offre, et ce, conformément à la politique de confidentialité du commanditaire (voir ci-dessous). Les
questions, plaintes ou commentaires concernant l’offre doivent être adressés au commanditaire. Vous ne pouvez utiliser qu’un seul compte (le    <strong>« compte »</strong>) pour participer à l’offre. Aucun achat n’est exigé. Toutefois, l’accès à Internet et un compte sont indispensables pour
    participer à l’offre. Un grand nombre de bibliothèques publiques, de commerces de détail et d’autres établissements mettent gratuitement à la disposition
    du public des ordinateurs permettant l’accès à Internet; de plus, certains fournisseurs de services Internet et d’autres entreprises proposent des comptes
    de courriel gratuits.
</p>
<p>
    <strong>B. Admissibilité</strong>
</p>
<p>
    L’offre s’adresse exclusivement aux résidents canadiens autorisés qui, au moment de leur participation, sont majeurs selon les lois de leur province ou de
    leur territoire de résidence, à l’exception des employés, des représentants ou des agents (ainsi que des personnes vivant sous le même toit que ceux-ci,
    qu’elles soient ou non parentes avec eux) du commanditaire, de ses sociétés mères, de ses filiales, de ses sociétés affiliées, de ses agences de publicité
    ou de promotion, de ses représentants ou de ses agents (collectivement : les <strong>« parties liées à l’offre »</strong>). Pour éviter toute ambiguïté :
    aucune personne ayant été au service de l’une des parties liées à l’offre le 1<sup>er</sup> janvier 2015 ou par la suite n’est admissible à l’offre.
    Quiconque vit sous le même toit qu’une telle personne, qu’il soit ou non parent avec celle-ci, n’est pas non plus admissible à l’offre. La moindre
    tentative d’un participant de contrevenir à la présente disposition relative à l’admissibilité peut, à la seule et entière discrétion du commanditaire,
    entraîner sa disqualification. L’offre ne s’adresse qu’aux personnes physiques. Les sociétés, associations ou autres groupes ne peuvent y participer.
</p>
<p>
    <strong>C. Durée de l’offre</strong>
</p>
<p>
    L’offre débute à 12 h (midi), heure de l’Est (<strong>« HE »</strong>), le 16 janvier 2015 et prend fin à 17 h, HE, le 30 janvier 2015 ou dès l’épuisement
des stocks officiels d’ensembles d’échantillons (au sens précisé ci-dessous), selon la première de ces éventualités (la    <strong>« durée de l’offre »</strong>).
</p>
<p>
    <strong>D. Comment participer</strong>
</p>
<p>
    AUCUN ACHAT N’EST REQUIS. Pour participer à l’offre, vous devez accéder à la page Web (http://www.biore.ca/fr-ca/formconfig/sampleform.aspx) (la <strong>« page Web »</strong>) pendant la durée de
l’offre, suivre les consignes affichées pour réclamer un ensemble d’échantillons en remplissant intégralement le formulaire de réclamation en ligne (le    <strong>« formulaire de réclamation »</strong>), en y indiquant entre autres votre prénom, votre nom, votre date de naissance, votre adresse (ville,
    province ou territoire et code postal compris), ainsi que votre adresse de courriel. Après avoir dûment rempli le formulaire de réclamation, vous devez
    soumettre celui-ci, à savoir votre <strong>« réclamation »</strong>, conformément aux directives affichées. Pour être valide, votre réclamation doit être
    soumise et reçue pendant la durée de l’offre.
</p>
<p>
    <strong>E. Limite de demandes de réclamation</strong>
</p>
<p>
    Une (1) seule réclamation par personne, par adresse de courriel et par compte est autorisée pendant la durée de l’offre. Pour éviter toute ambiguïté : vous
    ne pouvez utiliser qu’une (1) seule adresse de courriel et qu’un (1) seul compte pour participer à l’offre. S’il est établi qu’une personne : i) a tenté de
    soumettre plus d’une (1) réclamation par personne, par adresse de courriel et par compte pendant la durée de l’offre ou ii) a utilisé ou tenté d’utiliser
    plus d’un (1) nom, d’une (1) identité, d’une (1) adresse de courriel ou d’un (1) compte pour participer à l’offre, le commanditaire peut, à sa seule et
    entière discrétion, disqualifier cette personne, dont toutes les réclamations sont annulées le cas échéant. Il est interdit, sous peine de disqualification
    par le commanditaire, d’utiliser ou de tenter d’utiliser plus d’un (1) nom, d’une (1) identité ou d’une (1) adresse de courriel ou encore quelque macro,
    script ou encore système ou programme automatisé ou robotique que ce soit pour participer à l’offre ou dans le but de perturber celle-ci. Les parties
    exonérées (au sens précisé ci-dessous) ne peuvent être tenues responsables des demandes de réclamation tardives, perdues, incorrectement acheminées,
    retardées, incomplètes ou inadmissibles, qui sont toutes annulées.
</p>
<p>
    <strong>F. Ensembles d’échantillons </strong>
</p>
<p>
    Au plus, cinq mille (5 000) ensembles d’échantillons sont susceptibles d’être offerts pendant la durée de l’offre, chaque ensemble contenant un (1) pain
désincrustant au charbon pour les pores de Bioré<sup>MD</sup>, de 19 g, et une (1) bande de nettoyage en profondeur des pores au charbon de Bioré    <sup>MD</sup> (collectivement, l’« ensemble d’échantillons »). La valeur au détail approximative de chaque ensemble d’échantillons est de 2 $ CAN.
</p>
<p>
    <strong>G. Expédition des ensembles d’échantillons</strong>
</p>
<p>
    Une fois que vous êtes proclamé gagnant d’un ensemble d’échantillons, ce dernier vous sera expédié par la poste à l’adresse indiquée sur votre formulaire
    de réclamation. Veuillez prévoir de quatre (4) à six (6) semaines avant que votre ensemble d’échantillons ne parvienne à l’adresse que vous avez indiquée
    sur votre formulaire de réclamation. Si, huit (8) semaines après avoir été proclamé gagnant conformément aux présentes modalités, vous n’avez toujours pas
    reçu votre ensemble d’échantillons, veuillez le signaler par courriel à <a href="mailto:bioreskincarelistens@biore.ca">bioreskincarelistens@biore.ca</a>.
    Aucun ensemble d’échantillons perdu ou volé ne sera remplacé.
</p>
<p>
    <strong>H. Modalités générales</strong>
</p>
<p>
    Les modalités générales suivantes s’appliquent à l’offre :
</p>
<ol start="1" type="1">
    <li>
        Les présentes modalités sont nulles là où elles sont interdites par la loi. L’offre est nulle là où elle est interdite, imposable ou réglementée ou
        encore là où un enregistrement s’impose. L’ensemble des lois fédérales et provinciales ainsi que des règlements fédéraux, provinciaux et municipaux
        s’appliquent.
    </li>
    <li>
        Le commanditaire se réserve le droit d’annuler, d’interrompre ou de modifier l’offre si une fraude, un virus, des problèmes techniques ou tout autre
        facteur viennent en perturber la gestion.
    </li>
    <li>
        Tout usage de moyens automatisés ou de programmes pour soumettre une réclamation en ligne est strictement interdit.
    </li>
    <li>
        Le commanditaire n’est pas responsable des erreurs de communication, des virus, des vers ou des autres éléments contaminants susceptibles de toucher ou
        d’endommager l’ordinateur ou les données d’un participant lors de sa participation à l’offre.
    </li>
    <li>
        Sauf là où cela est interdit par la loi, en remplissant le formulaire de réclamation, tout participant dégage le commanditaire, les filiales et les
        sociétés affiliées de celui-ci ainsi que leurs actionnaires, administrateurs, dirigeants, employés, représentants ou agents respectifs de toute
        responsabilité liée aux pertes ou aux dommages de quelque nature que ce soit, directs, indirects ou punitifs, découlant de l’offre.
    </li>
    <li>
        En prenant part à l’offre, chaque participant autorise expressément le commanditaire, ses agents et (ou) ses représentants à stocker, à divulguer et à
        exploiter les renseignements personnels soumis dans le formulaire de réclamation, mais uniquement aux fins de la gestion de l’offre et conformément à
la politique de confidentialité du commanditaire accessible au        <a href="http://www.kaobrands.com/privacy_policy.asp">http://www.kaobrands.com/privacy_policy.asp</a>, sauf si le participant accepte qu’il en soit
        autrement.
    </li>
    <li>
        Sauf là où cela est interdit par la loi, en remplissant le formulaire de réclamation, tout participant convient que les parties liées à l’offre ne sont
        pas responsables : (a) de la moindre information incorrecte ou inexacte, que ce soit par la faute du participant ou en raison d’erreurs d’impression;
        (b) de la moindre intervention humaine non autorisée touchant quelque aspect de l’offre que ce soit; (c) des erreurs techniques ou humaines
        susceptibles d’entacher la gestion de l’offre ou le processus de réclamation; ou (d) des dommages corporels ou matériels susceptibles découlant
        directement ou indirectement, en tout ou en partie, de la participation du participant à l’offre ou encore de la réception, de l’usage ou du mésusage
        d’un ensemble d’échantillons.
    </li>
    <li>
        L’offre ne s’adresse qu’aux personnes physiques. Les sociétés, associations ou autres groupes ne peuvent y participer. Une personne physique, une
        société, une association ou un groupe qui demande à des personnes d’utiliser un compte unique afin d’accumuler des réclamations en vue d’en faire un
        usage combiné, ou encore qui les y encourage ou les y autorise, commet un acte frauduleux.
    </li>
</ol>
<br/>
<p>
    <strong>I. Autres modalités</strong>
</p>
<p>
    En cas de divergence ou d’incompatibilité entre les modalités de la version anglaise des présentes modalités et les déclarations ou autres mentions
    figurant dans tout autre document lié à l’offre, y compris le formulaire de réclamation, la page Web et la présente version française ou la moindre
    publicité au sein de points de vente, à la télévision, sur papier ou en ligne, les modalités de la version anglaise des présentes modalités ont préséance.
</p>



                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
