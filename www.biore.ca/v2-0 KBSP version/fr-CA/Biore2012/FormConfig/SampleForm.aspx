﻿<%@ Page Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="SampleForm.aspx.cs"
    Inherits="Biore2012.FormConfig.SampleForm" %>
<%@ Register TagPrefix="skm" Namespace="skmValidators" Assembly="skmValidators" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%#SquishIt.Framework.Bundle .Css()
        .Add("~/css/formConfig.css")
        .Render("~/css/combinedform_#.css")
    %>
    
    <style type="text/css">

        body {
            background:#90c84b url(/fr-ca/images/forms/sampleformbg.jpg) repeat-x top left;
            color:#19224c;
        }

        .forms #mainContent {
            background-image: none !important;
        }

        .responsive #contactFormWrap .SubmitContainerSample{
        clear: left;
	    overflow:hidden;
	    padding: 12px 20px 0px 0px;
    }

    .responsive .MemberInfoContainer {
        margin-top: 10px;
    }

    .responsive .req {
        color: #404041;
    }

    .responsive #contactFormWrap .SubmitContainerSample input{
        background-color:#19224d;
        border: 0 none;
        color: #fff;
        cursor: pointer;
        display: block;
        font-size: 12px;
        width: 165px;
        height: 47px;
        text-align: center;
        text-decoration: none;
        text-transform: uppercase;
        margin: 18px auto;
    
    }

    .notouch .responsive #contactFormWrap .SubmitContainerSample input:hover{
        background-position:bottom left;
    }

        #ctl00_ContentPlaceHolder1_ucForm_PageHeader {
            text-indent:-9999px;
        }

        .responsive #contactFormWrap .FormBottom {
            position: absolute;
            bottom: -21px;
            z-index: -1;
            left: 0px;
            padding-bottom: 0;
            margin: 0;
            width: 100%;
            height: 290px;
            background: transparent url(/fr-CA/images/forms/sampleform-splotch.png) no-repeat top left;
        }

    @media screen and (max-width: 640px) {
        .responsive #contactFormWrap .FormBottom {
            background:none !important;
        }
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow">
            </div>
            <div class="centeringDiv">
                <div class="responsive contact">
                    <div id="contactFormWrap">
                        <div id="BrandImageContainer" class="png-fix">
                            <img id="ImageHeader" class="png-fix" src="../images/forms/sampleform-bottle-fr.png" style="border-width: 0px;margin-top: 15%" runat="server" />
                        </div>
                        <div id="ContactFormContainer">
                            <asp:Panel ID="ContactDetails" runat="server">
                                <!-- Header -->
                                <div id="formHeader">
                                    <h1 id="ctl00_ContentPlaceHolder1_ucForm_PageHeader">
                                        INTRODUCING CHARCOAL. WEARING BLACK ISN’T ONLY SLIMMING, IT’S CLEANSING
                                    </h1>
                                    <!--<img src="/en-ca/images/forms/sampleform-header.png" class="png-fix" style="width:100%" />-->
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="OptinForm" runat="server">
                                <!-- Description -->
                                <div class="DescriptionContainer png-fix">
                                    <h2>MESSAYEZ NOS NOUVEAUX PRODUITS AU CHARBON AVANT LES AUTRES!</h2>
Remplissez le formulaire d’inscription ci-dessous pour demander des échantillons d’essai exclusifs de notre nouveau pain désincrustant au charbon et de nos nouvelles bandes de nettoyage en profondeur au charbon pour les pores!

                                    
                                </div>
                                <!-- From Fields -->
                                <div class="FormContainer">
                                    <p class="req png-fix">
                                        <em>Champ obligatoire*</em></p>
                                    <div class="MemberInfoContainer png-fix">
                                        <div class="NameWrapper">
                                            <div class="FirstNameContainer Question">
                                                <div class="mmSweet">
                                                    <asp:Label ID="FNameLbl" runat="server" Text="First Name*" AssociatedControlID="FrstName"></asp:Label>
                                                    <asp:TextBox ID="FrstName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="FNameLbl1" runat="server" Text="Prénom*" AssociatedControlID="FName"></asp:Label>
                                                <asp:TextBox ID="FName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="FNameValidator1" runat="server" ErrorMessage="Veuillez entrer votre prénom."
                                                        ControlToValidate="FName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="FNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your First Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="FName" EnableClientScript="true"
                                                        SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="LastNameContainer Question">
                                                <asp:Label ID="LNameLbl" runat="server" Text="Nom*" AssociatedControlID="LName"></asp:Label>
                                                <asp:TextBox ID="LName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="LNameValidator1" runat="server" ErrorMessage="Veuillez entrer votre nom."
                                                        ControlToValidate="LName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="LNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Last Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="LName" SetFocusOnError="true"
                                                        Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="EmailWrapper">
                                            <div class="EmailContainer Question">
                                                <asp:Label ID="EmailLbl" runat="server" Text="Courriel*" AssociatedControlID="Email"></asp:Label>
                                                <asp:TextBox ID="Email" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="EmailValidator1" runat="server" ErrorMessage="Veuillez entrer votre adresse de courriel."
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="EmailValidator2" runat="server" ErrorMessage="Please enter a valid Email Address."
                                                        ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                             <div class="ConfirmEmailContainer Question">
                                                <asp:Label ID="RetypeEmailLbl" runat="server" Text="Confirmation du courriel*" AssociatedControlID="RetypeEmail"></asp:Label>
                                                <asp:TextBox ID="RetypeEmail" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="RetypeValidator1" runat="server" ErrorMessage="Veuillez confirmer votre adresse de courriel."
                                                        ControlToValidate="RetypeEmail" EnableClientScript="true" SetFocusOnError="true"
                                                        Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="RetypeValidator2" Text="Make sure you retyped the Email correctly."
                                                        ControlToValidate="RetypeEmail" ControlToCompare="Email" EnableClientScript="true"
                                                        SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelAddress" class="AddressContainer">
                                            <div class="AddressWrapper">
                                                <div class="Address1Container Question">
                                                    <asp:Label ID="Address1Lbl" runat="server" Text="Adresse 1*" AssociatedControlID="Address1"></asp:Label>
                                                    <asp:TextBox ID="Address1" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="Address1Validator1" runat="server" ErrorMessage="Veuillez entrer votre adresse."
                                                            ControlToValidate="Address1" EnableClientScript="true" SetFocusOnError="true"
                                                            Display="Dynamic" CssClass="errormsg" />
                                                        <asp:RegularExpressionValidator ID="Address1Validator2" runat="server" ErrorMessage="Please do not use special characters in the Address 1 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address1" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="Address2Container Question">
                                                    <asp:Label ID="Address2Lbl" runat="server" Text="Adresse 2" AssociatedControlID="Address2"></asp:Label>
                                                    <asp:TextBox ID="Address2" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="Address2Validator1" runat="server" ErrorMessage="Please do not use special characters in the Address 2 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address2" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="CityContainer Question">
                                                    <asp:Label ID="CityLbl" runat="server" Text="Ville*" AssociatedControlID="City"></asp:Label>
                                                    <asp:TextBox ID="City" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="CityValidator1" runat="server" ErrorMessage="Veuillez entrer votre ville."
                                                            ControlToValidate="City" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                            CssClass="errormsg" />
                                                        <asp:RegularExpressionValidator ID="CityValidator2" runat="server" ErrorMessage="Please do not use special characters in the City field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="City" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="StateContainer Question">
                                                <asp:Label ID="StateLbl" runat="server" Text="Province*" AssociatedControlID="State"></asp:Label>
                                                <asp:DropDownList ID="State" runat="server">
                                                    <asp:ListItem Value="" Selected="true">sélectionner la province</asp:ListItem>
                                                    <asp:ListItem Value="1">Alberta</asp:ListItem>
                                                    <asp:ListItem Value="2">British Columbia</asp:ListItem>
                                                    <asp:ListItem Value="3">Manitoba</asp:ListItem>
                                                    <asp:ListItem Value="4">New Brunswick</asp:ListItem>
                                                    <asp:ListItem Value="5">Newfoundland</asp:ListItem>
                                                    <asp:ListItem Value="6">Northwest Territories</asp:ListItem>
                                                    <asp:ListItem Value="7">Nova Scotia</asp:ListItem>
                                                    <asp:ListItem Value="8">Nunavut</asp:ListItem>
                                                    <asp:ListItem Value="9">Ontario</asp:ListItem>
                                                    <asp:ListItem Value="10">Prince Edward Island</asp:ListItem>
                                                    <asp:ListItem Value="11">Quebec</asp:ListItem>
                                                    <asp:ListItem Value="12">Saskatchewan</asp:ListItem>
                                                    <asp:ListItem Value="13">Yukon</asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="StateValidator" runat="server" ErrorMessage="Veuillez sélectionner votre province."
                                                        ControlToValidate="State" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div class="PostalCodeContainer Question">
                                            <asp:Label ID="PostalCodeLbl" runat="server" Text="Code postal*" AssociatedControlID="PostalCode"></asp:Label>
                                            <asp:TextBox ID="PostalCode" runat="server" MaxLength="20" CssClass="inputTextBox"></asp:TextBox>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="PostalCodeValidator1" runat="server" ErrorMessage="Veuillez entrer votre code postal."
                                                    ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true"
                                                    Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                
                                            </div>
                                        </div>
                                        <div class="CountryContainer Question">
                                            <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_CountryLbl">Pays
                                            </span><span class="fake_input">Canada</span>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelLanguage" class="GenderContainer Question">
                                            <asp:Label ID="LanguageLbl" runat="server" Text="Langue de correspondance préférée*" AssociatedControlID="PreferredLanguage"></asp:Label>
                                            <asp:DropDownList ID="PreferredLanguage" runat="server">
                                                <asp:ListItem Value="">sélectionner</asp:ListItem>
                                                <asp:ListItem Value="EN">English</asp:ListItem>
                                                <asp:ListItem Value="FR">French</asp:ListItem>
                                            </asp:DropDownList>
                                            
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelGender" class="GenderContainer Question">
                                            <asp:Label ID="GenderLbl" runat="server" Text="Gender" AssociatedControlID="Gender"></asp:Label>
                                            <asp:DropDownList ID="Gender" runat="server">
                                                <asp:ListItem Value="">sélectionner</asp:ListItem>
                                                <asp:ListItem Value="F">FEMME</asp:ListItem>
                                                <asp:ListItem Value="M">HOMME</asp:ListItem>
                                            </asp:DropDownList>
                                            
                                        </div>
                                        <div class="BirthdayContainer Question">
                                            <asp:Label ID="DOBLbl" runat="server" Text="Date de naissance*" AssociatedControlID="yyyy"></asp:Label>
                                            <asp:DropDownList ID="yyyy" runat="server" CssClass="inputSelectBDay year">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="mm" runat="server" CssClass="inputSelectBDay month">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="dd" runat="server" CssClass="inputSelectBDay day">
                                            </asp:DropDownList>
                                            <br />
                                            <div style="padding-left:100px;font-size:10px;font-style:italic">Vous devez être âgé de 13 as et plus pour vous inscrire</div>
                                            <div class="ErrorContainer">
                                                <asp:CustomValidator ID="DOBValidator" runat="server" ErrorMessage="Please enter a valid date."
                                                    OnServerValidate="DOBValidate" EnableClientScript="false" Display="Dynamic" CssClass="errormsg validdate"></asp:CustomValidator>
                                                <asp:RequiredFieldValidator ID="yyyyValidator" runat="server" ErrorMessage="Veuillez entrer votre année de naissance."
                                                    ControlToValidate="yyyy" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg year"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="mmValidator" runat="server" ErrorMessage="Veuillez entrer votre mois de naissance."
                                                    ControlToValidate="mm" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg month"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="ddValidator" runat="server" ErrorMessage="Veuillez entrer votre jour de naissance."
                                                    ControlToValidate="dd" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg day"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="seperator png-fix">
                                        </div>
                                    </div>           
           
                                    <div class="OptinContainer">
                                        <div class="seperator png-fix"></div>
	                                    <p class="privacy">
		                                    Le respect de votre vie privée nous tient à cœur. Soyez assuré que Kao Canada inc., utilisera vos renseignements personnels conformément à sa <a href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">politique de renseignements personnels</a>.
	                                    </p>
        
                                         <ul>
		                                        <li>
							                <asp:CheckBox Checked="false" ID="bioreTerms" runat="server" />
							                <asp:Label ID="Label1" runat="server" Text="I have read the <a href='/fr-CA/FormConfig/sampleformterms.aspx' target='_blank'>Terms &amp; Conditions</a>."
								                AssociatedControlID="bioreTerms" />
							               <skm:CheckBoxValidator ID="cbv_bioreTerms" runat="server" ControlToValidate="bioreTerms" ErrorMessage="<br />Veuillez confirmer que vous avez lu <a href='/fr-CA/FormConfig/sampleformterms.aspx' target='_blank'>les conditions générales</a> en cochant cette case." CssClass="errormsg" Display="Dynamic"></skm:CheckBoxValidator>
                                                </li>
                                            </ul>

                                    </div> 

                                    <div id="submit-container" class="SubmitContainerSample png-fix">
                                        <asp:Button ID="Button1" runat="server" CssClass="submit png-fix" Text="SUBMIT ›" OnClick="Step1_ButtonClick"  />
                                    </div>
                                    <asp:HiddenField ID="hfHookID" runat="server" />

                                    <!-- Disclaimer -->
                                    <div id="DisclaimerContainer" class="png-fix">
                                         <p>† Aucun achat n’est requis. L’offre débute le 16 janvier 2015 à 12 h (midi), HE, et prend fin le 30 janvier 2015 à 17 h, HE, ou dès l’épuisement des stocks officiels d’ensembles d’échantillons, selon la première de ces éventualités. Le concours s’adresse aux résidents canadiens autorisés et majeurs. Participation en ligne et modalités complètes au (http://www.biore.ca/fr-CA/formconfig/sampleform.aspx). Un maximum de cinq mille (5 000) ensembles d’échantillons sont susceptibles d’être offerts (valeur au détail approximative : 2 $ chacun). 
 
  </p>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="OptinFormResults" runat="server">
                                 <div class="ConfrimationContainer"><h2>Thank You!</h2><p>Thank you for signing up, we look forward to hearing what you think about our new products!  Your package should arrive within 4-6 weeks.</p></div>  

                            </asp:Panel>
                            <asp:Panel ID="OptinError" runat="server">
                                An error has occurred while attempting to submit your information.   <asp:Literal ID="litError" runat="server"></asp:Literal>
                            </asp:Panel>
                            <asp:Panel ID="OptinEventExpired" runat="server">
                               Sorry, this promotion has expired. <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                            </asp:Panel>
                            <div class="FormBottom png-fix">
                            </div>
                            
                        </div>
                    </div>

                    <script type="text/javascript">

                    </script>

                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script type="text/javascript">
        $(function() {

            if ($(".signUp").length) {
                toggle("noneToggle1");
                toggle("noneToggle2");
                toggle("noneToggle3");
                toggle("noneToggle4");
            }

            $("#ctl00_ContentPlaceHolder1_cureloptin, #ctl00_ContentPlaceHolder1_jergensoptin").click(function() {
                if ($('#ctl00_ContentPlaceHolder1_cureloptin:checked').val()
                    || $('#ctl00_ContentPlaceHolder1_jergensoptin:checked').val()) 
                {
                        $('#ctl00_ContentPlaceHolder1_MultiOptin').attr("checked", "true");
                }
                else { $('#ctl00_ContentPlaceHolder1_MultiOptin').removeAttr("checked"); }
            });
            $("#ctl00_ContentPlaceHolder1_MultiOptin").click(function() {
                if ($('#ctl00_ContentPlaceHolder1_MultiOptin:checked').val()) {
                    $("#ctl00_ContentPlaceHolder1_cureloptin").attr("checked", "true");
                    $("#ctl00_ContentPlaceHolder1_jergensoptin").attr("checked", "true");
                } else {
                    $("#ctl00_ContentPlaceHolder1_cureloptin").removeAttr("checked");
                    $("#ctl00_ContentPlaceHolder1_jergensoptin").removeAttr("checked");
                }
            });

            /*// $(this).prop('checked', false); */
            //Rank Order handling.

            $(".RankHolders input").click(function () {
                rankOrderHandler(".RankHolders input", this);
            });

            $(".RankHolders2 input").click(function () {
                rankOrderHandler(".RankHolders2 input", this);
            });

        });

        function rankOrderHandler(rankClass, inputId) {
            var RankerString = $(inputId).attr('id');
            var RankerCheckedNum = RankerString.substr(RankerString.length - 1);
            var CompareString;
            var CompareNum;

            $(rankClass).each(function () {
                CompareString = $(this).attr('id');
                CompareNum = CompareString.substr(CompareString.length - 1);
                if (CompareString != RankerString && CompareNum == RankerCheckedNum) {
                    if ($("#" + CompareString).attr("checked")) {
                        $("#" + CompareString).attr("checked", false);
                    }
                }
             })
        }
        
        function toggle(className) {
            var checkToggle = $("." + className + " input:last");
            checkToggle.click(function() {
                if ($("#" + checkToggle.attr('id') + ":checked").val()) {
                    $("." + className + " input").each(function() {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).attr("checked", false);
                            $(this).attr('disabled', 'disabled');
                        }
                    });
                } else {
                    $("." + className + "  input").each(function() {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).removeAttr('disabled');
                        }
                    });
                }
            });
        }
    </script>

</asp:Content>
