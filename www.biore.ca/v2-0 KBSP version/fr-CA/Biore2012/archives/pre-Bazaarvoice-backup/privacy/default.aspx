﻿<%@ Page Title="politique de protection des renseignements personnels | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.privacy._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1>politique de protection des renseignements personnels</h1>
                 <p>À Kao Canada inc. (« Kao Canada Inc. ») le respect de votre vie privée nous tient à cœur. Kao Canada Inc. prend toutes les mesures raisonnables pour respecter votre vie privée. Notre site Web est conçu de façon que, en général, vous n’ayez pas à vous identifier ou à révéler quelque renseignement personnel que ce soit (renseignements permettant de vous identifier ou de communiquer avec vous, comme votre nom, votre adresse postale, votre numéro de téléphone, votre adresse de courriel ou tout renseignement de même nature). Si vous choisissez de nous fournir des renseignements personnels, soyez assurés qu'ils ne seront utilisés que conformément aux principes énoncés ci-dessous. </p><br />


<p><strong>Nous respectons le droit à la protection des renseignements personnels</strong></p><br />

<p>Kao Canada Inc. se conforme aux exigences juridiques concernant la protection des renseignements personnels ainsi que toute loi en la matière. Kao Canada Inc. révise, de temps à autre, ses pratiques de collecte, d’utilisation et de diffusion de renseignements personnels afin de s’assurer qu’elles se conforment aux lois et réglements en vigueur.</p><br />

<p><strong>Renseignements personnels recueillis<br /></strong><br />Sur certaines pages du site Web de Kao Canada Inc., vous pouvez communiquer avec nous ou accéder à notre carnet de visite. Les renseignements personnels recueillis sur ces pages peuvent comprendre le nom, l’adresse postale et l’adresse de courriel.</p><br />

<p>Afin de personnaliser nos futures communications selon vos besoins et de continûment améliorer nos produits et services, nous pourrions être amenés à vous demander de nous fournir des renseignements concernant vos intérêts personnels et professionnels, des renseignements de nature démographique, des commentaires sur nos produits, ou encore des précisions sur la façon dont vous préférez que nous communiquions avec vous.</p><br />

<p>Nous ne recueillons aucun renseignement personnel autre que ceux que vous nous fournissez. Par contre, sachez que vous nous fournissez volontairement des renseignements quand vous nous envoyez un courriel ou répondez à une question sur le site. Ne nous envoyez aucun renseignement personnel si vous ne voulez pas que nous les enregistrions dans notre base de données.</p><br />

<p>Le site Web auquel vous avez accédé peut également recueillir des renseignements personnels concernant un tiers (par exemple, le site vous permet d’envoyer un courriel à un ami et nous vous demandons le nom et l’adresse de courriel de cette personne). Veuillez prendre note que nous n’utilisons ces renseignements qu’aux fins précisées (envoi d’un courriel) et que nous n’envoyons aucun autre courriel à ladite personne que si celle-ci communique avec nous et nous en donne la permission.</p><br />

<p><strong>Utilisation de vos renseignements<br /></strong><br />De temps à autre, Kao Canada Inc. utilise vos renseignements pour communiquer avec vous dans le cadre d’une étude de marché ou pour vous fournir de l’information de nature commerciale pouvant vous intéresser. Dans tous les cas, vous pouvez retirer votre nom des listes d’envoi utilisées à ces fins.</p><br />

<p>Kao Canada Inc. peut échanger les renseignements personnels que vous avez fournis en ligne avec d’autres divisions ou sociétés affiliées de Kao Canada Inc. ou avec ses partenaires, qui agissent en son nom ou qui offrent des produits et services susceptibles de vous intéresser, incluant le transfert des renseignements personnels à d’autres pays. Si des renseignements personnels sont transférés à des tiers tel qu'il a été précisé ci-dessus, ceux-ci sont tenus, en vertu d’un contrat, de respecter les modalités de la présente politique de protection des renseignements personnels et de vous aviser quand des renseignements sont recueillis. Lorsque la loi le lui permet ou qu’une injonction l’y oblige, Kao Canada Inc. se réserve le droit de recueillir, d'utiliser et de divulguer vos renseignements personnels dans le cadre de procédures ne nécessitant l’émission d’aucun avis ou toute autre activité connexe (par exemple, dans le cadre d’une enquête policière).</p><br />

<p>En cas de vente, de fusion, de liquidation, de dissolution, de réorganisation et d’acquisition de Kao Canada Inc. ou d’une unité commerciale de Kao Canada Inc., la société peut vendre ou transférer les renseignements personnels recueillis à votre sujet. Le cas échéant, l’acquéreur doit toutefois s’engager à respecter les modalités de la politique de protection des renseignements personnels de Kao Canada Inc. et de l'avis qui vous a été envoyé lors de la collecte desdits renseignements.</p><br />

<p><strong>Choix<br /></strong><br />Kao Canada Inc. n’utilise ni ne partage les renseignements personnels fournis en ligne à des fins autres que celles décrites ci-dessus qu’après vous en avoir informé et vous avoir donné la possibilité de refuser cet usage. Vous pouvez également nous informer que vous ne désirez pas recevoir d’envois de marketing direct non sollicités et, le cas échéant, nous déployons tous les efforts possibles pour respecter votre choix. Il n’est pas de notre responsabilité de retirer vos renseignements personnels des listes d’envoi de tout tiers (par exemple, un partenaire d’affaires) qui nous a fourni lesdits renseignements conformément aux modalités de la présente politique.</p><br />

<p><strong>Information agrégée recueillie<br /></strong><br />Nous pouvons également collecter des renseignements non personnels et regrouper ces renseignements sans identifier une personne en particulier.</p><br />

<p>Les témoins sont des éléments d’information qu’un site Web place sur votre ordinateur afin de faciliter et d'améliorer la communication et l’interaction avec le site. De nombreux sites Web, incluant les nôtres, utilisent les témoins à cette fin. Vous pouvez bloquer ou restreindre l'installation de témoins sur votre ordinateur ou encore supprimer ces derniers de votre navigateur en le configurant en conséquence. Le cas échéant, vous pourrez continuer à visiter notre site, mais sachez que cela pourrait interférer avec certaines fonctionnalités.</p><br />

<p>Quand vous naviguez sur Internet, vous laissez des traces électroniques sur chaque site que vous visitez. Cette information, aussi appelée parcours de navigation, peut être recueillie et enregistrée par un serveur de site Web comme le nôtre. Le parcours de navigation nous indique le type d’ordinateur et de navigateur que vous utilisez ainsi que l’adresse du site à partir duquel vous avez accédé à notre site.</p><br />

<p><strong>Utilisation de l’information agrégée<br /></strong><br />Nous pouvons utiliser des témoins afin de personnaliser votre visite de notre site et rendre l’expérience plus agréable, ou encore pour améliorer nos services. Nous n’utilisons pas les témoins pour recueillir des renseignements personnels à des fins non liées à l’utilisation de notre site Web.</p><br />

<p>Il peut arriver que nous recueillions et utilisions les données issues du parcours de navigation à titre d’information agrégée afin de déterminer combien de temps les visiteurs passent dans chaque page de notre site, comment ces derniers naviguent dans le site et comment nous pouvons modifier nos pages Web en fonction de leurs besoins. De même, nous pouvons utiliser ces renseignements pour améliorer notre site et nos services. Toute donnée de parcours de navigation recueillie ou utilisée demeure anonyme et agrégée et ne contient pas intentionnellement de renseignements personnels.</p><br />

<p><strong>Sécurité<br /></strong><br />Kao Canada Inc. s’engage à prendre toutes les mesures nécessaires pour protéger vos renseignements personnels. Pour empêcher tout accès non autorisé à des données et d’en assurer l’exactitude et l’utilisation appropriée, nous avons mis en place des procédures physiques, électroniques et administratives visant à garantir la protection et la sécurité des renseignements que nous recueillons en ligne.</p><br />

<p>Collecte d'information auprès d'enfants<br />Kao Canada Inc. n'a nullement l’intention de recueillir des renseignements personnels auprès d’enfants de moins de 13 ans. Kao Canada Inc. prend des mesures particulières pour s’assurer que les renseignements relatifs aux enfants ne sont recueillis qu’avec un consentement explicite et qu’ils sont protégés contre tout usage illégitime conformément à la loi en vigueur. Si un enfant fournit des renseignements personnels à Kao Canada Inc., cette dernière exige qu’un parent ou tuteur de l’enfant communique avec Kao Canada Inc. au moyen de la page Contactez-nous pour nous en informer. Kao Canada Inc. prendra alors toutes les mesures raisonnables pour supprimer lesdits renseignements de sa base de données.</p><br />

<p><strong>Consentement<br /></strong><br />En utilisant le présent site Web, vous acceptez que nous utilisions vos renseignements personnels tels que définis ci-dessus. Si nous décidons de modifier notre politique de protection des renseignements personnels, les changements seront affichés sur cette page. Vous pouvez ainsi toujours être au courant des renseignements que nous recueillons, de l’usage que nous en faisons et des conditions sous lesquelles nous les divulguons.</p><br />

<p><strong>Renseignements non personnels<br /></strong><br />Toute communication ou tout élément que vous nous envoyez par courriel ou tout autre moyen, incluant les commentaires, données, questions, suggestions ou autres, est considéré comme non confidentiel et non exclusif. Sauf si autrement précisé dans la présente politique, nous pouvons utiliser tout renseignement transmis ou affiché à quelque fin que ce soit, incluant sans s’y limiter, la reproduction, la divulgation, la transmission, la publication, la diffusion et l’affichage. Qui plus est, vous convenez que nous pouvons utiliser tout concept, idée, savoir-faire ou technique contenu dans toute communication que vous nous avez transmise, et cela, sans compensation et pour tout usage, incluant sans s’y limiter, le développement, la fabrication et la commercialisation de produits et services à l'aide des renseignements en question.</p><br />

<p><strong>Accès et correction des renseignements<br /></strong><br />Conformément aux dispositions de la loi, Kao Canada Inc. dispose de procédures permettant aux usagers d'avoir accès aux renseignements personnels recueillis et, s'il y a lieu, de corriger tout renseignement inexact ou incomplet, de modifier leur niveau de consentement ou de supprimer leurs renseignements personnels.</p><br />

<p><strong>Conservation des renseignements<br /></strong><br />Nous conserverons les renseignements personnels tant et aussi longtemps que nous considérerons qu'ils peuvent être utilisés aux fins définies dans cette politique de protection des renseignements personnels.</p><br />

<p><strong>Suppression des renseignements<br /></strong><br />Si les renseignements personnels ne sont pas utiles aux fins définies dans cette politique de protection des renseignements personnels, ou si vous demandez qu'ils soient supprimés, nous prendrons les mesures raisonnables pour supprimer ou détruire ces renseignements.</p><br />

<p><strong>Commentaires et questions<br /></strong><br />Si vous avez des commentaires ou des questions concernant notre politique de protection des renseignements personnels, désirez voir et modifier vos renseignements personnels ou souhaitez que nous supprimions vos renseignements personnels de notre base de données, veuillez communiquer avec nous au moyen de la page Contactez-nous.</p><br />

<p><strong>Modification de la politique de protection des renseignements personnels<br /></strong><br />Kao Canada Inc. se réserve le droit de modifier et de mettre à jour la présente politique ou ses pratiques commerciales connexes en tout temps. Kao Canada Inc. N'appliquera AUCUN changement à la présente politique rétroactivement aux renseignements qu’elle a déjà recueillis.</p><br />

<p><strong>Entrée en vigueur<br /></strong><br />La présente politique de protection des renseignements personnels entre en vigueur le 15 novembre 2006, date de sa dernière mise à jour.<br /></p><br />

            </div>
        </div>
    </div>
</asp:Content>