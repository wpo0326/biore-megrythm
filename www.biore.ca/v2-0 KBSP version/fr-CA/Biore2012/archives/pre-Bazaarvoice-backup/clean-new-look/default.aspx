﻿<%@ Page Title="New Style | Bior&eacute;&reg; Skincare" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="default.aspx.cs" Inherits="Biore2012.special_offers_and_skincare_tips._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Découvrez le nouveau look des soins pour la peau de BioréMD" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/education.css")
        .Render("~/css/combinededucation_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div id="bg"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/educationBg.jpg") %>" alt="" /></div>
        <div class="centeringDiv">
            <h1>
                <span class="newLine firstLine">NOUS AVONS D’IMPORTANTES NOUVELLES&nbsp;.&nbsp;.&nbsp;.</span>
                <span class="newLine secondLine">ET SOMMES IMPATIENTS DE LES RÉPANDRE!</span>
                <img id="arrow" src="<%= VirtualPathUtility.ToAbsolute("~/images/education/arrow.png") %>" alt="" />
            </h1>
            <h2><img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/learnAboutTheLatest.png") %>" alt="DÉCOUVREZ LES PLUS RÉCENTES NOUVELLES SUR LES SOINS POUR LA PEAU BIORÉMD" /></h2>
            <div id="steps">
                <div class="imageWrapper">
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/educationProduct.jpg") %>" class="productImg" alt="Bior&eacute;&reg; Steam Activated Cleanser" />
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/handdrawnOverlay.png") %>" alt="" class="drawnArrows" />
                    <a href="#item1" id="item1Nav" class="scrollPage"></a>
                    <a href="#item2" id="item2Nav" class="scrollPage"></a>
                    <a href="#item3" id="item3Nav" class="scrollPage"></a>
                </div>
                <ul id="listCopy">
                    <li id="item1">
                        <div class="copy">
                            <h3><img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/boldNewLookH.png") %>" alt="NOUVEAU LOOK AUDACIEUX" /></h3>
                            <p>Emballage pur et blanc avec information sur le produit facile à trouver</p>
                        </div>
                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/oneBg.jpg") %>" alt="" class="sideImg" />
                    </li>
                    <li id="item2">
                        <div class="copy">
                            <h3><img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/sameFormulaH.png") %>" alt="MÊMES FORMULES DE NETTOYAGE EN PROFONDEUR " /></h3>
                            <p>Nos formules de nettoyage en profondeur contre la saleté sont aussi efficaces et revitalisantes que jamais</p>
                        </div>
                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/twoBg.jpg") %>" alt="" class="sideImg" />
                    </li>
                    <li id="item3">
                        <div class="copy">
                            <h3><img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/newColorCodingH.png") %>" alt="CATÉGORIES À CODE DE COULEUR" /></h3>
                            <p>Nouvelles catégories de produits – Nettoyage en profondeur (bleu), Soin purifiant (orange) et Démaquillant (rose)</p>
                        </div>
                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/threeBg.png") %>" alt="" class="sideImg" />
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
            <div id="copy">
                <div class="col marginRight">
                    <h3 class="new">Nouvel emballage</h3>
                    <p>Nous ciblons exclusivement la création de produits nettoyants en profondeur efficaces. Tout comme nos produits, notre nouvel emballage est pur, simple et honnête. Avant tout, il accomplit sa mission. </p>
                    <div class="combinationSkin">
                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/combinationSkin.jpg") %>" alt="" />
                        <h3 class="blue"><span class="new">NOUVEAU</span> NETTOYANT ÉQUILIBRANT PEAU MIXTE</h3>
                        <p>Nous savons que souvent la peau n’est ni tout à fait grasse ni tout à fait sèche. C’est pourquoi nous avons créé <a href="<%= VirtualPathUtility.ToAbsolute("~/deep-cleansing-products/combination-skin-balancing-cleanser") %>">le nettoyant équilibrant peau mixte</a>. Si votre peau n’est ni tout à fait grasse ni tout à fait sèche, varie selon le moment ou a des besoins complexes, apportez-lui un peu d’équilibre avec ce nouveau nettoyant.</p>
                    </div>
                </div>
                <div class="col">
                    <h3><span class="blue">Ayez une peau propre et soyez prête en tout temps</h3>
                    <p>Lors d’une entrevue, d’une sortie avec des amis ou d’une soirée sur le divan à regarder un bon film, qui veut se préoccuper de sa peau? Pas vous. Nous avons créé toute une gamme de produits pour que vous obteniez un nettoyage en profondeur et soyez prête en tout temps. </p>
                    <p>Avoir la peau fraîche est tout simple : il suffit de la nettoyer chaque jour et <a href="<%= VirtualPathUtility.ToAbsolute("~/make-up-removing-products/make-up-removing-towelettes") %>">d’utiliser les bandes de nettoyage</a> chaque semaine. Il n’est même pas nécessaire de se rendre au lavabo. Nos lingettes démaquillantes vous assurent un nettoyage rapide partout et en tout temps.</p>
                    <p>Les soins pour la peau de Bioré<sup>MD</sup> procurent à votre peau propreté et fraîcheur à compter du moment où vous prenez votre café au lait le matin jusqu’à celui du débat politique diffusé à la télé le soir. Ayez une peau propre et soyez prête en tout temps.</p>
                    <!--<p class="faceAnything"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/faceAnything.png") %>" alt="Faites face à toutMC" /></p>-->
                </div>
            </div>
            <div class="teaser" id="newLook">
                <img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/newLookBg.jpg") %>" class="backgroundImg" alt="" />
                <img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/newLookH.png") %>" alt="New Look!" class="newLookImg" />
                <p>Partez à la découverte de tous nos produits et de leur nouvel emballage. &raquo;</p>                
                <a href="<%= VirtualPathUtility.ToAbsolute("~/biore-facial-cleansing-products/") %>"></a>
            </div>
            <!--<div class="teaser" id="productChanges">
                <img src="<%= VirtualPathUtility.ToAbsolute("~/images/productChanges.png") %>" alt="" />
                <h3>See All Our Great Updates.</h3>
                <p>Get all the details on the updates we've made to our product lineup. &raquo;</p>
                <a href="<%= VirtualPathUtility.ToAbsolute("~/past-biore-favorites/") %>"></a>
            </div>-->
            <div class="clear"></div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
