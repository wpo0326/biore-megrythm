﻿<%@ Page Title="Exfoliez pour une peau bien propre – découvrez tous les produits | Soins pour la peau Bioré<sup>MD</sup>" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.our_products.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Pour une peau bien propre. Les soins pour la peau Bioré<sup>MD</sup> nettoient et exfolient doucement pour donner à la peau une apparence saine et radieuse! Découvrez la gamme complète de soins pour la peau Bioré<sup>MD</sup>!" name="description" />
    <meta content="Peau propre, exfolier" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="intro">
                <h1>Nos produits</h1>
                <p>Obtenez un nettoyage en profondeur Bioré<sup>MD</sup> et soyez prête en tout temps – exfoliez et nettoyez votre peau tous les jours avec nos nettoyants revigorants et utilisez une bande de nettoyage pour les pores toutes les semaines.</p>
            </div>
            <div id="deepCleansingProds" class="prodList">
                <h2 class="pie roundedCorners deepCleansing">Nettoyage en profondeur <span>(8)</span><span class="arrow"></span></h2>
                <ul>
                    <li>
                        <img src="../images/ourProducts/products/small/combinationSkinBalancingCleanser.jpg" alt="" />
                        <h3><span class="new">NOUVEAU</span> Nettoyant équilibrant peau mixte</h3>
                        <p>Assure un nettoyage uniforme et complet qui répond aux besoins complexes de la peau mixte.</p>
                        <a href="../deep-cleansing-products/combination-skin-balancing-cleanser">details ></a>
                    </li>
                    <li>
                        <img src="../images/ourProducts/products/small/4In1DetoxifyingCleanser.jpg" alt="" />
                        <h3>Nettoyant revitalisant 4 en 1</h3>
                        <p>Revitalise et ravive la peau pour lui donner une apparence plus saine.</p>
                        <a href="../deep-cleansing-products/4-in-1-detoxifying-cleanser">details ></a>
                    </li>
                    <li>
                        <img src="../images/ourProducts/products/small/poreUncloggingScrub.jpg" alt="" />
                        <h3>Nettoyant granuleux pour les pores</h3>
                        <p>Déloge les impuretés et les résidus huileux qui obstruent les pores et procure un teint clair.</p>
                        <a href="../deep-cleansing-products/pore-unclogging-scrub">details ></a>
                    </li>
                    <li>
                        <img src="../images/ourProducts/products/small/dailyPurifyingScrub.jpg" alt="" />
                        <h3>Gommage quotidien purifiant</h3>
                        <p>Dévoile une peau fraîche et rayonnante en exfoliant et en purifiant</p>
                        <a href="../deep-cleansing-products/daily-purifying-scrub">details ></a>
                    </li>
                    <li>
                        <img src="../images/ourProducts/products/small/steamActivatedCleanser.jpg" alt="" />
                        <h3>Nettoyant activé par la vapeur</h3>
                        <p>Tire parti de la vapeur de la douche pour ouvrir les pores et nettoyer en profondeur.</p>
                        <a href="../deep-cleansing-products/steam-activated-cleanser">details ></a>
                    </li>
                    <%--<li class="taller" id="dailyCleansingCloths">
                        <img src="../images/ourProducts/products/small/dailyCleansingCloths.jpg" alt="" />
                        <h3>DAILY CLEANSING CLOTHS</h3>
                        <p>Wipe away dirt to reveal healthy, glowing skin.</p>
                        <a href="../deep-cleansing-products/daily-cleansing-cloths">details ></a>
                    </li>--%>
                    <li class="taller">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStrips.jpg" alt="" />
                        <h3>Bandes de nettoyage en profondeur </h3>
                        <p>Élimine en quelques minutes ce qui s’est accumulé en une semaine.</p>
                        <a href="../deep-cleansing-product-family/pore-strips">details ></a>
                    </li>
                    <li class="taller">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.jpg" alt="" />
                        <h3>Bandes de nettoyage en profondeur - Emballage assorti</h3>
                        <p>Bandes de nettoyage pour le nez, mais aussi pour les joues, le menton et le front.</p>
                        <a href="../deep-cleansing-product-family/pore-strips#combo">details ></a>
                    </li>
                    <li class="taller">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltra.jpg" alt="" />
                        <h3>Bandes de nettoyage en profondeur Ultra</h3>
                        <p>Bandes de nettoyage contenant des ingrédients qui picotent comme l’huile de théier, le menthol et l’hamamélis.</p>
                        <a href="../deep-cleansing-product-family/pore-strips#ultra">details ></a>
                    </li>
                </ul>
            </div>
            <div id="complexionClearingProds" class="prodList">
                <h2 class="pie roundedCorners complexionClearing">Soin purifiant <span>(3)</span><span class="arrow"></span></h2>
                <ul>
                    <li class="taller">
                        <img src="../images/ourProducts/products/small/blemishFightingIceCleanser.jpg" alt="" />
                        <h3>Nettoyant glacé anti-acné</h3>
                        <p>Maîtrise la situation, aide à éliminer les imperfections avant leur apparition.</p>
                        <a href="../complexion-clearing-products/blemish-fighting-ice-cleanser">details ></a>
                    </li>
                    <li class="taller">
                        <img src="../images/ourProducts/products/small/blemishTreatingAstringent.jpg" alt="" />
                        <h3>Astringent anti-acné</h3>
                        <p>Va au-delà du nettoyage, en aidant à traiter les imperfections à l’aide de l’acide salicylique.</p>
                        <a href="../complexion-clearing-products/blemish-treating-astringent">details ></a>
                    </li>
                    <li class="taller">
                        <img src="../images/ourProducts/products/small/warmingAntiBlackheadCleanser.jpg" alt="" />
                        <h3>Nettoyant chauffant en crème anti-points noirs</h3>
                        <p>Cible les points noirs en dilatant les pores pour un nettoyage en profondeur.</p>
                        <a href="../complexion-clearing-products/warming-anti-blackhead-cleanser">details ></a>
                    </li>
                </ul>
            </div>
            <div id="murtProds" class="prodList">
                <h2 class="pie roundedCorners murt">Démaquillant  <span>(1)</span><span class="arrow"></span></h2>
                <ul>
                    <li id="murt">
                        <img src="../images/ourProducts/products/small/makeupRemovingTowelettes.jpg" alt="" />
                        <h3>Lingettes démaquillantes</h3>
                        <p>Enlèvent le mascara hydrofuge mieux que les lingettes d’un concurrent populaire.</p>
                        <a href="../make-up-removing-products/make-up-removing-towelettes">details ></a>
                    </li>
                </ul>
            </div>
            <%--<div id="productChanges" class="teaser">
                <img src="../images/productChanges.png" alt="" />
                <h3>Can't find your favorite?</h3>
                <p>Check out the updates we’ve made to our product lineup. &raquo;</p>
                <a href="../past-biore-favorites/"></a>
            </div>--%>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
