﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductDetail.aspx.cs" Inherits="Biore2012.our_products.ProductDetail" ResponseEncoding="ISO-8859-1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/productDetail.css")
        .Render("~/css/combineddetail_#.css")
    %>
    <meta name="description" id="metaDescription" runat="server" />
    <meta name="keywords" id="metaKeywords" runat="server" />
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="x-rim-auto-match" content="none">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="col floatRight" id="productName">
                <h2 class="roundedCorners pie <asp:literal ID="theCategoryClass" runat="server" />"><asp:literal ID="theCategory" runat="server" /></h2>
                <h1><asp:literal ID="theName" runat="server" /></h1>
                <p><span class="new"><asp:literal ID="thePromoCopy" runat="server" /></span> <asp:literal ID="theDescription" runat="server" /></p>
                
                <asp:HyperLink id="theBuyNowLink" runat="server" Target="_blank" CssClass="buyNow">Buy Now <span class=\"arrow\">&rsaquo;</span></asp:HyperLink>
                <div class="plusBtn" id="plusBtnDiv"><asp:Literal ID="plusButton" runat="server" /></div>
                <div class="likeBtn" id="likeBtnDiv"><asp:Literal ID="fbLike" runat="server" /></div>     
                <div class="clear"></div>           
            </div>
            <div class="col bottomBorder" id="productImages">
                <div class="prodImgHolder on" id="newProd">
                    <asp:Image ID="theNewImage" runat="server" />
                </div>
                <div class="prodImgHolder" id="oldProd">
                    <asp:Image ID="theOldImage" runat="server" /> 
                </div>
                <asp:Panel ID="oldNewControlPanel" runat="server" Visible="true">
                <ul class="prodLookNav hide">
                    <li><a href="#newProd" class="on"><span class="pie circle"></span> New Look</a></li>
                    <li><a href="#oldProd"><span class="pie circle"></span> Old Look</a></li>
                </ul>
                </asp:Panel>
            </div>
            <asp:Panel ID="thePoreStripNavPanel" CssClass="col floatRight" runat="server" Visible="false">
                <ul id="poreStripNav">
                    <li class="poreStripNav selected" id="regularNav">
                        <div class="imgWrapper">
                            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/ourProducts/productDetail/poreStripNavRegular.png") %>" />
                            <span class="pie roundedBoxBg"></span>    
                            <a class="poreNav" href="#Regular">
                                <div class="pie">
                                    <span class="dc">Deep Cleansing</span>
                                    <span class="ps pie">Pore Strips</span>
                                </div>
                            </a>
                        </div>                        
                    </li>
                    <li class="poreStripNav" id="comboNav">
                        <div class="imgWrapper">
                            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/ourProducts/productDetail/poreStripNavCombo.png") %>" />
                            <span class="pie roundedBoxBg"></span>
                            <a class="poreNav" href="#Combo">
                                <div class="pie">
                                    <span class="dc">Deep Cleansing</span>
                                    <span class="ps pie">Pore Strips Combo</span>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li class="poreStripNav" id="ultraNav">
                        <div class="imgWrapper">
                            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/ourProducts/productDetail/poreStripNavUltra.png") %>" />
                            <span class="pie roundedBoxBg"></span>
                            <a class="poreNav" href="#Ultra">
                                <div class="pie">
                                    <span class="dc">Deep Cleansing</span>
                                    <span class="ps pie">Pore Strips Ultra</span>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li class="clear"></li>
                </ul>
                </asp:Panel>
            <div class="col floatRight" id="productInfo">
                <div class="contentHolder open">
                    <h3 class="pie">What It Is <span class="pie circle"></span></h3>
                    <div class="collapsibleContent" id="whatItIsContent">
                        <asp:Literal ID="theWhatItIs" runat="server" />
                    </div>
                </div>
                <div class="contentHolder">
                    <h3 class="pie">Product Details <span class="pie circle"></span></h3>
                    <div class="collapsibleContent" id="ingredientsContent">
                        <asp:Literal ID="theIngredients" runat="server" />
                    </div>
                </div>
                <asp:Panel ID="theFunFactsPanel" runat="server" Visible="true">
                <div class="contentHolder">
                    <h3 class="pie">Fun Facts <span class="pie circle"></span></h3>
                    <div class="collapsibleContent" id="funFactsContent">
                        <asp:Literal ID="theFunFacts" runat="server" />
                    </div>
                </div>
                </asp:Panel>       
                <div class="contentHolder">
                    <h3 class="pie">how it works <span class="pie circle"></span></h3>
                    <div class="collapsibleContent" id="howItWorksContent">
                        <asp:Literal ID="theHowItWorks" runat="server" />
                        <h4>Cautions</h4>
                        <asp:Literal ID="theCautions" runat="server" />
                    </div>
                </div>
                <asp:Panel ID="theVideoPanel" runat="server" visible="true">
                <div class="videoHolder">
                    <div class="video pie">
                        <div id="lbVideo">
                            <asp:Hyperlink ID="theVideoLink" runat="server" Target="_blank" CssClass="videoLink" >
                                <asp:Image ID="theVideoStillImage" runat="server" />
                            </asp:Hyperlink>
                        </div>
                    </div>
                </div>
                </asp:Panel>
                <div class="clear"></div>
            </div>
            <div class="col" id="alsoLike">
                <h3><asp:literal ID="theSidebarHeader" runat="server"></asp:literal></h3>
                <asp:literal runat="server" id="theSidebarCopy"></asp:literal>
                <div id="prodCarouselWrapper">
                    <a href="#" class="carouselNav circle disabled pie" id="prev">Previous</a>
                    <div id="prodCarousel">
                        <ul>
                            <asp:ListView ID="theSideBarProducts" runat="server" OnItemDataBound="sidebar_ItemDataBound">
                                <LayoutTemplate>
                                    <li runat="server" id="itemPlaceholder"></li>
                                </LayoutTemplate>
                                <ItemTemplate>
                                  <li runat="server" id="theSideBarListItem">
                                      <asp:HyperLink ID="theSideBarProductLink" runat="server">
                                        <asp:Image ID="theSideBarProductImage" runat="server" />
                                        <span><asp:Literal ID="theSideBarProductName" runat="server" /> &raquo;</span>
                                      </asp:HyperLink>
                                  </li>
                                   
                                </ItemTemplate>
                            </asp:ListView>
                        </ul>
                    </div>
                    <a href="#" class="carouselNav circle pie" id="next">Next</a>
                </div>
            </div>
            <%--<div class="teaser" id="proveIt">
                <img src="<%= VirtualPathUtility.ToAbsolute("~/images/ourProducts/productDetail/proveItModel.jpg") %>" alt="" class="model" />
                <div class="floatLeft">
                    <h2><img src="<%= VirtualPathUtility.ToAbsolute("~/images/ourProducts/productDetail/proveItLogo.png") %>" alt="Prove It!&trade; Reward Points" /></h2>
                    <h3>Make Pore Strip purchases count</h3>
                    <p>Look for Prove It!&trade; Reward Point codes in specially marked Bior&eacute;<sup>&reg;</sup> Pore Strip packages.</p>
                    <asp:HyperLink ID="proveItPromoLink" NavigateUrl="http://www.facebook.com/bioreskin?sk=app_205787372796203" runat="server" Target="_blank">Start earning now! <span class="arrow">&rsaquo;</span></asp:HyperLink>
                </div>
                    
            </div>--%>
            <div class="clear"></div>
            <asp:Literal ID="theOutput" runat="server"></asp:Literal>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
<script type="text/javascript">
    <asp:Literal ID="thePoreStripsJSON" runat="server" />
</script>

<%=SquishIt.Framework.Bundle .JavaScript()
    .Add("~/js/swfobject.min.js")
    .Add("~/js/jquery.ba-hashchange.min.js")
    .Add("~/js/jquery.touchwipe.min.js")
    .Add("~/js/productDetail.js")
    .Render("~/js/combineddetail_#.js")
%>

</asp:Content>   