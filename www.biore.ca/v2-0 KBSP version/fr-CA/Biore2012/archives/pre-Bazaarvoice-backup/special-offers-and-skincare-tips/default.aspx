﻿<%@ Page Title="Special Offers, New Products &amp; Skincare Tips | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.new_products_and_offers._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="conseils en matière de soins pour la peau, offres spéciales et nouveaux produits des soins pour la peau BioréMD" name="description" />
    <meta content="conseils en matière de soins pour la peau, prenez soin de votre peau, nouveau produit" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/whatsNew.css")
        .Render("~/css/combinednew_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="intro">
                <h1>QUOI NEUF?</h1>
                <p>Nos meilleurs produits, nos nouveautés et nos actualités les plus récentes.</p>
            </div>
            <div id="promos">
                <ul>
                    <li id="promo1" class="promo">
                        <div class="promoImage"><img src="../images/whatsNew/promo1Img.png" alt="" /></div>
                        <div class="promoContent">
                            <h3 class="promoHeader">Notre orientation?</h3>
                            <p class="promoBody">Nous avons fait des changements emballants pour 2012!  </p>
                            <div class="promoButton"><a href="../clean-new-look">Pour en savoir plus <span class="arrow">&rsaquo;</span></a></div>
                        </div>
                    </li>
                    <%--<li id="promo2" class="promo">
                        <div class="promoImage"><img src="../images/whatsNew/promo2Img.png" alt="" /></div>
                        <div class="promoContent">
                            <h3 class="promoHeader">Get Rewarded</h3>
                            <p class="promoBody">Build up Prove It!&trade; Reward Points on Facebook towards free products and more.</p>
                            <div class="promoButton"> <asp:HyperLink ID="proveItPromoLink" NavigateUrl="http://www.facebook.com/bioreskin?sk=app_205787372796203" runat="server" Target="_blank">Start Earning <span class="arrow">&rsaquo;</span></asp:HyperLink></div>
                        </div>
                    </li>--%>
                    <li id="promo3" class="promo">
                        <div class="promoImage"><img src="../images/whatsNew/promo3Img.png" alt="" /></div>
                        <div class="promoContent">
                            <h3 class="promoHeader"><span class="new">NOUVEAU</span> NETTOYANT ÉQUILIBRANT PEAU MIXTE</h3>
                            <p class="promoBody">Ajoutez un peu d’équilibre à votre vie avec ce tout nouveau nettoyant.</p>
                            <div class="promoButton"><a href="../deep-cleansing-products/combination-skin-balancing-cleanser">Jetez-y un coup d’œil <span class="arrow">&rsaquo;</span></a></div>
                        </div>
                    </li>
                </ul>
            </div>
            <div id="polaroids">
                <img src="../images/whatsNew/whatsNewPolaroids.jpg" alt="" />
            </div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
