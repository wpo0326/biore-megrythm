<!DOCTYPE html>
<!--[if IE 7 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie7 ie"><![endif]-->
<!--[if IE 8 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie8 ie"><![endif]-->
<!--[if IE 9 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie9 ie"><![endif]-->
<!--[if !IE]><!--><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" ><!--<![endif]-->
<head>
<title>
	Bior� Combination Skin Balancing Cleanser Sampling Guidelines
</title>
    
    <link rel="stylesheet" type="text/css" href="/en-CA/css/global.css" />
<style type="text/css">
#mainbody{width:80%; margin-left:auto;margin-right:auto;}
h1 {padding:10px 0px 10px 0px;}
p {padding-bottom:20px}
</style>
</head>

<body>
<div id="mainbody">
<div id="languageToggle"><a href="/en-CA/promotions/first-to-reply-contest.aspx">English</a></div>

<h1>CONCOURS ARRIVEZ PREMIERS DU 22 F�VRIER 2012 (LE � CONCOURS �) 
DES SOINS POUR LA PEAU BIOR�<sup>MD</sup></h1>

<h2>CE CONCOURS EST R�SERV� AUX R�SIDENTS DU CANADA (HORMIS CEUX DU QU�BEC ET DES PROVINCES � L�OUEST DU QU�BEC) ET EST R�GI PAR LES LOIS DU CANADA</h2>
<br />
<p><b>1.	DUR�E DU CONCOURS</b> 
Le concours d�bute le <b>22 f�vrier 2012</b> � 10 h 1 s (HE) et doit prendre fin le 
    <b>22 f�vrier 2012</b> � 23 h 59 min 59 s (HE), ou lorsque 10 participants auront �t� proclam�s gagnants conform�ment au pr�sent r�glement officiel (le � 
    <b>r�glement</b> �), selon la premi�re de ces �ventualit�s (la � <b>dur�e du concours</b> �). </p>

<p><b>2.	ADMISSIBILIT� </b>
Le concours s�adresse � tous les r�sidents canadiens (hormis ceux du Qu�bec et des provinces � l�ouest du Qu�bec) qui, au moment de leur participation, sont majeurs selon les lois de leur province ou de leur territoire de r�sidence, sauf les employ�s, les repr�sentants ou les agents (et les personnes vivant sous le m�me toit que ces derniers, qu�elles soient ou non parentes avec ceux-ci) de Kao Canada Inc. (le � 
    <b>commanditaire</b> �), ses soci�t�s m�res, ses filiales, ses soci�t�s affili�es, ses agences de publicit� et de promotion, des fournisseurs de prix et des juges du concours (collectivement appel�s les �
    <b>parties li�es au concours</b> �).  </p>

<p><b>3.	PRIX </b>
Dix (10) prix sont � gagner. Chacun, d�une valeur au d�tail approximative de vingt-quatre dollars (24 $), consiste en deux (2) billets de cin�ma, pour adultes (le � 
    <b>prix</b> �).  Une fois un participant proclam� gagnant conform�ment au pr�sent r�glement officiel, son prix lui est normalement livr� par la poste dans les quatre (4) � six (6) semaines qui suivent. Le commanditaire se r�serve le droit de remplacer le prix ou tout �l�ment de celui-ci, pour une raison quelconque, par un prix ou par un �l�ment de valeur �gale ou sup�rieure. Le prix doit �tre accept� tel quel et ne peut �tre transf�r�. Aucun �change n�est accord�, sauf si le commanditaire y consent. Le prix n�est remis qu�� la personne dont le nom complet et une adresse de courriel valide v�rifiables figurent sur le bulletin de participation (d�crit ci-dessous). Toutes les d�penses non express�ment mentionn�es aux pr�sentes sont � la charge des gagnants.</p>

<p><b>4.	COMMENT PARTICIPER  </b>
AUCUN ACHAT N�EST REQUIS. Pour participer, vous devez envoyer un courriel (le � <b>courriel</b> �) � bioreskincarelistens@biore.ca, indiquant : (i) votre nom complet, votre adresse postale compl�te (y compris le code postal), votre num�ro de t�l�phone et votre �ge; (ii) votre produit Bior�MD pr�f�r� et (iii) votre film pr�f�r�. Pour �tre admissible, votre courriel doit �tre envoy� et re�u pendant la dur�e du concours (courriel et description ci-apr�s collectivement appel�s le �
    <b>bulletin de participation</b> �). REMARQUE : LA DESCRIPTION NE SERA PAS JUG�E. </p>

<p>Une (1) seule participation par personne ou adresse de courriel est autoris�e pendant la dur�e du concours.  Pour �viter toute ambigu�t�, vous ne pouvez utiliser qu�une (1) seule adresse de courriel pour participer au concours. S�il est �tabli qu�une personne a tent� i) de soumettre, par jour, plus d�un (1) bulletin de participation par personne ou adresse de courriel pendant la dur�e du concours ou ii) d�utiliser plus d�une adresse de courriel pour participer au concours, le commanditaire peut alors, � son enti�re discr�tion, prononcer sa disqualification ainsi que l�annulation de tous ses bulletins de participation. Tout bulletin de participation est rejet� si le commanditaire estime, � son enti�re discr�tion, que le courriel n�a pas �t� envoy� et re�u pendant la dur�e du concours. L�utilisation du moindre moyen automatis� (scripts, macros, robots ou autres programmes compris) pour participer au concours est strictement interdite et constitue un motif de disqualification par le commanditaire. Les parties exon�r�es (d�finies ci-dessous) ne peuvent �tre tenues responsables des bulletins de participation en retard, perdus, incorrectement achemin�s, retard�s, incomplets ou inadmissibles. </p>

<p>Tous les bulletins de participation peuvent faire l�objet d�une v�rification. Le commanditaire se r�serve le droit � son enti�re discr�tion d�exiger une preuve d�identit� et (ou) d�admissibilit� au concours, sous une forme acceptable � ses yeux.  Le d�faut de fournir une telle preuve en temps opportun peut entra�ner la disqualification. Le ou les serveurs du concours repr�sentent l�unique m�thode visant � d�terminer la date et la validit� d�une inscription au concours.</p>

<p><b>5.	D�TERMINATION DES GAGNANTS</b>
Chacun des <b>dix (10)</b> premiers participants admissibles � soumettre un bulletin de participation admissible conform�ment au pr�sent r�glement court la chance de remporter un prix (sous r�serve du respect du pr�sent r�glement). Les probabilit�s de gagner d�pendent de l�heure � laquelle chaque bulletin de participation admissible est re�u pendant la dur�e du concours. </p>


<p>Le commanditaire ou son repr�sentant d�sign� tentera au maximum � trois (3) reprises de joindre chacun des gagnants potentiels par courriel (au moyen de l�adresse de courriel utilis�e pour envoyer le bulletin de participation) dans les cinq (5) jours ouvrables suivant la soumission du bulletin.  Si un gagnant potentiel n�a toujours pas pu �tre joint au bout de trois (3) tentatives ou que quinze (15) jours ouvrables se sont �coul�s depuis la soumission du bulletin de participation (selon la premi�re de ces �ventualit�s), ou encore en cas de retour du moindre avis pour cause de livraison ou de transmission impossible, le gagnant potentiel en question est disqualifi� (et perd ainsi tout droit au prix), le commanditaire se r�servant dans ce cas le droit, � son enti�re discr�tion, de s�lectionner le prochain participant admissible parmi le reste des bulletins de participation admissibles (le cas �ch�ant, les dispositions pr�c�dentes du pr�sent paragraphe s�appliquent au nouveau gagnant potentiel). </p>

<p>POUR �TRE PROCLAM� GAGNANT DU PRIX, chaque gagnant potentiel doit au pr�alable : (a) r�pondre correctement, sans aide m�canique ou autre, � une question r�glementaire d�arithm�tique; et (b) signer et retourner, dans les cinq (5) jours ouvrables suivant l�avis de sa s�lection, le formulaire de d�claration et de d�charge du commanditaire par lequel, notamment, ledit gagnant potentiel : (i) confirme respecter le pr�sent r�glement officiel; (ii) confirme accepter le prix tel quel; (iii) s�engage � d�gager les parties li�es au concours ainsi que chacun de leurs dirigeants, administrateurs, mandataires, repr�sentants, successeurs et ayants droit respectifs (collectivement les � parties exon�r�es �) de toute responsabilit� li�e au concours, � sa participation � celui-ci et (ou) � l�attribution, � l�utilisation ou au m�susage du prix ou de toute partie de celui-ci; et (iv) autorise la publication, la reproduction et (ou) toute autre utilisation, sans autre avis ni r�mun�ration, de son nom, de son adresse, de sa voix, de ses d�clarations au sujet du concours et (ou) de sa photographie ou de tout �l�ment semblable dans toute publicit� effectu�e par le commanditaire par quelque moyen que ce soit, y compris sur papier, dans les m�dias �lectroniques ou sur Internet. Si un gagnant potentiel : (a) ne r�pond pas correctement � la question r�glementaire; (b) ne renvoie pas les documents du concours d�ment sign�s dans le d�lai prescrit; et (ou) (c) ne peut accepter le prix comme il est d�cern� pour une raison quelconque, il est disqualifi� (et perd ainsi tout droit au prix) et le commanditaire se r�serve le droit, � son enti�re discr�tion, de s�lectionner le prochain participant admissible parmi le reste des inscriptions admissibles (le cas �ch�ant, les dispositions pr�c�dentes du pr�sent paragraphe s�appliquent au nouveau gagnant potentiel). </p>

<p><b>6.	G�N�RALIT�S </b>
Tous les bulletins de participation deviennent la propri�t� du commanditaire. Les parties exon�r�es ne peuvent �tre tenues responsables des bulletins de participation perdus, retard�s, incomplets, inadmissibles ou mal achemin�s. Le concours est assujetti � l�ensemble des lois et r�glements f�d�raux, provinciaux et municipaux en vigueur. En participant au concours, les participants consentent � �tre li�s par le pr�sent r�glement et par les d�cisions du commanditaire relatives � tout aspect du concours, qui sont irr�vocables, sans appel et ex�cutoires en plus de lier l�ensemble des participants � y compris, sans s�y limiter, les d�cisions relatives � l�admissibilit� des bulletins de participation et (ou) des participants, au rejet des premiers ou encore � la disqualification des seconds.</p>

<p>Les parties exon�r�es ne peuvent �tre tenues responsables : (i) de la moindre d�faillance du site Web pendant la dur�e du concours; (ii) de tout dysfonctionnement technique ou autre probl�me li� � un r�seau ou � des lignes t�l�phoniques, � des syst�mes informatiques en ligne, � des serveurs, � des fournisseurs d�acc�s, � du mat�riel informatique ou � des logiciels; (iii) de la non-r�ception de la moindre participation par les parties li�es au concours pour quelque raison que ce soit, y compris, sans s�y limiter, des probl�mes techniques ou encore la saturation d�Internet ou d�un site Web quelconque; (iv) des dommages corporels ou encore des dommages subis par l�ordinateur ou par tout autre appareil d�un participant ou d�une autre personne li�s � la participation au concours ou au t�l�chargement du moindre �l�ment dans le cadre de celui-ci; et (ou) (v) de toute combinaison des �l�ments pr�cit�s. </p>

<p>En cas de diff�rend concernant la personne ayant soumis un bulletin de participation, celui-ci sera jug� avoir �t� soumis par le titulaire autoris� du compte de l�adresse de courriel soumise au moment de l�inscription.  Un � titulaire autoris� de compte � d�signe la personne � qui une adresse de courriel a �t� attribu�e par un fournisseur d�acc�s Internet, un fournisseur de services en ligne ou une autre organisation (p. ex., une entreprise, un �tablissement d�enseignement, etc.) responsable d�attribuer des adresses de courriel pour le domaine associ� � l�adresse de courriel soumise. Tout participant peut �tre tenu de fournir la preuve qu�il est bien le titulaire autoris� du compte de courriel de l�adresse associ�e au bulletin de participation s�lectionn�.</p>

<p>Tous les bulletins de participation peuvent faire l�objet d�une v�rification. Le commanditaire se r�serve le droit, � sa seule et enti�re discr�tion, d�exiger une preuve d�identit� et (ou) d�admissibilit� au concours, dont la forme est acceptable � ses yeux (y compris, sans s�y limiter, une pi�ce d�identit� avec photo �mise par un gouvernement). Le d�faut de fournir une telle preuve en temps opportun peut entra�ner la disqualification. Le ou les serveurs du concours repr�sentent l�unique m�thode visant � d�terminer la date et la validit� d�un bulletin de participation au concours.</p>

<p>Le commanditaire se r�serve, � son enti�re discr�tion, le droit de modifier sans pr�avis, dans la mesure n�cessaire, les dates et (ou) les horaires pr�vus par le pr�sent r�glement, que ce soit pour s�assurer du respect du pr�sent r�glement par tout participant ou tout bulletin de participation, � la suite de probl�mes techniques, ou encore en raison d�autres circonstances que le commanditaire estime, � son enti�re discr�tion, susceptibles de nuire � la bonne gestion du concours conform�ment au pr�sent r�glement.</p>

<p>Le commanditaire se r�serve le droit, � son enti�re discr�tion, de mettre fin au concours, de le modifier ou de le suspendre (ou encore de modifier le pr�sent r�glement) de quelque fa�on que ce soit si une erreur, un probl�me technique, un virus informatique, un bogue, un sabotage, une intervention non autoris�e, une fraude, une d�faillance technique ou tout autre facteur pouvant raisonnablement �tre consid�r� comme ind�pendant de la volont� du commanditaire nuit au d�roulement du concours conform�ment au pr�sent r�glement. Toute tentative d�lib�r�e d�endommager tout site Web ou de perturber le bon d�roulement du pr�sent concours constitue une violation des lois criminelles et p�nales et, advenant une telle tentative, le commanditaire se r�serve le droit d�engager un recours pour dommages subis, jusqu�aux limites permises par la loi. Le commanditaire se r�serve le droit, � son enti�re discr�tion, d�annuler ou de suspendre le concours ou encore de modifier ce dernier ou le pr�sent r�glement, sans pr�avis ni obligation de sa part, en cas d�accident, d�erreur d�impression, d�erreur administrative ou d�erreur de toute autre nature, ou encore pour toute autre raison. </p>

<p>En remplissant son bulletin de participation, tout participant consent � la collecte, � l�utilisation et � la divulgation de ses renseignements personnels par le commanditaire, mais uniquement aux fins de la gestion du concours; lesdits renseignements sont conserv�s pendant environ un (1) an. Le commanditaire s�engage � ne pas vendre ni transmettre ces renseignements � des tiers, sauf aux fins de la gestion du concours. Pour en savoir davantage sur l�utilisation des renseignements personnels en lien avec le concours, consultez la politique de protection des renseignements personnels publi�e au <a href="http://www.biore.ca/fr-CA/privacy/">http://www.biore.ca/fr-CA/privacy/</a>.</p>

<p>Les modalit�s du concours �nonc�es dans le pr�sent r�glement ne peuvent �tre modifi�es ou faire l�objet d�une contre-proposition, sauf dans la mesure pr�vue aux pr�sentes.</p>

<p>L�ensemble de la propri�t� intellectuelle utilis�e par le commanditaire en lien avec la promotion et (ou) la gestion du concours, y compris l�ensemble des marques de commerce, des appellations commerciales, des logos, des conceptions, des outils promotionnels, des pages Web, des codes d�origine, des dessins, des illustrations, des slogans et des repr�sentations, appartient au commanditaire et (ou) � ses soci�t�s affili�es ou est utilis� par ceux-ci en vertu d�une licence. Tous les droits sont r�serv�s. La copie ou l�utilisation non autoris�es de la moindre propri�t� intellectuelle pr�cit�e sans le consentement expr�s �crit de son propri�taire sont strictement interdites. </p>

<p>En cas de divergence ou d�incompatibilit� entre les modalit�s du pr�sent r�glement et les d�clarations ou autres mentions figurant dans tout autre document li� au concours, y compris le bulletin de participation au concours ou la moindre publicit� au sein de points de vente, � la t�l�vision, sur papier ou en ligne, les modalit�s du pr�sent r�glement pr�valent.</p>

<p><b>7.</b>	Le concours est commandit� par Kao Canada Inc., 60 Courtneypark Drive West, Unit 5, Mississauga (Ontario)  L5W 0B3. </p>

<p><b>8.</b>	Le concours n�est en rien commandit�, approuv� ou g�r� par Facebook ou encore associ� � celui-ci. Vous convenez que les renseignements que vous divulguez le sont aupr�s du commanditaire, et non aupr�s de Facebook. Ces renseignements sont utilis�s aux fins de la participation au concours et de l�attribution des prix d�cern�s dans le cadre de celui-ci. Ils sont int�gr�s au contenu du site Web pour une dur�e ind�termin�e. Les questions, plaintes ou commentaires concernant le concours doivent �tre adress�s au commanditaire, et non � Facebook. </p>

</div>
</body>

</html>
