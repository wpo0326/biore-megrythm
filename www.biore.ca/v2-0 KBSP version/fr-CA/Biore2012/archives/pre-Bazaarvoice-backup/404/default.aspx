﻿<%@ Page Title="Page Not Found | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012._404._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Bioré® Skincare – Page Not Found" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <div id="content">
                    <h1>404 Page Not Found</h1>
                    <p>Nous sommes désolés. La page que vous avez demandée est introuvable ou une erreur s’est produite en cours de processus.</p>
                    <div class="promoButton"><a href="<%= System.Configuration.ConfigurationManager.AppSettings["path"] %>">Accueil <span class="arrow">&rsaquo;</span></a></div>
                 </div>
                 <div id="photo">
                    <img src="<%= System.Configuration.ConfigurationManager.AppSettings["path"] %>images/utilityPages/polaroid.jpg" alt="" />
                 </div>
                 <div class="clear"></div>
            </div>
        </div>
    </div>
</asp:Content>