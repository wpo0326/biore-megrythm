﻿<%@ Page Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="NewsletterForm.aspx.cs"
    Inherits="Biore2012.FormConfig.NewsletterForm" %>
<%@ Register TagPrefix="skm" Namespace="skmValidators" Assembly="skmValidators" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%#SquishIt.Framework.Bundle .Css()
        .Add("~/css/formConfig.css")
        .Render("~/css/combinedform_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow">
            </div>
            <div class="centeringDiv">
                <div class="responsive contact">
                    <div id="contactFormWrap">
                        <div id="BrandImageContainer" class="png-fix">
                            <img class="png-fix" src="images/forms/signUpPhoto.jpg" style="border-width: 0px;" />
                        </div>
                        <div id="ContactFormContainer">
                            <asp:Panel ID="ContactDetails" runat="server">
                                <!-- Header -->
                                <div id="formHeader">
                                    <h1 id="ctl00_ContentPlaceHolder1_ucForm_PageHeader">
                                        JOIGNEZ-VOUS AU MOUVEMENT POUR UNE BELLE PEAU EN TOUT TEMPS!
                                    </h1>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="OptinForm" runat="server">
                                <!-- Description -->
                                <div class="DescriptionContainer png-fix">
                                    <h2>
                                        Merci de l’intérêt que vous portez aux soins pour la peau Bioré<sup>MD</sup></h2>
                                    <p>Veuillez fournir les renseignements ci-dessous afin de recevoir de l'information sur nos promotions, nos concours et nos actualités les plus récentes.</p>
                                </div>
                                <!-- From Fields -->
                                <div class="FormContainer">
                                    <p class="req png-fix">
                                        <em>Champ obligatoire*</em></p>
                                    <div class="MemberInfoContainer png-fix">
                                        <div class="NameWrapper">
                                            <div class="FirstNameContainer Question">
                                                <asp:Label ID="FNameLbl" runat="server" Text="Prénom*" AssociatedControlID="FName"></asp:Label>
                                                <asp:TextBox ID="FName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="FNameValidator1" runat="server" ErrorMessage="Veuillez entrer votre Prénom."
                                                        ControlToValidate="FName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="FNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your First Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="FName" EnableClientScript="true"
                                                        SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="LastNameContainer Question">
                                                <asp:Label ID="LNameLbl" runat="server" Text="Nom*" AssociatedControlID="LName"></asp:Label>
                                                <asp:TextBox ID="LName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="LNameValidator1" runat="server" ErrorMessage="Veuillez entrer votre Nom."
                                                        ControlToValidate="LName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="LNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Last Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="LName" SetFocusOnError="true"
                                                        Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="EmailWrapper">
                                            <div class="EmailContainer Question">
                                                <asp:Label ID="EmailLbl" runat="server" Text="Adresse de courriel*" AssociatedControlID="Email"></asp:Label>
                                                <asp:TextBox ID="Email" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="EmailValidator1" runat="server" ErrorMessage="Veuillez entrer votre Courriel."
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="EmailValidator2" runat="server" ErrorMessage="Please enter a valid Email Address."
                                                        ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div class="PhoneContainer Question">
                                            <asp:Label ID="MobilePhoneLbl" runat="server" Text="Numéro de téléphone mobile" AssociatedControlID="MobilePhone"></asp:Label>
                                            <asp:TextBox ID="MobilePhone" MaxLength="50" runat="server" />
                                            <div class="ErrorContainer">
                                                <asp:RegularExpressionValidator ID="v_MobilePhone" runat="server" ErrorMessage="Your Mobile Phone is not required, but please do not use special characters in the Mobile Phone field."
                                                    ValidationExpression="^[^<>]+$" ControlToValidate="MobilePhone" EnableClientScript="true"
                                                    SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                <%-- The following RegEx Validators is for US type numbers.  IMPORTANT: 
								 Please test against your user base before using as you may need to modify it for other formats!  --%>
                                                <%--<asp:RegularExpressionValidator ID="PhoneValidator2" runat="server"
								ErrorMessage="<br/>Your Home Phone is not required, but please enter only valid characters."
								ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"
								ControlToValidate="Phone" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg phone"></asp:RegularExpressionValidator>--%>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelAddress" class="AddressContainer">
                                            <div class="AddressWrapper">
                                                <div class="Address1Container Question">
                                                    <asp:Label ID="Address1Lbl" runat="server" Text="Adresse 1*" AssociatedControlID="Address1"></asp:Label>
                                                    <asp:TextBox ID="Address1" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="Address1Validator1" runat="server" ErrorMessage="Veuillez entrer votre Adresse."
                                                            ControlToValidate="Address1" EnableClientScript="true" SetFocusOnError="true"
                                                            Display="Dynamic" CssClass="errormsg" />
                                                        <asp:RegularExpressionValidator ID="Address1Validator2" runat="server" ErrorMessage="Please do not use special characters in the Address 1 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address1" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="Address2Container Question">
                                                    <asp:Label ID="Address2Lbl" runat="server" Text="Adresse 2" AssociatedControlID="Address2"></asp:Label>
                                                    <asp:TextBox ID="Address2" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="Address2Validator1" runat="server" ErrorMessage="Please do not use special characters in the Address 2 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address2" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="CityContainer Question">
                                                    <asp:Label ID="CityLbl" runat="server" Text="Ville*" AssociatedControlID="City"></asp:Label>
                                                    <asp:TextBox ID="City" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="CityValidator1" runat="server" ErrorMessage="Veuillez entrer votre Adresse Ville."
                                                            ControlToValidate="City" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                            CssClass="errormsg" />
                                                        <asp:RegularExpressionValidator ID="CityValidator2" runat="server" ErrorMessage="Please do not use special characters in the City field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="City" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="StateContainer Question">
                                                <asp:Label ID="StateLbl" runat="server" Text="Province*" AssociatedControlID="State"></asp:Label>
                                                <asp:DropDownList ID="State" runat="server">
                                                    <asp:ListItem Value="" Selected="true">Sélectionner la province</asp:ListItem>
                                                    <asp:ListItem Value="1">Alberta</asp:ListItem>
                                                    <asp:ListItem Value="2">Colombie-Britannique</asp:ListItem>
                                                    <asp:ListItem Value="3">Manitoba</asp:ListItem>
                                                    <asp:ListItem Value="4">Nouveau-Brunswick</asp:ListItem>
                                                    <asp:ListItem Value="5">Terre-Neuve-et-Labrador</asp:ListItem>
                                                    <asp:ListItem Value="6">Territoires du Nord-Ouest</asp:ListItem>
                                                    <asp:ListItem Value="7">Nouvelle-Écosse</asp:ListItem>
                                                    <asp:ListItem Value="8">Nunavut</asp:ListItem>
                                                    <asp:ListItem Value="9">Ontario</asp:ListItem>
                                                    <asp:ListItem Value="10">Île-du-Prince-Édouard</asp:ListItem>
                                                    <asp:ListItem Value="11">Québec</asp:ListItem>
                                                    <asp:ListItem Value="12">Saskatchewan</asp:ListItem>
                                                    <asp:ListItem Value="13">Yukon</asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="StateValidator" runat="server" ErrorMessage="Veuillez entrer votre Province."
                                                        ControlToValidate="State" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div class="PostalCodeContainer Question">
                                            <asp:Label ID="PostalCodeLbl" runat="server" Text="Code postal*" AssociatedControlID="PostalCode"></asp:Label>
                                            <asp:TextBox ID="PostalCode" runat="server" MaxLength="20" CssClass="inputTextBox"></asp:TextBox>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="PostalCodeValidator1" runat="server" ErrorMessage="Veuillez entrer votre Code postal."
                                                    ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true"
                                                    Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                <%-- The following RegEx Validator is for Canadian Postal codes.  IMPORTANT: 
							         Please test against your user base before using as you may need to modify it for other formats!
							         
							         To validate that Canadian postal code has a space, use this: "[ABCEGHJKLMNPRSTVXY]\d[A-Z] \d[A-Z]\d"
							         To validate that Candian postal code without checking for space, use this: "^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$"  
							         
							                    --%>
						                        <asp:RegularExpressionValidator ID="PostalCodeValidator2" runat="server" Display="Dynamic"
							        ErrorMessage="<br />Please enter a valid 6-digit Canadian Postal Code." ValidationExpression="^[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy]{1}\d{1}[A-Za-z]{1} *\d{1}[A-Za-z]{1}\d{1}$"
							        ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true"
							        CssClass="errormsg" ForeColour="#aa0000"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                        <div class="CountryContainer Question">
                                            <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_CountryLbl">Pays
                                            </span><span class="fake_input">Canada</span>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelGender" class="GenderContainer Question">
                                            <asp:Label ID="GenderLbl" runat="server" Text="Sexe*" AssociatedControlID="Gender"></asp:Label>
                                            <asp:DropDownList ID="Gender" runat="server">
                                                <asp:ListItem Value="">Sélectionner</asp:ListItem>
                                                <asp:ListItem Value="F">Femme</asp:ListItem>
                                                <asp:ListItem Value="M">Homme</asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="GenderValidator" runat="server" ErrorMessage="Veuillez entrer votre Sexe."
                                                    ControlToValidate="Gender" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="BirthdayContainer Question">
                                            <asp:Label ID="DOBLbl" runat="server" Text="Date de naissance*" AssociatedControlID="yyyy"></asp:Label>
                                            <asp:DropDownList ID="yyyy" runat="server" CssClass="inputSelectBDay year">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="mm" runat="server" CssClass="inputSelectBDay month">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="dd" runat="server" CssClass="inputSelectBDay day">
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:CustomValidator ID="DOBValidator" runat="server" ErrorMessage="Please enter a valid date."
                                                    OnServerValidate="DOBValidate" EnableClientScript="false" Display="Dynamic" CssClass="errormsg validdate"></asp:CustomValidator>
                                                <asp:RequiredFieldValidator ID="yyyyValidator" runat="server" ErrorMessage="Veuillez entrer votre année de naissance."
                                                    ControlToValidate="yyyy" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg year"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="mmValidator" runat="server" ErrorMessage="Veuillez entrer votre mois de naissance."
                                                    ControlToValidate="mm" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg month"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="ddValidator" runat="server" ErrorMessage="Veuillez entrer votre année de naissance."
                                                    ControlToValidate="dd" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg day"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="seperator png-fix">
                                        </div>
                                    </div>
                                              
                                    <div id="ctl00_ContentPlaceHolder1_ucForm_QuestionPanel" class="QuestionContainer">
                        			            
                                        <div class="MarketingQuestionContainer">
				                            <div class="CustomQuestiondropdown Question">
					                            <label for="Biore_mkt_q1" id="Biore_mkt_q1LBL" class="dropdownLabel">Combien de produits pour la peau utilisez-vous quotidiennement?*</label>
					                              <asp:DropDownList ID="Biore_mkt_q1" runat="server" CssClass="dropDownAnswer">
					                                <asp:ListItem value="">Sélectionner</asp:ListItem>
						                            <asp:ListItem value="1">1</asp:ListItem>
						                            <asp:ListItem value="2">2</asp:ListItem>
						                            <asp:ListItem value="3">3</asp:ListItem>
						                            <asp:ListItem value="4">4</asp:ListItem>
						                            <asp:ListItem value="5 ou plus">5 ou plus</asp:ListItem>
					                              </asp:DropDownList>
					                              <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="v_q1" runat="server" ControlToValidate="Biore_mkt_q1" ErrorMessage="Veuillez répondre à toutes les questions requises." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
				                            </div>
				                            <div class="CustomQuestiondropdown Question">
					                            <label for="Biore_mkt_q7" id="Biore_mkt_q1LBL" class="dropdownLabel">J'accorde une plus grande attention à ma peau ou je lui consacre plus de temps que la plupart des gens*.</label>

					                                <asp:DropDownList ID="Biore_mkt_q7" runat="server" CssClass="dropDownAnswer">
					                                   <asp:ListItem value="">Sélectionner</asp:ListItem>
						                                <asp:ListItem value="Entièrement d'accord">Entièrement d'accord</asp:ListItem>
						                                <asp:ListItem value="Plutôt d'accord">Plutôt d'accord</asp:ListItem>
						                                <asp:ListItem value="Ni en accord ni en désaccord">Ni en accord ni en désaccord</asp:ListItem>
						                                <asp:ListItem value="Plutôt en désaccord">Plutôt en désaccord</asp:ListItem>
						                                <asp:ListItem value="Entièrement en désaccord">Entièrement en désaccord</asp:ListItem>
					                                </asp:DropDownList>
					                                <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="v_Biore_mkt_q7" runat="server" ControlToValidate="Biore_mkt_q7" ErrorMessage="Veuillez répondre à toutes les questions requises." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                                </div>

				                            </div>
				                            <div class="CustomQuestiondropdown Question">
					                            <label for="Biore_mkt_q8" id="Biore_mkt_q8LBL" class="dropdownLabel">Je paie volontiers plus cher les produits pour la peau dont j'ai réellement besoin*.</label>
					                            <asp:DropDownList ID="Biore_mkt_q8" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem value="">Sélectionner</asp:ListItem>
						                            <asp:ListItem value="Entièrement d'accord">Entièrement d'accord</asp:ListItem>
						                                <asp:ListItem value="Plutôt d'accord">Plutôt d'accord</asp:ListItem>
						                                <asp:ListItem value="Ni en accord ni en désaccord">Ni en accord ni en désaccord</asp:ListItem>
						                                <asp:ListItem value="Plutôt en désaccord">Plutôt en désaccord</asp:ListItem>
						                                <asp:ListItem value="Entièrement en désaccord">Entièrement en désaccord</asp:ListItem>

					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
						                            <asp:RequiredFieldValidator ID="v_Biore_mkt_q8" runat="server" ControlToValidate="Biore_mkt_q8" ErrorMessage="Veuillez répondre à toutes les questions requises." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
					                            </div>
				                            </div>
				                            <div class="CustomQuestiondropdown Question">
					                            <label for="Biore_mkt_q2" id="Biore_mkt_q2LBL" class="dropdownLabel">À quel point est-il probable que vous recommandiez vos produits pour la peau favoris à vos amis*?</label>
					                            <asp:DropDownList ID="Biore_mkt_q2" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem value="">Sélectionner</asp:ListItem>
						                            <asp:ListItem value="Très probable">Très probable</asp:ListItem>
						                            <asp:ListItem value="Plutôt probable">Plutôt probable</asp:ListItem>
						                            <asp:ListItem value="Plutôt improbable">Plutôt improbable</asp:ListItem>
						                            <asp:ListItem value="Improbable">Improbable</asp:ListItem>

					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
						                            <asp:RequiredFieldValidator ID="v_Biore_mkt_q2" runat="server" ControlToValidate="Biore_mkt_q2" ErrorMessage="Veuillez répondre à toutes les questions requises." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
					                            </div>
				                            </div>
				                            <div class="CustomQuestioncheckbox Question trait413 cbVert noneToggle1">
				                                <label for="Biore_mkt_q3" id="Biore_mkt_q3LBL" class="checkboxLabel">Quels produits Bioré<sup>MD</sup> utilisez-vous actuellement? (Sélectionnez toutes les réponses pertinentes*.)</label>
				                                <asp:CheckBoxList ID="Biore_mkt_q3" RepeatDirection="Vertical" runat="server" RepeatLayout="Flow" CssClass="checkboxAnswer">
							                        <asp:ListItem Value="NOUVEAU Nettoyant équilibrant peau mixte de BioréMD">NOUVEAU Nettoyant équilibrant peau mixte de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Nettoyant activé par la vapeur de BioréMD">Nettoyant activé par la vapeur de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Nettoyant revitalisant 4 en 1 de BioréMD">Nettoyant revitalisant 4 en 1 de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Nettoyant granuleux pour les pores de BioréMD">Nettoyant granuleux pour les pores de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Gommage quotidien purifiant de BioréMD">Gommage quotidien purifiant de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Bandes de nettoyage en profondeur de BioréMD">Bandes de nettoyage en profondeur de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Bandes de nettoyage en profondeur - Emballage assorti de BioréMD">Bandes de nettoyage en profondeur - Emballage assorti de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Bandes de nettoyage en profondeur Ultra de BioréMD">Bandes de nettoyage en profondeur Ultra de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Nettoyant glacé anti-acné de BioréMD">Nettoyant glacé anti-acné de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Astringent anti-acné de BioréMD">Astringent anti-acné de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Nettoyant chauffant en crème anti-points noirs de BioréMD">Nettoyant chauffant en crème anti-points noirs de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Lingettes démaquillantes de BioréMD">Lingettes démaquillantes de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="None of the above">Aucune de ces réponses</asp:ListItem>
							                    </asp:CheckBoxList>
							                    
			                    
				         <span class="cbinput"></span>
				         
					                            <div class="ErrorContainer">
						                            <skm:CheckBoxListValidator ID="HairAppliancesLblVal" ControlToValidate="Biore_mkt_q3" runat="server" Display="Dynamic" CssClass="errormsg" ErrorMessage="Which of the following hair appliances do you use?" />
					                            </div>
				                            </div>
				                            <div class="CustomQuestiondropdown Question">
					                            <label for="Biore_mkt_q4" id="Biore_mkt_q4LBL" class="dropdownLabel">Quelle marque de soins pour le visage utilisez-vous le plus souvent? (Sélectionnez une réponse*.)</label>
					                            
					                            <asp:DropDownList ID="Biore_mkt_q4" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem value="">Sélectionner</asp:ListItem>
						                            <asp:ListItem value="Aveeno">AveenoMD</asp:ListItem>
						                            <asp:ListItem value="Avon">AvonMD</asp:ListItem>
						                            <asp:ListItem value="Biore">Bior&#233;MD products</asp:ListItem>
						                            <asp:ListItem value="Cetaphil">CetaphilMD</asp:ListItem>
						                            <asp:ListItem value="Clean and Clear">Clean &amp; ClearMD</asp:ListItem>
						                            <asp:ListItem value="Clearasil">ClearasilMD;</asp:ListItem>
						                            <asp:ListItem value="Clinique">CliniqueMD</asp:ListItem>
						                            <asp:ListItem value="Dove">DoveMD</asp:ListItem>
						                            <asp:ListItem value="Garnier Nutritioniste">Nutritioniste de Garnier</asp:ListItem>
						                            <asp:ListItem value="L'Oreal">L' OrealMD</asp:ListItem>
						                            <asp:ListItem value="Mary Kay">Mary KayMD</asp:ListItem>
						                            <asp:ListItem value="Neutrogena">NeutrogenaMD</asp:ListItem>
						                            <asp:ListItem value="Noxzema">NoxzemaMD</asp:ListItem>
                                                    <asp:ListItem value="Olay">OlayMD</asp:ListItem>
						                            <asp:ListItem value="ProActiv">ProActivMD</asp:ListItem>
						                            <asp:ListItem value="St. Ives">St. IvesMD</asp:ListItem>
						                            <asp:ListItem value="Marque maison">Marque maison</asp:ListItem>
						                            <asp:ListItem value="Marque d'un autre grand magasin">Marque d'un autre grand magasin</asp:ListItem>
						                            <asp:ListItem value="Autre">Autre</asp:ListItem>
						                            <asp:ListItem value="Aucune de ces réponses">Aucune de ces réponses</asp:ListItem>
					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
						                             <asp:RequiredFieldValidator ID="v_Biore_mkt_q4" runat="server" ControlToValidate="Biore_mkt_q4" ErrorMessage="Veuillez répondre à toutes les questions requises." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
					                            </div>
				                            </div>
				                            <div class="CustomQuestioncheckbox Question trait419 cbVert noneToggle2">
					                            <label for="Biore_mkt_q9" id="Biore_mkt_q9LBL" class="checkboxLabel">Quels types de produits utilisez-vous généralement parmi les suivants? (Sélectionnez toutes les réponses pertinentes*.)</label>
					                           <asp:CheckBoxList ID="Biore_mkt_q9" RepeatDirection="Vertical" runat="server" RepeatLayout="Flow" CssClass="checkboxAnswer">
							                        <asp:ListItem>Nettoyant liquide, en crème ou en gel</asp:ListItem>
							                        <asp:ListItem>Lingettes nettoyantes</asp:ListItem>
							                        <asp:ListItem>Lotion ou crème hydratante pour le visage</asp:ListItem>
							                        <asp:ListItem>Lotion ou crème hydratante pour le visage avec FPS</asp:ListItem>
							                        <asp:ListItem>Nettoyant ou exfoliant pour le visage</asp:ListItem>
							                        <asp:ListItem>Toner / Astringent</asp:ListItem>
							                        <asp:ListItem>Traitement anti-acné et anti-imperfections</asp:ListItem>
							                        <asp:ListItem>Traitement contre la décoloration de la peau (dommages causés par le soleil)</asp:ListItem>
							                        <asp:ListItem>Traitement, crème ou gel contour des yeux</asp:ListItem>
							                        <asp:ListItem>Crème ou hydratant de nuit</asp:ListItem>
							                        <asp:ListItem>Démaquillant</asp:ListItem>
							                        <asp:ListItem>Aucune de ces réponses</asp:ListItem>
							                    </asp:CheckBoxList>
							                   
							                    
				         <span class="cbinput"></span>
					                            <div class="ErrorContainer">
						                            <skm:CheckBoxListValidator ID="v_Biore_mkt_q9" ControlToValidate="Biore_mkt_q9"
                                                    runat="server" Display="Dynamic" CssClass="errormsg" ErrorMessage="Veuillez répondre à toutes les questions requises." />
					                            </div>
					                           
				                            </div>
				                            <div class="CustomQuestioncheckbox Question trait415 cbVert noneToggle3">
					                            <label for="Biore_mkt_q5" id="Biore_mkt_q5LBL" class="checkboxLabel">Quelles caractéristiques, s'il y a lieu, recherchez-vous dans un soin pour le visage parmi les suivantes? (Sélectionnez toutes les réponses pertinentes*.)</label>
					                            
					                            <asp:CheckBoxList ID="Biore_mkt_q5" RepeatDirection="Vertical" runat="server" RepeatLayout="Flow" CssClass="checkboxAnswer">
							                        <asp:ListItem>Commode</asp:ListItem>
							                        <asp:ListItem>Anti-âge</asp:ListItem>
							                        <asp:ListItem>Nettoie en profondeur</asp:ListItem>
							                        <asp:ListItem>Doux, formulé pour la peau sensible</asp:ListItem>
							                        <asp:ListItem>Formulé pour la peau sujette à l'acné</asp:ListItem>
							                        <asp:ListItem>Formulé pour la peau mixte</asp:ListItem>
							                        <asp:ListItem>Facilite le démaquillage ou met en valeur le maquillage</asp:ListItem>
							                        <asp:ListItem>Aucune de ces réponses</asp:ListItem>


							                    </asp:CheckBoxList>
							                    
				         <span class="cbinput"></span>
					                            <div class="ErrorContainer">
						                            <skm:CheckBoxListValidator ID="v_Biore_mkt_q5" ControlToValidate="Biore_mkt_q5"
                                                    runat="server" Display="Dynamic" CssClass="errormsg" ErrorMessage="Veuillez répondre à toutes les questions requises." />
					                            </div>

				                            </div>
				                            <div class="CustomQuestioncheckbox Question trait416 cbVert noneToggle4">
					                            <label for="Biore_mkt_q6" id="Biore_mkt_q6LBL" class="checkboxLabel">Par quels problèmes relatifs à la peau de votre visage, s'il y a lieu, êtes-vous extrêmement ou très préoccupé parmi les suivants? (Sélectionnez toutes les réponses pertinentes*.)</label>
					                            
					                            
					                            <asp:CheckBoxList ID="Biore_mkt_q6" RepeatDirection="Vertical" runat="server" RepeatLayout="Flow" CssClass="checkboxAnswer">					                    
                                                    <asp:ListItem>Acné</asp:ListItem>
                                                    <asp:ListItem>Taches de vieillissement</asp:ListItem>
                                                    <asp:ListItem>Points noirs</asp:ListItem>
                                                    <asp:ListItem>Imperfections et boutons</asp:ListItem>
                                                    <asp:ListItem>Taches et cicatrices</asp:ListItem>
                                                    <asp:ListItem>Pattes-d'oie</asp:ListItem>
                                                    <asp:ListItem>Cernes</asp:ListItem>
                                                    <asp:ListItem>Rides</asp:ListItem>
                                                    <asp:ListItem>Poils faciaux</asp:ListItem>
                                                    <asp:ListItem>Décoloration de la peau du visage</asp:ListItem>
                                                    <asp:ListItem>Ridules</asp:ListItem>
                                                    <asp:ListItem>Manque de fermeté</asp:ListItem>
                                                    <asp:ListItem>Pores dilatés</asp:ListItem>
                                                    <asp:ListItem>Zones huileuses ou brillantes</asp:ListItem>
                                                    <asp:ListItem>Yeux bouffis</asp:ListItem>
                                                    <asp:ListItem>Rougeurs ou rosacée</asp:ListItem>
                                                    <asp:ListItem>Peau sensible</asp:ListItem>
                                                    <asp:ListItem>Dommages causés par le soleil</asp:ListItem>
                                                    <asp:ListItem>Texture irrégulière de la peau</asp:ListItem>
                                                    <asp:ListItem>Teint inégal</asp:ListItem>
                                                    <asp:ListItem>Aucune de ces réponses</asp:ListItem>
		                                        </asp:CheckBoxList>
							                    
				         <span class="cbinput"></span>
					                            <div class="ErrorContainer">
						                            <skm:CheckBoxListValidator ID="v_Biore_mkt_q6" ControlToValidate="Biore_mkt_q6"
                                                    runat="server" Display="Dynamic" CssClass="errormsg" ErrorMessage="Veuillez répondre à toutes les questions requises." />
					                            </div>
				                            </div>
				                            <div class="CustomQuestiondropdown Question trait420">
					                            <label for="Biore_mkt_q10" id="Biore_mkt_q10LBL" class="dropdownLabel">Comment décririez-vous votre type de peau? (Sélectionnez une réponse*.)</label>
                                                
                                                <asp:DropDownList ID="Biore_mkt_q10" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem value="">Sélectionner</asp:ListItem>
						                            <asp:ListItem value="Sèche">Sèche</asp:ListItem>
						                            <asp:ListItem value="Normale">Normale</asp:ListItem>

						                            <asp:ListItem value="Grasse">Grasse</asp:ListItem>
						                            <asp:ListItem value="Mixte - normale à grasse">Mixte - normale à grasse</asp:ListItem>
						                            <asp:ListItem value="Mixte - normale à sèche">Mixte - normale à sèche</asp:ListItem>
						                            <asp:ListItem value="Mixte - grasse et sèche">Mixte - grasse et sèche</asp:ListItem>
						                            
						                            

					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
						                             <asp:RequiredFieldValidator ID="v_Biore_mkt_q10" runat="server" ControlToValidate="Biore_mkt_q10" ErrorMessage="Veuillez répondre à toutes les questions requises." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
					                            </div>
				                            </div>
			                            </div>
		                            </div>
           
                                    <div class="OptinContainer">
                                        <div class="seperator png-fix"></div>
	                                    <p class="privacy">
		                                   Nous accordons beaucoup d'importance à la protection de vos renseignements personnels. Soyez assuré que Kao Canada Inc. utilisera les renseignements personnels conformément à sa <a href="http://www.kaobrands.com/privacy_policy.asp"
			                                    target="_blank">politique de protection des renseignements personnels</a>.
	                                    </p>

	                                    <div class="CurrentSiteOptinContainer">
	                                        <ul>
		                                        <li>
		                                            <asp:CheckBox ID="bioreoptin" runat="server" />
			                                        <asp:Label ID="bioreoptinLabel" runat="server" Text="Oui, je veux recevoir des renseignements et des offres sur les produits Bioré<sup>MD</sup>." AssociatedControlID="bioreoptin" CssClass="siteOptinChkbox" />

			                                        <div class="ErrorContainer">
                                    			        <skm:CheckBoxValidator ID="cbv_bioreOptin" runat="server" ControlToValidate="bioreoptin" ErrorMessage="Si vous voulez devenir un membre Bioré<sup>MD</sup> Brand, s'il vous plaît vérifier Oui!" CssClass="errormsg" Display="Dynamic"></skm:CheckBoxValidator>
				                                    </div>

		                                        </li>
	                                        </ul>
	                                    </div>
                                        
                                        <!--<div id="ctl00_ContentPlaceHolder1_ucForm_ucFormOptin_PanelMultiOptin" class="MultiBrandOptinContainer">
                                    			            
		                                    <ul>            
			                                    <li id="optinToAll">
			                                        <asp:CheckBox ID="MultiOptin" runat="server" CssClass="multiOptin" />
			                                        <asp:Label ID="MultiOptinLabel" runat="server" Text="Yes, I'd like to receive emails and newsletters from other great products from Kao Brands Company:" AssociatedControlID="MultiOptin" />
			        
			                                    </li>			

			                                    <li id="ctl00_ContentPlaceHolder1_ucForm_ucFormOptin_curelListItem">
			                                        <asp:CheckBox ID="cureloptin" runat="server" />
			                                <asp:Label ID="cureloptinLabel" runat="server" Text="Cur&eacute;l<sup>&reg;</sup></a> Skincare's full line of hand, foot and body moisturizers delivers freedom from dry skin." AssociatedControlID="cureloptin"
				                                CssClass="brandLabels" />
			                         
			                                    </li>
			                                    <li id="ctl00_ContentPlaceHolder1_ucForm_ucFormOptin_jergensListItem">
			                                        <asp:CheckBox ID="jergensoptin" runat="server" />
			                                        <asp:Label ID="jergensoptinLabel" runat="server" Text="Jergens<sup>&reg;</sup></a> Skincare collection of moisturizers delivers a natural glow, smooth and firm skin and an allure that captivates." AssociatedControlID="jergensoptin"
				                                CssClass="brandLabels" />

			                                    </li>

			                                    <li>
				                                    <p class="privDisclaimer">Before submitting your information, please view our <a
					                                    href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">Privacy Policy</a>.</p>
			                                    </li>
		                                    </ul>
                                    	
		                                    </div>	-->	

                                    </div> 

                                    <div id="submit-container" class="SubmitContainer png-fix">
                                        <asp:Button ID="Button1" runat="server" CssClass="submit buttonLink png-fix" Text="SOUMETTRE ›" OnClick="Step1_ButtonClick"  />
                                    </div>
                                    <asp:HiddenField ID="hfHookID" runat="server" />
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="OptinFormResults" runat="server">
                                 <div class="ConfrimationContainer"><h2>Merci!</h2><p>Merci pour votre intérêt dans Bioré <sup> ® </ sup> Soin. Nous nous réjouissons de vous envoyer les dernières nouvelles sur des événements passionnants, des concours et des informations produit tout au long de l'année.</p></div>  

                            </asp:Panel>
                            <asp:Panel ID="OptinError" runat="server">
Une erreur est survenue en tentant de soumettre vos informations. <asp:Literal ID="litError" runat="server"></asp:Literal>
                            </asp:Panel>
                            <div class="FormBottom png-fix">
                            </div>
                            <!-- Disclaimer -->
                            <div id="DisclaimerContainer" class="png-fix">
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">

                    </script>

                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script type="text/javascript">
        $(function() {

            if ($(".signUp").length) {
                toggle("noneToggle1");
                toggle("noneToggle2");
                toggle("noneToggle3");
                toggle("noneToggle4");
            }

            $("#ctl00_ContentPlaceHolder1_cureloptin, #ctl00_ContentPlaceHolder1_jergensoptin").click(function() {
                if ($('#ctl00_ContentPlaceHolder1_cureloptin:checked').val()
                    || $('#ctl00_ContentPlaceHolder1_jergensoptin:checked').val()) 
                {
                        $('#ctl00_ContentPlaceHolder1_MultiOptin').attr("checked", "true");
                }
                else { $('#ctl00_ContentPlaceHolder1_MultiOptin').removeAttr("checked"); }
            });
            $("#ctl00_ContentPlaceHolder1_MultiOptin").click(function() {
                if ($('#ctl00_ContentPlaceHolder1_MultiOptin:checked').val()) {
                    $("#ctl00_ContentPlaceHolder1_cureloptin").attr("checked", "true");
                    $("#ctl00_ContentPlaceHolder1_jergensoptin").attr("checked", "true");
                } else {
                    $("#ctl00_ContentPlaceHolder1_cureloptin").removeAttr("checked");
                    $("#ctl00_ContentPlaceHolder1_jergensoptin").removeAttr("checked");
                }
            });
        });
        
        
        function toggle(className) {
            var checkToggle = $("." + className + " input:last");
            checkToggle.click(function() {
                if ($("#" + checkToggle.attr('id') + ":checked").val()) {
                    $("." + className + " input").each(function() {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).attr("checked", false);
                            $(this).attr('disabled', 'disabled');
                        }
                    });
                } else {
                    $("." + className + "  input").each(function() {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).removeAttr('disabled');
                        }
                    });
                }
            });
        }
    </script>

</asp:Content>
