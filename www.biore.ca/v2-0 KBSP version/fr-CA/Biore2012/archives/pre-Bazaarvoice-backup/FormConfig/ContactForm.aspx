﻿<%@ Page Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="ContactForm.aspx.cs" Inherits="Biore2012.FormConfig.ContactForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <%#SquishIt.Framework.Bundle .Css()
        .Add("~/css/formConfig.css")
        .Render("~/css/combinedform_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <div id="ctl00_ContentPlaceHolder1_ucForm_PageContainer" class="responsive contact">
	
                <div id="contactFormWrap">

                    <div id="BrandImageContainer" class="png-fix">
                        <img id="ctl00_ContentPlaceHolder1_ucForm_Image1" class="png-fix" src="images/forms/contactUsPhoto.jpg" style="border-width:0px;" />	
	                </div>
                	
	                <div id="ContactFormContainer">
	                     <!-- Header --> 
                        <div id="formHeader">
                            <h1 id="ctl00_ContentPlaceHolder1_ucForm_PageHeader">
                                POUR NOUS JOINDRE
                            </h1> 
                        </div>
                         
                        <!-- Description --> 
                        <div id="ctl00_ContentPlaceHolder1_ucForm_DescriptionContainer" class="DescriptionContainer png-fix">

                		 
                            <h2>By Mail</h2><p>Consumer Care Dept.<br>Kao Brands Company<br>2535 Spring Grove Ave<br>Cincinnati, OH 45214</p><h2>By Phone</h2><p>If you're calling from the United States, Canada or Puerto Rico, you can <span>call us  toll free at 1-800-742-8798</span>, between 9am - 5pm (EST). If you're calling from Australia, you can call us toll free at 1-800-468-318.</p><h2>Online</h2><p>Fill out the form below.</p>  
                        
	                </div>     

                        
                        <!-- From Fields --> 
                        <div id="ctl00_ContentPlaceHolder1_ucForm_PanelForm" class="FormContainer">
                		              
                            <p class="req png-fix"><em>Required*</em></p>

                            <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelMemberInfo" class="MemberInfoContainer png-fix">
                			

                    <div class="NameWrapper">
	                     <div class="FirstNameContainer Question">
		                    <label for="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_FName" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_FNameLbl">First Name*</label>
		                    <input name="ctl00$ContentPlaceHolder1$ucForm$ucFormMemberInfo$FName" type="text" maxlength="50" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_FName" class="inputTextBox" />
		                    <div class="ErrorContainer">
		                        <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_FNameValidator1" class="errormsg" style="color:Red;display:none;">Please enter your First Name.</span>
			                </div>

			                <div class="ErrorContainer">
		                        <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_FNameValidator2" class="errormsg" style="color:Red;display:none;">The characters '>' and '<' are not permitted. Please re-enter your First Name.</span>
			                </div>
	                        <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_RegularExpressionValidator1" class="errormsg" style="color:Red;display:none;">Please limit the entry to 50 characters.</span>

	                    </div>
	                     <div class="LastNameContainer Question">
		                    <label for="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_LName" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_LNameLbl">Last Name*</label>

		                    <input name="ctl00$ContentPlaceHolder1$ucForm$ucFormMemberInfo$LName" type="text" maxlength="50" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_LName" class="inputTextBox" />
		                    <div class="ErrorContainer">
		                        <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_LNameValidator1" class="errormsg" style="color:Red;display:none;">Please enter your Last Name.</span>
			                </div>
			                <div class="ErrorContainer">
		                        <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_LNameValidator2" class="errormsg" style="color:Red;display:none;">The characters '>' and '<' are not permitted. Please re-enter your Last Name.</span>
		                    </div>

		                    <div class="ErrorContainer">
		                    <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_RegularExpressionValidator2" class="errormsg" style="color:Red;display:none;">Please limit the entry to 50 characters.</span>
			                </div>	
	                    </div>
	                </div>
                	   
	                    <div class="EmailWrapper">
	                    <div class="EmailContainer Question">
		                    <label for="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Email" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_EmailLbl">Email*</label>

		                    <input name="ctl00$ContentPlaceHolder1$ucForm$ucFormMemberInfo$Email" type="text" maxlength="150" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Email" class="inputTextBox" />
		                    <div class="ErrorContainer">
		                        <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_EmailValidator1" class="errormsg" style="color:Red;display:none;">Please enter your Email Address.</span>
		                    </div>
		                    <div class="ErrorContainer">
		                        <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_EmailValidator2" class="errormsg" style="color:Red;display:none;">Please enter a valid Email Address.</span>
		                    </div>
		                    <div class="ErrorContainer">

		                       <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_RegularExpressionValidator3" class="errormsg" style="color:Red;display:none;">Please limit the entry to 150 characters.</span>
	                        </div>
	                    </div>		    
	                    <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelConfirmEmail" class="ConfirmEmailContainer Question">
                				
		                    <label for="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Email" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_ConfirmEmailLbl">Confirm Email*</label>
		                    <input name="ctl00$ContentPlaceHolder1$ucForm$ucFormMemberInfo$ConfirmEmail" type="text" maxlength="150" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_ConfirmEmail" class="inputTextBox" />
		                    <div class="ErrorContainer">
		                        <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_RequiredFieldValidator1" class="errormsg" style="color:Red;display:none;">Please confirm your Email Address.</span>

                            </div>
                            <div class="ErrorContainer">
                                <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_CompareEmailValidator" class="errormsg" style="color:Red;display:none;">The email values do not match</span>    
	                        </div>
                	    
			                </div>
		                </div>
		                <div class="seperator png-fix"></div>
                		
                	
                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelAddress" class="AddressContainer">
                				  
                            <div class="AddressWrapper">

                                <div class="Address1Container Question">  
				                    <label for="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Address1" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Address1Lbl">Address 1*</label>
			                        <input name="ctl00$ContentPlaceHolder1$ucForm$ucFormMemberInfo$Address1" type="text" maxlength="50" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Address1" class="inputTextBox" />
			                        <div class="ErrorContainer">
			                            <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Address1Validator1" class="errormsg" style="color:Red;display:none;">Please enter your Address.</span>
			                        </div>
			                        <div class="ErrorContainer">
			                            <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Address1Validator2" class="errormsg" style="color:Red;display:none;">Please do not use special characters in the Address 1 field.</span>

		                           </div>
		                           <div class="ErrorContainer">
		                              <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_RegularExpressionValidator4" class="errormsg" style="color:Red;display:none;">Please limit the entry to 50 characters.</span>
			                        </div>
			                    </div>
		                        <div class="Address2Container Question">
			                        <label for="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Address2" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Address2Lbl">Address 2</label>
			                        <input name="ctl00$ContentPlaceHolder1$ucForm$ucFormMemberInfo$Address2" type="text" maxlength="50" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Address2" class="inputTextBox" />

			                        <div class="ErrorContainer">
			                            <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Address2Validator1" class="errormsg" style="color:Red;display:none;">Please do not use special characters in the Address 2 field.</span>
		                           </div>
		                            <div class="ErrorContainer">
		                                <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_RegularExpressionValidator5" class="errormsg" style="color:Red;display:none;">Please limit the entry to 50 characters.</span>
			                        </div>	        
		                        </div>
		                        <div class="Address3Container Question">

			                        <label for="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Address3" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Address3Lbl">Address 3</label>
			                        <input name="ctl00$ContentPlaceHolder1$ucForm$ucFormMemberInfo$Address3" type="text" maxlength="50" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Address3" class="inputTextBox" />
			                        <div class="ErrorContainer">
			                            <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Address3Validator1" class="errormsg" style="color:Red;display:none;">Please do not use special characters in the Address 3 field.</span>
				                    </div>
		                            <div class="ErrorContainer">
                                        <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_RegularExpressionValidator6" class="errormsg" style="color:Red;display:none;">Please limit the entry to 50 characters.</span>

			                        </div>
		                        </div>
		                        <div class="CityContainer Question">
			                        <label for="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_City" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_CityLbl">City*</label>
			                        <input name="ctl00$ContentPlaceHolder1$ucForm$ucFormMemberInfo$City" type="text" maxlength="50" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_City" class="inputTextBox" />
			                        <div class="ErrorContainer">
			                            <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_CityValidator1" class="errormsg" style="color:Red;display:none;">Please enter your City.</span>
				                    </div>

			                        <div class="ErrorContainer">
			                            <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_CityValidator2" class="errormsg" style="color:Red;display:none;">Please do not use special characters in the City field.</span>
		                            </div>
		                            <div class="ErrorContainer">
		                                <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_RegularExpressionValidator7" class="errormsg" style="color:Red;display:none;">Please limit the entry to 50 characters.</span>
		                            </div>
		                        </div>	
		                    </div>		    	
		                    <div class="StateContainer Question">

			                    <label for="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_State" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_StateLbl">State*</label>
			                    <select name="ctl00$ContentPlaceHolder1$ucForm$ucFormMemberInfo$State" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_State">
					                <option selected="selected" value="">Select State</option>
					                <option value="AL">Alabama</option>
					                <option value="AK">Alaska</option>
					                <option value="AZ">Arizona</option>

					                <option value="AR">Arkansas</option>
					                <option value="CA">California</option>
					                <option value="CO">Colorado</option>
					                <option value="CT">Connecticut</option>
					                <option value="DE">Delaware</option>
					                <option value="DC">District Of Columbia</option>

					                <option value="FL">Florida</option>
					                <option value="GA">Georgia</option>
					                <option value="HI">Hawaii</option>
					                <option value="ID">Idaho</option>
					                <option value="IL">Illinois</option>
					                <option value="IN">Indiana</option>

					                <option value="IA">Iowa</option>
					                <option value="KS">Kansas</option>
					                <option value="KY">Kentucky</option>
					                <option value="LA">Louisiana</option>
					                <option value="ME">Maine</option>
					                <option value="MD">Maryland</option>

					                <option value="MA">Massachusetts</option>
					                <option value="MI">Michigan</option>
					                <option value="MN">Minnesota</option>
					                <option value="MS">Mississippi</option>
					                <option value="MO">Missouri</option>
					                <option value="MT">Montana</option>

					                <option value="NE">Nebraska</option>
					                <option value="NV">Nevada</option>
					                <option value="NH">New Hampshire</option>
					                <option value="NJ">New Jersey</option>
					                <option value="NM">New Mexico</option>
					                <option value="NY">New York</option>

					                <option value="NC">North Carolina</option>
					                <option value="ND">North Dakota</option>
					                <option value="OH">Ohio</option>
					                <option value="OK">Oklahoma</option>
					                <option value="OR">Oregon</option>
					                <option value="PA">Pennsylvania</option>

					                <option value="RI">Rhode Island</option>
					                <option value="SC">South Carolina</option>
					                <option value="SD">South Dakota</option>
					                <option value="TN">Tennessee</option>
					                <option value="TX">Texas</option>
					                <option value="UT">Utah</option>

					                <option value="VT">Vermont</option>
					                <option value="VA">Virginia</option>
					                <option value="WA">Washington</option>
					                <option value="WV">West Virginia</option>
					                <option value="WI">Wisconsin</option>
					                <option value="WY">Wyoming</option>

				                </select>
			                    <div class="ErrorContainer">
			                        <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_StateValidator" class="errormsg" style="color:Red;display:none;">Please select your State.</span>
				                </div>
		                    </div>  	
                        
			                </div>
                        
                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelPostalCode" class="PostalCodeContainer Question">
                				
                            <label for="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PostalCode" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PostalCodeLbl">ZIP Code*</label>

	                        <input name="ctl00$ContentPlaceHolder1$ucForm$ucFormMemberInfo$PostalCode" type="text" maxlength="5" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PostalCode" class="inputTextBox" />
	                        <div class="ErrorContainer">
	                            <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PostalCodeValidator1" class="errormsg" style="color:Red;display:none;">Please enter your ZIP Code.</span>						
	                        </div>
                            <div class="ErrorContainer">
                                <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PostalCodeValidator3" class="errormsg" style="color:Red;display:none;">Please enter a valid 5 digit ZIP code.</span>
                                
                            </div>
                        
			                </div>

                        
                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelCoutnry" class="CountryContainer Question">
                				
                            <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_CountryLbl">Country </span>
	                        <span class="fake_input">United States</span>
                        
			                </div>      
                         <div class="seperator png-fix"></div>
                   
                   
                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelGender" class="GenderContainer Question">
                				
	                        <label for="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Gender" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_GenderLbl">Gender</label>            
	                        <select name="ctl00$ContentPlaceHolder1$ucForm$ucFormMemberInfo$Gender" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Gender">

					                <option value="">Select</option>
					                <option value="Female">Female</option>
					                <option value="Male">Male</option>

				                </select>
	                        <div class="ErrorContainer">
                	            
    	                    </div>
                    	
			                </div>

                    	
		                <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelDOB" class="BirthdayContainer Question">
                				
		                    <label for="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_yyyy" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_DOBLbl">Birthdate*</label>
		                    <select name="ctl00$ContentPlaceHolder1$ucForm$ucFormMemberInfo$mm" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_mm" class="inputSelectBDay month">
					                <option value="">Mon</option>
					                <option value="1">1</option>
					                <option value="2">2</option>
					                <option value="3">3</option>

					                <option value="4">4</option>
					                <option value="5">5</option>
					                <option value="6">6</option>
					                <option value="7">7</option>
					                <option value="8">8</option>
					                <option value="9">9</option>

					                <option value="10">10</option>
					                <option value="11">11</option>
					                <option value="12">12</option>

				                </select>
		                    <select name="ctl00$ContentPlaceHolder1$ucForm$ucFormMemberInfo$dd" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_dd" class="inputSelectBDay day">
					                <option value="">Day</option>

					                <option value="1">1</option>
					                <option value="2">2</option>
					                <option value="3">3</option>
					                <option value="4">4</option>
					                <option value="5">5</option>
					                <option value="6">6</option>

					                <option value="7">7</option>
					                <option value="8">8</option>
					                <option value="9">9</option>
					                <option value="10">10</option>
					                <option value="11">11</option>
					                <option value="12">12</option>

					                <option value="13">13</option>
					                <option value="14">14</option>
					                <option value="15">15</option>
					                <option value="16">16</option>
					                <option value="17">17</option>
					                <option value="18">18</option>

					                <option value="19">19</option>
					                <option value="20">20</option>
					                <option value="21">21</option>
					                <option value="22">22</option>
					                <option value="23">23</option>
					                <option value="24">24</option>

					                <option value="25">25</option>
					                <option value="26">26</option>
					                <option value="27">27</option>
					                <option value="28">28</option>
					                <option value="29">29</option>
					                <option value="30">30</option>

					                <option value="31">31</option>

				                </select>
		                    <select name="ctl00$ContentPlaceHolder1$ucForm$ucFormMemberInfo$yyyy" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_yyyy" class="inputSelectBDay year">
					                <option value="">Year</option>
					                <option value="1999">1999</option>
					                <option value="1998">1998</option>

					                <option value="1997">1997</option>
					                <option value="1996">1996</option>
					                <option value="1995">1995</option>
					                <option value="1994">1994</option>
					                <option value="1993">1993</option>
					                <option value="1992">1992</option>

					                <option value="1991">1991</option>
					                <option value="1990">1990</option>
					                <option value="1989">1989</option>
					                <option value="1988">1988</option>
					                <option value="1987">1987</option>
					                <option value="1986">1986</option>

					                <option value="1985">1985</option>
					                <option value="1984">1984</option>
					                <option value="1983">1983</option>
					                <option value="1982">1982</option>
					                <option value="1981">1981</option>
					                <option value="1980">1980</option>

					                <option value="1979">1979</option>
					                <option value="1978">1978</option>
					                <option value="1977">1977</option>
					                <option value="1976">1976</option>
					                <option value="1975">1975</option>
					                <option value="1974">1974</option>

					                <option value="1973">1973</option>
					                <option value="1972">1972</option>
					                <option value="1971">1971</option>
					                <option value="1970">1970</option>
					                <option value="1969">1969</option>
					                <option value="1968">1968</option>

					                <option value="1967">1967</option>
					                <option value="1966">1966</option>
					                <option value="1965">1965</option>
					                <option value="1964">1964</option>
					                <option value="1963">1963</option>
					                <option value="1962">1962</option>

					                <option value="1961">1961</option>
					                <option value="1960">1960</option>
					                <option value="1959">1959</option>
					                <option value="1958">1958</option>
					                <option value="1957">1957</option>
					                <option value="1956">1956</option>

					                <option value="1955">1955</option>
					                <option value="1954">1954</option>
					                <option value="1953">1953</option>
					                <option value="1952">1952</option>
					                <option value="1951">1951</option>
					                <option value="1950">1950</option>

					                <option value="1949">1949</option>
					                <option value="1948">1948</option>
					                <option value="1947">1947</option>
					                <option value="1946">1946</option>
					                <option value="1945">1945</option>
					                <option value="1944">1944</option>

					                <option value="1943">1943</option>
					                <option value="1942">1942</option>
					                <option value="1941">1941</option>
					                <option value="1940">1940</option>
					                <option value="1939">1939</option>
					                <option value="1938">1938</option>

					                <option value="1937">1937</option>
					                <option value="1936">1936</option>
					                <option value="1935">1935</option>
					                <option value="1934">1934</option>
					                <option value="1933">1933</option>
					                <option value="1932">1932</option>

					                <option value="1931">1931</option>
					                <option value="1930">1930</option>
					                <option value="1929">1929</option>
					                <option value="1928">1928</option>
					                <option value="1927">1927</option>
					                <option value="1926">1926</option>

					                <option value="1925">1925</option>
					                <option value="1924">1924</option>
					                <option value="1923">1923</option>
					                <option value="1922">1922</option>
					                <option value="1921">1921</option>
					                <option value="1920">1920</option>

					                <option value="1919">1919</option>
					                <option value="1918">1918</option>
					                <option value="1917">1917</option>
					                <option value="1916">1916</option>
					                <option value="1915">1915</option>
					                <option value="1914">1914</option>

					                <option value="1913">1913</option>

				                </select>
		                    <div class="ErrorContainer">
		                    <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_mmValidator1" class="errormsg month" style="color:Red;display:none;">Please enter your Birth Month.</span>
		                    </div>
		                    <div class="ErrorContainer">
		                    <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_ddValidator2" class="errormsg day" style="color:Red;display:none;">Please enter your Birth Day.</span>

		                    </div>
		                    <div class="ErrorContainer">
		                    <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_yyyyValidator3" class="errormsg year" style="color:Red;display:none;">Please enter your Birth Year.</span>
		                    </div>
		                    <div class="ErrorContainer">
                		    
		                    </div>
		                    <div class="ErrorContainer">
                		    
		                    </div>

                		
			                </div>
                		
		                <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelPhone" class="PhoneContainer Question">
                				   
	                        <label for="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Phone" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PhoneLbl">Phone</label>
			                <input name="ctl00$ContentPlaceHolder1$ucForm$ucFormMemberInfo$Phone" type="text" maxlength="50" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_Phone" />
			                <div class="ErrorContainer">
			                    <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PhoneValidator1" class="errormsg" style="color:Red;display:none;">Your Phone is not required, but please do not use special characters in the Phone field.</span>
			                </div>
			                <div class="ErrorContainer">

			                    <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_RegularExpressionValidator8" class="errormsg" style="color:Red;display:none;">Please limit the entry to 50 characters.</span>
	                        </div>
                	    
			                </div> 	   
                		
		                <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelComment" class="CommentContainer Question">
                				   
			                    <label for="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_QuestionComment" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_QuestionCommentLbl">Question / Comment*</label>
			                    <textarea name="ctl00$ContentPlaceHolder1$ucForm$ucFormMemberInfo$QuestionComment" rows="2" cols="20" id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_QuestionComment"></textarea>
			                <div class="ErrorContainer">
			                    <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_QuestionCommentValidator" class="errormsg questioncomment" style="color:Red;display:none;">Please complete: Question / Comment.</span>

		                    </div>
		                    <div class="ErrorContainer">
		                    <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_RegularExpressionValidator9" class="errormsg" style="color:Red;display:none;">Please limit the entry to 5000 characters.</span>
		                    </div>
		                    <div class="ErrorContainer">
			                    <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_RegularExpressionValidator10" class="errormsg" style="color:Red;display:none;">The characters '>' and '<' are not permitted.</span>
			                </div>

                		
			                </div>
                        <div class="seperator png-fix"></div>


		                </div>                    
                            <div id="ctl00_ContentPlaceHolder1_ucForm_QuestionPanel" class="QuestionContainer">
                			            
                            
		                </div>
                           
                            <div id="submit-container" class="SubmitContainer png-fix">
                                <input type="submit" name="ctl00$ContentPlaceHolder1$ucForm$submit" value="SUBMIT ›" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$ucForm$submit&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))" id="ctl00_ContentPlaceHolder1_ucForm_submit" class="submit buttonLink png-fix" />
                            </div>

                        
	                </div>    
                              
                        <div class="FormBottom png-fix"></div>
                                  
                        <!-- Disclaimer --> 
                        <div id="DisclaimerContainer" class="png-fix">
                              
                        </div>
                        
	                </div>
                </div>
                <script type="text/javascript">

                </script>

            </div>

                <div class="clear"></div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
<script type="text/javascript">
    $(function() {
        if ($(".signUp").length) {
            toggle("noneToggle1");
            toggle("noneToggle2");
            toggle("noneToggle3");
            toggle("noneToggle4");
        }
    });
    function toggle(className) {
        var checkToggle = $("." + className + " input:last");
        checkToggle.click(function() {
            if ($("#" + checkToggle.attr('id') + ":checked").val()) {
                $("." + className + " input").each(function() {
                    if ($(this).attr('id') != checkToggle.attr('id')) {
                        $(this).attr("checked", false);
                        $(this).attr('disabled', 'disabled');
                    }
                });
            } else {
                $("." + className + "  input").each(function() {
                    if ($(this).attr('id') != checkToggle.attr('id')) {
                        $(this).removeAttr('disabled');
                    }
                });
            }
        });
    }
</script>
</asp:Content>