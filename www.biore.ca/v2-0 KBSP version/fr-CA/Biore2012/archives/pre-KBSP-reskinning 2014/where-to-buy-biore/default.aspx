﻿<%@ Page Title="Où se procurer les produits? | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.where_to_buy._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Quickly find where to buy your favorite Bioré® products" name="description" />
    <meta content="where to buy Bioré products, find Bioré retailers" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/whereToBuy.css")
        .Render("~/css/combinedbuy_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <h1>O&ugrave; se procurer les produits?</h1>
                <div id="logoHolder">
                    <div id="storeLogos" class="logos">
                        <p>Achetez les produits Bior&eacute;<sup>MD</sup> chez un détaillant près de chez vous :</p>
                        <ul>
                            <li><a style="background-image: url(../images/whereToBuy/brunet.png);">brunet</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/familiprix.png);">familiprix</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/jean-coutu.png);">jean-coutu</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/london-drugs.png);">london-drugs</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/overwaitea.png);">overwaitea</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/pharmaprix.png);">pharmaprix</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/price-smart-foods.png);">price-smart-foods</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/rexall.png);">rexall</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/safeway.png);">safeway</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/saveonfoods.png);">saveonfoods</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/drug-mart.png);">drug-mart</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/target.png);">Target</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/uniprix.png);">uniprix</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/walmart.png);">walmart</a></li>
                        </ul>                
                    </div>
                </div>
                <!--<div id="productShot">
                    <img src="../images/whereToBuy/productGrouping.jpg" alt="" />
                </div>-->
            </div>
        </div>
    </div>
</asp:Content>

