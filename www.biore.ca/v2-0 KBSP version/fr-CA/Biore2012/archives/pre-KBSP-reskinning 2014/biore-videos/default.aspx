﻿<%@ Page Title="Watch it Work | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.watch_it_work._default" %>
<script Language="c#" runat="server">
  void Page_Load(object source, EventArgs e)
  {
    Response.Redirect("/fr-CA/");
  }
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Voyez les résultats | Soins pour la peau BioréMD." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/videoLanding.css")
        .Render("~/css/combinedvideo_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <div class="intro">
                    <h1>Voyez les résultats</h1>
                    <p>Voyez à l’œuvre les bandes de nettoyage et les nettoyants pour les pores.</p>
                </div>
                <div id="content">
                    <ul>
                        <li>
                            <div class="videoHolder">
                                <div class="video pie">
                                    <div id="video2">
                                        <a class="videoLink" target="_blank" href="../video/TOWELETTES_US_512x288_h264_768Kbps.mp4" rel="videoParams::512|288|34">
                                            <img src="../images/videoLanding/videoStill2.jpg" alt="" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="copyAndImg">
                                <img src="../images/videoLanding/murt.jpg" alt="" />
                                <h3>LINGETTES DÉMAQUILLANTES</h3>
                                <p>Les lingettes démaquillantes enlèvent le maquillage – même le mascara hydrofuge – sans laisser de résidus.</p>
                            </div>  
                            <div class="clear"></div> 
                        </li>
                        <%-- DEACTIVATED PER KAO
                        <li>
                            <div class="videoHolder">
                                <div class="video pie">
                                    <div id="video1">
                                        <a class="videoLink" target="_blank" href="../video/PORE_STRIPS_US_512x288_h264_768Kbps.mp4" rel="videoParams::512|288|40">
                                            <img src="../images/videoLanding/videoStill1.jpg" alt="" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="copyAndImg">
                                <img src="../images/videoLanding/poreStrips.jpg" alt="" />
                                <h3>Bandes de nettoyage en profondeur de Bioré<sup style="font-size:10px;">MD</sup></h3>
                                <p>Nos bandes de nettoyage en profondeur éliminent les résidus que votre visage accumule pendant une semaine. La saleté qui se retrouve sur la bande en est la preuve.</p>
                            </div>   
                            <div class="clear"></div>                      
                        </li>
                        
                        <li>
                            <div class="videoHolder">
                                <div class="video pie">
                                    <div id="video3">
                                        <a class="videoLink" target="_blank" href="../video/CLEANSER_US_512x288_h264_768Kbps.mp4" rel="videoParams::512|288|40">
                                            <img src="../images/videoLanding/videoStill3.jpg" alt="" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="copyAndImg">
                                <img src="../images/videoLanding/csbc.jpg" alt="" />
                                <h3><span class="new">NOUVEAU</span> NETTOYANT ÉQUILIBRANT PEAU MIXTE</h3>
                                <p>Obtenez un nettoyage uniforme et complet sans trop assécher la peau.</p>
                            </div>
                            <div class="clear"></div>    
                        </li>
                        --%>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    
<%=SquishIt.Framework.Bundle .JavaScript()
            .Add("~/js/swfobject.min.js")
        .Render("~/js/combinedvideo_#.js")
%>
</asp:Content>
