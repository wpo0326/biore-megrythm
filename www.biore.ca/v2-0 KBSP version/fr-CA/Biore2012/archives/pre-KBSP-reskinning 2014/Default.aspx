﻿<%@ Page Title="Get a Clean Face | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Get healthy, radiant skin with products from Bioré® Skincare" name="description" />
    <meta content="skincare, clean face" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/homepage.css")
        .Add("~/css/flexslider.css")  
        .Render("~/CSS/combinedhome_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>       
       <div class="flexslider" id="theater">
            <ul class="slides">
    	        <li id="theaterItem5" class="theaterHolder">
    		        <div class="theaterBox">
    		            <h1>BONJOUR ÉQUIPE ANTI-ACNÉ ADIEU BOUTONS!</h1>
    		            <p>
    		                <!--<span>Hello, Clearer Healthier Skin - in <strong>Just Two Days</strong>.</span>-->
    	                    <asp:HyperLink ID="blemishFIghting" NavigateUrl="biore-facial-cleansing-products" runat="server">
    	                        <img class="normal" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/learnMoreBtn.png") %>" alt="Learn More" />
    	                        <img class="over" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/learnMoreBtnOver.png") %>" alt="Learn More" />
    	                    </asp:HyperLink>
    		            </p>
    		            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/theater5Bg.jpg") %>" alt="" />
    		        </div>
    	        </li>
    	        <li id="theaterItem4" class="theaterHolder">
    		        <div class="theaterBox">
    		            <h1>Évaluez nos produits</h1>
    		            <p>
    	                    <asp:HyperLink ID="proveItTheaterLink" NavigateUrl="biore-facial-cleansing-products" runat="server">
    	                        <img class="normal" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/writeAReviewBtn.png") %>" alt="Évaluez nos produits" />
    	                        <img class="over" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/writeAReviewBtnOver.png") %>" alt="Évaluez nos produits" />
    	                    </asp:HyperLink>
    		            </p>
    		            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/theater4Bg.jpg") %>" alt="" />
    		        </div>
    	        </li>
    	        <li id="theaterItem1" class="theaterHolder">
    		        <div class="theaterBox">
    		            <h1>ADIEU EXFOLIANTS IRRITANTS!</h1>
    		            <p>
    		                <span>Bonjour peau nette et d’apparence plus saine, en <strong>seulement deux jours</strong>.</span>
    	                    <asp:HyperLink ID="acneScrub" NavigateUrl="complexion-clearing-products/acne-clearing-scrub" runat="server">
    	                        <img class="normal" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/learnMoreBtn.png") %>" alt="Learn More" />
    	                        <img class="over" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/learnMoreBtnOver.png") %>" alt="Learn More" />
    	                    </asp:HyperLink>
    		            </p>
    		            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/theater1Bg.jpg") %>" alt="" />
    		        </div>
    	        </li>
            </ul>
        </div>
        <div class="centeringDiv" id="pagerWrapper"><div id="pager"></div></div>
    </div>
    <div id="promoHolder">
        <div id="promo1" class="promo">
            <div class="promoImage"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/promo3Img.jpg") %>" alt="" /></div>
            <div class="promoContent">
                <div class="promoHeader">Joignez-vous au mouvement</div>
                <div class="promoBody">Inscrivez-vous pour connaître les offres et les promotions des soins pour la peau Bioré<sup>MD</sup>.</div>
                <div class="promoButton"><a href="email-newsletter-sign-up">Jetez-y un coup d’œil <span class="arrow">&rsaquo;</span></a></div>
            </div>
        </div>
        <div id="promo2" class="promo">
            <!--<div class="promoImage"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/promo4Img.jpg") %>" alt="" /></div>-->
            <div class="promoContent" style="margin-left:75px;">
                <div class="promoHeader">Nous aimerions beaucoup avoir vos commentaires!</div>
                <div class="promoBody"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/stars.jpg") %>" alt="" /></div>
                <div class="promoButton"><a href="biore-facial-cleansing-products">Évaluez nos produits <span class="arrow">&rsaquo;</span></a></div>
            </div>
        </div>
        <div id="promo3" class="promo">
            <div class="promoImage"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/promo1Img.jpg") %>" alt="" /></div>
            <div class="promoContent">
                <div class="promoHeader">Deux étapes pour un nettoyage complet</div>
                <div class="promoBody">1. Utilisez un nettoyant tous les jours<br /> 2. Utilisez une bande de nettoyage pour les pores toutes les semaines</div>
                <div class="promoButton"><a href="biore-facial-cleansing-products">Pour en savoir plus <span class="arrow">&rsaquo;</span></a></div>
            </div>
        </div>
    </div>        
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/jquery.flexslider.min.js")
        .Render("~/js/combinedhome_#.js")
    %>
    <!--<div style="width:955px; margin:-60px auto 20px;">
    *Aucun achat requis. Le concours prend fin le 3 septembre 2012 à 17 h, HE. Il s'adresse aux résidents canadiens autorisés (ayant atteint l’âge de la majorité). Pour participer, notez le NIP fourni 1) sur les produits de soins pour la peau BioréMD spécialement marqués et 2) par demande postale, ou rendez-vous au www.bioremusic.ca. Un grand prix est en jeu et consiste en quatre (4) billets pour assister au concert d’un artiste de Sony Music dans une ville canadienne; le transport terrestre; une (1) nuit d’hébergement; quatre (4) paniers-cadeaux de soins pour la peau BioréMD; quatre (4) caméras vidéo HD Bloggie SportMC de SonyMD; 500 $ CA en argent de poche accordés au gagnant et à chacun des invités. La valeur au détail approximative du grand prix est de 10 000 $ CA. Cent vingt-huit (128) prix sont à gagner chaque jour; chacun consiste en une caméra vidéo HD Bloggie SportMC de SonyMD (valeur au détail approximative de 200 $ CA chacune). Les participants sélectionnés doivent répondre à une question réglementaire d’arithmétique. Les chances de gagner varient selon le nombre de bulletins de participation admissibles reçus. Pour consulter le règlement complet du concours, la description des prix ainsi que les exigences en matière de participation et d’admissibilité, rendez-vous au <a href="http://www.bioremusic.ca" target="_blank">www.bioremusic.ca</a>.
 
    </div>-->
</asp:Content>
