﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="KaoBrands.FormStuff.ucFormUnsubscribe" Codebehind="ucFormUnsubscribe.ascx.cs" %>

<div id="unsubscribe">
		<div id="container">
			<div id="main-content">
				<div id="optOut">
				    <asp:Panel ID="OptinForm" runat="server">
					    <h1>Se désabonner</h1>
					    <p>Si vous ne souhaitez plus être informé des dernières nouvelles, des promotions et des concours, il vous suffit d’entrer votre adresse de courriel ci-dessous.</p>
					    <p class="req">obligatoire<span class="required">*</span></p>
					    <div class="tbin">
						    <div class="input-container">
							    <asp:Label ID="emailLbl" runat="server" AssociatedControlID="Email" Text="Courriel*" />
							    <asp:TextBox ID="Email" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
							    <div class="ErrorContainer">
							    <asp:RequiredFieldValidator ID="EmailValidator1" Display="Dynamic" runat="server"
								    ErrorMessage="Please enter your Email Address." ControlToValidate="Email" EnableClientScript="true"
								    SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
							    <asp:RegularExpressionValidator ID="EmailValidator2" Display="Dynamic" runat="server"
								    ErrorMessage="Please enter a valid Email Address." ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
								    ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RegularExpressionValidator>								
							    <asp:RegularExpressionValidator ID="EmailValidator3" runat="server" Display="Dynamic"
								    ErrorMessage="Please limit the entry to 50 characters."
								    ValidationExpression="^[\s\S]{0,50}$" ControlToValidate="Email" SetFocusOnError="true"
								    CssClass="errormsg"></asp:RegularExpressionValidator>
							    <asp:RegularExpressionValidator ID="EmailValidator4" runat="server" Display="Dynamic"
								    ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Email."
								    ValidationExpression="^[^<>]+$" ControlToValidate="Email" EnableClientScript="true"
								    SetFocusOnError="true" CssClass="errormsg" ></asp:RegularExpressionValidator>
							    </div>
							    <!--end Required Field Validator Container-->
						    </div>
					    </div>
					    <div id="submit-container">
						    <p>
							    <asp:Button UseSubmitBehavior="true" ID="submit" Text="Se désabonner ›" runat="server" CssClass="submit buttonLink" />
						    </p>
					    </div>
				    </asp:Panel>
				    <asp:Panel ID="OptinFormSuccess" runat="server">
					    <h1>We're sorry to see you go...</h1>
					    <p>Your request has been sent and we&rsquo;ll go ahead and remove your e-mail address from our list.</p>
					    <p>If you change your mind, you can always re-subscribe right here on our website.</p>
					    <p><a href="<%= ResolveClientUrl("~/") %>">Return home</a>.</p>
				    </asp:Panel>
				    <asp:Panel ID="OptinFormFailure" runat="server">
					    <h1>We're Sorry...</h1>
					    <p>There has been a problem with your form submission.</p>
					    <p>Please go back and <a href="javascript: history.back(-1)">try again</a>.</p>
				    </asp:Panel>
				</div> <%-- end #optOut --%>
			</div> <%-- end #main-content --%>
			<div id="unsubscribe-img">
			</div>
		</div> <%-- end #container --%>
	</div>

