<!DOCTYPE html>
<!--[if IE 7 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie7 ie"><![endif]-->
<!--[if IE 8 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie8 ie"><![endif]-->
<!--[if IE 9 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie9 ie"><![endif]-->
<!--[if !IE]><!--><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" ><!--<![endif]-->
<head>
<title>
	Bior� Face the Planet Rules and Regulations
</title>
    
    <link rel="stylesheet" type="text/css" href="/en-CA/css/global.css" />
<style type="text/css">
#mainbody{font-family:Arial;width:80%; margin-left:auto;margin-right:auto;}
h1 {padding:10px 0px 10px 0px;}
p {padding-bottom:20px}
    </style>
</head>

<body>
<div id="mainbody">
<div id="languageToggle"><a href="/en-CA/promotions/rules/ditch-the-dirt/">English</a></div>

   <h1>FINIE LA SALET� AVEC LES SOINS POUR LA PEAU BIOR�<sup>MD</sup></h1>


<p><b>R�glement de la campagne</b><br />
La campagne n�est en aucun cas commandit�e, approuv�e ou g�r�e par Facebook ou encore associ�e � Facebook. Vous convenez que les renseignements que vous communiquez le sont au commanditaire, non � Facebook. Les renseignements que vous fournissez ne seront utilis�s qu�aux fins de la gestion de la campagne, en conformit� avec la politique de confidentialit� du commanditaire (voir ci-dessous). Tout participant � la campagne d�gage Facebook de toute responsabilit� li�e � la campagne. Les questions, commentaires ou plaintes concernant la campagne doivent �tre adress�s au commanditaire, et non � Facebook.</p>

<p><b>1. DUR�E DE LA CAMPAGNE</b><br />
La campagne Finie la salet� des soins pour la peau Bior�<sup>MD</sup> (la � <strong>campagne</strong> �), d�bute � midi, HE, le 14 mai 2012 et prend fin � 17 h le 29 juin 2012  (la � 
    <strong>dur�e de la campagne</strong> �).</p>

<p><b>2. COMMANDITE</b><br />
La campagne est commandit�e et g�r�e par Kao Canada Inc. (le � <strong>commanditaire</strong> �).</p>

<p><b>3. ADMISSIBILIT�</b><br />
La campagne s�adresse � tous les r�sidents canadiens qui, au moment de leur participation, sont majeurs selon les lois de leur province ou de leur territoire de r�sidence, sauf les employ�s, les repr�sentants ou les agents (et les personnes vivant sous le m�me toit que ces derniers, qu�elles soient ou non parentes avec ceux-ci) du commanditaire, de ses soci�t�s m�res, de ses filiales, de ses soci�t�s affili�es, de ses agences de publicit� et de promotion et les fournisseurs de prix (collectivement appel�s les � 
    <strong>parties li�es � la campagne</strong> �).</p>

<p><b>4. AVIS JURIDIQUE</b><br />
La campagne est soumise aux lois et aux r�glements f�d�raux, provinciaux, territoriaux et municipaux en vigueur et est nulle l� o� la loi l�interdit.</p>

<p><b>5. COMMENT PARTICIPER</b><br />
Aucun achat requis. Pour participer, vous devez : (i) avoir acc�s � Internet, (ii) �tre (ou devenir) un adepte de la page des fans (la � 
    <strong>page des fans</strong> �) de Bior� Canada, (iii) avoir (ou obtenir) une adresse de courriel valide et un compte Facebook (le � compte �). Pour participer, rendez-vous � l�adresse www.facebook.com/BioreCanada (le � 
    <strong>site Web</strong> �) et suivez les instructions � l��cran pour : (i) cliquer sur le bouton � J�aime � (si vous n��tes pas d�j� un adepte de la page des fans), (ii) obtenir le bulletin de participation officiel de la campagne (le � 
    <strong>bulletin de participation</strong> �). Remplissez int�gralement le bulletin de participation (sans oublier d�indiquer que vous avez lu et compris les conditions du pr�sent r�glement officiel et que vous acceptez d�y �tre li� juridiquement). Le bulletin de participation demande �galement que vous r�pondiez correctement, sans aide m�canique ou autre, � une question r�glementaire d�arithm�tique. Si vous ne r�pondez pas � la question ou que vous y r�pondez incorrectement, vous ne serez pas admissible au prix (d�crit ci-dessous). Une fois le bulletin de participation d�ment rempli, vous devez suivre les instructions � l��cran et soumettre un texte de 100 mots (le nombre maximal de caract�res peut changer). Une fois tous les champs obligatoires du bulletin de participation remplis, cliquez sur le bouton � Soumettre � pour achever votre inscription (le bulletin de participation et le texte sont collectivement appel�s ci apr�s �<strong> le bulletin de participation</strong> �). Une fois que vous avez soumis un bulletin de participation admissible, celui-ci est pris en compte dans le tirage au sort vous offrant la chance de gagner un des 250 flacons de nettoyant �quilibrant peau mixte de format ordinaire. De plus, les 10 000 premiers participants admissibles recevront �galement un (1) �chantillon de nettoyant �quilibrant peau mixte de Bior�<sup>MD</sup> en sachet de 7 ml, jusqu�� �puisement des stocks (l�� 
    <strong>�chantillon</strong> �, voir le point 9). Une (1) seule inscription par personne ou adresse de courriel est autoris�e pendant la dur�e de la campagne. Pour plus de clart� : vous ne pouvez utiliser qu�une (1) seule adresse de courriel pour participer � la campagne. S�il est �tabli qu�une personne a tent� : (i) d�obtenir plus d�un bulletin de participation par personne, par adresse de courriel ou par compte pendant la dur�e de la campagne ou (ii) d�utiliser (ou de tenter d�utiliser) plus d�un nom, d�une identit�, d�une adresse de courriel ou d�un compte pour participer � la campagne, le commanditaire peut alors, � sa seule et enti�re discr�tion, disqualifier cette personne. Votre bulletin de participation sera rejet� (� la seule et enti�re discr�tion du commanditaire) : (i) si le bulletin de participation n�est pas d�ment rempli et soumis pendant la dur�e de la campagne ou (ii) si votre texte n�est pas conforme aux exigences pr�cis�es ci-dessous. Il est interdit, sous peine de disqualification par le commanditaire, d�utiliser (ou de tenter d�utiliser) plus d�un nom, d�une identit�, d�une adresse de courriel ou d�un compte, le moindre syst�me ou programme automatis� ou robotique ou encore la moindre macro ou le moindre script pour participer au concours ou pour en perturber le d�roulement. Les parties exon�r�es (d�finies ci-dessous) ne peuvent �tre tenues responsables des bulletins de participation en retard, perdus, incorrectement achemin�s, retard�s, incomplets ou inadmissibles.</p>

<p>Tous les bulletins de participation sont susceptibles d��tre v�rifi�s, � tout moment. Le commanditaire se r�serve le droit d�exiger, � sa seule et enti�re discr�tion, une preuve d�identit� et (ou) d�admissibilit� � la campagne, dont la forme est acceptable � ses yeux (y compris, sans s�y limiter, une pi�ce d�identit� avec photo �mise par un gouvernement). Le d�faut de fournir en temps opportun une telle preuve � la satisfaction du commanditaire peut entra�ner la disqualification. Le ou les serveurs de la campagne repr�sentent l�unique m�thode visant � d�terminer la date et la validit� d�un bulletin de participation � la campagne.</p>

<p><b>6. OCCASION DE DIFFUSER DE L�INFORMATION SUR LA CAMPAGNE</b><br />
Au moment de son inscription � la campagne (ou � tout autre moment par la suite au moyen de l�application), un participant admissible pourra transmettre de l�information sur la campagne � ses amis et sur son r�seau en ligne. Suivez les instructions � l��cran et cliquez sur le bouton � Diffuser � apr�s avoir rempli votre bulletin de participation pour diffuser l�information sur la campagne. Vous NE recevrez rien pour avoir diffus� l�information. </p>

<p><b>7. EXIGENCES SUR LA SOUMISSION DE TEXTES</b><br />
EN SOUMETTANT UN BULLETIN DE PARTICIPATION, VOUS CONVENEZ QUE VOTRE BULLETIN DE PARTICIPATION ET VOTRE TEXTE (AINSI QUE CHACUN DE SES �L�MENTS) SATISFONT � TOUTES LES CONDITIONS �NONC�ES DANS LE PR�SENT R�GLEMENT OFFICIEL. LES PARTIES EXON�R�ES (D�FINIES CI-DESSOUS) NE SONT AUCUNEMENT LI�ES JURIDIQUEMENT EN CE QUI CONCERNE L�UTILISATION DE VOTRE BULLETIN DE PARTICIPATION. VOUS VOUS ENGAGEZ � D�GAGER LES PARTIES EXON�R�ES (D�FINIES CI-DESSOUS) DE TOUTE RESPONSABILIT� SI L�ON D�COUVRE PAR LA SUITE QUE VOUS AVEZ D�ROG� AU PR�SENT R�GLEMENT OU QUE VOUS NE VOUS Y �TES PAS ENTI�REMENT CONFORM�. </p>

<p>En prenant part � la campagne, chaque participant accepte d��tre li� par le pr�sent r�glement officiel (le � 
    <strong>r�glement</strong> �) et par l�interpr�tation de ce dernier par le commanditaire. Il garantit en outre que son texte :</p>

<div style="width:550px;padding-left:40px">
i. a bel et bien �t� r�dig� par lui et qu�il poss�de tous les droits n�cessaires sur celui-ci exig�s aux fins de sa participation � la campagne (y compris, sans s�y limiter, le consentement � son utilisation); <br />
ii. n�est pas contraire � quelque loi, statut, ordonnance ou r�glement que ce soit;<br />
iii. ne comporte aucune r�f�rence � de tierces personnes identifiables ni aucune ressemblance avec celles-ci, si ce n�est avec le consentement de celles-ci ou de leurs parents ou tuteurs l�gaux si elles sont mineures selon les lois de leur province ou territoire de r�sidence;<br />
iv. n�est susceptible de donner lieu � aucune r�clamation pour contrefa�on, atteinte � la vie priv�e ou au droit � l�image ou atteinte aux droits de tiers, ou encore � aucune r�clamation visant � obtenir quelque paiement que ce soit;<br />
v. n�est ni diffamatoire, notamment sur le plan commercial, ni de nature pornographique ou obsc�ne et qu�il ne contient, n�inclut, n�aborde ou n��voque, notamment, aucun des �l�ments suivants : nudit�; consommation d�alcool ou de drogue ou usage du tabac; activit�s sexuelles explicitement d�peintes ou simplement sugg�r�es; propos et (ou) symboles crus, vulgaires ou offensants; descriptions m�prisantes de quelque groupe ethnique, racial, sexuel, religieux ou autre que ce soit (y compris, sans s�y limiter, des concurrents du commanditaire); contenu approuvant, justifiant et (ou) �voquant des conduites ou des comportements ill�gaux, d�plac�s ou � risque; renseignements personnels concernant des individus, notamment leur nom, leur num�ro de t�l�phone, leur adresse postale ou leur adresse �lectronique; publicit�, comparaisons ou sollicitations visant des produits ou des services autres que ceux du commanditaire; produits, marques de commerce ou autres et (ou) logos de tiers identifiables, hormis ceux du commanditaire; conduite ou activit� contraires au pr�sent r�glement officiel; et (ou) tout autre contenu inappropri�, d�plac� ou offensant ou encore susceptible d'�tre jug� comme tel, le tout �tant laiss� � la seule et enti�re discr�tion du commanditaire.</div>

<p>Si le commanditaire, son agence de promotion ou son mod�rateur de contenu d�sign� (le � 
    <strong>r�viseur</strong> �) juge, � sa seule et enti�re discr�tion, que le texte enfreint les modalit�s indiqu�es dans le pr�sent r�glement, celui-ci ne sera pas publi� sur le site Web au moyen de l�application et entra�nera la disqualification du participant. Avant la publication d�un texte sur le site Web au moyen de l�application ou de son acceptation comme bulletin de participation admissible, le r�viseur se r�serve le droit, � sa seule et enti�re discr�tion, de le modifier ou encore de demander � tout participant de modifier ou de resoumettre son texte, afin de s�assurer qu�il est conforme au pr�sent r�glement, ou pour tout autre motif.</p>
 
<p>En participant � la campagne et en soumettant un bulletin de participation, chaque participant : (i) accorde au commanditaire une licence perp�tuelle et non exclusive l�autorisant � publier, � diffuser, � reproduire, � modifier ou � utiliser de quelque autre mani�re que ce soit son texte de fa�on anonyme, en tout ou en partie, � des fins de publicit� ou de promotion de la campagne ou encore � toute autre fin, (ii) renonce � tous ses droits moraux sur ce texte au profit du commanditaire et (iii) s�engage � d�gager les parties li�es � la campagne, leurs agents, employ�s, administrateurs, successeurs et ayants droit respectifs (collectivement appel�s les � 
    <strong>parties exon�r�es</strong> �) de toute responsabilit� li�e aux r�clamations fond�es sur le droit � l�image, la diffamation, la violation de la vie priv�e, la violation de droits d�auteur, la contrefa�on de marques de commerce ou tout autre motif associ� � la propri�t� intellectuelle li�e au texte. Pour �viter toute ambigu�t� : le r�viseur se r�serve le droit, � sa seule et enti�re discr�tion, et ce, � tout moment pendant ou apr�s la campagne, de modifier ou de retirer tout texte, ou encore de demander � tout participant de modifier son texte, en cas de r�ception d�une plainte concernant celui-ci ou pour tout autre motif.</p>

<p><b>8. PRIX</b><br />
Deux cents cinquante (250) prix seront remis lors d�un tirage au sort. Chacun consiste en un (1) flacon de nettoyant �quilibrant peau mixte de Bior�<sup>MD</sup> en format de 200 ml, d�une valeur au d�tail approximative de 9,99 $ chacun (le � 
    <strong>prix</strong> �). Chaque prix doit �tre accept� tel qu�il est d�cern� et n�est ni cessible ni �changeable contre de l�argent. Aucun �change n�est accord�, sauf si le commanditaire y consent. Le commanditaire se r�serve le droit de remplacer un prix ou tout �l�ment de celui-ci, � sa seule et enti�re discr�tion, par un prix de valeur �gale ou sup�rieure, y compris, sans s�y limiter, mais � sa seule et enti�re discr�tion, par une somme d�argent. Chaque prix n�est remis qu�� la personne dont le nom complet et une adresse de courriel valide v�rifiables figurent sur le bulletin de participation. La limite est d�un (1) prix par personne.</p>

<p><b>9. �CHANTILLONS</b><br />
Les 10 000 premiers participants admissibles recevront un (1) �chantillon, qui leur sera envoy� par la poste � leur domicile dans les trois � six semaines suivant leur inscription, si leur bulletin de participation s�est av�r� admissible. Un avis sera publi� sur la page des fans une fois que les 10 000 �chantillons auront �t� distribu�s, � la fin de l�offre.</p>

<p><b>10. PROC�DURE DE S�LECTION DES GAGNANTS</b><br />
Le 7 julliet 2012 (la � <strong>date du tirage</strong> �) � Mississauga, en Ontario, vers 10 h, HE, deux cents cinquante (250) participants admissibles seront s�lectionn�s par un tirage au sort parmi tous les bulletins de participation admissibles re�us pendant la dur�e de la campagne. Les probabilit�s de gagner d�pendent du nombre de bulletins de participation admissibles re�us durant la campagne.</p>

<p>POUR �TRE PROCLAM� GAGNANT D�UN PRIX, chaque participant s�lectionn� doit r�pondre correctement, sans aide m�canique ou autre, � la question r�glementaire d�arithm�tique figurant sur le bulletin de participation. En outre, en participant � la campagne et en acceptant le prix, par les pr�sentes, vous : (i) confirmez que vous respectez le pr�sent r�glement; (ii) acceptez le prix tel qu�il est d�cern�; (iii) d�gagez les parties exon�r�es (�num�r�es ci-dessus) de toute responsabilit� eu �gard � la pr�sente campagne, � votre participation ou � la remise et � l�usage ou au mauvais usage du prix et de toute portion du prix; iv) consentez � la publication et � l�affichage de votre confession anonyme dans le cadre de l�application Finie la salet� (ou � son rejet, advenant qu�elle ne correspond pas aux lignes directrices pr�sent�es dans les exigences sur la soumission des textes, sans autre avis ni compensation, dans le cadre de toute publicit� ou promotion men�e par le commanditaire ou en son nom de quelque mani�re que ce soit, y compris dans la presse �crite, dans les �missions diffus�es ou sur Internet.  If a selected entrant : (a) ne r�pond pas correctement � la question r�glementaire figurant sur le bulletin de participation; (b) ne peut accepter le prix tel qu�il est d�cern� pour une raison quelconque, il est disqualifi� (et perd ainsi tout droit au prix) et le commanditaire se r�serve le droit, � sa seule et enti�re discr�tion, de s�lectionner au hasard un autre participant admissible parmi le reste des bulletins de participation admissibles (le cas �ch�ant, les dispositions pr�c�dentes du pr�sent paragraphe s�appliquent au nouveau participant s�lectionn�).</p>

<p>Le commanditaire ou son repr�sentant d�sign� tentera une (1) fois de faire parvenir le prix � chacun des participants s�lectionn�s. � la r�ception d�un avis indiquant l�impossibilit� d�effectuer la livraison, le participant s�lectionn� perd tout droit au prix, et le commanditaire se r�serve le droit, � sa seule et enti�re discr�tion, de s�lectionner au hasard un autre participant admissible parmi le reste des bulletins de participation admissibles (le cas �ch�ant, les dispositions pr�c�dentes du pr�sent paragraphe s'appliquent au nouveau participant s�lectionn�).</p>

<p><b>11. CONDITIONS G�N�RALES</b><br />
Tous les bulletins de participation deviennent la propri�t� du commanditaire. Les d�cisions du commanditaire concernant tout aspect de la campagne sont ex�cutoires et sans appel et lient tous les participants, y compris, sans s�y limiter, toute d�cision concernant l�admissibilit� ou la disqualification de participants et (ou) de bulletins de participation.</p>

<p>Les parties exon�r�es ne peuvent �tre tenues responsables : (i) de la moindre d�faillance du site Web ou de l�application pendant la dur�e de la campagne, (ii) de tout dysfonctionnement technique ou autre probl�me li� � un r�seau ou � des lignes t�l�phoniques, � des syst�mes informatiques en ligne, � des serveurs, � des fournisseurs d�acc�s, � du mat�riel informatique ou � des logiciels, (iii) de la non-r�ception du moindre bulletin de participation pour quelque raison que ce soit, y compris, sans s�y limiter, des probl�mes techniques ou encore de la saturation d�Internet ou d�un site Web quelconque, (iv) des dommages corporels ou encore des dommages subis par l�ordinateur ou par tout autre appareil d�un participant ou d�une autre personne li�s � la participation � la campagne ou au t�l�chargement du moindre �l�ment dans le cadre de celle-ci et (ou) (v) de toute combinaison des �l�ments pr�cit�s.</p>

<p>En cas de litige concernant la personne ayant soumis un bulletin de participation, ce dernier est r�put� avoir �t� soumis par le titulaire autoris� du compte associ� � l�adresse de courriel indiqu�e au moment de la soumission de ce bulletin de participation. Le � titulaire autoris� d�un compte � est la personne � laquelle une adresse de courriel associ�e audit compte a �t� attribu�e par un fournisseur d�acc�s � Internet ou de services en ligne ou encore par toute autre organisation (entreprise, �tablissement d�enseignement, etc.) responsable de l�attribution des adresses de courriel pour le domaine associ� � l�adresse de courriel en question. Tout participant peut �tre tenu de prouver (d�une mani�re acceptable aux yeux du commanditaire, y compris au moyen d�une pi�ce d�identit� avec photo �mise par un gouvernement) qu�il est bien le titulaire autoris� de l�adresse de courriel associ�e au bulletin de participation qu�il a soumis.</p>

<p>Sous r�serve de l�approbation de la R�gie des alcools, des courses et des jeux du Qu�bec (la � 
    <strong>R�gie</strong> �), le commanditaire se r�serve le droit, � sa seule et enti�re discr�tion, de mettre fin � la campagne, de la modifier ou de la suspendre (ou encore de modifier le pr�sent r�glement) de quelque fa�on que ce soit si une erreur, un probl�me technique, un virus informatique, un bogue, un sabotage, une intervention non autoris�e, une fraude, une d�faillance technique ou tout autre facteur pouvant raisonnablement �tre consid�r� comme ind�pendant de la volont� du commanditaire nuit au d�roulement de la campagne conform�ment au pr�sent r�glement. Toute tentative d�lib�r�e d�endommager tout site Web ou de perturber le bon d�roulement de la pr�sente campagne de quelque fa�on que ce soit (� d�terminer par le commanditaire � sa seule et enti�re discr�tion) constitue une violation des lois criminelles et p�nales et, advenant une telle tentative, le commanditaire se r�serve le droit d�engager un recours pour dommages subis, dans la pleine mesure permise par la loi. Le commanditaire se r�serve le droit, � sa seule et enti�re discr�tion et uniquement sous r�serve de l�approbation de la R�gie au Qu�bec, d�annuler, de modifier ou de suspendre la campagne ou encore de modifier le pr�sent r�glement, sans pr�avis ni obligation de sa part, en cas d�accident, d�erreur d�impression, d�erreur administrative ou d�erreur de toute autre nature, ou encore pour toute autre raison.</p>

<p>Le commanditaire se r�serve le droit, � sa seule et enti�re discr�tion, de modifier sans pr�avis, dans la mesure n�cessaire, les dates et (ou) les horaires pr�vus par le pr�sent r�glement officiel, que ce soit pour s�assurer du respect du pr�sent r�glement officiel par tout participant ou tout bulletin de participation, � la suite de probl�mes techniques, ou encore en raison d�autres circonstances que le commanditaire estime, � sa seule et enti�re discr�tion, susceptibles d�emp�cher la campagne de se d�rouler conform�ment au pr�sent r�glement officiel.</p>

<p>En cas de divergence ou d�incompatibilit� entre les modalit�s du pr�sent r�glement officiel et les d�clarations ou autres mentions figurant dans tout autre document li� � la campagne, y compris, sans s�y limiter, le bulletin de participation � la campagne, le site Web, l�application et (ou) la moindre publicit� aux points de vente, � la t�l�vision, dans la presse �crite ou en ligne, les modalit�s du pr�sent r�glement officiel pr�valent.</p>

<p>Pour les r�sidents du Qu�bec : Tout litige relatif au d�roulement ou � l�organisation d�un concours publicitaire peut �tre soumis � la R�gie des alcools, des courses et des jeux pour qu�une d�cision soit rendue. Tout litige concernant l�attribution d�un prix peut �tre soumis � la R�gie, mais uniquement dans le but d�aider les parties � parvenir � un r�glement.</p>

<p><b>12. RENSEIGNEMENTS PERSONNELS</b><br />
Kao Canada Inc. recueille des renseignements personnels sur les participants aux fins de l�administration de la campagne. Les textes demeureront anonymes. Kao Canada Inc. n'est pas autoris�e � utiliser vos renseignements personnels � des fins autres que celles pr�vues, et la campagne est assujettie aux lois f�d�rales et provinciales canadiennes sur le respect de la vie priv�e. Veuillez consulter la politique de protection des renseignements personnels de Kao Canada Inc. � http://www.biore.ca/fr-CA/privacy/ pour obtenir de l�information sur sa politique en mati�re de maintien de la confidentialit� et de la s�curit� des renseignements sur les utilisateurs.</p>

<p><b>R�GLEMENT ABR�G�</b><br />
Aucun achat requis. Le concours se termine le 29 juin 2012, � 17 h et s�adresse aux r�sidents canadiens qui ont atteint l��ge de la majorit�. Participation en ligne et r�glement complet � l�adresse www.facebook.com/BioreCanada. Il y a deux cents cinquante (250) prix � gagner. Chacun consiste en un (1) flacon de nettoyant �quilibrant peau mixte de Bior�<sup>MD</sup> en format de 200 ml (valeur au d�tail approximative : 9,99 $ chacun). La r�ponse � une question r�glementaire est exig�e. Les chances de gagner d�pendent du nombre de bulletins de participation admissibles.</p>






</div>
</body>

</html>
