<!DOCTYPE html>
<!--[if IE 7 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie7 ie"><![endif]-->
<!--[if IE 8 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie8 ie"><![endif]-->
<!--[if IE 9 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie9 ie"><![endif]-->
<!--[if !IE]><!--><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" ><!--<![endif]-->
<head>
<title>
	Campagne Au service de la plan�te des soins pour la peau Bior�MD
</title>
    
    <link rel="stylesheet" type="text/css" href="/en-CA/css/global.css" />
<style type="text/css">
#mainbody{width:80%; margin-left:auto;margin-right:auto;}
h1 {padding:10px 0px 10px 0px;}
p {padding-bottom:20px}
    .style1
    {
        font-weight: normal;
    }
    .style2
    {
        font-weight: bold;
        width: 200px;
    }
    .style3
    {
        width: 138px;
    }
    .style4
    {
        width: 200px;
    }
    .style5
    {
        text-decoration: underline;
    }
    .style6
    {
        font-weight: bold;
        text-decoration: underline;
    }
</style>
</head>

<body>
<div id="mainbody">
<div id="languageToggle"><a href="/en-CA/promotions/rules/face-the-planet.aspx">English</a></div>

    <h1>Campagne Au service de la plan�te des soins pour la peau Bior�<sup>MD</sup></h1>
    
    <h4><i><span class="style1">Soyez belle et sentez-vous bien avec les soins pour le visage de Bior�<sup>MD</sup></span></i>.</h4>

    <p><b>R�glement de la campagne</b></p>

    <p>La campagne n�est en aucun cas commandit�e, approuv�e ou g�r�e par Facebook ou encore associ�e � Facebook. Vous convenez que les renseignements que vous divulguez le sont aupr�s du commanditaire, non aupr�s de Facebook. Les renseignements que vous divulguez ne seront utilis�s qu�aux fins de la gestion de la campagne, en conformit� avec la politique de confidentialit� du commanditaire (voir ci-dessous). Tout participant � la campagne d�gage Facebook de toute responsabilit� li�e � la campagne. Les questions, plaintes ou commentaires concernant la campagne doivent �tre adress�s au commanditaire, et non � Facebook.</p>

    <p><b>1.	DUR�E DE LA CAMPAGNE</b></p>

    <p>La campagne Au service de la plan�te des soins pour la peau Bior�<sup>MD</sup> (la � 
        <b>campagne</b> �) d�bute � 12 h 00 min 01 PM HNE le 6  mars 2012 et prend fin � 23 h 59 min le 30 avril 2012  (la � 
        <b>dur�e de la campagne</b> �). 

    <p>2.<b>	COMMANDITE</b></p>

    <p>La campagne est commandit�e et g�r�e par Kao Canada Inc. (le � <b>commanditaire</b> �).</p>

    <p><b>3.	ADMISSIBILIT�</b></p>

    <p>La campagne s�adresse � tous les r�sidents canadiens qui, au moment de leur participation, sont majeurs selon les lois de leur province ou de leur territoire de r�sidence, sauf les employ�s, les repr�sentants ou les agents (et les personnes vivant sous le m�me toit que ces derniers, qu�elles soient ou non parentes avec ceux-ci) du commanditaire, ses soci�t�s m�res, ses filiales, ses soci�t�s affili�es, ses agences de publicit� et de promotion et les fournisseurs de prix (collectivement appel�s les � 
        <b>parties li�es � la campagne</b> �).</p>

    <p><b>4.	AVIS JURIDIQUE

    </b>

    <p>La campagne est soumise aux lois et aux r�glements f�d�raux, provinciaux, territoriaux et municipaux en vigueur et est nulle l� o� la loi l�interdit.</p>

    <p><b>5.	COMMENT PARTICIPER</b></p>

    <p>Aucun achat n�est requis. Pour participer, vous devez : (i) avoir acc�s � Internet, (ii) �tre (ou devenir) un adepte de la page des fans (la � 
        <b>page des fans</b> �) de Bior� Canada, (iii) avoir (ou obtenir) une adresse de courriel valide et un compte Facebook (le � 
        <b>compte</b> �). Pour participer, rendez-vous au www.facebook.com/BioreCanada (le � 
        <b>site Web</b> �) et suivez les instructions � l��cran pour : cliquer sur le bouton J�aime (si vous n��tes pas d�j� un adepte de la page des fans), (ii) accepter l�application Au service de la plan�te des soins pour la peau Bior�<sup>MD</sup> (l�� 
        <b>application</b> �) (remarque : il suffit d�accepter l�application lorsque vous visitez le site Web la premi�re fois), (iii) obtenir le bulletin de participation officiel de la campagne (le �<b> bulletin de participation</b> �). Remplissez int�gralement le bulletin de participation (sans oublier d�indiquer que vous avez lu et compris les conditions du pr�sent r�glement officiel et que vous acceptez d�y �tre li� juridiquement). Le bulletin de participation demande �galement que vous r�pondiez correctement, sans aide m�canique ou autre, � une question r�glementaire d�arithm�tique. Si vous ne r�pondez pas � la question ou que vous y r�pondez incorrectement, vous ne serez pas admissible � gagner un prix (d�crit ci-dessous). Une fois le bulletin de participation d�ment rempli, vous devez suivre les instructions � l��cran et t�l�verser une photographie (la � 
        <b>photo</b> �) montrant votre visage lorsqu�il brille ou qu�il est huileux. Votre photo doit satisfaire aux exigences pr�cis�es au point 7. Une fois tous les champs obligatoires du bulletin remplis (et que vous avez t�l�vers� votre photo), cliquez sur le bouton Soumettre pour terminer votre inscription (le bulletin de participation et la photo ci-apr�s collectivement appel�s l�� 
        <b>inscription</b> �. Une fois que vous avez soumis une inscription admissible, celle-ci est prise en compte dans le tirage au sort vous offrant la chance de gagner un des 250 flacons de nettoyant �quilibrant peau mixte de format ordinaire. De plus, les 10 000 premiers participants admissibles recevront �galement un (1) �chantillon de nettoyant �quilibrant peau mixte de Bior�<sup>MD</sup> en sachet de 7 ml, jusqu�� �puisement des stocks (l�� 
        <b>�chantillon</b> �, voir le point 9). En outre, le commanditaire versera 1 $ pour chaque inscription admissible re�ue durant la campagne � Toronto Wildlife Centre (jusqu�� un maximum de 10 000 $ CA, voir le point 10). Pour �tre admissible, votre inscription doit �tre re�ue pendant la dur�e de la campagne. </p>

    <p>Une (1) seule inscription par personne ou adresse de courriel est autoris�e pendant la dur�e de la campagne. Pour plus de clart� : vous ne pouvez utiliser qu�une (1) seule adresse de courriel pour participer � la campagne. S�il est �tabli qu�une personne a tent� : (i) d�effectuer plus d�une (1) inscription par personne, par adresse de courriel ou par compte pendant la dur�e de la campagne ou (ii) d�utiliser plus d�un (1) nom, d�une identit�, adresse de courriel ou d�un compte pour participer � la campagne, le commanditaire peut alors, � sa seule et enti�re discr�tion, disqualifier cette personne. Votre inscription sera rejet�e (� la seule et enti�re discr�tion du commanditaire) : (i) si le bulletin de participation n�est pas d�ment rempli (ce qui inclut le t�l�versement de la photo) et soumis pendant la dur�e de la campagne ou (ii) si votre photo n�est pas conforme aux exigences pr�cis�es ci-dessous. Il est interdit, sous peine de disqualification par le commanditaire, d�utiliser ou de tenter d�utiliser plus d�un (1) nom, identit�, adresse de courriel, compte ou le moindre syst�me automatis�, robotis�, script, la moindre macroinstruction ou encore tout autre syst�me ou programme pour participer � la campagne ou la perturber de toute autre fa�on. Les parties exon�r�es (d�finies ci-dessous) ne peuvent �tre tenues responsables des inscriptions en retard, perdues, incorrectement achemin�es, retard�es, incompl�tes ou inadmissibles.</p>

    <p>Toutes les inscriptions peuvent faire l�objet d�une v�rification � tout moment. Le commanditaire se r�serve le droit d�exiger, � sa seule et enti�re discr�tion, une preuve d�identit� et (ou) d�admissibilit� � la campagne � titre de candidat, dont la forme est acceptable � ses yeux (y compris, sans s�y limiter, une pi�ce d�identit� avec photo �mise par un gouvernement). Ne pas fournir une telle preuve � la satisfaction du commanditaire et en temps opportun peut entra�ner la disqualification. Le ou les serveurs de la campagne repr�sentent l�unique m�thode visant � d�terminer la date et la validit� d�une inscription � la campagne.</p>

    <p><b>6.	OCCASION DE DIFFUSER DE L�INFORMATION SUR LA CAMPAGNE</b></p>

    <p>Au moment de son inscription � la campagne (ou � tout autre moment par la suite au moyen de l�application), un participant admissible pourra transmettre de l�information sur la campagne � ses amis et sur son r�seau en ligne. Suivez les instructions � l��cran et cliquez sur le bouton Diffuser apr�s avoir termin� votre inscription pour diffuser l�information sur la campagne. Vous NE recevrez rien pour avoir diffus� l�information. </p>

    <p><b>7.	EXIGENCES RELATIVES � LA PHOTO</b></p>

    <p>EN VOUS INSCRIVANT, VOUS CONFIRMEZ QUE VOTRE INSCRIPTION (AINSI QUE CHACUN DE SES �L�MENTS) SATISFAIT � TOUTES LES CONDITIONS �NONC�ES DANS LE PR�SENT R�GLEMENT OFFICIEL. LES PARTIES EXON�R�ES (D�FINIES CI-DESSOUS) NE SONT AUCUNEMENT LI�ES JURIDIQUEMENT EN CE QUI CONCERNE L�UTILISATION DE VOTRE INSCRIPTION. VOUS VOUS ENGAGEZ � D�GAGER LES PARTIES EXON�R�ES (D�FINIES CI-DESSOUS) DE TOUTE RESPONSABILIT� SI L�ON D�COUVRE PAR LA SUITE QUE VOUS AVEZ D�ROG� AU PR�SENT R�GLEMENT OU QUE VOUS NE VOUS Y �TES PAS ENTI�REMENT CONFORM�.</p>

    <p>La photo que vous avez soumise ne doit pas avoir d�j� �t� s�lectionn�e comme gagnante dans une autre campagne. Pour �tre admissible � la pr�sente campagne, votre photo doit r�pondre aux crit�res suivants : </p>

    <table border="1" cellpadding="2" cellspacing="1" style="padding: 5px; border: 1px solid rgb(100, 100, 100); border-collapse: collapse; border-spacing: 2px;">
    <tr>
        <td><b>Taille et quantit� maximales</b></td><td>
        <b>Formats accept�s</b></td><td><b>Exigences relatives au contenu</b></td>
    </tr>
    <tr>
        <td class="style4">2 Mo</td><td class="style3">.JPG, .PNG, .GIF</td><td>Photo de votre visage lorsqu�il brille ou qu�il est huileux</td>
    </tr>
    </table>		
    		<br /><br />

    <p>En prenant part � la campagne, chaque participant accepte d��tre li� par le pr�sent r�glement officiel (le � 
        <b>r�glement</b> �) et par l�interpr�tation de ce dernier par le commanditaire. Il garantit en outre : </p>

    <p>i.	que c�est bien lui qui figure sur sa photographie et qu�il poss�de tous les droits sur celle-ci exig�s aux fins de sa participation � la campagne (y compris, sans s�y limiter, le consentement � son utilisation de la personne ayant pris la photographie);</p>

    <p>ii.	que la photographie n�enfreint aucune loi, aucune l�gislation, aucune ordonnance ni aucun r�glement;</p>

    <p>iii.	que sa soumission ne comporte aucune r�f�rence � de tierces personnes identifiables ni aucune ressemblance avec celles-ci, si ce n�est avec le consentement de celles-ci ou de leurs parents ou tuteurs l�gaux si elles sont mineures selon les lois de leur lieu de r�sidence (si vous ne pouvez obtenir le consentement d�une personne figurant sur votre photographie, son visage doit �tre flout� de mani�re � �tre m�connaissable); </p>

    <p>iv.	que la photographie n�engendrera aucune revendication relative � une transgression, une atteinte � la vie priv�e ou une publicit�, ni n�empi�tera sur tous droits ou int�r�ts de tout tiers, ni n�engendrera aucune demande de remboursement de quelque nature que ce soit;</p>
     
    <p>v.	que sa soumission n�est ni diffamatoire, notamment sur le plan commercial, ni de nature pornographique ou obsc�ne et qu�elle ne contient, ne d�peint, n�inclut, n�aborde ou n��voque, notamment, aucun des �l�ments suivants : nudit�; consommation d�alcool ou usage de drogues ou du tabac; activit�s sexuelles explicitement d�peintes ou simplement sugg�r�es; propos et (ou) symboles crus, vulgaires ou offensants; descriptions m�prisantes de quelque cat�gorie ethnique, raciale, sexuelle ou religieuse ou de quelque groupe que ce soit (incluant sans s�y limiter, tout concurrent du commanditaire); contenu approuvant, tol�rant et (ou) �voquant des conduites, des activit�s ou des comportements ill�gaux, d�plac�s ou � risque; renseignements personnels concernant des particuliers, y compris leur nom, leur num�ro de t�l�phone, leur adresse postale ou leur adresse de courriel; publicit�, comparaisons ou sollicitations visant des produits ou des services autres que ceux du commanditaire; produits, marques de commerce ou autres et (ou) logos de tiers identifiables, hormis ceux du commanditaire (p. ex., aucun v�tement ni produit visible sur votre photographie ne doit comporter de logos, de marques de commerce ou d�autres �l�ments de tiers identifiables � moins que les autorisations appropri�es n�aient �t� obtenues, et l�ensemble des produits, des marques d�pos�es, des marques de commerce et (ou) logos de tiers identifiables pour lesquels les autorisations appropri�es n�ont pas �t� obtenues doivent �tre flout�s de mani�re � �tre m�connaissables); conduite ou activit�s contraires au pr�sent r�glement; et (ou) tout autre contenu inappropri�, d�plac� ou offensant ou encore susceptible d��tre jug� tel, le tout �tant laiss� � l�enti�re appr�ciation du commanditaire.</p>

    <p>Si le commanditaire, son agence de promotion ou son mod�rateur de contenu d�sign� (le � r�viseur �) juge, � sa seule et enti�re discr�tion, qu�une photo enfreint les modalit�s indiqu�es dans le pr�sent r�glement, elle ne sera pas publi�e sur le site Web au moyen de l�application et entra�nera la disqualification du participant. Avant la publication d�une photo sur le site Web au moyen de l�application ou de son acceptation comme inscription admissible, le r�viseur se r�serve le droit, � sa seule et enti�re discr�tion, de la modifier ou encore de demander � tout participant de modifier ou de resoumettre sa photo, afin de s�assurer qu�elle est conforme au r�glement, ou pour tout autre motif.</p>

    <p>En participant � la campagne et en s�inscrivant, chaque participant : (i) accorde au commanditaire une licence perp�tuelle et non exclusive l�autorisant � publier, � diffuser, � reproduire, � modifier ou � utiliser de quelque autre mani�re que ce soit sa photo, en tout ou en partie, � des fins de publicit� ou de promotion de la campagne ou encore � toute autre fin, (ii) renonce � tous ses droits moraux sur cette photo au profit du commanditaire et (iii) s�engage � d�gager les parties li�es � la campagne, leurs agents, employ�s, administrateurs, successeurs et ayants droit respectifs (collectivement appel�s les � parties exon�r�es �) de toute responsabilit� li�e aux r�clamations fond�es sur le droit � l�image, la diffamation, la violation de la vie priv�e, la violation de droits d�auteur, la contrefa�on de marques de commerce ou tout autre motif associ� � la propri�t� intellectuelle li� � la photo. Pour �viter toute ambigu�t� : le r�viseur se r�serve le droit, � sa seule et enti�re discr�tion, et ce, � tout moment pendant ou apr�s la campagne, de modifier ou de retirer toute photo, ou encore de demander � tout participant de modifier sa photo, en cas de r�ception d�une plainte concernant celle-ci ou pour tout autre motif. </p>

    <p><b>8.	PRIX</b></p>

    <p>Deux cent cinquante (250) prix seront remis lors d�un tirage au sort. Chacun consiste en un (1) flacon de nettoyant �quilibrant peau mixte de Bior�<sup>MD</sup> en format de 200 ml, d�une valeur au d�tail approximative de 9,99 $ chacun (le � 
        <b>prix</b> �). Chaque prix doit �tre accept� tel qu�il est d�cern� et n�est ni cessible ni �changeable contre de l�argent. Aucun �change n�est accord�, sauf si le commanditaire y consent. Le commanditaire se r�serve le droit de remplacer un prix ou tout �l�ment de celui-ci, � sa seule et enti�re discr�tion, par un prix de valeur �gale ou sup�rieure, y compris, sans s�y limiter, mais � sa seule et enti�re discr�tion, par une somme d�argent. Chaque prix n�est remis qu�� la personne dont le nom complet et une adresse de courriel valide v�rifiables figurent sur le bulletin de participation. La limite est d�un (1) prix par personne. </p>
    
    <p><b>9.	�CHANTILLONS</b></p>

    <p>Les 10 000 premiers participants recevront un (1) �chantillon, qui leur sera envoy� par la poste � leur domicile dans les trois � six semaines suivant leur inscription, si celle-ci s�est av�r�e admissible. Un avis sera publi� sur la page des fans une fois que les 10 000 �chantillons auront �t� distribu�s, � la fin de l�offre.</p>

    <p><b>10.	DON DE BIENFAISANCE</b></p>

    <p>Le commanditaire versera 1 $ CA pour chaque inscription admissible re�ue conform�ment au pr�sent r�glement au Toronto Wildlife Centre (jusqu'� un maximum de 10 000 $ CA). Si, � la fin de la campagne, moins de 10 000 inscriptions ont �t� re�ues, le commanditaire effectuera un don minimal de 10 000 $ sous forme de versement unique. </p>

    <p><b>11.	PROC�DURE DE S�LECTION DES GAGNANTS</b></p>

    <p>Le 7 mai 2012 (la � <b>date du tirage</b> �) � Mississauga, en Ontario, vers 10 h, HNE, deux cent cinquante (250) participants admissibles seront s�lectionn�s par un tirage au sort parmi toutes les inscriptions admissibles re�ues pendant la dur�e de la campagne. Les probabilit�s de gagner d�pendent du nombre d�inscriptions admissibles re�ues durant la campagne. </p>

    <p>POUR �TRE PROCLAM� GAGNANT D�UN PRIX, chaque gagnant potentiel s�lectionn� doit r�pondre correctement, sans aide m�canique ou autre, � la question r�glementaire d�arithm�tique figurant sur le bulletin de participation. En outre, en participant � la campagne et en acceptant le prix, par les pr�sentes, vous : (i) confirmez que vous respectez le pr�sent r�glement; (ii) acceptez le prix tel qu�il est d�cern�; (iii) d�gagez les parties exon�r�es (�num�r�es ci-dessus) de toute responsabilit� eu �gard � la pr�sente campagne, � votre participation ou � la remise et � l�usage ou au mauvais usage du prix et de toute portion du prix; (iv) consentez � la publication, la reproduction ou tout autre usage de vos nom, adresse, voix, d�clarations au sujet de la campagne ou photo ou autre ressemblance, sans autre avis ni compensation, dans le cadre de toute publicit� ou promotion men�e par le commanditaire ou en son nom de quelque mani�re que ce soit, y compris la presse �crite, les �missions diffus�es ou Internet. Si le participant s�lectionn� : (a) ne r�pond pas correctement � la question r�glementaire figurant sur le bulletin de participation; (b) ne peut accepter le prix tel qu�il est d�cern� pour une raison quelconque, il est disqualifi� (et perd ainsi tout droit au prix) et le commanditaire se r�serve le droit, � sa seule et enti�re discr�tion, de s�lectionner au hasard un autre participant admissible parmi le reste des inscriptions admissibles (le cas �ch�ant, les dispositions pr�c�dentes du pr�sent paragraphe s�appliquent au nouveau participant s�lectionn�). </p>

    <p>Le commanditaire ou son repr�sentant d�sign� tentera une fois (1) de faire parvenir le prix au participant s�lectionn�. � la r�ception d�un avis indiquant l�impossibilit� d�effectuer la livraison, le participant s�lectionn� perd tout droit au prix, et le commanditaire se r�serve le droit, � sa seule et enti�re discr�tion, de s�lectionner au hasard un autre participant admissible parmi le reste des inscriptions admissibles (le cas �ch�ant, les dispositions pr�c�dentes du pr�sent paragraphe s�appliquent au nouveau participant s�lectionn�). </p>

    <p><b>12.	CONDITIONS G�N�RALES</b></p>

    <p>Toutes les inscriptions deviennent la propri�t� du commanditaire. Les d�cisions du commanditaire concernant tout aspect de la campagne sont ex�cutoires et sans appel et lient tous les participants, y compris, sans s�y limiter, toute d�cision concernant l�admissibilit� ou la disqualification de participants et (ou) d�inscriptions.</p>

    <p>Les parties exon�r�es ne peuvent �tre tenues responsables : (i) de la moindre d�faillance du site Web ou de l�application pendant la dur�e de la campagne, (ii) de tout dysfonctionnement technique ou autre probl�me li� � un r�seau ou � des lignes t�l�phoniques, � des syst�mes informatiques en ligne, � des serveurs, � des fournisseurs d�acc�s, � du mat�riel informatique ou � des logiciels, (iii) de la non-r�ception de la moindre inscription pour quelque raison que ce soit, y compris, sans s�y limiter, des probl�mes techniques ou encore la saturation d�Internet ou d�un site Web quelconque, (iv) des dommages corporels ou encore des dommages subis par l�ordinateur ou par tout autre appareil d�un participant ou d�une autre personne li�s � la participation � la campagne ou au t�l�chargement du moindre �l�ment dans le cadre de celle-ci et (ou) (v) de toute combinaison des �l�ments pr�cit�s. </p>

    <p>En cas de diff�rend concernant la personne ayant soumis une inscription, celle-ci sera jug�e avoir �t� soumise par le titulaire autoris� du compte de l�adresse de courriel soumise au moment de l�inscription. Un � titulaire autoris� de compte � d�signe la personne � qui une adresse de courriel a �t� attribu�e par un fournisseur d�acc�s Internet, un fournisseur de services en ligne ou une autre organisation (p. ex., une entreprise, un �tablissement d�enseignement, etc.) responsable d�attribuer des adresses de courriel pour le domaine associ� � l�adresse de courriel soumise. Tout participant peut �tre tenu de prouver (d�une mani�re acceptable aux yeux du commanditaire, y compris, sans s�y limiter, au moyen d�une pi�ce d�identit� avec photo �mise par un gouvernement), qu�il est bien le titulaire autoris� du compte de courriel associ� � l�inscription s�lectionn�e.</p>

    <p>Sous r�serve de l�approbation de la R�gie des alcools, des courses et des jeux du Qu�bec (la � R�gie �), le commanditaire se r�serve le droit, � sa seule et enti�re discr�tion, de mettre fin � la campagne, de la modifier ou de la suspendre (ou encore de modifier le pr�sent r�glement) de quelque fa�on que ce soit si une erreur, un probl�me technique, un virus informatique, un bogue, un sabotage, une intervention non autoris�e, une fraude, une d�faillance technique ou tout autre facteur pouvant raisonnablement �tre consid�r� comme ind�pendant de la volont� du commanditaire nuit au d�roulement de la campagne conform�ment au pr�sent r�glement. Toute tentative d�lib�r�e d�endommager tout site Web ou de perturber le bon d�roulement de la pr�sente campagne de quelque fa�on que ce soit (� d�terminer par le commanditaire � sa seule et enti�re discr�tion) constitue une violation des lois criminelles et p�nales et, advenant une telle tentative, le commanditaire se r�serve le droit d�engager un recours pour dommages subis, dans la pleine mesure permise par la loi. Le commanditaire se r�serve le droit, � sa seule et enti�re discr�tion et sous r�serve de l�approbation de la R�gie, d�annuler, de modifier ou de suspendre la campagne ou encore de modifier le pr�sent r�glement, sans pr�avis ni obligation de sa part, en cas d�accident, d�erreur d�impression, d�erreur administrative ou d�erreur de toute autre nature, ou encore pour toute autre raison. </p>

    <p>Le commanditaire se r�serve le droit, � sa seule et enti�re discr�tion, de modifier sans pr�avis, dans la mesure n�cessaire, les dates et (ou) les horaires pr�vus par le pr�sent r�glement officiel, que ce soit pour s�assurer du respect du pr�sent r�glement officiel par tout participant ou toute participation, � la suite de probl�mes techniques, ou encore en raison d�autres circonstances que le commanditaire estime, � sa seule et enti�re discr�tion, susceptibles d�emp�cher la campagne de se d�rouler conform�ment au pr�sent r�glement officiel.</p>

    <p>En cas de divergence ou d�incompatibilit� entre les modalit�s du pr�sent r�glement officiel et les d�clarations ou autres mentions figurant dans tout autre document li� � la campagne, y compris, sans s�y limiter, le bulletin de participation � la campagne, le site Web, l�application et (ou) la moindre publicit� au sein de points de vente, � la t�l�vision, dans la presse �crite ou en ligne, les modalit�s du pr�sent r�glement officiel pr�valent.</p>

    <p><span class="style5">Pour les r�sidents du Qu�bec</span> : Tout litige relatif au d�roulement ou � l�organisation d�un concours publicitaire peut �tre soumis � la R�gie des alcools, des courses et des jeux pour qu�une d�cision soit rendue. Un litige touchant la remise d�un prix ne peut �tre soumis � la R�gie que dans le but d�aider les parties � conclure un accord � l�amiable. </p>

    <p><b>13.	RENSEIGNEMENTS PERSONNELS</b></p>
    
    <p>Kao Canada Inc. recueille des renseignements personnels sur les participants aux fins d�administration de la campagne. Kao Canada Inc. n�est pas autoris�e � utiliser vos renseignements personnels � des fins autres que celles pr�vues, et la campagne est assujettie aux lois f�d�rales et provinciales canadiennes sur le respect de la vie priv�e. Veuillez consulter la politique de confidentialit� de Kao Canada Inc. au http://www.kaobrands.com/privacy_policy.asp pour toute information suppl�mentaire sur sa politique en mati�re de confidentialit� et de s�curit� des renseignements sur les utilisateurs.</p>

    <p><span class="style6">R�GLEMENT ABR�G�</span><br />
    Aucun achat n�est requis. La campagne prend fin � 23 h 59 min le 30 avril 2012. Elle s�adresse � tous les r�sidents canadiens ayant atteint l��ge de la majorit�. Participation en ligne et r�glement complet au www.facebook.com/BioreCanada. Il y a 250 prix � gagner. Chacun consiste en un (1) flacon de nettoyant �quilibrant peau mixte de Bior�<sup>MD</sup> en format de 200 ml (valeur au d�tail approximative : 9,99 $ chacun). La r�ponse � une question r�glementaire est exig�e. Les chances de gagner d�pendent du nombre d�inscriptions admissibles.</p>


   

</div>
</body>

</html>
