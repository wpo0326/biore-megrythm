﻿<%@ Page Title="Sitemap | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.sitemap._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1>Sitemap</h1>
                 <div id="linkList">
                     <div id="prodLinks">
                         <ul class="col" id="col1">
                            <li><a href="../biore-facial-cleansing-products">Our products <span class="arrow">&rsaquo;</span></a></li>
                            <li>
                                <h2 class="pie deepCleansing">Deep Cleansing</h2>
                                <ul>
                                    <li><a href="<%= VirtualPathUtility.ToAbsolute("~/deep-cleansing-products/combination-skin-balancing-cleanser") %>"><span class="new">New</span> Nettoyant équilibrant peau mixte</a></li>
                                    <li><a href="<%= VirtualPathUtility.ToAbsolute("~/deep-cleansing-products/4-in-1-detoxifying-cleanser") %>">Nettoyant revitalisant 4 en 1 </a></li>
                                    <li><a href="<%= VirtualPathUtility.ToAbsolute("~/deep-cleansing-products/pore-unclogging-scrub") %>">Nettoyant granuleux pour les pores</a></li>
                                    <li><a href="<%= VirtualPathUtility.ToAbsolute("~/deep-cleansing-products/daily-purifying-scrub") %>">Gommage quotidian purifiant</a></li>
                                    <%--<li><a href="<%= VirtualPathUtility.ToAbsolute("~/deep-cleansing-products/daily-cleansing-cloths") %>">Daily Cleansing Cloths</a></li>--%>
                                    <!--<li><a href="<%= VirtualPathUtility.ToAbsolute("~/deep-cleansing-products/steam-activated-cleanser") %>">Nettoyant activé par la vapeur</a></li>-->
                                    <li><a href="<%= VirtualPathUtility.ToAbsolute("~/deep-cleansing-product-family/pore-strips#regular") %>">Bandes de nettoyage en profondeur </a></li>
                                    <li><a href="<%= VirtualPathUtility.ToAbsolute("~/deep-cleansing-product-family/pore-strips#combo") %>">Bandes de nettoyage en profondeur emballage assorti</a></li>
                                    <li><a href="<%= VirtualPathUtility.ToAbsolute("~/deep-cleansing-product-family/pore-strips#ultra") %>">Bandes de nettoyage en profondeur Ultra </a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="col" id="col2">
                            <li>
                                <h2 class="pie complexionClearing">Complexion Clearing</h2>
                                <ul>
                                    <li><a href="<%= VirtualPathUtility.ToAbsolute("~/complexion-clearing-products/blemish-fighting-ice-cleanser") %>">Nettoyant glacé anti-acné</a></li>
                                    <li><a href="<%= VirtualPathUtility.ToAbsolute("~/complexion-clearing-products/blemish-treating-astringent") %>">Astringent anti-acné</a></li>
                                    <li><a href="<%= VirtualPathUtility.ToAbsolute("~/complexion-clearing-products/warming-anti-blackhead-cleanser") %>">Nettoyant chauffant en crème anti-points noirs</a></li>
                                </ul>
                                <h2 class="pie murt">Make-Up Removing</h2>
                                <ul>
                                    <li><a href="<%= VirtualPathUtility.ToAbsolute("~/make-up-removing-products/make-up-removing-towelettes") %>">Lingettes démaquillantes</a></li>
                                </ul>
                                <ul>
                                    <li class="discontinued"><a href="../past-biore-favorites">Discontinued Products</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <ul class="col noPadding" id="col3">
                        <li>
                            <a href="../special-offers-and-skincare-tips">Quoi de neuf? <span class="arrow">&rsaquo;</span></a>
                            <ul>
                                <li><a href="../clean-new-look">Jetez un coup d’œil</a></li>
                            </ul>
                        </li>
                        <li><a href="../email-newsletter-sign-up">Inscrivez-moi <span class="arrow">&rsaquo;</span></a></li>
                        <%--<li><a href="../where-to-buy-biore">Where To Buy <span class="arrow">&rsaquo;</span></a></li>--%>
                        <!--<li><a href="../biore-videos">Voyez les résultats <span class="arrow">&rsaquo;</span></a></li>-->
                        <li><a href="../about-us">À propos des soins pour la peau Bioré<sup>MD</sup><span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../contact-us">Pour nous joindre <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaobrands.com/" target="_blank">Kao Brands Company <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaobrands.com/legal.asp" target="_blank">Avis juridiques <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">Politique de confidentialité <span class="arrow">&rsaquo;</span></a></li>
                     </ul>
                 </div>
                 <div id="photo"></div>
            </div>
        </div>
    </div>
</asp:Content>