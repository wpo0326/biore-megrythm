﻿<%@ Page Title="Bior&eacute;&reg; Skincare | Rules" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SampleFormTerms.aspx.cs" Inherits="Biore2012.FormConfig.SampleTerms" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Bioré® rules" name="description" />
    <meta content="" name="keywords" />
 <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedrules_#.css")
    %>
    <style type="text/css">

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <div id="content-sample-form-terms">
                    <h2>Modalités de la promotion Ensemble d’échantillons au charbon Soyez parmi les premiers à les essayer de Bioré<sup>MD</sup> (les « modalités »). À LIRE ATTENTIVEMENT. </h2>
                    
                    <p><b>promotion est réservée aux résidents du Canada et EST RÉGIE par les lois du Canada.</b></p>

                    <p><b>A. Aperçu</b></p>

                    <p>La promotion Ensemble d’échantillons au charbon Soyez parmi les premiers à les essayer de Bioré<sup>MD</sup>  (la « promotion ») est commanditée par Kao Canada Inc. (le « commanditaire »). Aucun achat n’est exigé pour y participer. Les renseignements que vous divulguez ne sont utilisés qu’aux fins de la gestion de la promotion, et ce, conformément à la politique de confidentialité du commanditaire (voir ci-dessous). Les questions, plaintes ou commentaires concernant la promotion doivent être adressés au commanditaire. Vous ne pouvez utiliser qu’un seul compte (le « compte ») pour participer à la promotion. Aucun achat n’est exigé. Toutefois, l’accès à Internet et un compte sont indispensables pour participer à la promotion. Un grand nombre de bibliothèques publiques, de commerces de détail et d’autres établissements mettent gratuitement à la disposition du public des ordinateurs permettant l’accès à Internet; de plus, certains fournisseurs de services Internet et d’autres entreprises proposent des comptes de courriel gratuits.</p> 

                    <p><b>B. Admissibilité</b></p>

                    <p>La promotion s’adresse exclusivement aux résidents canadiens autorisés qui, au moment de leur participation, sont majeurs selon les lois de leur province ou de leur territoire de résidence, à l’exception des employés, des représentants ou des agents (ainsi que des personnes vivant sous le même toit que ceux-ci, qu’elles soient ou non parentes avec eux) du commanditaire, de ses sociétés mères, de ses filiales, de ses sociétés affiliées, de ses agences de publicité ou de promotion, de ses représentants ou de ses agents (collectivement : les « parties liées à la promotion »). Pour éviter toute ambiguïté : aucune personne ayant été au service de l’une des parties liées à la promotion le 1er janvier 2014 ou par la suite n’est admissible à la promotion. Quiconque vit sous le même toit qu’une telle personne, qu’il soit ou non parent avec celle-ci, n’est pas non plus admissible à la promotion. La moindre tentative d’un participant de contrevenir à la présente disposition relative à l’admissibilité peut, à la seule et entière discrétion du commanditaire, entraîner sa disqualification. La promotion ne s’adresse qu’aux personnes physiques. Les sociétés, associations ou autres groupes ne peuvent y participer. </p>

                    <p><b>C. Durée de la promotion</b></p>

                    <p>La promotion débute à 12 h (midi), heure de l’Est (« HE »), le 28 janvier 2014 et prend fin à 17 h, HE, le 7 février 2014 ou dès l’épuisement des stocks officiels d’ensembles d’échantillons (au sens précisé ci-dessous), selon la première de ces éventualités (la « durée de la promotion »). </p>

                    <p><b>D. Comment participer</b></p>

                    <p>AUCUN ACHAT N’EST REQUIS. Pour participer à la promotion, vous devez accéder à la page Web http://www.biore.ca/fr-CA/formconfig/sampleform.aspx (la « page Web ») pendant la durée de la promotion, suivre les consignes affichées pour réclamer un ensemble d’échantillons en remplissant intégralement le formulaire de réclamation en ligne (le « formulaire de réclamation »), en y indiquant entre autres votre prénom, votre nom, votre date de naissance, votre adresse (ville, province ou territoire et code postal compris), ainsi que votre adresse de courriel. Après avoir dûment rempli le formulaire de réclamation, vous devez soumettre celui-ci, à savoir votre « réclamation », conformément aux directives affichées. Pour être valide, votre réclamation doit être soumise et reçue pendant la durée de la promotion.</p>

                    <p><b>E. Limite de demandes de réclamation</b></p>

                    <p>Une (1) seule réclamation par personne, par adresse de courriel et par compte est autorisée pendant la durée de la promotion. Pour éviter toute ambiguïté : vous ne pouvez utiliser qu’une (1) seule adresse de courriel et qu’un (1) seul compte pour participer à la promotion. S’il est établi qu’une personne (i) a tenté de soumettre plus d’une (1) réclamation par personne, par adresse de courriel et par compte pendant la durée de la promotion et (ou) (ii) qu’elle a utilisé ou tenté d’utiliser plus d’un (1) nom, d’une (1) identité, d’une (1) adresse de courriel ou d’un (1) compte pour participer à la promotion, le commanditaire peut, à sa seule et entière discrétion, disqualifier cette personne, dont toutes les réclamations sont annulées le cas échéant. Il est interdit, sous peine de disqualification par le commanditaire, d’utiliser ou de tenter d’utiliser plus d’un (1) nom, d’une (1) identité ou d’une (1) adresse de courriel ou encore quelque macro, script ou encore système ou programme automatisé ou robotique que ce soit pour participer à la promotion ou dans le but de perturber celle-ci. Les parties exonérées (au sens précisé ci-dessous) ne peuvent être tenues responsables des demandes de réclamation tardives, perdues, incorrectement acheminées, retardées, incomplètes ou inadmissibles, qui sont toutes annulées.</p> 
 
                    <p><b>F. Ensembles d’échantillons </b></p>

                    <p>Au plus, cinq mille (5 000) ensembles d’échantillons sont susceptibles d’être offerts pendant la durée de la promotion, chaque ensemble contenant un (1) masque une minute auto-chauffant au charbon naturel de Bioré<sup>MD</sup>  de 7 g et un (1) nettoyant en profondeur au charbon pour les pores de Bioré<sup>MD</sup>  de 7 ml (collectivement appelé l’« ensemble d’échantillons »). La valeur au détail approximative de chaque ensemble d’échantillons est de 2 $ CA. </p>

                    <p><b>G. Expédition des ensembles d’échantillons</b></p>

                    <p>Une fois que vous êtes proclamé gagnant d’un ensemble d’échantillons, ce dernier vous sera expédié par la poste à l’adresse indiquée sur votre formulaire de réclamation. Veuillez prévoir de quatre (4) à six (6) semaines avant que votre ensemble d’échantillons ne parvienne à l’adresse que vous avez indiquée sur votre formulaire de réclamation. Si, huit (8) semaines après avoir été proclamé gagnant conformément aux présentes modalités, vous n’avez toujours pas reçu votre ensemble d’échantillons, veuillez le signaler par courriel à bioreskincarelistens@biore.ca. Aucun ensemble d’échantillons perdu ou volé ne sera remplacé.</p>

                    <p><b>H. Modalités générales</b></p>

                    <p>Les modalités générales suivantes s’appliquent à la promotion :</p>

                    <ol>
                        <li>Les présentes modalités sont nulles là où elles sont interdites par la loi. La promotion est nulle là où elle est interdite, imposable ou réglementée ou encore là où un enregistrement s’impose. L’ensemble des lois fédérales et provinciales ainsi que des règlements fédéraux, provinciaux et municipaux s’appliquent. </li>
                    
                        <li>Le commanditaire se réserve le droit d’annuler, d’interrompre ou de modifier la promotion si une fraude, un virus, des problèmes techniques ou tout autre facteur viennent en perturber la gestion.</li>
                    
                        <li>Tout usage de moyens automatisés ou de programmes pour soumettre une réclamation en ligne est strictement interdit.</li>
                    
                        <li>Le commanditaire n’est pas responsable des erreurs de communication, des virus, des vers ou des autres éléments contaminants susceptibles de toucher ou d’endommager l’ordinateur ou les données d’un participant lors de sa participation à la promotion.</li>
                    
                        <li>Sauf là où cela est interdit par la loi, en remplissant le formulaire de réclamation, tout participant dégage le commanditaire, les filiales et les sociétés affiliées de celui-ci ainsi que leurs actionnaires, administrateurs, dirigeants, employés, représentants ou agents respectifs de toute responsabilité liée aux pertes ou aux dommages de quelque nature que ce soit, directs, indirects ou punitifs, découlant de la promotion.</li>
                    
                        <li>En prenant part à la promotion, chaque participant autorise expressément le commanditaire, ses agents et (ou) ses représentants à stocker, à divulguer et à exploiter les renseignements personnels soumis dans le formulaire de réclamation, mais uniquement aux fins de la gestion de la promotion et conformément à la politique de confidentialité du commanditaire accessible au http://www.kaobrands.com/privacy_policy.asp, sauf si le participant accepte qu’il en soit autrement.</li>
                    
                        <li>Sauf là où cela est interdit par la loi, en remplissant le formulaire de réclamation, tout participant convient que les parties liées à la promotion ne sont pas responsables : (a) de la moindre information incorrecte ou inexacte, que ce soit par la faute du participant ou en raison d’erreurs d’impression; (b) de la moindre intervention humaine non autorisée touchant quelque aspect de la promotion que ce soit; (c) des erreurs techniques ou humaines susceptibles d’entacher la gestion de la promotion ou le processus de réclamation; ou (d) des dommages corporels ou matériels susceptibles découlant directement ou indirectement, en tout ou en partie, de la participation du participant à la promotion ou encore de la réception, de l’usage ou du mésusage d’un ensemble d’échantillons.</li>
                   
                        <li>La promotion ne s’adresse qu’aux personnes physiques. Les sociétés, associations ou autres groupes ne peuvent y participer. Une personne physique, une société, une association ou un groupe qui demande à des personnes d’utiliser un compte unique afin d’accumuler des réclamations en vue d’en faire un usage combiné, ou encore qui les y encourage ou les y autorise, commet un acte frauduleux.</li>
                    </ol>

                    <p><b>I. Autres modalités</b></p>

                    <p>En cas de divergence ou d’incompatibilité entre les modalités de la version anglaise des présentes modalités et les déclarations ou autres mentions figurant dans tout autre document lié à la promotion, y compris le formulaire de réclamation, la page Web et la présente version française ou la moindre publicité au sein de points de vente, à la télévision, sur papier ou en ligne, les modalités de la version anglaise des présentes modalités ont préséance.</p>



                    <p><b>Version abrégée des modalités</b></p>

                    <p>† Aucun achat n’est requis. La promotion débute le 28 janvier 2014 à 12 h (midi), HE, et prend fin le 7 février 2014 à 17 h, HE, ou dès l’épuisement des stocks officiels d’ensembles d’échantillons, selon la première de ces éventualités. Le concours s’adresse aux résidents canadiens autorisés et majeurs. Participation en ligne et modalités complètes au http://www.biore.ca/fr-CA/formconfig/sampleform.aspx. Un maximum de cinq mille (5 000) ensembles d’échantillons sont susceptibles d’être offerts (valeur au détail approximative : 2 $ chacun). </p>


                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
