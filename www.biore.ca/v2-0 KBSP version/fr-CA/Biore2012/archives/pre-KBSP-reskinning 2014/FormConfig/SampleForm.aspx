﻿<%@ Page Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="SampleForm.aspx.cs"
    Inherits="Biore2012.FormConfig.SampleForm" %>
<%@ Register TagPrefix="skm" Namespace="skmValidators" Assembly="skmValidators" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%#SquishIt.Framework.Bundle .Css()
        .Add("~/css/formConfig.css")
        .Render("~/css/combinedform_#.css")
    %>
    
    <style type="text/css">

        body {
            background:#90c84b url(/fr-ca/images/forms/sampleformbg.jpg) repeat-x top left;
            color:#19224c;
        }

        .forms #mainContent {
            background-image: none !important;
        }

        .responsive #contactFormWrap .SubmitContainerSample{
        clear: left;
	    overflow:hidden;
	    padding: 12px 20px 0px 0px;
    }

    .responsive .MemberInfoContainer {
        margin-top: 10px;
    }

    .responsive .req {
        color: #404041;
    }

    .responsive #contactFormWrap .SubmitContainerSample input{
        background-color:#19224d;
        border: 0 none;
        color: #fff;
        cursor: pointer;
        display: block;
        font-size: 12px;
        width: 165px;
        height: 47px;
        text-align: center;
        text-decoration: none;
        text-transform: uppercase;
        margin: 18px auto;
    
    }

    .notouch .responsive #contactFormWrap .SubmitContainerSample input:hover{
        background-position:bottom left;
    }

        #ctl00_ContentPlaceHolder1_ucForm_PageHeader {
            text-indent:-9999px;
        }

        .responsive #contactFormWrap .FormBottom {
            position: absolute;
            bottom: -21px;
            z-index: -1;
            left: 0px;
            padding-bottom: 0;
            margin: 0;
            width: 100%;
            height: 290px;
            background: transparent url(/en-CA/images/forms/sampleform-splotch.png) no-repeat top left;
        }

    @media screen and (max-width: 640px) {
        .responsive #contactFormWrap .FormBottom {
            background:none !important;
        }
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow">
            </div>
            <div class="centeringDiv">
                <div class="responsive contact">
                    <div id="contactFormWrap">
                        <div id="BrandImageContainer" class="png-fix">
                            <img id="ImageHeader" class="png-fix" src="images/forms/sampleform-bottle-fr.png" style="border-width: 0px;margin-top: 15%" runat="server" />
                        </div>
                        <div id="ContactFormContainer">
                            <asp:Panel ID="ContactDetails" runat="server">
                                <!-- Header -->
                                <div id="formHeader">
                                    <h1 id="ctl00_ContentPlaceHolder1_ucForm_PageHeader">
                                        Essayez les nouveautés en matière de soins pour la peau avant tout le monde! 
                                    </h1>
                                    <img src="/fr-ca/images/forms/sampleform-header.png" class="png-fix" style="width:100%" />
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="OptinForm" runat="server">
                                <!-- Description -->
                                <div class="DescriptionContainer png-fix">
                                    <h2>Essayez les nouveautés en matière de soins pour la peau avant tout le monde! </h2>
                                    Inscrivez-vous pour obtenir un ensemble GRATUIT exclusif d’échantillons du NOUVEAU nettoyant en profondeur au charbon pour les pores de Bioré<sup>MD</sup> (7 ml) et du NOUVEAU masque une minute auto-chauffant au charbon naturel de Bioré<sup>MD</sup> (7 g). 
                                </div>
                                <!-- From Fields -->
                                <div class="FormContainer">
                                    <p class="req png-fix">
                                        <em>Champ obligatoire*</em></p>
                                    <div class="MemberInfoContainer png-fix">
                                        <div class="NameWrapper">
                                            <div class="FirstNameContainer Question">
                                                <div class="mmSweet">
                                                    <asp:Label ID="FNameLbl" runat="server" Text="First Name*" AssociatedControlID="FrstName"></asp:Label>
                                                    <asp:TextBox ID="FrstName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="Label2" runat="server" Text="Prénom*" AssociatedControlID="FName"></asp:Label>
                                                <asp:TextBox ID="FName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="FNameValidator1" runat="server" ErrorMessage="Veuillez entrer votre Prénom."
                                                        ControlToValidate="FName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="FNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your First Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="FName" EnableClientScript="true"
                                                        SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                            </div>
                                            <div class="LastNameContainer Question">
                                                <asp:Label ID="LNameLbl" runat="server" Text="Nom*" AssociatedControlID="LName"></asp:Label>
                                                <asp:TextBox ID="LName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="LNameValidator1" runat="server" ErrorMessage="Veuillez entrer votre Nom."
                                                        ControlToValidate="LName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="LNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Last Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="LName" SetFocusOnError="true"
                                                        Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="EmailWrapper">
                                            <div class="EmailContainer Question">
                                                <asp:Label ID="EmailLbl" runat="server" Text="Adresse de courriel*" AssociatedControlID="Email"></asp:Label>
                                                <asp:TextBox ID="Email" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="EmailValidator1" runat="server" ErrorMessage="Veuillez entrer votre Courriel."
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="EmailValidator2" runat="server" ErrorMessage="Please enter a valid Email Address."
                                                        ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div class="PhoneContainer Question">
                                            <asp:Label ID="MobilePhoneLbl" runat="server" Text="Numéro de téléphone mobile" AssociatedControlID="MobilePhone"></asp:Label>
                                            <asp:TextBox ID="MobilePhone" MaxLength="50" runat="server" />
                                            <div class="ErrorContainer">
                                                <asp:RegularExpressionValidator ID="v_MobilePhone" runat="server" ErrorMessage="Your Mobile Phone is not required, but please do not use special characters in the Mobile Phone field."
                                                    ValidationExpression="^[^<>]+$" ControlToValidate="MobilePhone" EnableClientScript="true"
                                                    SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                <%-- The following RegEx Validators is for US type numbers.  IMPORTANT: 
								 Please test against your user base before using as you may need to modify it for other formats!  --%>
                                                <%--<asp:RegularExpressionValidator ID="PhoneValidator2" runat="server"
								ErrorMessage="<br/>Your Home Phone is not required, but please enter only valid characters."
								ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"
								ControlToValidate="Phone" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg phone"></asp:RegularExpressionValidator>--%>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelAddress" class="AddressContainer">
                                            <div class="AddressWrapper">
                                                <div class="Address1Container Question">
                                                    <asp:Label ID="Address1Lbl" runat="server" Text="Adresse 1*" AssociatedControlID="Address1"></asp:Label>
                                                    <asp:TextBox ID="Address1" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="Address1Validator1" runat="server" ErrorMessage="Veuillez entrer votre Adresse."
                                                            ControlToValidate="Address1" EnableClientScript="true" SetFocusOnError="true"
                                                            Display="Dynamic" CssClass="errormsg" />
                                                        <asp:RegularExpressionValidator ID="Address1Validator2" runat="server" ErrorMessage="Please do not use special characters in the Address 1 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address1" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="Address2Container Question">
                                                    <asp:Label ID="Address2Lbl" runat="server" Text="Adresse 2" AssociatedControlID="Address2"></asp:Label>
                                                    <asp:TextBox ID="Address2" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="Address2Validator1" runat="server" ErrorMessage="Please do not use special characters in the Address 2 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address2" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="CityContainer Question">
                                                    <asp:Label ID="CityLbl" runat="server" Text="Ville*" AssociatedControlID="City"></asp:Label>
                                                    <asp:TextBox ID="City" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="CityValidator1" runat="server" ErrorMessage="Veuillez entrer votre Adresse Ville."
                                                            ControlToValidate="City" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                            CssClass="errormsg" />
                                                        <asp:RegularExpressionValidator ID="CityValidator2" runat="server" ErrorMessage="Please do not use special characters in the City field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="City" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="StateContainer Question">
                                                <asp:Label ID="StateLbl" runat="server" Text="Province*" AssociatedControlID="State"></asp:Label>
                                                <asp:DropDownList ID="State" runat="server">
                                                    <asp:ListItem Value="" Selected="true">Sélectionner la province</asp:ListItem>
                                                    <asp:ListItem Value="1">Alberta</asp:ListItem>
                                                    <asp:ListItem Value="2">Colombie-Britannique</asp:ListItem>
                                                    <asp:ListItem Value="10">Île-du-Prince-Édouard</asp:ListItem>
                                                    <asp:ListItem Value="3">Manitoba</asp:ListItem>
                                                    <asp:ListItem Value="4">Nouveau-Brunswick</asp:ListItem>
                                                    <asp:ListItem Value="7">Nouvelle-Écosse</asp:ListItem>
                                                    <asp:ListItem Value="8">Nunavut</asp:ListItem>
                                                    <asp:ListItem Value="9">Ontario</asp:ListItem>
                                                    <asp:ListItem Value="11">Québec</asp:ListItem>
                                                    <asp:ListItem Value="12">Saskatchewan</asp:ListItem>
                                                    <asp:ListItem Value="5">Terre-Neuve-et-Labrador</asp:ListItem>
                                                    <asp:ListItem Value="6">Territoires du Nord-Ouest</asp:ListItem>
                                                    <asp:ListItem Value="13">Yukon</asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="StateValidator" runat="server" ErrorMessage="Veuillez entrer votre Province."
                                                        ControlToValidate="State" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div class="PostalCodeContainer Question">
                                            <asp:Label ID="PostalCodeLbl" runat="server" Text="Code postal*" AssociatedControlID="PostalCode"></asp:Label>
                                            <asp:TextBox ID="PostalCode" runat="server" MaxLength="20" CssClass="inputTextBox"></asp:TextBox>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="PostalCodeValidator1" runat="server" ErrorMessage="Veuillez entrer votre Code postal."
                                                    ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true"
                                                    Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                               
                                            </div>
                                        </div>
                                        <div class="CountryContainer Question">
                                            <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_CountryLbl">Pays
                                            </span><span class="fake_input">Canada</span>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelLanguage" class="GenderContainer Question">
                                            <asp:Label ID="LanguageLbl" runat="server" Text="Langue préférée*" AssociatedControlID="PreferredLanguage"></asp:Label>
                                            <asp:DropDownList ID="PreferredLanguage" runat="server">
                                                <asp:ListItem Value="">Sélectionner</asp:ListItem>
                                                <asp:ListItem Value="EN">Anglais</asp:ListItem>
                                                <asp:ListItem Value="FR">Français</asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="LanguageValidator" runat="server" ErrorMessage="Veuillez entrer votre Langue préférée."
                                                    ControlToValidate="PreferredLanguage" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelGender" class="GenderContainer Question">
                                            <asp:Label ID="GenderLbl" runat="server" Text="Sexe*" AssociatedControlID="Gender"></asp:Label>
                                            <asp:DropDownList ID="Gender" runat="server">
                                                <asp:ListItem Value="">Sélectionner</asp:ListItem>
                                                <asp:ListItem Value="F">Femme</asp:ListItem>
                                                <asp:ListItem Value="M">Homme</asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="GenderValidator" runat="server" ErrorMessage="Veuillez entrer votre Sexe."
                                                    ControlToValidate="Gender" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="BirthdayContainer Question">
                                            <asp:Label ID="DOBLbl" runat="server" Text="Date de naissance*" AssociatedControlID="yyyy"></asp:Label>
                                            <asp:DropDownList ID="yyyy" runat="server" CssClass="inputSelectBDay year">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="mm" runat="server" CssClass="inputSelectBDay month">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="dd" runat="server" CssClass="inputSelectBDay day">
                                            </asp:DropDownList>
                                            <br />
                                            <div style="padding-left:100px;font-size:10px;font-style:italic">vous devez avoir au moins 13 ans pour vous inscrire</div>
                                            <div class="ErrorContainer">
                                                <asp:CustomValidator ID="DOBValidator" runat="server" ErrorMessage="Please enter a valid date."
                                                    OnServerValidate="DOBValidate" EnableClientScript="false" Display="Dynamic" CssClass="errormsg validdate"></asp:CustomValidator>
                                                <asp:RequiredFieldValidator ID="yyyyValidator" runat="server" ErrorMessage="Veuillez entrer votre année de naissance."
                                                    ControlToValidate="yyyy" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg year"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="mmValidator" runat="server" ErrorMessage="Veuillez entrer votre mois de naissance."
                                                    ControlToValidate="mm" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg month"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="ddValidator" runat="server" ErrorMessage="Veuillez entrer votre année de naissance."
                                                    ControlToValidate="dd" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg day"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="seperator png-fix">
                                        </div>
                                    </div>              
           
                                    <div class="OptinContainer">
                                        <div class="seperator png-fix"></div>
	                                    <p class="privacy">
		                                   Nous accordons beaucoup d'importance à la protection de vos renseignements personnels. Soyez assuré que Kao Canada Inc. utilisera les renseignements personnels conformément à sa <a href="http://www.kaobrands.com/privacy_policy.asp"
			                                    target="_blank">politique de protection des renseignements personnels</a>.
	                                    </p>
        
                                         <ul>
		                                        <li>
							                <asp:CheckBox Checked="false" ID="bioreTerms" runat="server" />
							                <asp:Label style="width:100%" ID="Label1" runat="server" Text="J'ai lu les <a href='/fr-CA/FormConfig/sampleformterms.aspx' target='_blank'>Conditions Générales d'Utilisation</a>."
								                AssociatedControlID="bioreTerms" />
							               <skm:CheckBoxValidator ID="cbv_bioreTerms" runat="server" ControlToValidate="bioreTerms" ErrorMessage="<br />Please indicate that you have read the <a href='/en-CA/FormConfig/sampleformterms.aspx' target='_blank'>Terms &amp; Conditions</a> by checking the box." CssClass="errormsg" Display="Dynamic"></skm:CheckBoxValidator>
                                                </li>
                                            </ul>

                                    </div> 

                                    <div id="submit-container" class="SubmitContainerSample png-fix">
                                        <asp:Button ID="Button1" runat="server" CssClass="submit png-fix" Text="SOUMETTRE ›" OnClick="Step1_ButtonClick"  />
                                    </div>
                                    <asp:HiddenField ID="hfHookID" runat="server" />
                                    
                                    <!-- Disclaimer -->
                                    <div id="DisclaimerContainer" class="png-fix">
                                        <p>† Aucun achat n’est requis. La promotion débute le 28 janvier 2014 à 12 h (midi), HE, et prend fin le 7 février 2014 à 17 h, HE, ou dès l’épuisement des stocks officiels d’ensembles d’échantillons, selon la première de ces éventualités. Le concours s’adresse aux résidents canadiens autorisés et majeurs. Participation en ligne et modalités complètes au http://www.biore.ca/fr-CA/formconfig/sampleform.aspx. Un maximum de cinq mille (5 000) ensembles d’échantillons sont susceptibles d’être offerts (valeur au détail approximative : 2 $ chacun). </p>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="OptinFormResults" runat="server">
                                 <div class="ConfrimationContainer"><h2>Merci!</h2><p>Merci pour votre intérêt dans Bioré <sup> ® </ sup> Soin. Nous nous réjouissons de vous envoyer les dernières nouvelles sur des événements passionnants, des concours et des informations produit tout au long de l'année.</p></div>  

                            </asp:Panel>
                            <asp:Panel ID="OptinError" runat="server">
                                Une erreur est survenue en tentant de soumettre vos informations.   <asp:Literal ID="litError" runat="server"></asp:Literal>
                            </asp:Panel>
                            <asp:Panel ID="OptinEventExpired" runat="server">
                               Désolé, cette promotion a expiré. <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                            </asp:Panel>
                            <div class="FormBottom png-fix">
                            </div>
                            
                        </div>
                    </div>

                    <script type="text/javascript">

                    </script>

                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script type="text/javascript">
        $(function () {

            if ($(".signUp").length) {
                toggle("noneToggle1");
                toggle("noneToggle2");
                toggle("noneToggle3");
                toggle("noneToggle4");
            }

            $("#ctl00_ContentPlaceHolder1_cureloptin, #ctl00_ContentPlaceHolder1_jergensoptin").click(function () {
                if ($('#ctl00_ContentPlaceHolder1_cureloptin:checked').val()
                    || $('#ctl00_ContentPlaceHolder1_jergensoptin:checked').val()) {
                    $('#ctl00_ContentPlaceHolder1_MultiOptin').attr("checked", "true");
                }
                else { $('#ctl00_ContentPlaceHolder1_MultiOptin').removeAttr("checked"); }
            });
            $("#ctl00_ContentPlaceHolder1_MultiOptin").click(function () {
                if ($('#ctl00_ContentPlaceHolder1_MultiOptin:checked').val()) {
                    $("#ctl00_ContentPlaceHolder1_cureloptin").attr("checked", "true");
                    $("#ctl00_ContentPlaceHolder1_jergensoptin").attr("checked", "true");
                } else {
                    $("#ctl00_ContentPlaceHolder1_cureloptin").removeAttr("checked");
                    $("#ctl00_ContentPlaceHolder1_jergensoptin").removeAttr("checked");
                }
            });

            /*// $(this).prop('checked', false); */
            //Rank Order handling.

            $(".RankHolders input").click(function () {
                rankOrderHandler(".RankHolders input", this);
            });

            $(".RankHolders2 input").click(function () {
                rankOrderHandler(".RankHolders2 input", this);
            });

        });

        function rankOrderHandler(rankClass, inputId) {
            var RankerString = $(inputId).attr('id');
            var RankerCheckedNum = RankerString.substr(RankerString.length - 1);
            var CompareString;
            var CompareNum;

            $(rankClass).each(function () {
                CompareString = $(this).attr('id');
                CompareNum = CompareString.substr(CompareString.length - 1);
                if (CompareString != RankerString && CompareNum == RankerCheckedNum) {
                    if ($("#" + CompareString).attr("checked")) {
                        $("#" + CompareString).attr("checked", false);
                    }
                }
            })
        }

        function toggle(className) {
            var checkToggle = $("." + className + " input:last");
            checkToggle.click(function () {
                if ($("#" + checkToggle.attr('id') + ":checked").val()) {
                    $("." + className + " input").each(function () {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).attr("checked", false);
                            $(this).attr('disabled', 'disabled');
                        }
                    });
                } else {
                    $("." + className + "  input").each(function () {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).removeAttr('disabled');
                        }
                    });
                }
            });
        }
    </script>

</asp:Content>
