﻿<%@ Page Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="NewsletterForm.aspx.cs"
    Inherits="Biore2012.FormConfig.NewsletterForm" %>
<%@ Register TagPrefix="skm" Namespace="skmValidators" Assembly="skmValidators" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%#SquishIt.Framework.Bundle .Css()
        .Add("~/css/formConfig.css")
        .Render("~/css/combinedform_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow">
            </div>
            <div class="centeringDiv">
                <div class="responsive contact">
                    <div id="contactFormWrap">
                        <!--<div id="BrandImageContainer" class="png-fix">
                            <img class="png-fix" src="images/forms/signUpPhoto.jpg" style="border-width: 0px;" />
                        </div>-->
                        <div id="ContactFormContainer">
                            <asp:Panel ID="ContactDetails" runat="server">
                                <!-- Header -->
                                <div id="formHeader">
                                    <h1 id="ctl00_ContentPlaceHolder1_ucForm_PageHeader">
                                        JOIGNEZ-VOUS AU MOUVEMENT POUR UNE BELLE PEAU EN TOUT TEMPS!
                                    </h1>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="OptinForm" runat="server">
                                <!-- Description -->
                                <div class="DescriptionContainer png-fix">
                                    <h2>
                                        Merci de l’intérêt que vous portez aux soins pour la peau Bioré<sup>MD</sup></h2>
                                    <p>Veuillez fournir les renseignements ci-dessous afin de recevoir de l'information sur nos promotions, nos concours et nos actualités les plus récentes.</p>
                                </div>
                                <!-- From Fields -->
                                <div class="FormContainer">
                                    <p class="req png-fix">
                                        <em>Champ obligatoire*</em></p>
                                    <div class="MemberInfoContainer png-fix">
                                        <div class="NameWrapper">
                                            <div class="FirstNameContainer Question">

                                                <div class="mmSweet">
                                                    <asp:Label ID="Label1" runat="server" Text="First Name*" AssociatedControlID="FrstName"></asp:Label>
                                                    <asp:TextBox ID="FrstName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="FNameLbl" runat="server" Text="Prénom*" AssociatedControlID="FName"></asp:Label>
                                                <asp:TextBox ID="FName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="FNameValidator1" runat="server" ErrorMessage="Veuillez entrer votre Prénom."
                                                        ControlToValidate="FName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="FNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your First Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="FName" EnableClientScript="true"
                                                        SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="LastNameContainer Question">
                                                <asp:Label ID="LNameLbl" runat="server" Text="Nom*" AssociatedControlID="LName"></asp:Label>
                                                <asp:TextBox ID="LName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="LNameValidator1" runat="server" ErrorMessage="Veuillez entrer votre Nom."
                                                        ControlToValidate="LName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="LNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Last Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="LName" SetFocusOnError="true"
                                                        Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="EmailWrapper">
                                            <div class="EmailContainer Question">
                                                <asp:Label ID="EmailLbl" runat="server" Text="Adresse de courriel*" AssociatedControlID="Email"></asp:Label>
                                                <asp:TextBox ID="Email" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="EmailValidator1" runat="server" ErrorMessage="Veuillez entrer votre Courriel."
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="EmailValidator2" runat="server" ErrorMessage="Please enter a valid Email Address."
                                                        ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div class="PhoneContainer Question">
                                            <asp:Label ID="MobilePhoneLbl" runat="server" Text="Numéro de téléphone mobile" AssociatedControlID="MobilePhone"></asp:Label>
                                            <asp:TextBox ID="MobilePhone" MaxLength="50" runat="server" />
                                            <div class="ErrorContainer">
                                                <asp:RegularExpressionValidator ID="v_MobilePhone" runat="server" ErrorMessage="Your Mobile Phone is not required, but please do not use special characters in the Mobile Phone field."
                                                    ValidationExpression="^[^<>]+$" ControlToValidate="MobilePhone" EnableClientScript="true"
                                                    SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                <%-- The following RegEx Validators is for US type numbers.  IMPORTANT: 
								 Please test against your user base before using as you may need to modify it for other formats!  --%>
                                                <%--<asp:RegularExpressionValidator ID="PhoneValidator2" runat="server"
								ErrorMessage="<br/>Your Home Phone is not required, but please enter only valid characters."
								ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"
								ControlToValidate="Phone" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg phone"></asp:RegularExpressionValidator>--%>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelAddress" class="AddressContainer">
                                            <div class="AddressWrapper">
                                                <div class="Address1Container Question">
                                                    <asp:Label ID="Address1Lbl" runat="server" Text="Adresse 1*" AssociatedControlID="Address1"></asp:Label>
                                                    <asp:TextBox ID="Address1" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="Address1Validator1" runat="server" ErrorMessage="Veuillez entrer votre Adresse."
                                                            ControlToValidate="Address1" EnableClientScript="true" SetFocusOnError="true"
                                                            Display="Dynamic" CssClass="errormsg" />
                                                        <asp:RegularExpressionValidator ID="Address1Validator2" runat="server" ErrorMessage="Please do not use special characters in the Address 1 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address1" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="Address2Container Question">
                                                    <asp:Label ID="Address2Lbl" runat="server" Text="Adresse 2" AssociatedControlID="Address2"></asp:Label>
                                                    <asp:TextBox ID="Address2" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="Address2Validator1" runat="server" ErrorMessage="Please do not use special characters in the Address 2 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address2" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="CityContainer Question">
                                                    <asp:Label ID="CityLbl" runat="server" Text="Ville*" AssociatedControlID="City"></asp:Label>
                                                    <asp:TextBox ID="City" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="CityValidator1" runat="server" ErrorMessage="Veuillez entrer votre Adresse Ville."
                                                            ControlToValidate="City" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                            CssClass="errormsg" />
                                                        <asp:RegularExpressionValidator ID="CityValidator2" runat="server" ErrorMessage="Please do not use special characters in the City field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="City" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="StateContainer Question">
                                                <asp:Label ID="StateLbl" runat="server" Text="Province*" AssociatedControlID="State"></asp:Label>
                                                <asp:DropDownList ID="State" runat="server">
                                                    <asp:ListItem Value="" Selected="true">Sélectionner la province</asp:ListItem>
                                                    <asp:ListItem Value="1">Alberta</asp:ListItem>
                                                    <asp:ListItem Value="2">Colombie-Britannique</asp:ListItem>
                                                    <asp:ListItem Value="10">Île-du-Prince-Édouard</asp:ListItem>
                                                    <asp:ListItem Value="3">Manitoba</asp:ListItem>
                                                    <asp:ListItem Value="4">Nouveau-Brunswick</asp:ListItem>
                                                    <asp:ListItem Value="7">Nouvelle-Écosse</asp:ListItem>
                                                    <asp:ListItem Value="8">Nunavut</asp:ListItem>
                                                    <asp:ListItem Value="9">Ontario</asp:ListItem>
                                                    <asp:ListItem Value="11">Québec</asp:ListItem>
                                                    <asp:ListItem Value="12">Saskatchewan</asp:ListItem>
                                                    <asp:ListItem Value="5">Terre-Neuve-et-Labrador</asp:ListItem>
                                                    <asp:ListItem Value="6">Territoires du Nord-Ouest</asp:ListItem>
                                                    <asp:ListItem Value="13">Yukon</asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="StateValidator" runat="server" ErrorMessage="Veuillez entrer votre Province."
                                                        ControlToValidate="State" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div class="PostalCodeContainer Question">
                                            <asp:Label ID="PostalCodeLbl" runat="server" Text="Code postal*" AssociatedControlID="PostalCode"></asp:Label>
                                            <asp:TextBox ID="PostalCode" runat="server" MaxLength="20" CssClass="inputTextBox"></asp:TextBox>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="PostalCodeValidator1" runat="server" ErrorMessage="Veuillez entrer votre Code postal."
                                                    ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true"
                                                    Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                               
                                            </div>
                                        </div>
                                        <div class="CountryContainer Question">
                                            <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_CountryLbl">Pays
                                            </span><span class="fake_input">Canada</span>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelLanguage" class="GenderContainer Question">
                                            <asp:Label ID="LanguageLbl" runat="server" Text="Langue préférée*" AssociatedControlID="PreferredLanguage"></asp:Label>
                                            <asp:DropDownList ID="PreferredLanguage" runat="server">
                                                <asp:ListItem Value="">Sélectionner</asp:ListItem>
                                                <asp:ListItem Value="EN">Anglais</asp:ListItem>
                                                <asp:ListItem Value="FR">Français</asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="LanguageValidator" runat="server" ErrorMessage="Veuillez entrer votre Langue préférée."
                                                    ControlToValidate="PreferredLanguage" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelGender" class="GenderContainer Question">
                                            <asp:Label ID="GenderLbl" runat="server" Text="Sexe*" AssociatedControlID="Gender"></asp:Label>
                                            <asp:DropDownList ID="Gender" runat="server">
                                                <asp:ListItem Value="">Sélectionner</asp:ListItem>
                                                <asp:ListItem Value="F">Femme</asp:ListItem>
                                                <asp:ListItem Value="M">Homme</asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="GenderValidator" runat="server" ErrorMessage="Veuillez entrer votre Sexe."
                                                    ControlToValidate="Gender" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="BirthdayContainer Question">
                                            <asp:Label ID="DOBLbl" runat="server" Text="Date de naissance*" AssociatedControlID="yyyy"></asp:Label>
                                            <asp:DropDownList ID="yyyy" runat="server" CssClass="inputSelectBDay year">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="mm" runat="server" CssClass="inputSelectBDay month">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="dd" runat="server" CssClass="inputSelectBDay day">
                                            </asp:DropDownList>
                                            <br />
                                            <div style="padding-left:100px;font-size:10px;font-style:italic">vous devez avoir au moins 13 ans pour vous inscrire</div>
                                            <div class="ErrorContainer">
                                                <asp:CustomValidator ID="DOBValidator" runat="server" ErrorMessage="Please enter a valid date."
                                                    OnServerValidate="DOBValidate" EnableClientScript="false" Display="Dynamic" CssClass="errormsg validdate"></asp:CustomValidator>
                                                <asp:RequiredFieldValidator ID="yyyyValidator" runat="server" ErrorMessage="Veuillez entrer votre année de naissance."
                                                    ControlToValidate="yyyy" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg year"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="mmValidator" runat="server" ErrorMessage="Veuillez entrer votre mois de naissance."
                                                    ControlToValidate="mm" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg month"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="ddValidator" runat="server" ErrorMessage="Veuillez entrer votre année de naissance."
                                                    ControlToValidate="dd" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg day"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="seperator png-fix">
                                        </div>
                                    </div>
                                              
                                    <div id="ctl00_ContentPlaceHolder1_ucForm_QuestionPanel" class="QuestionContainer">
                        			            
                                        <div class="MarketingQuestionContainer">
                                            <div class="CustomQuestiondropdown Question">
					                            <label for="HowDidYouHear" id="HowDidYouHearLBL" class="dropdownLabel">Comment avez-vous entendu parler des soins pour la peau Bioré<sup>MD</sup>?*</label>
					                            <asp:DropDownList ID="HowDidYouHear" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem Value="">S&#233;lectionner...</asp:ListItem>
						                            <asp:ListItem>Essai antérieur des produits</asp:ListItem>
						                            <asp:ListItem>Annonce dans un magazine</asp:ListItem>
						                            <asp:ListItem>Promotion en ligne</asp:ListItem>

						                            <asp:ListItem>Découverte en magasin</asp:ListItem>
						                            <asp:ListItem>Internet</asp:ListItem>
                                                    <asp:ListItem>Courriel</asp:ListItem>
                                                    <asp:ListItem>Ami(e)</asp:ListItem>
                                                    <asp:ListItem>Autre</asp:ListItem>
					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
						                            <asp:RequiredFieldValidator ID="v_HowDidYouHear" runat="server" ControlToValidate="HowDidYouHear" ErrorMessage="Veuillez répondre à toutes les questions requises." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
					                            </div>
				                            </div>
                                            <div class="CustomQuestiondropdown Question">
					                            <label for="CleanserQualityRankOne" id="Label2" class="dropdownLabel">Quels sont les effets importants que vous recherchez dans un nettoyant pour le visage?*<br />(classez les 6 options proposées ci-dessous de 1 à 6, 1 étant la plus importante et 6, la moins importante)</label>
                                                <div class="RankHolders">
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Il laisse mon visage propre.</div>
                                                        <asp:RadioButtonList ID="CleanserQualityRankOne" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Il donne à mon visage une apparence saine.</div>
                                                        <asp:RadioButtonList ID="CleanserQualityRankTwo" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Il nettoie la peau en profondeur.</div>
                                                        <asp:RadioButtonList ID="CleanserQualityRankThree" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Il régule la production de sébum et contrôle la brillance.</div>
                                                        <asp:RadioButtonList ID="CleanserQualityRankFour" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Il prévient l’apparition des boutons d’acné ou les traite.</div>
                                                        <asp:RadioButtonList ID="CleanserQualityRankFive" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Il rend les pores de ma peau moins apparents.</div>
                                                        <asp:RadioButtonList ID="CleanserQualityRankSix" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                </div>

					                              <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="CleanserQualityRankOne" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="CleanserQualityRankOne" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="CleanserQualityRankThree" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="CleanserQualityRankFour" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="CleanserQualityRankFive" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="CleanserQualityRankSix" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
				                            </div>

                                            <div class="CustomQuestiondropdown Question">
					                            <label for="ChooseCleanerRankOne" id="Label2" class="dropdownLabel">En fonction de quels critères choisissez-vous votre nettoyant pour le visage?*<br />(classez les 6 options proposées ci-dessous de 1 à 6, 1 étant la plus importante et 6, la moins importante)</label>
                                                <div class="RankHolders2">
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Il m’a été recommandé par un ami ou un membre de ma famille.</div>
                                                        <asp:RadioButtonList ID="ChooseCleanerRankOne" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Il m’a été recommandé par un médecin ou un dermatologue. </div>
                                                        <asp:RadioButtonList ID="ChooseCleanerRankTwo" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Son prix est raisonnable.</div>
                                                        <asp:RadioButtonList ID="ChooseCleanerRankThree" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Il s’agit de la marque que j’utilise toujours.</div>
                                                        <asp:RadioButtonList ID="ChooseCleanerRankFour" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Il convient à mon type de peau.</div>
                                                        <asp:RadioButtonList ID="ChooseCleanerRankFive" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">J’aime les effets ou les caractéristiques annoncés.</div>
                                                        <asp:RadioButtonList ID="ChooseCleanerRankSix" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                </div>

					                              <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ChooseCleanerRankOne" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ChooseCleanerRankTwo" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ChooseCleanerRankThree" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ChooseCleanerRankFour" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="ChooseCleanerRankFive" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ChooseCleanerRankSix" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
				                            </div>

				                            <div class="CustomQuestiondropdown Question">
					                            <label for="WhatSkinConcerns" id="WhatSkinConcernsLBL" class="dropdownLabel">À quelles exigences particulières de votre peau le nettoyant doit-il répondre?<br />(sélectionnez une réponse)*</label>
					                              <asp:DropDownList ID="WhatSkinConcerns" runat="server" CssClass="dropDownAnswer">
					                                <asp:ListItem Value="">S&#233;lectionner...</asp:ListItem>
						                            <asp:ListItem>Nettoyage quotidien (peau normale)</asp:ListItem>
						                            <asp:ListItem>Peau sèche</asp:ListItem>
						                            <asp:ListItem>Peau grasse</asp:ListItem>
						                            <asp:ListItem>Peau mixte – normale à grasse</asp:ListItem>
						                            <asp:ListItem>Peau mixte – normale à sèche</asp:ListItem>
                                                    <asp:ListItem>Peau mixte – grasse/sèche</asp:ListItem>
                                                    <asp:ListItem>Peau sujette à l’acné</asp:ListItem>
					                              </asp:DropDownList>
					                              <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="v_q1" runat="server" ControlToValidate="WhatSkinConcerns" ErrorMessage="Veuillez répondre à toutes les questions requises." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
				                            </div>
				                            <div class="CustomQuestiondropdown Question">
					                            <label for="WhatFacialCleanser" id="WhatFacialCleanserLBL" class="dropdownLabel">Quelle forme de nettoyant pour le visage utilisez-vous le plus souvent? (choisissez une réponse)*</label>
					                            
					                                <asp:DropDownList ID="WhatFacialCleanser" runat="server" CssClass="dropDownAnswer">
					                                   <asp:ListItem Value="">S&#233;lectionner...</asp:ListItem>
						                                <asp:ListItem>Nettoyant liquide/en gel ou en crème</asp:ListItem>
						                                <asp:ListItem>Exfoliant pour le visage</asp:ListItem>
						                                <asp:ListItem>Tonique/astringent</asp:ListItem>
						                                <asp:ListItem>Lingettes de nettoyage</asp:ListItem>
						                                <asp:ListItem>Tampons anti-acné /anti-imperfections</asp:ListItem>
                                                        <asp:ListItem>Démaquillant (liquide ou lingettes)</asp:ListItem>
					                                </asp:DropDownList>
					                                <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="v_WhatFacialCleanser" runat="server" ControlToValidate="WhatFacialCleanser" ErrorMessage="Veuillez répondre à toutes les questions requises." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                                </div>

				                            </div>
                                            <div class="CustomQuestiondropdown Question">
					                            <label for="WhichFaceCleanserBrands" id="WhichFaceCleanserBrandsLBL" class="dropdownLabel">Lesquelles des marques de nettoyants pour le visage suivantes avez-vous utilisées dans les 12 derniers mois?*</label>
					                            <asp:DropDownList ID="WhichFaceCleanserBrands" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem Value="">S&#233;lectionner...</asp:ListItem>
						                            <asp:ListItem Value="Aveeno">Aveeno&#174; produits</asp:ListItem>
                                                    <asp:ListItem Value="Biore">Bior&#233;&#174; produits </asp:ListItem>
                                                    <asp:ListItem Value="Cetaphil">Cetaphil&#174; produits</asp:ListItem>
						                            <asp:ListItem Value="Clean and Clear">Clean &amp; Clear&#174; produits</asp:ListItem>
						                            <asp:ListItem Value="Clearasil">Clearasil&#174; produits</asp:ListItem>
						                            <asp:ListItem Value="Garnier">Garnier produits</asp:ListItem>
						                            <asp:ListItem Value="L'Oreal">L' Oréal&#174; produits</asp:ListItem>
						                            <asp:ListItem Value="Nivea">Nivea&#174; produits</asp:ListItem>
						                            <asp:ListItem Value="Neutrogena">Neutrogena&#174; produits</asp:ListItem>
                                                    <asp:ListItem Value="Olay">Olay&#174; produits</asp:ListItem>
						                            <asp:ListItem Value="Simple">Simple&#174; produits</asp:ListItem>
						                            <asp:ListItem Value="St. Ives">St. Ives&#174; produits</asp:ListItem>
						                            <asp:ListItem Value="Autre">Autre</asp:ListItem>
						                            <asp:ListItem Value="Aucune de ces responses">Aucune de ces résponses</asp:ListItem>
					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
						                             <asp:RequiredFieldValidator ID="v_WhichFaceCleanserBrands" runat="server" ControlToValidate="WhichFaceCleanserBrands" ErrorMessage="Veuillez répondre à toutes les questions requises." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
					                            </div>
				                            </div>

                                            <div class="CustomQuestioncheckbox Question trait413 cbVert noneToggle1">
				                                <label for="WhichDoYouUse" id="WhichDoYouUseLBL" class="checkboxLabel">Si vous vous servez des soins pour la peau produits Bioré<sup>MD</sup>, lesquels parmi les suivants utilisez-vous actuellement? (sélectionnez tous ceux qui s’appliquent)* </label>
				                                <asp:CheckBoxList ID="WhichDoYouUse" RepeatDirection="Vertical" runat="server" RepeatLayout="Flow" CssClass="checkboxAnswer">

							                        <asp:ListItem Value="Nettoyant équilibrant peau mixte de BioréMD">Nettoyant équilibrant peau mixte de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Nettoyant activé par la vapeur de BioréMD">Nettoyant activé par la vapeur de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Nettoyant revitalisant 4 en 1 de BioréMD">Nettoyant revitalisant 4 en 1 de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Nettoyant granuleux pour les pores de BioréMD">Nettoyant granuleux pour les pores de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Gommage quotidien purifiant de BioréMD">Gommage quotidien purifiant de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Bandes de nettoyage en profondeur de BioréMD">Bandes de nettoyage en profondeur de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Bandes de nettoyage en profondeur - Emballage assorti de BioréMD">Bandes de nettoyage en profondeur - Emballage assorti de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Bandes de nettoyage en profondeur Ultra de BioréMD">Bandes de nettoyage en profondeur Ultra de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Nettoyant glacé anti-acné de BioréMD">Nettoyant glacé anti-acné de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Astringent anti-acné de BioréMD">Astringent anti-acné de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Nettoyant chauffant en crème anti-points noirs de BioréMD">Nettoyant chauffant en crème anti-points noirs de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="Lingettes démaquillantes de BioréMD">Lingettes démaquillantes de Bioré&lt;sup&gt;MD&lt;/sup&gt;</asp:ListItem>
							                        <asp:ListItem Value="None of the above">Aucune de ces réponses</asp:ListItem>
							                    </asp:CheckBoxList>
							                    
				         <span class="cbinput"></span>
				         
					                            <div class="ErrorContainer">
						                            <skm:CheckBoxListValidator ID="HairAppliancesLblVal" ControlToValidate="WhichDoYouUse"
                                                    runat="server" Display="Dynamic" CssClass="errormsg" ErrorMessage="Veuillez répondre à toutes les questions requises." />
					                            </div>
				                            </div>

                                            <div class="CustomQuestiondropdown Question">
					                            <label for="RecommendToFriends" id="RecommendToFriendsLBL" class="dropdownLabel">Dans quelle mesure recommanderiez-vous vos produits pour la peau favoris à vos amis?*</label>
					                            <asp:DropDownList ID="RecommendToFriends" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem Value="">S&#233;lectionner...</asp:ListItem>
						                            <asp:ListItem>Très probable</asp:ListItem>
						                            <asp:ListItem>Plutôt probable</asp:ListItem>
						                            <asp:ListItem>Plutôt improbable</asp:ListItem>
						                            <asp:ListItem>Improbable</asp:ListItem>

					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
						                            <asp:RequiredFieldValidator ID="v_RecommendToFriends" runat="server" ControlToValidate="RecommendToFriends" ErrorMessage="Veuillez répondre à toutes les questions requises." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
					                            </div>
				                            </div>
				                            
				                            <div class="CustomQuestioncheckbox Question trait419 cbVert">
					                            <label for="WhatBioreInterests" id="WhatBioreInterestsLBL" class="checkboxLabel">Qu’aimeriez-vous recevoir de Bioré? (Cochez toutes les réponses pertinentes.)*</label>

					                           <asp:CheckBoxList ID="WhatBioreInterests" RepeatDirection="Vertical" runat="server" RepeatLayout="Flow" CssClass="checkboxAnswer">
							                        <asp:ListItem>Des offres spéciales, des promotions et des offres de concours</asp:ListItem>
							                        <asp:ListItem>De l’information sur les nouveaux produits</asp:ListItem>
							                        <asp:ListItem>Des conseils et de l’information sur les soins pour la peau</asp:ListItem>
							                        
							                    </asp:CheckBoxList>
							                    
				         <span class="cbinput"></span>
					                            <div class="ErrorContainer">
						                            <skm:CheckBoxListValidator ID="v_WhatBioreInterests" ControlToValidate="WhatBioreInterests"
                                                    runat="server" Display="Dynamic" CssClass="errormsg" ErrorMessage="Veuillez répondre à toutes les questions requises." />
					                            </div>
					                           
				                            </div>
				                            <div class="CustomQuestiondropdown Question">
					                            <label for="ContactHowOften" id="ContactHowOftenLBL" class="dropdownLabel">À quelle fréquence aimeriez-vous recevoir des nouvelles?*</label>


					                            <asp:DropDownList ID="ContactHowOften" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem Value="">S&#233;lectionner...</asp:ListItem>
						                            <asp:ListItem>Très rarement</asp:ListItem>
							                        <asp:ListItem>Quelques fois par année</asp:ListItem>
							                        <asp:ListItem>Régulièrement</asp:ListItem>
							                        <asp:ListItem>Tout le temps (j’aimerais avoir le plus d’information possible)</asp:ListItem>

					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
                                                     <asp:RequiredFieldValidator ID="v_ContactHowOften" runat="server" ControlToValidate="ContactHowOften" ErrorMessage="Veuillez répondre à toutes les questions requises." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                </div>

				                            </div>
			                            </div>
		                            </div>
           
                                    <div class="OptinContainer">
                                        <div class="seperator png-fix"></div>
	                                    <p class="privacy">
		                                   Nous accordons beaucoup d'importance à la protection de vos renseignements personnels. Soyez assuré que Kao Canada Inc. utilisera les renseignements personnels conformément à sa <a href="http://www.kaobrands.com/privacy_policy.asp"
			                                    target="_blank">politique de protection des renseignements personnels</a>.
	                                    </p>

	                                    <div class="CurrentSiteOptinContainer">
	                                        <ul>
		                                        <li>
		                                            <asp:CheckBox ID="bioreoptin" runat="server" />
			                                        <asp:Label ID="bioreoptinLabel" runat="server" Text="Oui, je veux recevoir des renseignements et des offres sur les produits Bioré<sup>MD</sup>." AssociatedControlID="bioreoptin" CssClass="siteOptinChkbox" />

			                                        <div class="ErrorContainer">
                                    			        <skm:CheckBoxValidator ID="cbv_bioreOptin" runat="server" ControlToValidate="bioreoptin" ErrorMessage="Si vous voulez devenir un membre Bioré<sup>MD</sup> Brand, s'il vous plaît vérifier Oui!" CssClass="errormsg" Display="Dynamic"></skm:CheckBoxValidator>
				                                    </div>

		                                        </li>
	                                        </ul>
	                                    </div>
                                        
                                        <!--<div id="ctl00_ContentPlaceHolder1_ucForm_ucFormOptin_PanelMultiOptin" class="MultiBrandOptinContainer">
                                    			            
		                                    <ul>            
			                                    <li id="optinToAll">
			                                        <asp:CheckBox ID="MultiOptin" runat="server" CssClass="multiOptin" />
			                                        <asp:Label ID="MultiOptinLabel" runat="server" Text="Yes, I'd like to receive emails and newsletters from other great products from Kao Brands Company:" AssociatedControlID="MultiOptin" />
			        
			                                    </li>			

			                                    <li id="ctl00_ContentPlaceHolder1_ucForm_ucFormOptin_curelListItem">
			                                        <asp:CheckBox ID="cureloptin" runat="server" />
			                                <asp:Label ID="cureloptinLabel" runat="server" Text="Cur&eacute;l<sup>&reg;</sup></a> Skincare's full line of hand, foot and body moisturizers delivers freedom from dry skin." AssociatedControlID="cureloptin"
				                                CssClass="brandLabels" />
			                         
			                                    </li>
			                                    <li id="ctl00_ContentPlaceHolder1_ucForm_ucFormOptin_jergensListItem">
			                                        <asp:CheckBox ID="jergensoptin" runat="server" />
			                                        <asp:Label ID="jergensoptinLabel" runat="server" Text="Jergens<sup>&reg;</sup></a> Skincare collection of moisturizers delivers a natural glow, smooth and firm skin and an allure that captivates." AssociatedControlID="jergensoptin"
				                                CssClass="brandLabels" />

			                                    </li>

			                                    <li>
				                                    <p class="privDisclaimer">Before submitting your information, please view our <a
					                                    href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">Privacy Policy</a>.</p>
			                                    </li>
		                                    </ul>
                                    	
		                                    </div>	-->	

                                    </div> 

                                    <div id="submit-container" class="SubmitContainer png-fix">
                                        <asp:Button ID="Button1" runat="server" CssClass="submit buttonLink png-fix" Text="SOUMETTRE ›" OnClick="Step1_ButtonClick"  />
                                    </div>
                                    <asp:HiddenField ID="hfHookID" runat="server" />
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="OptinFormResults" runat="server">
                                 <div class="ConfrimationContainer"><h2>Merci!</h2><p>Merci pour votre intérêt dans Bioré <sup> ® </ sup> Soin. Nous nous réjouissons de vous envoyer les dernières nouvelles sur des événements passionnants, des concours et des informations produit tout au long de l'année.</p></div>  

                            </asp:Panel>
                            <asp:Panel ID="OptinError" runat="server">
Une erreur est survenue en tentant de soumettre vos informations. <asp:Literal ID="litError" runat="server"></asp:Literal>
                            </asp:Panel>
                            <div class="FormBottom png-fix">
                            </div>
                            <!-- Disclaimer -->
                            <div id="DisclaimerContainer" class="png-fix">
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">

                    </script>

                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script type="text/javascript">
        $(function () {

            if ($(".signUp").length) {
                toggle("noneToggle1");
                toggle("noneToggle2");
                toggle("noneToggle3");
                toggle("noneToggle4");
            }

            $("#ctl00_ContentPlaceHolder1_cureloptin, #ctl00_ContentPlaceHolder1_jergensoptin").click(function () {
                if ($('#ctl00_ContentPlaceHolder1_cureloptin:checked').val()
                    || $('#ctl00_ContentPlaceHolder1_jergensoptin:checked').val()) {
                    $('#ctl00_ContentPlaceHolder1_MultiOptin').attr("checked", "true");
                }
                else { $('#ctl00_ContentPlaceHolder1_MultiOptin').removeAttr("checked"); }
            });
            $("#ctl00_ContentPlaceHolder1_MultiOptin").click(function () {
                if ($('#ctl00_ContentPlaceHolder1_MultiOptin:checked').val()) {
                    $("#ctl00_ContentPlaceHolder1_cureloptin").attr("checked", "true");
                    $("#ctl00_ContentPlaceHolder1_jergensoptin").attr("checked", "true");
                } else {
                    $("#ctl00_ContentPlaceHolder1_cureloptin").removeAttr("checked");
                    $("#ctl00_ContentPlaceHolder1_jergensoptin").removeAttr("checked");
                }
            });



            /*// $(this).prop('checked', false); */
            //Rank Order handling.

            $(".RankHolders input").click(function () {
                rankOrderHandler(".RankHolders input", this);
            });

            $(".RankHolders2 input").click(function () {
                rankOrderHandler(".RankHolders2 input", this);
            });

        });

        function rankOrderHandler(rankClass, inputId) {
            var RankerString = $(inputId).attr('id');
            var RankerCheckedNum = RankerString.substr(RankerString.length - 1);
            var CompareString;
            var CompareNum;

            $(rankClass).each(function () {
                CompareString = $(this).attr('id');
                CompareNum = CompareString.substr(CompareString.length - 1);
                if (CompareString != RankerString && CompareNum == RankerCheckedNum) {
                    if ($("#" + CompareString).attr("checked")) {
                        $("#" + CompareString).attr("checked", false);
                    }
                }
            })
        }
        
        
        function toggle(className) {
            var checkToggle = $("." + className + " input:last");
            checkToggle.click(function() {
                if ($("#" + checkToggle.attr('id') + ":checked").val()) {
                    $("." + className + " input").each(function() {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).attr("checked", false);
                            $(this).attr('disabled', 'disabled');
                        }
                    });
                } else {
                    $("." + className + "  input").each(function() {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).removeAttr('disabled');
                        }
                    });
                }
            });
        }
    </script>

</asp:Content>
