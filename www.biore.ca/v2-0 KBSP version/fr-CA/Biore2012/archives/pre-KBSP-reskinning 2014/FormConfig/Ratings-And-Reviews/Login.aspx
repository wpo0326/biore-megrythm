﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Biore2012.Forms.RatingsAndReviews.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/login.css")
        .Render("~/CSS/combinedlogin_#.css")
    %>
    <!--<link href="<%= System.Configuration.ConfigurationManager.AppSettings["cssprefix"] %>login.min.css" rel="stylesheet" type="text/css" media="screen,projection" />  --> 
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="contactFormWrap">
        <div id="BrandImageContainer">
            <!--<img style="border-width: 0px;" src="<%= System.Configuration.ConfigurationManager.AppSettings["imageprefix"] %>forms/ratingsReviewModel.jpg" id="ctl00_ContentPlaceHolder1_ucFormConfig_Image1" />-->
	    </div>
	    <div id="ContactFormContainer">
	         <!-- Header --> 
            <div id="formHeader">
                <h1 id="PageHeader" runat="server">
                    OUVRIR UNE SESSION
                </h1> 
            </div>
            <!-- Description --> 
            <div class="DescriptionContainer" id="ctl00_ContentPlaceHolder1_ucFormConfig_DescriptionContainer">
                <h2 class="first">Merci de l’intérêt que vous portez à l’évaluation des produits capillaires Biore<sup>MD</sup>!</h2>
                <p><br />Pour soumettre une évaluation, veuillez ouvrir une session à l’aide de votre adresse de courriel et de votre mot de passe.</p>
	        </div>
	        <p class="req"><em>Champ obligatoire*</em></p>
	        <div class="Question">
                <asp:Label ID="Label12" runat="server" AssociatedControlID="loginUsername">Courriel* </asp:Label>
                <asp:TextBox runat="server" MaxLength="70" CssClass="text" ID="loginUsername"></asp:TextBox>
            </div>          
            <div class="ErrorContainer">
                <asp:RequiredFieldValidator ID="reqEmail" runat="server" Display="Dynamic" ErrorMessage="Veuillez entrer une adresse de courriel."
                    ControlToValidate="loginUsername" EnableClientScript="true" SetFocusOnError="true"
                    CssClass="errormsg"></asp:RequiredFieldValidator>
            </div>
            <div class="ErrorContainer">
                <asp:RegularExpressionValidator ID="regEmail" runat="server" Display="Dynamic" ErrorMessage="Veuillez entrer une adresse de courriel valide."
                    ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                    ControlToValidate="loginUsername" EnableClientScript="true" SetFocusOnError="true"
                    CssClass="errormsg"></asp:RegularExpressionValidator>
            </div>
            <div class="Question">
                <asp:Label ID="Label1" runat="server" AssociatedControlID="loginPassword">Mot de passe* </asp:Label>
                <asp:TextBox runat="server" MaxLength="70" CssClass="text loginPassword" TextMode="password" ID="loginPassword"></asp:TextBox>
            </div>
            <div class="ErrorContainer">
                <asp:RequiredFieldValidator ID="reqPassword" runat="server" Display="Dynamic"
                    ErrorMessage="Please enter your Password." ControlToValidate="loginPassword" EnableClientScript="true"
                    SetFocusOnError="true"  CssClass="errormsg"></asp:RequiredFieldValidator>
            </div>
            <div class="ErrorContainer">
                <asp:TextBox runat="server" ID="username"  Visible="false" Text="username" />
                <asp:RequiredFieldValidator ID="reqUsername" runat="server" Display="Dynamic"
                    ErrorMessage="" ControlToValidate="username" EnableClientScript="true"
                    SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
            </div>
            <div class="Buttons">
                <asp:Button id="Button" Text="OUVRIR UNE SESSION" runat="server" OnClick="login_Click" CssClass="buttonLink submit"></asp:Button>
            </div>
            <div class="Buttons">
                <asp:Literal ID="literalRegisterButton" runat="server" />
                <!-- <a class="buttonLink" href="register.aspx">FIRST REVIEW? REGISTER NOW</a> --><br />
                <asp:Literal ID="literalForgotPasswordButton" runat="server" />
                <!-- <a class="buttonLink" href="resetPassword.aspx">Forgot your password?</a> -->
            </div>  
 	    </div>
    </div>
    
</asp:Content>