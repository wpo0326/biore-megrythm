﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ResetPassword.aspx.cs" Inherits="Biore2012.Forms.RatingsAndReviews.ResetPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/login.css")
        .Render("~/CSS/combinedlogin_#.css")
    %>
    <!--<link href="<%= System.Configuration.ConfigurationManager.AppSettings["cssprefix"] %>login.min.css" rel="stylesheet" type="text/css" media="screen,projection" />  -->   

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="contactFormWrap">
        <div id="BrandImageContainer">
            <!--<img style="border-width: 0px;" src="<%= System.Configuration.ConfigurationManager.AppSettings["imageprefix"] %>forms/ratingsReviewModel.jpg" id="ctl00_ContentPlaceHolder1_ucFormConfig_Image1" />-->
        </div>
        <div id="ContactFormContainer">
            <!-- Header -->
            <div id="formHeader">
                <h1 id="PageHeader" runat="server">MODIFIER LE MOT DE PASSE</h1>
            </div>
            <!-- Description -->
            <asp:Panel ID="formPanel" runat="server" Visible="true">
                <div class="DescriptionContainer">
                    <h2 class="first">
                        Pour modifier votre mot de passe, veuillez entrer l’adresse de courriel utilisée pour vous inscrire et nous vous enverrons un nouveau mot de passe.</h2>
                </div>
                <p class="req">
                    <em>Champ obligatoire*</em></p>
                <div class="Question">
                    <asp:Label ID="Label6" runat="server" AssociatedControlID="forgotPasswordUsername">Courriel*</asp:Label>
                    <asp:TextBox runat="server" MaxLength="70" CssClass="text" ID="forgotPasswordUsername"></asp:TextBox>
                </div>
                <%--<div class="ErrorContainer" id="EmailError">
                    <asp:Literal runat="server" ID="literalEmailError" />
                </div>--%>
                <div class="ErrorContainer">
                    <asp:RequiredFieldValidator ID="reqEmail" runat="server" Display="Dynamic" ErrorMessage="Please enter your Email Address."
                        ControlToValidate="forgotPasswordUsername" EnableClientScript="true" SetFocusOnError="true"
                        CssClass="errormsg"></asp:RequiredFieldValidator>
                </div>
                <div class="ErrorContainer">
                    <asp:RegularExpressionValidator ID="regEmail" runat="server" Display="Dynamic" ErrorMessage="Please enter a valid Email Address."
                        ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                        ControlToValidate="forgotPasswordUsername" EnableClientScript="true" SetFocusOnError="true"
                        CssClass="errormsg"></asp:RegularExpressionValidator>
                </div>
                <div class="ErrorContainer">
                    <asp:TextBox runat="server" ID="username" Visible="false" Text="username" />
                    <asp:RequiredFieldValidator ID="reqUsername" runat="server" Display="Dynamic" ErrorMessage=""
                        ControlToValidate="username" EnableClientScript="true" SetFocusOnError="true"
                        CssClass="errormsg"></asp:RequiredFieldValidator>
                </div>
                <div class="Buttons">
                    <asp:Button ID="Button" runat="server" OnClientClick="return passwordValidate()"
                        OnClick="reset_Click" CssClass="buttonLink submit" Text="Reset My Password">
                    </asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="confirmationPanel" runat="server" Visible="false">
                <div class="DescriptionContainer">
                    <h2 class="first">
                        Je vous remercie! Nous avons envoyé un email contenant un nouveau mot de passe à l'adresse associée à votre compte.</h2>
                    <p>
                        <br />
                        <asp:Literal ID="literalLoginButton" runat="server" />
                        <!-- <a href="Login.aspx" class="buttonLink">Login</a> -->
                        <br />
                    </p>
                </div>
            </asp:Panel>
            <asp:Panel ID="lockedOutPanel" runat="server" Visible="false">
                <div class="DescriptionContainer">
                    <h2 class="first">
                        Votre compte a été bloqué en raison d’un nombre excessif de tentatives ratées d’ouverture de session Essayez de nouveau dans 30 minutes</h2>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
