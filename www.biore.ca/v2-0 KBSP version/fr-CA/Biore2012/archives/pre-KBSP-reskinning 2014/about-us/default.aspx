﻿<%@ Page Title="About Bior&eacute;&reg; Skincare | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.about_us._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Pour en savoir plus sur les soins pour la peau BioréMD" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
                .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <!--<div id="polaroids">
                    <img src="../images/about/productGrouping.jpg" alt="" />
                </div>-->
                <div id="content" style="margin-left:4%">
                    <h1>À propos des soins pour la peau Bioré<sup>MD</sup></h1>
                    <p>
                        <img src="../images/about/productGroupingSmall.jpg" alt="" id="productGroupingSmall" />
                        De nettoyage en profondeur à soin purifiant en passant par les démaquillants, les produits de BioréMD aident à garder votre peau belle et propre. Passez du bureau au centre de conditionnement, puis aux activités de la fin de semaine sans vous soucier de votre peau, toujours prête.
                    </p>                    
                    <p class="imageText"><img src="../images/about/availableAtStores.png" alt="Les soins pour la peau BioréMD sont offerts dans certains magasins d’alimentation, pharmacies et grandes surfaces. " /></p>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

