<!DOCTYPE html>
<!--[if IE 7 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie7 ie"><![endif]-->
<!--[if IE 8 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie8 ie"><![endif]-->
<!--[if IE 9 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie9 ie"><![endif]-->
<!--[if !IE]><!--><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" ><!--<![endif]-->
<head>
<title>
	Bior� Combination Skin Balancing Cleanser Sampling Guidelines
</title>
    
    <link rel="stylesheet" type="text/css" href="/en-CA/css/global.css" />
<style type="text/css">
#mainbody{width:80%; margin-left:auto;margin-right:auto;}
h1 {padding:10px 0px 10px 0px;}
p {padding-bottom:20px}
</style>
</head>

<body>
<div id="mainbody">

<div id="languageToggle"><a href="/en-CA/facebook/sampling-guidelines.aspx">English</a></div>

<h1>Directives de distribution gratuite des 30 000 �chantillons</h1>

<p>Directives de distribution gratuite des 30 000 �chantillons (7 ml) de nettoyant �quilibrant peau mixte de Bior�MD du 13 f�vrier au 4 juin 2012 (y compris un bon de r�duction de 2 $ valable sur le prochain achat de nettoyant �quilibrant peau mixte de 200 ml) aux premiers citoyens canadiens admissibles qui s�inscriront en ligne � l�adresse suivante  <a href="http://www.facebook.com/BioreCanada?sk=app_220813114676908">www.facebook.com/BioreCanada?sk=app_220813114676908</a>, en passant par l�onglet Nettoyant �quilibrant peau mixte de Facebook. Les participants doivent �tre adeptes de Bior�MD pour �tre en droit de recevoir un �chantillon. La pr�sente offre vous permet d�obtenir un (1) �chantillon de nettoyant �quilibrant peau mixte de 7 ml, qui sera envoy� au domicile par la poste dans un d�lai de trois � six semaines suivant l�inscription. Pour �tre admissible � recevoir un �chantillon gratuit, vous devez : i) �tre un citoyen canadien dont la r�sidence permanente se situe au Canada et ii) �tre majeur selon les lois en vigueur dans votre province ou votre territoire de r�sidence. Vous devez vous inscrire en ligne au  <a href="http://www.facebook.com/BioreCanada?sk=app_220813114676908">www.facebook.com/BioreCanada?sk=app_220813114676908</a> en indiquant votre nom et pr�nom, votre adresse postale compl�te et votre adresse de courriel. Limite d�un (1) seul �chantillon par personne, par adresse de courriel et par m�nage. Pour �viter toute ambigu�t�, vous ne pouvez utiliser qu�une (1) seule adresse de courriel pour participer � cette campagne. Si vous essayez de vous inscrire plus d�une fois, toutes les demandes subs�quentes seront annul�es. Un avis sera publi� sur le site Internet lorsque les 30 000 �chantillons auront �t� distribu�s et que l�offre ne sera plus en vigueur. 
</p> 

<p>La distribution d��chantillons et l�onglet Nettoyant �quilibrant peau mixte de Bior�MD ne sont en aucun cas commandit�s, approuv�s ou g�r�s par Facebook ou encore associ�s � ce dernier. Vous convenez que vos renseignements sont divulgu�s aupr�s du commanditaire et non aupr�s de Facebook. Les renseignements que vous divulguez sont uniquement utilis�s aux fins de la gestion de la distribution d��chantillons, en conformit� avec � la politique de confidentialit� du commanditaire (voir ci-dessous). Les participants d�gagent Facebook de toute responsabilit� li�e � la pr�sente campagne d��chantillons. Les questions, plaintes ou commentaires concernant la campagne doivent �tre adress�s au commanditaire et non � Facebook.</p>

<p>
Kao Canada Inc. demande les renseignements personnels des participants uniquement aux fins de la gestion de la campagne. Kao Canada Inc., n�est pas autoris�e � utiliser vos renseignements personnels d�une mani�re diff�rente de celle indiqu�e et est soumise aux lois f�d�rales et provinciales applicables en mati�re de confidentialit�. Consultez la politique de confidentialit� de Kao Canada Inc. � l�adresse suivante : <a href="http://www.kaobrands.com/privacy_policy.asp">http://www.kaobrands.com/privacy_policy.asp</a> pour obtenir de plus amples renseignements sur la protection des renseignements personnels et la confidentialit�. </p>
</div>
</body>

</html>







