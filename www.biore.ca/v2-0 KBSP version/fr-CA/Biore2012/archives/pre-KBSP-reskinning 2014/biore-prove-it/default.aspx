﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.prove_it_landing.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedproveit_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <h1 id="proveItLogo"><img src="../images/proveIt/proveItLogo.png" alt="Prove It!&trade;" /></h1>
                <h2><img src="../images/proveIt/getRewards.png" alt="Get Rewards for Your Routine" /></h2>
                <p>Have we told you lately how much we love our loyal fans? Well we do! And to thank you, we've launched <strong>Prove It!&trade; Reward Points</strong> on Facebook. Join us today to start earning points towards coupons, free products, and more to come! </p>
                <p><strong>Prove It!&trade; Reward Points</strong> on Facebook isn't supported by your mobile device. Access it the next time you're on a desktop computer.</p>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>