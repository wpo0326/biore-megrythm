﻿<%@ Page Title="Sitemap | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.sitemap._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1>Plan du site</h1>
                 <div id="linkList">
                     <div id="prodLinks">
                         <ul class="col" id="col1">
                            <li><a href="../biore-facial-cleansing-products">Nos produits <span class="arrow">&rsaquo;</span></a></li>
                            <li>
                                <h2 class="pie deepCleansing"><span class="subNavHeadlines">Pensez <span class="greenBold">propre!<sup>MC</sup></span></span></h2>
                                <ul>
                                    <li><a href="../dont-be-dirty/blue-agave-baking-soda-instant-warming-clay-mask">Agave Bleu + Bicarbonate de Soude Masque Chauffant Instantané à L’argile de Biore</a></li>
                                                 <li><a href="../dont-be-dirty/charcoal-whipped-mask">Masque détox onctueux et purifiant au charbon de Biore</a></li>
                                                 <li><a href="../dont-be-dirty/blue-agave-baking-soda-whipped-mask">Masque détox onctueux et nourrissant au agave bleu + bicarbonate de soude de Biore</a></li>
                                                <li><a href="../dont-be-dirty/two-step-charcoal-pore-kit">Ensemble pour les pores à 2 étapes au charbon de Biore</a></li>
                                                <li><a href="../dont-be-dirty/blue-agave-baking-soda-balancing-pore-cleanser">Nettoyant équilibrant au agave bleu + bicarbonate de soude pour les pores de Biore</a></li>
                                	<li><a href="../dont-be-dirty/baking-soda-cleansing-scrub">NETTOYANT EXFOLIANT AU BICARBONATE DE SOUDE</a></li>
                                	<li><a href="../dont-be-dirty/baking-soda-pore-cleanser">NETTOYANT AU BICARBONATE DE SOUDE POUR LES PORES</a></li>
                                	<li><a href="../dont-be-dirty/charcoal-bar"><!--<span class="new">New</span>-->Pain désincrustant au charbon pour les pores</a></li>
                                    <li><a href="../dont-be-dirty/deep-pore-charcoal-cleanser"><!--<span class="new">New</span>-->Nettoyant en profondeur au charbon pour les pores</a></li>
                                    <li><a href="../dont-be-dirty/self-heating-one-minute-mask">Masque une minute auto-chauffant</a></li>
                                    <li><a href="../dont-be-dirty/pore-unclogging-scrub">Nettoyant granuleux pour les pores</a></li>
                                    <li><a href="../dont-be-dirty/make-up-removing-towelettes">Lingettes démaquillantes quotidiennes</a></li>
                                </ul>
                                 <h2 class="pie murt"><span class="subNavHeadlines">Adieu <span class="redBold">points noirs!<sup>MC</sup></span></span></h2>
                                <ul>
                                    <li><a href="../breakup-with-blackheads/deep-cleansing-pore-strips-ultra">Bandes de nettoyage en profondeur ultra</a></li>
                                    <li><a href="../breakup-with-blackheads/deep-cleansing-pore-strips">Bandes de nettoyage en profondeur</a></li>
                                    <li><a href="../breakup-with-blackheads/deep-cleansing-pore-strips-face">Bandes de nettoyage en profondeur pour les pores du visage</a></li>
                                    <li><a href="../breakup-with-blackheads/charcoal-pore-strips">Bandes de nettoyage en profondeur des pores au charbon </a></li>
                                    <li><a href="../breakup-with-blackheads/deep-cleansing-pore-strips-combo">Bandes de nettoyage en profondeur — emballage assorti</a></li>
                                
                                
                                </ul>
                            </li>
                        </ul>
                        <ul class="col" id="col2">
                            <li>
                                <h2 class="pie complexionClearing"><span class="subNavHeadlines"><span class="orangeBold">Fini</span> l’acné!<sup>MC</sup></span></h2>
                                <ul>
                                    <li><a href="../acnes-outta-here/blemish-fighting-astringent-toner">Astringent anti-acné</a></li>
                                    <li><a href="../acnes-outta-here/blemish-fighting-ice-cleanser">Nettoyant glacé anti-acné</a></li>
                                </ul>
                                
                                 <h2 class="pie takeItOff"><span class="subNavHeadlines">Juste enlevez <span class="purpleBold">tout</span></span></h2>
                                            <ul>
                                                <li class="purpleHover"><a href="../take-it-all-off/baking-soda-cleansing-micellar-water">Eau micellaire nettoyante au bicarbonate de soude</a></li>
                                                <li class="purpleHover"><a href="../take-it-all-off/charcoal-cleansing-micellar-water">Eau micellaire nettoyante au charbon</a></li>
                                                
                                            </ul>
                            </li>
                        </ul>
                    </div>
                    <ul class="col noPadding" id="col3">
                        <li><a href="../biore-facial-cleansing-products/charcoal.aspx">Quoi de neuf?<span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../email-newsletter-sign-up">Inscrivez-Moi<span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../where-to-buy">Ou Acheter<span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../ratings-and-reviews">Commentaires<span class="arrow">&rsaquo;</span></a></li>
                        <!--<li><a href="../email-newsletter-sign-up">Sign Me Up <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../where-to-buy-biore">Where To Buy <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../biore-videos">Watch It Work <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../about-us">About Bior&eacute;<sup>&reg;</sup> Skincare <span class="arrow">&rsaquo;</span></a></li>-->
                        <li><a href="http://www.kaobrands.com/" target="_blank">Kao<span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../contact-us">Pour nous joindre<span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../legal">Avis juridiques <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../privacy">Politique de confidentialité<span class="arrow">&rsaquo;</span></a></li>
                     </ul>
                 </div>
                 <div id="photo"></div>
            </div>
        </div>
    </div>
</asp:Content>