﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml.XPath;

namespace Biore2012.where_to_buy
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // BWR-292 ATAM
            Response.Redirect("~/");

            Site myMaster = (Site)this.Master;
            myMaster.bodyClass += " whereToBuy";

#if DEBUG
            XmlDataSource1.DataFile = "/where-to-buy-biore/wheretobuy.xml";
#endif
        }

        protected void lvRetailer_ItemDataBound(object sender, ListViewItemEventArgs e)
        {

            IXPathNavigable navigableDataItem = (IXPathNavigable)((ListViewDataItem)e.Item).DataItem;
            XPathNavigator theNavigator = navigableDataItem.CreateNavigator();

            String linkText = theNavigator.SelectSingleNode("LinkText").Value;
            String storeLinkHref = theNavigator.SelectSingleNode("StoreLink").Value;
            String onlineLinkHref = theNavigator.SelectSingleNode("OnlineLink").Value;
            String titleText = theNavigator.SelectSingleNode("TitleText").Value;
            String fileName = theNavigator.SelectSingleNode("LinkImage/FileName").Value;
            String target = theNavigator.SelectSingleNode("Target").Value;

            //This way allows both titles and alt text
            HyperLink imglink = (HyperLink)e.Item.FindControl("hlRetailer");
            HyperLink storelocatorlink = (HyperLink)e.Item.FindControl("locatorLink");
            HyperLink onlinelink = (HyperLink)e.Item.FindControl("onlineLink");
            if (storeLinkHref != "")
            {
                storelocatorlink.NavigateUrl = storeLinkHref;
                storelocatorlink.Visible = true;
            }
            if (onlineLinkHref != "")
            {
                onlinelink.NavigateUrl = onlineLinkHref;
                onlinelink.Visible = true;
            }
            imglink.NavigateUrl = HttpContext.Current.Request.Url.ToString() + "#"; 
            imglink.Text = linkText;
            imglink.ToolTip = Server.HtmlDecode(titleText).Replace("&reg;", "®").Replace("&trade;", "™");
            //imglink.Target = target;

#if DEBUG
            imglink.Attributes.Add("style", "background: url(/images/wheretobuy/" + fileName + ") top left no-repeat;");
#else
            imglink.Attributes.Add("style", "background: url(/en-US/images/wheretobuy/" + fileName + ") top left no-repeat;");
#endif


#if DEBUG
            HtmlImage imageBottom = (HtmlImage)e.Item.FindControl("ImageBottom");

            string sImageUrl = imageBottom.Src.Replace("/en-US", "");

            imageBottom.Src = sImageUrl;
#endif
        }
    }
}
