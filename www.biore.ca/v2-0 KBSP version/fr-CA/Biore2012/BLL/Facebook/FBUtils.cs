﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.Script.Serialization;

namespace Facebook
{
    public class FBUtils
	{
		public static string decodeSignedReq()
		{
			string signedReq = "";

			if (!string.IsNullOrEmpty(HttpContext.Current.Request["signed_request"]) || HttpContext.Current.Session["signed_request"] != null)
			{
				string encodedSignedReq = string.IsNullOrEmpty(HttpContext.Current.Request["signed_request"]) ?
					HttpContext.Current.Session["signed_request"].ToString() :
					HttpContext.Current.Request["signed_request"];

				string[] splitEncReq = encodedSignedReq.Split('.');
				string encodedSig = splitEncReq[0];
				string payload = splitEncReq[1];

				// Needs to pad the payload to the proper length for a Base64Url encoded string if is isn't correct already
				int missingChars = 4 - (payload.Length % 4);
				if (missingChars < 4)
				{
					for (int i = 0; i < missingChars; i++)
					{
						payload += "=";
					}
				}

				//Response.Write(payload);
				// Replace encoded characters - and _ with correct characters + and /
				payload.Replace("-", "+");
				payload.Replace("_", "/");

				// Decode the Payload (to UTF8)
				string json = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(payload));

				signedReq = json;
			}
			return signedReq;
		}

		public static bool parseJsonForLike(string decodedJson)
		{
			bool result = false;

			if (!string.IsNullOrEmpty(decodedJson))
			{
				if (decodedJson.IndexOf("liked\":") != -1)
				{
					return decodedJson.IndexOf("liked\":true") != -1;
				}
			}

			return result;
		}

		/// <summary>
		/// Process the jsonData string into a class.
		/// </summary>
		/// <param name="jsonData">The jsonData string.</param>
		/// <returns>The response object.</returns>
		public static T serializeSignedRequestJSON<T>(string jsonData) where T : class
		{
			T result;
			try
			{
				JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
				result = (T)jsSerializer.Deserialize<T>(jsonData);
			}
			catch (Exception ex)
			{
				string error = ex.ToString();
				result = null;
				//throw;
			}
			return result;
		}

		/// <summary>
		/// Check for a stored data (session or cookie)
		/// </summary>
		/// <param name="varName">The session or cookie name (optional via overload).</param>
		/// <returns>String with data.</returns>
		public static string getStoredData(string name)
		{
			string data = "error";

			if (HttpContext.Current.Session[name] != null && HttpContext.Current.Session[name] != "")
			{
				data = HttpContext.Current.Session[name].ToString();
			}
			else
			{
				// Need to add Cookie lookup
			}

			return data;
		}

		public static void setSignedRequestSession(string signedReq, FacebookSignedRequest signedReqObj)
		{
			if (!string.IsNullOrEmpty(HttpContext.Current.Request["signed_request"]))
			{
				HttpContext.Current.Session["signed_request"] = HttpContext.Current.Request["signed_request"];
			}
			HttpContext.Current.Session["signedRequestObj"] = signedReqObj;
			HttpContext.Current.Session["signedRequest"] = signedReq;
		}
    }
}
