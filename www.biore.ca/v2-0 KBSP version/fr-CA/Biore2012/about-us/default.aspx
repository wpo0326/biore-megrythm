﻿<%@ Page Title="About Bior&eacute;&reg; Skincare | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.about_us._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Learn more about Bioré® Skincare" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
                .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <div id="content">
                    <h1>À PROPOS DES SOINS DE LA PEAU BIORÉ<sup>MD</sup></h1>
                    <div id="responseRule"></div>
                    <p>Saviez-vous que KAO Corporation (prononcé « ka-ou »), dont le siège social se trouve à Tokyo, au Japon, est la société mère de la marque Bioré<sup>MD</sup>?</p>

<p>KAO a été fondée à Tokyo en 1887, par Tomiro Nagase, en tant que petite entreprise de soins pour le visage. L’objectif de monsieur Nagase : offrir un savon pour le visage de grande qualité, mais abordable. Il a baptisé son savon « Kao », dont la prononciation est semblable à celle du mot japonais pour « visage » (pas fou!).</p>

<p>Bioré<sup>MD</sup> est maintenant une marque canadienne de soins pour le visage axée sur les pores (obsédée par les pores, pardon!) qui, fidèle à ses origines, fabrique des produits de grande qualité, mais abordables offrant un éventail de bienfaits pour la peau. Même si nous créons notre propre gamme de produits pour le Canada, notre approche dans la conception de nos produits est ancrée dans la philosophie japonaise de l’esthétique. Nous tirons profit des incroyables technologies japonaises de notre société mère qui s’appuient sur des années de recherche et de développement (une véritable mine d’or pour les soins de la peau!).</p>

<p>Nos origines japonaises en esthétique nous poussent à toujours nous améliorer et à innover; l’innovation est dans notre sang. Nos produits sont efficaces, point à la ligne. Notre approche scientifique et pragmatique est axée sur l’efficacité plutôt que sur la force, mais on n’oublie quand même pas d’avoir un peu de plaisir en cours de route! L’objectif de nos produits? Nettoyer en profondeur les 20 000 pores de votre visage pour vous donner la peau dont vous rêvez!</p>

                </div>
            </div>
        </div>
    </div>
</asp:Content>



