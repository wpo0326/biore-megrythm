﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Biore2012.buynow
{
    public partial class PriceSpider : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DoPriceSpider();

        }

        protected void DoPriceSpider()
        {
            string strSku = "";
            string url = Request.Url.OriginalString;
            string strPoreStripsHash = "";
           

            Uri pageURI = new Uri(Request.Url.OriginalString);
            string urlType = pageURI.Segments[1].Replace("/", "");
            string urlResult = pageURI.Segments[2].Replace("/", "");
            string urlResultOrig = urlResult;
            string urlResultTemp = "";

            if (url.IndexOf("/fr-CA/") > -1) //for non-local
            {
                urlType = pageURI.Segments[2].Replace("/", "");
                urlResult = pageURI.Segments[3].Replace("/", "");
                urlResultOrig = urlResult;
            }

            using (StreamReader sr = new StreamReader(Server.MapPath("~/buynow/PriceSpider.json")))
            {

                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Spider> products = serializer.Deserialize<List<Spider>>(sr.ReadToEnd());

                foreach (var product in products)
                {

                    //NOT USED because hash tag URL fragments aren't seen by server, only browser
                    //First, check pore strips special JSON hash toggle to change out lookup based on URL hash
                    //if (product.productName.ToLower() == urlResult && product.productName.ToLower() == "pore-strips")
                    //{
                        //System.Web.UI.WebControls.HiddenField hfPoreStripsHash = null;
                        //HiddenField hf = (HiddenField)this.Page.Master.FindControl("ContentPlaceHolder1").FindControl("litPriceSpiderPoreStripsHash");

                        //while (true)
                        //{
                            //hfPoreStripsHash = (System.Web.UI.WebControls.HiddenField)hf.FindControl("litPriceSpiderPoreStripsHash");
                            //if (hfPoreStripsHash != null)
                            //{
                                //strPoreStripsHash = hfPoreStripsHash.Value.ToString().ToLower();
                                //if (strPoreStripsHash != "")
                                    //urlResult = urlResult + "-" + strPoreStripsHash.ToLower();
                            //}
                            //break;
                        //}
                    //}


                    //First, check for pore strips query string value to switch product lookup
                    if (Request.QueryString["p"] != null && Request.QueryString["p"] != "" && Request.QueryString["p"].ToLower() != "regular")
                    {
                        urlResultTemp = "";
                        strPoreStripsHash = Request.QueryString["p"].ToLower();
                        urlResultTemp = urlResultOrig + "-" + strPoreStripsHash; //append query string to default pore strips product id
                        urlResult = urlResultTemp;
                    }


                    if (product.productType == urlType && product.productName.ToLower() == urlResult)
                    {
                        strSku = product.productSku;
                        break;
                    }

                }

            }

            System.Web.UI.WebControls.Literal litPriceSpiderButtonHldr = null;
            Literal ctl = (Literal)this.Page.Master.FindControl("ContentPlaceHolder1").FindControl("litPriceSpiderButton");      //Parent.Page.FindControl("MainContent_litPriceSpiderButton");

            while (true)
            {
                //Response.Write("we got this far" + ctl);
                litPriceSpiderButtonHldr = (System.Web.UI.WebControls.Literal)ctl.FindControl("litPriceSpiderButton");
                if (litPriceSpiderButtonHldr != null && strSku != "")
                {
                    litPriceSpiderButtonHldr.Text = "<div class=\"ps-widget\" ps-sku=\"" + strSku + "\"></div>";
                    //Response.Write("foo");
                }
                //else { Response.Write("meh"); }

                break;
            } 

        }

    }


    public class Spider
    {
        public string productType;
        public string productName;
        public string productSku;
    }
}