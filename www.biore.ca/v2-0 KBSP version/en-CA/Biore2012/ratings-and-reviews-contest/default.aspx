﻿<%@ Page Title="Ratings and Reviews | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.sitemap._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent" style="min-height:652px;">
            <div class="centeringDiv">
            	<div style="position:absolute; bottom:20px; text-align:center; color:#FFF; font-size:9px; line-height:11px; padding:0 250px;">
                No purchase necessary. Contest closes July 1, 2014 at 11:59:59 p.m. ET. Open to legal residents of Canada (age of majority) excluding Quebec. Enter online at http://www.biore.ca/en-CA/email-newsletter-sign-up. Five prizes available each consisting of a $100 CDN pre-paid credit card. Correct answer to mathematical skill-testing question required. Odds of winning depend on the number of eligible entries received. See full contest rules entry and eligibility requirements at www.__________________.com.
                </div>
            	<a href="../ratings-and-reviews/"><img src="main.jpg" alt="Click here to start" style="margin-top:3px;" /></a></div>
        </div>
    </div>
</asp:Content>
                