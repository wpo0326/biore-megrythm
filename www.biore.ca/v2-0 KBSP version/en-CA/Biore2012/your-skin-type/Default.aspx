﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Biore2012.your_skin_type.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="floodlightPixels" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main" class="yourSkinType">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centerDiv">
                 <h1><img src="../images/your-skin-type/your-skin-type-header.png" alt="Your Skin Type" /></h1>
                
                <div class="infographic">
                    <div class="infographic-left">
                        <img src="../images/your-skin-type/oily-skin.png" alt="Oily Skin" />
                        <div class="textLeft1">
                            <ul>
                                <li>Consistent texture with oily areas throughout the face.</li>                                <li>Pores are overly active throughout the face.</li>
                            </ul>
                        </div>
                         <img class="faces" src="../images/your-skin-type/left-face.png" alt="How Biore Charcoal Products Oily Skin" />
                        <div class="textLeft2">
                            <ul>
                               <li>deep clean</li>                                <li>absorb excess oil</li>                                <li>remove impurities</li>
                            </ul>
                        </div>
                        <div class="packsLeft">
                            <img src="../images/your-skin-type/left-product-packs.png" alt="Biore Deep Pore Charcoal Cleanser and Biore Charcoal Acne Clearing Cleanser" />
                        </div>
                        <div class="swooshLeft">
                            <img src="../images/your-skin-type/black-large-swoosh.png" alt="black swoosh" />
                            <span>2x better clean than a basic cleanser.</span>
                        </div>
                    </div>
                    <div class="infographic-right">
                        <img src="../images/your-skin-type/combination-skin.png" alt="Combination Skin" />
                        <div class="textRight1">
                            <ul>
                                <li>Dryness on cheeks with an oily t-zone (nose, forehead, chin).</li>                                <li>There is an uneven distribution of active pores on the face.</li>
                            </ul>
                        </div> 
                        <img class="faces" src="../images/your-skin-type/right-face.png" alt="How Biore Baking Soda Products Treat Combination Skin" />
                        <div class="textRight2">
                            <ul>
                               <li>smooth skin</li>                                <li>deep clean pores</li>                                <li>balances skin</li>
                            </ul>
                        </div>
                        <div class="packsRight">
                            <img src="../images/your-skin-type/right-product-packs.png" alt="Biore Baking Soda Pore Cleanser and Biore Baking Soda Acne Cleansing Foam" />
                        </div>
                        <div class="swooshRight">
                            <img src="../images/your-skin-type/blue-large-swoosh.png" alt="blue swoosh" />
                             <span>Deep cleans oily areas while gentle on dry areas.</span>
                        </div>
                    </div>
                    

                </div>
                <div style="clear: both"></div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
