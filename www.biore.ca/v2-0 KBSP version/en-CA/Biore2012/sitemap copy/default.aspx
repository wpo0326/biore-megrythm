﻿<%@ Page Title="Sitemap | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.sitemap._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1>Sitemap</h1>
                 <div id="linkList">
                     <div id="prodLinks">
                         <ul class="col" id="col1">
                            <li><a href="../biore-facial-cleansing-products">Our products <span class="arrow">&rsaquo;</span></a></li>
                            <li>
                                <h2 class="pie deepCleansing"><span class="subNavHeadlines">don't be <span class="greenBold">dirty<sup>™</sup></span></span></h2>
                                <ul>
                                    <li><a href="../dont-be-dirty/deep-pore-charcoal-cleanser"><!--<span class="new">New</span>-->Deep Pore Charcoal Cleanser</a></li>
                                    <li><a href="../dont-be-dirty/self-heating-one-minute-mask">Self Heating One Minute Mask</a></li>
                                    <li><a href="../dont-be-dirty/pore-unclogging-scrub">Pore Unclogging Scrub</a></li>
                                    <li><a href="../dont-be-dirty/combination-skin-balancing-cleanser">Combination Skin Balancing Cleanser</a></li>
                                   <!-- <li><a href="../dont-be-dirty/daily-cleansing-cloths">Daily Deep Pore Cleansing Cloths</a></li>-->
                                    <li><a href="../dont-be-dirty/make-up-removing-towelettes">Daily Makeup Removing Towelettes</a></li>
                                    <li><a href="../dont-be-dirty/pore-detoxifying-foam-cleanser">Pore Revitalizing Foam Cleanser</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="col" id="col2">
                            <li>
                                <h2 class="pie complexionClearing"><span class="subNavHeadlines"><span class="orangeBold">acne's</span> outta here!<sup>™</sup></span></h2>
                                <ul>
                                    <li><a href="../acnes-outta-here/blemish-fighting-astringent-toner">Blemish Fighting Astringent</a></li>
                                    <li><a href="../acnes-outta-here/acne-clearing-scrub">Acne Clearing Scrub</a></li>
                                    <li><a href="../acnes-outta-here/blemish-fighting-ice-cleanser">Blemish Fighting Ice Cleanser</a></li>
                                </ul>
                                <h2 class="pie murt"><span class="subNavHeadlines">Breakup With <span class="redBold">Blackheads<sup>™</sup></span></span></h2>
                                <ul>
                                    <li><a href="../breakup-with-blackheads/pore-strips#ultra">Ultra Deep Cleansing Pore Strips</a></li>
                                    <li><a href="../breakup-with-blackheads/pore-strips#regular">Deep Cleansing Pore Strips</a></li>
                                    <li><a href="../breakup-with-blackheads/warming-anti-blackhead-cleanser">Warming Anti-Blackhead Cleanser</a></li>
                                    <li><a href="../breakup-with-blackheads/pore-strips#combo">Deep Cleansing Pore Strips Combo</a></li>
                                </ul>
                                <ul>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <ul class="col noPadding" id="col3">
                        <li>
                            <a href="../biore-facial-cleansing-products/charcoal.aspx">What's New <span class="arrow">&rsaquo;</span></a>
                  
                        </li>
                        <!--<li><a href="../email-newsletter-sign-up">Sign Me Up <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../where-to-buy-biore">Where To Buy <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../biore-videos">Watch It Work <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../about-us">About Bior&eacute;<sup>&reg;</sup> Skincare <span class="arrow">&rsaquo;</span></a></li>-->
                        <li><a href="http://www.kaobrands.com/" target="_blank">Kao Brands Company <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../contact-us">Contact Us <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaobrands.com/legal.asp" target="_blank">Legal <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">Privacy Policy <span class="arrow">&rsaquo;</span></a></li>
                     </ul>
                 </div>
                 <div id="photo"></div>
            </div>
        </div>
    </div>
</asp:Content>