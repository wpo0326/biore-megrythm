﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;

namespace Biore2012.DAL
{
    public class Hybris
    {
        public static string JsonPost(string JSONString)
        {
            string strReturn = "";

            try
            {

                //Development credentials
                //var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://e2125-iflmap.hcisbt.eu1.hana.ondemand.com/http/api/v1/contacts");
                //httpWebRequest.ContentType = "application/json";
                //httpWebRequest.Credentials = new NetworkCredential("P1942379172", "432wvdjvaio;j");
                //new production user/pass as of 12/14/18 user: P2000988722 password: HCI@DeveloperKAO123

                //Production credentials
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://l2125-iflmap.hcisbp.eu1.hana.ondemand.com/http/api/v1/contacts");
                httpWebRequest.ContentType = "application/json";
                //httpWebRequest.Credentials = new NetworkCredential("P1942414435", "2FZoqZof"); //pre wipro credentials deprecated as of Dec. 2018.
                httpWebRequest.Credentials = new NetworkCredential("P2000988722", "HCI@DeveloperKAO123");


                httpWebRequest.Method = "POST";
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;

                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    /*string json = new JavaScriptSerializer().Serialize(new
                    {
                        Username = "P1942379172",
                        Password = "432wvdjvaio;j"
                    });*/
                    streamWriter.Write(JSONString);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    if (result.ToString().IndexOf("SUCCESS") > -1)
                        strReturn = "TRUE";
                }
            }
            catch (Exception e)
            {
                strReturn = "ERROR: " + e.ToString();

            }
            return strReturn;
        }
    }
}