﻿/****************************************************************
Utils JavaScript Document for Biore Alloy Dating Rules Top Ten Rules tab
*****************************************************************/
$(function() {
	ttUtils.init();
});

var ttUtils = {
    init: function() {
        this.fixLog();
        this.setupEvents();
        this.fixGrid();
    },
    fixLog: function() {
        window.log = function() {
            log.history = log.history || []; // store logs to an array for reference
            log.history.push(arguments);
            if (this.console) {
                console.log(Array.prototype.slice.call(arguments));
            }
        };
    },
    setupEvents: function() {
        $(".voteShare").delegate("a", "click", ttUtils.voteShareBnts);
        $("#grid").delegate(".overlayClose", "click", ttUtils.closeOverlay);
        $("#grid").delegate("a", "click", ttUtils.showOverlay);
        $("#showTopTen").click(ttUtils.showTopTen);
        $("#showAll").click(ttUtils.showAll);
    },
    fixGrid: function() {
        var $centerItem = $(".centerBlock").closest(".gridDouble");
        $centerItem.addClass("centerItem");
        $(".gridDouble").eq(4).after($centerItem)
    },
    showTopTen: function(e) {
        var bottomTen = [],
            $blocks = $(".gridItemLink"),
            bLen = $blocks.length,
            btLen = 10,
            i = 0;

        e.preventDefault();

        for (i = 0; i < bLen; i++) {
            bottomTen.push({ "$block": $blocks.eq(i), "vote": $blocks.eq(i).attr("rel") * 1 });
        }
        bottomTen = bottomTen.sort(function(a, b) { return (a.vote < b.vote) ? 1 : ((b.vote < a.vote) ? -1 : 0); });
        bottomTen = bottomTen.slice(10, 20);

        btLen = bottomTen.length;
        for (i = 0; i < btLen; i++) {
            bottomTen[i].$block.siblings(".screenGridItem").fadeIn();
        }
    },
    showAll: function(e) {
        if (typeof e !== "undefined") {
            e.preventDefault();
        }
        $(".screenGridItem").fadeOut();
    },
    voteShareBnts: function(e) {
        var $this = $(this),
            drId = this.href.split("#")[1],
            url;

        e.preventDefault();

        switch ($this.attr("class")) {
            case "luvit":
                ttUtils.addVote(drId, 1);
                if (!ttUtils.voted) {
                    $this.closest(".voteShare").addClass("upVoted");
                }
                break;
            case "shareBtn":
                url = socialSharing.createFBShareLink(drId, $this.closest(".overlay").find(".voteDesc").text(), ttUtils.lastModifiedVote);
                socialSharing.openWindow(url);
                break;
            case "meh":
                ttUtils.addVote(drId, -1);
                if (!ttUtils.voted) {
                    $this.closest(".voteShare").addClass("downVoted");
                }
                break;
            default:
                break;
        }
    },
    addVote: function(drID, modifier) {
        var drData = { "drID": drID, "modifier": modifier };
        if (!ttUtils.voted) {
            $.ajax({
                type: "POST",
                url: "drWSvote.asmx/addVote",
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(drData),
                dataType: "json",
                success: function(response) {
                    // Do stuff after vote is added
                    //log(response.d, drID);
                    if (response.d !== "0") {
                        ttUtils.voteAdded(drID, response.d);
                    }
                }
            });
        }
    },
    voteAdded: function(drID, modifier) {
        ttUtils.voted = true;
        ttUtils.lastModified = drID;
        ttUtils.lastModifiedVote = modifier;
        $(".voted").addClass("afterVote");
    },
    voteAddedAnimation: function(drID, modifier) {
        //log("animate modifier!");
        var $img = $("<img>"),
            $block = $(".gridItemLink[href='#" + drID + "']"),
            voteCountOrg = $block.attr("rel"),
            voteCountUpdated = (voteCountOrg * 1) + (modifier * 1);

        $img.addClass("voteUpdateImg");

        $img.appendTo($block);
        if (modifier > 0) {
            $img.attr("src", "images/votePlusOne.png").addClass("upVoted");
            $img.animate({ top: -10 }, 600, function() { $img.remove(); });
        } else {
            $img.attr("src", "images/voteMinusOne.png").addClass("downVoted");
            $img.animate({ top: 110 }, 600, function() { $img.remove(); });
        }

        // Update link rel value and voted image if necessary
        $block.attr("rel", voteCountUpdated);
        if (voteCountOrg == 0 && voteCountUpdated == -1) {
            $block.find(".voteStatus").attr("src", "images/voteDownBtn.png");
        } else if (voteCountOrg == -1 && voteCountUpdated == 0) {
            $block.find(".voteStatus").attr("src", "images/voteUpBtn.png");
        }
    },
    showOverlay: function(e) {
        var $this = $(this);

        if ($this.closest(".voteShare").length) {
            return false;
        }
        if ($this.hasClass("centerBlock") || $this.hasClass("drPromoText")) {
            return;
        }

        e.preventDefault();
        ttUtils.showAll();
        if (ttUtils.voted) { ttUtils.voted = false; }
        $this.siblings(".overlay").fadeIn();
    },
    closeOverlay: function(e) {
        e.preventDefault();
        $(this).closest(".overlay").fadeOut();
        $(".upVoted").removeClass("upVoted");
        $(".downVoted").removeClass("downVoted");
        $(".afterVote").removeClass("afterVote");
        if (ttUtils.voted) {
            setTimeout(function() { ttUtils.voteAddedAnimation(ttUtils.lastModified, ttUtils.lastModifiedVote); }, 400);
            ttUtils.voted = false;
        }
    }
}
var socialSharing = {
    openWindow: function(url) {
        window.open(url, 'feedDialog', 'toolbar=0,status=0,width=580,height=400');
        return false;
    },
    createFBShareLink: function(drID, caption, modifier) {
        // Reference for the Feed Dialog:
        // http://developers.facebook.com/docs/reference/dialogs/feed/
        var nameLine = "I voted '",
            url;

        if (ttUtils.voted) {
            nameLine += (modifier == 1 ? "Luv It!" : "Meh.") + "' on Dating Rule " + drID;
        } else {
            nameLine = 'Dating Rule ' + drID;
        }
        url = 'http://www.facebook.com/dialog/feed?app_id=' + FBVars.fbAppId +
			'&link=' + FBVars.FBShareUrl +
			'&picture=' + FBVars.baseURL + FBVars.FBSharePicPath + "gridImage_" + drID + ".jpg" +
			'&name=' + encodeURIComponent(nameLine) +
			'&caption=' + encodeURIComponent(caption) +
			'&description=' + encodeURIComponent('Read them all on the Top Ten Dating Rules Facebook page') +
			'&redirect_uri=' + FBVars.baseURL + 'PopupClose.aspx' +
			'&display=popup';

        return url;
    }
}