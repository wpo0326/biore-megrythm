﻿<%@ Page Title="Free Your Pores – Get Clean, Healthy Skin – Browse All Products  | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.our_products.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Bior&eacute;&reg; Skincare products cleanse and gently exfoliate, giving you healthy, clean skin! See the complete Bior&eacute;&reg; Skincare product line." name="description" />
    <meta content="Clear Skin, Biore&reg; Products" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>
    <meta name="viewport" content="width=device-width">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="intro">
                <h1>Our Products</h1>
                <div id="responseRule"></div>
                <p>Target the root of all skin problems &mdash; by cleansing daily and using pore strips weekly. Our powerful, pore-cleansing products come in liquid, scrub, and strip forms to keep your skin clean and healthy-looking.</p>
            </div>

            <div id="takeitalloffProds" class="prodList">
                <h2 class="pie roundedCorners takeitalloff">just take it <span>all off</span>
                    <span></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
 
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-cleansing-micellar-water.png" alt="" />
                        <asp:Literal ID="litBSMicellarWater" runat="server" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:9pt;">New</span> BAKING SODA<br />CLEANSING MICELLAR WATER</h3>
                        <p>Removes makeup, deep cleans pores & refreshes</p>
                        <a href="../take-it-all-off/baking-soda-cleansing-micellar-water" id="details-baking-soda-cleansing-micellar-water">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-cleansing-micellar-water.png" alt="" />
                        <asp:Literal ID="litCharcoalMicellarWater" runat="server" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:9pt;">New</span> CHARCOAL CLEANSING<br />MICELLAR WATER</h3>
                        <p>Removes makeup, deep cleans pores & refreshes</p>
                        <a href="../take-it-all-off/charcoal-cleansing-micellar-water" id="details-charcoal-cleansing-micellar-water">details ></a>
                    </li>
                </ul>
            </div>

            <div id="complexionClearingProds" class="prodList">
                <h2 class="pie roundedCorners complexionClearing">acne's <span>outta here!<sup>&trade;</sup></span>
                    <span></span>
                    <span class="arrow"></span>
                </h2>
                <ul>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-acne-cleansing-foam.png" alt="" />
                        <asp:Literal ID="litBSAcneFoam" runat="server" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:9pt;">New</span> BAKING SODA ACNE CLEANSING FOAM</h3>
                        <p>Gentle foam targets acne and deep cleans pores.</p>
                        <a href="../acnes-outta-here/baking-soda-acne-cleansing-foam" id="details-baking-soda-acne-cleansing-foam">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-acne-scrub.png" alt="" />
                        <asp:Literal ID="litBSAcneScrub" runat="server" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:9pt;">New</span> BAKING SODA <br />ACNE SCRUB</h3>
                        <p>Gently exfoliates to help eliminate breakouts.</p>
                        <a href="../acnes-outta-here/baking-soda-acne-scrub" id="details-baking-soda-acne-scrub">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-acne-clearing-cleanser.png" alt="" />
                        <asp:Literal ID="litCharcoalAcneCleanser" runat="server" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:9pt;">New</span> CHARCOAL ACNE <br />CLEARING CLEANSER</h3>
                        <p>Deep cleans to help prevent breakouts.</p>
                        <a href="../acnes-outta-here/charcoal-acne-clearing-cleanser" id="details-charcoal-acne-clearing-cleanser">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-acne-scrub.png" alt="" />
                        <asp:Literal ID="litCharcoalAcneScrub" runat="server" />
                        <h3><span style="font-family:ProximaNova-Bold, Arial;color:yellow;font-size:9pt;">New</span> CHARCOAL<br />ACNE SCRUB</h3>
                        <p>Absorbs excess oil to help eliminate breakouts.</p>
                        <a href="../acnes-outta-here/charcoal-acne-scrub" id="details-charcoal-acne-scrub">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishFightingIceCleanser.png" alt="" />
                        <asp:Literal ID="litBlemishIceCleanser" runat="server" />
                        <h3>BLEMISH FIGHTING<br />ICE CLEANSER</h3>
                        <p>With Salicylic acid, this formula cleans pores of dirt, oil, and makeup.</p>
                        <a href="../acnes-outta-here/blemish-fighting-ice-cleanser" id="details-blemish-fighting-ice-cleanser">details ></a>
                    </li>
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/acneClearingScrub.png" alt="" />
                        <asp:Literal ID="litAcneClearingScrub" runat="server" />
                        <h3>ACNE CLEARING<br />SCRUB</h3>
                        <p>Defeat deep-down dirt and oil in 2 days.</p>
                        <a href="../acnes-outta-here/acne-clearing-scrub" id="details-acne-clearing-scrub">details ></a>
                    </li>-->
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishTreatingAstringentToner.png" alt="" />
                        <asp:Literal ID="litBlemishAstringent" runat="server" />
                        <h3>BLEMISH FIGHTING ASTRINGENT</h3>
                        <p>Help prevent breakouts with an oil-free toner.</p>
                        <a href="../acnes-outta-here/blemish-fighting-astringent-toner" id="details-blemish-fighting-astringent">details ></a>
                    </li>
                </ul>
            </div>

            <div id="deepCleansingProds" class="prodList">
                <h2 class="pie roundedCorners deepCleansing">don't be <span>dirty<sup>&trade;</sup></span>
                    <span class="arrow"></span>
                </h2>
                <ul>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blue-agave-baking-soda-instant-warming-clay-mask.png" alt="" />
                         <asp:Literal ID="litBABSWarmingClayMask" runat="server" />
                        <h3>Blue Agave + Baking Soda Instant Warming Clay Mask</h3>
                        <p>Heats on contact and purifies pores in just one minute.</p>
                        <a href="../dont-be-dirty/blue-agave-baking-soda-instant-warming-clay-mask">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoal-whipped-mask.png" alt="" />
                         <asp:Literal ID="litCharcoalWhippedMask" runat="server" />
                        <h3>Charcoal Whipped Mask</h3>
                        <p>With Natural Charcoal, known for its ability to draw out impurities & trap them.</p>
                        <a href="../dont-be-dirty/charcoal-whipped-mask">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blue-agave-baking-soda-whipped-mask.png" alt="" />
                         <asp:Literal ID="litBABSWhippedMask" runat="server" />
                        <h3>Blue Agave + Baking Soda Whipped Mask</h3>
                        <p>With Natural Blue Agave, known for its ability to soothe & condition.</p>
                        <a href="../dont-be-dirty/baking-soda-pore-cleanser">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/two-step-charcoal-pore-kit.png" alt="" />
                         <asp:Literal ID="litCharcoalTwoStepKit" runat="server" />
                        <h3>Charcoal 2-Step Pore Kit</h3>
                        <p>Remove dirt, oil & blackheads BETTER with the perfect charcoal duo</p>
                        <a href="../dont-be-dirty/two-step-charcoal-pore-kit">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blue-agave-baking-soda-balancing-pore-cleanser.png" alt="" />
                         <asp:Literal ID="litBABSBalancingPoreCleanser" runat="server" />
                        <h3>Blue Agave + Baking Soda Balancing Pore Cleanser</h3>
                        <p>Deep cleans pores & gently exfoliates.</p>
                        <a href="../dont-be-dirty/blue-agave-baking-soda-balancing-pore-cleanser">details ></a>
                    </li>


		<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-pore-cleanser.png" alt="" />
                         <asp:Literal ID="litBakingSodaPoreCleanser" runat="server" />
                        <h3>Baking Soda<br />Pore Cleanser</h3>
                        <p>Deep cleans pores & gently exfoliates.</p>
                        <a href="../dont-be-dirty/baking-soda-pore-cleanser" id="A1">details ></a>
                    </li>
		<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-cleansing-scrub.png" alt="" />
                         <asp:Literal ID="litBakingSodaCleansingScrub" runat="server" />
                        <h3>Baking Soda<br />Cleansing Scrub</h3>
                        <p>Activates with water for a deep clean and gentle exfoliation.</p>
                        <a href="../dont-be-dirty/baking-soda-cleansing-scrub" id="A1">details ></a>
                    </li>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CHARCOAL_100X235.png" alt="" />
                         <asp:Literal ID="litDeepPoreCharcoalCleanser" runat="server" />
                        <h3>Deep Pore<br />Charcoal Cleanser</h3>
                        <p>Charcoal formula acts as a magnet to draw out dirt and oil for a tingly clean.</p>
                        <a href="../dont-be-dirty/deep-pore-charcoal-cleanser" id="A1">details ></a>
                    </li>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalBar.png" alt="" />
                         <asp:Literal ID="litCharcoalBar" runat="server" />
                        <h3>Pore Penetrating <br />Charcoal Bar</h3>
                        <p>Ideal for face and body.</p>
                        <a href="../dont-be-dirty/charcoal-bar" id="A1">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/SHM_100X235.png" alt="" />
                        <asp:Literal ID="litSelfHeatingOneMinuteMask" runat="server" />
                        <h3>Self Heating<br />One Minute Mask</h3>
                        <p>Heats on contact and purifies pores in just one minute.</p>
                        <a href="../dont-be-dirty/self-heating-one-minute-mask" id="A2">details ></a>
                    </li>
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/4In1DetoxifyingCleanser.png" alt="" />
                        <asp:Literal ID="litPoreFoamCleanser" runat="server" />
                        <h3>Pore Revitalizing Foam Cleanser</h3>
                        <p>Clean, tone, and stimulate with our self-foaming formula.</p>
                        <a href="../dont-be-dirty/pore-detoxifying-foam-cleanser" id="details-pore-detoxifying-foam-cleanser">details ></a>
                    </li>  -->

                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/combinationSkinBalancingCleanser.png" alt="" />
                        <asp:Literal ID="litComboSkinCleanser" runat="server" />
                        <h3>COMBINATION SKIN BALANCING CLEANSER</h3>
                        <p>Balance skin's moisture and address combination skin.</p>
                        <a href="../dont-be-dirty/combination-skin-balancing-cleanser" id="details-combination-skin-balancing-cleanser">details ></a>
                    </li>-->

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/makeupRemovingTowelettes.png" alt="" />
                        <asp:Literal ID="litMakeUpTowelettes" runat="server" />
                        <h3>Daily Makeup Removing Towelettes</h3>
                        <p>Remove stubborn waterproof makeup and clean pores.</p>
                        <a href="../dont-be-dirty/make-up-removing-towelettes" id="details-make-up-removing-towelettes">details ></a>
                    </li>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/poreUncloggingScrub.png" alt="" />
                        <asp:Literal ID="litPoreUncloggingScrub" runat="server" />
                        <h3>PORE UNCLOGGING SCRUB</h3>
                        <p>Smooths &amp; unclogs skin by targeting dirt and oil.</p>
                        <a href="../dont-be-dirty/pore-unclogging-scrub" id="A3">details ></a>
                    </li>
                      
                </ul>
            </div>



            <div id="murtProds" class="prodList">
                <h2 class="pie roundedCorners murt">breakup with <span>blackheads<sup>&trade;</sup></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltra.png" alt="" />
                        <asp:Literal ID="litPoreStripsUltra" runat="server" />
                        <h3>Ultra Deep Cleansing Pore Strips</h3>
                        <p>Defeat blackheads and remove 2x more deep down dirt.*</p>
                        <a href="../breakup-with-blackheads/deep-cleansing-pore-strips-ultra" id="details-deep-cleansing-pore-strips-ultra">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStrips.png" alt="" />
                        <asp:Literal ID="litPoreStrips" runat="server" />
                        <h3>DEEP CLEANSING<br />PORE STRIPS</h3>
                        <p>Unclog pores with magnet-like power. Instantly removes weeks' worth of gunk. </p>
                        <a href="../breakup-with-blackheads/deep-cleansing-pore-strips" id="details-deep-cleansing-pore-strips">details ></a>
                    </li>
                    
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.png" alt="" />
                        <asp:Literal ID="litPoreStripsCombo" runat="server" />
                        <h3>Deep Cleansing Pore<br />Strips Combo</h3>
                        <p>Unclog pores with magnet-like power on your nose, chin, cheeks, and forehead.</p>
                        <a href="../breakup-with-blackheads/deep-cleansing-pore-strips-combo" id="details-deep-cleansing-pore-strips-combo">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsFace.png" alt="" />
                        <asp:Literal ID="litPoreStripsFace" runat="server" />
                        <h3>Deep Cleansing Pore<br />Strips for the Face</h3>
                        <p>Unclog pores on your chin, cheeks, and forehead.</p>
                        <a href="../breakup-with-blackheads/deep-cleansing-pore-strips-face" id="details-deep-cleansing-pore-strips-face">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" />
                        <asp:Literal ID="litCharcoalStrips" runat="server" />
                        <h3 style="padding-bottom:14px;">Deep Cleansing Charcoal<br />Pore Strips</h3>
                        <p>Unclog pores & see 3x less oil with a single use.</p>
                        <a href="../breakup-with-blackheads/charcoal-pore-strips" id="charcoal-pore-strips">details ></a>
                    </li>
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/warmingAntiBlackheadCleanser.png" alt="" />
                        <asp:Literal ID="litBlackheadCleanser" runat="server" />
                        <h3>Warming Anti-Blackhead Cleanser</h3>
                        <p>Destroy dirt & oil with a warming formula of micro-beads. </p>
                        <a href="../breakup-with-blackheads/warming-anti-blackhead-cleanser" id="details-deep-cleansing-blackhead-cleanser">details ></a>
                    </li>-->

                </ul>
            </div>
            <div style="font-size:9px; line-height:10px; margin:20px 0;">*Bioré® Ultra Deep Cleansing Pore Strips remove 2x more deep pore clogs than previous Bioré Ultra Deep Cleansing Pore Strips</div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
