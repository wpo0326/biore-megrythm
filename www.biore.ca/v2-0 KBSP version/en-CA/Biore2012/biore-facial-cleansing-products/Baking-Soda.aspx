﻿<%@ Page Title="Baking Soda Scrub and Baking Soda Cleanser | Bioré® Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Baking-Soda.aspx.cs" Inherits="Biore2012.biore_facial_cleansing_products.Charcoal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta name="description" content="Deep clean pores and gently exfoliate with our Baking Soda Scrub and Baking Soda Cleanser. Try Baking Soda Cleansing Scrub and Baking Soda Pore Cleanser from Bioré® Skincare." />
    <meta content="Charcoal Mask, Charcoal Face Wash, charcoal" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/charcoalLanding.css")
        .Render("~/css/combinednew_#.css")
    %>
    <meta name="viewport" content="width=device-width">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">
<script type="text/javascript" src="http://o2.eyereturn.com/?site=2687&amp;page=biore_charcoal_EN"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main" class="charcoalLanding">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
                <div class="charcoalHeaderSpacer"></div>
                <h1><img src="<%= VirtualPathUtility.ToAbsolute("~/images/charcoalLanding/charcoalHeader.png") %>" border="0" alt="" /></h1>
                
            <div id="charcoalProdWrap">
            	<div id="charProducts"></div>
                <div id="charDescript">
                    <!--<div id="charPaint"></div>-->
                    <h2 class="archer-book" style="color:#FFF;">Help us celebrate by looking good!</h2>
                    <p style="color:#FFF;">To celebrate our 20th birthday, our Bioré&reg; Deep Cleansing Pore Strips got a fresh, new face created by one of the freshest faces in design.<br />
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/charcoalLanding/katie.png") %>" alt="Katie Kavanagh, Canadian Graphic Design student" /></p>
                </div>
                
                <div class="charTry-container">                
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/breakup-with-blackheads/pore-strips#regular") %>"><div class="charTry">LEARN MORE ABOUT PORE STRIPS</div></a>
                </div>
                <div style="clear:both;"></div>
                <br /><br /><br />
            </div>
            
            
            <!--<div class="charList archer-bold">
                <span class="charGreeen">Benefits of Charcoal Bar</span><br />

                <span class="charbullet"></span>Gently exfoliates  & cleans deep down to remove oil and dirt, leaving pores purified & tingly clean<br />
            </div>-->

            <!--<div class="charList archer-bold">
                <span class="charGreeen">Benefits of Charcoal Pore Strips</span><br />

                <span class="charbullet"></span>3X less oil with a single use<br />
            </div>-->

            <!--<div id="charTryNowWrap">
                <div id="charTryProdOne">
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/dont-be-dirty/charcoal-bar") %>"><div class="charTry">Try it Now</div></a>
                </div>
                <div id="charTryProdTwo">
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/breakup-with-blackheads/charcoal-pore-strips#regular") %>"><div class="charTry">Try it Now</div></a>
                </div>
            </div>-->
           
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
