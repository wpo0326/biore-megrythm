﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Charcoal.aspx.cs" Inherits="Biore2012.biore_facial_cleansing_products.Charcoal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Purify pores with our Charcoal Mask and Charcoal Face Wash. Try Deep Pore Charcoal Cleanser and Self Heating One Minute Mask from Bioré® Skincare." />
    <meta content="Charcoal Mask, Charcoal Face Wash, charcoal" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/charcoalLanding.css")
        .Render("~/css/combinednew_#.css")
    %>
    <meta name="viewport" content="width=device-width">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">
<script type="text/javascript" src="http://o2.eyereturn.com/?site=2687&amp;page=biore_charcoal_EN"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main" class="charcoalLanding">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
                <div class="charcoalHeaderSpacer"></div>
                <h1><img src="<%= VirtualPathUtility.ToAbsolute("~/images/charcoalLanding/charcoalHeader.png") %>" border="0" alt="" /></h1>
                
            <div id="charcoalProdWrap">
                <div id="charDescript">
                    <!--<div id="charPaint"></div>-->
                    <h2 class="archer-book">Really? <span class="charGrey archer-bold">Charcoal?</span></h2>
                    <p>Yes, charcoal! Like a magnet, charcoal draws out and traps skin impurities for  a deep clean. Bioré<sup>®</sup> Skincare brings you this revolutionary ingredient in 2 new products:  
                        <br /><br /><b>NEW <span class="charGreeen">Deep Cleansing Charcoal Pore Strips </span></b> and our <b><span class="charGreeen">Pore Penetrating Charcoal Bar.</span></b><br /></p>
                </div>
                <div id="charProducts"></div>
                <div style="width:285px; float:right; text-align:right; margin-top:0px;">                
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/breakup-with-blackheads/charcoal-pore-strips#regular") %>"><div class="charTry" style="margin-top:0px; width:130px;">Try it Now</div></a>
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/dont-be-dirty/charcoal-bar") %>"><div class="charTry" style="margin-top:0px; width:120px;">Try it Now</div></a>
                </div>
                <div style="clear:both;"></div>
                <br /><br /><br />
            </div>
            
            
            <!--<div class="charList archer-bold">
                <span class="charGreeen">Benefits of Charcoal Bar</span><br />

                <span class="charbullet"></span>Gently exfoliates  & cleans deep down to remove oil and dirt, leaving pores purified & tingly clean<br />
            </div>-->

            <!--<div class="charList archer-bold">
                <span class="charGreeen">Benefits of Charcoal Pore Strips</span><br />

                <span class="charbullet"></span>3X less oil with a single use<br />
            </div>-->

            <!--<div id="charTryNowWrap">
                <div id="charTryProdOne">
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/dont-be-dirty/charcoal-bar") %>"><div class="charTry">Try it Now</div></a>
                </div>
                <div id="charTryProdTwo">
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/breakup-with-blackheads/charcoal-pore-strips#regular") %>"><div class="charTry">Try it Now</div></a>
                </div>
            </div>-->
           
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
