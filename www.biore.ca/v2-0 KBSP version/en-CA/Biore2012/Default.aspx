﻿<%@ Page Title="Get a Clean Face | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Get healthy, radiant skin with products from Bioré® Skincare" name="description" />
    <meta content="skincare, clean face" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/UniversPro67.css")
        .Add("~/css/homepage.css")
        .Add("~/css/flexslider.css")
        .Render("~/CSS/combinedhome_#.css")
    %>
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" type="text/css" href="/en-CA/CSS/homepage.css">
    <script type="text/javascript">
        $(document).ready(function () {
            //$('.scroll-dots a').click(function (event) {
            //    event.preventDefault();
            //    var target = $(this).attr('id');
            //    console.log(target);
            //});
        });
    </script>

    <script type="text/javascript">
        $(function() {
             /*$('#theaterItem1 a').click(
                 function (e) {
                     e.preventDefault();

                    
                 });*/

            var windowSize = $(window).width();
            var mobileSize = 768;

            $('a[href*=#]:not([href=#]):not([href^="/en"])').click(function(event) {
                event.preventDefault();
                console.log('Alvaro');
                if (windowSize > mobileSize) {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if (target.length) {
                            $('html,body').animate({
                                scrollTop: target.offset().top
                            }, 1000);
                            return false;
                        }
                    }
                }
            });
         });
    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--
The banners will be placed in the following order on the home page:
-	Masks
-	Micellar (use existing banner)
-	Pore Strips
-	Acne
        -->
    <div id="main1">
        <div class="home-content">

            <section class="home-section" id="section-01">
                <div class="bg-left"></div>
                <div class="bg-right"></div>
                <div class="container3">
                    <div class="section1-title">
                        <img src="images/homepage/whats-new-mask.png" alt="What's New Mask" />
                    </div>
                    <div class="container">


                        <div class="headline">
                            <img src="images/homepage/whipped-products-left.png" alt="Whipped Products" />
                        </div>
                        <div class="product">
                            <img src="images/homepage/whipped-products-right-no-splotch.png" alt="Whipped Products" />
                        </div>


                    </div>
                </div>
                <div class="dontbedirty">
                    <img src="images/homepage/dont-be-dirty-text.png" alt="Don't Be Dirty" />
                </div>
                <div class="learnmore">
                    <a href="/en-CA/biore-facial-cleansing-products/#deepCleansingProds">LEARN MORE</a>
                </div>

                <!--<p class="scrollDown">SCROLL DOWN</p>-->
                <div id="arrowBounce">
                    <a href="/en-CA/biore-facial-cleansing-products/#deepCleansingProds">
                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/arrowDown.png") %>" class="section-down-arrow" /></a>
                </div>
                <div class="scroll-dots">
                     <a href="#section-01"><span class="scroll-dot enable"></span></a>
                    <a href="#section-micellar-water"><span class="scroll-dot"></span></a>
                    <a href="#section-02"><span class="scroll-dot"></span></a>
                    <a href="#section-03"><span class="scroll-dot"></span></a>
                </div>
            </section>
            <section class="home-section" id="section-micellar-water">
                <div class="bg-left" style="background-position: center left;"></div>
                <div class="bg-right"></div>
                <div class="container3">
                    <div style="position: absolute; top: 25px; right: 25px; width: 100%;">
                        <div style="width: 15%; float: right;">
                            <img src="images/homepage/new.png" style="width: 100%; max-width: 175px;" />
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                    <div class="container2">
                        <div class="container1">
                            <div class="headline">
                                <img src="images/homepage/micellar-removes-makeup.png" alt="Saving Face" />
                            </div>
                            <div class="animation">
                                <div class="animations-container">
                                    <img src="images/homepage/micellar-water-packs.png" alt="Micellar Water Products" />
                                </div>
                            </div>
                            <div class="product">
                                <img src="images/homepage/micellar-water-all-in-one.png" alt="All-in-one Micellar Water - No Need to Rinse!" width="90%" />
                            </div>

                        </div>
                    </div>
                </div>
                <div class="learnmore">
                    <a href="/en-CA/biore-facial-cleansing-products/">LEARN MORE</a>
                </div>

                <!--<p class="scrollDown">SCROLL DOWN</p>-->
                <div id="arrowBounce">
                    <a href="/en-CA/biore-facial-cleansing-products/">
                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/arrowDown.png") %>" class="section-down-arrow" /></a>
                </div>
                <div class="scroll-dots">
                    <a href="#section-01"><span class="scroll-dot"></span></a>
                    <a href="#section-micellar-water"><span class="scroll-dot enable"></span></a>
                    <a href="#section-02"><span class="scroll-dot"></span></a>
                    <a href="#section-03"><span class="scroll-dot"></span></a>
                </div>
            </section>
            <section class="home-section" id="section-02">
                <div class="bg-left"></div>
                <div class="bg-right"></div>
                <div class="container3">
                    <div class="porestrip-container">
                        <div class="section2-title">
                            <img src="images/homepage/the-pore-strip-collection.png" alt="The Pore Stip Collection" />
                        </div>
                        <div class="container">
                            <div class="headline">
                                <img src="images/homepage/pore-strip-products.png" alt="Porestrip Products" />
                            </div>
                        </div>
                        <div class="breakupwithbh">
                            <img src="images/homepage/breakup-with-blackheads-text.png" alt="Breakup with Blackheads" />
                        </div>
                        <div class="learnmore">
                            <a href="/en-CA/biore-facial-cleansing-products/#murtProds">LEARN MORE</a>
                        </div>

                        <!--<p class="scrollDown">SCROLL DOWN</p>-->
                        <div id="arrowBounce">
                            <a href="/en-CA/biore-facial-cleansing-products/#murtProds">
                                <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/arrowDown.png") %>" class="section-down-arrow" /></a>
                        </div>
                    </div>
                    <div class="model-container">
                        <img src="images/homepage/madelained-porestrip.png" alt="Madelaine Porestrips" />
                    </div>

                </div>

                <div class="scroll-dots">
                    <a href="#section-01"><span class="scroll-dot"></span></a>
                    <a href="#section-micellar-water"><span class="scroll-dot"></span></a>
                    <a href="#section-02"><span class="scroll-dot enable"></span></a>
                    <a href="#section-03"><span class="scroll-dot"></span></a>
                </div>
            </section>
            <section class="home-section" id="section-03">
                <div class="bg-left"></div>
                <div class="bg-right"></div>
                <div class="container3">
                    <div class="porestrip-container">
                        <div class="section3-title">
                            <img src="images/homepage/the-acne-collection.png" alt="The Acne Collection" />
                        </div>
                        <div class="container">
                            <div class="headline">
                                <img src="images/homepage/acne-collection.png" alt="Acne Collection Products" />
                            </div>
                        </div>
                        <div class="acnesouttahere">
                            <img src="images/homepage/acnes-outta-here-text.png" alt="Acne's Outta Here" />
                        </div>
                        <div class="learnmore">
                            <a href="/en-CA/biore-facial-cleansing-products/#complexionClearingProds">LEARN MORE</a>
                        </div>

                        <!--<p class="scrollDown">SCROLL DOWN</p>-->
                        <div id="arrowBounce">
                            <a href="/en-CA/biore-facial-cleansing-products/#complexionClearingProds">
                                <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/arrowDown.png") %>" class="section-down-arrow" /></a>
                        </div>
                    </div>
                    <div class="model-container">
                        <img src="images/homepage/madelaine.png" alt="Madelaine" />
                    </div>

                </div>

                <div class="scroll-dots">
                    <a href="#section-01"><span class="scroll-dot"></span></a>
                    <a href="#section-micellar-water"><span class="scroll-dot"></span></a>
                    <a href="#section-02"><span class="scroll-dot"></span></a>
                    <a href="#section-03"><span class="scroll-dot enable"></span></a>
                </div>
            </section>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/jquery.flexslider.min.js")
        .Render("~/js/combinedhome_#.js")
    %>

    <!--Begin Mediavest DMP-->
    <script type="text/javascript" src="/en-US/js/kao-VivaKiDIL_6.4.js"></script>
    <!--End Mediavest DMP-->
</asp:Content>
