﻿<%@ Page Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="NewsletterForm.aspx.cs"
    Inherits="Biore2012.FormConfig.NewsletterForm" %>
<%@ Register TagPrefix="skm" Namespace="skmValidators" Assembly="skmValidators" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%#SquishIt.Framework.Bundle .Css()
        .Add("~/css/formConfig.css")
        .Render("~/css/combinedform_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow">
            </div>
            <div class="centeringDiv">
                <div class="responsive contact">
                    <div id="contactFormWrap">
                        <div id="BrandImageContainer" class="png-fix">
                            <img class="png-fix" src="images/forms/signUpPhoto.jpg" style="border-width: 0px;" />
                        </div>
                        <div id="ContactFormContainer">
                            <asp:Panel ID="ContactDetails" runat="server">
                                <!-- Header -->
                                <div id="formHeader">
                                    <h1 id="ctl00_ContentPlaceHolder1_ucForm_PageHeader">
                                        Join the Movement for Great Skin 24/7!
                                    </h1>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="OptinForm" runat="server">
                                <!-- Description -->
                                <div class="DescriptionContainer png-fix">
                                    <h2>
                                        Thanks for your interest in Bioré® Skincare!</h2>
                                    Please enter the information below to hear about all our latest promotions, contests,
                                    and news.
                                </div>
                                <!-- From Fields -->
                                <div class="FormContainer">
                                    <p class="req png-fix">
                                        <em>Required*</em></p>
                                    <div class="MemberInfoContainer png-fix">
                                        <div class="NameWrapper">
                                            <div class="FirstNameContainer Question">
                                                <asp:Label ID="FNameLbl" runat="server" Text="First Name*" AssociatedControlID="FName"></asp:Label>
                                                <asp:TextBox ID="FName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="FNameValidator1" runat="server" ErrorMessage="Please enter your First Name."
                                                        ControlToValidate="FName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="FNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your First Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="FName" EnableClientScript="true"
                                                        SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="LastNameContainer Question">
                                                <asp:Label ID="LNameLbl" runat="server" Text="Last Name*" AssociatedControlID="LName"></asp:Label>
                                                <asp:TextBox ID="LName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="LNameValidator1" runat="server" ErrorMessage="Please enter your Last Name."
                                                        ControlToValidate="LName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="LNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Last Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="LName" SetFocusOnError="true"
                                                        Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="EmailWrapper">
                                            <div class="EmailContainer Question">
                                                <asp:Label ID="EmailLbl" runat="server" Text="Email*" AssociatedControlID="Email"></asp:Label>
                                                <asp:TextBox ID="Email" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="EmailValidator1" runat="server" ErrorMessage="Please enter your Email Address."
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="EmailValidator2" runat="server" ErrorMessage="Please enter a valid Email Address."
                                                        ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div class="PhoneContainer Question">
                                            <asp:Label ID="MobilePhoneLbl" runat="server" Text="Mobile" AssociatedControlID="MobilePhone"></asp:Label>
                                            <asp:TextBox ID="MobilePhone" MaxLength="50" runat="server" />
                                            <div class="ErrorContainer">
                                                <asp:RegularExpressionValidator ID="v_MobilePhone" runat="server" ErrorMessage="Your Mobile Phone is not required, but please do not use special characters in the Mobile Phone field."
                                                    ValidationExpression="^[^<>]+$" ControlToValidate="MobilePhone" EnableClientScript="true"
                                                    SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                <%-- The following RegEx Validators is for US type numbers.  IMPORTANT: 
								 Please test against your user base before using as you may need to modify it for other formats!  --%>
                                                <%--<asp:RegularExpressionValidator ID="PhoneValidator2" runat="server"
								ErrorMessage="<br/>Your Home Phone is not required, but please enter only valid characters."
								ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"
								ControlToValidate="Phone" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg phone"></asp:RegularExpressionValidator>--%>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelAddress" class="AddressContainer">
                                            <div class="AddressWrapper">
                                                <div class="Address1Container Question">
                                                    <asp:Label ID="Address1Lbl" runat="server" Text="Address 1*" AssociatedControlID="Address1"></asp:Label>
                                                    <asp:TextBox ID="Address1" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="Address1Validator1" runat="server" ErrorMessage="Please enter your Address."
                                                            ControlToValidate="Address1" EnableClientScript="true" SetFocusOnError="true"
                                                            Display="Dynamic" CssClass="errormsg" />
                                                        <asp:RegularExpressionValidator ID="Address1Validator2" runat="server" ErrorMessage="Please do not use special characters in the Address 1 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address1" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="Address2Container Question">
                                                    <asp:Label ID="Address2Lbl" runat="server" Text="Address 2" AssociatedControlID="Address2"></asp:Label>
                                                    <asp:TextBox ID="Address2" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="Address2Validator1" runat="server" ErrorMessage="Please do not use special characters in the Address 2 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address2" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="CityContainer Question">
                                                    <asp:Label ID="CityLbl" runat="server" Text="City*" AssociatedControlID="City"></asp:Label>
                                                    <asp:TextBox ID="City" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="CityValidator1" runat="server" ErrorMessage="Please enter your City."
                                                            ControlToValidate="City" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                            CssClass="errormsg" />
                                                        <asp:RegularExpressionValidator ID="CityValidator2" runat="server" ErrorMessage="Please do not use special characters in the City field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="City" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="StateContainer Question">
                                                <asp:Label ID="StateLbl" runat="server" Text="Province*" AssociatedControlID="State"></asp:Label>
                                                <asp:DropDownList ID="State" runat="server">
                                                    <asp:ListItem Value="" Selected="true">Select Province</asp:ListItem>
                                                    <asp:ListItem Value="1">Alberta</asp:ListItem>
                                                    <asp:ListItem Value="2">British Columbia</asp:ListItem>
                                                    <asp:ListItem Value="3">Manitoba</asp:ListItem>
                                                    <asp:ListItem Value="4">New Brunswick</asp:ListItem>
                                                    <asp:ListItem Value="5">Newfoundland</asp:ListItem>
                                                    <asp:ListItem Value="6">Northwest Territories</asp:ListItem>
                                                    <asp:ListItem Value="7">Nova Scotia</asp:ListItem>
                                                    <asp:ListItem Value="8">Nunavut</asp:ListItem>
                                                    <asp:ListItem Value="9">Ontario</asp:ListItem>
                                                    <asp:ListItem Value="10">Prince Edward Island</asp:ListItem>
                                                    <asp:ListItem Value="11">Quebec</asp:ListItem>
                                                    <asp:ListItem Value="12">Saskatchewan</asp:ListItem>
                                                    <asp:ListItem Value="13">Yukon</asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="StateValidator" runat="server" ErrorMessage="Please select your Province."
                                                        ControlToValidate="State" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div class="PostalCodeContainer Question">
                                            <asp:Label ID="PostalCodeLbl" runat="server" Text="Postal Code*" AssociatedControlID="PostalCode"></asp:Label>
                                            <asp:TextBox ID="PostalCode" runat="server" MaxLength="20" CssClass="inputTextBox"></asp:TextBox>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="PostalCodeValidator1" runat="server" ErrorMessage="Please enter your Postal Code"
                                                    ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true"
                                                    Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                <%-- The following RegEx Validator is for Canadian Postal codes.  IMPORTANT: 
							         Please test against your user base before using as you may need to modify it for other formats!
							         
							         To validate that Canadian postal code has a space, use this: "[ABCEGHJKLMNPRSTVXY]\d[A-Z] \d[A-Z]\d"
							         To validate that Candian postal code without checking for space, use this: "^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$"  
							         
							                    --%>
						                        <asp:RegularExpressionValidator ID="PostalCodeValidator2" runat="server" Display="Dynamic"
							        ErrorMessage="<br />Please enter a valid 6-digit Canadian Postal Code." ValidationExpression="^[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy]{1}\d{1}[A-Za-z]{1} *\d{1}[A-Za-z]{1}\d{1}$"
							        ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true"
							        CssClass="errormsg" ForeColour="#aa0000"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                        <div class="CountryContainer Question">
                                            <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_CountryLbl">Country
                                            </span><span class="fake_input">Canada</span>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelGender" class="GenderContainer Question">
                                            <asp:Label ID="GenderLbl" runat="server" Text="Gender*" AssociatedControlID="Gender"></asp:Label>
                                            <asp:DropDownList ID="Gender" runat="server">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="F">Female</asp:ListItem>
                                                <asp:ListItem Value="M">Male</asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="GenderValidator" runat="server" ErrorMessage="Please enter your Gender."
                                                    ControlToValidate="Gender" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="BirthdayContainer Question">
                                            <asp:Label ID="DOBLbl" runat="server" Text="Birthdate*" AssociatedControlID="yyyy"></asp:Label>
                                            <asp:DropDownList ID="yyyy" runat="server" CssClass="inputSelectBDay year">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="mm" runat="server" CssClass="inputSelectBDay month">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="dd" runat="server" CssClass="inputSelectBDay day">
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:CustomValidator ID="DOBValidator" runat="server" ErrorMessage="Please enter a valid date."
                                                    OnServerValidate="DOBValidate" EnableClientScript="false" Display="Dynamic" CssClass="errormsg validdate"></asp:CustomValidator>
                                                <asp:RequiredFieldValidator ID="yyyyValidator" runat="server" ErrorMessage="Please enter your Birth Year."
                                                    ControlToValidate="yyyy" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg year"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="mmValidator" runat="server" ErrorMessage="Please enter your Birth Month."
                                                    ControlToValidate="mm" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg month"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="ddValidator" runat="server" ErrorMessage="Please enter your Birth Day."
                                                    ControlToValidate="dd" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg day"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="seperator png-fix">
                                        </div>
                                    </div>
                                              
                                    <div id="ctl00_ContentPlaceHolder1_ucForm_QuestionPanel" class="QuestionContainer">
                        			            
                                        <div class="MarketingQuestionContainer">
				                            <div class="CustomQuestiondropdown Question">
					                            <label for="Biore_mkt_q1" id="Biore_mkt_q1LBL" class="dropdownLabel">How many skin care products do you use on a daily basis?*</label>
					                              <asp:DropDownList ID="Biore_mkt_q1" runat="server" CssClass="dropDownAnswer">
					                                <asp:ListItem value="">Select...</asp:ListItem>
						                            <asp:ListItem value="1">1</asp:ListItem>
						                            <asp:ListItem value="2">2</asp:ListItem>
						                            <asp:ListItem value="3">3</asp:ListItem>
						                            <asp:ListItem value="4">4</asp:ListItem>
						                            <asp:ListItem value="5 or More">5 or More</asp:ListItem>
					                              </asp:DropDownList>
					                              <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="v_q1" runat="server" ControlToValidate="Biore_mkt_q1" ErrorMessage="Please answer all required questions." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
				                            </div>
				                            <div class="CustomQuestiondropdown Question">
					                            <label for="Biore_mkt_q7" id="Biore_mkt_q1LBL" class="dropdownLabel">I pay more attention/spend more time on my skin than other people.*</label>
					                            
					                                <asp:DropDownList ID="Biore_mkt_q7" runat="server" CssClass="dropDownAnswer">
					                                   <asp:ListItem value="">Select...</asp:ListItem>
						                                <asp:ListItem value="Agree strongly">Agree strongly</asp:ListItem>
						                                <asp:ListItem value="Agree somewhat">Agree somewhat</asp:ListItem>
						                                <asp:ListItem value="Neither agree nor disagree">Neither agree nor disagree</asp:ListItem>
						                                <asp:ListItem value="Disagree somewhat">Disagree somewhat</asp:ListItem>
						                                <asp:ListItem value="Disagree strongly">Disagree strongly</asp:ListItem>
					                                </asp:DropDownList>
					                                <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="v_Biore_mkt_q7" runat="server" ControlToValidate="Biore_mkt_q7" ErrorMessage="Please answer all required questions." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                                </div>

				                            </div>
				                            <div class="CustomQuestiondropdown Question">
					                            <label for="Biore_mkt_q8" id="Biore_mkt_q8LBL" class="dropdownLabel">I am willing to pay more for skin care products I really want.*</label>
					                            <asp:DropDownList ID="Biore_mkt_q8" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem value="">Select...</asp:ListItem>
						                            <asp:ListItem value="Agree strongly">Agree strongly</asp:ListItem>
						                            <asp:ListItem value="Agree somewhat">Agree somewhat</asp:ListItem>
						                            <asp:ListItem value="Neither agree nor disagree">Neither agree nor disagree</asp:ListItem>

						                            <asp:ListItem value="Disagree somewhat">Disagree somewhat</asp:ListItem>
						                            <asp:ListItem value="Disagree strongly">Disagree strongly</asp:ListItem>

					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
						                            <asp:RequiredFieldValidator ID="v_Biore_mkt_q8" runat="server" ControlToValidate="Biore_mkt_q8" ErrorMessage="Please answer all required questions." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
					                            </div>
				                            </div>
				                            <div class="CustomQuestiondropdown Question">
					                            <label for="Biore_mkt_q2" id="Biore_mkt_q2LBL" class="dropdownLabel">How likely are you to recommend your favorite skin care products to friends?*</label>
					                            <asp:DropDownList ID="Biore_mkt_q2" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem value="">Select...</asp:ListItem>
						                            <asp:ListItem value="Very likely">Very likely</asp:ListItem>
						                            <asp:ListItem value="Somewhat likely">Somewhat likely</asp:ListItem>
						                            <asp:ListItem value="Somewhat unlikely">Somewhat unlikely</asp:ListItem>
						                            <asp:ListItem value="Not at all likely">Not at all likely</asp:ListItem>

					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
						                            <asp:RequiredFieldValidator ID="v_Biore_mkt_q2" runat="server" ControlToValidate="Biore_mkt_q2" ErrorMessage="Please answer all required questions." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
					                            </div>
				                            </div>
				                            <div class="CustomQuestioncheckbox Question trait413 cbVert noneToggle1">
				                                <label for="Biore_mkt_q3" id="Biore_mkt_q3LBL" class="checkboxLabel">Which Bior&eacute;<sup>&reg;</sup> products do you currently use? (select all that apply)*</label>
				                                <asp:CheckBoxList ID="Biore_mkt_q3" RepeatDirection="Vertical" runat="server" RepeatLayout="Flow" CssClass="checkboxAnswer">
							                        <asp:ListItem Value="Bioré® Combination Skin Balancing Cleanser">NEW Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Combination Skin Balancing Cleanser</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Steam Activated Cleanser">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Steam Activated Cleanser</asp:ListItem>
							                        <asp:ListItem Value="Bioré® 4-in-1 Revitalizing Cleanser">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; 4-in-1 Revitalizing Cleanser</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Pore Unclogging Scrub">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Pore Unclogging Scrub</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Daily Purifying Scrub">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Daily Purifying Scrub</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Deep Cleansing Pore Strips">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Deep Cleansing Pore Strips</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Deep Cleansing Pore Strips Combo">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Deep Cleansing Pore Strips Combo</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Deep Cleansing Pore Strips Ultra">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Deep Cleansing Pore Strips Ultra</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Blemish Fighting Ice Cleanser">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Blemish Fighting Ice Cleanser</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Blemish Fighting Astringent">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Blemish Fighting Astringent</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Warming Anti-Blackhead Cream">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Warming Anti-Blackhead Cream</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Make-Up Removing Towelettes">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Make-Up Removing Towelettes</asp:ListItem>
							                        <asp:ListItem Value="None of the above">None of the above</asp:ListItem>
							                    </asp:CheckBoxList>
							                    
				         <span class="cbinput"></span>
				         
					                            <div class="ErrorContainer">
						                            <skm:CheckBoxListValidator ID="HairAppliancesLblVal" ControlToValidate="Biore_mkt_q3"
                                                    runat="server" Display="Dynamic" CssClass="errormsg" ErrorMessage="Which of the following hair appliances do you use?" />
					                            </div>
				                            </div>
				                            <div class="CustomQuestiondropdown Question">
					                            <label for="Biore_mkt_q4" id="Biore_mkt_q4LBL" class="dropdownLabel">What brand of face care products do you use most often? (select one)*</label>
					                            
					                            <asp:DropDownList ID="Biore_mkt_q4" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem value="">Select...</asp:ListItem>
						                            <asp:ListItem value="Aveeno">Aveeno&#174;</asp:ListItem>
						                            <asp:ListItem value="Avon®">Avon&#174;</asp:ListItem>

						                            <asp:ListItem value="Biore">Bior&#233;&#174; products</asp:ListItem>
						                            <asp:ListItem value="Cetaphil">Cetaphil&#174;</asp:ListItem>
						                            <asp:ListItem value="Clean and Clear">Clean &amp; Clear&#174;</asp:ListItem>
						                            <asp:ListItem value="Clearasil">Clearasil&#174;</asp:ListItem>
						                            <asp:ListItem value="Clinique®">Clinique&#174;</asp:ListItem>

						                            <asp:ListItem value="Dove">Dove&#174;</asp:ListItem>
						                            <asp:ListItem value="Garnier Nutritioniste">Garnier Nutritioniste</asp:ListItem>
						                            <asp:ListItem value="L'Oreal">L' Oreal&#174;</asp:ListItem>
						                            <asp:ListItem value="Mary Kay">Mary Kay&#174;</asp:ListItem>
						                            <asp:ListItem value="Neutrogena">Neutrogena&#174;</asp:ListItem>
						                            <asp:ListItem value="Noxzema">Noxzema&#174;</asp:ListItem>
                                                    <asp:ListItem value="Olay">Olay&#174;</asp:ListItem>
						                            <asp:ListItem value="ProActiv Solutions">ProActiv&#174; Solutions</asp:ListItem>
						                            <asp:ListItem value="St. Ives">St. Ives&#174;</asp:ListItem>
						                            <asp:ListItem value="Store Brand">Store Brand</asp:ListItem>
						                            <asp:ListItem value="Other Department Store brand">Other Department Store brand</asp:ListItem>

						                            <asp:ListItem value="Other">Other</asp:ListItem>
						                            <asp:ListItem value="None of these">None of these</asp:ListItem>
					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
						                             <asp:RequiredFieldValidator ID="v_Biore_mkt_q4" runat="server" ControlToValidate="Biore_mkt_q4" ErrorMessage="Please answer all required questions." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
					                            </div>
				                            </div>
				                            <div class="CustomQuestioncheckbox Question trait419 cbVert noneToggle2">
					                            <label for="Biore_mkt_q9" id="Biore_mkt_q9LBL" class="checkboxLabel">Which of the following types of products do you typically use? (select all that apply)*</label>
					                           <asp:CheckBoxList ID="Biore_mkt_q9" RepeatDirection="Vertical" runat="server" RepeatLayout="Flow" CssClass="checkboxAnswer">
							                        <asp:ListItem>Liquid/Cream/Gel cleanser</asp:ListItem>
							                        <asp:ListItem>Cleansing Cloths</asp:ListItem>
							                        <asp:ListItem>Facial moisturizing lotion/cream</asp:ListItem>
							                        <asp:ListItem>Facial moisturizing lotion/cream with SPF</asp:ListItem>
							                        <asp:ListItem>Facial scrub/exfoliator</asp:ListItem>
							                        <asp:ListItem>Toner / Astringent</asp:ListItem>
							                        <asp:ListItem>Acne/Blemish spot treatment</asp:ListItem>
							                        <asp:ListItem>Skin discoloration treatment product (sun damage)</asp:ListItem>
							                        <asp:ListItem>Eye gel/cream/treatment</asp:ListItem>
							                        <asp:ListItem>Night cream/ overnight moisturizer</asp:ListItem>
							                        <asp:ListItem>Make-up remover</asp:ListItem>
							                        <asp:ListItem>None of the above</asp:ListItem>
							                    </asp:CheckBoxList>
							                    
				         <span class="cbinput"></span>
					                            <div class="ErrorContainer">
						                            <skm:CheckBoxListValidator ID="v_Biore_mkt_q9" ControlToValidate="Biore_mkt_q9"
                                                    runat="server" Display="Dynamic" CssClass="errormsg" ErrorMessage="Please answer all required questions." />
					                            </div>
					                           
				                            </div>
				                            <div class="CustomQuestioncheckbox Question trait415 cbVert noneToggle3">
					                            <label for="Biore_mkt_q5" id="Biore_mkt_q5LBL" class="checkboxLabel">Which, if any, of the following attributes would you be interested in in a face care product? (select all that apply)*</label>
					                            
					                            <asp:CheckBoxList ID="Biore_mkt_q5" RepeatDirection="Vertical" runat="server" RepeatLayout="Flow" CssClass="checkboxAnswer">
							                        <asp:ListItem>Convenience</asp:ListItem>
							                        <asp:ListItem>Anti-aging</asp:ListItem>
							                        <asp:ListItem>Deep cleaning</asp:ListItem>
							                        <asp:ListItem>Gentle / sensitive skin</asp:ListItem>
							                        <asp:ListItem>For acne-prone skin</asp:ListItem>
							                        <asp:ListItem>For combination skin</asp:ListItem>
							                        <asp:ListItem>Makeup removal / enhancement</asp:ListItem>
							                        <asp:ListItem>None of the above</asp:ListItem>
							                    </asp:CheckBoxList>
							                    
				         <span class="cbinput"></span>
					                            <div class="ErrorContainer">
						                            <skm:CheckBoxListValidator ID="v_Biore_mkt_q5" ControlToValidate="Biore_mkt_q5"
                                                    runat="server" Display="Dynamic" CssClass="errormsg" ErrorMessage="Please answer all required questions." />
					                            </div>

				                            </div>
				                            <div class="CustomQuestioncheckbox Question trait416 cbVert noneToggle4">
					                            <label for="Biore_mkt_q6" id="Biore_mkt_q6LBL" class="checkboxLabel">Which, if any, of the following face care problems would you say you are extremely or very concerned with? (select all that apply)*</label>
					                            
					                            
					                            <asp:CheckBoxList ID="Biore_mkt_q6" RepeatDirection="Vertical" runat="server" RepeatLayout="Flow" CssClass="checkboxAnswer">
							                        <asp:ListItem>Acne</asp:ListItem>
							                        <asp:ListItem>Age spots</asp:ListItem>
							                        <asp:ListItem>Blackheads</asp:ListItem>
							                        <asp:ListItem>Blemishes / pimples</asp:ListItem>
							                        <asp:ListItem>Blemish marks / scars</asp:ListItem>
							                        <asp:ListItem>Crows feet around eyes</asp:ListItem>
							                        <asp:ListItem>Dark circles under eyes</asp:ListItem>
							                        <asp:ListItem>Deep lines / wrinkles</asp:ListItem>
							                        <asp:ListItem>Combination Skin</asp:ListItem>
							                        <asp:ListItem>Facial hair</asp:ListItem>
							                        <asp:ListItem>Facial skin discoloration</asp:ListItem>
							                        <asp:ListItem>Fine lines / wrinkles</asp:ListItem>
							                        <asp:ListItem>Lack of firmness</asp:ListItem>
							                        <asp:ListItem>Large / enlarged pores</asp:ListItem>
							                        <asp:ListItem>Oily / shiny areas</asp:ListItem>
							                        <asp:ListItem>Puffy eyes</asp:ListItem>
							                        <asp:ListItem>Redness / rosacea</asp:ListItem>
							                        <asp:ListItem>Sensitive skin</asp:ListItem>
							                        <asp:ListItem>Sun damage</asp:ListItem>
							                        <asp:ListItem>Uneven skin texture</asp:ListItem>
							                        <asp:ListItem>Uneven skin tone</asp:ListItem>
							                        <asp:ListItem>None of the above</asp:ListItem>
							                    </asp:CheckBoxList>
							                    
				         <span class="cbinput"></span>
					                            <div class="ErrorContainer">
						                            <skm:CheckBoxListValidator ID="v_Biore_mkt_q6" ControlToValidate="Biore_mkt_q6"
                                                    runat="server" Display="Dynamic" CssClass="errormsg" ErrorMessage="Please answer all required questions." />
					                            </div>
				                            </div>
				                            <div class="CustomQuestiondropdown Question trait420">
					                            <label for="Biore_mkt_q10" id="Biore_mkt_q10LBL" class="dropdownLabel">How would you describe your skin type? (select one)*</label>
                                                
                                                <asp:DropDownList ID="Biore_mkt_q10" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem value="">Select...</asp:ListItem>
						                            <asp:ListItem value="Dry">Dry</asp:ListItem>
						                            <asp:ListItem value="Normal">Normal</asp:ListItem>

						                            <asp:ListItem value="Oily">Oily</asp:ListItem>
						                            <asp:ListItem value="Normal to Oily">Combination Normal to Oily</asp:ListItem>
						                            <asp:ListItem value="Combination Normal to Dry">Combination Normal to Dry</asp:ListItem>
						                            <asp:ListItem value="Combination Oily-Dry">Combination Oily-Dry</asp:ListItem>

					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
						                             <asp:RequiredFieldValidator ID="v_Biore_mkt_q10" runat="server" ControlToValidate="Biore_mkt_q10" ErrorMessage="Please answer all required questions." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
					                            </div>
				                            </div>
			                            </div>
		                            </div>
           
                                    <div class="OptinContainer">
                                        <div class="seperator png-fix"></div>
	                                    <p class="privacy">
		                                    Your privacy is important to us. You can trust that Kao Canada Inc will use personal information in accordance with our <a href="http://www.kaobrands.com/privacy_policy.asp"
			                                    target="_blank">Privacy Policy</a>.
	                                    </p>

	                                    <div class="CurrentSiteOptinContainer">
	                                        <ul>
		                                        <li>
		                                            <asp:CheckBox ID="bioreoptin" runat="server" />
			                                        <asp:Label ID="bioreoptinLabel" runat="server" Text="Yes, tell me about future Bior&eacute;<sup>&reg;</sup> product news and offerings." AssociatedControlID="bioreoptin" CssClass="siteOptinChkbox" />

			                                        <div class="ErrorContainer">
                                    			        <skm:CheckBoxValidator ID="cbv_bioreOptin" runat="server" ControlToValidate="bioreoptin" ErrorMessage="If you want to become a Bioré® Brand Member, please check Yes!" CssClass="errormsg" Display="Dynamic"></skm:CheckBoxValidator>
				                                    </div>

		                                        </li>
	                                        </ul>
	                                    </div>
                                        
                                        <!--<div id="ctl00_ContentPlaceHolder1_ucForm_ucFormOptin_PanelMultiOptin" class="MultiBrandOptinContainer">
                                    			            
		                                    <ul>            
			                                    <li id="optinToAll">
			                                        <asp:CheckBox ID="MultiOptin" runat="server" CssClass="multiOptin" />
			                                        <asp:Label ID="MultiOptinLabel" runat="server" Text="Yes, I'd like to receive emails and newsletters from other great products from Kao Brands Company:" AssociatedControlID="MultiOptin" />
			        
			                                    </li>			

			                                    <li id="ctl00_ContentPlaceHolder1_ucForm_ucFormOptin_curelListItem">
			                                        <asp:CheckBox ID="cureloptin" runat="server" />
			                                <asp:Label ID="cureloptinLabel" runat="server" Text="Cur&eacute;l<sup>&reg;</sup></a> Skincare's full line of hand, foot and body moisturizers delivers freedom from dry skin." AssociatedControlID="cureloptin"
				                                CssClass="brandLabels" />
			                         
			                                    </li>
			                                    <li id="ctl00_ContentPlaceHolder1_ucForm_ucFormOptin_jergensListItem">
			                                        <asp:CheckBox ID="jergensoptin" runat="server" />
			                                        <asp:Label ID="jergensoptinLabel" runat="server" Text="Jergens<sup>&reg;</sup></a> Skincare collection of moisturizers delivers a natural glow, smooth and firm skin and an allure that captivates." AssociatedControlID="jergensoptin"
				                                CssClass="brandLabels" />

			                                    </li>

			                                    <li>
				                                    <p class="privDisclaimer">Before submitting your information, please view our <a
					                                    href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">Privacy Policy</a>.</p>
			                                    </li>
		                                    </ul>
                                    	
		                                    </div>	-->	

                                    </div> 

                                    <div id="submit-container" class="SubmitContainer png-fix">
                                        <asp:Button ID="Button1" runat="server" CssClass="submit buttonLink png-fix" Text="SUBMIT ›" OnClick="Step1_ButtonClick"  />
                                    </div>
                                    <asp:HiddenField ID="hfHookID" runat="server" />
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="OptinFormResults" runat="server">
                                 <div class="ConfrimationContainer"><h2>Thanks!</h2><p>Thanks for your interest in Bior&eacute;<sup>&reg;</sup> Skincare. We look forward to sending you all the latest news on exciting events, contests, and product information throughout the year.</p></div>  

                            </asp:Panel>
                            <asp:Panel ID="OptinError" runat="server">
                                An error has occurred while attempting to submit your information.   <asp:Literal ID="litError" runat="server"></asp:Literal>
                            </asp:Panel>
                            <div class="FormBottom png-fix">
                            </div>
                            <!-- Disclaimer -->
                            <div id="DisclaimerContainer" class="png-fix">
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">

                    </script>

                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script type="text/javascript">
        $(function() {

            if ($(".signUp").length) {
                toggle("noneToggle1");
                toggle("noneToggle2");
                toggle("noneToggle3");
                toggle("noneToggle4");
            }

            $("#ctl00_ContentPlaceHolder1_cureloptin, #ctl00_ContentPlaceHolder1_jergensoptin").click(function() {
                if ($('#ctl00_ContentPlaceHolder1_cureloptin:checked').val()
                    || $('#ctl00_ContentPlaceHolder1_jergensoptin:checked').val()) 
                {
                        $('#ctl00_ContentPlaceHolder1_MultiOptin').attr("checked", "true");
                }
                else { $('#ctl00_ContentPlaceHolder1_MultiOptin').removeAttr("checked"); }
            });
            $("#ctl00_ContentPlaceHolder1_MultiOptin").click(function() {
                if ($('#ctl00_ContentPlaceHolder1_MultiOptin:checked').val()) {
                    $("#ctl00_ContentPlaceHolder1_cureloptin").attr("checked", "true");
                    $("#ctl00_ContentPlaceHolder1_jergensoptin").attr("checked", "true");
                } else {
                    $("#ctl00_ContentPlaceHolder1_cureloptin").removeAttr("checked");
                    $("#ctl00_ContentPlaceHolder1_jergensoptin").removeAttr("checked");
                }
            });
        });
        
        
        function toggle(className) {
            var checkToggle = $("." + className + " input:last");
            checkToggle.click(function() {
                if ($("#" + checkToggle.attr('id') + ":checked").val()) {
                    $("." + className + " input").each(function() {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).attr("checked", false);
                            $(this).attr('disabled', 'disabled');
                        }
                    });
                } else {
                    $("." + className + "  input").each(function() {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).removeAttr('disabled');
                        }
                    });
                }
            });
        }
    </script>

</asp:Content>
