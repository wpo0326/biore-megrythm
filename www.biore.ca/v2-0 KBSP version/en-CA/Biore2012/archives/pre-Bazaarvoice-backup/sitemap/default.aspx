﻿<%@ Page Title="Sitemap | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.sitemap._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1>Sitemap</h1>
                 <div id="linkList">
                     <div id="prodLinks">
                         <ul class="col" id="col1">
                            <li><a href="../biore-facial-cleansing-products">Our products <span class="arrow">&rsaquo;</span></a></li>
                            <li>
                                <h2 class="pie deepCleansing">Deep Cleansing</h2>
                                <ul>
                                    <li><a href="../deep-cleansing-products/combination-skin-balancing-cleanser"><span class="new">New</span> Combination Skin Balancing Cleanser</a></li>
                                    <li><a href="../deep-cleansing-products/steam-activated-cleanser">Steam Activated Cleanser</a></li>
                                    <li><a href="../deep-cleansing-products/4-in-1-detoxifying-cleanser">4-in-1 Revitalizing Cleanser</a></li>
                                    <li><a href="../deep-cleansing-products/pore-unclogging-scrub">Pore Unclogging Scrub</a></li>
                                    <li><a href="../deep-cleansing-products/daily-cleansing-cloths">Daily Purifying Scrub</a></li>
                                    <li><a href="../deep-cleansing-product-family/pore-strips#regular">Deep Cleansing Pore Strips</a></li>
                                    <li><a href="../deep-cleansing-product-family/pore-strips#ultra">Deep Cleansing Pore Strips Ultra</a></li>
                                    <li><a href="../deep-cleansing-product-family/pore-strips#combo">Deep Cleansing Pore Combo</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="col" id="col2">
                            <li>
                                <h2 class="pie complexionClearing">Complexion Clearing</h2>
                                <ul>
                                    <li><a href="../complexion-clearing-products/blemish-fighting-ice-cleanser">Blemish Fighting Ice Cleanser</a></li>
                                    <li><a href="../complexion-clearing-products/blemish-treating-astringent">Blemish Treating Astringent</a></li>
                                    <li><a href="../complexion-clearing-products/warming-anti-blackhead-cleanser">Warming Anti-Blackhead Cream Cleanser</a></li>
                                </ul>
                                <h2 class="pie murt">Make-Up Removing</h2>
                                <ul>
                                    <li><a href="../make-up-removing-products/make-up-removing-towelettes">Make-Up Removing Towelettes</a></li>
                                </ul>
                                <!--<ul>
                                    <li class="discontinued"><a href="../past-biore-favorites">Discontinued Products</a></li>
                                </ul>-->
                            </li>
                        </ul>
                    </div>
                    <ul class="col noPadding" id="col3">
                        <li>
                            <a href="../special-offers-and-skincare-tips">What's New <span class="arrow">&rsaquo;</span></a>
                            <ul>
                                <li><a href="../clean-new-look">New Look</a></li>
                            </ul>
                        </li>
                        <li><a href="../email-newsletter-sign-up">Sign Me Up <span class="arrow">&rsaquo;</span></a></li>
                        <!--<li><a href="../where-to-buy-biore">Where To Buy <span class="arrow">&rsaquo;</span></a></li>-->
                        <li><a href="../biore-videos">Watch It Work <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../about-us">About Bior&eacute;<sup>&reg;</sup> Skincare <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../contact-us">Contact Us <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaobrands.com/" target="_blank">Kao Canada <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaobrands.com/legal.asp" target="_blank">Legal <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">Privacy Policy <span class="arrow">&rsaquo;</span></a></li>
                     </ul>
                 </div>
                 <div id="photo"></div>
            </div>
        </div>
    </div>
</asp:Content>