﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TargetEndCap.aspx.cs" Inherits="Biore2012.TargetEndCap.TargetEndCap" %>

<%@ Register TagPrefix="uc1" TagName="ucFormConfig" Src="~/Controls/ucFormConfig.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
		<title></title>
		<link href="TargetEndCap.css" rel="stylesheet" type="text/css" />
		<link href="../css/formConfig.css" rel="stylesheet" type="text/css" />
		<link href="../css/pie.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
        <script type="text/javascript">
            if (typeof jQuery == 'undefined') {
                document.write(unescape("%3Cscript src='../js/jquery-1.6.4.min.js' type='text/javascript'%3E%3C/script%3E"));
            }
        </script>
	</head>
	<body>
		<form id="form1" runat="server">
		<div id="wrapper">
			<div id="header">
				<div id="headWrap">
					<a href="/usa/" class="ir" id="logo">Bior&eacute;&reg;</a>
					<h1 class="ir">The NEW LOOK Sweepstakes! Your chance to Win a $500 Gift Card!<sup>1</sup></h1>
					<h2 class="ir">We're celebrating our new look by giving away great prizes. Join us!
						Enter the information below to enter the Bior&eacute;&reg; Skincare New Look Sweepstakes.</h2>
					<img src="images/BioreProducts.jpg" alt="Biore Deep Cleansing Pore Strips and Blemish Fighting Ice Cleanser"
						height="244" width="157" />
				</div>
			</div>
			<div id="middle">
				<%--Include the Dynamic Web Form--%>
				<uc1:ucFormConfig ID="ucFormConfig" runat="server"></uc1:ucFormConfig>
				<p class="disclaimer"><strong><sup>1</sup>NO PURCHASE NECESSARY.</strong>  Legal residents of the 50 United States (D.C.) 18 years and older.  Enter by 1/31/12. To enter and for Official Rules, including odds, and prize descriptions, visit <a href="Rules.aspx" target="_blank">www.biore.com/en-US/TargetEndCap/Rules.aspx</a>.  Void where prohibited.</p>
			</div>
			<div id="footer">
				<ul>
					<li id="copyright">&copy; 2011 Kao Brands Company. All Rights Reserved</li>
					<li><a href="/usa/about-biore.aspx">About Bior&eacute;<sup>&reg;</sup> Skincare</a></li>
					<li><a target="_blank" href="http://www.kaobrands.com/">Kao Brands Company</a></li>
					<li><a href="/usa/where-to-buy.aspx">Where to Buy</a></li>
					<li><a href="/usa/ContactUs.aspx">Contact Us</a></li>
					<li><a target="_blank" href="http://www.kaobrands.com/legal.asp">Legal</a></li>
					<li><a target="_blank" href="http://www.kaobrands.com/privacy_policy.asp">Privacy Policy</a></li>
				</ul>
			</div>
		</div>
		</form>

		<script type="text/javascript">


          $(function() {


          toggle("noneToggle1");
          toggle("noneToggle2");
          toggle("noneToggle3");
          toggle("noneToggle4");

              function toggle(className) {
                  var checkToggle = $("." + className + " input:last");
                  checkToggle.click(function() {
                  if ($("#" + checkToggle.attr('id') + ":checked").val()) {
                      $("." + className + " input").each(function() {
                      if ($(this).attr('id') != checkToggle.attr('id')) {
                                  $(this).attr("checked", false);
                                  $(this).attr('disabled', 'disabled');
                              }
                          });
                      } else {
                      $("." + className + "  input").each(function() {
                      if ($(this).attr('id') != checkToggle.attr('id')) {
                                  $(this).removeAttr('disabled');
                              }
                          });
                      }

                  });
              }
          }); 
             
		</script>

		<script type="text/javascript">
  var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
  document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script>

		<script type="text/javascript">
  try {
    var firstTracker = _gat._getTracker("UA-385129-27"); // Global Tag
    firstTracker._setDomainName("none");
    firstTracker._trackPageview();
    var secondTracker = _gat._getTracker("UA-385129-15"); // Biore Tag
    secondTracker._trackPageview();
  } catch(err) {}
		</script>

	</body>
</html>
