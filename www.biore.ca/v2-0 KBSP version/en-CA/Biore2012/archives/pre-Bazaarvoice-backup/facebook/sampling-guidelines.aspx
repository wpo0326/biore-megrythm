<!DOCTYPE html>
<!--[if IE 7 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie7 ie"><![endif]-->
<!--[if IE 8 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie8 ie"><![endif]-->
<!--[if IE 9 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie9 ie"><![endif]-->
<!--[if !IE]><!--><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" ><!--<![endif]-->
<head>
<title>
	Bior� Combination Skin Balancing Cleanser Sampling Guidelines
</title>
    
    <link rel="stylesheet" type="text/css" href="/en-CA/css/global.css" />
<style type="text/css">
#mainbody{width:80%; margin-left:auto;margin-right:auto;}
h1 {padding:10px 0px 10px 0px;}
p {padding-bottom:20px}
</style>
</head>

<body>
<div id="mainbody">
<div id="languageToggle"><a href="/fr-CA/facebook/sampling-guidelines.aspx">Fran�ais</a></div>

<h1>Bior� Combination Skin Balancing Cleanser Sampling Guidelines</h1>

<p>Bior� Combination Skin Balancing Cleanser Sampling Guidelines Commencing on Feb 13, 2012, and ending no later than June 4, 2012, up to 30,000 Bior� Combination Skin Balancing Cleanser (7mL) (including a $2.00 off coupon for a future purchase of a 200mL Combination Skin Balancing Cleanser) will be issued and distributed on a first come first serve basis to qualifying Canadian citizens who register online for a free sample at  <a href="http://www.facebook.com/BioreCanada?sk=app_220813114676908">http://www.facebook.com/BioreCanada?sk=app_220813114676908</a>, through the Combination Skin Balancing Cleanser Facebook Tab. Entrants must be a fan of Bior� on Facebook to be eligible for the sample. This sample offer is for one (1) Combination Skin Balancing Cleanser 7mL sample which will be mailed to your home within 3-6 weeks of registration. To be eligible for the free sample, you must be: (i) a Canadian citizen with a lawful permanent residence in Canada and (ii) be over the age of majority in your province/territory of residence. Registration will be exclusively online at <a href="http://www.facebook.com/BioreCanada?sk=app_220813114676908">http://www.facebook.com/BioreCanada?sk=app_220813114676908</a> and must include first and last name, complete mailing address and email address. Limit one (1) sample per person/email address/ household. For greater certainty, you can only use one email address to register for this Campaign."  If you attempt to register more than once, all subsequent requests will be void. A notice will be posted on the site when 30,000 samples have been distributed, and the offer is no longer available. </p> 

<p>The Bior� Combination Skin Balancing Cleanser Tab and Sampling effort is in no way sponsored, endorsed or administered by, or associated with Facebook.  You understand that you are providing your information to the Sponsor and not to Facebook. The information you provide will only be used for the administration of this Sampling Effort and in accordance with the Sponsor�s privacy policy (see below). Facebook is completely released of all liability by each entrant in this Sampling effort.  Any questions, comments or complaints regarding the Campaign must be directed to the Sponsor and not Facebook.</p>

<p>Kao Canada Inc. is collecting personal data about participants for the purpose of administering this Campaign.  Kao Canada Inc. is not authorized to use your personal information in any way other than the intended use and is subject to applicable Canadian Federal and Provincial privacy legislation. Please see the Kao Canada Inc. Privacy Policy at <a href="http://www.kaobrands.com/privacy_policy.asp">http://www.kaobrands.com/privacy_policy.asp</a> for information on its policy towards maintaining the privacy and security of user information. </p>
</div>
</body>

</html>
