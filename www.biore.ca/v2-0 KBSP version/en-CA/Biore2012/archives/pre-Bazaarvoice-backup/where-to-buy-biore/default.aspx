﻿<%@ Page Title="Where To Buy | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.where_to_buy._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Quickly find where to buy your favorite Bioré® products" name="description" />
    <meta content="where to buy Bioré products, find Bioré retailers" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/whereToBuy.css")
        .Render("~/css/combinedbuy_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <h1>Where To Buy</h1>
                <div id="logoHolder">
                    <div id="storeLogos" class="logos">
                        <p>Shop for Bior&eacute;<sup>&reg;</sup> products at a retailer near you:</p>
                        <ul>
                            <li class="walmart"><a href="http://www.walmart.com/cservice/ca_storefinder.gsp" target="_blank">Walmart</a></li>
                            <li class="target"><a href="http://sites.target.com/site/en/spot/page.jsp?title=store_locator_new&ref=nav_storelocator">Target</a></li>
                            <li class="harmon"><a href="http://store.facevaluesonline.com/store-locations.html" target="_blank">Harmon</a></li>
                            <li class="riteaid"><a href="http://www.riteaid.com/stores/locator/" target="_blank">RiteAid</a></li>
                            <li class="float">
                                <div class="walgreens"><a href="http://www.walgreens.com/marketing/storelocator/find.jsp?foot=store_locator" target="_blank">Walgreens</a></div>
                                <div class="cvs"><a href="http://www.cvs.com/CVSApp/store/storefinder.jsp" target="_blank">CVS/pharmacy</a></div>
                            </li>
                            <li class="kmart"><a href="http://www.kmart.com/shc/s/StoreLocatorView?storeId=10151" target="_blank">Kmart</a></li>
                        </ul>                
                    </div>
                    <div id="onlineLogos" class="logos">
                        <p>Shop for Bior&eacute;<sup>&reg;</sup> products online:</p>
                        <ul>
                            <li class="walmart"><a href="http://www.walmart.com/search/search-ng.do?search_constraint=0&ic=48_0&search_query=biore&Find.x=20&Find.y=4" target="_blank">Walmart</a></li>
                            <li class="target"><a href="http://www.target.com/s?searchTerm=biore">Target</a></li>
                            <li class="harmon"><a href="http://store.facevaluesonline.com/searchresult.html?catalog=yhst-29523360387793&query=biore&x=0&y=0" target="_blank">Harmon</a></li>
                            <li class="ulta"><a href="http://search.ulta.com/nav/brand/Biore/0" target="_blank">Ulta</a></li>
                            <li class="walgreens"><a href="http://www.walgreens.com/search/results.jsp?Ntt=biore&x=0&y=0" target="_blank">Walgreens</a></li>
                            <li class="drugstore"><a href="http://www.drugstore.com/templates/brand/default.asp?brand=8696&trx=SBB-0-AB&trxp1=8696" target="_blank">drugstore.com</a></li>
                            <li class="duanereade"><a href="http://www.duanereade.com/Default.aspx" target="_blank">Duanereade</a></li>
                            <li class="riteaid"><a href="http://www.riteaidonlinestore.com/s?searchKeywords=biore" target="_blank">RiteAid</a></li>
                            <li class="cvs"><a href="http://www.cvs.com/CVSApp/search/search.jsp?searchTerm=biore" target="_blank">CVS/pharmacy</a></li>
                        </ul>                
                    </div>
                </div>
                <div id="productShot">
                    <img src="../images/whereToBuy/productGrouping.jpg" alt="" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

