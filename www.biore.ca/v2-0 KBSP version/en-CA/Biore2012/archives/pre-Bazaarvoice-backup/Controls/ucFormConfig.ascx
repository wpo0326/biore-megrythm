﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="KaoBrands.FormStuff.ucFormConfig" Codebehind="ucFormConfig.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="ucFormMemberInfo" Src="~/Controls/ucFormMemberInfo.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ucFormOptin" Src="~/Controls/ucFormOptin.ascx" %>
<asp:Panel ID="PageContainer" cssClass="responsive noForm" runat="server">
<div id="contactFormWrap">
    <div id="BrandImageContainer" class="png-fix">
        <asp:Image ID="Image1" AlternateText="test" runat="server" Visible="false" CssClass="png-fix"/>	
	</div>
	
	<div id="ContactFormContainer">
	     <!-- Header --> 
        <div id="formHeader">
            <h1 id="PageHeader" runat="server">
                <asp:Literal ID="LitHeaderText" runat="server"></asp:Literal>
            </h1> 
        </div>
         
        <!-- Description --> 
        <asp:Panel ID="DescriptionContainer" CssClass="DescriptionContainer png-fix" runat="server"> 
            <asp:Literal ID="LitDescriptionText" runat="server"></asp:Literal>  
        </asp:Panel>     

        
        <!-- From Fields --> 
        <asp:Panel ID="PanelForm" CssClass="FormContainer" runat="server">              
            <p class="req png-fix"><em>Required*</em></p>
            <uc1:ucFormMemberInfo ID="ucFormMemberInfo" runat="server" />                    
            <asp:Panel ID="QuestionPanel" cssClass="QuestionContainer" runat="server">            
            </asp:Panel>
           <uc2:ucFormOptin ID="ucFormOptin" runat="server" />
            <div id="submit-container" class="SubmitContainer png-fix">
                <asp:Button UseSubmitBehavior="true" ID="submit" Text="" runat="server" CssClass="submit buttonLink png-fix" />
            </div>
        </asp:Panel>    
              
        <div class="FormBottom png-fix"></div>
                  
        <!-- Disclaimer --> 
        <div id="DisclaimerContainer" class="png-fix">
            <asp:Literal ID="LitDisclaimer" runat="server"></asp:Literal>  
        </div>
        
	</div>
</div>
<script type="text/javascript">
<asp:Literal runat="server" id="equalHeightVars"></asp:Literal>
</script>
</asp:Panel>