﻿<%@ Page Title="Get a Clean Face | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Get healthy, radiant skin with products from Bioré® Skincare" name="description" />
    <meta content="skincare, clean face" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/homepage.css")
        .Add("~/css/flexslider.css")  
        .Render("~/CSS/combinedhome_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>       
       <div class="flexslider" id="theater">
            <ul class="slides">
    	        <li id="theaterItem1" class="theaterHolder">
    	            <div class="theaterBox">
    	                <h1>Face Anything!</h1>
    	                <p>
    	                    <span>We've designed our new look to match what's inside: extraordinary skincare formulas. Get clean skin so you can be ready 24/7. </span>
    	                    <a href="clean-new-look">
    	                        <img class="normal" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/takeALookBtn.png") %>" alt="Take a look" />
    	                        <img class="over" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/takeALookBtnOver.png") %>" alt="Take a look" />
    	                    </a>
    	                </p>
    		            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/theater1Bg.jpg") %>" alt="" />
    		       </div>
    	        </li>
    	        <%--<li id="theaterItem2" class="theaterHolder">
    		        <div class="theaterBox">
    		            <h1>Prove It!&trade; Reward Points</h1>
    		            <p>
    		                <span>Get rewards for your routine on Facebook.</span>
    	                    <asp:HyperLink ID="proveItTheaterLink" NavigateUrl="http://www.facebook.com/bioreskin?sk=app_205787372796203" runat="server" Target="_blank">
    	                        <img class="normal" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/startEarningTodayBtn.png") %>" alt="Start Earning Today!" />
    	                        <img class="over" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/startEarningTodayBtnOver.png") %>" alt="Start Earning Today!" />
    	                    </asp:HyperLink>
    		            </p>
    		            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/theater2Bg.jpg") %>" alt="" />
    		        </div>
    	        </li>--%>
    	        <li id="theaterItem3" class="theaterHolder">
    		       <div class="theaterBox">
    		            <h1>NEW! A Solution for Combination Skin</h1>
    		            <p>
    		                <span>Life is unpredictable but your skin doesn't have to be. Complex complexion? Try Combination Skin Balancing Cleanser.</span>
    	                    <a href="deep-cleansing-products/combination-skin-balancing-cleanser">
    	                        <img class="normal" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/learnMoreBtn.png") %>" alt="Learn More" />
    	                        <img class="over" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/learnMoreBtnOver.png") %>" alt="Learn More" />
    	                    </a>
    		            </p>
    		            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/theater3Bg.jpg") %>" alt="" />
    		        </div>
    	        </li>
            </ul>
        </div>
        <div class="centeringDiv" id="pagerWrapper"><div id="pager"></div></div>
    </div>
    <div id="promoHolder">
        <div id="promo1" class="promo" style="margin-right:200px">
            <div class="promoImage"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/promo3Img.jpg") %>" alt="" /></div>
            <div class="promoContent">
                <div class="promoHeader">Join the movement</div>
                <div class="promoBody">Sign up to hear about offers and promos from Bior&eacute;<sup>&reg;</sup> Skincare.</div>
                <div class="promoButton"><a href="email-newsletter-sign-up">Check it Out <span class="arrow">&rsaquo;</span></a></div>
            </div>
        </div>
        <%--<div id="promo2" class="promo">
            <div class="promoImage"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/promo2Img.jpg") %>" alt="" /></div>
            <div class="promoContent">
                <div class="promoHeader">Love our stuff?</div>
                <div class="promoBody">Want to know when you can see Bior&eacute;<sup>&reg;</sup> Pore Strips in the new web series, Dating Rules from My Future Self?</div>
                <div class="promoButton"><a href="http://www.facebook.com/DatingRules" target="_blank">Learn more <span class="arrow">&rsaquo;</span></a></div>
            </div>
        </div>--%>
        <div id="promo3" class="promo">
            <div class="promoImage"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/promo1Img.jpg") %>" alt="" /></div>
            <div class="promoContent">
                <div class="promoHeader">2 Steps to a<br />Complete Clean</div>
                <div class="promoBody">1. Cleanse daily<br /> 2. Pore Strip weekly</div>
                <div class="promoButton"><a href="biore-facial-cleansing-products">Learn More <span class="arrow">&rsaquo;</span></a></div>
            </div>
        </div>
    </div>        
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/jquery.flexslider.min.js")
        .Render("~/js/combinedhome_#.js")
    %>
</asp:Content>
