﻿<%@ Page Title="New Style | Bior&eacute;&reg; Skincare" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="default.aspx.cs" Inherits="Biore2012.special_offers_and_skincare_tips._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Check out our new look and product updates from Bioré® Skincare" name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/education.css")
        .Render("~/css/combinededucation_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div id="bg"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/educationBg.jpg") %>" alt="" /></div>
        <div class="centeringDiv">
            <h1>
                <span class="newLine firstLine">We've Got Big News&nbsp;.&nbsp;.&nbsp;.</span>
                <span class="newLine secondLine">And We Can't Wait To&nbsp;Share It!</span>
                <img id="arrow" src="<%= VirtualPathUtility.ToAbsolute("~/images/education/arrow.png") %>" alt="" />
            </h1>
            <h2><img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/learnAboutTheLatest.png") %>" alt="Learn about the latest updates to Bior&eacute;&reg; Skincare" /></h2>
            <div id="steps">
                <div class="imageWrapper">
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/educationProduct.jpg") %>" class="productImg" alt="Bior&eacute;&reg; Steam Activated Cleanser" />
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/handdrawnOverlay.png") %>" alt="" class="drawnArrows" />
                    <a href="#item1" id="item1Nav" class="scrollPage"></a>
                    <a href="#item2" id="item2Nav" class="scrollPage"></a>
                    <a href="#item3" id="item3Nav" class="scrollPage"></a>
                </div>
                <ul id="listCopy">
                    <li id="item1">
                        <div class="copy">
                            <h3><img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/boldNewLookH.png") %>" alt="Bold New Look" /></h3>
                            <p>Clean, white packaging with easy-to-find product info.</p>
                        </div>
                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/oneBg.jpg") %>" alt="" class="sideImg" />
                    </li>
                    <li id="item2">
                        <div class="copy">
                            <h3><img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/sameFormulaH.png") %>" alt="Same deep-clean formulas" /></h3>
                            <p>Our dirt-fighting, deep-cleaning formulas are as effective and invigorating as always.</p>
                        </div>
                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/twoBg.jpg") %>" alt="" class="sideImg" />
                    </li>
                    <li id="item3">
                        <div class="copy">
                            <h3><img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/newColorCodingH.png") %>" alt="Color-coded categories" /></h3>
                            <p>New product categories – Deep Cleansing (blue), Complexion Clearing (orange), and Make-up Removing (pink).</p>
                        </div>
                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/threeBg.png") %>" alt="" class="sideImg" />
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
            <div id="copy">
                <div class="col marginRight">
                    <h3 class="new">New Look</h3>
                    <p>We're all about creating effective, deep-cleansing products. And just like our products, our new look is clean, straightforward and honest. Above all, it gets the job done. </p>
                    <div class="combinationSkin">
                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/combinationSkin.jpg") %>" alt="" />
                        <h3 class="blue"><span class="new">New</span> Combination Skin Balancing Cleanser</h3>
                        <p>We know skin often doesn't fit into the classic categories of oily vs. dry. That's why we've created <a href="<%= VirtualPathUtility.ToAbsolute("~/deep-cleansing-products/combination-skin-balancing-cleanser") %>">Combination Skin Balancing Cleanser</a>. If you've got in-between, can't-make-up-its mind, or it's-complicated skin, bring some balance back to your face with our new cleanser. </p>
                    </div>
                </div>
                <div class="col">
                    <h3><span class="blue">Get Clean Skin and Be Ready</span> <span class="orange">24/7</span></h3>
                    <p>Whether you're at a job interview, out with your girls, or just cuddling on the couch for movie night—who wants to worry about their skin? Not you. We’ve created a whole lineup of products so you can get a deep clean and be ready 24/7.</p>
                    <p>Getting a fresh face is simple: cleanse daily and strip weekly. You don't even have to go to the sink. Our <a href="<%= VirtualPathUtility.ToAbsolute("~/make-up-removing-products/make-up-removing-towelettes") %>">Make-up Removing Towelettes</a> provide a quick clean anytime, anywhere.</p>
                    <p>Bior&eacute;<sup>&reg;</sup> Skincare gets you clean, fresh skin, from your morning skinny vanilla latte, to your late night political debate. Get clean skin and be ready 24/7.</p>
                    <p class="faceAnything"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/faceAnything.png") %>" alt="Face Anything&trade;" /></p>
                </div>
            </div>
            <div class="teaser" id="newLook">
                <img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/newLookBg.jpg") %>" class="backgroundImg" alt="" />
                <img src="<%= VirtualPathUtility.ToAbsolute("~/images/education/newLookH.png") %>" alt="New Look!" class="newLookImg" />
                <p>Start exploring all of our products and check out the new look we're rocking. &raquo;</p>                
                <a href="<%= VirtualPathUtility.ToAbsolute("~/biore-facial-cleansing-products/") %>"></a>
            </div>
            <!--<div class="teaser" id="productChanges">
                <img src="<%= VirtualPathUtility.ToAbsolute("~/images/productChanges.png") %>" alt="" />
                <h3>See All Our Great Updates.</h3>
                <p>Get all the details on the updates we've made to our product lineup. &raquo;</p>
                <a href="<%= VirtualPathUtility.ToAbsolute("~/past-biore-favorites/") %>"></a>
            </div>-->
            <div class="clear"></div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
