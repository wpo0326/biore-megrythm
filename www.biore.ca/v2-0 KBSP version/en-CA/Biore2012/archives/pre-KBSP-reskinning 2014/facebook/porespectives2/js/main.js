﻿var ps2 = {
	init: function() {
		if (ps2.$id("ctl00_ContentPlaceHolder1_submitBtn") != null) {
			ps2.addEvent(ps2.$id("ctl00_ContentPlaceHolder1_submitBtn"), "click", function(e) {
				//e.preventDefault();
				var commentLen = ps2.$id("ctl00_ContentPlaceHolder1_tbComment").value.length;
				if (commentLen && commentLen <= 400) {
					ps2.fbVars.FBShareDesc = ps2.$id("ctl00_ContentPlaceHolder1_tbComment").value;
					if (ps2.fbVars.FBShareDesc.match(/^[^<>]+$/)) {
						ps2.openFeedDialog();
					}
				}
			});
		}

		if (ps2.$id("closeBtn") != null) {
			ps2.addEvent(ps2.$id("closeBtn"), "click", function(e) {
				if ("preventDefault" in e) { e.preventDefault(); }
				window.opener.ps2.$id("ctl00_ContentPlaceHolder1_tbComment").value = "";
				window.close();
				return false;
			});
		}
	},

	openFeedDialog: function() {
		// Reference for the Feed Dialog:
		// http://developers.facebook.com/docs/reference/dialogs/feed/    		
		var url = 'http://www.facebook.com/dialog/feed?app_id=' + ps2.fbVars.FBAppID +
				  '&link=' + ps2.fbVars.FBShareUrl +
				  '&picture=' + ps2.fbVars.FBSharePic +
				  '&name=' + encodeURIComponent(ps2.fbVars.FBShareName) +
				  '&caption=' + encodeURIComponent(ps2.fbVars.FBShareCaption) +
				  '&description=' + encodeURIComponent(ps2.fbVars.FBShareDesc) +
				  '&message=' + encodeURIComponent(ps2.fbVars.FBShareMsg) +
				  '&redirect_uri=' + ps2.fbVars.FBShareRedirUrl +
				  '&display=popup';

		window.open(url, 'feedDialog', 'toolbar=0,status=0,width=580,height=400');
	},
	addEvent: function(elem, type, eventHandle) {
		// Bind an event handler to an element
		if (elem.addEventListener) {
			elem.addEventListener(type, eventHandle, false);
		} else if (elem.attachEvent) {
			elem.attachEvent("on" + type, eventHandle);
		}
	},
	$id: function(id) {
		return document.getElementById(id);
	}
}
if (window.addEventListener) { window.addEventListener("load", ps2.init, false); }
else if (window.attachEvent) { window.attachEvent("onload", ps2.init); }
else { setTimeout(ps2.init, 500); }	