<%@ Page Title="Get a Clean Face | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Get healthy, radiant skin with products from Bioré® Skincare" name="description" />
    <meta content="skincare, clean face" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/homepage.css")
        .Add("~/css/flexslider.css")  
        .Render("~/CSS/combinedhome_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>       
       <div class="flexslider" id="theater">
            <ul class="slides">
    	        <li id="theaterItem1" class="theaterHolder">
    	            <div class="theaterBox">
    	                <h1>Face Anything!</h1>
    	                <p>
    	                    <span>We've designed our new look to match what's inside: extraordinary skincare formulas. Get clean skin so you can be ready 24/7. </span>
    	                    <a href="clean-new-look">
    	                        <img class="normal" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/takeALookBtn.png") %>" alt="Take a look" />
    	                        <img class="over" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/takeALookBtnOver.png") %>" alt="Take a look" />
    	                    </a>
    	                </p>
    		            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/theater1Bg.jpg") %>" alt="" />
    		       </div>
    	        </li>
    	        <li id="theaterItem4" class="theaterHolder">
    		        <div class="theaterBox">
    		            <h1>We'd love to hear what you think!</h1>
    		            <p>
    	                    <asp:HyperLink ID="proveItTheaterLink" NavigateUrl="~/biore-facial-cleansing-products/" runat="server">
    	                        <img class="normal" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/writeAReviewBtn.png") %>" alt="Write A Review" />
    	                        <img class="over" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/writeAReviewBtnOver.png") %>" alt="Write A Review" />
    	                    </asp:HyperLink>
    		            </p>
    		            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/theater4Bg.jpg") %>" alt="" />
    		        </div>
    	        </li>
    	        <li id="theaterItem3" class="theaterHolder">
    		       <div class="theaterBox">
    		            <h1>NEW! A Solution for Combination Skin</h1>
    		            <p>
    		                <span>Life is unpredictable but your skin doesn't have to be. Complex complexion? Try Combination Skin Balancing Cleanser.</span>
    	                    <a href="deep-cleansing-products/combination-skin-balancing-cleanser">
    	                        <img class="normal" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/learnMoreBtn.png") %>" alt="Learn More" />
    	                        <img class="over" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/learnMoreBtnOver.png") %>" alt="Learn More" />
    	                    </a>
    		            </p>
    		            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/theater3Bg.jpg") %>" alt="" />
    		        </div>
    	        </li>
    	        <!--<li id="theaterItem2" class="theaterHolder">
    		       <div class="theaterBox">
    		            <h1>NEW! A Solution for Combination Skin</h1>
    		            <p>
    		                <span>Life is unpredictable but your skin doesn't have to be. Complex complexion? Try Combination Skin Balancing Cleanser.</span>
    	                    <a href="deep-cleansing-products/combination-skin-balancing-cleanser">
    	                        <img class="normal" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/learnMoreBtn.png") %>" alt="Learn More" />
    	                        <img class="over" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/learnMoreBtnOver.png") %>" alt="Learn More" />
    	                    </a>
    		            </p>
    		            <img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/theater2Bg.jpg") %>" alt="" />
    		        </div>
    	        </li>-->
            </ul>
        </div>
        <div class="centeringDiv" id="pagerWrapper"><div id="pager"></div></div>
    </div>
    <div id="promoHolder">
        <div id="promo1" class="promo">
            <div class="promoImage"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/promo3Img.jpg") %>" alt="" /></div>
            <div class="promoContent">
                <div class="promoHeader">Join the movement</div>
                <div class="promoBody">Sign up to hear about offers and promos from Bior&eacute;<sup>&reg;</sup> Skincare.</div>
                <div class="promoButton"><a href="email-newsletter-sign-up">Check it Out <span class="arrow">&rsaquo;</span></a></div>
            </div>
        </div>
        <div id="promo2" class="promo">
            <div class="promoContent" style="margin-left:75px;">
                <div class="promoHeader">WE’D LOVE TO HEAR WHAT
YOU THINK!</div>
                <div class="promoBody"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/stars.jpg") %>" alt="" /></div>
                <div class="promoButton"><a href="biore-facial-cleansing-products">Write a Review <span class="arrow">&rsaquo;</span></a></div>
            </div>
        </div>
        <div id="promo3" class="promo">
            <div class="promoImage"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/home/promo1Img.jpg") %>" alt="" /></div>
            <div class="promoContent">
                <div class="promoHeader">2 Steps to a<br />Complete Clean</div>
                <div class="promoBody">1. Cleanse daily<br /> 2. Pore Strip weekly</div>
                <div class="promoButton"><a href="biore-facial-cleansing-products">Learn More <span class="arrow">&rsaquo;</span></a></div>
            </div>
        </div>
    </div>        
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/jquery.flexslider.min.js")
        .Render("~/js/combinedhome_#.js")
    %>
    <!--<div style="width:90%; max-width:890px; clear:both; padding:0px 35px; margin:0px auto 20px;">
    *No purchase necessary. Contest closes Sept 3, 2012 at 5 p.m. ET. Open to legal residents of Canada (age of majority). Enter via package PIN available 1) on specially marked packages of Biore® skincare and 2) by mail-in request or enter via www.bioremusic.ca. 1 grand prize is available consisting of 4 tickets to see a Sony Music artist in concert in a Canadian city; gr. transportation; 1 night accommodation; 4 Bioré® skincare gift baskets; 4 Sony® Bloggie Sport™ HD video cameras; and $500 CDN spending money each for winner and guests. Total approx. retail value of grand prize is $10,000 CAD.  128 daily prizes available; each consists of 1 Sony® Bloggie Sport™ HD video cameras (approx. retail value of $200 CAD each).  Correct answer to mathematical skill-testing question required.  Odds of winning depend on the number of eligible entries received.  See full contest rules, prize details, entry and eligibility requirements visit <a href="http://www.bioremusic.ca" target="_blank">www.bioremusic.ca</a>.
    </div>-->
</asp:Content>
