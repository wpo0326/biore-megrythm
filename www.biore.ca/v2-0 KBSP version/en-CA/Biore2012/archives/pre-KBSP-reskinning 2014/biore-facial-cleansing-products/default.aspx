﻿<%@ Page Title="Exfoliate &amp; Get a Clean Face - Browse All Products  | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.our_products.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Get clean skin: Bioré® Skincare products cleanse and gently exfoliate, giving you healthy, radiant skin! See the complete Bioré® Skincare product line." name="description" />
    <meta content="exfoliate" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>
<!--[if IE 9]> 
    <style type="text/css"> 
        .prodList li h3{font-size:13px !important}
    </style> 
<![endif]--> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="intro">
                <h1>Our Products</h1>
                <p>Get a deep clean so you're ready 24/7&mdash;exfoliate and cleanse your skin daily with our invigorating cleansing products, plus pore strip weekly for a complete Bior&eacute;<sup>&reg;</sup> clean.</p>
            </div>
            <div id="deepCleansingProds" class="prodList">
                <h2 class="pie roundedCorners deepCleansing">Deep Cleansing <span>(8)</span><span class="arrow"></span></h2>
                <ul>
                    <li>
                        <img src="../images/ourProducts/products/small/combinationSkinBalancingCleanser.jpg" alt="" />
                        <asp:Literal ID="litComboSkinCleanser" runat="server" />
                        <h3><span class="new">NEW</span> COMBINATION SKIN BALANCING CLEANSER</h3>
                        <p>Get an even, all-over clean for the complex needs of combination skin.</p>
                        <a href="../deep-cleansing-products/combination-skin-balancing-cleanser">details ></a>
                    </li>
                    <li>
                        <img src="../images/ourProducts/products/small/4In1DetoxifyingCleanser.jpg" alt="" />
                        <asp:Literal ID="litRevitalizingCleanser" runat="server" />
                        <h3>4-IN-1 REVITALIZING CLEANSER</h3>
                        <p>Revitalize and refresh for healthier-looking skin.</p>
                        <a href="../deep-cleansing-products/4-in-1-detoxifying-cleanser">details ></a>
                    </li>
                    <li>
                        <img src="../images/ourProducts/products/small/poreUncloggingScrub.jpg" alt="" />
                        <asp:Literal ID="litPoreUncloggingScrub" runat="server" />
                        <h3>PORE UNCLOGGING SCRUB</h3>
                        <p>Target pore-clogging dirt and oil for beautifully clean skin, smooth skin.</p>
                        <a href="../deep-cleansing-products/pore-unclogging-scrub">details ></a>
                    </li>
                    <li>
                        <img src="../images/ourProducts/products/small/dailyPurifyingScrub.jpg" alt="" />
                        <asp:Literal ID="litPurifyingScrub" runat="server" />
                        <h3>DAILY PURIFYING SCRUB</h3>
                        <p>Delivers fresh, glowing skin by exfoliating and purifying </p>
                        <a href="../deep-cleansing-products/daily-purifying-scrub">details ></a>
                    </li>
                    <li style="display:none;">
                        <img src="../images/ourProducts/products/small/steamActivatedCleanser.jpg" alt="" />
                        <asp:Literal ID="litSteamCleanser" runat="server" />
                        <h3>STEAM ACTIVATED CLEANSER</h3>
                        <p>Harness the power of pore-opening steam for a truly deep clean.</p>
                        <a href="../deep-cleansing-products/steam-activated-cleanser">details ></a>
                    </li>
                    <%--<li id="dailyCleansingCloths">
                        <img src="../images/ourProducts/products/small/dailyCleansingCloths.jpg" alt="" />
                        <h3>DAILY CLEANSING CLOTHS</h3>
                        <p>Wipe away dirt to reveal healthy, glowing skin.</p>
                        <a href="../deep-cleansing-products/daily-cleansing-cloths">details ></a>
                    </li>--%>
                    <li>
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStrips.jpg" alt="" />
                        <asp:Literal ID="litPoreStrips" runat="server" />
                        <h3>DEEP CLEANSING<br />PORE STRIPS</h3>
                        <p>Remove a week's worth of build-up in minutes.</p>
                        <a href="../deep-cleansing-product-family/pore-strips">details ></a>
                    </li>
                    <li>
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.jpg" alt="" />
                        <asp:Literal ID="litPoreStripsCombo" runat="server" />
                        <h3>DEEP CLEANSING<br />PORE STRIPS COMBO</h3>
                        <p>Strip more than just your nose with Pore Strips for your cheeks, chin and forehead.</p>
                        <a href="../deep-cleansing-product-family/pore-strips#combo">details ></a>
                    </li>
                    <li>
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltra.jpg" alt="" />
                        <asp:Literal ID="litPoreStripsUltra" runat="server" />
                        <h3>DEEP CLEANSING<br />PORE STRIPS ULTRA</h3>
                        <p>Amp up the Pore Strip with tingling ingredients like tea tree oil, menthol and witch hazel.</p>
                        <a href="../deep-cleansing-product-family/pore-strips#ultra">details ></a>
                    </li>
                </ul>
            </div>
            <div id="complexionClearingProds" class="prodList">
                <h2 class="pie roundedCorners complexionClearing">Complexion Clearing <span>(3)</span><span class="arrow"></span></h2>
                <ul>
                    <li>
                        <img src="../images/ourProducts/products/small/acneClearingScrub.jpg" alt="" />
                        <asp:Literal ID="litAcneClearingScrub" runat="server" />
                        <h3>ACNE CLEARING SCRUB</h3>
                        <p>Get clearer, healthier-looking skin in just two days.</p>
                        <a href="../complexion-clearing-products/acne-clearing-scrub" id="details-acne-clearing-scrub">details ></a>
                    </li>
                    <li>
                        <img src="../images/ourProducts/products/small/blemishFightingIceCleanser.jpg" alt="" />
                        <asp:Literal ID="litBlemishIceCleanser" runat="server" />
                        <h3>BLEMISH FIGHTING ICE CLEANSER</h3>
                        <p>Stay in control, help stop blemishes before they start.</p>
                        <a href="../complexion-clearing-products/blemish-fighting-ice-cleanser">details ></a>
                    </li>
                    <li>
                        <img src="../images/ourProducts/products/small/blemishTreatingAstringent.jpg" alt="" />
                        <asp:Literal ID="litBlemishAstringent" runat="server" />
                        <h3>BLEMISH FIGHTING ASTRINGENT</h3>
                        <p>Go beyond clean to help treat blemishes with salicylic acid.</p>
                        <a href="../complexion-clearing-products/blemish-treating-astringent">details ></a>
                    </li>
                    <li>
                        <img src="../images/ourProducts/products/small/warmingAntiBlackheadCleanser.jpg" alt="" />
                        <asp:Literal ID="litBlackheadCleanser" runat="server" />
                        <h3>WARMING ANTI-BLACKHEAD CREAM CLEANSER</h3>
                        <p>Target blackheads by opening and deep cleaning pores.</p>
                        <a href="../complexion-clearing-products/warming-anti-blackhead-cleanser">details ></a>
                    </li>
                </ul>
            </div>
            <div id="murtProds" class="prodList">
                <h2 class="pie roundedCorners murt">Make-Up Removing <span>(1)</span><span class="arrow"></span></h2>
                <ul>
                    <li id="murt">
                        <img src="../images/ourProducts/products/small/makeupRemovingTowelettes.jpg" alt="" />
                        <asp:Literal ID="litMakeUpTowelettes" runat="server" />
                        <h3>MAKE-UP REMOVING TOWELETTES</h3>
                        <p>Remove waterproof mascara better than a leading towelette.</p>
                        <a href="../make-up-removing-products/make-up-removing-towelettes">details ></a>
                    </li>
                </ul>
            </div>
            <div id="productChanges" class="teaser">
                <!--<img src="../images/productChanges.png" alt="" />-->
                <br /><br />
                <h3 style="line-height:20px;">Writing a review<br />is as easy as 1 2 3</h3>
                <img src="../images/stars.png" alt="" />
                <div style="font-size:13px; line-height:18px;">1. Choose product <br />
                2. Click write a review on product page<br />
                3. Write review and submit</div>
                <!--<a href="../past-biore-favorites/"></a>-->
            </div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
