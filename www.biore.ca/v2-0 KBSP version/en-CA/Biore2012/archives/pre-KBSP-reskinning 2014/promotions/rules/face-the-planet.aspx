<!DOCTYPE html>
<!--[if IE 7 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie7 ie"><![endif]-->
<!--[if IE 8 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie8 ie"><![endif]-->
<!--[if IE 9 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie9 ie"><![endif]-->
<!--[if !IE]><!--><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" ><!--<![endif]-->
<head>
<title>
	Bior� Face the Planet Rules and Regulations
</title>
    
    <link rel="stylesheet" type="text/css" href="/en-CA/css/global.css" />
<style type="text/css">
#mainbody{width:80%; margin-left:auto;margin-right:auto;}
h1 {padding:10px 0px 10px 0px;}
p {padding-bottom:20px}
    .style1
    {
        font-weight: normal;
        font-style: italic;
    }
</style>
</head>

<body>
<div id="mainbody">
<div id="languageToggle"><a href="/fr-CA/promotions/rules/face-the-planet.aspx">Fran�ais</a></div>

   <h1>Face The Planet with Bior� Skin Care</h1>
   
<h4 class="style1">Look good and feel good with Bior� face care</h4>

<p><b>Complete Campaign Rules and Regulations</b></p>

<p>The Campaign is in no way sponsored, endorsed or administered by, or associated with Facebook.  You understand that you are providing your information to the Sponsor and not to Facebook. The information you provide will only be used for the administration of this Campaign and in accordance with the Sponsor�s privacy policy (see below). Facebook is completely released of all liability by each entrant in this Campaign.  Any questions, comments or complaints regarding the Campaign must be directed to the Sponsor and not Facebook.</p>

<p><b>1.	CAMPAIGN PERIOD</b></p>

<p>The Bior� Skin Care Face The Planet Campaign (the �<b>Campaign</b>�) starts at 12:00:01 pm ET on March 06, 2012 and continues until 11:59 pm on April 30, 2012 (the �<b>Campaign Period</b>�). </p>

<p><b>2.	SPONSORSHIP</b></p>

<p>The Campaign is sponsored and run by Kao Canada Inc. (the �<b>Sponsor</b>�).</p>

<p><b>3.	ELIGIBILITY</b></p>

<p>This Campaign is open to individual residents of Canada who have reached the age of majority in their province/territory of residence at the time of entry, except employees, representatives or agents (and those with whom such persons are domiciled, whether related or not) of Sponsor, its parent companies, subsidiaries, affiliates, prize suppliers and advertising/promotion agencies (collectively, the �<b>Campaign Parties</b>�).   </p>

<p><b>4.	LEGAL</b></p>

<p>The Campaign is subject to all applicable federal, provincial/territorial and municipal laws and regulations; and is void where prohibited by law.</p>

<p><b>5.	HOW TO PARTICIPATE</b></p>

<p>No purchase necessary.  In order to participate, you must: (i) have internet access; (ii) be (or become) a fan of Bior� Canada fanpage (the �<b>Fanpage</b>�); and (iii) have (or obtain) a valid email address and Facebook account (the �Account�).  To enter, go to www.facebook.com/BioreCanada (the �<b>Website</b>�) and follow the on-screen instructions to: (i) click on the �Like� button (if you are not already a fan of the Fanpage); (ii) allow the Bior� Skin Care Face The Planet Application (the �<b>Application</b>�) (note: you only need to allow the Application the first time you visit the Website); and (iii) obtain the official Campaign entry form (the  �<b>Entry Form</b>�).  Fully complete the Entry Form with all required information (including your indication that you have read, accept and agree to be legally bound by the terms and conditions of these Official Rules and Regulations).  The Entry Form also requires you to correctly answer a mathematical skill-testing question without mechanical or other aid.  If the skill-testing question is not answered and/or is answered incorrectly, then you will not be eligible to win a Prize (defined below).  Once you have fully completed the Entry Form, you will be required to follow the on-screen instructions and upload a photograph (the �<b>Photo</b>�) depicting your face when it is shiny or oily.  Your Photo must be in accordance with the specific Photo Requirements listed below in Rule #7. When all required fields of the Entry Form are completed (including uploading your Photo), click the �Submit� button to complete your entry (Entry Form and Photo hereinafter collectively referred to as �<b>Entry/Entries</b>�).  Upon successfully submitting an eligible Entry, you will be entered into the random prize draw for a chance to win one of 250 full-sized bottles of Combination Skin Balancing Cleanser.  In addition, the first 10,000 eligible entrants will also receive one (1) 7mL sample sachet of Bior� Combination Skin Balancing Cleanser, while supplies last (the �<b>Sample(s)</b>�, see Rule #9). In addition, for each eligible Entry received during the Campaign Period, Sponsor will make a $1 CAD donation to Toronto Wildlife Center (up to a maximum of $10,000 CAD, see Rule #10).  To be eligible, your Entry must be received within the Campaign Period. </p>

<p>There is a limit of one (1) Entry per person/email address permitted during the Campaign Period.  For greater certainty, you can only use one (1) email address/Account to enter the Campaign.  If it is discovered that any person has attempted to: (i) obtain more than one (1) Entry per person/email address/Account during the Campaign Period; and/or (ii) use (or attempt to use) multiple names, identities and/or more than one (1) email address and/or Account to enter the Campaign; then (in the sole and absolute discretion of the Sponsor) he/she may be disqualified from the Campaign.  Your Entry will be rejected if (in the sole and absolute discretion of the Sponsor): (i) the Entry Form is not fully completed (including uploading your Photo) and submitted during the Campaign Period; and/or (ii) your Photo is not in accordance with the specific Photo Requirements listed below. Use (or attempted use) of multiple names, identities, email addresses, Accounts and/or any automated, macro, script, robotic or other systems(s) or program(s) to enter or otherwise participate in or disrupt this Contest is prohibited and is grounds for disqualification by the Sponsor. The Releasees (defined below) are not responsible for late, lost, misdirected, delayed, incomplete or incompatible Entries.    </p>

<p>All Entries are subject to verification at any time.  The Sponsor reserves the right, in its sole and absolute discretion, to require proof of identity and/or eligibility (in a form acceptable to the Sponsor � including, without limitation, government issued photo identification) to participate in this Campaign.  Failure to provide such proof to the satisfaction of the Sponsor in a timely manner may result in disqualification.  The sole determinant of the time for the purposes of a valid Entry in this Campaign will be the Campaign server machine(s).</p>

<p><b>6.	OPPORTUNITY TO SHARE INFORMATION ABOUT THE CAMPAIGN</b></p>

<p>At the time of entering the Campaign (or at anytime thereafter via the Application), an eligible entrant will have the opportunity to share information about the Campaign with his/her friends and online network. Follow the on-screen instructions by clicking on the �Share� button after completing your Entry to share information about the Campaign.  You will NOT receive anything for the act of sharing information.  </p>

<p><b>7.	PHOTO REQUIREMENTS</b></p>

<p>BY SUBMITTING AN ENTRY, YOU AGREE THAT THE ENTRY (AND EACH INDIVIDUAL COMPONENT THEREOF) COMPLIES WITH ALL CONDITIONS STATED IN THESE RULES.  THE RELEASEES (DEFINED BELOW) WILL BEAR NO LEGAL LIABILITY REGARDING THE USE OF YOUR ENTRY.  THE RELEASEES (DEFINED BELOW) SHALL BE HELD HARMLESS BY YOU IN THE EVENT IT IS SUBSEQUENTLY DISCOVERED THAT YOU HAVE DEPARTED FROM OR NOT FULLY COMPLIED WITH ANY OF THESE RULES.</p>

<p>Your submitted Photo must never have been previously selected as a winner in any other Campaign.  To be eligible for entry in this Campaign, your Photo must meet the following specific requirements: </p>

<table border="1" cellpadding="2" cellspacing="1" style="padding: 5px; border: 1px solid rgb(100, 100, 100); border-collapse: collapse; border-spacing: 2px;">
    <tr>
        <td><b>Maximum Length/Size/Amount	</b></td><td>
        <b>Acceptable Formats</b></td><td><b>Content Requirements</b></td>
    </tr>
    <tr>
        <td>2 MB</td><td>.JPG, .PNG, .GIF</td><td>Photo of ones shiny or oily face</td>
    </tr>
    </table>
<br /><br />
<p>By participating in the Campaign, each entrant agrees to be legally bound by these Official Campaign Rules (the �<b>Rules</b>�) and by the interpretation of these Rules by the Sponsor, and further warrants and represents that his/her Photo: </p>

<p>i.	is original to him/her and that the entrant has all necessary rights (including, without limitation, consent of the photographer) in and to the Photo to enter the Campaign;</p>

<p>ii.	does not violate any law, statute, ordinance or regulation;</p>

<p>iii.	does not contain any reference to or likeness of any identifiable third parties, unless consent has been obtained from all such individuals and their parent/legal guardian if they are under the age of majority in their jurisdiction of residence (note: if you cannot obtain the consent of an individual appearing in your Photo, then his/her face must be blurred out so as to be unrecognizable); </p>

<p>iv.	will not give rise to any claims of infringement, invasion of privacy or publicity, or infringe on any rights and/or interests of any third party, or give rise to any claims for payment whatsoever; and</p>
 
<p>v.	is not defamatory, trade libelous, pornographic or obscene, and further that it will not contain, depict, include, discuss or involve, without limitation, any of the following: nudity; alcohol/drug consumption or smoking; explicit or graphic sexual activity, or sexual innuendo; crude, vulgar or offensive language and/or symbols; derogatory characterizations of any ethnic, racial, sexual, religious or other groups (including, without limitation, any competitors of Sponsor); content that endorses, condones and/or discusses any illegal, inappropriate or risky behaviour or conduct; personal information of individuals, including, without limitation, names, telephone numbers and addresses (physical or electronic); commercial messages, comparisons or solicitations for products or services other than products of Sponsor; any identifiable third party products, trade-marks, brands and/or logos, other than those of Sponsor (e.g. any clothing worn and/or products appearing in your Photo must not contain any visible logos, trade-marks or other third party materials unless the appropriate consents have been obtained --- note: all identifiable third party products, trade-marks, brands and/or logos for which consent has not been obtained by the entrant must be blurred out so as to be unrecognizable); conduct or other activities in violation of these Rules; and/or any other content that is or could be considered inappropriate, unsuitable or offensive, all as determined by the Sponsor in its sole and absolute discretion.</p>

<p>Any Photo that the Sponsor and/or its promotional agency or designated content moderator (the �<b>Reviewer</b>�) deems, in its sole and absolute discretion, to violate the terms and conditions set forth in these Rules will not be posted to the Website through the Application and will result in disqualification of the Entry.  Prior to being posted on the Website through the Application and/or being accepted as an eligible Entry, the Reviewer reserves the right, in its sole and absolute discretion, to edit or modify any Photo, or to request an entrant to modify, edit and/or re-submit his or her Photo, in order to ensure that the Photo complies with these Rules, or for any other reason.  </p>

<p>By entering the Campaign and submitting an Entry, each entrant: (i) grants to the Sponsor, in perpetuity, a non-exclusive license to publish, display, reproduce, modify, edit or otherwise use his/her Photo, in whole or in part, for advertising or promoting the Campaign or for any other reason; (ii) waives all moral rights in and to his/her Photo in favour of the Sponsor; and (iii) agrees to release and hold harmless the Campaign Parties and each of their respective agents, employees, directors, successors, and assigns (collectively, the �<b>Releasees</b>�) from and against any and all claims based on publicity rights, defamation, invasion of privacy, copyright infringement, trade-mark infringement or any other intellectual property related cause of action that relate in any way to the Photo.  For greater certainty, the Reviewer reserves the right, in its sole and absolute discretion and at any time during or after the Campaign, to modify, edit or remove any Photo, or to request an entrant to modify or edit his or her Photo, if a complaint is received with respect to the Photo, or for any other reason.  </p>

<p><b>8.	PRIZES</b></p>

<p>There will be two hundred and fifty (250) randomly drawn prizes available to be won, each consisting of one (1) 200mL Bior� Combination Skin Cleanser; with an approximate retail prize value of $9.99 each (the �<b>Prize(s)</b>�).  Each Prize must be accepted as awarded and is not transferable, assignable or convertible to cash.  No substitutions except at Sponsor�s option.  Sponsor reserves the right, in its sole and absolute discretion, to substitute any Prize or a component thereof with a prize of equal or greater value, including, without limitation, but at Sponsor�s sole and absolute discretion, a cash award. Each Prize will only be awarded to the person whose verifiable full name and valid email address appears on the Entry Form.  There is a limit of one (1) Prize per person. </p>

<p><b>9.	SAMPLES</b></p>

<p>Upon successfully submitting an eligible Entry, the first 10,000 eligible entrants will receive one (1) Sample, which will be mailed to their home within 3-6 weeks of registration.  A notice will be posted on the Fanpage when 10,000 Samples have been distributed, and the offer is no longer available.</p>

<p><b>10.	CHARITABLE DONATION</b></p>

<p>For each eligible Entry received in accordance with these Rules, a $1 CAD donation will be made by Sponsor to Toronto Wildlife Center (up to a maximum of $10,000 CAD).  In the event that fewer than 10,000 Entries are received at the end of the Campaign Period, Sponsor will make a minimum donation of $10,000 donation in one sum. </p>

<p><b>11.	WINNER SELECTION PROCESS</b></p>

<p>On May 7, 2012 (the �<b>Draw Date</b>�) in Mississauga, Ontario at approximately 10:00 a.m. ET, two hundred and fifty (250) eligible entrants will be selected by random draw from among all eligible Entries received during the Campaign Period.  The odds of winning depend on the number of eligible Entries received during the Campaign Period. </p>

<p>BEFORE BEING DECLARED A CONFIRMED PRIZE WINNER, each selected entrant must have correctly answered the mathematical skill-testing question appearing on the Entry Form without mechanical or other aid.  Further, by participating in this Campaign and accepting a Prize, you hereby: (i) confirm compliance with these Rules; (ii) acknowledge acceptance of the Prize as awarded; (iii) release the Releasees (defined above) from any and all liability in connection with this Campaign, his/her participation therein and/or the awarding and use/misuse of the Prize or any portion thereof; and (iv) agrees to the publication, reproduction and/or other use of his/her name, address, voice, statements about the Campaign and/or photograph or other likeness without further notice or compensation, in any publicity or advertisement carried out by or on behalf of the Sponsor in any manner whatsoever, including print, broadcast or the internet.  If a selected entrant: (a) failed to correctly answer the skill-testing question appearing on the Entry Form; and/or (b) cannot accept a Prize for any reason; then he/she will be disqualified (and will forfeit all rights to a Prize) and the Sponsor reserves the right, in its sole and absolute discretion, to randomly select an alternate eligible entrant from among the remaining eligible Entries (in which case the foregoing provisions of this section shall apply to such new selected entrant).  </p>

<p>The Sponsor or its designated representative will make one (1) attempt to send the each confirmed winner his/her Prize. If there is a return of any notification as undeliverable; then he/she will forfeit all rights to a Prize and the Sponsor reserves the right, in its sole and absolute discretion, to randomly select an alternate eligible entrant from among the remaining eligible Entries (in which case the foregoing provisions of this section shall apply to such new selected entrant).  </p>

<p><b>12.	GENERAL CONDITIONS</b></p>

<p>All Entries become the property of the Sponsor. The decisions of the Sponsor with respect to all aspects of this Campaign are final and binding on all entrants without right of appeal, including, without limitation, any decisions regarding the eligibility/disqualification of Entries and/or entrants.</p>

<p>The Releasees will not be liable for: (i) any failure of the Website or Application during the Campaign; (ii) any technical malfunction or other problems relating to the telephone network or lines, computer on-line systems, servers, access providers, computer equipment or software; (iii) the failure of any Entry to be received for any reason, including, but not limited to, technical problems or traffic congestion on the Internet or at any website; (iv) any injury or damage to an entrant�s or any other person�s computer or other device related to or resulting from participating or downloading any material in the Campaign; and/or (v) any combination of the above. </p>

<p>In the event of a dispute regarding who submitted an Entry, Entries will be deemed to have been submitted by the authorized account holder of the email address submitted at the time of entry.  �Authorized account holder� is defined as the person who is assigned an email address by an internet provider, online service provider, or other organization (e.g. business, educational institute, etc.) that is responsible for assigning email addresses for the domain associated with the submitted email address. An entrant may be required to provide proof (in a form acceptable to the Sponsor � including, without limitation, government issued photo identification) that he/she is the authorized account holder of the email address associated with the selected Entry.</p>

<p>The Sponsor reserves the right, in its sole and absolute discretion subject only to the approval of the R�gie des alcools, des courses et des jeux (the �<b>R�gie</b>�) in Quebec, to withdraw, amend or suspend this Campaign (or to amend these Rules) in any way, in the event of an error, technical problem, computer virus, bugs, tampering, unauthorized intervention, fraud, technical failure or any other cause beyond the reasonable control of the Sponsor that interferes with the proper conduct of this Campaign as contemplated by these Rules.  Any attempt to deliberately damage any website or to undermine the legitimate operation of this Campaign in any way (as determined by Sponsor in its sole and absolute discretion) is a violation of criminal and civil laws and should such an attempt be made, the Sponsor reserves the right to seek remedies and damages to the fullest extent permitted by law.  The Sponsor reserves the right, in its sole and absolute discretion subject only to the approval of the R�gie in Quebec, to cancel, amend or suspend this Campaign, or to amend these Rules, without prior notice or obligation, in the event of any accident, printing, administrative, or other error of any kind, or for any other reason. </p>

<p>The Sponsor reserves the right, in its sole and absolute discretion, and without prior notice, to adjust any of the dates and/or timeframes stipulated in these Rules, to the extent necessary, for purposes of verifying compliance by any entrant or Entry with these Rules, or as a result of technical problems, or in light of any other circumstances which, in the opinion of the Sponsor, in its sole and absolute discretion, affect the proper administration of the Campaign as contemplated in these Rules.</p>

<p>In the event of any discrepancy or inconsistency between the terms and conditions of these Rules and disclosures or other statements contained in any Campaign-related materials, including, but not limited to: the Entry Form, Website, Application and/or point of sale, television, print or online advertising; the terms and conditions of these Rules shall prevail, govern and control.</p>

<p>For Quebec residents:  Any litigation respecting the conduct or organization of a publicity contest may be submitted to the R�gie des alcools, des courses et des jeux for a ruling.  Any litigation respecting the awarding of the prize may be submitted to the R�gie only for the purpose of helping the parties reach a settlement. </p>

<p><b>13.	PERSONAL DATA</b></p>

<p>Kao Canada Inc. is collecting personal data about participants for the purpose of administering this Campaign.  Kao Canada Inc. is not authorized to use your personal information in any way other than the intended use and is subject to applicable Canadian Federal and Provincial privacy legislation. Please see the Kao Canada Inc. Privacy Policy at 
    <a href="http://www.kaobrands.com/privacy_policy.asp">http://www.kaobrands.com/privacy_policy.asp</a> for information on its policy towards maintaining the privacy and security of user information. </p>

<p><b>MINI-RULES:</b></p>
<p>No purchase necessary.  Closes 11:59 pm on April 30, 2012.  Open to individual residents of Canada who have reached the age of majority.  Enter online and full rules at: www.facebook.com/BioreCanada.  250 prizes available to be won each consisting of one (1) 200mL Bior� Combination Skin Cleanser (ARV: $9.99 each).  Skill-testing question required.  Odds depend on number of eligible entries.</p>



</div>
</body>

</html>
