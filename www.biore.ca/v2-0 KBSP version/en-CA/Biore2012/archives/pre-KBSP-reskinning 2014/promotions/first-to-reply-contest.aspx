<!DOCTYPE html>
<!--[if IE 7 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie7 ie"><![endif]-->
<!--[if IE 8 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie8 ie"><![endif]-->
<!--[if IE 9 ]><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" class="ie9 ie"><![endif]-->
<!--[if !IE]><!--><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" ><!--<![endif]-->
<head>
<title>
	Bior� First to Reply Contest
</title>
    
    <link rel="stylesheet" type="text/css" href="/en-CA/css/global.css" />
<style type="text/css">
#mainbody{width:80%; margin-left:auto;margin-right:auto;}
h1 {padding:10px 0px 10px 0px; color:#005daa; font-size:14px;}
p {padding-bottom:20px}
</style>
</head>

<body>
<div id="mainbody">
<div id="languageToggle"><a href="/fr-CA/promotions/first-to-reply-contest.aspx">Fran�ais</a></div>
	<img src="/en-CA/images/promotions/rules.jpg" border="0" usemap="#Map">
    <map name="Map">
      <area shape="rect" coords="91,233,384,312" href="#rules">
    </map>
<p>*No purchase necessary. Closes 12:00 p.m. ET or whenever 50 eligible winners have submitted entries, the first Monday of each month, March-December 2012. Open to legal residents of Canada (age of majority) excluding Atlantic Canada, Nunavut, Yukon and Northwest Territories. Enter by emailing name, phone number, and response to Monday Movies first to reply post to bioreskincarelistens@biore.ca. Full rules BELOW. Maximum of 50 prizes available per month consisting of 2 adult-admission movie tickets valid at participating Cineplex theaters (ARV: $24.00 each).  Skill-testing question required.  Odds depend on number of eligible entries.</p>
   <h1>BIORE� SKINCARE MONDAY MOVIES, 2012 FIRST TO REPLY CONTEST (�CONTEST�)</h1>

<a name="rules"></a>
<h3>FULL RULES</h3>
<br>
<p><b>1.	CONTEST PERIOD:</b>  
Contest begins on<b> the first Monday of each month</b>, from March 2012 through December, 2012, beginning at 10:00:01 a.m. Eastern Time (ET) and ending at 11:59:59 p.m. ET, or whenever <b>50</b> eligible winners have been determined in accordance with these Official Rules and Regulations (the �<b>Rules</b>�), whichever occurs first (the �<b>Contest Period</b>�).  </p>

<p><b>2.	ELIGIBILITY: </b>
Contest is open to all legal residents of Canada (Excluding Atlantic Canada or Nunavut, Yukon and Northwest Territories) who have reached the age of majority in their province/territory of residence at the time of entry, except employees, representatives or agents (and those with whom such persons are domiciled, whether related or not) of Kao Canada Inc. (the �<b>Sponsor</b>�), its parent companies, subsidiaries, affiliates, prize suppliers, advertising/promotion agencies and the Contest judges (collectively, the �<b>Contest Parties</b>�).    </p>  

<p><b>3.	THE PRIZE: </b>
There are fifty (50) prizes available to be won, each consisting of two (2) adult-admission movie tickets (approximate retail value of $24 per prize) valid at participating Cineplex Entertainment LP (�Cineplex�) theatres (each, the <b>�Prize�</b>).  Please allow 4 to 6 weeks after being declared and confirmed a winner in accordance with these Rules to receive (via mail) your Prize. Sponsor reserves the right to substitute any Prize or a component thereof for any reason with a prize or a prize component of equal or greater value. The Prize must be accepted as awarded and is not transferable.  No substitutions except at Sponsor�s option.  The Prize will only be awarded to the person whose verifiable full name and valid email address appears on the Entry (defined below). All expenses not specifically listed herein are the responsibility of winners.</p>

<p><b>4.    HOW TO ENTER:</b>  
NO PURCHASE NECESSARY.  To enter, send an email (the �<b>Email</b>�) to bioreskincarelistens@biore.ca that contains: (i) your full name and telephone number and (ii) the response to the Monday Movies first to reply post made on the first Monday of the month at 10:00:01 a.m. Eastern Time (ET) on the Bior� Canada facebook page <a href="http://www.facebook.com/biorecanada" target="_blank">www.facebook.com/biorecanada</a>. To be eligible, your Email must be sent and received during the Contest Period (Email and Response hereinafter collectively referred to as <strong>�Entry�</strong>). NOTE: RESPONSES WILL NOT BE JUDGED. </p>

<p>There is a limit of one (1) Entry per person/email address permitted during the Contest Period.   For greater certainty, you can only use one (1) email address to enter, or otherwise participate in, the Contest.  If it is discovered that any person has attempted to: (i) obtain more than one (1) Entry per person/email address during the Contest Period; or (ii) use more than one (1) email address to enter, or otherwise participate in, the Contest; then (in the sole and absolute discretion of the Sponsor) he/she may be disqualified from the Contest and all of his/her Entries are subject to disqualification by the Sponsor. Your Entry will be rejected if (in the sole and absolute discretion of the Sponsor (i) your Email is not sent and received during the Contest Period.  Use of any automated, script, macro, robotic or other program(s) to enter or otherwise participate in this Contest is prohibited and is grounds for disqualification by the Sponsor.  The Releasees (defined below) are not responsible for late, lost, misdirected, delayed, incomplete or incompatible Entries.  </p>

<p>All Entries are subject to verification.  The Sponsor reserves the right, in its sole and absolute discretion, to require proof of identity and/or eligibility (in a form acceptable to the Sponsor) to participate in this Contest.  Failure to provide such proof in a timely manner may result in disqualification.  The sole determinant of the time for the purposes of a valid Entry in this Contest will be the Contest server machine(s).</p>

<p><b>5. 	WINNER DETERMINATION PROCESS:</b>
Each of the first fifty (50) eligible entrants to submit an Entry in accordance with these Rules will be eligible to win a Prize (subject to compliance with these Rules). The odds of winning depend the time that each eligible Entry is received during the Contest Period and the number of Entries received.  
The Sponsor or its designated representative will make a maximum of three (3) attempts to contact each eligible winner by email or phone (using the email address used to send the Entry and phone number provided in the Entry) within five (5) business days of submission of the corresponding Entry.  If an eligible winner cannot be contacted within three (3) attempts or five (5) business days of submission of the corresponding Entry (whichever occurs first), or if there is a return of any notification as undeliverable; then he/she will be disqualified (and will forfeit all rights to the Prize) and the Sponsor reserves the right, in its sole and absolute discretion, to select the next eligible entrant from among the remaining eligible Entries (in which case the foregoing provisions of this section shall apply to such new eligible winner).</p>  

<p>Before being declared a Confirmed winner, each eligible winner will be required to: (a) correctly answer a mathematical skill-testing question without mechanical or other aid; and (b) sign and return within ten (10) business days of notification the Sponsor�s declaration and release form, which (among other things): (i) confirms compliance with these Rules; (ii) acknowledges acceptance of the Prize as awarded; (iii) releases the Contest Parties and each of their respective officers, directors, agents, representatives, successors and assigns (collectively, the �Releasees�) from any and all liability in connection with this Contest, his/her participation therein and/or the awarding and use/misuse of the Prize or any portion thereof; and (iv) agrees to the publication, reproduction and/or other use of his/her name, address, voice, statements about the Contest and/or photograph or other likeness without further notice or compensation, in any publicity or advertisement carried out by the Sponsor in any manner whatsoever, including print, broadcast or the internet.  If an eligible winner: (a) fails to correctly answer the skill-testing question; (b) fails to return the properly executed declaration and release within the specified time; and/or (c) cannot accept the Prize for any reason; then he/she will be disqualified (and will forfeit all rights to the Prize) and the Sponsor reserves the right, in its sole and absolute discretion, to select the next eligible entrant from among the remaining eligible Entries (in which case the foregoing provisions of this section shall apply to such new eligible winner).  </p>

<p><b>6.	GENERAL:  </b>
All Entries become the property of the Sponsor.  The Releasees assume no responsibility for lost, delayed, incomplete, incompatible or misdirected Entries.  This Contest is subject to all applicable federal, provincial and municipal laws.  By entering the Contest, participants agree to be bound by these Official Contest Rules and Regulations (the <strong>�Contest Rules�</strong>) and by the decisions of the Sponsor with respect to all aspects of this Contest, which are final and binding on all entrants without right of appeal, including, without limitation, any decisions regarding the eligibility/disqualification of Entries and/or entrants.</p>

<p>The Releasees will not be liable for: (i) any failure of the Website during the Contest; (ii) any technical malfunction or other problems relating to the telephone network or lines, computer on-line systems, servers, access providers, computer equipment or software; (iii) the failure of any Entry to be received by the Contest Parties for any reason including, but not limited to, technical problems or traffic congestion on the Internet or at any website; (iv) any injury or damage to an entrant�s or any other person�s computer or other device related to or resulting from participating or downloading any material in the Contest; and/or (v) any combination of the above.  </p>

<p>In the event of a dispute regarding who submitted an Entry, Entries will be deemed to have been submitted by the authorized account holder of the email address submitted at the time of entry.  �Authorized account holder� is defined as the person who is assigned an email address by an internet provider, online service provider, or other organization (e.g. business, educational institute, etc.) that is responsible for assigning email addresses for the domain associated with the submitted email address. An entrant may be required to provide proof that he/she is the authorized account holder of the email address associated with a selected Entry.</p>

<p>All Entries are subject to verification.  The Sponsor reserves the right, in its sole and absolute discretion, to require proof of identity and/or eligibility (in a form acceptable to the Sponsor � including, without limitation, government issued photo identification) to participate in this Contest.  Failure to provide such proof in a timely manner may result in disqualification.  The sole determinant of the time for the purposes of a valid Entry in this Contest will be the Contest server machine(s).</p>

<p>The Sponsor reserves the right, in its sole and absolute discretion, and without prior notice, to adjust any of the dates and/or timeframes stipulated in these Contest Rules, to the extent necessary, for purposes of verifying compliance by any entrant or Entry with these Contest Rules, or as a result of technical problems, or in light of any other circumstances which, in the opinion of the Sponsor, in its sole and absolute discretion, affect the proper administration of the Contest as contemplated in these Contest Rules.</p>

<p>The Sponsor reserves the right, in its sole and absolute discretion, to withdraw, amend or suspend this Contest (or to amend these Contest Rules) in any way, in the event of an error, technical problem, computer virus, bugs, tampering, unauthorized intervention, fraud, technical failure or any other cause beyond the reasonable control of the Sponsor that interferes with the proper conduct of this Contest as contemplated by these Contest Rules.  Any attempt to deliberately damage any website or to undermine the legitimate operation of this Contest is a violation of criminal and civil laws and should such an attempt be made, the Sponsor reserves the right to seek remedies and damages to the fullest extent permitted by law.  The Sponsor reserves the right, in its sole and absolute discretion, to cancel, amend or suspend this Contest, or to amend these Contest Rules, without prior notice or obligation, in the event of any accident, printing, administrative, or other error of any kind, or for any other reason. </p>

<p>By completing the entry form, all entrants consent to the collection, use and distribution of their personal information by the Sponsor only for the purposes of running the Contest (information is stored for approximately one year).  Sponsor will not sell or transmit this information to third parties except for the purposes of administering the Contest.  For information on use of personal information in connection with this Contest, see the privacy policy posted at <a href="http://www.biore.ca/en-CA/privacy/" target="_blank">http://www.biore.ca/en-CA/privacy/</a>.</p>

<p>The terms of this Contest, as set out in these Contest Rules, are not subject to amendment or counter-offer, except as set out herein.</p>

<p>All intellectual property used by the Sponsor in connection with the promotion and/or administration of the Contest, including, without limitation, all trade-marks, trade names, logos, designs, promotional materials, web pages, source code, drawings, illustrations, slogans and representations are owned (or licensed, as the case may be) by the Sponsor and/or its affiliates. All rights are reserved. Unauthorized copying or use of any such intellectual property without the express written consent of its owner is strictly prohibited. </p>

<p>In the event of any discrepancy or inconsistency between the terms and conditions of these Contest Rules and disclosures or other statements contained in any Contest-related materials, including, but not limited to: the Contest entry form and/or point of sale, television, print or online advertising; the terms and conditions of these Contest Rules shall prevail, govern and control.</p>

<p><b>7.</b>	The Sponsor of this contest is Kao Canada Inc., 60 Courtneypark Drive West, Unit 5, Mississauga, ON L5W 0B3.</p>

<p><b>8.</b>	This Promotion is in no way sponsored, endorsed or administered by, or associated with, Facebook.  You understand that you are providing your information to the Sponsor and not to Facebook.  The information you provide will be used only for entry and prize fulfillment in connection with this Contest, and will remain as content on the Website.  Any questions, comments or complaints regarding the Contest must be directed to Sponsor and not Facebook. </p>

</div>
</body>

</html>
