﻿<%@ Page Title="Special Offers, New Products &amp; Skincare Tips | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.new_products_and_offers._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Skincare tips, special offers and new products from Bioré® Skincare" name="description" />
    <meta content="skincare tips, take care of your skin, new product" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/whatsNew.css")
        .Render("~/css/combinednew_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="intro">
                <h1>What's New</h1>
                <p>Our latest and greatest products and news.</p>
            </div>
            <div id="promos">
                <ul>
                    <li id="promo2" class="promo">
                        <div class="promoImage"><img src="../images/whatsNew/promo2Img.png" alt="" /></div>
                        <div class="promoContent">
                            <h3 class="promoHeader"><span class="new">NEW</span> Acne Clearing Scrub</h3>
                            <p class="promoBody">Get clearer, healthier-looking skin in just two days.</p>
                            <div class="promoButton"> <a href="../complexion-clearing-products/acne-clearing-scrub">Take a look <span class="arrow">&rsaquo;</span></a></div>
                        </div>
                    </li>
                    <li id="promo1" class="promo">
                        <div class="promoImage"><img src="../images/whatsNew/promo1Img.png" alt="" /></div>
                        <div class="promoContent">
                            <h3 class="promoHeader">What's Trending?</h3>
                            <p class="promoBody">We've made some exciting changes for 2012! </p>
                            <div class="promoButton"><a href="../clean-new-look">Learn More <span class="arrow">&rsaquo;</span></a></div>
                        </div>
                    </li>
                    <%--<li id="promo2" class="promo">
                        <div class="promoImage"><img src="../images/whatsNew/promo2Img.png" alt="" /></div>
                        <div class="promoContent">
                            <h3 class="promoHeader">Get Rewarded</h3>
                            <p class="promoBody">Build up Prove It!&trade; Reward Points on Facebook towards free products and more.</p>
                            <div class="promoButton"> <asp:HyperLink ID="proveItPromoLink" NavigateUrl="http://www.facebook.com/bioreskin?sk=app_205787372796203" runat="server" Target="_blank">Start Earning <span class="arrow">&rsaquo;</span></asp:HyperLink></div>
                        </div>
                    </li>--%>
                    <li id="promo3" class="promo">
                        <div class="promoImage"><img src="../images/whatsNew/promo3Img.png" alt="" /></div>
                        <div class="promoContent">
                            <h3 class="promoHeader"><span class="new">NEW</span> Combination Skin Balancing Cleanser</h3>
                            <p class="promoBody">Help bring some balance to your life with this brand new cleanser.</p>
                            <div class="promoButton"><a href="../deep-cleansing-products/combination-skin-balancing-cleanser">Take a Look <span class="arrow">&rsaquo;</span></a></div>
                        </div>
                    </li>
                </ul>
            </div>
            <!--<div id="polaroids">
                <img src="../images/whatsNew/whatsNewPolaroids.jpg" alt="" />
            </div-->
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
