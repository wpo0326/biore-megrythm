﻿<%@ Page Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="form.aspx.cs" Inherits="Biore2012.FormConfig.form" %>
<%@ Register TagPrefix="uc1" TagName="ucForm" Src="~/Controls/ucFormConfig.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <%#SquishIt.Framework.Bundle .Css()
        .Add("~/css/formConfig.css")
        .Render("~/css/combinedform_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <uc1:ucForm ID="ucForm" runat="server" />
                <div class="clear"></div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
<script type="text/javascript">
    $(function() {
        if ($(".signUp").length) {
            toggle("noneToggle1");
            toggle("noneToggle2");
            toggle("noneToggle3");
            toggle("noneToggle4");
        }
    });
    function toggle(className) {
        var checkToggle = $("." + className + " input:last");
        checkToggle.click(function() {
            if ($("#" + checkToggle.attr('id') + ":checked").val()) {
                $("." + className + " input").each(function() {
                    if ($(this).attr('id') != checkToggle.attr('id')) {
                        $(this).attr("checked", false);
                        $(this).attr('disabled', 'disabled');
                    }
                });
            } else {
                $("." + className + "  input").each(function() {
                    if ($(this).attr('id') != checkToggle.attr('id')) {
                        $(this).removeAttr('disabled');
                    }
                });
            }
        });
    }
</script>
</asp:Content>