﻿<%@ Page Title="Bior&eacute;&reg; Skincare | Rules" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SampleFormTerms.aspx.cs" Inherits="Biore2012.FormConfig.SampleTerms" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Bioré® rules" name="description" />
    <meta content="" name="keywords" />
 <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedrules_#.css")
    %>
    <style type="text/css">

        

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <div id="content-sample-form-terms">
                    <h2>The Bioré® Charcoal Try-it-First Sample Pack Giveaway Terms and Conditions (the “Terms”).<br />PLEASE READ CAREFULLY.</h2>

                    <p><b>This giveaway is open to Canadian residents only and is GOVERNED by Canadian law.</b></p>

                    <p><b>A.  Overview</b></p>

                    <p>The Bioré® Charcoal Try-it-First Sample Pack Giveaway (the “Giveaway”) is sponsored by Kao Canada Inc. (the “Sponsor”). No purchase is necessary for this Giveaway.  The information you provide will only be used for the administration of this Giveaway and in accordance with the Sponsor’s privacy policy (see below). Any questions, comments or complaints regarding the Giveaway must be directed to the Sponsor.  You may only use one account (the “Account”) to participate in this Giveaway.  No purchase is required. However, online access to the Internet and an Account are required to participate in the Giveaway. Many public libraries, retail businesses and others provide free computer and Internet access, and certain Internet service providers or other companies offer free e-mail accounts. </p>

                    <p><b>B.  Eligibility</b></p>

                    <p>Giveaway is open only to legal residents of Canada who have reached the age of majority in their province/territory of residence at the time of entry, except employees, representatives or agents (and those with whom such persons are domiciled, whether related or not) of Sponsor, its parent companies, subsidiaries, affiliates, advertising/promotional agencies, representatives or agents (collectively the “Giveaway Parties”).  For greater certainty and the avoidance of any doubt, no person (or any person with whom that person lives, whether related or not) who has been employed by any of the Giveaway Parties at any point since January 1, 2014 is eligible for this Giveaway.  Any attempted violation of this eligibility rule may, in the sole and absolute discretion of the Sponsor, result in an entrant’s disqualification from the Giveaway.  The Giveaway is intended for individuals only.  Corporations, associations or other groups may not participate in the Giveaway.  </p>

                    <p><b>C.  Duration of the Giveaway</b></p>

                    <p>The Giveaway will start at 12:00 p.m. Eastern Time (“ET”) on January 28, 2014 and is scheduled to end at 5:00 p.m. ET on February 7, 2014, or whenever official supplies of the Sample Packs (defined below) are exhausted, whichever occurs first (the “Giveaway Period”). </p>

                    <p><b>D.  How to Participate</b></p>

                    <p>NO PURCHASE NECESSARY. To participate in the Giveaway, you must visit the http://www.biore.ca/en-CA/formconfig/sampleform.aspx (the “Webpage”) during the Giveaway Period and follow the on-screen instructions to request the Sample Pack by fully completing the online redemption form (“Redemption Form”), including your First Name, Last Name, Date of Birth, Address, City, Province/Territory, Postal Code, and your E-mail Address. Once you have fully completed the Redemption Form with all required information, follow the on-screen instructions to submit your completed form (“Redemption”). To be eligible, your Redemption must be submitted and received within the Giveaway Period.</p>

                    <p><b>E.  Redemption Request Limit</b></p>

                    <p>There is a limit of one (1) Redemption per person/email address/Account permitted during the Giveaway Period.  For greater certainty, you can only use one (1) email address/Account to participate in the Giveaway.  If it is discovered that any person has attempted to: (i) obtain more than one (1) Redemption Form per person/email address/Account during the Giveaway Period; and/or (ii) use (or attempt to use) multiple names, identities and/or more than one (1) email address or Account to participate in the Giveaway; then (in the sole and absolute discretion of the Sponsor) he/she may be disqualified from the Giveaway and all of his/her Redemptions voided.  Use (or attempted use) of multiple names, identities, email addresses and/or any automated, macro, script, robotic or other system(s) or program(s) to participate in or disrupt this Giveaway is prohibited and is grounds for disqualification by the Sponsor. The Releasees (defined below) are not responsible for late, lost, misdirected, delayed, incomplete or incompatible Redemption Requests (all of which are void).</p>     
 
                    <p><b>F.  Sample Packs</b> </p>

                    <p>There are a maximum of five thousand (5,000) Sample Packs available during the Giveaway Period, each consisting of one (1) 7g Bioré® Self-Heating One Minute Mask with natural Charcoal packette and  one (1) 7mL Bioré® Deep Pore Charcoal Cleanser packette (collectively, the “Sample Pack”).  Each Sample Pack has an approximate retail value of  $2 CAN. </p> 

                    <p><b>G.  Delivery of Sample Packs</b></p>

                    <p>Once confirmed as a Recipient of a Sample Pack, your Sample Pack will be mailed to the address provided on the Redemption Form.  Please allow four (4) to six (6) weeks for your Sample Pack to arrive to the address you provided on the Redemption Form.  If you haven’t received your Sample Pack and it has been at least eight (8) weeks from the date that you were confirmed a Recipient in accordance with these Terms, then you can email bioreskincarelistens@biore.ca to let us know. Lost or stolen Sample Packs will not be replaced.</p>

                    <p><b>H. General Conditions</b></p>

                    <p>The following general conditions apply to this Giveaway:</p>

                    <ol>
                        <li>Void where prohibited. The Giveaway is void where prohibited, taxed, regulated or where registration is required. All federal, provincial and local laws and regulations apply.</li>
                        
                        <li>The Sponsor reserves the right to cancel, suspend or modify the Giveaway if fraud, virus or other technical problems or any other factors in any way interfere or corrupt the administration of the Giveaway.</li>
                        
                        <li>Any use of automated or programmed methods of effecting Redemption online is prohibited.</li>
                        
                        <li>The Sponsor is not responsible for communication errors, viruses, worms or other contaminants that may affect or damage a participant’s computer system or data while participating in the Giveaway.</li>
                        
                        <li>Except where prohibited, by completing the Redemption Form, participant releases and holds harmless the Sponsor or subsidiaries, affiliates and their shareholders, directors, officers, employees, representatives or agents, be responsible or liable for any damages or losses of any kind, including direct, indirect, incidental, consequential or punitive damages arising out of the Giveaway.</li>
                        
                        <li>By entering this Giveaway, each entrant expressly consents to the Sponsor, its agents and/or representatives, storing, sharing and using the personal information submitted with his/her Redemption Form only for the purpose of administering the Giveaway and in accordance with Sponsor’s privacy policy (available at: http://www.kaobrands.com/privacy_policy.asp), unless the participant otherwise agrees.</li>
                        
                        <li>Except where prohibited, by completing the Redemption Form, participants agree that the Giveaway Parties are not responsible for: (a) any incorrect or inaccurate information, whether caused by the participant or printing errors; (b) unauthorized human intervention in any part of the Giveaway; (c) technical or human error which may occur in the administration of the Giveaway or the processing of Redemptions; or (d) any injury or damage to persons or property which may be caused, directly or indirectly, in whole or in part, from the participants participation in the Giveaway or receipt, use or misuse of any Sample Pack.</li>
                        <li>The Giveaway is intended for individuals only.  Corporations, associations or other groups may not participate in the Giveaway.  It is fraudulent for any individual, company, association or group to direct, encourage, or allow individuals to use a single account for the purpose of accumulating in Giveaway Redemption for combined use.</li>
                    </ol>


                    <p><b>I. Other</b></p>

                    <p>In the event of any discrepancy or inconsistency between these English terms and conditions and disclosures or other statements contained in any Giveaway-related materials, including, but not limited to: Redemption Form, Webpage, and the French version of these Rules, and/or point of sale, television, print or online advertising; these English terms and conditions shall prevail, govern and control.</p>


                   <p><b>Mini Terms</b></p>

                    <p>†No purchase necessary. Giveaway runs from January 28th, 2014 at 12:00 p.m. ET to February 7th, 2014 at 5:00 p.m. ET or whenever official supplies of Sample Packs are exhausted, whichever occurs first.   Open to legal residents of Canada (age of majority). Enter online and full rules at: http://www.biore.ca/en-CA/formconfig/sampleform.aspx.   Maximum of five thousand (5,000) Sample Packs available (ARV: $2 each).  </p>


                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
