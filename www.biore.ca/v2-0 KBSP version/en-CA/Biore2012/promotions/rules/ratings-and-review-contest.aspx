﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="ratings-and-review-contest.aspx.cs" Inherits="Biore2012.promotions.rules.ratings_and_review_contest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedrules_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <!--<div id="polaroid"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/forms/signUpPhoto.jpg") %>" alt="" /></div>-->
                <div id="content">
                    <h1>Bior&eacute;<sup>&reg;</sup> Skincare Ratings and Reviews Contest </h1>

                    <p><strong>1.	THE CONTEST:</strong>  The Bioré® Skincare Ratings and Reviews Contest (the “<strong>Contest</strong>”) is being conducted by Kao Canada Inc. (hereinafter referred to as the “<strong>Sponsor</strong>”).  No purchase necessary.  The Contest begins at 12:00:01 AM (EST) on September 1, 2014 and closes at 11:59:59 PM (EST) on September 30, 2014 (hereafter referred to as the “<strong>Contest Period</strong>”).  The Contest is open to legal residents of Canada who have reached the age of majority in their province/territory of residence at the time of entry.  Employees, representatives and agents (and persons living in the same household as such persons, whether related or not) of the Sponsor, its affiliates, subsidiaries, parent or related companies, its advertising or promotional agencies (collectively, the “<strong>Contest Parties</strong>”) are not eligible to participate in this Contest.</p>
                    <p>&nbsp;</p>

                    <p><strong>2.	THE GRAND PRIZE:  </strong>There is one (1) Grand Prize available to be won.  The Grand Prize consists of a Bioré product gift basket based on the winner’s skin type (Approx. retail value of $150) and 52 single admission tickets to Cineplex theaters (Approx. retail value of $450). Total prize value of $ 600CDN. Please allow 4 to 6 weeks after being declared the winner in accordance with these Contest Rules to receive (via courier) your Grand Prize. Grand Prize must be accepted as awarded and is not transferable, assignable or convertible to cash.  No substitutions except at Sponsor’s option.  Sponsor reserves the right, in its sole and absolute discretion, to substitute the Grand Prize or a component thereof with a prize of equal or greater value, including, without limitation, but at Sponsor’s sole and absolute discretion, a cash award. </p>
                    <p>&nbsp;</p>

                    <p><strong>3.	HOW TO ENTER:  </strong>There are two (2) ways to enter the Contest.  <strong>NO PURCHASE NECESSARY. </strong>  </p>
                    <p>&nbsp;</p>

                    <div style="margin-left:50px">
                        <p>a.	To enter the Contest, visit www.biore.ca (the “<strong>Website</strong>”) and follow the on-screen instructions to: (i) post a rating and/or review (the “<strong>Review(s)</strong>”) of any of the Bioré® Skincare products in the space provided; (ii) fully complete and submit the entry form; and (iii) check off that you have read, accept and agree to be legally bound by these Contest Rules.  To be eligible, your Review must be (i) at least <strong>50 characters</strong>  or more in length; (ii) written in English or French; and (iii) written in the space provided and otherwise in accordance with the specific Review Requirements listed below in Rule 5. Once you have completed your Review and entry form, follow the on-screen instructions to submit your completed entry (the “<strong>Entry</strong>”).  All eligible Entries received during the Contest Period will be entered into the random prize draw (see Rule 6).  Your Entry will be rejected if (in the sole and absolute discretion of the Sponsor): (i) the entry form (including the Review) is not fully completed and submitted during the Contest Period; and/or (ii) the Review does not conform to the specific Review Requirements listed below in Rule 5.  Use of any automated, script, macro, robotic or other program(s) to enter or otherwise participate in this Contest is prohibited and is grounds for disqualification by the Sponsor.  The Releasees (defined below) are not responsible for late, lost, misdirected, delayed, incomplete or incompatible Entries.  </p>
                        <p>&nbsp;</p>

                        <p>b.	To obtain one (1) Entry in the Contest without writing a Review, print your first name, last name, telephone number, complete mailing address, age and signature on a plain white piece of paper and mail it (in an envelope with sufficient postage) along with a 50 characters or more unique and original essay on Why I Love Bioré® Skincare to:  Bioré® Skincare Ratings and Reviews Contest, Kao Canada Inc., 75 Courtneypark Drive West, Unit 2, Mississauga, ON, L5W 0E3.  Upon receipt of your mail-in request in accordance with these Contest Rules, you will receive one (1) Entry in the Contest. To be eligible, all mail-in Entries must be post marked during the Contest Period and received by no later than October 3, 2014. The Releasees (defined below) take no responsibility for any lost, stolen, delayed, illegible, damaged, misdirec3ed, late or destroyed mail-in no purchase necessary Entries.</p>
                        <p>&nbsp;</p>
                    </div>

                    <p><strong>4.	ENTRY LIMIT: </strong>There is a limit of one (1) Entry per person during the Contest Period, regardless of the method of Entry.  Entrants may use only one (1) name to enter and any Entries generated by script, macro, robotic, programmed, or any other automated means or by any other means which in the opinion of the Sponsor, in its sole and absolute discretion, subvert the entry process are void.  If it is discovered that any person has attempted to submit one or more Entries otherwise than in accordance with these Rules, all Entries submitted by that person are subject to disqualification in the sole and absolute discretion of the Sponsor.  In the event of a dispute as to who submitted an Entry, it will be in the sole and absolute discretion of the Sponsor (using whatever evidence is available to the Sponsor) to determine which Entrant, if any, is eligible to win a Prize. Proof of transmission (screenshots or captures etc) does not constitute proof of delivery.</p>
                    <p>&nbsp;</p> 

                    <p>All Entries are subject to verification at any time.  The Sponsor reserves the right, in its sole and absolute discretion, to require proof of identity and/or eligibility (in a form acceptable to the Sponsor – including, without limitation, government issued photo identification) to participate in this Contest.  Failure to provide such proof to the satisfaction of the Sponsor in a timely manner may result in disqualification.  The sole determinant of the time for the purposes of a valid Entry in this Contest will be the Contest server machine(s).</p>
                    <p>&nbsp;</p>

                    <p>
                        <strong>5.	REVIEW REQUIREMENTS: </strong>
                    </p>
                    <p>
                        <br /><br /> 

                    BY SUBMITTING A REVIEW, YOU AGREE THAT THE REVIEW COMPLIES WITH ALL CONDITIONS STATED IN THESE CONTEST RULES. THE RELEASEES (DEFINED BELOW) WILL BEAR NO LEGAL LIABILITY REGARDING THE USE OF YOUR REVIEW BY THE CONTEST PARTIES AND THE RELEASEES (DEFINED BELOW) SHALL BE HELD HARMLESS BY YOU IN THE EVENT IT IS SUBSEQUENTLY DISCOVERED THAT YOU HAVE DEPARTED FROM OR NOT FULLY COMPLIED WITH ANY OF THESE CONTEST RULES.
                    </p>
                    <p>
                        &nbsp;</p> 

                    <p>To be eligible for entry in this Contest, your Review must be: (i) at least <strong>50 characters</strong> or more in length; (ii) written in English or French; and (iii) written in the space provided. By participating in the Contest, each entrant agrees to be bound by these Contest Rules and by the interpretation of these Contest Rules by the Sponsor, and further warrants and represents that his/her Review: </p>
                    <p>&nbsp;</p>

                    <div style="margin-left:50px">
                    i.	is original to him/her and that the entrant has all necessary rights in and to the Review to enter the Contest;<br /><br />

                    ii.	does not violate any law, statute, ordinance or regulation;<br /><br />

                    iii.	does not contain any reference to or likeness of any identifiable third parties, unless consent has been obtained from all such individuals and their parent/legal guardian if they are under the age of majority in their jurisdiction of residence;<br /><br />

                    iv.	will not give rise to any claims of infringement, invasion of privacy or publicity, or infringe on any rights and/or interests of any third party, or give rise to any claims for payment whatsoever; and<br /><br />
 
                    v.	is not defamatory, trade libelous, pornographic or obscene, and further that it will not discuss or involve, without limitation, any of the following: nudity; explicit, graphic or sexual activity or sexual innuendo; crude, vulgar or offensive language and/or symbols; derogatory characterizations of any ethnic, racial, sexual or religious groups; content that endorses, condones and/or discusses any illegal, inappropriate or risky activity, behaviour or conduct; personal information of individuals, including, without limitation, names and addresses (physical or electronic); conduct or other activities in violation of these Rules; commercial messages, comparisons or solicitations for products or services other than those of Sponsor; any identifiable third party products and/or trade-marks, brands or logos, other than those of Sponsor; and/or any other content that is or could be considered inappropriate, unsuitable or offensive, all as determined by the Sponsor and/or the Contest Parties in its/their sole discretion.<br /><br />
                    </div>

                    <p>By entering the Contest and submitting a Review, each entrant: (i) grants to the Sponsor, in perpetuity, a non-exclusive license to publish, display, reproduce, modify, edit or otherwise use his/her Review, in whole or in part, for advertising or promoting the Contest or for any other reason; (ii) waives all moral rights in and to his/her Review in favour of the Sponsor; and (iii) agrees to release and hold harmless the Contest Parties and each of their respective agents, employees, directors, successors, and assigns (collectively, the “<strong>Releasees</strong>”) from and against any and all claims based on publicity rights, defamation, invasion of privacy, copyright infringement, trade-mark infringement or any other intellectual property related cause of action.  For greater certainty, the Sponsor reserves the right, in its sole discretion, to modify, edit or disqualify any Entry, or to request an entrant to modify or edit his or her Review, for any reason.  <strong>REVIEWS WILL NOT BE JUDGED.  SUBMISSION OF AN ELIGIBLE ENTRY (INCLUDING AN ELIGIBLE REVIEW) IN ACCORDANCE WITH THESE CONTEST RULES WILL EARN THE ELIGIBLE ENTRANT ONE (1) ENTRY IN THE RANDOM PRIZE DRAW.</strong></p>
                    <p>&nbsp;</p>

                    <p><strong>6.	THE RANDOM DRAW: </strong>The eligible Grand Prize winners will be selected from all eligible Entries received during the Contest Period in a random draw conducted by the independent judging organization at approximately 2:00 PM EDT in Mississauga, Ontario on October 6, 2014 (the “<strong>Draw Date</strong>”).  The odds of winning the Grand Prize depend on the total number of eligible Entries received during the Contest Period in accordance with these Contest Rules. The Sponsor or its designated representative will make a maximum of three (3) attempts to contact the selected entrant by telephone, mail or email (using the information provided at the time of entry) within fifteen (15) business days of the Draw Date.  If the selected entrant cannot be contacted within three (3) attempts or fifteen (15) business days of the Draw Date (whichever occurs first), or if there is a return of any notification as undeliverable; then that selected entrant will be disqualified (and will forfeit all rights to the Grand Prize) and the Sponsor reserves the right, in its sole discretion, to randomly select an alternate eligible entrant from among the remaining eligible Entries received during the Contest Period (in which case the foregoing provisions of this section shall apply to such new selected entrant).  </p>
                    <p>&nbsp;</p>

                    <p>7.	In order to be declared the confirmed winner of the Grand Prize, the selected entrant must first: (a) correctly answer a time-limited mathematical skill-testing question without mechanical or other aid to be administered by mail, email or telephone; and (b) sign and return within ten (10) business days of notification the Sponsor’s declaration and release form, which (among other things): (i) confirms compliance with these Contest Rules; (ii) acknowledges acceptance of the Grand Prize as awarded; (iii) releases the Releasees from any and all liability in connection with this Contest, his/her participation therein and/or the awarding and use/misuse of the Grand Prize or any portion thereof; and (iv) agrees to the publication, reproduction and/or other use of his/her name, address, voice, statements about the Contest and/or photograph or other likeness without further notice or compensation, in any publicity or advertisement carried out by or on behalf of the Sponsor in any manner whatsoever, including print, broadcast or the internet.  If the selected entrant: (a) fails to correctly answer the skill-testing question; (b) fails to return the properly executed Contest documents within the specified time; and/or (c) cannot accept the Grand Prize for any reason; then he/she will be disqualified (and will forfeit all rights to the Grand Prize) and the Sponsor reserves the right, in its sole discretion, to randomly select an alternate eligible entrant from among the remaining eligible Entries received during the Contest Period (in which case the foregoing provisions of this section shall apply to such new selected entrant).  </p>
                    <p>&nbsp;</p>

                    <p><strong>8.	GENERAL:  </strong>All Entry materials become the property of the Sponsor.  The Releasees assume no responsibility for lost, delayed, incomplete, incompatible or misdirected Entries.  This Contest is subject to all applicable federal, provincial and municipal laws.  By entering the Contest, participants agree to be bound by these Official Contest Rules and Regulations (the “<strong>Contest Rules</strong>”) and by the decisions of the Sponsor with respect to all aspects of this Contest, which are final and binding on all entrants without right of appeal, including, without limitation, any decisions regarding the eligibility/disqualification of Entries and/or entrants.</p>
                    <p>&nbsp;</p>

                    <p>The Releasees will not be liable for: (i) any failure of the Website during the Contest; (ii) any technical malfunction or other problems relating to the telephone network or lines, computer on-line systems, servers, access providers, computer equipment or software; (iii) the failure of any Entry to be received by the Contest Parties for any reason including, but not limited to, technical problems or traffic congestion on the Internet or at any website; (iv) any injury or damage to an entrant’s or any other person’s computer or other device related to or resulting from participating or downloading any material in the Contest; and/or (v) any combination of the above. </p>
                    <p>&nbsp;</p> 

                    <p>In the event of a dispute regarding who submitted an Entry, Entries will be deemed to have been submitted by the authorized account holder of the email address submitted at the time of entry.  “Authorized account holder” is defined as the person who is assigned an email address by an internet provider, online service provider, or other organization (e.g. business, educational institute, etc.) that is responsible for assigning email addresses for the domain associated with the submitted email address. An entrant may be required to provide proof that he/she is the authorized account holder of the email address associated with the selected Entry.</p>
                    <p>&nbsp;</p>

                    <p>All Entries are subject to verification.  The Sponsor reserves the right, in its sole and absolute discretion, to require proof of identity and/or eligibility (in a form acceptable to the Sponsor) to participate in this Contest.  Failure to provide such proof in a timely manner may result in disqualification.  The sole determinant of the time for the purposes of a valid Entry in this Contest will be the Contest server machine(s).</p>
                    <p>&nbsp;</p>

                    <p>The Sponsor reserves the right, in its sole and absolute discretion (subject only to the approval of the Regie, if applicable), and without prior notice, to adjust any of the dates and/or timeframes stipulated in these Rules, to the extent necessary, for purposes of verifying compliance by any entrant or Entry with these Rules, or as a result of technical problems, or in light of any other circumstances which, in the opinion of the Sponsor, in its sole and absolute discretion, affect the proper administration of the Contest as contemplated in these Contest Rules.</p>
                    <p>&nbsp;</p>

                    <p>The Sponsor reserves the right, subject only to the approval of the Régie des alcools, des courses et des jeux (the “Régie”) in Quebec, to withdraw, amend or suspend this Contest (or to amend these Contest Rules) in any way, in the event of an error, technical problem, computer virus, bugs, tampering, unauthorized intervention, fraud, technical failure or any other cause beyond the reasonable control of the Sponsor that interferes with the proper conduct of this Contest as contemplated by these Contest Rules.  Any attempt to deliberately damage any website or to undermine the legitimate operation of this Contest is a violation of criminal and civil laws and should such an attempt be made, the Sponsor reserves the right to seek remedies and damages to the fullest extent permitted by law.  The Sponsor, with the consent of the Régie, reserves the right to cancel, amend or suspend this Contest, or to amend these Contest Rules, without prior notice or obligation, in the event of any accident, printing, administrative, or other error or any kind, or for any other reason. </p>
                    <p>&nbsp;</p>

                    <p>By completing the entry form, all entrants consent to the collection, use and distribution of their personal information by the Sponsor only for the purposes of running the Contest (information is stored for approximately one year).  Sponsor will not sell or transmit this information to third parties except for the purposes of administering the Contest.  For information on use of personal information in connection with this Contest, see the privacy policy posted at <a href="http://www.biore.ca/en-CA/privacy/">http://www.biore.ca/en-CA/privacy/</a>. </p>
                    <p>&nbsp;</p>

                    <p>The terms of this Contest, as set out in these Contest Rules, are not subject to amendment or counter-offer, except as set out herein.</p>
                    <p>&nbsp;</p>

                    <p>For Quebec residents:  Any litigation respecting the conduct or organization of a publicity contest may be submitted to the Régie des alcools, des courses et des jeux for a ruling.  Any litigation respecting the awarding of the prize may be submitted to the Régie only for the purpose of helping the parties reach a settlement. </p>
                    <p>&nbsp;</p>

                    <p>All intellectual property used by the Sponsor in connection with the promotion and/or administration of the Contest, including, without limitation, all trade-marks, trade names, logos, designs, promotional materials, web pages, source code, drawings, illustrations, slogans and representations are owned (or licensed, as the case may be) by the Sponsor and/or its affiliates. All rights are reserved. Unauthorized copying or use of any such intellectual property without the express written consent of its owner is strictly prohibited. </p>
                    <p>&nbsp;</p>

                    <p>
                        In the event of any discrepancy or inconsistency between the terms and conditions of these English Contest Rules and disclosures or other statements contained in any Contest-related materials, including, but not limited to: the Contest entry form, French version of these Contest Rules, and/or point of sale, television, print or online advertising; the terms and conditions of these English Contest Rules shall prevail, govern and control.
                    </p>
                    <p>
                        &nbsp;</p>

                    <p>9.	The Sponsor of this contest is Kao Canada Inc., 75 Courtneypark Drive West, Unit 2, Mississauga, ON, L5W 0E3.</p>
                    <p>&nbsp;</p>  

                </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>

