﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="ratings-and-review-contest.aspx.cs" Inherits="Biore2012.promotions.rules.ratings_and_review_contest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedrules_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <!--<div id="polaroid"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/forms/signUpPhoto.jpg") %>" alt="" /></div>-->
                <div id="content">
                    <h1>BIORE<sup style="font-size:40px; line-height:40px;">&reg;</sup> PLEDGE TO STRIP CAMPUS TWITTER CONTEST OFFICIAL CONTEST RULES</h1>

<h2 style="text-transform:uppercase;">This contest is open to residents of CANADA
and is GOVERNED by canadian law</h2>
<p>&nbsp;</p>
                    <p><strong>1.	CONTEST PERIOD:  </strong>  </p>
                    <p>&nbsp;</p><p>The Biore® Pledge To Strip Campus Twitter Contest (the “<strong>Contest</strong>”) begins at 9:00AM, EST on September 8, 2015 and ends at 11:59PM, EST on November 26, 2015, as more specifically set forth for each location in the chart at paragraph 3 (the “<strong>Contest Period</strong>”). </p>
                    <p>&nbsp;</p>

                    <p><strong>2.	ELIGIBILITY: </strong></p>
                    <p>&nbsp;</p>
                    <p>Contest is open to residents of Canada who are seventeen (17) years of age or older as of September 8, 2015; except officers, directors, employees, representatives or agents (and those with whom such persons are domiciled, whether related or not) of Kao Canada Inc. (the “<strong>Sponsor</strong>”), Campus Intercept Inc., their respective parent companies, subsidiaries, affiliates, prize suppliers, advertising/promotion agencies, and any entity involved in the development, production, implementation, administration, judging or fulfillment of the Contest (collectively, the “<strong>Contest Parties</strong>”).</p>
                    <p>&nbsp;</p>

                    <p><strong>3.	HOW TO ENTER:</strong></p>
                    <p>&nbsp;</p>

                        <p>NO PURCHASE NECESSARY.  The following table lists the respective campus entry locations (each, a “<strong>Location</strong>”) where a Bioré-branded vending machine will be placed on the dates specified below (each, a “Date”). </p>
                        <p>&nbsp;</p>
                        
                        
                        <table border="1" cellspacing="15" cellpadding="0">
                          <tr bgcolor="#B9B9B9">
                            <td style="padding:5px;"><p><strong>Campus Location</strong></p></td>
                            <td style="padding:5px;"><p><strong>Date</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Mohawk College </strong></p></td>
                            <td><p><strong>September 8, 2015 – October 1, 2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>McGill University </strong></p></td>
                            <td><p><strong>September 8, 2015 – October 1, 2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Seneca College </strong></p></td>
                            <td><p><strong>September 8, 2015 – October 1, 2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>University of Toronto Mississauga</strong></p></td>
                            <td><p><strong>September 8, 2015 – October 1, 2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>University of Windsor </strong></p></td>
                            <td><p><strong>September 8, 2015 – October 1, 2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Queen's University </strong></p></td>
                            <td><p><strong>October 6, 2015 – October 29, 2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>University of Ottawa </strong></p></td>
                            <td><p><strong>October 6, 2015 – October 29, 2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>York University</strong></p></td>
                            <td><p><strong>October 6, 2015 – October 29, 2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>George Brown College</strong></p></td>
                            <td><p><strong>October 6, 2015 – October 29, 2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>University of Waterloo </strong></p></td>
                            <td><p><strong>October 6, 2015 – October 29, 2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Centennial College </strong></p></td>
                            <td><p><strong>November 3, 2015 – November 26, 2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>University of Western Ontario</strong></p></td>
                            <td><p><strong>November 3, 2015 – November 26, 2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Fanshawe College </strong></p></td>
                            <td><p><strong>November 3, 2015 – November 26, 2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Brock University </strong></p></td>
                            <td><p><strong>November 3, 2015 – November 26, 2015</strong></p></td>
                          </tr>
                          <tr>
                            <td><p><strong>Wilfrid Laurier University </strong></p></td>
                            <td><p><strong>November 3, 2015 – November 26, 2015</strong></p></td>
                          </tr>
                        </table>
<p>&nbsp;</p>
                        <p>To enter the Contest visit the Bioré-branded vending machine at the Location on the corresponding Date. Upon visiting the Bioré-branded vending machine at the Location, a unique numeric code (a “<strong>Code</strong>”) will be provided via a touchscreen on the Bioré-branded vending machine. From your personal Twitter account (your “<strong>Account</strong>”), Tweet a message which includes: (i) a mention of “@biorecanada” (the “<strong>Mention</strong>”); and (ii) the hashtags #pledgetostrip, #[name of location] (e.g. #Carleton), and #[Code] (e.g. #1234) (the “<strong>Hashtags</strong>”) (collectively, a “<strong>Tweet</strong>”).</p>
                        <p>&nbsp;</p>
                  

                    

                  <p>To be eligible, your Tweet must:</p>
                    <p>&nbsp;</p>

                    <div style="margin-left:50px">
                    i.	comply with the specific Submission Requirements listed below in Rule 5;<br /><br />

                    ii.	be submitted from a public Account;<br /><br />

                    iii. include the Mention and Hashtags;<br /><br />

                    iv.	comply with the Twitter Terms of Service and Twitter Rules available at <a href="http://www.twitter.com">www.twitter.com</a><br /><br />
 
                    v.	must not be deleted until 12:00 PM EST on December 2, 2015 once it has been submitted. </div>
                    <p>&nbsp;</p>
                    <p>
Any Tweet that does not follow the above format (as determined by the Sponsor in its sole and absolute discretion) will be discarded and will not be eligible for entry in this Contest. A Tweet will be considered to be void (and an Entry [as defined below] will not be granted) if the Tweet: (i) is not submitted and received during the Contest Period in accordance with these Rules; (ii) is not submitted from a public Account; (iii) does not include the Hashtags and Mention; (iv) does not comply with the Twitter Terms of Service and Twitter Rules; (v) does not comply with the specific Submission Requirements listed below in Rule 5 (all as determined by the Sponsor in its sole and absolute discretion); and/or (vi) was deleted anytime prior to 12:00PM EST on December 2, 2015 once submitted.</p><br />
                    

                    <p>An eligible entrant (see above) will be eligible to receive one (1) entry (the “Entry”) when he/she Tweets his/her Tweet in accordance with these Official Contest Rules (the “Rules”), as determined by the Sponsor at its sole and absolute discretion.</p>
                    <p>&nbsp;</p>
                    <p>To participate in this Contest, you must have a valid Account.  If you do not have an Account, visit <a href="http://www.twitter.com">www.twitter.com</a> and register in accordance with the enrolment instructions for a free Twitter account.</p><p>&nbsp;</p>
                    
                    <p>Standard text messaging and/or data rates apply to entrants who submit a Tweet via a wireless mobile device. Wireless service providers may charge for airtime for each standard text message sent and received.  Please call your service provider for pricing and service plan information and rates before mobile device participation.</p>
                    <p>&nbsp;</p>

                    <p><strong>4.	LIMITS AND VERIFICATION:</strong></p>
                    <p>&nbsp;</p><p>There is a limit of one (1) Tweet per Account permitted during the Contest Period.  There is a limit of one (1) Entry per Account permitted during the Contest Period. If it is discovered that any person has attempted to: (i) submit more than one (1) Tweet per person/Account during the Contest Period; (ii) obtain more than one (1) Entry per person/Account; and/or (ii) use (or attempt to use) multiple names, identities, email addresses and/or more than one (1) Account to enter the Contest; then (in the sole and absolute discretion of the Sponsor) he/she may be disqualified from the Contest and all of his/her Tweets and Entries voided.  Use (or attempted use) of multiple names, identities, email addresses, Accounts and/or any automated, macro, script, robotic or other system(s) or program(s) to enter or otherwise participate in or disrupt this Contest is prohibited and is grounds for disqualification by the Sponsor. The Releasees (defined below) are not responsible for late, lost, misdirected, delayed, incomplete or incompatible Tweets (all of which are void).</p>
                    <p>&nbsp;</p>
                    
                    <p>All Tweets and Entries are subject to verification by the Sponsor at any time and for any reason.  The Sponsor reserves the right, in its sole and absolute discretion, to require proof of identity and/or eligibility (in a form acceptable to the Sponsor – including, without limitation, government issued photo identification): (i) for the purposes of verifying an individual’s eligibility to participate in this Contest; (ii) for the purposes of verifying the eligibility and/or legitimacy of any Entry or Tweet for the purposes of this Contest; and/or (iii) for any other reason the Sponsor deems necessary, in its sole and absolute discretion, for the purposes of administering this Contest in accordance with these Rules.  Failure to provide such proof to the complete satisfaction of the Sponsor in a timely manner may result in disqualification, in the sole and absolute discretion of the Sponsor. The sole determinant of the time for the purposes of a valid Tweet/Entry in this Contest will be the Contest server machine(s).</p>
                    <p>&nbsp;</p>
                    <p>IN NO EVENT AND UNDER NO CIRCUMSTANCES WILL ANY PERSON/FOLLOWER BE PERMITTED TO OBTAIN MORE THAN THE MAXIMUM NUMBER OF ENTRIES AS STATED IN THESE RULES.</p>
                    <p>&nbsp;</p>

                    <p><strong>5.	SUBMISSION REQUIREMENTS: </strong></p>
                    <p>&nbsp;</p>
                    <p>BY SUBMITTING A TWEET, YOU AGREE THAT THE TWEET COMPLIES WITH ALL CONDITIONS STATED IN THESE RULES AND THE TWITTER TERMS OF SERVICE.  THE RELEASEES (DEFINED BELOW) WILL BEAR NO LEGAL LIABILITY REGARDING THE USE OF ANY TWEET YOU SUBMIT.  THE RELEASEES (DEFINED BELOW) SHALL BE HELD HARMLESS BY YOU IN THE EVENT IT IS SUBSEQUENTLY DISCOVERED THAT YOU HAVE NOT FULLY COMPLIED WITH ANY OF THESE RULES.</p>
                    <p>&nbsp;</p>

                    <p>By participating in the Contest and submitting a Tweet, each entrant agrees to be legally bound by these Rules and by the interpretation of these Rules by the Sponsor, and further warrants and represents that any content he/she submits as part of his/her Tweet (excluding the Mention and Hashtag) (collectively, his/her “<strong>Submission</strong>”): </p>
                    <p>&nbsp;</p>
                    <p>(a)	is original to him/her and that the entrant has all necessary rights in and to his/her Submission to enter the Contest;</p>
                    
                    <p>(b)	does not violate any law;</p>
                    <p>(c)	does not contain any reference to any identifiable third parties, unless consent has been obtained from each such individual and his/her parent/legal guardian if he/she is under the age of majority in his/her jurisdiction of residence;</p>
                    <p>(d)	will not give rise to any claims of infringement, invasion of privacy or publicity, or infringe on any rights and/or interests of any third party, or give rise to any claims whatsoever; and</p>
                    <p>(e)	is not defamatory, trade libelous, pornographic or obscene, and further that it will not contain, include, discuss or involve, without limitation, any of the following: nudity; alcohol/drug consumption or smoking; explicit or graphic sexual activity, or sexual innuendo; crude, vulgar or offensive language and/or symbols; derogatory characterizations of any ethnic, racial, sexual, religious or other groups; content that endorses, condones and/or discusses any illegal, inappropriate or risky behaviour or conduct; personal information of individuals, including, without limitation, names, telephone numbers and addresses (physical or electronic); commercial messages, comparisons or solicitations for products or services other than products of Sponsor; any identifiable third party products, trade-marks, brands and/or logos, other than those of Sponsor; conduct or other activities in violation of these Rules; and/or any other content that is or could be considered inappropriate, unsuitable or offensive, all as determined by the Sponsor in its sole and absolute discretion.</p>
                    
                    <p>&nbsp;</p> 

                    <p>By entering the Contest and submitting a Tweet, each entrant: (i) without limiting the Twitter Terms of Service grants to the Sponsor, in perpetuity, a non-exclusive license to publish, display, reproduce, modify, edit or otherwise use his/her Tweet, in whole or in part, for advertising or promoting the Contest or for any other reason; (ii) waives all moral rights in and to his/her Tweet in favour of the Sponsor; and (iii) agrees to release and hold harmless the Contest Parties and each of their respective agents, employees, directors, successors, and assigns (collectively, the “Releasees”) from and against any and all claims based on publicity rights, defamation, invasion of privacy, copyright infringement, trade-mark infringement or any other intellectual property related cause of action that relate in any way to the Tweet.  For greater certainty, the Sponsor reserves the right, in its sole and absolute discretion and at any time during the Contest, to modify, edit or disqualify any Tweet if a complaint is received with respect to the Tweet, or for any other reason.  If such an action is necessary at any point, then the Sponsor reserves the right, in its sole and absolute discretion, to disqualify the Tweet (and corresponding Entry) and/or the associated entrant.   If the Sponsor determines, in its sole and absolute discretion, that any Tweet does not comply with these Rules for any reason at any time, then the Sponsor reserves the right, in its sole and absolute discretion, to disqualify the Tweet (and corresponding Entry) and/or the associated entrant.  Tweets will NOT be judged.</p>
                    <p>&nbsp;</p>

                    <p><strong>6. 	THE PRIZE:</strong> </p>
                    <p>&nbsp;</p>
                    <p>There are fifteen (15) Biore® Prizes (the “<strong>Prizes</strong>”) available to be won, each of which consists of a one (1) year supply of Biore® pore strips.</p>
                    <p>&nbsp;</p>

                    <p>One (1) Prize will be awarded per Location. Each Prize has a total approximate retail value of $70.00 CAD.  The characteristics and features of each Prize element, except as explicitly described above, are at the Sponsor’s sole and absolute discretion.  The Prize must be accepted as awarded and is not transferable, assignable or convertible to cash (except as may be specifically permitted by Sponsor in its sole and absolute discretion).  No substitutions except at Sponsor’s option.  Sponsor reserves the right, in its sole and absolute discretion, to substitute the Prize, or a component thereof, with a prize of equal or greater retail value, including, without limitation, but at Sponsor’s sole and absolute discretion, a cash award.</p>
                    <p>&nbsp;</p>

                    <p><strong>7. 	THE PRIZE DRAW:</strong></p>
                    <p>&nbsp;</p>
                    <p> The following table lists the Prize draw dates (each a “<strong>Draw Date</strong>”) for each respective Location: </p>
                    <p>&nbsp;</p>
                    <table border="1" cellspacing="15" cellpadding="0">
                      <tr bgcolor="#B4B4B4">
                        <td style="padding:5px;"><p><strong>Campus Locations</strong></p></td>
                        <td style="padding:5px;"><p><strong>Draw Date</strong></p></td>
                      </tr>
                      <tr>
                        <td><p><strong>Mohawk College </strong><br>
                          <strong>McGill University </strong><br>
                          <strong>Seneca College </strong><br>
                          <strong>University of Toronto Mississauga</strong><br>
                          <strong>University of Windsor</strong></p></td>
                        <td><p><strong>Tuesday, October 6, 2015</strong></p></td>
                      </tr>
                      <tr>
                        <td><p><strong>Queen's University </strong><br>
                          <strong>University of Ottawa </strong><br>
                          <strong>York University</strong><br>
                          <strong>George Brown College</strong><br>
                          <strong>University of Waterloo</strong></p></td>
                        <td><p><strong>Tuesday, November 3, 2015</strong></p></td>
                      </tr>
                      <tr>
                        <td><p><strong>Centennial College </strong><br>
                          <strong>University of Western   Ontario</strong><br>
                          <strong>Fanshawe College </strong><br>
                          <strong>Brock University </strong><br>
                          <strong>Wilfrid Laurier University</strong></p></td>
                        <td><p><strong>Tuesday, December 1, 2015</strong></p></td>
                      </tr>
                    </table>
<p>&nbsp;</p>
                    

                    <p>On or about each Draw Date, at approximately 2:00PM, EST, a random draw will be conducted in Toronto, Ontario from all eligible Entries received in accordance with these Rules (as determined by the Sponsor at its sole and absolute discretion), to select the potential Prize winner from each Location. The odds of winning the Prize will depend on the number of eligible Entries received in accordance with these Rules (as determined by the Sponsor at its sole and absolute discretion).</p>
                    <p>&nbsp;</p>

                    <p>The Sponsor or its designated representative(s) will make a minimum of three (3) attempts to contact the potential Prize winners via a Twitter tag post from its @biorecanada channel, inviting the potential Prize winner to “direct message” the Sponsor within five (5) business days following the Draw Date. In the event that the potential Prize winner cannot be contacted for any reason within five (5) business days of the Draw Date, or in the event that the potential Prize winner does not otherwise comply with these Rules, then the potential Prize winner will be disqualified (and will forfeit all rights to the Prize) and the Sponsor reserves the right, in its sole and absolute discretion, to randomly select an alternate potential Prize winner by random draw from among the remaining eligible Entries (in which case the foregoing provisions of this section shall apply to the new potential Prize winner, with the necessary amendments).</p>
                    <p>&nbsp;</p>

                    <p>Before being declared the CONFIRMED prize winner, the potential Prize winner will be required to: (a) correctly answer, without mechanical or other aid, a time-limited mathematical skill-testing question to be administered, at the election of the Sponsor, either by telephone or e-mail, at a mutually convenient time, or via the required declaration and release form; and (b) sign and return, within the timeframe specified by the Sponsor (or its designated representative), the Sponsor’s declaration and release form, which (among other things): (i) confirms compliance with these Rules; (ii) acknowledges acceptance of the Prize as awarded; (iii) releases Releasees from any and all liability in connection with this Contest, his/her participation therein and/or the awarding and use/misuse of the Prize or any portion thereof; and (iv) agrees to the publication, reproduction and/or other use of his/her name, address, voice, statements about the Contest and/or photograph or other likeness without further notice or compensation, in any publicity or advertisement carried out by or on behalf of the Sponsor in any manner whatsoever, including print, broadcast or the internet. In the event that a potential winner is below the age of majority in his or her jurisdiction of residence, Sponsor may require the potential winner’s parent or legal guardian to ratify the potential winner’s release form. If the potential Prize winner: (a) fails to correctly answer the skill-testing question; (b) fails to sign and return the Contest documents as aforesaid; and/or (c) cannot accept the Prize for any reason; then he/she will be disqualified (and will forfeit all rights to the Prize) and the Sponsor reserves the right, in its sole and absolute discretion, to randomly select an alternate potential Prize winner by random draw from among the remaining eligible Entries (in which case the foregoing provisions of this section shall apply to the new potential Prize winner, with the necessary amendments).</p>
                    <p>&nbsp;</p>

                    <p><strong>8.	GENERAL CONDITIONS:</strong></p>
                    <p>&nbsp;</p>
                    <p> By participating in this Contest, participants acknowledge compliance with, and agree to be legally bound by, these Rules, including eligibility requirements.  In addition, each participant releases and holds harmless the Releasees from and against any and all manner of action, causes of action, suits, debts, covenants, contracts, claims and demands (including legal fees and expenses) of any type whatsoever, including but not limited to claims based on negligence, breach of contract and fundamental breach and liability for physical injury, death, or property damage which the participant or his/her administrators, heirs, successors or assigns might have or could have, by reason of or arising out of the participant’s participation in the Contest and/or in connection with the acceptance and/or exercise by the participant of the Prizes and from and against any and all liability with respect to or in any way arising from the Contest and any Prize(s), including liability for personal injury or damage to property. This Contest is subject to all applicable federal, provincial and municipal laws.  The decisions of the Sponsor (or, where instructed accordingly, the Sponsor’s designated representative) with respect to all aspects of this Contest are final and binding on all entrants without right of appeal, including, without limitation, any decisions regarding the eligibility/disqualification of entrants, Tweets or Entries. ANYONE DETERMINED TO BE IN VIOLATION OF THESE RULES FOR ANY REASON IS SUBJECT TO DISQUALIFICATION IN THE SOLE AND ABSOLUTE DISCRETION OF THE SPONSOR AT ANY TIME. </p>
                    <p>&nbsp;</p>

                    <p>
                        The Releasees will not be liable for: (i) any technical malfunction or other problems relating to any telephone network, handset, equipment or lines, computer on-line systems, servers, access providers, computer equipment, social media platforms or software; (ii) the failure of any Tweet or Entry or other information to be received, captured or recorded for any reason whatsoever; and/or (iii) any combination of the above. 
                    </p>
                    <p>&nbsp;</p>
                    <p>In the event of a dispute regarding who submitted a Tweet, the Sponsor reserves the right to deem the Tweet to have been submitted by the authorized account holder of Account used to submit the applicable Tweet.  “Authorized account holder” is defined as the person who is assigned the Account by Twitter (on the basis of its records). An entrant may be required to provide proof (in a form acceptable to the Sponsor) that he/she is the authorized account holder of the Account used to submit the applicable Tweet.</p>
                    <p>&nbsp;</p>
                    <p>The Sponsor reserves the right, in its sole and absolute discretion, to withdraw, amend or suspend this Contest (or to amend these Rules) in any way, in the event of an error, technical problem, computer virus, bugs, tampering, unauthorized intervention, fraud, technical or other failure or any other cause beyond the reasonable control of the Sponsor that interferes with the proper conduct of this Contest as contemplated by these Rules with the prior consent of the Régie (defined below) in Quebec where applicable.  Any attempt to undermine the legitimate operation of this Contest in any way (as determined by Sponsor in its sole and absolute discretion) is a violation of criminal and civil laws and should such an attempt be made, the Sponsor reserves the right to seek remedies and damages to the fullest extent permitted by law.  The Sponsor reserves the right, in its sole and absolute discretion, to cancel, amend or suspend this Contest, or to amend these Rules, in any way without prior notice or obligation, in the event of any accident, printing, administrative, or other error of any kind, or for any other reason whatsoever.  Without limiting the generality of the forgoing, the Sponsor reserves the right, in its sole and absolute discretion, to administer an alternate test of skill as it deems appropriate based on the circumstances and/or to comply with applicable law.</p>
                    <p>&nbsp;</p>
                    <p>The Sponsor reserves the right, in its sole and absolute discretion, and without prior notice, to adjust any of the dates and/or timeframes stipulated in these Rules, to the extent necessary, for purposes of verifying compliance by any entrant, Tweet or  Entry with these Rules, or as a result of any technical or other problems, or in light of any other circumstances which, in the opinion of the Sponsor, in its sole and absolute discretion, affect the proper administration of the Contest as contemplated in these Rules, or for any other reason.</p>
                   
                    
                    <p>&nbsp;
                        </p>

                    <p><strong>9.	QUEBEC RESIDENTS:</strong> </p>
                    <p>&nbsp;</p>
                    <p>Any litigation respecting the conduct or organization of a publicity contest may be submitted to the Régie des alcools, des courses et des jeux (the “Régie”) for a ruling.  Any litigation respecting the awarding of the prize may be submitted to the Régie only for the purpose of helping the parties reach a settlement. </p>
                    <p>&nbsp;</p>
                    <p><strong>10.	CONSENT TO USE PERSONAL INFORMATION:</strong></p>
                    <p>&nbsp;</p>
                    <p> By entering this Contest, each entrant expressly consents to the Sponsor, its agents and/or representatives, storing, sharing and using the personal information submitted with his/her Tweet only for the purpose of administering the Contest and in accordance with Sponsor’s privacy policy (available at http://www.biore.ca/en-CA/privacy/), unless the entrant otherwise agrees.</p>
                    <p>&nbsp;</p>
                    <p><strong>11. 	OTHER:</strong> </p>
                    <p>&nbsp;</p>
                    <p>In the event of any discrepancy or inconsistency between the terms and conditions of these English Rules and disclosures or other statements contained in any Contest-related materials, including, but not limited to the French version of these Rules and/or point of sale, television, print or online advertising, the terms and conditions of these English Rules shall prevail, govern and control to the fullest extent permitted by applicable law.</p>
                    <p>&nbsp;</p>  

                </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>

