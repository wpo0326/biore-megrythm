﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="ratings-and-review-contest.aspx.cs" Inherits="Biore2012.promotions.rules.ratings_and_review_contest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedrules_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <!--<div id="polaroid"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/forms/signUpPhoto.jpg") %>" alt="" /></div>-->
                <div id="content">
                    <h1>BIORE BACK-TO-SCHOOL INSTAGRAM CONTEST OFFICIAL CONTEST RULES</h1>

                    <p><strong>1.	CONTEST PERIOD: </strong>  The Bioré Back-to-School Instagram Contest (the “Contest”) begins at 9:00AM, EST on August 7, 2017 and ends at 9:00PM, EST on August 28, 2017 (the “Contest Period”). </p>
                    <p>&nbsp;</p>

                    <p><strong>2.	ELIGIBILITY:   </strong>Contest is open to residents of Canada only (excluding Quebec) who are eighteen (18) years of age or older as of August 7, 2017; except officers, directors, employees, representatives or agents (and those with whom such persons are domiciled, whether related or not) of Kao Canada Inc. (the “Sponsor”) and its parent company, subsidiaries, affiliates, prize suppliers, advertising/Contest agencies, and any entity involved in the development, production, implementation, administration, judging or fulfillment of the Contest (collectively, the “Contest Parties”).</p>
                    <p>&nbsp;</p>

                    <p><strong>3.	HOW TO ENTER:  </strong></p>
				NO PURCHASE NECESSARY.
                    <p>&nbsp;</p>
                        <p>To enter the Contest, eligible entrants must follow @bioreca on Instagram.  Then, during the Contest Period, leave comment responsive to Sponsor’s post: “To enter the Contest, follow @bioreca on Instagram and, using your personal Instagram Account, leave a comment sharing your best early morning hack (the “Contest Post”).  An eligible entrant will receive one (1) entry (the “Entry”) when he/she posts his/her Comment in accordance with these Official Contest Rules (the “Rules”), as determined by the Sponsor at its sole and absolute discretion.</p>
                        <p>&nbsp;</p>

                        <p>To participate in this Contest, an entrant must have a valid and public Instagram account (“Account”).  If you do not have an Account, visit www.instagram.com and register in accordance with the enrolment instructions for a free Instagram account. Standard text messaging and/or data rates may apply to entrants who submit an Entry via a wireless mobile device. </p>
                        <p>&nbsp;</p>
                    </div>

                    <p><strong>4.	LIMITS AND VERIFICATION: </strong>There is a limit of one (1) Entry per Account permitted during each of the three Entry Periods.  If it is discovered that any person has attempted to: (i) submit more than one (1) Entry per Account during each Entry Period; (ii) obtain more than one (1) Entry per Account; and/or (iii) use (or attempt to use) multiple names, identities, email addresses and/or more than one (1) Account to enter the Contest; then (in the sole and absolute discretion of the Sponsor) he/she may be disqualified from the Contest and all of his/her Entries voided.  Use (or attempted use) of multiple names, identities, email addresses, Accounts and/or any automated, macro, script, robotic or other system(s) or program(s) to enter or otherwise participate in or disrupt this Contest is prohibited and is grounds for disqualification by the Sponsor. The Releasees (defined below) are not responsible for late, lost, misdirected, delayed, incomplete or incompatible Entries (all of which are void). </p>
                    <p>&nbsp;</p> 

                    <p>All Entries are subject to verification by the Sponsor at any time and for any reason.  The Sponsor reserves the right, in its sole and absolute discretion, to require proof of identity and/or eligibility (in a form acceptable to the Sponsor – including, without limitation, government issued photo identification): (i) for the purposes of verifying an individual’s eligibility to participate in this Contest; (ii) for the purposes of verifying the eligibility and/or legitimacy of any Entry for the purposes of this Contest; and/or (iii) for any other reason the Sponsor deems necessary, in its sole and absolute discretion, for the purposes of administering this Contest in accordance with these Rules.  Failure to provide such proof to the complete satisfaction of the Sponsor in a timely manner may result in disqualification, in the sole and absolute discretion of the Sponsor. The sole determinant of the time for the purposes of a valid Entry in this Contest will be the Contest server machine(s).</p>

                    <p>&nbsp;</p> 

                    <p>IN NO EVENT AND UNDER NO CIRCUMSTANCES WILL ANY PERSON/FOLLOWER BE PERMITTED TO OBTAIN MORE THAN THE MAXIMUM NUMBER OF ENTRIES AS STATED IN THESE RULES.</p>

                    <p>&nbsp;</p> 

                    <p>
                        <strong>5.	THE PRIZE DRAW: </strong>There will be three (3) prize draws each week of the Contest Period as follows: August 14, 2017, August 21, 2017 and August 29, 2017 @12:00 pm.
                    </p>

                        &nbsp;</p> 

                    <p>
Sponsor will conduct a random draw of all eligible Entries received prior to each draw in Toronto, Ontario in accordance with these Rules (as determined by the Sponsor at its sole and absolute discretion) to select the potential prize winner for each Entry Period. The odds of winning the prize will depend on the number of eligible Entries received in accordance with these Rules (as determined by the Sponsor at its sole and absolute discretion).
                    </p>
                    <p>
                        &nbsp;</p> 

                    <p>The Sponsor or its designated representative(s) will notify each potential prize winner by email (“Winner Notification”) and request a response, which includes a valid email address, within two (2) business days of the draw.  In the event that the potential prize winner cannot be contacted for any reason or fails to respond to the Winner Notification within two (2) business days, or in the event that the potential prize winner does not otherwise comply with these Rules, then the potential prize winner will be disqualified (and will forfeit all rights to the prize) and the Sponsor reserves the right, in its sole and absolute discretion, to randomly select an alternate potential prize winner by random draw from among the remaining eligible Entries (in which case the foregoing provisions of this section shall apply to the new potential prize winner, with the necessary amendments). </p>
                    <p>&nbsp;</p>

                    <p><strong>BEFORE BEING DECLARED THE CONFIRMED PRIZE WINNER</strong>, the potential prize winner will be required to: (a) correctly answer, without mechanical or other aid, a time-limited mathematical skill-testing question to be administered, at the election of the Sponsor, either by telephone or e-mail, at a mutually convenient time, or via the required declaration and release form; and (b) sign and return, within the timeframe specified by the Sponsor (or its designated representative), the Sponsor’s declaration and release form (“Declaration & Release”), which (among other things): (i) confirms compliance with these Rules; (ii) acknowledges acceptance of the prize as awarded; (iii) releases Releasees from any and all liability in connection with this Contest, his/her participation therein and/or the awarding and use/misuse of the prize or any portion thereof; and (iv) agrees to the publication, reproduction and/or other use of his/her name, address, voice, statements about the Contest and/or photograph or other likeness without further notice or compensation, in any publicity or advertisement carried out by or on behalf of the Sponsor in any manner whatsoever, including print, broadcast or the internet. In the event that a potential winner is below the age of majority in his or her jurisdiction of residence, Sponsor may require the potential winner’s parent or legal guardian to ratify the potential winner’s release form. If the potential prize winner: (a) fails to correctly answer the skill-testing question; (b) fails to sign and return the Contest documents as aforesaid; and/or (c) cannot accept the prize for any reason; then he/she will be disqualified (and will forfeit all rights to the prize) and the Sponsor reserves the right, in its sole and absolute discretion, to randomly select an alternate potential prize winner by random draw from among the remaining eligible Entries (in which case the foregoing provisions of this section shall apply to the new potential prize winner, with the necessary amendments).</p>
                    <p>&nbsp;</p>

                    <p><strong>6.	THE PRIZE: </strong>There are six (6) prizes (the “Prize”) available to be won, with two (2) potential winners chosen during each draw.  Each confirmed winner will receive the following Bioré product collection:
</br>Bioré® Deep Cleansing Pore Strips (ARV CAD $9.99), Bioré® Deep Cleansing Charcoal Pore Strips (ARV CAD $9.99), Bioré® Baking Soda Cleansing Scrub (ARV CAD $9.99), Bioré® Deep Pore Charcoal Cleanser (ARV CAD $9.99) and Bioré® Self Heating One Minute Mask (ARV CAD $9.99) </p>
                    <p>&nbsp;</p>

                    <p>Approximate retail value of each Prize is $49.95 CAD. </p>

                    <p>&nbsp;</p>

                    <p>The prize must be accepted as awarded and is not transferable, assignable or convertible to cash (except as may be specifically permitted by Sponsor in its sole and absolute discretion).  No substitutions except at Sponsor’s option.  Sponsor reserves the right, in its sole and absolute discretion, to substitute the prize, or a component thereof, with a prize of equal or greater retail value, including, without limitation, but at Sponsor’s sole and absolute discretion, a cash award. </p>

                    <p>&nbsp;</p>

                    <p><strong>7.	PRIZE DELIVERY.   </strong>Winner will be sent prize within 4-6 weeks of receipt of the Declaration & Release document described above.  Sponsor is not responsible if the prize cannot be delivered due to an incorrect mailing address or for lost or misdirected requests.</p>
                    <p>&nbsp;</p>

                    <p><strong>8.	LICENSE, WAIVER, AND RELEASE: </strong>By entering the Contest and submitting an Entry, each entrant: (i) without limiting the Instagram Terms of Service, grants to the Sponsor, in perpetuity, a non-exclusive license to publish, display, reproduce, modify, edit or otherwise use his/her Entry, in whole or in part, for advertising or promoting the Contest or for any other reason; (ii) waives all moral rights in and to his/her Entry in favour of the Sponsor; and (iii) agrees to release and hold harmless the Contest Parties and each of their respective agents, employees, directors, successors, and assigns (collectively, the “Releases”) from and against any and all claims based on publicity rights, defamation, invasion of privacy, copyright infringement, trade-mark infringement or any other intellectual property related cause of action that relate in any way to the Entry.  For greater certainty, the Sponsor reserves the right, in its sole and absolute discretion and at any time during the Contest, to modify, edit or disqualify any Entry if a complaint is received with respect to the Entry, or for any other reason.  If such an action is necessary at any point, then the Sponsor reserves the right, in its sole and absolute discretion, to disqualify the Entry (and corresponding Entry) and/or the associated entrant.   If the Sponsor determines, in its sole and absolute discretion, that any Entry does not comply with these Rules for any reason at any time, then the Sponsor reserves the right, in its sole and absolute discretion, to disqualify the Entry (and corresponding Entry) and/or the associated entrant.  Entries will NOT be judged.</p>
                    <p>&nbsp;</p>

                    <p><strong>9.	GENERAL CONDITIONS: </strong>By participating in this Contest, participants acknowledge compliance with, and agree to be legally bound by, these Rules, including eligibility requirements.  In addition, each participant releases and holds harmless the Releasees from and against any and all manner of action, causes of action, suits, debts, covenants, contracts, claims and demands (including legal fees and expenses) of any type whatsoever, including but not limited to claims based on negligence, breach of contract and fundamental breach and liability for physical injury, death, or property damage which the participant or his/her administrators, heirs, successors or assigns might have or could have, by reason of or arising out of the participant’s participation in the Contest and/or in connection with the acceptance and/or exercise by the participant of the prizes and from and against any and all liability with respect to or in any way arising from the Contest and any prize(s), including liability for personal injury or damage to property. This Contest is subject to all applicable federal, provincial and municipal laws.  The decisions of the Sponsor (or, where instructed accordingly, the Sponsor’s designated representative) with respect to all aspects of this Contest are final and binding on all entrants without right of appeal, including, without limitation, any decisions regarding the eligibility/disqualification of entrants, Entrys or Entries. ANYONE DETERMINED TO BE IN VIOLATION OF THESE RULES FOR ANY REASON IS SUBJECT TO DISQUALIFICATION IN THE SOLE AND ABSOLUTE DISCRETION OF THE SPONSOR AT ANY TIME. </p>
                    <p>&nbsp;</p>

                    <p>The Releasees will not be liable for: (i) any technical malfunction or other problems relating to any telephone network, handset, equipment or lines, computer on-line systems, servers, access providers, computer equipment, social media platforms or software; (ii) the failure of any Entry or Entry or other information to be received, captured or recorded for any reason whatsoever; and/or (iii) any combination of the above.  </p>
                    <p>&nbsp;</p> 

                    <p>In the event of a dispute regarding who submitted an Entry, the Sponsor reserves the right to deem the Entry to have been submitted by the authorized account holder of Account used to submit the applicable Entry.  “Authorized account holder” is defined as the person who is assigned the Account by Instagram (on the basis of its records). An entrant may be required to provide proof (in a form acceptable to the Sponsor) that he/she is the authorized account holder of the Account used to submit the applicable Entry.</p>
                    <p>&nbsp;</p>

                    <p>The Sponsor reserves the right, in its sole and absolute discretion, to withdraw, amend or suspend this Contest (or to amend these Rules) in any way, in the event of an error, technical problem, computer virus, bugs, tampering, unauthorized intervention, fraud, technical or other failure or any other cause beyond the reasonable control of the Sponsor that interferes with the proper conduct of this Contest as contemplated by these Rules.  Any attempt to undermine the legitimate operation of this Contest in any way (as determined by Sponsor in its sole and absolute discretion) is a violation of criminal and civil laws and should such an attempt be made, the Sponsor reserves the right to seek remedies and damages to the fullest extent permitted by law.  The Sponsor reserves the right, in its sole and absolute discretion, to cancel, amend or suspend this Contest, or to amend these Rules, in any way without prior notice or obligation, in the event of any accident, printing, administrative, or other error of any kind, or for any other reason whatsoever.  Without limiting the generality of the forgoing, the Sponsor reserves the right, in its sole and absolute discretion, to administer an alternate test of skill as it deems appropriate based on the circumstances and/or to comply with applicable law.</p>
                    <p>&nbsp;</p>

                    <p><strong>10.	CONSENT TO USE PERSONAL INFORMATION: </strong>By entering this Contest, each entrant expressly consents to the Sponsor, its agents and/or representatives, storing, sharing and using the personal information submitted with his/her Entry only for the purpose of administering the Contest and in accordance with Sponsor’s privacy policy http://www.biore.ca/en-CA/privacy/, unless the entrant otherwise agrees. </p>
                    <p>&nbsp;</p>

                    <p>11.	Instagram, LLC is not a sponsor of or in any way affiliated with this Contest. Entrants acknowledge that the Contest is in no way sponsored, endorsed or administered by Instagram, LLC; will direct any questions, comments or complaints regarding the Contest to the Sponsor, not to Instagram, LLC; and release Instagram, LLC and its associated companies from all liability arising in respect of the Contest. Entry and continued participation in the Contest is dependent on entrants following and acting in accordance with the Instagram Terms of Use, which can be viewed at https://instagram.com/about/legal/terms/.</p>
                    <p>&nbsp;</p>  

                </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>

