﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="formCustomProveIt2.aspx.cs" Inherits="Biore2012.FormConfig.formCustomProveIt2" %>
<%@ Register TagPrefix="uc1" TagName="ucForm" Src="~/Controls/ucFormConfig.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <%#SquishIt.Framework.Bundle .Css()
        .Add("~/css/formConfig.css")
        .Render("~/css/combinedform_#.css")
    %>
    <meta name="robots" content="noindex,follow">
</asp:Content>

<asp:Content ID="floodlightPixelsContent" ContentPlaceHolderID="floodlightPixels" runat="server" Visible="false">
    <!--
    Start of DoubleClick Floodlight Tag: Please do not remove
    Activity name of this tag: Biore Newsletter SignUp
    URL of the webpage where the tag is expected to be placed: http://www.biore.com/en-US/email-newsletter-sign-up
    This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
    Creation Date: 02/07/2012
    -->
    <script type="text/javascript">
        var axel = Math.random() + "";
        var a = axel * 10000000000000;
        document.write('<iframe src="http://fls.doubleclick.net/activityi;src=1418070;type=biore797;cat=biore622;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
    </script>
    <noscript>
    <iframe src="http://fls.doubleclick.net/activityi;src=1418070;type=biore797;cat=biore622;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
    </noscript>

    <!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <uc1:ucForm ID="ucForm" runat="server" />
                <div class="clear"></div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
<script type="text/javascript">
    $(function() {
        if ($(".signUp").length) {
            toggle("noneToggle1");
            toggle("noneToggle2");
            toggle("noneToggle3");
            toggle("noneToggle4");
        }
    });
    function toggle(className) {
        var checkToggle = $("." + className + " input:last");
        checkToggle.click(function() {
            if ($("#" + checkToggle.attr('id') + ":checked").val()) {
                $("." + className + " input").each(function() {
                    if ($(this).attr('id') != checkToggle.attr('id')) {
                        $(this).attr("checked", false);
                        $(this).attr('disabled', 'disabled');
                    }
                });
            } else {
                $("." + className + "  input").each(function() {
                    if ($(this).attr('id') != checkToggle.attr('id')) {
                        $(this).removeAttr('disabled');
                    }
                });
            }
        });
    }
</script>
</asp:Content>