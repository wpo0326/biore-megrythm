﻿var stc = {

    cookieSet: false,
    coinID: null,
    vvVotes: null,
    pfVotes: null,
    totalVotes: null,
    countdown: null,
    hoursLeft: null,

    init: function() {
        $('html').addClass('stcAll js').removeClass('no-js ieX');
        stc.getPollResults();
        stc.fbShare();
        stc.daysRemaining();
        stc.initPollVote();
        stc.learnMoreLinks();
    },

    //----- Animate the jars to the proper levels

    getPollResults: function() {
        $.ajax({
            type: "POST",
            url: "poll.asmx/getPollResults",
            contentType: 'application/json; charset=utf-8',
            // data: what should this be?,
            dataType: "json",
            success: function(data) {
                // Do stuff after data is fetched
                stc.animateJarsHelper(data.d);
            }
        });
    },

    animateJarsHelper: function(data) {
        stc.vvVotes = parseInt(data[0].Votes);
        stc.pfVotes = parseInt(data[1].Votes);
        stc.totalVotes = parseInt(stc.vvVotes + stc.pfVotes);

        // The jar animations are delayed 2.5 seconds before animating up to allow user time to scroll down to see them
        setTimeout('stc.animateJar($("div#vitalVoicesJar"), stc.vvVotes)', 2500);
        setTimeout('stc.animateJar($("div#petfinderJar"), stc.pfVotes)', 2500);
    },

    animateJar: function(jar, currentVote) {
        var $jar = jar,
            $coins = $jar.find('.coinLevel'),
            blocksRemaining = (stc.countdown <= 31) ? Math.floor(parseInt(stc.countdown) / 3) : 10,    // Every 3 days is one 'block', rounded down into whole blocks
            blockHeight = ((1 / 10) * 302),                                                               // Height in pixels of one 3-day block: 1/10 of 302 (the range within which the coins can move)
            percentOfVote = (currentVote / stc.totalVotes),                                             // Vote per charity as a decimal of the total vote
            coinLevelpx = (blocksRemaining * blockHeight + (blockHeight - (blockHeight * percentOfVote))) + 85;

        $coins.animate({
            top: coinLevelpx
        }, 2000);

    },

    //----- Animate Learn More from clicking their links

    learnMoreLinks: function() {

        // Vital Voices Learn More
        $('a#learnMoreVV').toggle(function(e) {
            var $thisLink = $(this);
            // Open
            stc.learnMoreDown($thisLink);
            e.preventDefault();

        }, function(e) {
            var $thisLink = $(this);
            // Close
            stc.learnMoreUp($thisLink, 130);
            e.preventDefault();
        });

        // Petfinder Learn More
        $('a#learnMorePF').toggle(function(e) {
            var $thisLink = $(this);
            // Open
            stc.learnMoreDown($thisLink);
            e.preventDefault();

        }, function(e) {
            var $thisLink = $(this);
            // Close
            stc.learnMoreUp($thisLink, 106);
            e.preventDefault();
        });

    },

    //----- Animate Learn More open

    learnMoreDown: function(link) {
        var $learnMoreLink = link,
            $learnMoreDiv = $learnMoreLink.closest('div.learnMoreDiv');

        $learnMoreLink.text('Close').addClass('open');
        $learnMoreDiv.animate({
            marginTop: 0
        }, 400);
    },

    //----- Animate Learn More closed

    learnMoreUp: function(link, margin) {
        var $learnMoreLink = link,
            $learnMoreDiv = $learnMoreLink.closest('div.learnMoreDiv'),
            negMargin = -margin;

        $learnMoreLink.text('Learn More').removeClass('open');
        $learnMoreDiv.animate({
            marginTop: negMargin
        }, 400);

    },

    //----- Vote

    initPollVote: function() {

        if ($.cookie('theCookie') != null) {
            stc.cookieSet = true;
        }

        if (!stc.cookieSet) {

            $('div.voteOption').on('click', function() {
                var $thisVoteDiv = $(this),
                    $thisInput = $thisVoteDiv.find('input'),
                    $voteContainer = $thisVoteDiv.closest('#poll'),
                    $thisCoin = $thisVoteDiv.find('div.voteCoin');

                stc.coinID = $thisCoin.attr('id');

                $voteContainer.addClass('voted');
                $thisVoteDiv.addClass('votedForThis');
                $thisInput.attr('checked', 'checked');

                var checkedValue = $thisInput.val();
                stc.addVote(checkedValue);

            });

            // Add a rollover state to the coins if you currently have the ability to cast a vote
            $('div.voteCoin').on('mouseover', function() {
                $(this).addClass('hovered');
            }).on('mouseleave', function() {
                $(this).removeClass('hovered');
            });

        } else {
            stc.alreadyVoted();
        }
    },

    addVote: function(pollAnswerID) {
        var pollData = { "pollAnsID": pollAnswerID };

        $.ajax({
            type: "POST",
            url: "poll.asmx/addPollVote",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(pollData),
            dataType: "json",
            success: function(response) {
                // Do stuff after vote is added
                stc.voteAdded(pollAnswerID, response.d);
                stc.setCookie();
                stc.animateCoin(stc.coinID);
            }
        });

    },

    voteAdded: function(pollVote, data) {

        stc.vvVotes = parseFloat(data[0].Votes);
        stc.pfVotes = parseFloat(data[1].Votes);
        stc.totalVotes = (stc.vvVotes + stc.pfVotes);

        var $vvContainer = $('div#vitalVoicesVote'),
            $pfContainer = $('div#petfinderVote'),
            $yourVote = $('span#yourVote'),
            $yourStatus = $('span#yourStatus'),
            $vvVotesTotal = $('span#vvVotesTotal'),
            $pfVotesTotal = $('span#pfVotesTotal'),
            $daysLeftP = $('p#daysLeft'),
            $numDaysLeft = $('span#numDaysLeft'),
            votedForTitle,
            myVotePercent,
            statusInTheLead,
            statusBarelyInTheLead,
            statusBehind,
            statusVeryBehind,
            statusTied;

        //------- Which charity did they vote for

        // If they voted for Vital Voices
        if ($vvContainer.hasClass('votedForThis')) {
            myVotePercent = ((stc.vvVotes / stc.totalVotes) * 100);
            votedForTitle = '<strong>Vital Voices</strong>';
            statusInTheLead = "&mdash;and thanks to you, they're in the lead. Come back tomorrow to continue supporting this cause!";
            statusBarelyInTheLead = ". They're in the lead, but the Petfinder Foundation isn't far behind! Share with friends to keep Vital Voices loud and proud.";
            statusBehind = "&mdash;but the Petfinder Foundation has the most support! Help Vital Voices catch up and share with friends to let them be heard.";
            statusTied = ". They're tied with the Petfinder Foundation! Help them get in the lead by coming back tomorrow or sharing with friends!";

            // If they voted for Petfinder
        } else if ($pfContainer.hasClass('votedForThis')) {
            myVotePercent = ((stc.pfVotes / stc.totalVotes) * 100);
            votedForTitle = 'the <strong>Petfinder Foundation</strong>';
            statusInTheLead = "&mdash;and thanks to you, they're at the head of the pack. Come back tomorrow to continue supporting this cause!";
            statusBarelyInTheLead = ". They're in the lead, but Vital Voices isn't far behind! Share with friends to keep the Petfinder Foundation running ahead.";
            statusBehind = "&mdash;but Vital Voices has the most support! Help the Petfinder Foundation catch up by sharing with friends.";
            statusTied = ". They're tied with Vital Voices! Help them get in the lead by coming back tomorrow or sharing with friends!";
        }

        statusVeryBehind = ". They're far behind, but you can help them get in the lead by coming back tomorrow or sharing with friends!";

        //------- Status results

        // Echo which charity they voted for
        $yourVote.html(votedForTitle);

        // Echo that charity's status in the running
        if (myVotePercent < 25) {
            $yourStatus.html(statusVeryBehind);
        }

        else if (myVotePercent >= 25 && myVotePercent < 50) {
            $yourStatus.html(statusBehind);
        }

        else if (myVotePercent === 50) {
            $yourStatus.html(statusTied);
        }

        else if (myVotePercent > 50 && myVotePercent < 65) {
            $yourStatus.html(statusBarelyInTheLead);
        }

        else if (myVotePercent >= 65) {
            $yourStatus.html(statusInTheLead);
        }

        // Echo vote counts
        $vvVotesTotal.text(stc.vvVotes);
        $pfVotesTotal.text(stc.pfVotes);

        // If there are "days" left to vote, say so. If there is only 1 day left, drop the plural
        if (stc.countdown >= 2) {
            $numDaysLeft.text(stc.countdown + ' days');
        } else if (stc.countdown === 1) {
            $numDaysLeft.text(stc.countdown + ' day');
        }

        // If there are less than 24 hours left to vote, print out a special sentence saying that today is the last day to vote
        if (stc.hoursLeft <= 24 && stc.countdown < 1) {
            $daysLeftP.html('Today is the last day to show your support!');
        }

    },

    //----- Figure out how many days are left to vote between today and 8/24/2012

    daysRemaining: function() {
        // number of days left between today and 8/24/2012
        var today = new Date(),
            endDate = new Date(today.getFullYear(), 8 - 1, 24, 23, 59, 59, 0),
            hours = 1000 * 60 * 60,
            oneDay = 1000 * 60 * 60 * 24;

        // endDateEDT = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate() + 1, 0, 0, 59);

        stc.countdown = Math.floor((endDate.getTime() - today.getTime()) / oneDay);
        stc.hoursLeft = Math.floor((endDate.getTime() - today.getTime()) / hours);

    },

    //----- If voting, animate the coin falling into the appropriate jar and then call the modal

    animateCoin: function(coinID) {

        var $thisCoin = $('div#' + coinID),
            $thisArrow = $thisCoin.closest('div.coinDropContainer').siblings('div.arrow');

        if (coinID === 'voteVitalVoices') {
            $thisCoin.animate({
                crSpline: $.crSpline.buildSequence([
                    [186, 7],
                    [80, 20],
                    [60, 96],
                    [60, 630]
                ])
            },
                1600
            );
        } else if (coinID === 'votePetfinder') {
            $thisCoin.animate({
                crSpline: $.crSpline.buildSequence([
                    [0, 7],
                    [114, 20],
                    [134, 96],
                    [134, 630]
                ])
            },
                1600
            );
        }

        $thisArrow.fadeOut(400);

        // The IEs wait longer to play the sound than Firefox and Chrome; browser issue, not HTML5 vs. Flash issue.
        if ($.browser.msie) {
            stc.playSound();
        } else {
            setTimeout('stc.playSound()', 1000);
        }

        setTimeout('stc.voteModal(false)', 1000);    /* False because they haven't voted previously today */

    },

    //----- Sound of the coin dropping onto other coins in the jar

    playSound: function() {
        $('#jplayer').jPlayer({
            ready: function() {
                $(this)
                    .jPlayer('setMedia', { mp3: 'js/coin-drop-to-other-coins.mp3' })
                    .jPlayer('play');
            },
            preload: true,
            swfPath: 'js/'
        });
    },

    //----- If already voted, can't vote again until the cookie expires

    alreadyVoted: function() {
        $('div.voteOption').each(function() {
            var $voteOption = $(this),
                $inputs = $voteOption.find('input');

            $inputs.remove();

            $voteOption
            .off('click')
            .on('click', function() {
                var $vvVotesTotal = $('span.vvVotesTotal'),
                    $pfVotesTotal = $('span.pfVotesTotal');

                $vvVotesTotal.text(stc.vvVotes);
                $pfVotesTotal.text(stc.pfVotes);

                stc.voteModal(true);    /* True because they want the Try Tomorrow message; already voted previously today */
            });

            // Clear and remove rollover states on voting coins
            $('div.voteCoin').removeClass('hovered').off('mouseover').off('mouseleave');

        });
    },

    //----- Set a cookie if voted and expire the cookie at 12:01 AM EDT the next day

    setCookie: function() { 
        var curTime = new Date(),
            expTime = new Date(),
            tmpYear = curTime.getUTCFullYear(),
            tmpMonth = curTime.getUTCMonth(),
            tmpDate = curTime.getUTCDate(),
            tmpHrs = curTime.getUTCHours(),
            tmpMin = curTime.getUTCMinutes(),
            localExpTime;
    
        if (curTime.getUTCHours() > 4 || (curTime.getUTCHours() == 4 && curTime.getUTCMinutes() >= 1)) {
            expTime.setUTCFullYear(tmpYear,tmpMonth,tmpDate+1);
            expTime.setUTCHours(4,1,0,0);
        } else {
            expTime.setUTCFullYear(tmpYear,tmpMonth,tmpDate);
            expTime.setUTCHours(4,1,0,0);
        }    
        
        localExpTime = new Date(expTime.toUTCString());

        $.cookie('theCookie', 'youVoted', { expires: localExpTime });

        stc.cookieSet = true;
        stc.alreadyVoted();
    },

    //----- Voting Modal Dialog

    voteModal: function(votedPreviously) {
        var $modal = $('div#entireModal'),
            $modalOverlay = $modal.find('div#voteModalOverlay'),
            $MsgVoteSuccess = $('div#voteSuccess'),
            $MsgTryTomorrow = $('div#tryTomorrow'),
            $MsgDaysLeft = $('p#daysLeft'),
            $vvVotesTotal = $('span#vvVotesTotal'),
            $pfVotesTotal = $('span#pfVotesTotal'),
            $closeModal = $('a#closeModal');

        // Highlight the currently-winning vote
        if (stc.vvVotes > stc.pfVotes) {
            // Vital Voices winning
            $vvVotesTotal.addClass('winning');
            $pfVotesTotal.removeClass('winning');

        } else if (stc.vvVotes < stc.pfVotes) {
            // Petfinder winning
            $vvVotesTotal.removeClass('winning');
            $pfVotesTotal.addClass('winning');

        } else if (stc.vvVotes === stc.pfVotes) {
            // Tied
            $vvVotesTotal.addClass('winning');
            $pfVotesTotal.addClass('winning');
        }

        // If they try to click to vote but they've already voted before within the day
        if (votedPreviously) {
            $MsgDaysLeft.hide();
            $MsgVoteSuccess.hide();
            $MsgTryTomorrow.show();

            // If they successfully submitted a vote and are seeing vote results
        } else if (!votedPreviously) {
            $MsgTryTomorrow.hide();
            $MsgDaysLeft.show();
            $MsgVoteSuccess.show();
        }

        $modal.fadeIn(350);

        $closeModal.on('click', function(e) {
            $modal.fadeOut(350);
            e.preventDefault();
        });

        $modalOverlay.on('click', function(e) {
            $modal.fadeOut(350);
            e.preventDefault;
        });
    },

    //----- Facebook Share

    fbShare: function() {

        $('a#fbShareVitalVoices').on('click', function(e) {
            var url = socialSharing.shareVitalVoices();
            socialSharing.openWindow(url);
            e.preventDefault();
        });

        $('a#fbSharePetfinder').on('click', function(e) {
            var url = socialSharing.sharePetfinder();
            socialSharing.openWindow(url);
            e.preventDefault();
        });
    }

}

//----- Social Sharing

var socialSharing = {

    openWindow: function(url) {
        window.open(url, 'feedDialog', 'toolbar=0,status=0,width=580,height=400');
        return false;
    },

    shareVitalVoices: function() {
        // Reference for the Feed Dialog:
        // http://developers.facebook.com/docs/reference/dialogs/feed/

        var url = 'http://www.facebook.com/dialog/feed?app_id=' + FBVars.fbAppId +
			'&link=' + FBVars.FBShareUrl +
			'&picture=' + FBVars.baseURL + 'images/ChangeForChange_Share50x50.jpg' +
			'&name=' + encodeURIComponent(' ') +
			'&caption=' + encodeURIComponent('Support Vital Voices for Bioré® Skincare—See The Change') +
			'&description=' + encodeURIComponent('Bioré® Skincare is raising money to help support a great cause—and Vital Voices needs your help! Deposit "coins" for the organization you most want to support, and your daily contributions will ultimately choose which charity will receive a donation from Bioré® Skincare.') +
			'&redirect_uri=' + FBVars.baseURL + 'PopupClose.aspx' +
			'&display=popup';
        return url;
    },

    sharePetfinder: function() {
        var url = 'http://www.facebook.com/dialog/feed?app_id=' + FBVars.fbAppId +
            '&link=' + FBVars.FBShareUrl +
            '&picture=' + FBVars.baseURL + 'images/ChangeForChange_Share50x50.jpg' +
            '&name=' + encodeURIComponent(' ') +
            '&caption=' + encodeURIComponent('Support Petfinder Foundation for Bioré® Skincare—See The Change') +
            '&description=' + encodeURIComponent('Bioré® Skincare is raising money to help support a great cause—and the Petfinder Foundation needs your help! Deposit "coins" for the organization you most want to support, and your daily contributions will ultimately choose which charity will receive a donation from Bioré® Skincare.') +
            '&redirect_uri=' + FBVars.baseURL + 'PopupClose.aspx' +
            '&display=popup';
        return url;
    }
}

// Run on document ready

$(function() {
    $('a.videoLink').ENLvideo({
        flashPrefix: '../../flash/',                    /* Flash player relative to this file */
        flashVideoPrefix: '../facebook/SeeTheChange/',  /* relative from the player URL to the video link URL on the page */
        lightbox: true,
        hideVideoLink: false,
        lightboxOverlayOpacity: 0
    });
});