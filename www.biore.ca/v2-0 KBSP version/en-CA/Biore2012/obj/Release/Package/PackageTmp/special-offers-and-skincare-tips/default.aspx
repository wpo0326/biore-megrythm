﻿<%@ Page Title="Special Offers, New Products &amp; Skincare Tips | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.new_products_and_offers._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Skincare tips, special offers and new products from Bioré® Skincare" name="description" />
    <meta content="skincare tips, take care of your skin, new product" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/whatsNew.css")
        .Render("~/css/combinednew_#.css")
    %>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">
<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: Biore Whats New Page
URL of the webpage where the tag is expected to be placed: http://www.biore.com/en-US/special-offers-and-skincare-tips/
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 02/01/2012
-->
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="http://fls.doubleclick.net/activityi;src=1418070;type=biore797;cat=biore504;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="http://fls.doubleclick.net/activityi;src=1418070;type=biore797;cat=biore504;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>

<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="intro">
                <h1>What's New</h1>
                <p>Our latest and greatest products and news.</p>
            </div>
            <div id="promos">
                <ul>
                    <li id="promo2" class="promo">
                        <div class="promoImage"><img src="../images/whatsNew/promoImgProveIt.png" alt="" /></div>
                        <div class="promoContent">
                            <h3 class="promoHeader">Get Rewarded</h3>
                            <p class="promoBody">Build up Prove It!<sup>&reg;</sup> Reward Points on Facebook towards free products.</p>
                            <div class="promoButton"> <asp:HyperLink ID="proveItPromoLink" NavigateUrl="http://www.facebook.com/bioreskin?sk=app_205787372796203" runat="server" Target="_blank">Start Earning <span class="arrow">&rsaquo;</span></asp:HyperLink></div>
                        </div>
                    </li>
                </ul>                
            </div>            
            <div id="polaroids">
                <img src="../images/whatsNew/whatsNewPolaroids.jpg" alt="" />
            </div>
            <%--
            <p id="goAnywhereRules">
                * NO PURCHASE NECESSARY.  Legal residents of the 50 United States (D.C.) 18 years and older.  Ends 4/30/12.  To enter and for Official Rules, including odds and prize descriptions, visit <a href="http://www.biore.com/en-US/promotions/rules/prove-it-vacation-giveaway.aspx" target="_blank">http://www.biore.com/en-US/promotions/rules/prove-it-vacation-giveaway.aspx</a>.  Void where prohibited.
            </p>
            --%>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
