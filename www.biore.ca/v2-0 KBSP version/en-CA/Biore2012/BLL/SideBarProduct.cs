﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace Biore2012.BLL
{
    public class SideBarProduct
    {
        /// <summary>
		/// The Sidebar product Image
		/// </summary>
		[XmlElement("Image")]
        public string Image;
        
        /// <summary>
        /// The Sidebar product Name
		/// </summary>
		[XmlElement("ProductName")]
        public string ProductName;

        /// <summary>
        /// The Sidebar product URL
		/// </summary>
		[XmlElement("ProductURL")]
        public string ProductURL;

        /// <summary>
        /// The Sidebar product URL
        /// </summary>
        [XmlElement("ClassName")]
        public string ClassName;

        /// <summary>
        /// The Sidebar product URL
        /// </summary>
        [XmlElement("CategoryArrowSmallProduct")]
        public string CategoryArrowSmallProduct;

        public SideBarProduct()
        {

        }

        public SideBarProduct(string image, string productName, string productURL, string className, string categoryArrowSmallProduct)
        {
            Image = image;
            ProductName = productName;
            ProductURL = productURL;
            ClassName = className;
            CategoryArrowSmallProduct = categoryArrowSmallProduct;

        }
    }
}
