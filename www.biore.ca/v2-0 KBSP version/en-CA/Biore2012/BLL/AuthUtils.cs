﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Web.Security;
using System.Text;
using System.IO;

namespace Biore2012.BLL
{
    public class AuthUtils
    {
        public MembershipUser FetchUser(String Username)
        {
            MembershipUser u;
            u = Membership.GetUser(Username, false);
            return u;
        }

        public MembershipUser fetchUserByEmail(String Email)
        {
            MembershipUserCollection users = Membership.FindUsersByEmail(Email);
            foreach (MembershipUser theUser in users)
            {
                return theUser;
            }
            return null;
        }

        public bool CheckIfAuthenticated()
        {
            return HttpContext.Current.Request.IsAuthenticated;
        }

        public void CreateUser(String Username, String Password, String Email)
        {
            Membership.CreateUser(Username, Password, Email);
        }

        public bool LogInUser(String Username, String Password)
        {
            if (Membership.ValidateUser(Username, Password))
            {
                FormsAuthentication.SetAuthCookie(Username, false);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool LogInUserByEmail(String Email, String Password)
        {
            MembershipUserCollection users = Membership.FindUsersByEmail(Email);
            if (users.Count > 0)
            {
                foreach (MembershipUser theUser in users)
                {
                    return LogInUser(theUser.UserName, Password);
                }
                return false;
            }
            else
            {
                return false;
            }
        }

        public void LogOutUser()
        {
            FormsAuthentication.SignOut();
        }

        public String GetCurrentUserID()
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                MembershipUser myObject = Membership.GetUser();
                String UserID = myObject.ProviderUserKey.ToString();
                return UserID;
            }
            else
            {
                return "0";
            }
        }

        public String GetCurrentUserName()
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                MembershipUser myObject = Membership.GetUser(HttpContext.Current.User.Identity.Name);
                String UserName = myObject.UserName;
                return UserName;
            }
            else
            {
                return "0";
            }
        }

        public Boolean testUniqueEmail(String Email)
        {
            if (Membership.FindUsersByEmail(Email).Count == 0) return true;
            else return false;
        }

        public Boolean testUniqueUsername(String Username)
        {
            if (Membership.FindUsersByName(Username).Count == 0) return true;
            else return false;
        }

        public XmlDocument getUserInfoXML(String lookup, String method)
        {
            MembershipUser theUser = null;
            String status = "0";
            String message = "failed";
            String email = "";
            switch (method)
            {
                case "username":
                    theUser = FetchUser(lookup);
                    status = "1";
                    message = "success";
                    email = theUser.Email;
                    break;
                case "email":
                    theUser = fetchUserByEmail(lookup);
                    status = "1";
                    message = "success";
                    email = theUser.Email;
                    break;
                case "failed":
                    break;
            }

            StringBuilder sb = new StringBuilder();
            XmlTextWriter writer = new XmlTextWriter(new StringWriter(sb));

            writer.WriteStartDocument();
            writer.WriteStartElement("data");
            writer.WriteStartElement("result");

            writer.WriteStartElement("status");
            writer.WriteString(status);
            writer.WriteEndElement();

            writer.WriteStartElement("message");
            writer.WriteString(message);
            writer.WriteEndElement();

            writer.WriteStartElement("email");
            writer.WriteString(email);
            writer.WriteEndElement();

            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndDocument();

            writer.Flush();

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(sb.ToString());
            return xmlDocument;
        }

    }
}
