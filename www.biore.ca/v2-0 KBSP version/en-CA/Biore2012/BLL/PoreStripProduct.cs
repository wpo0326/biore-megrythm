﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.Xml;
using System.Collections;

namespace Biore2012.BLL
{
    [System.Web.Script.Services.ScriptService]
    [XmlRoot("Product")]
    public class PoreStripProduct
    {
        /// <summary>
        /// NewLookImage is the path to the new packaging product shot
        /// </summary>
        [XmlElement("NewLookImage")]
        public string NewLookImage;

        /// <summary>
        /// OldLookImage is the path to the old packaging product shot
        /// </summary>
        [XmlElement("OldLookImage")]
        public string OldLookImage;

        /// <summary>
        /// SocialImage is the path to the social packaging product shot
        /// </summary>
        [XmlElement("SocialImage")]
        public string SocialImage;


        /// <summary>
        /// Name
        /// </summary>
        [XmlElement("Name")]
        public string Name { get; set; }

        /// <summary>
        /// Description
        /// </summary>        
        [XmlElement("Description")]
        public string Description { get; set; }

        /// <summary>
        /// BuyNowURL
        /// </summary>        
        [XmlElement("BuyNowURL")]
        public string BuyNowURL { get; set; }

        /// <summary>
        /// LikeURL
        /// </summary>        
        [XmlElement("LikeURL")]
        public string LikeURL { get; set; }

        /// <summary>
        /// WhatItIs
        /// </summary>        
        [XmlElement("WhatItIs")]
        public string WhatItIs { get; set; }

        /// <summary>
        /// Ingredients
        /// </summary>        
        [XmlElement("ProductDetails")]
        public string ProductDetails { get; set; }

        /// <summary>
        /// FunFacts
        /// </summary>        
        [XmlElement("FunFacts")]
        public string FunFacts { get; set; }

        /// <summary>
        /// HowItWorks
        /// </summary>        
        [XmlElement("HowItWorks")]
        public string HowItWorks { get; set; }

        /// <summary>
        /// Cautions
        /// </summary>        
        [XmlElement("Cautions")]
        public string Cautions { get; set; }

        /// <summary>
        /// Adsense Block
        /// </summary>        
        [XmlElement("AdSense")]
        public string AdSense { get; set; }

        public PoreStripProduct() 
        {
        } 
    }
}
