﻿<%@ Page Title="Where To Buy | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.wheretobuy._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
	#storeLogos ul li { width:240px; height:120px; float:left; display:block; text-align:center;}
    #storeLogos ul li a {width:180px; height:100px; background-position:center left;text-indent:-9999px; display:block;}
	#storeLogos ul li a:hover { background-position:right;}
	</style>
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1>Where To Buy</h1>
                    <div id="storeLogos" class="logos">
                        <p>Shop for Bior&eacute;<sup>&reg;</sup> products at a retailer near you:<br /><br /><br /></p>
                        <ul>
							<li><a style="background-image: url(../images/whereToBuy/amazon.png);" href="http://www.amazon.ca" target="wtb">amazon</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/brunet.png);" href="http://www.brunet.ca/en/homepage.html" target="wtb">brunet</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/familiprix.png);" href="http://www.familiprix.com/default.aspx">familiprix</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/jean-coutu.png);" href="http://www.jeancoutu.com/en" target="wtb">jean-coutu</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/london-drugs.png);" href="http://www.londondrugs.com" target="wtb">london-drugs</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/overwaitea.png);" href="http://www.overwaitea.com/" target="wtb">overwaitea</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/pharmaprix.png);" href="http://www1.pharmaprix.ca/fr/Home" target="wtb">pharmaprix</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/price-smart-foods.png);" href="http://www.pricesmartfoods.ca/" target="wtb">price-smart-foods</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/rexall1.png);" href="http://www.rexall.ca/" target="wtb">rexall</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/rexall.png);" href="http://www.rexall.ca/" target="wtb">rexall</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/safeway.png);" href="http://www.safeway.ca/" target="wtb">safeway</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/saveonfoods.png);" href="http://www.saveonfoods.com" target="wtb">saveonfoods</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/drug-mart.png);" href="http://www1.shoppersdrugmart.ca/en/Home" target="wtb">drug-mart</a></li>
                            <!--<li><a style="background-image: url(../images/whereToBuy/target.png);" href="http://www.target.ca/en/" target="wtb">Target</a></li>-->
                            <li><a style="background-image: url(../images/whereToBuy/uniprix.png);" href="http://www.uniprix.com/en/" target="wtb">uniprix</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/walmart.png);" href="http://www.walmart.ca/search/biore" target="wtb">walmart</a></li>
                                                        <!--<li><a style="background-image: url(../images/whereToBuy/loblaws.png);" href="http://www.loblaws.ca/" target="wtb">Loblaws</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/rass.png);" href="http://www.atlanticsuperstore.ca/" target="wtb">Rass</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/maxi.png);" href="http://www.maxi.ca/en_CA.html" target="wtb">MaxiandCie</a></li>
                            <li><a style="background-image: url(../images/whereToBuy/rcss.png);" href="http://www.realcanadiansuperstore.ca/" target="wtb">RCSS</a></li>-->
							<li><a style="background-image: url(../images/whereToBuy/well.png);" href="https://well.ca/brand/biore.html?utm_source=brand&utm_medium=brand-wheretobuy&utm_campaign=biore" target="_blank">Well.ca</a></li>
                        </ul>                
                    </div>
                    <div style="clear:both; height:0px;"></div>
            </div>
        </div>
    </div>
</asp:Content>