﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Biore2012.BLL;
using Biore2012.DAL;
using System.Configuration;
using System.Web.UI.HtmlControls;
using KaoBrands.FormStuff;
using KAOForms;

//Add Hybris post 5/17/18 -MCT

namespace Biore2012.FormConfig
{
    public partial class NewsletterForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            OptinFormResults.Visible = false;
            OptinError.Visible = false;

            Page.Header.DataBind();

            // Grab masterpage, set appropriate body classes
            Site myMaster = (Site)this.Master;
            myMaster.bodyClass += " forms signUp";

            if (Request.Url.ToString().Contains("email-newsletter-sign-up")) myMaster.bodyClass += " signUp";
            else if (Request.Url.ToString().Contains("contact-us")) myMaster.bodyClass += " contactUs";


            // This turns on and off the Question Pro Include for this page...
            ((Panel)Page.Master.FindControl("panel_Question_Pro")).Visible = false;

            CheckHookID();

            DateTime dt = DateTime.Now;
            BioreUtils ut = new BioreUtils();

            ut.queueNumber(dt.Year - 100, dt.Year - 13, "Year", yyyy); //Build Year
            ut.queueNumber(1, 13, "Mon", mm); //Build Day
            ut.queueNumber(1, 32, "Day", dd); //Build Month

        }

        public void Step1_ButtonClick(Object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                ProcessSubmission();
            }

        }

        protected string returnCheckBoxListAnswers(CheckBoxList cbl)
        {
            string strChkList = "";
            foreach (ListItem li in cbl.Items)
            {
                if (li.Selected)
                    strChkList += li.Text + ",";
            }

            return strChkList;
        }

        protected void DOBValidate(object source, ServerValidateEventArgs args)
        {
            try
            {

                int Day = int.Parse(dd.SelectedValue.ToString());
                int Month = int.Parse(mm.SelectedValue.ToString());
                int Year = int.Parse(yyyy.SelectedValue.ToString());

                DateTime Test = new DateTime(Year, Month, Day);

                //Are they 13 years of age or older?
                if (Test > DateTime.Now.AddYears(-13))
                {
                    args.IsValid = false;
                    //redirect to sorry page
                    //Response.Redirect("sorry.aspx");
                }
                else
                {
                    args.IsValid = true;
                }

            }
            catch (FormatException Arg)
            {
                string error = Arg.ToString();
                // item not selected (couldn't convert int)
                args.IsValid = false;
            }
            catch (ArgumentOutOfRangeException Arg)
            {
                string error = Arg.ToString();
                // invalid date (31 days in February)
                args.IsValid = false;
            }
        }

        protected string GetAgeGroup(string DOB)
        {
            string strReturn = "";

            DateTime dob = Convert.ToDateTime(DOB);

            DateTime Now = DateTime.Now;
                int Years = new DateTime(DateTime.Now.Subtract(dob).Ticks).Year - 1;
                DateTime PastYearDate = dob.AddYears(Years);
            //int Months = 0;
            //for (int i = 1; i <= 12; i++)
            //{
            //if (PastYearDate.AddMonths(i) == Now)
            //{
            //Months = i;
            //break;
            //}
            // else if (PastYearDate.AddMonths(i) >= Now)
            //{
            //Months = i - 1;
            //break;
            //}
            //}
            //int Days = Now.Subtract(PastYearDate.AddMonths(Months)).Days;
            //int Hours = Now.Subtract(PastYearDate).Hours;
            //int Minutes = Now.Subtract(PastYearDate).Minutes;
            //int Seconds = Now.Subtract(PastYearDate).Seconds;
            //16-18, 19-25, 26-45, 46-60, Over 60

            if (Years >= 16 && Years < 19)
            {
                strReturn = "16-18";
            }
            else if (Years >= 19 && Years < 26)
            {
                strReturn = "19-25";
            }
            else if (Years >= 26 && Years < 46)
            {
                strReturn = "26-45";
            }
            else if (Years >= 46 && Years < 61)
            {
                strReturn = "45-60";
            }
            else if (Years > 60)
            {
                strReturn = "Over 60";
            }

            return strReturn;

        }
        protected Boolean IsActiveEvent(string strEventID)
        {
            bool blnReturn = true;

            //if event has either not started or has ended, show inactive message.
            if (CGOptin.EventHasStarted(strEventID) == false || CGOptin.EventHasEnded(strEventID) == true)
            {
                blnReturn = false;
            }

            //check to see if sample count has been reached.
            if (CGOptin.checkSampleCount(System.Configuration.ConfigurationManager.AppSettings["EventId_sample"].ToString()) >= Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["sampletotal"].ToString()))
            {
                //no more samples
                blnReturn = false;
            }

            return blnReturn;
        }

        public void CheckHookID()
        {
            //Find hook id passed in query string, if available.  This is a means by which we can track promo source (e.g. Facebook)
            string strHookIDRequest = Request.QueryString["hook"] == null ? System.Configuration.ConfigurationManager.AppSettings["HookId"].ToString() : Request.QueryString["hook"].ToString();

            hfHookID.Value = strHookIDRequest;
        }


        protected void optinValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (bioreoptin.Checked == true);
        }
        
        protected void ProcessSubmission()
        {
            string strEntryReturn = "";
            string strTraitReturn = "";
            string strOptinReturn = "";
            string strHybrisReturn = "";

            Page.Validate();

            if (Page.IsValid && FrstName.Text.Trim() == "") //FrstName is a honeypot trap, so make sure it's blank before we proceed.
            {
                OptinForm.Visible = false;
                string DOB = mm.SelectedValue + "/" + dd.SelectedValue + "/" + yyyy.SelectedValue;

                string strFName = FName.Text;
                string strLName = LName.Text;
                string strEmail = Email.Text;
                string strPhone = "";
                string strMobilePhone = "";
                string strStreet1 = Address1.Text;
                string strStreet2 = Address2.Text;
                string strCity = City.Text;
                string strProvince = State.SelectedItem.ToString();
                string strPostalCode = PostalCode.Text;
                string strPrimaryLanguage = PreferredLanguage.SelectedValue.ToString(); //EN or FR preferred language
                string strGender = Gender.SelectedValue.ToString();
                string strDOBdd = dd.SelectedValue;
                string strDOBmo = mm.SelectedValue;
                string strDOByr = yyyy.SelectedValue;
                string strDOB = DOB;
                string strDOBHybris = strDOByr + "-" + strDOBmo + "-" + strDOBdd;
                string strDOBAgeGroup = GetAgeGroup(DOB);
                string strDateTimeZulu = DateTime.UtcNow.ToString("s") + "+0000"; //DateTime.Now.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");

                //segmentation questions
                string strWhatSkinConcerns = WhatSkinConcerns.SelectedItem.ToString();
                string strWhatFaceCareBrand = returnCheckBoxListAnswers(WhichFaceCleanserBrands);
                string strWhichDoYouUse = returnCheckBoxListAnswers(WhichDoYouUse);


                //optin info
                bool blnMultiOptin = MultiOptin.Checked ? true : false;
                //bool blnBanOptin = banoptin.Checked ? true : false;
                bool blnBioreOptin = bioreoptin.Checked ? true : false;
                bool blnCurelOptin = cureloptin.Checked ? true : false;
                bool blnJergensOptin = jergensoptin.Checked ? true : false;
                //bool blnJohnFriedaOptin = johnfriedaoptin.Checked ? true : false;

                if (blnBioreOptin)
                    blnMultiOptin = true;

                int intSiteID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SiteID"]);

                int intBioreEventID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["EventId_signup_Biore"].ToString());
                string strBioreEventCode = System.Configuration.ConfigurationManager.AppSettings["EventCode_signup_Biore"].ToString();
                int intBioreSiteID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SiteID_Biore"].ToString());

                int intCurelEventID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["EventId_signup_Curel"].ToString());
                string strCurelEventCode = System.Configuration.ConfigurationManager.AppSettings["EventCode_signup_Curel"].ToString();
                int intCurelSiteID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SiteID_Curel"].ToString());

                int intJergensEventID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["EventId_signup_Jergens"].ToString());
                string strJergensEventCode = System.Configuration.ConfigurationManager.AppSettings["EventCode_signup_Jergens"].ToString();
                int intJergensSiteID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SiteID_Jergens"].ToString());

                /*
                int intJFEventID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["EventId_signup_JF"].ToString());
                string strJFEventCode = System.Configuration.ConfigurationManager.AppSettings["EventCode_signup_JF"].ToString();
                int intJFSiteID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SiteID_JF"].ToString());
                */

                string strURL = Request.RawUrl == null ? "" : Request.RawUrl.ToString();
                string strRemoteAddr = Request.ServerVariables["REMOTE_ADDR"] == null ? "" : Request.ServerVariables["REMOTE_ADDR"].ToString();
                string strUserAgent = Request.ServerVariables["HTTP_USER_AGENT"] == null ? "" : Request.ServerVariables["HTTP_USER_AGENT"].ToString(); ;

                string strContactContent = "";
                string strContactVia = "";
                string strContactFreq = "";

                //INSERT OPT-IN VALUES IF USER OPTED INTO AT LEAST ONE NEWSLETTER.
                if (blnMultiOptin)
                {
                    strContactContent = "samples,contests,sweepstakes,other info,";
                    strContactVia = "email,direct mail,";
                    strContactFreq = "as needed,";
                }

                string strHookID = hfHookID.Value;

                //PROCESS SAMPLES ENTRY.
                strEntryReturn = CGOptin.ProcessOptIn(intBioreSiteID, intBioreEventID, strFName, strLName, strEmail, strDOBdd, strDOBmo, strDOByr,
                                      strStreet1, strStreet2, strProvince, strCity, strPostalCode, strPrimaryLanguage, strPhone, strMobilePhone,
                                      strGender, strURL, strRemoteAddr, strUserAgent, strBioreEventCode,
                                      strContactContent, strContactVia, strContactFreq, strURL.Replace("http://", ""), "", strHookID);


                //PROCESS SEGMENTATION TRAIT QUESTIONS and OPT-INS.
                if (strEntryReturn.IndexOf("TRUE") != -1)
                {
                    //do this for each individual trait question
                    strTraitReturn = CGOptin.ProcessTraits(Convert.ToInt32(strEntryReturn.Replace("TRUE", "")), intBioreEventID, strRemoteAddr, strUserAgent, strURL, Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["traitID_WhatSkinConcerns"]), strWhatSkinConcerns);
                    strTraitReturn = CGOptin.ProcessTraits(Convert.ToInt32(strEntryReturn.Replace("TRUE", "")), intBioreEventID, strRemoteAddr, strUserAgent, strURL, Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["traitID_WhichFaceCleanserBrands"]), strWhatFaceCareBrand);
                    strTraitReturn = CGOptin.ProcessTraits(Convert.ToInt32(strEntryReturn.Replace("TRUE", "")), intBioreEventID, strRemoteAddr, strUserAgent, strURL, Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["traitID_WhichDoYouUse"]), strWhichDoYouUse);

                    //apply all opt-ins.
                    //PROCESS ALL OPT-INS.

                    if (blnMultiOptin)
                    {
                        
                        if (blnCurelOptin)
                        {
                            strOptinReturn = CGOptin.ProcessOptIn(intCurelSiteID, intCurelEventID, strFName, strLName, strEmail, strDOBdd, strDOBmo, strDOByr,
                                          strStreet1, strStreet2, strProvince, strCity, strPostalCode, strPrimaryLanguage, strPhone, strMobilePhone,
                                          strGender, strURL, strRemoteAddr, strUserAgent, strCurelEventCode,
                                          strContactContent, strContactVia, strContactFreq, strURL.Replace("http://", ""), "", strHookID);
                        }
                        if (blnJergensOptin)
                        {
                            strOptinReturn = CGOptin.ProcessOptIn(intJergensSiteID, intJergensEventID, strFName, strLName, strEmail, strDOBdd, strDOBmo, strDOByr,
                                          strStreet1, strStreet2, strProvince, strCity, strPostalCode, strPrimaryLanguage, strPhone, strMobilePhone,
                                          strGender, strURL, strRemoteAddr, strUserAgent, strJergensEventCode,
                                          strContactContent, strContactVia, strContactFreq, strURL.Replace("http://", ""), "", strHookID);
                        }
                        
                    }

                    strHybrisReturn = Hybris.JsonPost("{ \"id\":\"" + strEmail + "\",\"timestamp\":\"" + strDateTimeZulu + "\",\"idOrigin\":\"CONSUMER_INFO\",\"contactType\":\"REGD\",\"addressLine1\":\"" + strStreet1 + "\",\"addressLine2\":\"" + strStreet2 + "\",\"addressLine3\":\"\",\"city\":\"" + strCity + "\",\"state\":\"" + strProvince + "\",\"country\":\"CA\",\"postCode\":\"" + strPostalCode + "\",\"emailAddress\":\"" + strEmail + "\",\"emailOptin\":\"Y\",\"firstName\":\"" + strFName + "\",\"gender\":\"" + strGender + "\",\"lastName\":\"" + strLName + "\",\"phoneNumber\":\"" + strPhone + "\",\"phoneOptin\":\"Y\",\"mobileNumber\":\"" + strMobilePhone + "\",\"mobileOptin\":\"N\",\"title\":\"\",\"dateOfBirth\":\"" + strDOBHybris + "\",\"ageGroup\":\"" + strDOBAgeGroup + "\",\"frequencyOfSubscription\":\"4\",\"channelSource\":\"WEB\",\"marketingLocationId\":\"store_gb\",\"activitySource\":\"\",\"subscriptionPreference\":\"\",\"language\":\"" + strPrimaryLanguage + "\",\"action\":\"newsletter\",\"marketingArea\":\"BIORE_CA\",\"marketingAttributes\":[{\"marketingAttributeCategory\":\"SkinType\",\"marketingAttributeValue\":\"" + strWhatSkinConcerns.Replace(" ", "_").TrimEnd(',') + "\"},{\"marketingAttributeCategory\":\"BioreProductsPast\",\"marketingAttributeValue\":\"" + strWhatFaceCareBrand.Replace(" ", "_").TrimEnd(',') + "\"}, {\"marketingAttributeCategory\":\"BioreProductsCurrent\",\"marketingAttributeValue\":\"" + strWhichDoYouUse.Replace(" ", "_").TrimEnd(',') + "\"} ]}");

                }

                //if all processes were true OR the user has already entered, give the generic thanks message.
                //if ((strEntryReturn.IndexOf("TRUE") != -1 && strTraitReturn == "TRUE"))
                if ((strEntryReturn.IndexOf("TRUE") != -1 && strTraitReturn == "TRUE" && strHybrisReturn == "TRUE"))
                {
                    OptinFormResults.Visible = true;
                    OptinForm.Visible = false;
                    //Response.Redirect("thank-you.aspx");
                }
                else
                {
                    //Oops!
                    litError.Text = "<span style=\"color:#C00\">An error has occurred.<br />" + strEntryReturn + "<br /><br />" + strTraitReturn + "<br /><br />" + strHybrisReturn + "</span>";
                    OptinError.Visible = true;
                }
            }

        }
    }
         
}
