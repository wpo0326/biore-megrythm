﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ChangePassword.aspx.cs" Inherits="Biore2012.Forms.RatingsAndReviews.ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/login.css")
        .Render("~/CSS/combinedlogin_#.css")
    %>
    <!--<link href="<%= System.Configuration.ConfigurationManager.AppSettings["cssprefix"] %>login.min.css" rel="stylesheet" type="text/css" media="screen,projection" />  --> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="contactFormWrap">
        <div id="BrandImageContainer">            
            <!--<img style="border-width: 0px;" src="<%= System.Configuration.ConfigurationManager.AppSettings["imageprefix"] %>forms/ratingsReviewModel.jpg" id="ctl00_ContentPlaceHolder1_ucFormConfig_Image1" />-->
        </div>
        <div id="ContactFormContainer">
            <!-- Header -->
            <div id="formHeader">
                <h1 id="PageHeader" runat="server">CHANGE PASSWORD</h1>
            </div>
            <!-- Description -->
            <asp:Panel ID="formPanel" runat="server">
                <div class="DescriptionContainer" id="ctl00_ContentPlaceHolder1_ucFormConfig_DescriptionContainer">
                    <h2 class="first">
                        To change your password, please submit the information below.</h2>
                    <!-- <p><br />[CHANGE PASSWORD COPY]</p>  -->
                </div>
                <p class="req">
                    <em>Required*</em></p>
                <div class="Question">
                    <asp:Label ID="Label12" runat="server" AssociatedControlID="textCurrent">Current Password*</asp:Label>
                    <asp:TextBox runat="server" TextMode="password" CssClass="text" ID="textCurrent"></asp:TextBox>
                </div>
                <div class="ErrorContainer">
                    <asp:RequiredFieldValidator ID="reqCurrPassword" runat="server" Display="Dynamic"
                        ErrorMessage="Please enter your Password." ControlToValidate="textCurrent" EnableClientScript="true"
                        SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                </div>
                <div class="ErrorContainer">
                    <asp:RegularExpressionValidator ID="regCurrLengthPassword" runat="server" Display="Dynamic"
                        ErrorMessage="Please enter a password with a minimum of six characters." ValidationExpression="^.{6,68}$"
                        ControlToValidate="textCurrent" EnableClientScript="true" SetFocusOnError="true"
                        CssClass="errormsg"></asp:RegularExpressionValidator>
                </div>
                <div class="ErrorContainer">
                    <asp:RegularExpressionValidator ID="regCurrPassword" runat="server" Display="Dynamic"
                        ErrorMessage="Re-enter a password using letters, numbers and/or these characters ~!@#$%^*-=+?,:."
                        ValidationExpression="(?=^.{1,68}$)(?!.*\s)[0-9a-zA-Z!@{}#|$%:,;\-~\?*()_+^&]*$"
                        ControlToValidate="textCurrent" EnableClientScript="true" SetFocusOnError="true"
                        CssClass="errormsg" Enabled="false"></asp:RegularExpressionValidator>
                </div>
                <div class="Question">
                    <asp:Label ID="Label1" runat="server" AssociatedControlID="textNew">New Password*</asp:Label>
                    <asp:TextBox runat="server" CssClass="text loginPassword" TextMode="password" ID="textNew"></asp:TextBox>
                </div>
                <div class="ErrorContainer">
                    <asp:RequiredFieldValidator ID="reqNewPass" runat="server" Display="Dynamic" ErrorMessage="Please enter your Password."
                        ControlToValidate="textNew" EnableClientScript="true" SetFocusOnError="true"
                        CssClass="errormsg"></asp:RequiredFieldValidator>
                </div>
                <div class="ErrorContainer">
                    <asp:RegularExpressionValidator ID="regLenNewPass" runat="server" Display="Dynamic"
                        ErrorMessage="Please enter a password with a minimum of six characters." ValidationExpression="^.{6,68}$"
                        ControlToValidate="textNew" EnableClientScript="true" SetFocusOnError="true"
                        CssClass="errormsg"></asp:RegularExpressionValidator>
                </div>
                <div class="ErrorContainer">
                    <asp:RegularExpressionValidator ID="regNewPass" runat="server" Display="Dynamic"
                        ErrorMessage="Re-enter a password using letters, numbers and/or these characters ~!@#$%^*-=+?,:."
                        ValidationExpression="(?=^.{1,68}$)(?!.*\s)[0-9a-zA-Z!@{}#|$%:,;\-~\?*()_+^&]*$"
                        ControlToValidate="textNew" EnableClientScript="true" SetFocusOnError="true"
                        CssClass="errormsg"></asp:RegularExpressionValidator>
                </div>
                <div class="Question">
                    <asp:Label ID="Label2" runat="server" AssociatedControlID="textNew2">Confirm New Password*</asp:Label>
                    <asp:TextBox runat="server" CssClass="text loginPassword" TextMode="password" ID="textNew2"></asp:TextBox>
                </div>
                <div class="ErrorContainer">
                    <asp:RequiredFieldValidator ID="reqPasswordConfirm" runat="server" Display="Dynamic"
                        ErrorMessage="Please confirm your password." ControlToValidate="textNew2" EnableClientScript="true"
                        SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                </div>
                <div class="ErrorContainer">
                    <asp:CompareValidator ID="cmpPasswordConfirm" runat="server" ErrorMessage="The password values do not match"
                        EnableClientScript="true" ControlToCompare="textNew" ControlToValidate="textNew2"
                        Display="Dynamic" CssClass="errormsg" SetFocusOnError="true"></asp:CompareValidator>
                </div>
                <div class="ErrorContainer">
                    <asp:TextBox runat="server" ID="userpass" Visible="false" Text="userpass" />
                    <asp:RequiredFieldValidator ID="reqPassCheck" runat="server" Display="Dynamic" ControlToValidate="userpass"
                        EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                </div>
                <div class="Buttons">
                    <asp:Button ID="Button" runat="server" OnClick="change_Click"
                        CssClass="buttonLink submit" Text="Change Password"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="confirmationPanel" runat="server" Visible="false">
                <div class="DescriptionContainer">
                    <h2 class="first">
                        Your password has been changed.</h2>
                    <p>
                        <br />
                        <asp:Literal ID="literalBackButton" runat="server" />
                    </p>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
