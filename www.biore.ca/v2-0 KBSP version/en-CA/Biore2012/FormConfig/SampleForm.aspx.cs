﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Biore2012.BLL;
using Biore2012.DAL;
using System.Configuration;
using System.Web.UI.HtmlControls;
using KaoBrands.FormStuff;
using KAOForms;

namespace Biore2012.FormConfig
{
    public partial class SampleForm : System.Web.UI.Page
    {
        public string PageEventID = System.Configuration.ConfigurationManager.AppSettings["EventId_sample"].ToString();
        public string PageEventCode = System.Configuration.ConfigurationManager.AppSettings["EventCode_sample"].ToString();
        public int PageMaxEntries = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["EventMaxEntries_sample"].ToString());
        public string PageHeaderImageText = "";
        public string PageHeaderImage = "";
        public string PageHeaderImageURL = System.Configuration.ConfigurationManager.AppSettings["imageprefix"] + "forms/" + System.Configuration.ConfigurationManager.AppSettings["EventHeaderImage_sample"].ToString();
        public int intSiteID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SiteID"]);


        protected void Page_Load(object sender, EventArgs e)
        {
            OptinFormResults.Visible = false;
            OptinError.Visible = false;

            Page.Header.DataBind();

            //ImageHeader.Src = PageHeaderImageURL;

            // Grab masterpage, set appropriate body classes
            Site myMaster = (Site)this.Master;
            myMaster.bodyClass += " forms signUp";

            if (Request.Url.ToString().Contains("email-newsletter-sign-up")) myMaster.bodyClass += " signUp";
            else if (Request.Url.ToString().Contains("contact-us")) myMaster.bodyClass += " contactUs";


            // This turns on and off the Question Pro Include for this page...
            ((Panel)Page.Master.FindControl("panel_Question_Pro")).Visible = false;

            CheckHookID();

            if (!IsPostBack)
            {

                if (!IsActiveEvent(PageEventID))
                {
                    //if event expired, show appropriate message.
                    ShowExpiredMessage();
                }
                //check to see if coupon count has been reached.
                else if (CGOptin.checkSampleCount(PageEventID) >= PageMaxEntries)
                {
                    //if no more coupons, hide checkbox and text.
                    ShowExpiredMessage();
                }
                else
                {
                    OptinEventExpired.Visible = false;
                    DateTime dt = DateTime.Now;
                    BioreUtils ut = new BioreUtils();

                    ut.queueNumber(dt.Year - 100, dt.Year - 13, "Year", yyyy); //Build Year
                    ut.queueNumber(1, 13, "Mon", mm); //Build Day
                    ut.queueNumber(1, 32, "Day", dd); //Build Month
               }
            }

        }

        public void Step1_ButtonClick(Object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                ProcessSubmission();
            }

        }

        protected string returnCheckBoxListAnswers(CheckBoxList cbl)
        {
            string strChkList = "";
            foreach (ListItem li in cbl.Items)
            {
                if (li.Selected)
                    strChkList += li.Text + ",";
            }

            return strChkList;
        }

        protected void DOBValidate(object source, ServerValidateEventArgs args)
        {
            try
            {

                int Day = int.Parse(dd.SelectedValue.ToString());
                int Month = int.Parse(mm.SelectedValue.ToString());
                int Year = int.Parse(yyyy.SelectedValue.ToString());

                DateTime Test = new DateTime(Year, Month, Day);

                //Are they 13 years of age or older?
                if (Test > DateTime.Now.AddYears(-13))
                {
                    args.IsValid = false;
                    //redirect to sorry page
                    //Response.Redirect("sorry.aspx");
                }
                else
                {
                    args.IsValid = true;
                }

            }
            catch (FormatException Arg)
            {
                string error = Arg.ToString();
                // item not selected (couldn't convert int)
                args.IsValid = false;
            }
            catch (ArgumentOutOfRangeException Arg)
            {
                string error = Arg.ToString();
                // invalid date (31 days in February)
                args.IsValid = false;
            }
        }

        protected Boolean IsActiveEvent(string strEventID)
        {
            bool blnReturn = true;

            //if event has either not started or has ended, show inactive message.
            if (CGOptin.EventHasStarted(strEventID) == false || CGOptin.EventHasEnded(strEventID) == true)
            {
                blnReturn = false;
            }

            //check to see if sample count has been reached.
            if (CGOptin.checkSampleCount(PageEventID) >= Convert.ToInt32(PageMaxEntries))
            {
                //no more samples
                blnReturn = false;
            }

            return blnReturn;
        }

        public void CheckHookID()
        {
            //Find hook id passed in query string, if available.  This is a means by which we can track promo source (e.g. Facebook)
            string strHookIDRequest = Request.QueryString["hook"] == null ? System.Configuration.ConfigurationManager.AppSettings["HookId"].ToString() : Request.QueryString["hook"].ToString();

            hfHookID.Value = strHookIDRequest;
        }


        protected void optinValidate(object source, ServerValidateEventArgs args)
        {
            //args.IsValid = (bioreoptin.Checked == true);
        }

        protected void rulesValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (bioreTerms.Checked == true);
        }
        
        protected void ProcessSubmission()
        {
            string strEntryReturn = "";
            string strTraitReturn = "";

            Page.Validate();

            if (Page.IsValid && FrstName.Text.Trim() == "") //FrstName is a honeypot trap, so make sure it's blank before we proceed.
            {
                OptinForm.Visible = false;
                string DOB = mm.SelectedValue + "/" + dd.SelectedValue + "/" + yyyy.SelectedValue;

                string strFName = FName.Text;
                string strLName = LName.Text;
                string strEmail = Email.Text;
                string strPhone = "";
                string strMobilePhone = "";
                string strStreet1 = Address1.Text;
                string strStreet2 = Address2.Text;
                string strCity = City.Text;
                string strProvince = State.SelectedItem.ToString();
                string strPostalCode = PostalCode.Text;
                string strPrimaryLanguage = PreferredLanguage.SelectedValue.ToString(); //EN or FR preferred language
                string strGender = Gender.SelectedValue.ToString();
                string strDOBdd = dd.SelectedValue;
                string strDOBmo = mm.SelectedValue;
                string strDOByr = yyyy.SelectedValue;
                string strDOB = DOB;

                string strURL = Request.RawUrl == null ? "" : Request.RawUrl.ToString();
                string strRemoteAddr = Request.ServerVariables["REMOTE_ADDR"] == null ? "" : Request.ServerVariables["REMOTE_ADDR"].ToString();
                string strUserAgent = Request.ServerVariables["HTTP_USER_AGENT"] == null ? "" : Request.ServerVariables["HTTP_USER_AGENT"].ToString(); ;

                string strContactContent = "";
                string strContactVia = "";
                string strContactFreq = "";

                //INSERT OPT-IN VALUES IF USER OPTED INTO AT LEAST ONE NEWSLETTER.
    
                strContactContent = "";
                strContactVia = "";
                strContactFreq = "";

                string strHookID = hfHookID.Value;

                //PROCESS SAMPLES ENTRY.
                strEntryReturn = CGOptin.ProcessOptIn(intSiteID, Convert.ToInt32(PageEventID), strFName, strLName, strEmail, strDOBdd, strDOBmo, strDOByr,
                                      strStreet1, strStreet2, strProvince, strCity, strPostalCode, strPrimaryLanguage, strPhone, strMobilePhone,
                                      strGender, strURL, strRemoteAddr, strUserAgent, PageEventCode,
                                      strContactContent, strContactVia, strContactFreq, strURL.Replace("http://", ""), "", strHookID);


                //if all processes were true OR the user has already entered, give the generic thanks message.
                if ((strEntryReturn.IndexOf("TRUE") != -1))
                {
                    OptinFormResults.Visible = true;
                    OptinForm.Visible = false;
                    //Response.Redirect("thank-you.aspx");
                }
                else
                {
                    //Oops!
                    litError.Text = "<span style=\"color:#C00\">An error has occurred.<br />" + strEntryReturn + "<br /><br />" + strTraitReturn + "</span>";
                    OptinError.Visible = true;
                }
            }

        }

        protected void ShowExpiredMessage()
        {
            OptinEventExpired.Visible = true;
            OptinForm.Visible = false;

        }
    }

         
}
