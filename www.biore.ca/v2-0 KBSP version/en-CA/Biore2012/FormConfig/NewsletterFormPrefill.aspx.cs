﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Biore2012.BLL;
using Biore2012.DAL;
using System.Configuration;
using System.Web.UI.HtmlControls;
using KaoBrands.FormStuff;
using KAOForms;

namespace Biore2012.FormConfig
{
    public partial class NewsletterFormPrefill : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            OptinFormResults.Visible = false;
            OptinError.Visible = false;

            Page.Header.DataBind();

            // Grab masterpage, set appropriate body classes
            Site myMaster = (Site)this.Master;
            myMaster.bodyClass += " forms signUp";

            if (Request.Url.ToString().Contains("email-newsletter-sign-up")) myMaster.bodyClass += " signUp";
            else if (Request.Url.ToString().Contains("contact-us")) myMaster.bodyClass += " contactUs";

            //check to see if member id is passed.  If so, proceed.  If not, return error message.
            string strMemberID = Request.QueryString["m"] == null ? "" : Request.QueryString["m"].ToString();

            // This turns on and off the Question Pro Include for this page...
            ((Panel)Page.Master.FindControl("panel_Question_Pro")).Visible = false;

            CheckHookID();

            DateTime dt = DateTime.Now;
            BioreUtils ut = new BioreUtils();

            ut.queueNumber(dt.Year - 100, dt.Year - 13, "Year", yyyy); //Build Year
            ut.queueNumber(1, 13, "Mon", mm); //Build Day
            ut.queueNumber(1, 32, "Day", dd); //Build Month

            if (!IsPostBack)
            {
                //Prefill form fields.
                if (strMemberID != "")
                {
                    hfMemberID.Value = strMemberID;
                    PreFillMemberData(strMemberID);
                }
            }

        }

        public void Step1_ButtonClick(Object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                ProcessSubmission();
            }

        }

        protected string returnCheckBoxListAnswers(CheckBoxList cbl)
        {
            string strChkList = "";
            foreach (ListItem li in cbl.Items)
            {
                if (li.Selected)
                    strChkList += li.Text + ",";
            }

            return strChkList;
        }

        protected void DOBValidate(object source, ServerValidateEventArgs args)
        {
            try
            {

                int Day = int.Parse(dd.SelectedValue.ToString());
                int Month = int.Parse(mm.SelectedValue.ToString());
                int Year = int.Parse(yyyy.SelectedValue.ToString());

                DateTime Test = new DateTime(Year, Month, Day);

                //Are they 13 years of age or older?
                if (Test > DateTime.Now.AddYears(-13))
                {
                    args.IsValid = false;
                    //redirect to sorry page
                    //Response.Redirect("sorry.aspx");
                }
                else
                {
                    args.IsValid = true;
                }

            }
            catch (FormatException Arg)
            {
                string error = Arg.ToString();
                // item not selected (couldn't convert int)
                args.IsValid = false;
            }
            catch (ArgumentOutOfRangeException Arg)
            {
                string error = Arg.ToString();
                // invalid date (31 days in February)
                args.IsValid = false;
            }
        }

        protected Boolean IsActiveEvent(string strEventID)
        {
            bool blnReturn = true;

            //if event has either not started or has ended, show inactive message.
            if (CGOptin.EventHasStarted(strEventID) == false || CGOptin.EventHasEnded(strEventID) == true)
            {
                blnReturn = false;
            }

            //check to see if sample count has been reached.
            if (CGOptin.checkSampleCount(System.Configuration.ConfigurationManager.AppSettings["EventId_sample"].ToString()) >= Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["sampletotal"].ToString()))
            {
                //no more samples
                blnReturn = false;
            }

            return blnReturn;
        }

        public void CheckHookID()
        {
            //Find hook id passed in query string, if available.  This is a means by which we can track promo source (e.g. Facebook)
            string strHookIDRequest = Request.QueryString["hook"] == null ? System.Configuration.ConfigurationManager.AppSettings["HookId"].ToString() : Request.QueryString["hook"].ToString();

            hfHookID.Value = strHookIDRequest;
        }


        protected void optinValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (bioreoptin.Checked == true);
        }
        
        protected void ProcessSubmission()
        {
            string strEntryReturn = "";
            string strTraitReturn = "";
            string strOptinReturn = "";

            Page.Validate();

            if (Page.IsValid && FrstName.Text.Trim() == "") //FrstName is a honeypot trap, so make sure it's blank before we proceed.
            {
                OptinForm.Visible = false;
                string DOB = mm.SelectedValue + "/" + dd.SelectedValue + "/" + yyyy.SelectedValue;

                string strFName = FName.Text;
                string strLName = LName.Text;
                string strEmail = Email.Text;
                string strPhone = "";
                string strMobilePhone = "";
                string strStreet1 = Address1.Text;
                string strStreet2 = Address2.Text;
                string strCity = City.Text;
                string strProvince = State.SelectedItem.ToString();
                string strPostalCode = PostalCode.Text;
                string strPrimaryLanguage = PreferredLanguage.SelectedValue.ToString(); //EN or FR preferred language
                string strGender = Gender.SelectedValue.ToString();
                string strDOBdd = dd.SelectedValue;
                string strDOBmo = mm.SelectedValue;
                string strDOByr = yyyy.SelectedValue;
                string strDOB = DOB;

                //segmentation questions
                string strWhatSkinConcerns = WhatSkinConcerns.SelectedItem.ToString();
                string strWhatFaceCareBrand = returnCheckBoxListAnswers(WhichFaceCleanserBrands);
                string strWhichDoYouUse = returnCheckBoxListAnswers(WhichDoYouUse);

                //optin info
                bool blnMultiOptin = MultiOptin.Checked ? true : false;
                //bool blnBanOptin = banoptin.Checked ? true : false;
                bool blnBioreOptin = bioreoptin.Checked ? true : false;
                bool blnCurelOptin = cureloptin.Checked ? true : false;
                bool blnJergensOptin = jergensoptin.Checked ? true : false;
                //bool blnJohnFriedaOptin = johnfriedaoptin.Checked ? true : false;

                if (blnBioreOptin)
                    blnMultiOptin = true;

                int intSiteID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SiteID"]);

                int intBioreEventID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["EventId_signup_Biore"].ToString());
                string strBioreEventCode = System.Configuration.ConfigurationManager.AppSettings["EventCode_signup_Biore"].ToString();
                int intBioreSiteID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SiteID_Biore"].ToString());

                int intCurelEventID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["EventId_signup_Curel"].ToString());
                string strCurelEventCode = System.Configuration.ConfigurationManager.AppSettings["EventCode_signup_Curel"].ToString();
                int intCurelSiteID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SiteID_Curel"].ToString());

                int intJergensEventID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["EventId_signup_Jergens"].ToString());
                string strJergensEventCode = System.Configuration.ConfigurationManager.AppSettings["EventCode_signup_Jergens"].ToString();
                int intJergensSiteID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SiteID_Jergens"].ToString());

                /*
                int intJFEventID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["EventId_signup_JF"].ToString());
                string strJFEventCode = System.Configuration.ConfigurationManager.AppSettings["EventCode_signup_JF"].ToString();
                int intJFSiteID = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SiteID_JF"].ToString());
                */

                string strURL = Request.RawUrl == null ? "" : Request.RawUrl.ToString();
                string strRemoteAddr = Request.ServerVariables["REMOTE_ADDR"] == null ? "" : Request.ServerVariables["REMOTE_ADDR"].ToString();
                string strUserAgent = Request.ServerVariables["HTTP_USER_AGENT"] == null ? "" : Request.ServerVariables["HTTP_USER_AGENT"].ToString(); ;

                string strContactContent = "";
                string strContactVia = "";
                string strContactFreq = "";

                //INSERT OPT-IN VALUES IF USER OPTED INTO AT LEAST ONE NEWSLETTER.
                if (blnMultiOptin)
                {
                    strContactContent = "samples,contests,sweepstakes,other info,";
                    strContactVia = "email,direct mail,";
                    strContactFreq = "as needed,";
                }

                string strHookID = hfHookID.Value;

                //PROCESS SAMPLES ENTRY.
                strEntryReturn = CGOptin.ProcessOptIn(intBioreSiteID, intBioreEventID, strFName, strLName, strEmail, strDOBdd, strDOBmo, strDOByr,
                                      strStreet1, strStreet2, strProvince, strCity, strPostalCode, strPrimaryLanguage, strPhone, strMobilePhone,
                                      strGender, strURL, strRemoteAddr, strUserAgent, strBioreEventCode,
                                      strContactContent, strContactVia, strContactFreq, strURL.Replace("http://", ""), "", strHookID);


                //PROCESS SEGMENTATION TRAIT QUESTIONS and OPT-INS.
                if (strEntryReturn.IndexOf("TRUE") != -1)
                {
                    //do this for each individual trait question
                    strTraitReturn = CGOptin.ProcessTraits(Convert.ToInt32(strEntryReturn.Replace("TRUE", "")), intBioreEventID, strRemoteAddr, strUserAgent, strURL, Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["traitID_WhatSkinConcerns"]), strWhatSkinConcerns);
                    strTraitReturn = CGOptin.ProcessTraits(Convert.ToInt32(strEntryReturn.Replace("TRUE", "")), intBioreEventID, strRemoteAddr, strUserAgent, strURL, Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["traitID_WhichFaceCleanserBrands"]), strWhatFaceCareBrand);
                    strTraitReturn = CGOptin.ProcessTraits(Convert.ToInt32(strEntryReturn.Replace("TRUE", "")), intBioreEventID, strRemoteAddr, strUserAgent, strURL, Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["traitID_WhichDoYouUse"]), strWhichDoYouUse);


                    //apply all opt-ins.
                    //PROCESS ALL OPT-INS.

                    if (blnMultiOptin)
                    {
                        
                        if (blnCurelOptin)
                        {
                            strOptinReturn = CGOptin.ProcessOptIn(intCurelSiteID, intCurelEventID, strFName, strLName, strEmail, strDOBdd, strDOBmo, strDOByr,
                                          strStreet1, strStreet2, strProvince, strCity, strPostalCode, strPrimaryLanguage, strPhone, strMobilePhone,
                                          strGender, strURL, strRemoteAddr, strUserAgent, strCurelEventCode,
                                          strContactContent, strContactVia, strContactFreq, strURL.Replace("http://", ""), "", strHookID);
                        }
                        if (blnJergensOptin)
                        {
                            strOptinReturn = CGOptin.ProcessOptIn(intJergensSiteID, intJergensEventID, strFName, strLName, strEmail, strDOBdd, strDOBmo, strDOByr,
                                          strStreet1, strStreet2, strProvince, strCity, strPostalCode, strPrimaryLanguage, strPhone, strMobilePhone,
                                          strGender, strURL, strRemoteAddr, strUserAgent, strJergensEventCode,
                                          strContactContent, strContactVia, strContactFreq, strURL.Replace("http://", ""), "", strHookID);
                        }
                        
                    }

                }

                //if all processes were true OR the user has already entered, give the generic thanks message.
                if ((strEntryReturn.IndexOf("TRUE") != -1 && strTraitReturn == "TRUE"))
                {
                    OptinFormResults.Visible = true;
                    OptinForm.Visible = false;
                    //Response.Redirect("thank-you.aspx");
                }
                else
                {
                    //Oops!
                    litError.Text = "<span style=\"color:#C00\">An error has occurred.<br />" + strEntryReturn + "<br /><br />" + strTraitReturn + "</span>";
                    OptinError.Visible = true;
                }
            }

        }


        protected string GetBirthDayPart(string strMonthDayYear, string strBirthDay)
        {
            string strReturn = "";

            switch (strMonthDayYear)
            {
                case "year":

                    try
                    {
                        DateTime dt = Convert.ToDateTime(strBirthDay);

                        strReturn = dt.Year.ToString();
                    }
                    catch { }

                    return strReturn;

                case "month":

                    try
                    {
                        DateTime dt = Convert.ToDateTime(strBirthDay);

                        strReturn = dt.Month.ToString();
                    }
                    catch { }

                    return strReturn;

                case "day":

                    try
                    {
                        DateTime dt = Convert.ToDateTime(strBirthDay);

                        strReturn = dt.Day.ToString();
                    }
                    catch { }

                    return strReturn;

                default:

                    return strReturn;
            }


        }


        public SqlDataReader returnMemberData(string strMemberID)
        {
            SqlDataReader sqlDR = null;

            try
            {
                string connStr = System.Configuration.ConfigurationManager.ConnectionStrings["ConsumerInfo"].ConnectionString;
                SqlConnection sqlConn = new SqlConnection(connStr);

                /*SqlDataAdapter da = new SqlDataAdapter
                ("select * from mytable;", sqlConn);
                DataSet ds = new DataSet();
                da.Fill(ds);
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    Console.WriteLine(dr[0].ToString());
                }*/

                //SqlCommand sqlCmd = new SqlCommand("select b.TraitID, b.TraitValue, a.email, a.fname, a.lname, a.addr1, a.addr2, a.addr3, a.city, a.regionid, a.postalcode, a.gender, a.dob, a.homephone from tblMembers a inner join tblMemberTraits b on b.MemberID = a.MemberID where a.MemberID ='" + strMemberID + "' and b.SiteID = '2006'", sqlConn);

                SqlCommand sqlCmd = new SqlCommand(@"select b.TraitID, b.TraitValue, a.email, a.fname, a.lname, a.addr1, a.addr2, a.addr3, 
                                                    a.city, a.regionid, a.postalcode, a.gender, a.dob, a.primarylanguageid, a.homephone 
                                                    from tblMembers a 
                                                    left outer join tblMemberTraits b on b.MemberID = a.MemberID
                                                    inner join tblMemberInterests c on c.MemberID = a.MemberID
                                                    where a.MemberID ='" + strMemberID + "' and c.SiteID = " + System.Configuration.ConfigurationManager.AppSettings["SiteID"] + " and c.ContactVia like '%email%'", sqlConn);

                sqlConn.Open();
                sqlDR = sqlCmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {

                Response.Write("error: " + strMemberID + " " + e);
            }

            return sqlDR;

        }


        protected void PreFillMemberData(string MemberID)
        {
            SqlDataReader subscriberData = returnMemberData(MemberID);
            //int Counter = 0;

            //foreach (string str in subscriberData)
            //{
            // Response.Write(str + " " + Counter);
            //Counter++;
            //}

            while (subscriberData.Read())
            {
                //Prefill PII data
                FName.Text = subscriberData["fname"].ToString();
                LName.Text = subscriberData["lname"].ToString();
                Email.Text = subscriberData["email"].ToString();
                Address1.Text = subscriberData["addr1"].ToString();
                Address2.Text = subscriberData["addr2"].ToString();
                City.Text = subscriberData["city"].ToString();
                State.SelectedValue = subscriberData["regionid"].ToString();

                if (subscriberData["primarylanguageid"].ToString().Trim().ToLower() == "en")
                    PreferredLanguage.SelectedValue = "EN";
                else if (subscriberData["primarylanguageid"].ToString().Trim().ToLower() == "fr")
                    PreferredLanguage.SelectedValue = "FR";

                PostalCode.Text = subscriberData["postalcode"].ToString();

                if (subscriberData["gender"].ToString() == "F")
                    Gender.SelectedValue = "F";
                else if (subscriberData["gender"].ToString() == "M")
                    Gender.SelectedValue = "M";

                yyyy.SelectedValue = GetBirthDayPart("year", subscriberData["dob"].ToString());
                mm.SelectedValue = GetBirthDayPart("month", subscriberData["dob"].ToString());
                dd.SelectedValue = GetBirthDayPart("day", subscriberData["dob"].ToString());

                bioreoptin.Checked = true; //pre-check the opt-in box per Julie Gomes 11/30/12

                //Prefill Trait Info
                /*
                 * 
                 * TraitID	TraitDesc	TraitPrompt	TraitValueList	dtCreated   
                    733	biore_ca_en_mkt_q14	What skin concerns are you seeking to solve with your cleanser?	~select~Daily cleansing (Normal Skin)~Dry skin~Oily skin~Combination skin – Normal to Oily~Combination skin – Normal to Dry~Combination skin – Oily / Dry~Acne prone skin~	5/10/2010 1:08:30 PM
                   
                    735	biore_ca_en_mkt_q16	Which of the following face cleanser brands have you used in the past 12 months?	~select~1~2~3~4~5 or more~	5/10/2010 1:08:30 PM
                    --736	biore_ca_en_mkt_q17	If you use Bioré® products , which of the following do you currently use?	~checkbox~1~2~3~4~5 or more~	5/10/2010 1:08:30 PM
                  

                        <add key="traitID_WhatSkinConcerns" value="733"/>
                        <add key="traitID_WhichFaceCleanserBrands" value="735"/>
                        <add key="traitID_WhichDoYouUse" value="736"/>
                 */

                switch (subscriberData["TraitID"].ToString())
                {
                    

                    case "733":
                        WhatSkinConcerns.SelectedValue = subscriberData["TraitValue"].ToString();
                        break;

                    case "735":
                        //this one potentially has multiple checkbox options
                        string[] strValuesArray735 = subscriberData["TraitValue"].ToString().Split(',');

                        foreach (string strValue in strValuesArray735)
                        {
                            foreach (ListItem li in WhichFaceCleanserBrands.Items)
                            {
                                if (li.Text == strValue)
                                    li.Selected = true;
                            }
                        }
                       
                        break;

                    case "736":
                        //this one potentially has multiple checkbox options
                        string[] strValuesArray413 = subscriberData["TraitValue"].ToString().Split(',');

                        foreach (string strValue in strValuesArray413)
                        {
                            foreach (ListItem li in WhichDoYouUse.Items)
                            {
                                if (li.Text == strValue)
                                    li.Selected = true;
                            }
                        }

                        break;


                }

            }

            if (Email.Text == "")
            {
                OptinForm.Visible = false;
                litNoMemberId.Text = "<p><br />No pre-existing subscription information could be retrieved.  If you are not a current subscriber, please visit us at: <a href=\"http://www.biore.ca/en-CA/formconfig/newsletterform.aspx\">http://www.biore.ca/en-CA/formconfig/newsletterform.aspx</a>.</p>";
            }
        }

    }
         
}
