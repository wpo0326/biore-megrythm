﻿$(document).ready(function() {
    productDetail.init();
});

var productDetail = {
    updatePlusOne: false,
    init: function() {
        productDetail.initEvents();
        productDetail.checkHashAndUpdate();

        if ($(".ie7").length == 0) {
            $(window).hashchange(function() {
                productDetail.updatePlusOne = false;
                productDetail.checkHashAndUpdate();
            });
        }
    },
    initEvents: function() {

        // add hide class for product detail
        $(".collapsibleContent").addClass("hide");

        $(".open, .prodLookNav").removeClass("hide");


        // click event for accordian on Product Detail
        $(".contentHolder h3").click(function() {
            global.showHideContent(this, $(".contentHolder"), $(this).parent(), false);
        });

        // click event for old / new product show / hide on Product Detail
        $(".prodLookNav a").click(function(e) {
            e.preventDefault();
            var elementClicked = $(this).attr("href");
            $(".prodImgHolder, .prodLookNav a").removeClass("on");
            $(this).addClass("on");
            $(elementClicked).addClass("on");
        });

        // subnav for pore strip
        $(".poreNav").click(function (e) {
            //disabled post pricespider.  
            //e.preventDefault();
            ////var href = $(this).attr("href").replace("#", "");
            //var href = $(this).attr("href").split("#").pop(); 
            //productDetail.updatePage(window["product" + href]);
            //$(".poreStripNav").removeClass("selected");
            //$(this).parent().parent().addClass("selected");
            //window.location.hash = href.toLowerCase();
        });

        $(".carouselNav").click(function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "next") {
                $("#prodCarousel ul").addClass("moreLeft");
                $("#prodCarouselWrapper #prev").removeClass("disabled");
            }
            else {
                $("#prodCarousel ul").removeClass("moreLeft");
                $("#prodCarouselWrapper #next").removeClass("disabled");
            }
            $(this).addClass("disabled");
        });

        $("#prodCarousel").touchwipe({
            wipeLeft: function() {
                $("#prodCarousel ul").addClass("moreLeft");
                $("#prodCarouselWrapper #prev").removeClass("disabled");
                $("#prodCarouselWrapper #next").addClass("disabled");
            },
            wipeRight: function() {
                $("#prodCarousel ul").removeClass("moreLeft");
                $("#prodCarouselWrapper #next").removeClass("disabled");
                $("#prodCarouselWrapper #prev").addClass("disabled");
            },
            preventDefaultEvents: false
        });
    },
    checkHashAndUpdate: function() {
        var location = window.location.href;
        var hashIndex = location.indexOf("#");
        var poreStripIndex = location.indexOf("pore-strips");
        if (hashIndex >= 0) {
            var hash = location.substring(hashIndex + 1, location.length);
            $("#" + hash + "Nav a").click();
        }
        else if (poreStripIndex >= 0) {
            window.location.hash = "regular";
        }
    },
    updatePage: function(theProduct) {
        var localPath = productDetail.getLocalPath();
        var location = "";
        if(window.location.port != '18570'){  // Hack to allow dev in VS
            location = "/" + localPath;
        }
        
        var locationHref = window.location.href;
        var hashIndex = locationHref.indexOf("#");
        if (hashIndex >= 0) {
            $("#ctl00_ContentPlaceHolder1_theNewImage").attr("src", theProduct["NewLookImage"].replace("~", location));
            $("#ctl00_ContentPlaceHolder1_theOldImage").attr("src", theProduct["OldLookImage"].replace("~", location));
        }
        
        $("#productName h1 .spanProductName").html(theProduct["Name"]);
        $("#productName p .new").html(theProduct["Description"]);
        $("#productName p .new").html(theProduct["Description"]);
        $("#whatItIsContent").html(theProduct["WhatItIs"]);
        $("#ingredientsContent").html(theProduct["ProductDetails"]);
        $("#funFactsContent").html(theProduct["FunFacts"]);
        //$("#howItWorksContent").html(theProduct["HowItWorks"] + "<h4>Cautions</h4>" + theProduct["Cautions"]);
        $("#howItWorksContent").html(theProduct["HowItWorks"]);
        $("#ctl00_ContentPlaceHolder1_theBuyNowLink").attr("href", theProduct["BuyNowURL"]);

        if (window.location.port == '80') {
            $("#ctl00_ContentPlaceHolder1_buyNowIFrame").attr("src", '/en-CA/BuyNow.aspx?RefName=' + encodeURIComponent(window.location.href.substring(window.location.href.lastIndexOf('/') + 1)));
        } else {
            $("#ctl00_ContentPlaceHolder1_buyNowIFrame").attr("src", '/BuyNow.aspx?RefName=' + encodeURIComponent(window.location.href.substring(window.location.href.lastIndexOf('/') + 1)));
        }
        
        productDetail.updateLikeBtn(theProduct);
        if ($(".ie7").length == 0 && !productDetail.updatePlusOne) {
            productDetail.updatePlusOneBtn(theProduct);
        }
    },
    updateLikeBtn: function(theProduct) {
        var nUrl = theProduct["LikeURL"].replace("~", "");
        var hostname = window.location.hostname;
        var localPath = productDetail.getLocalPath();
        $('#likeBtnDiv').html('<div class="fb-like" data-href="http://' + hostname + "/" + localPath + nUrl + '/" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false"></div>');
        if (typeof FB != "undefined") {
            FB.XFBML.parse(document.getElementById('likeBtnDiv'));
        }
    },
    updatePlusOneBtn: function(theProduct) {
        productDetail.updatePlusOne = true;
        var nUrl = theProduct["LikeURL"].replace("~", "");
        var hostname = window.location.hostname;
        var localPath = productDetail.getLocalPath();
        $('#plusBtnDiv div:first').html('<g:plusone></g:plusone>');
        if (typeof gapi != "undefined") {
            setTimeout(function() {
                gapi.plusone.render($('#plusBtnDiv div').get(0), { "annotation": "none", "href": "http://" + hostname + '/' + localPath + nUrl + "/", "size": "medium" });
            }, 0);
        }
    },
    getLocalPath: function() {
        var windowLocation = window.location.href;
        var hostname = window.location.hostname;
        var substringLocation = windowLocation.substring(windowLocation.indexOf(hostname) + hostname.length + 1, windowLocation.length);
        substringLocation = substringLocation.substring(0, substringLocation.indexOf("/"));
        return substringLocation;
    },
    socialScriptLoaded: function(scriptID) {
        if (scriptID == "gplus1js") {
            var location = window.location.href;
            var hashIndex = location.indexOf("#");
            if (hashIndex >= 0) {
                var hash = location.substring(hashIndex + 1, location.length);
                var uppercaseHash = hash.substring(0, 1).toUpperCase();
                uppercaseHash = uppercaseHash + hash.substring(1, hash.length);
                if ($(".ie7").length == 0 && !productDetail.updatePlusOne) {
                    productDetail.updatePlusOneBtn(window["product" + uppercaseHash]);
                }
            }
        }
    },
    showReviewsTab: function (tabID) {
        var thisDiv = $(".contentHolder h3").get(tabID);
        global.showHideContent(thisDiv, $(".contentHolder"), $(thisDiv).parent(), false);
    }
}
