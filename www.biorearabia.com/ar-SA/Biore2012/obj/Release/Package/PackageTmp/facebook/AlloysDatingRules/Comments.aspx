﻿<%@ Page Title="" Language="C#" MasterPageFile="~/facebook/FBContentPageMaster.Master" AutoEventWireup="true" CodeBehind="Comments.aspx.cs" Inherits="Biore2012.facebook.AlloysDatingRules.Comments" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<meta property="fb:admins" content="1064275431,25700203,1584985879"/>	
	<meta property="fb:app_id" content="<%= System.Configuration.ConfigurationManager.AppSettings["datingrulesFBAppId"] %>"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div id="fb-root"></div>
	<script type="text/javascript">
		var extJsHost_connect_facebook_net = (("https:" == document.location.protocol) ? "https://" : "http://");
		window.fbAsyncInit = function() {
			FB.init({
				appId: '<%= System.Configuration.ConfigurationManager.AppSettings["datingrulesFBAppId"] %>', // App ID
				//channelURL : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
				status: true, // check login status
				cookie: true, // enable cookies to allow the server to access the session
				oauth: true, // enable OAuth 2.0
				xfbml: true  // parse XFBML
			});

			// Additional initialization code here
			FB.Event.subscribe('comment.create', function() {
				window.opener.adr.fbCommentCreated();
				window.close();
			});
		};

		// Load the SDK Asynchronously
		(function(d) {
			var js, id = 'facebook-jssdk'; if (d.getElementById(id)) { return; }
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = extJsHost_connect_facebook_net + "connect.facebook.net/en_US/all.js";
			d.getElementsByTagName('head')[0].appendChild(js);
		} (document));
	</script>
	<div id="fbCommentsPopup" class="datingRules">
		<p>Add your comment below by clicking "Add a Comment"</p>
		<a href="#" onclick="window.close();">Close</a>
		<div class="fb-comments" data-href="<%= System.Configuration.ConfigurationManager.AppSettings["datingrulesCommentUrl"] %>" data-num-posts="5" data-width="500"></div>
	</div>
</asp:Content>
