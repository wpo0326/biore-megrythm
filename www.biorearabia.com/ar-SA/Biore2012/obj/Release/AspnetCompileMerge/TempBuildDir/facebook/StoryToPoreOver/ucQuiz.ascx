﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucQuiz.ascx.cs" Inherits="Biore2012.facebook.StoryToPoreOver.ucQuiz" %>
<asp:Panel ID="pan100Quiz" runat="server" CssClass="pan100Quiz">
	<div class="quizQuestionsHdr">
		<h4 class="spriteQuiz ir stpoQuizSubHdr">WHAT'S YOUR JANE GREEN STYLE?</h4>
		<h5>Loved Jane's short story? Answer three questions to uncover what your next read should be.</h5>
		<asp:PlaceHolder ID="phProveItTop" runat="server">
			<p>You'll even get Prove It!<sup>&reg;</sup> Reward Points<sup>&dagger;</sup> you can redeem for coupons, samples, free products and great giveaways!</p>
			<h5>Not a Prove It<sup>&reg;</sup> Reward Points member? Sign up today and start earning! <a href="http://www.facebook.com/bioreskin/app_205787372796203" target="_blank">SIGN UP &raquo;</a></h5>
		</asp:PlaceHolder>
	</div>
	<div class="quizAnswerHdr hide">
		<h4 class="spriteQuiz ir stpoQuizResultsInHdr">The results are in! Here are your Jane picks.</h4>
		<p class="loveForLove"><strong>Love for Love:</strong> You're a romantic soul through and through. These Jane picks are sure to put you in the right mood for all things lovely and romantic.</p>
		<p class="familyFirst"><strong>Family First:</strong> Blood runs thicker than water, and for you&mdash;family comes first. These novels by Jane will remind you how important those closest to you really are.</p>
		<p class="friendsForever"><strong>Friends Forever:</strong> A good friend knows all your best stories&mdash;but a best friend has lived them with you. These books by Jane will remind you how true this is.</p>
	</div>
	<div class="quizHolder">
		<div class="questions">
			<div class="question q1 qzPanel current">
				<h5>1. Where do you love to read the most?</h5>
				<ul>
					<li><input type="radio" name="q1" id="q1a" /><label for="q1a">A. On the Beach</label></li>
					<li><input type="radio" name="q1" id="q1b" /><label for="q1b">B. At Home</label></li>
					<li><input type="radio" name="q1" id="q1c" /><label for="q1c">C. On an Airplane</label></li>
				</ul>
			</div>
			<div class="question q2 qzPanel">
				<h5>2. What's your idea of fun night out?</h5>
				<ul>
					<li><input type="radio" name="q2" id="q2a" /><label for="q2a">A. Dinner Date</label></li>
					<li><input type="radio" name="q2" id="q2b" /><label for="q2b">B. Movie Night</label></li>
					<li><input type="radio" name="q2" id="q2c" /><label for="q2c">C. Girl's Night</label></li>
				</ul>
			</div>
			<div class="question q3 qzPanel">
				<h5>3. The most important thing in my life right now is:</h5>
				<ul>
					<li><input type="radio" name="q3" id="q3a" /><label for="q3a">A. Finding Mr. Right</label></li>
					<li><input type="radio" name="q3" id="q3b" /><label for="q3b">B. Family Time</label></li>
					<li><input type="radio" name="q3" id="q3c" /><label for="q3c">C. Old Friends</label></li>
				</ul>
			</div>
			<div class="result qzPanel">
				<div class="bookReco bookRecoLove">
					<a href="http://www.amazon.com/Straight-Talking-Novel-Jane-Green/dp/0767915593/ref=la_B001IGLSDS_1_13?ie=UTF8&qid=1345673178&sr=1-13" target="_blank" class="spriteQuiz stpoBookRecoStraightTalking"><span class="lvReco reco1">Straight Talking</span></a>
					<a href="http://www.amazon.com/Mr-Maybe-Novel-Jane-Green/dp/0767905202/ref=la_B001IGLSDS_1_2?ie=UTF8&qid=1345673068&sr=1-2" target="_blank" class="spriteQuiz stpoBookRecoMrMaybe"><span class="lvReco reco2">Mr. Maybe</span></a>
					<a href="http://www.amazon.com/Jemima-Novel-About-Ducklings-Swans/dp/0767905180/ref=la_B001IGLSDS_1_8?ie=UTF8&qid=1345673068&sr=1-8" target="_blank" class="spriteQuiz stpoBookRecoJemimaJ last"><span class="lvReco reco3">Jemima J</span></a>
				</div>
				<div class="bookReco bookRecoFam">
					<a href="http://www.amazon.com/Dune-Road-Novel-Jane-Green/dp/B004MPRWWI/ref=la_B001IGLSDS_1_12?ie=UTF8&qid=1345673068&sr=1-12" target="_blank" class="spriteQuiz stpoBookRecoDuneRoad"><span class="famReco reco1">Dune Road</span></a>
					<a href="http://www.amazon.com/The-Other-Woman-Jane-Green/dp/0452287146/ref=la_B001IGLSDS_1_11?ie=UTF8&qid=1345673068&sr=1-11" target="_blank" class="spriteQuiz stpoBookRecoTheOtherWoman"><span class="famReco reco2">The Other Woman</span></a>
					<a href="http://www.amazon.com/Beach-House-Jane-Green/dp/0670018856/ref=pd_bbs_sr_3?ie=UTF8&s=books&qid=1211377706&sr=8-3" target="_blank" class="spriteQuiz stpoBookRecoTheBeachHouse last"><span class="famReco reco3">The Beach House</span></a>
				</div>
				<div class="bookReco bookRecoFriend">
					<a href="http://www.amazon.com/Second-Chance-Jane-Green/dp/0452289440/ref=la_B001IGLSDS_1_5?ie=UTF8&qid=1345673068&sr=1-5" target="_blank" class="spriteQuiz stpoBookRecoSecondChance"><span class="frdReco reco1">Second Chance</span></a>
					<a href="http://www.amazon.com/Bookends-A-Novel-Jane-Green/dp/0767907817/ref=la_B001IGLSDS_1_7?ie=UTF8&qid=1345673068&sr=1-7" target="_blank" class="spriteQuiz stpoBookRecoBookends last"><span class="frdReco reco2">Bookends</span></a>
				</div>
			</div>
		</div>
		<div class="quizFooter">
			<div class="quizNav">
				<div class="navBtns">
					<a href="#prev" class="spriteQuiz ir stpoQuizPrevBtn">PREV</a>
					<p class="questionCounter">QUESTION <span class="currentQ">1</span> OF <span class="totalQ">3</span></p>
					<a href="#next" class="spriteQuiz ir stpoQuizNextBtn">NEXT</a>
				</div>
			</div>
			<a href="#quizSubmit" class="spriteQuiz ir stpoQuizSubmitBtn hide">Submit</a>
			<asp:Panel ID="panProveIt" runat="server">
				<div class="proveIt hide">
					<h5 class="spriteQuiz ir stpoQuizProveItLogo">Prove It! Reward Points</h5>
					<p>Thank you for completing our quiz. Now, get your Prove It!<sup>&reg;</sup> Reward Points<sup>&dagger;</sup> to redeem for coupons, samples, free products and great giveaways!</p>
					<a href="https://apps.facebook.com/proveitrewards/?promotionid=1" class="spriteQuiz ir stpoQuizProveItBtn" target="_blank">Get Points</a>
				</div>
				<p class="proveItDisclaimer"><sup>&dagger;</sup> Participate in the Jane Green quiz between 1 p.m. EST and 5 p.m. EST on November 1, 2012 and, upon completion, you will have the opportunity to access the "Get Points" link. This link will take you to the Prove It!<sup>&reg;</sup> Reward Points Facebook page. Upon signing in, you will see a pop-up indicating that 100 points have been added to your Prove It!<sup>&reg;</sup> Reward account.</p>
			</asp:Panel>
		</div>
	</div>
</asp:Panel>