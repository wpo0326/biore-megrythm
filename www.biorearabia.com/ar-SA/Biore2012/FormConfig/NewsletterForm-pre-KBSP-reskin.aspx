﻿<%@ Page Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="NewsletterForm-pre-KBSP-reskin.aspx.cs"
    Inherits="Biore2012.FormConfig.NewsletterForm" %>
<%@ Register TagPrefix="skm" Namespace="skmValidators" Assembly="skmValidators" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%#SquishIt.Framework.Bundle .Css()
        .Add("~/css/formConfig.css")
        .Render("~/css/combinedform_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow">
            </div>
            <div class="centeringDiv">
                <div class="responsive contact">
                    <div id="contactFormWrap">
                        <div id="BrandImageContainer" class="png-fix">
                            <img class="png-fix" src="images/forms/signUpPhoto.jpg" style="border-width: 0px;" />
                        </div>
                        <div id="ContactFormContainer">
                            <asp:Panel ID="ContactDetails" runat="server">
                                <!-- Header -->
                                <div id="formHeader">
                                    <h1 id="ctl00_ContentPlaceHolder1_ucForm_PageHeader">
                                        Join the Movement for Great Skin 24/7!
                                    </h1>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="OptinForm" runat="server">
                                <!-- Description -->
                                <div class="DescriptionContainer png-fix">
                                    <h2>
                                        Thanks for your interest in Bioré® Skincare!</h2>
                                    Please enter the information below to hear about all our latest promotions, contests,
                                    and news.
                                </div>
                                <!-- From Fields -->
                                <div class="FormContainer">
                                    <p class="req png-fix">
                                        <em>Required*</em></p>
                                    <div class="MemberInfoContainer png-fix">
                                        <div class="NameWrapper">
                                            <div class="FirstNameContainer Question">
                                                <div class="mmSweet">
                                                    <asp:Label ID="FNameLbl" runat="server" Text="First Name*" AssociatedControlID="FrstName"></asp:Label>
                                                    <asp:TextBox ID="FrstName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="FNameLbl1" runat="server" Text="First Name*" AssociatedControlID="FName"></asp:Label>
                                                <asp:TextBox ID="FName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="FNameValidator1" runat="server" ErrorMessage="Please enter your First Name."
                                                        ControlToValidate="FName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="FNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your First Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="FName" EnableClientScript="true"
                                                        SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="LastNameContainer Question">
                                                <asp:Label ID="LNameLbl" runat="server" Text="Last Name*" AssociatedControlID="LName"></asp:Label>
                                                <asp:TextBox ID="LName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="LNameValidator1" runat="server" ErrorMessage="Please enter your Last Name."
                                                        ControlToValidate="LName" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="LNameValidator2" runat="server" ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Last Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="LName" SetFocusOnError="true"
                                                        Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="EmailWrapper">
                                            <div class="EmailContainer Question">
                                                <asp:Label ID="EmailLbl" runat="server" Text="Email*" AssociatedControlID="Email"></asp:Label>
                                                <asp:TextBox ID="Email" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="EmailValidator1" runat="server" ErrorMessage="Please enter your Email Address."
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="EmailValidator2" runat="server" ErrorMessage="Please enter a valid Email Address."
                                                        ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div class="PhoneContainer Question">
                                            <asp:Label ID="MobilePhoneLbl" runat="server" Text="Mobile" AssociatedControlID="MobilePhone"></asp:Label>
                                            <asp:TextBox ID="MobilePhone" MaxLength="50" runat="server" />
                                            <div class="ErrorContainer">
                                                <asp:RegularExpressionValidator ID="v_MobilePhone" runat="server" ErrorMessage="Your Mobile Phone is not required, but please do not use special characters in the Mobile Phone field."
                                                    ValidationExpression="^[^<>]+$" ControlToValidate="MobilePhone" EnableClientScript="true"
                                                    SetFocusOnError="true" Display="Dynamic" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                <%-- The following RegEx Validators is for US type numbers.  IMPORTANT: 
								 Please test against your user base before using as you may need to modify it for other formats!  --%>
                                                <%--<asp:RegularExpressionValidator ID="PhoneValidator2" runat="server"
								ErrorMessage="<br/>Your Home Phone is not required, but please enter only valid characters."
								ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"
								ControlToValidate="Phone" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg phone"></asp:RegularExpressionValidator>--%>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelAddress" class="AddressContainer">
                                            <div class="AddressWrapper">
                                                <div class="Address1Container Question">
                                                    <asp:Label ID="Address1Lbl" runat="server" Text="Address 1*" AssociatedControlID="Address1"></asp:Label>
                                                    <asp:TextBox ID="Address1" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="Address1Validator1" runat="server" ErrorMessage="Please enter your Address."
                                                            ControlToValidate="Address1" EnableClientScript="true" SetFocusOnError="true"
                                                            Display="Dynamic" CssClass="errormsg" />
                                                        <asp:RegularExpressionValidator ID="Address1Validator2" runat="server" ErrorMessage="Please do not use special characters in the Address 1 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address1" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="Address2Container Question">
                                                    <asp:Label ID="Address2Lbl" runat="server" Text="Address 2" AssociatedControlID="Address2"></asp:Label>
                                                    <asp:TextBox ID="Address2" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="Address2Validator1" runat="server" ErrorMessage="Please do not use special characters in the Address 2 field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="Address2" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                                <div class="CityContainer Question">
                                                    <asp:Label ID="CityLbl" runat="server" Text="City*" AssociatedControlID="City"></asp:Label>
                                                    <asp:TextBox ID="City" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="CityValidator1" runat="server" ErrorMessage="Please enter your City."
                                                            ControlToValidate="City" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                            CssClass="errormsg" />
                                                        <asp:RegularExpressionValidator ID="CityValidator2" runat="server" ErrorMessage="Please do not use special characters in the City field."
                                                            ValidationExpression="^[^<>]+$" ControlToValidate="City" EnableClientScript="true"
                                                            SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="StateContainer Question">
                                                <asp:Label ID="StateLbl" runat="server" Text="Province*" AssociatedControlID="State"></asp:Label>
                                                <asp:DropDownList ID="State" runat="server">
                                                    <asp:ListItem Value="" Selected="true">Select Province</asp:ListItem>
                                                    <asp:ListItem Value="1">Alberta</asp:ListItem>
                                                    <asp:ListItem Value="2">British Columbia</asp:ListItem>
                                                    <asp:ListItem Value="3">Manitoba</asp:ListItem>
                                                    <asp:ListItem Value="4">New Brunswick</asp:ListItem>
                                                    <asp:ListItem Value="5">Newfoundland</asp:ListItem>
                                                    <asp:ListItem Value="6">Northwest Territories</asp:ListItem>
                                                    <asp:ListItem Value="7">Nova Scotia</asp:ListItem>
                                                    <asp:ListItem Value="8">Nunavut</asp:ListItem>
                                                    <asp:ListItem Value="9">Ontario</asp:ListItem>
                                                    <asp:ListItem Value="10">Prince Edward Island</asp:ListItem>
                                                    <asp:ListItem Value="11">Quebec</asp:ListItem>
                                                    <asp:ListItem Value="12">Saskatchewan</asp:ListItem>
                                                    <asp:ListItem Value="13">Yukon</asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="StateValidator" runat="server" ErrorMessage="Please select your Province."
                                                        ControlToValidate="State" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                        CssClass="errormsg" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div class="PostalCodeContainer Question">
                                            <asp:Label ID="PostalCodeLbl" runat="server" Text="Postal Code*" AssociatedControlID="PostalCode"></asp:Label>
                                            <asp:TextBox ID="PostalCode" runat="server" MaxLength="20" CssClass="inputTextBox"></asp:TextBox>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="PostalCodeValidator1" runat="server" ErrorMessage="Please enter your Postal Code"
                                                    ControlToValidate="PostalCode" EnableClientScript="true" SetFocusOnError="true"
                                                    Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                
                                            </div>
                                        </div>
                                        <div class="CountryContainer Question">
                                            <span id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_CountryLbl">Country
                                            </span><span class="fake_input">Canada</span>
                                        </div>
                                        <div class="seperator png-fix">
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelLanguage" class="GenderContainer Question">
                                            <asp:Label ID="LanguageLbl" runat="server" Text="Preferred Language*" AssociatedControlID="PreferredLanguage"></asp:Label>
                                            <asp:DropDownList ID="PreferredLanguage" runat="server">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="EN">English</asp:ListItem>
                                                <asp:ListItem Value="FR">French</asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="LanguageValidator" runat="server" ErrorMessage="Please select your Preferred Language."
                                                    ControlToValidate="PreferredLanguage" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div id="ctl00_ContentPlaceHolder1_ucForm_ucFormMemberInfo_PanelGender" class="GenderContainer Question">
                                            <asp:Label ID="GenderLbl" runat="server" Text="Gender*" AssociatedControlID="Gender"></asp:Label>
                                            <asp:DropDownList ID="Gender" runat="server">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="F">Female</asp:ListItem>
                                                <asp:ListItem Value="M">Male</asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="GenderValidator" runat="server" ErrorMessage="Please enter your Gender."
                                                    ControlToValidate="Gender" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="BirthdayContainer Question">
                                            <asp:Label ID="DOBLbl" runat="server" Text="Birthdate*" AssociatedControlID="yyyy"></asp:Label>
                                            <asp:DropDownList ID="yyyy" runat="server" CssClass="inputSelectBDay year">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="mm" runat="server" CssClass="inputSelectBDay month">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="dd" runat="server" CssClass="inputSelectBDay day">
                                            </asp:DropDownList>
                                            <br />
                                            <div style="padding-left:100px;font-size:10px;font-style:italic">You must be 13 years of age or older to sign up</div>
                                            <div class="ErrorContainer">
                                                <asp:CustomValidator ID="DOBValidator" runat="server" ErrorMessage="Please enter a valid date."
                                                    OnServerValidate="DOBValidate" EnableClientScript="false" Display="Dynamic" CssClass="errormsg validdate"></asp:CustomValidator>
                                                <asp:RequiredFieldValidator ID="yyyyValidator" runat="server" ErrorMessage="Please enter your Birth Year."
                                                    ControlToValidate="yyyy" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg year"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="mmValidator" runat="server" ErrorMessage="Please enter your Birth Month."
                                                    ControlToValidate="mm" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg month"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="ddValidator" runat="server" ErrorMessage="Please enter your Birth Day."
                                                    ControlToValidate="dd" EnableClientScript="true" SetFocusOnError="true" Display="Dynamic"
                                                    CssClass="errormsg day"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="seperator png-fix">
                                        </div>
                                    </div>
                                              
                                    <div id="ctl00_ContentPlaceHolder1_ucForm_QuestionPanel" class="QuestionContainer">
                        			            
                                        <div class="MarketingQuestionContainer">
                                            <div class="CustomQuestiondropdown Question">
					                            <label for="HowDidYouHear" id="HowDidYouHearLBL" class="dropdownLabel">How did you hear about Bioré® skincare?*</label>
					                            <asp:DropDownList ID="HowDidYouHear" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem Value="">Select...</asp:ListItem>
						                            <asp:ListItem>Tried products before</asp:ListItem>
						                            <asp:ListItem>Seen in a magazine</asp:ListItem>
						                            <asp:ListItem>Through a online promotion</asp:ListItem>

						                            <asp:ListItem>Seen in-store</asp:ListItem>
						                            <asp:ListItem>Internet</asp:ListItem>
                                                    <asp:ListItem>Email</asp:ListItem>
                                                    <asp:ListItem>Friend</asp:ListItem>
                                                    <asp:ListItem>Other</asp:ListItem>
					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
						                            <asp:RequiredFieldValidator ID="v_HowDidYouHear" runat="server" ControlToValidate="HowDidYouHear" ErrorMessage="Please answer all required questions." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
					                            </div>
				                            </div>
                                            <div class="CustomQuestiondropdown Question">
					                            <label for="CleanserQualityRankOne" id="Label2" class="dropdownLabel">What are the important qualities you expect face cleansers, to deliver?*<br />(rank from 1 to 6, with 1 being most important and 6 being least important)*</label>
                                                <div class="RankHolders">
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Leaves your face feeling clean</div>
                                                        <asp:RadioButtonList ID="CleanserQualityRankOne" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Leaves your face healthy-looking</div>
                                                        <asp:RadioButtonList ID="CleanserQualityRankTwo" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Provides superior deep cleansing</div>
                                                        <asp:RadioButtonList ID="CleanserQualityRankThree" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Controls oil & shine</div>
                                                        <asp:RadioButtonList ID="CleanserQualityRankFour" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Prevents and/or treats acne/blemishes</div>
                                                        <asp:RadioButtonList ID="CleanserQualityRankFive" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Makes my pores appear smaller</div>
                                                        <asp:RadioButtonList ID="CleanserQualityRankSix" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                </div>

					                             <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="CleanserQualityRankOne" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="CleanserQualityRankOne" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="CleanserQualityRankThree" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="CleanserQualityRankFour" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="CleanserQualityRankFive" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="CleanserQualityRankSix" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
				                            </div>

                                            <div class="CustomQuestiondropdown Question">
					                            <label for="ChooseCleanerRankOne" id="Label1" class="dropdownLabel">How do you choose what face care cleanser to use?*<br />(rank from 1 to 6, with 1 being most important and 6 being least important)</label>
                                                <div class="RankHolders2">
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Recommended by friends/family</div>
                                                        <asp:RadioButtonList ID="ChooseCleanerRankOne" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Recommended by Doctor/Dermatologist</div>
                                                        <asp:RadioButtonList ID="ChooseCleanerRankTwo" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Is reasonably priced</div>
                                                        <asp:RadioButtonList ID="ChooseCleanerRankThree" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Is a brand I always use</div>
                                                        <asp:RadioButtonList ID="ChooseCleanerRankFour" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">Is for my skin type</div>
                                                        <asp:RadioButtonList ID="ChooseCleanerRankFive" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="RankContainer">
                                                        <div class="RankAnswer">I like the effects/features it promises</div>
                                                        <asp:RadioButtonList ID="ChooseCleanerRankSix" runat="server" RepeatColumns="6" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="rankorder">
                                                            <asp:ListItem>1</asp:ListItem>
                                                            <asp:ListItem>2</asp:ListItem>
                                                            <asp:ListItem>3</asp:ListItem>
                                                            <asp:ListItem>4</asp:ListItem>
                                                            <asp:ListItem>5</asp:ListItem>
                                                            <asp:ListItem>6</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div style="clear:both"></div>
                                                </div>

					                              <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ChooseCleanerRankOne" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ChooseCleanerRankTwo" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ChooseCleanerRankThree" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ChooseCleanerRankFour" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="ChooseCleanerRankFive" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
                                                  <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ChooseCleanerRankSix" ErrorMessage="Please rank all selections." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
				                            </div>

				                            <div class="CustomQuestiondropdown Question">
					                            <label for="WhatSkinConcerns" id="WhatSkinConcernsLBL" class="dropdownLabel">What skin concerns are you seeking to solve with your cleanser? (select one)*</label>
					                              <asp:DropDownList ID="WhatSkinConcerns" runat="server" CssClass="dropDownAnswer">
					                                <asp:ListItem Value="">Select...</asp:ListItem>
						                            <asp:ListItem>Daily cleansing (Normal Skin)</asp:ListItem>
						                            <asp:ListItem>Dry skin</asp:ListItem>
						                            <asp:ListItem>Oily skin</asp:ListItem>
						                            <asp:ListItem>Combination skin – Normal to Oily</asp:ListItem>
						                            <asp:ListItem>Combination skin – Normal to Dry</asp:ListItem>
                                                    <asp:ListItem>Combination skin – Oily / Dry</asp:ListItem>
                                                    <asp:ListItem>Acne prone skin</asp:ListItem>
					                              </asp:DropDownList>
					                              <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="v_q1" runat="server" ControlToValidate="WhatSkinConcerns" ErrorMessage="Please answer all required questions." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                              </div>
				                            </div>
				                            <div class="CustomQuestiondropdown Question">
					                            <label for="WhatFacialCleanser" id="WhatFacialCleanserLBL" class="dropdownLabel">What form of facial cleanser do you use most often? (choose one)*</label>
					                            
					                                <asp:DropDownList ID="WhatFacialCleanser" runat="server" CssClass="dropDownAnswer">
					                                   <asp:ListItem Value="">Select...</asp:ListItem>
						                                <asp:ListItem>Liquid/Gel or Cream Cleanser</asp:ListItem>
						                                <asp:ListItem>Facial Scrub</asp:ListItem>
						                                <asp:ListItem>Toner/Astringent</asp:ListItem>
						                                <asp:ListItem>Cleansing Cloth/Towelette</asp:ListItem>
						                                <asp:ListItem>Acne/Blemish Pads</asp:ListItem>
                                                        <asp:ListItem>Make-Up Remover (liquid or cloth)</asp:ListItem>
					                                </asp:DropDownList>
					                                <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="v_WhatFacialCleanser" runat="server" ControlToValidate="WhatFacialCleanser" ErrorMessage="Please answer all required questions." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
						                           
					                                </div>

				                            </div>
                                            <div class="CustomQuestiondropdown Question">
					                            <label for="WhichFaceCleanserBrands" id="WhichFaceCleanserBrandsLBL" class="dropdownLabel">Which of the following face cleanser brands have you used in the past 12 months?*</label>
					                            <asp:DropDownList ID="WhichFaceCleanserBrands" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem Value="">Select...</asp:ListItem>
						                            <asp:ListItem Value="Aveeno">Aveeno&#174; products</asp:ListItem>
                                                    <asp:ListItem Value="Biore">Bior&#233;&#174; products</asp:ListItem>
                                                    <asp:ListItem Value="Cetaphil">Cetaphil&#174; products</asp:ListItem>
						                            <asp:ListItem Value="Clean and Clear">Clean &amp; Clear&#174; products</asp:ListItem>
						                            <asp:ListItem Value="Clearasil">Clearasil&#174; products</asp:ListItem>
						                            <asp:ListItem Value="Garnier">Garnier products</asp:ListItem>
						                            <asp:ListItem Value="L'Oreal">L' Oreal&#174; products</asp:ListItem>
						                            <asp:ListItem Value="Nivea">Nivea&#174; products</asp:ListItem>
						                            <asp:ListItem Value="Neutrogena">Neutrogena&#174; products</asp:ListItem>
                                                    <asp:ListItem Value="Olay">Olay&#174; products</asp:ListItem>
						                            <asp:ListItem Value="Simple">Simple&#174; products</asp:ListItem>
						                            <asp:ListItem Value="St. Ives">St. Ives&#174; products</asp:ListItem>
						                            <asp:ListItem Value="Other">Other</asp:ListItem>
						                            <asp:ListItem Value="None of these">None of these</asp:ListItem>
					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
						                             <asp:RequiredFieldValidator ID="v_WhichFaceCleanserBrands" runat="server" ControlToValidate="WhichFaceCleanserBrands" ErrorMessage="Please answer all required questions." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
					                            </div>
				                            </div>

                                            <div class="CustomQuestioncheckbox Question trait413 cbVert noneToggle1">
				                                <label for="WhichDoYouUse" id="WhichDoYouUseLBL" class="checkboxLabel">If you use Bioré® skincare products , which of the following do you currently use? (select all that apply)*</label>
				                                <asp:CheckBoxList ID="WhichDoYouUse" RepeatDirection="Vertical" runat="server" RepeatLayout="Flow" CssClass="checkboxAnswer">

							                        <asp:ListItem Value="Bioré® Combination Skin Balancing Cleanser">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Combination Skin Balancing Cleanser</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Steam Activated Cleanser">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Steam Activated Cleanser</asp:ListItem>
							                        <asp:ListItem Value="Bioré® 4-in-1 Revitalizing Cleanser">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; 4-in-1 Revitalizing Cleanser</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Pore Unclogging Scrub">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Pore Unclogging Scrub</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Daily Purifying Scrub">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Daily Purifying Scrub</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Deep Cleansing Pore Strips">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Deep Cleansing Pore Strips</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Deep Cleansing Pore Strips Combo">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Deep Cleansing Pore Strips Combo</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Deep Cleansing Pore Strips Ultra">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Deep Cleansing Pore Strips Ultra</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Blemish Fighting Ice Cleanser">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Blemish Fighting Ice Cleanser</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Blemish Fighting Astringent">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Blemish Fighting Astringent</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Warming Anti-Blackhead Cream Cleanser">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Warming Anti-Blackhead Cream Cleanser</asp:ListItem>
							                        <asp:ListItem Value="Bioré® Make-Up Removing Towelettes">Bior&eacute;&lt;sup&gt;&reg;&lt;/sup&gt; Make-Up Removing Towelettes</asp:ListItem>
							                        <asp:ListItem Value="None of the above">None of the above</asp:ListItem>
							                    </asp:CheckBoxList>
							                    
				         <span class="cbinput"></span>
				         
					                            <div class="ErrorContainer">
						                            <skm:CheckBoxListValidator ID="HairAppliancesLblVal" ControlToValidate="WhichDoYouUse"
                                                    runat="server" Display="Dynamic" CssClass="errormsg" ErrorMessage="Please answer all required questions." />
					                            </div>
				                            </div>

                                            <div class="CustomQuestiondropdown Question">
					                            <label for="RecommendToFriends" id="RecommendToFriendsLBL" class="dropdownLabel">How likely are you to recommend your favorite skin care products to friends?*</label>
					                            <asp:DropDownList ID="RecommendToFriends" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem Value="">Select...</asp:ListItem>
						                            <asp:ListItem Value="Very likely">Very likely</asp:ListItem>
						                            <asp:ListItem Value="Somewhat likely">Somewhat likely</asp:ListItem>
						                            <asp:ListItem Value="Somewhat unlikely">Somewhat unlikely</asp:ListItem>
						                            <asp:ListItem Value="Not at all likely">Not at all likely</asp:ListItem>

					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
						                            <asp:RequiredFieldValidator ID="v_RecommendToFriends" runat="server" ControlToValidate="RecommendToFriends" ErrorMessage="Please answer all required questions." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
					                            </div>
				                            </div>
				                            
				                            <div class="CustomQuestioncheckbox Question trait419 cbVert">
					                            <label for="WhatBioreInterests" id="WhatBioreInterestsLBL" class="checkboxLabel">What are you interested in receiving from Bioré&reg; skincare? (Check all that apply.)*</label>
					                           <asp:CheckBoxList ID="WhatBioreInterests" RepeatDirection="Vertical" runat="server" RepeatLayout="Flow" CssClass="checkboxAnswer">
							                        <asp:ListItem>Special offers, promotions and contests</asp:ListItem>
							                        <asp:ListItem>New product info</asp:ListItem>
							                        <asp:ListItem>Skincare tips and info</asp:ListItem>
							                        
							                    </asp:CheckBoxList>
							                    
				         <span class="cbinput"></span>
					                            <div class="ErrorContainer">
						                            <skm:CheckBoxListValidator ID="v_WhatBioreInterests" ControlToValidate="WhatBioreInterests"
                                                    runat="server" Display="Dynamic" CssClass="errormsg" ErrorMessage="Please answer all required questions." />
					                            </div>
					                           
				                            </div>
				                            <div class="CustomQuestiondropdown Question">
					                            <label for="ContactHowOften" id="ContactHowOftenLBL" class="dropdownLabel">How often do you like to be contacted?*</label>
					                            <asp:DropDownList ID="ContactHowOften" runat="server" CssClass="dropDownAnswer">
						                            <asp:ListItem Value="">Select...</asp:ListItem>
						                            <asp:ListItem>Very rarely</asp:ListItem>
							                        <asp:ListItem>A few times a year</asp:ListItem>
							                        <asp:ListItem>Regularly</asp:ListItem>
							                        <asp:ListItem>All the time – the more skincare info the better!</asp:ListItem>

					                            </asp:DropDownList>
					                            <div class="ErrorContainer">
                                                     <asp:RequiredFieldValidator ID="v_ContactHowOften" runat="server" ControlToValidate="ContactHowOften" ErrorMessage="Please answer all required questions." Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                </div>

				                            </div>
			                            </div>
		                            </div>
           
                                    <div class="OptinContainer">
                                        <div class="seperator png-fix"></div>
	                                    <p class="privacy">
		                                    Your privacy is important to us. You can trust that Kao Canada Inc. will use personal information in accordance with our <a href="http://www.kaobrands.com/privacy_policy.asp"
			                                    target="_blank">Privacy Policy</a>.
	                                    </p>

	                                    <div class="CurrentSiteOptinContainer">
	                                        <ul>
		                                        <li>
		                                            <asp:CheckBox ID="bioreoptin" runat="server" />
			                                        <asp:Label ID="bioreoptinLabel" runat="server" Text="Yes, tell me about future Bior&eacute;<sup>&reg;</sup> product news and offerings." AssociatedControlID="bioreoptin" CssClass="siteOptinChkbox" />

			                                        <div class="ErrorContainer">
                                    			        <skm:CheckBoxValidator ID="cbv_bioreOptin" runat="server" ControlToValidate="bioreoptin" ErrorMessage="If you want to become a Bioré® Brand Member, please check Yes!" CssClass="errormsg" Display="Dynamic"></skm:CheckBoxValidator>
				                                    </div>

		                                        </li>
	                                        </ul>
	                                    </div>
                                        
                                        <!--<div id="ctl00_ContentPlaceHolder1_ucForm_ucFormOptin_PanelMultiOptin" class="MultiBrandOptinContainer">
                                    			            
		                                    <ul>            
			                                    <li id="optinToAll">
			                                        <asp:CheckBox ID="MultiOptin" runat="server" CssClass="multiOptin" />
			                                        <asp:Label ID="MultiOptinLabel" runat="server" Text="Yes, I'd like to receive emails and newsletters from other great products from Kao Brands Company:" AssociatedControlID="MultiOptin" />
			        
			                                    </li>			

			                                    <li id="ctl00_ContentPlaceHolder1_ucForm_ucFormOptin_curelListItem">
			                                        <asp:CheckBox ID="cureloptin" runat="server" />
			                                <asp:Label ID="cureloptinLabel" runat="server" Text="Cur&eacute;l<sup>&reg;</sup></a> Skincare's full line of hand, foot and body moisturizers delivers freedom from dry skin." AssociatedControlID="cureloptin"
				                                CssClass="brandLabels" />
			                         
			                                    </li>
			                                    <li id="ctl00_ContentPlaceHolder1_ucForm_ucFormOptin_jergensListItem">
			                                        <asp:CheckBox ID="jergensoptin" runat="server" />
			                                        <asp:Label ID="jergensoptinLabel" runat="server" Text="Jergens<sup>&reg;</sup></a> Skincare collection of moisturizers delivers a natural glow, smooth and firm skin and an allure that captivates." AssociatedControlID="jergensoptin"
				                                CssClass="brandLabels" />

			                                    </li>

			                                    <li>
				                                    <p class="privDisclaimer">Before submitting your information, please view our <a
					                                    href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">Privacy Policy</a>.</p>
			                                    </li>
		                                    </ul>
                                    	
		                                    </div>	-->	

                                    </div> 

                                    <div id="submit-container" class="SubmitContainer png-fix">
                                        <asp:Button ID="Button1" runat="server" CssClass="submit buttonLink png-fix" Text="SUBMIT ›" OnClick="Step1_ButtonClick"  />
                                    </div>
                                    <asp:HiddenField ID="hfHookID" runat="server" />
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="OptinFormResults" runat="server">
                                 <div class="ConfrimationContainer"><h2>Thanks!</h2><p>Thanks for your interest in Bior&eacute;<sup>&reg;</sup> Skincare. We look forward to sending you all the latest news on exciting events, contests, and product information throughout the year.</p></div>  

                            </asp:Panel>
                            <asp:Panel ID="OptinError" runat="server">
                                An error has occurred while attempting to submit your information.   <asp:Literal ID="litError" runat="server"></asp:Literal>
                            </asp:Panel>
                            <div class="FormBottom png-fix">
                            </div>
                            <!-- Disclaimer -->
                            <div id="DisclaimerContainer" class="png-fix">
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">

                    </script>

                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">

    <script type="text/javascript">
        $(function() {

            if ($(".signUp").length) {
                toggle("noneToggle1");
                toggle("noneToggle2");
                toggle("noneToggle3");
                toggle("noneToggle4");
            }

            $("#ctl00_ContentPlaceHolder1_cureloptin, #ctl00_ContentPlaceHolder1_jergensoptin").click(function() {
                if ($('#ctl00_ContentPlaceHolder1_cureloptin:checked').val()
                    || $('#ctl00_ContentPlaceHolder1_jergensoptin:checked').val()) 
                {
                        $('#ctl00_ContentPlaceHolder1_MultiOptin').attr("checked", "true");
                }
                else { $('#ctl00_ContentPlaceHolder1_MultiOptin').removeAttr("checked"); }
            });
            $("#ctl00_ContentPlaceHolder1_MultiOptin").click(function() {
                if ($('#ctl00_ContentPlaceHolder1_MultiOptin:checked').val()) {
                    $("#ctl00_ContentPlaceHolder1_cureloptin").attr("checked", "true");
                    $("#ctl00_ContentPlaceHolder1_jergensoptin").attr("checked", "true");
                } else {
                    $("#ctl00_ContentPlaceHolder1_cureloptin").removeAttr("checked");
                    $("#ctl00_ContentPlaceHolder1_jergensoptin").removeAttr("checked");
                }
            });

            /*// $(this).prop('checked', false); */
            //Rank Order handling.

            $(".RankHolders input").click(function () {
                rankOrderHandler(".RankHolders input", this);
            });

            $(".RankHolders2 input").click(function () {
                rankOrderHandler(".RankHolders2 input", this);
            });

        });

        function rankOrderHandler(rankClass, inputId) {
            var RankerString = $(inputId).attr('id');
            var RankerCheckedNum = RankerString.substr(RankerString.length - 1);
            var CompareString;
            var CompareNum;

            $(rankClass).each(function () {
                CompareString = $(this).attr('id');
                CompareNum = CompareString.substr(CompareString.length - 1);
                if (CompareString != RankerString && CompareNum == RankerCheckedNum) {
                    if ($("#" + CompareString).attr("checked")) {
                        $("#" + CompareString).attr("checked", false);
                    }
                }
             })
        }
        
        function toggle(className) {
            var checkToggle = $("." + className + " input:last");
            checkToggle.click(function() {
                if ($("#" + checkToggle.attr('id') + ":checked").val()) {
                    $("." + className + " input").each(function() {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).attr("checked", false);
                            $(this).attr('disabled', 'disabled');
                        }
                    });
                } else {
                    $("." + className + "  input").each(function() {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).removeAttr('disabled');
                        }
                    });
                }
            });
        }
    </script>

</asp:Content>
