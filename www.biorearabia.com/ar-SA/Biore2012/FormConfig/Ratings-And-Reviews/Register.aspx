﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Register.aspx.cs" Inherits="Biore2012.Forms.RatingsAndReviews.Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">    
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/login.css")
        .Render("~/CSS/combinedlogin_#.css")
    %>
    <!--<link href="<%= System.Configuration.ConfigurationManager.AppSettings["cssprefix"] %>login.min.css" rel="stylesheet" type="text/css" media="screen,projection" />  -->   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="contactFormWrap">
        <div id="BrandImageContainer">
            <!--<img style="border-width: 0px;" src="/images/forms/ratingsReviewModel.jpg" id="ctl00_ContentPlaceHolder1_ucFormConfig_Image1" />-->
        </div>
        <div id="ContactFormContainer">
            <!-- Header -->
            <div id="formHeader">
                <h1 id="PageHeader" runat="server">REGISTER</h1>
            </div>
            <!-- Description -->
            <div class="DescriptionContainer" id="ctl00_ContentPlaceHolder1_ucFormConfig_DescriptionContainer">
                <h2 class="first">Thank you for your interest in reviewing Biore® products!</h2>
                <p><br />
                    Please provide your email address and a password. When you write reviews in the
                    future, you will login with this information.</p>
            </div>
            <p class="req">
                <em>Required*</em></p>
            <div class="Question">
                <asp:Label ID="Label3" runat="server" AssociatedControlID="txtEmail">Email* </asp:Label>
                <asp:TextBox runat="server" MaxLength="70" CssClass="text" ID="txtEmail"></asp:TextBox>
            </div>
            <div class="ErrorContainer">
                <asp:RequiredFieldValidator ID="reqEmail" runat="server" Display="Dynamic" ErrorMessage="Please enter your Email Address."
                    ControlToValidate="txtEmail" EnableClientScript="true" SetFocusOnError="true"
                    CssClass="errormsg"></asp:RequiredFieldValidator>
            </div>
            <div class="ErrorContainer">
                <asp:RegularExpressionValidator ID="regEmail" runat="server" Display="Dynamic" ErrorMessage="Please enter a valid Email Address."
                    ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                    ControlToValidate="txtEmail" EnableClientScript="true" SetFocusOnError="true"
                    CssClass="errormsg"></asp:RegularExpressionValidator>
            </div>
            <div class="ErrorContainer">
                <asp:TextBox runat="server" ID="username"  Visible="false" Text="username" />
                <asp:RequiredFieldValidator ID="reqEmailCheck" runat="server" Display="Dynamic"
                    ErrorMessage="Username already exists.  Please login and/or use password recovery." ControlToValidate="username" EnableClientScript="true"
                    SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
            </div>
            <div class="Question">
                <asp:Label ID="Label4" runat="server" AssociatedControlID="txtPW1">Password* </asp:Label>
                <asp:TextBox runat="server" MaxLength="70" CssClass="text" TextMode="Password" ID="txtPW1"></asp:TextBox>
            </div>
            <div class="ErrorContainer">
                <asp:RequiredFieldValidator ID="reqPassword" runat="server" Display="Dynamic"
                    ErrorMessage="Please enter your Password." ControlToValidate="txtPW1" EnableClientScript="true"
                    SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
            </div>
            <div class="ErrorContainer">
               <asp:RegularExpressionValidator ID="regLengthPassword" runat="server" Display="Dynamic" ErrorMessage="Please enter a password with a minimum of six characters."
                    ValidationExpression="^.{6,68}$"
                    ControlToValidate="txtPW1" EnableClientScript="true" SetFocusOnError="true"
                    CssClass="errormsg"></asp:RegularExpressionValidator>
            </div>
            <div class="ErrorContainer">
                <asp:RegularExpressionValidator ID="regPassword" runat="server" Display="Dynamic" ErrorMessage="Re-enter a password using letters, numbers and/or these characters ~!@#$%^*-=+?,:."
                    ValidationExpression="(?=^.{0,68}$)(?!.*\s)[0-9a-zA-Z!@{}#|$%:,;\-~\?*()_+^&]*$"
                    ControlToValidate="txtPW1" EnableClientScript="true" SetFocusOnError="true"
                    CssClass="errormsg"></asp:RegularExpressionValidator>
            </div>
            <div class="Question">
                <asp:Label ID="Label5" runat="server" AssociatedControlID="txtPW2">Confirm Password* </asp:Label>
                <asp:TextBox runat="server" MaxLength="70" CssClass="text" TextMode="Password" ID="txtPW2"></asp:TextBox>
            </div>
            <div class="ErrorContainer">
                <asp:RequiredFieldValidator ID="reqPasswordConfirm" runat="server" Display="Dynamic"
                    ErrorMessage="Please confirm your password." ControlToValidate="txtPW2" EnableClientScript="true"
                    SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
            </div>
            <div class="ErrorContainer">
                <asp:CompareValidator ID="cmpPasswordConfirm" runat="server" ErrorMessage="The password values do not match"
                    EnableClientScript="true" ControlToCompare="txtPW1" ControlToValidate="txtPW2"
                    Display="Dynamic" CssClass="errormsg" SetFocusOnError="true"></asp:CompareValidator>
            </div>
            <div class="Buttons">
                <asp:Button ID="Button" CssClass="submit buttonLink" runat="server" OnClick="register_Click"
                    Text="Register"></asp:Button>
            </div>
        </div>
    </div>
</asp:Content>
