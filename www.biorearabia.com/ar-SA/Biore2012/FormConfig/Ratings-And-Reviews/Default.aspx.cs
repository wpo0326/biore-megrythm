﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Biore2012.BLL;

namespace Biore2012.Forms.Ratings_And_Reviews
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                AuthUtils au = new AuthUtils();
                if (au.CheckIfAuthenticated() == true)
                {
                    UserToken theToken = BazaarvoiceUtils.CreateUserToken(au.GetCurrentUserID(), "I97ByXMPtn8sgAAFYw6k6igzey96J8O2OEq86VzuBANxAkTQSsUw281cNskxhJumY"); //Hash found in implementation guide.
                    literalUserToken.Text = "\"" + theToken.ToString() + "\"";
                }
                else { literalUserToken.Text = "\"\""; }

                String theBVAuthenticateUserFlag = "";

                if (Request.QueryString["bvauthenticateuser"] != null)
                {
                    theBVAuthenticateUserFlag = Request.QueryString["bvauthenticateuser"].ToLower();
                }
                if (theBVAuthenticateUserFlag == "true")
                {
                    if (au.CheckIfAuthenticated() == false)
                    {
                        String theRedirectPath = "Login.aspx?return=Default.aspx?" + Server.UrlEncode(Request.QueryString.ToString());
                        Response.Redirect(theRedirectPath);
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write("We're sorry.  An error has occurred.  Please try again later. " + "<!--" + ex + "-->");
            }
        }
    }
}
