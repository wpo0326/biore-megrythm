﻿<%@ Page Title="Bior&eacute;&reg; Skincare | Rules" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Rules.aspx.cs" Inherits="Biore2012.FormConfig.Rules" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Bioré® rules" name="description" />
    <meta content="" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedrules_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <div id="polaroid">
                    <img src="<%= VirtualPathUtility.ToAbsolute("~/images/forms/signUpPhoto.jpg") %>" alt="" /></div>
                <div id="content">
                    <h1>THE BIORE® SKINCARE NEWSLETTER CONFIRMATION CONTEST</h1>
                    <h2>THE BIORE® SKINCARE NEWSLETTER CONFIRMATION CONTEST OFFICIAL RULES</h2>

                    <p>THIS CONTEST IS OPEN TO CANADIAN RESIDENTS ONLY (EXCLUDING QUEBEC) AND IS GOVERNED BY CANADIAN LAW..</p>
                    <ol>
                        <li><strong>ELIGIBILITY:</strong> Contest is open to all legal residents of Canada (excluding Quebec) who (i) have reached the age of majority in their province/territory of residence at the time of entry or whose date of birth falls within the Contest Period and (ii) are members of the Bioré Skincare Newsletter database as of June 15, 2014, except employees, representatives or agents (and those with whom such persons are domiciled, whether related or not) of Sponsor, its parent companies, subsidiaries, affiliates, prize suppliers and advertising/promotion agencies (collectively, the “<b>Contest Parties</b>”). </li>

                        <li>
                            <p><strong>HOW TO ENTER:</strong> NO PURCHASE NECESSARY. Beginning at approximately 12:00:01 a.m. ET on June 15, 2014, the named/original recipients of the Bioré Newsletter Confirmation Request must fully complete and submit the online entry form confirming their interest in continuing to receive the Bioré Skincare Newsletter (“Newsletter”).  There is no purchase necessary receive the Newsletter and recipients may unsubscribe from the Newsletter at any time. To confirm your interest in receiving the Newsletter, click the unique link in the eblast sent to you, or go to http://www.biore.ca/en-CA/email-newsletter-sign-up (the “Website”) and fully complete the confirmation form, which requires you to enter your full name, complete mailing address (including postal code), valid email address, date of birth, and preferred language and to indicate products of interest as well as to indicate “Yes” to confirm your interest in  receiving the Newsletter and from the Bioré brand.  Once you have fully completed the confirmation form, click the “SUBMIT” button. Your confirmation will automatically count as your Entry into the Contest if your date of birth falls within the Contest Period (the “<b>Entry</b>”). To be eligible, your Entry must be received prior to the Contest Period. All eligible Entries received prior to or during the Contest Period will be entered into the random prize draw (see Rule 5). </p>

                            <p>There is a limit of one (1) Entry per person/email address permitted during the Contest Period.  For greater certainty, you can only use one (1) email address to enter the Contest.  If it is discovered that any person has attempted to: (i) obtain more than one (1) Entry per person/email address during the Contest Period; and/or (ii) use (or attempt to use) multiple names, identities, dates of birth and/or more than one (1) email address to enter the Contest; then (in the sole and absolute discretion of the Sponsor) he/she may be disqualified from the Contest. Your Entry will be rejected if (in the sole and absolute discretion of the Sponsor) the entry form is not fully completed and submitted prior to or during the Contest Period.  Use (or attempted use) of multiple names, identities, dates of birth, email addresses and/or any automated system to enter or otherwise disrupt this Contest is prohibited and is grounds for disqualification by the Sponsor. The Releasees (defined below) are not responsible for late, lost, misdirected, delayed, incomplete or incompatible Entries.  </p>

                            <p>All Entries are subject to verification at any time. The Sponsor reserves the right, in its sole and absolute discretion, to require proof of identity and/or eligibility (in a form acceptable to the Sponsor – including, without limitation, government issued photo identification) to participate in this Contest.  Failure to provide such proof in a timely manner may result in disqualification.  The sole determinant of the time for the purposes of a valid Entry in this Contest will be the Contest server machine(s).</p>

                        </li>

                        <li><strong>THE PRIZE AND APPROXIMATE RETAIL VALUE:</strong> There will be five (5) prizes available to be won consisting of a $100 CDN pre-paid credit card (“the Prize”). The Prize has a retail value of $100.00 CDN.  Prize must be accepted as awarded and is not transferable or convertible to cash. No substitutions except at Sponsor’s option. Sponsor reserves the right, in its sole and absolute discretion, to substitute the Prize or a component thereof with a prize of equal or greater value, including, without limitation, but at Sponsor’s sole and absolute discretion, a cash award. Prize will only be awarded to the person whose full name and valid email address appears on the official Contest entry form. </li>

                        <li>
                            <p><strong>RANDOM PRIZE DRAW AND WINNER SELECTION:</strong> Five (5) winners will be drawn at random from among all eligible Entries received during the Contest Period in Mississauga, Ontario, at approximately 10:00 a.m. ET on July 7, 2014 (the “<b>Draw Date</b>”).  The odds of winning depend on the number of eligible Entries received during the Contest Period. </p>

                            <p>The Sponsor or its designated representative will make a maximum of three (3) attempts to contact the selected entrant by email (using the information provided at the time of entry) within ten (10) business days of the Draw Date.  If the selected entrant cannot be contacted within the maximum three (3) attempts or ten (10) business days of the Draw Date (whichever occurs first), or if there is a return of any notification as undeliverable; then the selected entrant will be disqualified (and will forfeit all rights to the Prize) and the Sponsor reserves the right, in its sole and absolute discretion, to randomly select an alternate eligible entrant from among the remaining eligible Entries (in which case the foregoing provisions of this section shall apply to such new selected entrant).  </p>
                        </li>

                        <li><strong>BEFORE BEING DECLARED THE CONFIRMED PRIZE WINNER,</strong> the selected entrant will be required to: (a) correctly answer a mathematical skill-testing question without mechanical or other aid; and (b) sign and return within ten (10) business days of notification the Sponsor’s declaration and release form, which (among other things): (i) confirms compliance with these Rules; (ii) acknowledges acceptance of the Prize as awarded; (iii) releases the Contest Parties and each of their respective officers, directors, agents, representatives, successors and assigns (collectively, the “Releasees”) from any and all liability in connection with this Contest, his/her participation therein and/or the awarding and use/misuse of the Prize or any portion thereof; and (iv) agrees to the publication, reproduction and/or other use of his/her name, address, voice, statements about the Contest and/or photograph or other likeness without further notice or compensation, in any publicity or advertisement carried out by or on behalf of the Sponsor in any manner whatsoever, including print, broadcast or the internet.  If the selected entrant: (a) fails to correctly answer the skill-testing question; (b) fails to return the properly executed Contest documents within the specified time; and/or (c) cannot accept the Prize for any reason; then he/she will be disqualified (and will forfeit all rights to the Prize) and the Sponsor reserves the right, in its sole and absolute discretion, to randomly select an alternate eligible entrant from among the remaining eligible Entries (in which case the foregoing provisions of this section shall apply to such new selected entrant).  </li>

                        <li>
                            <p><strong>GENERAL:</strong><br />All Entry materials become the property of the Sponsor.  The Releasees assume no responsibility for lost, delayed, incomplete, incompatible or misdirected Entries.  This Contest is subject to all applicable federal, provincial and municipal laws.  By entering the Contest, participants agree to be bound by these Official Contest Rules and Regulations (the “<b>Contest Rules</b>”) and by the decisions of the Sponsor with respect to all aspects of this Contest, which are final and binding on all entrants without right of appeal, including, without limitation, any decisions regarding the eligibility/disqualification of Entries and/or entrants.</p>

                            <p>The Releasees will not be liable for: (i) any failure of the Website during the Contest; (ii) any technical malfunction or other problems relating to the telephone network or lines, computer on-line systems, servers, access providers, computer equipment or software; (iii) the failure of any Entry to be received by the Contest Parties for any reason including, but not limited to, technical problems or traffic congestion on the Internet or at any website; (iv) any injury or damage to an entrant’s or any other person’s computer or other device related to or resulting from participating or downloading any material in the Contest; and/or (v) any combination of the above.  </p>

                            <p>In the event of a dispute regarding who submitted an Entry, Entries will be deemed to have been submitted by the authorized account holder of the email address submitted at the time of entry.  “Authorized account holder” is defined as the person who is assigned an email address by an internet provider, online service provider, or other organization (e.g. business, educational institute, etc.) that is responsible for assigning email addresses for the domain associated with the submitted email address. An entrant may be required to provide proof that he/she is the authorized account holder of the email address associated with the selected Entry</p>

                            <p>The Sponsor reserves the right, in its sole and absolute discretion and without prior notice to adjust any of the dates and/or timeframes stipulated in these Rules, to the extent necessary, for purposes of verifying compliance by any entrant or Entry with these Rules, or as a result of technical problems, or in light of any other circumstances which, in the opinion of the Sponsor, in its sole and absolute discretion, affect the proper administration of the Contest as contemplated in these Contest Rules.</p>

                            <p>The Sponsor reserves the right to withdraw, amend or suspend this Contest (or to amend these Contest Rules) in any way, in the event of an error, technical problem, computer virus, bugs, tampering, unauthorized intervention, fraud, technical failure or any other cause beyond the reasonable control of the Sponsor that interferes with the proper conduct of this Contest as contemplated by these Contest Rules.  Any attempt to deliberately damage any website or to undermine the legitimate operation of this Contest is a violation of criminal and civil laws and should such an attempt be made, the Sponsor reserves the right to seek remedies and damages to the fullest extent permitted by law.  The Sponsor reserves the right to cancel, amend or suspend this Contest, or to amend these Contest Rules, without prior notice or obligation, in the event of any accident, printing, administrative, or other error or any kind, or for any other reason. </p>

                            <p>By completing the entry form, all entrants consent to the collection, use and distribution of their personal information by the Sponsor only for the purposes of running the Contest (information is stored for approximately one year).  Sponsor will not sell or transmit this information to third parties except for the purposes of administering the Contest.  For information on use of personal information in connection with this Contest, see the privacy policy posted at http://www.biore.ca/en-CA/privacy/. </p>

                            <p>The terms of this Contest, as set out in these Contest Rules, are not subject to amendment or counter-offer, except as set out herein.</p>

                            <p>All intellectual property used by the Sponsor in connection with the promotion and/or administration of the Contest, including, without limitation, all trade-marks, trade names, logos, designs, promotional materials, web pages, source code, drawings, illustrations, slogans and representations are owned (or licensed, as the case may be) by the Sponsor and/or its affiliates. All rights are reserved. Unauthorized copying or use of any such intellectual property without the express written consent of its owner is strictly prohibited. </p>

                            <p>In the event of any discrepancy or inconsistency between the terms and conditions of these English Contest Rules and disclosures or other statements contained in any Contest-related materials, including, but not limited to: the Contest entry form, French version of these Contest Rules, and/or point of sale, television, print or online advertising; the terms and conditions of these English Contest Rules shall prevail, govern and control.</p>

                        </li>
                    </ol>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
