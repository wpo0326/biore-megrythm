﻿<%@ Page Title="Where To Buy | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.wheretobuy._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
	#storeLogos ul li { width:240px; height:120px; float:left; display:block; text-align:center;}
    #storeLogos ul li a {width:180px; height:100px; background-position:center left;text-indent:-9999px; display:block;}
	#storeLogos ul li a:hover { background-position:right;}
	</style>
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1>منافذ البيع</h1>
                    <div id="storeLogos" class="logos">
                        <p>تتوافر منتجات العناية بالبشرة من بيوريه للبيع في جميع أنحاء المنطقة.<br /><br /><br /></p>
                        <div class="content png-fix">
             
                <div style="float:right; width:32%; margin-right:1.33%">
                <h2><a href="#">الإمارات العربية المتحدة</a></h2> 
                <p>جميعة أبو ظبي التعاونية <br>
                العزيزية بنده المتحدة<br>
                شركة المايا التجارية- شركة ذات مسئولية محدودة<br>
                مجموعة كارفور<br>
                مجموعة لولو و EMKE <br>
                أسواق ميغا مارت الكبرى<br>
                مجموعة سفير<br>
                سبينس<br>
                شويترام<br>
                تعاونية الاتحاد</p><br>
                </div>

                <div style="float:left; width:32%; margin-right:1.33%">
				<h2><a href="#">الكويت</a></h2>
                <p>الميرة<br>
                كارفور<br>
                سيتي سنتر<br>
                جيان<br>
                جلف مارت<br>
                مركز لندن التسوقي<br>
                لولو<br>
                مركز سلطان</p><br>
                </div>
                
                <div style="float:left; width:32%; margin-right:1.33%">
                <h2><a href="#">قطر</a></h2>
                <p>الميرة<br>
                كارفور<br>
                مركز العائلة للمواد الغذائية<br>
                كابايان سوبر ماركت<br>
                أسواق لولو الكبرى<br>
                أسواق لولو الكبرى<br>
                سفاري الأسواق الكبرى <br>
                مركز الأطعمة البحرية<br>
                </p>
                </div>

           		<div class="clear"></div>
                
                <div style="float:right; width:32%; margin-right:1.33%">
				<h2><a href="#">البحرين</a></h2>
                <p>الجزيرة<br>
                بابا سنز<br>
                كارفور<br>
                جيان<br>
                مركز التجارة اللبناني<br>
                أسواق لولو الكبرى<br>
                أسواق ميدوي<br>
                مجموعة صيدليات رويان<br>
                أسواق سانا الكبرى / لاست شانس</p><br>
                </div>
                
                
                <div style="float:left; width:32%; margin-right:1.33%">
                <h2><a href="#">عمان</a></h2>
                <p>الفير<br>
                الجديد<br>
                كارفور<br>
                مركز التسوق الأسري<br>
                مجموعة كيمجي<br>
                مجموعة لولو<br>
                رواسكو<br>
                سفير<br>
                مركز سلطان<br>
                أسواق الرفاهية</p><br>
                </div>

                <div style="float:left; width:32%; margin-right:1.33%">
				<h2><a href="#">الأردن</a></h2>
                <p>كارفور<br>
                كوزمو<br>
                سي تاون<br>
                فود سيتي<br>
                ميلز <br>
                بلازا إس إس<br>
                سيفوي <br>
                ياسر مول</p><br>
                </div>
                
           		<div class="clear"></div>
                
                <div style="float:right; width:32%; margin-right:1.33%">
                <h2><a href="#">المملكة العربية السعودية</a></h2>
                <p>الدانوب<br>
                محلات العثيم<br>
                أسواق الجزيرة الكبرى<br>
                السروات<br>
                أسواق بن داود<br>
                كارفور<br>
                9 المزرعة <br>
                مركز تسوق المزرعة<br>
                شركة بنده-العزيزية<br>
                أسواق النجمة<br>
                تميمي</p>
                </div>
                
           		<div class="clear"></div>
                
                </div>        
                    </div>
                    <div style="clear:both; height:0px;"></div>
            </div>
        </div>
    </div>
</asp:Content>