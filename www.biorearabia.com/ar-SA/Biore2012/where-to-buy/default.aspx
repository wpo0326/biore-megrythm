﻿<%@ Page Title="Where To Buy | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.wheretobuy._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
	#storeLogos ul li { width:240px; height:120px; float:left; display:block; text-align:center;}
    #storeLogos ul li a {width:180px; height:100px; background-position:center left;text-indent:-9999px; display:block;}
	#storeLogos ul li a:hover { background-position:right;}
	</style>
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1>منافذ البيع</h1>
                    <div id="storeLogos" class="logos">
                        <p style="text-align:center">تتوافر منتجات العناية بالبشرة من بيوريه للبيع في جميع أنحاء المنطقة.<br /><br /><br /></p>
                        <div class="content png-fix">
             
                <div class="buyCountry">
                <h2><a href="#">الإمارات العربية المتحدة</a></h2> 
                <p>جميعة أبو ظبي التعاونية <br>
                العزيزية بنده المتحدة<br>
                شركة المايا التجارية- شركة ذات مسئولية محدودة<br>
                مجموعة كارفور<br>
                مجموعة لولو و سوبر ماركت <br>
                أسواق ميغا مارت الكبرى<br>
                مجموعة سفير<br>
                سبينس<br>
                شويترام<br>
                تعاونية الاتحاد<br />
                صيدلية بوتس<br />
                صيدلية بن سينا<br />
                صيدلية الحياة 
                </p><br>
                </div>

                 
                <div class="buyCountry">
                <h2><a href="#">المملكة العربية السعودية</a></h2>
                <p>الدانوب<br>
                محلات العثيم<br>
                أسواق الجزيرة الكبرى<br>
                السروات<br>
                أسواق بن داود<br>
                كارفور<br>
                المزرعة و مركز تسوق المزرعة<br>
                شركة بنده-العزيزية<br>
                أسواق النجمة<br>
                تميمي<br />
                الصيدليات موحدة<br />
                الصيدليات Nahdy <br />
                صيدليات الدواء
                
                </p>
                </div>
                
                <div class="buyCountry">
                <h2><a href="#">سلطنة عمان & البحرين</a></h2>
                <p>
                في صيدليات وسوبرماركت محددة <br /> في جميع أنحاء البلد. <br />
                </p>
                </div>
                
           		<div class="clear"></div>
                <div class="buyCountry">
				<h2><a href="#">الكويت</a></h2>
                <p>
                لولو<br>
                سيتي سنتر<br>
                جلف مارت<br>
                جيان<br>
                كارفور<br>
                Saveco هايبر ماركت<br>
                مركز سلطان<br>
                Haras Aa Watani COOP<br />
                Hateen COOP Society<br />
                Abu-Halifa COOP Society<br />
                Sharq COOP<br />
                Shamieh & Shuwaikh COOP<br />
                Abdullah Salem & Mansooriya COOP<br />
                Faiha COOP<br />
                Nuzha COOP<br />
                Khaldiya COOP<br />
                Yarmouk COOP<br />
                Kaifan COOP<br />
                Adailiya COOP<br />
                Qadsiya COOP<br />
                Rowdah & Hawally COOP<br />
                Surra COOP <br />
                Shaab COOP<br />
                Salmiah COOP<br />
                Rumaithya COOP <br />
                Jabriya COOP <br />
                Daheiah COOP <br />
                Jahra COOP <br />
                Sulaibikhat & Doha COOP <br />
                Andalus & Riggae COOP <br />
                Firdous COOP <br />
                Ardiya COOP <br />
                Omariya & Rabya COOP <br />
                Farwania COOP <br />
                Khaitan COOP <br />
                Mishref COOP <br />
                Bayan COOP <br />
                Sabah Al-Salem COOP <br />
                Rikka COOP <br />
                Hadiya COOP <br />
                Sabahiya COOP <br />
                Fahaheel COOP <br />
                Mubarak Al Kabeer & Qurain COOP <br />
                Fintas COOP <br />
                Ali Sabah Al Salem COOP <br />
                Sabah Al Naser COOP <br />
                Daher COOP <br />
                Salwa COOP <br />
                Qortuba COOP <br />
                Jaber Al-Ali COOP <br />
                Adan & Qusoor COOP <br />
                Rihab COOP <br />
                Naseem COOP <br />
                Ahmadi COOP <br />
                Zahra COOP <br />
                Madina Saad Al Adulla COOP <br />
                Abdulla Al Mubarak COOP <br />
                Qairwan COOP <br />
                Defence Ministry COOP <br />
                Ali Abdul Wahab  & Sons Pharmacy <br />
                Fialaka Pharmacy <br />
                Al Qattan Pharmacy <br />
                </p><br>
                </div>
                </div>        
                    </div>
                    <div style="clear:both; height:0px;"></div>
            </div>
        </div>
    </div>
</asp:Content>