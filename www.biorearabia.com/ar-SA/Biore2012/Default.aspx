﻿<%@ Page Title="Get a Clean Face | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Get healthy-looking, radiant skin with products from Bioré® Skincare" name="description" />
    <meta content="skincare, clean face" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/UniversPro67.css")
        .Add("~/css/homepage.css")
        .Add("~/css/flexslider.css")
        .Render("~/CSS/combinedhome_#.css")
    %>
    <meta name="viewport" content="width=device-width">



    <script type="text/javascript">
        //$(document).ready(function () {
        //$('.scroll-dots a').click(function (event) {
        //    event.preventDefault();
        //    var target = $(this).attr('id');
        //    console.log(target);
        //});
        //});
    </script>
    <script src="js/homepage.js" type="text/javascript"></script>

    <script type="text/javascript">
    
    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main1">

        <div class="home-content">
            <section class="home-section iex" id="section-01">
                <!-- <div class="newicon">
                <img src="/ar-SA/images/homepage/new-top-icon.png" alt="new">
            </div>-->
                <div class="animation overwritte">
                    <div class="animation-button">

                        <a href="https://www.youtube.com/embed/OdcanMRDU5E?rel=0&autoplay=1" target="_blank" class="fancybox-media fbm  fancybox.iframe">
                            <img src="/ar-SA/images/homepage/top-play-btn.png" alt="video play button" />
                        </a>

                    </div>
                </div>
                <div class="welcome-module">
                    <!--<video autoplay="autoplay" loop="loop" muted="muted" preload="auto" id="welcomeVideo">
                    <source src="<%= VirtualPathUtility.ToAbsolute("~/video/homepage/shayzilla.mp4") %>" type="video/mp4"/>
                    <source src="<%= VirtualPathUtility.ToAbsolute("~/video/homepage/shayzilla.ogv") %>" type="video/ogg"/>
                    <source src="<%= VirtualPathUtility.ToAbsolute("~/video/homepage/shayzilla.webm") %>" type="video/webm"/>
                </video>-->
                    <div id="welcomeImage" class="hide-mobile show" style="height: 100%">
                        <img class="ïmg-less" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/charcoal-top-model-bg.jpg") %>" />
                    </div>
                    <div id="welcomeImage" class="hide-desktop">
                        <img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/charcoal-top-model-bg.jpg") %>" />
                    </div>
                    <div class="welcome-module-content">
                        <h1>- اكتشفي بيوريه<br />
                            خبيرة المسام الحرة</h1>
                        <p>
                            <span class="welcome-module-line1">و قوة التنظيف العميق</span><br />
                            <br />
                            <span class="welcome-module-line2">للفحم وبيكربونات الصودا</span>
                        </p>

                    </div>
                    <div class="hide-mobile">
                    </div>
                    <div class="bubbles-bg hide-mobile">
                        <div class="bubbles-charcoal-nosestrips">
                            <img class="products-responsive" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/charcoal-nosestrips.png") %>" />
                        </div>
                        <div class="bubbles-charcoal-active">
                            <img class="products-responsive" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/charcoal-active.png") %>" />
                        </div>
                        <div class="bubbles-charcoal-bakingsoda">
                            <img class="products-responsive" src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/products/charcoal-baking-soda.png") %>" />
                        </div>

                    </div>
                    <!--<p class="scrollDown">SCROLL DOWN</p>
                    <div id="arrowBounce" class="hide-mobile">
                        <a href="#section-02"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/homepage/arrowDown.png") %>" class="section-down-arrow"/></a>
                    </div>-->
                </div>
                <!--<div class="scroll-dots">
                <a href="#section-01"><span class="scroll-dot enable"></span></a>
                <a href="#section-02"><span class="scroll-dot"></span></a>
                <a href="#section-03"><span class="scroll-dot"></span></a>
                <a href="#section-04"><span class="scroll-dot"></span></a>
                <a href="#section-05"><span class="scroll-dot"></span></a>
            </div>-->
                <!--<div class="white-shadow hide-mobile"></div>-->
            </section>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/jquery.flexslider.min.js")
        .Render("~/js/combinedhome_#.js")
    %>
</asp:Content>

