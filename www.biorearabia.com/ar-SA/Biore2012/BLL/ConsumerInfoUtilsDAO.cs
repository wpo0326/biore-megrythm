﻿using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Text;

// NOTE: This is NOT the same as the ConsumerInfoUtilsDAO in GlobalBase2010!!!
namespace Biore2012.BLL
{
	public class ConsumerInfoUtilsDAO
	{
		private static string _connString = ConfigurationManager.ConnectionStrings["consumerInfo"].ToString();
		private static string _SiteID = ConfigurationManager.AppSettings["SiteID"].ToString();

		public static DataSet getMemberInfo(int memberID)
		{
			using (SqlConnection cn = new SqlConnection(_connString))
			{
				SqlCommand cmd = new SqlCommand("spMembersSelect", cn);
				cmd.CommandType = CommandType.StoredProcedure;

				cmd.Parameters.Add("@MemberID", SqlDbType.Int);
				cmd.Parameters["@MemberID"].Value = memberID;

				cn.Open();

				using (SqlDataAdapter myCommand = new SqlDataAdapter(cmd))
				{

					DataSet ds = new DataSet();

					try
					{
						myCommand.Fill(ds, "MemberInfoTable");

					}
					catch (Exception ex)
					{
						StringBuilder theMessage = new StringBuilder();
						theMessage.Append("Error in spMembersSelect memberID: " + memberID + "/r/n");
						theMessage.Append("SiteID : " + _SiteID + "/r/n");
						theMessage.Append("Error: " + ex.Message.ToString() + "/r/n");
						theMessage.Append("Inner Exception: " + ex.InnerException.Message.ToString());
						LogEntry myLog = new LogEntry();
						myLog.EventId = 87774;
						myLog.Message = theMessage.ToString();
						myLog.Priority = 1;
						Logger.Write(myLog);
						myLog = null;
						ds = null;
					}

					return ds;
				}
			}
		}

		public static DataSet getMemberID(string email)
		{
			using (SqlConnection cn = new SqlConnection(_connString))
			{
				SqlCommand cmd = new SqlCommand("spMembersGetMemberId", cn);
				cmd.CommandType = CommandType.StoredProcedure;

				cmd.Parameters.Add("@Email", SqlDbType.VarChar);
				cmd.Parameters["@Email"].Value = email;

				cn.Open();

				using (SqlDataAdapter myCommand = new SqlDataAdapter(cmd))
				{

					DataSet ds = new DataSet();

					try
					{
						myCommand.Fill(ds, "MemberInfoTable");

					}
					catch (Exception ex)
					{
						StringBuilder theMessage = new StringBuilder();
						theMessage.Append("Error in spMembersGetMemberId email: " + email + "/r/n");
						theMessage.Append("SiteID : " + _SiteID + "/r/n");
						theMessage.Append("Error: " + ex.Message.ToString() + "/r/n");
						theMessage.Append("Inner Exception: " + ex.InnerException.Message.ToString());
						LogEntry myLog = new LogEntry();
						myLog.EventId = 87776;
						myLog.Message = theMessage.ToString();
						myLog.Priority = 1;
						Logger.Write(myLog);
						myLog = null;
						ds = null;
					}

					return ds;
				}
			}

		}

		public static DataSet getTraitValues(int memberID)
		{
			using (SqlConnection cn = new SqlConnection(_connString))
			{
				SqlCommand cmd = new SqlCommand("spMemberTraitsAllSelect", cn);
				cmd.CommandType = CommandType.StoredProcedure;

				cmd.Parameters.Add("@MemberID", SqlDbType.Int);
				cmd.Parameters["@MemberID"].Value = memberID;
				cmd.Parameters.Add("@SiteID", SqlDbType.Int);
				cmd.Parameters["@SiteID"].Value = _SiteID;

				cn.Open();

				using (SqlDataAdapter myCommand = new SqlDataAdapter(cmd))
				{

					DataSet ds = new DataSet();

					try
					{
						myCommand.Fill(ds, "MemberTraitTable");

					}
					catch (Exception ex)
					{
						StringBuilder theMessage = new StringBuilder();
						theMessage.Append("Error in spMemberTraitsAllSelect memberID: " + memberID + "/r/n");
						theMessage.Append("SiteID : " + _SiteID + "/r/n");
						theMessage.Append("Error: " + ex.Message.ToString() + "/r/n");
						theMessage.Append("Inner Exception: " + ex.InnerException.Message.ToString());
						LogEntry myLog = new LogEntry();
						myLog.EventId = 87777;
						myLog.Message = theMessage.ToString();
						myLog.Priority = 1;
						Logger.Write(myLog);
						myLog = null;
						ds = null;
					}

					return ds;
				}
			}

		}
	}
}
