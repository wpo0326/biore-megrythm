﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Charcoal.aspx.cs" Inherits="Biore2012.biore_facial_cleansing_products.Charcoal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Purify pores with our Charcoal Mask and Charcoal Face Wash. Try Deep Pore Charcoal Cleanser and Self Heating One Minute Mask from Bioré® Skincare." />
    <meta content="Charcoal Mask, Charcoal Face Wash, charcoal" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/charcoalLanding.css")
        .Render("~/css/combinednew_#.css")
    %>
    <meta name="viewport" content="width=device-width">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">
<script type="text/javascript" src="http://o2.eyereturn.com/?site=2687&amp;page=biore_charcoal_EN"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main" class="charcoalLanding">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
                <div class="charcoalHeaderSpacer"></div>
                <h1><img src="<%= VirtualPathUtility.ToAbsolute("~/images/charcoalLanding/charcoalHeader.png") %>" border="0" alt="" /></h1>
                
            <div id="charcoalProdWrap">
                <div id="charDescript">
                    <!--<div id="charPaint"></div>-->
                    <h2 class="archer-book">Really? <span class="charGrey archer-bold">Charcoal?</span></h2>
                    <p>Yes, charcoal!  Like a magnet, charcoal draws out and traps skin impurities for a deep clean.  Biore Skincare brings you this revolutionary ingredient in 4 new products:<br /><br />
                        <b>NEW <span class="charGreeen">Deep Pore Charcoal Cleanser</span></b><br>
						<b>NEW <span class="charGreeen">Pore Penetrating Charcoal Bar</span></b><br>
						<b>NEW <span class="charGreeen">Self-Heating One Minute Mask</span></b><br>
						<b>NEW <span class="charGreeen">Charcoal Deep Cleansing Pore Strips</span></b></p>
                </div>
                <div id="charProducts"></div>
            </div>
            
            <div class="charList archer-bold">
                <span class="charGreeen">Used in combination, the new Charcoal products deliver outstanding results</span><br /><br />

				<span class="charbullet"></span>Removes 24% more dirt and oil*<br />
				<span style="font-size:12px; line-height:14px;">(*Biore Deep Pore Charcoal Cleanser + Biore Self Heating One Minute Mask vs Biore Self Heating One Minute Mask alone)</span><br /><br />
                <span class="charbullet"></span>Removes 60% of blackheads from the nose*<br />
                <span style="font-size:12px; line-height:14px;">(*Biore Deep Pore Charcoal Cleanser + Biore Self Heating One Minute Mask + Biore Deep Cleansing Charcoal Pore Strips)</span><br /><br />
                <span class="charbullet"></span>Deep cleans & gently exfoliates for 2.5x cleaner pores<br />
                <span style="font-size:12px; line-height:14px;">(Pore Penetrating Charcoal Bar)</span><br /><br />
                <span class="charbullet"></span>Leaves skin tingly-smooth<br /><br />
                <span class="charbullet"></span>Deep cleans all 200,000 of your pores<br />
            </div>

            <!--<div id="charTryNowWrap">
                <div id="charTryProdOne">
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/dirty-and-oily-skin-no-more/deep-pore-charcoal-cleanser") %>"><div class="charTry">Try it Now</div></a>
                </div>
                <div id="charTryProdTwo">
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/dirty-and-oily-skin-no-more/self-heating-one-minute-mask") %>"><div class="charTry">Try it Now</div></a>
                </div>
            </div>-->
           
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
