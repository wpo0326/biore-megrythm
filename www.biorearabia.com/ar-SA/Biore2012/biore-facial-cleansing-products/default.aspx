﻿<%@ Page Title="Free Your Pores – Get Clean, Healthy Skin – Browse All Products  | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.our_products.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Bior&eacute;&reg; Skincare products cleanse and gently exfoliate, giving you healthy, clean skin! See the complete Bior&eacute;&reg; Skincare product line." name="description" />
    <meta content="Clear Skin, Biore&reg; Products" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>
    <meta name="viewport" content="width=device-width">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="intro">
                <h1>منتجاتنا</h1>
                <div id="responseRule"></div>
                    <p>توجهي الى السبب الرئيسي لمشاكل البشرة استعمال لصاقات المسام أسبوعياً و التنظيف يومياً
                        <br /><br />
                    منتجاتنا القوية و منظفة للمسام تأتي بشكل
                     سائل ، رغوة، سكراب و لصاقات لتمنحك بشرة نظيفة و صحية
                    </p>        
            </div>

           
            <div id="deepCleansingProds" class="prodList">
                <h2 class="pie roundedCorners deepCleansing"><span>الفحم</span>
للبشرة العادية إلى الدهنية 
                     <span class="arrow"></span>
                </h2>
            	<!--<div class="intro">
                <p>Don't let dirt take over your pores.  Biore deep cleansing products target deep-down impurities to defend against daily buildup - like dirt, oil and make-up - for a truly deep clean.</p>
                </div>-->
                <ul>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CHARCOAL_100X235.png" alt="" />
                         <!--<asp:Literal ID="litDeepPoreCharcoalCleanser" runat="server" />-->
                         <br />
                        <h3> منظّف فحم <br />للمسام العميقة</h3>
                        <p>ينظّف بعمق مرتين أكثر و ينقي طبيعيا</p>
                        <a href="../charcoal-for-normal-or-oily-skin/deep-pore-charcoal-cleanser" id="A1">تفاصيل ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/SHM_100X235.png" alt="" />
                        <!--<asp:Literal ID="litSelfHeatingOneMinuteMask" runat="server" />-->
                        <br>
                        <h3>قناع ذاتي التسخين<br /> في دقيقة وحدة</h3>
                        <p>ينقي المسام مرتين ونصف أفضل*</p>
                        <a href="../charcoal-for-normal-or-oily-skin/self-heating-one-minute-mask" id="A2">تفاصيل ></a>
                    </li>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalBar.png" alt="" />
                         <!--<asp:Literal ID="litCharcoalBar" runat="server" />-->
                         <br>
                        <h3>صابونة فحم <br /> تخترق المسام</h3>
                        <p>تنظف بعمق وتقشر البشرة برفق لمسام أنظف مرتين ونصف أكثر* <br /> رائعة للوجه والجسم</p>
                        <a href="../charcoal-for-normal-or-oily-skin/pore-penetrating-charcoal-bar" id="A1">تفاصيل ></a>
                    </li> 
                </ul>
            </div>

            <div id="bigPoresProds" class="prodList">
                <h2 class="pie roundedCorners bigPores"><span>بيكربونات الصودا</span> للبشرة المختلطة
                    <span class="arrow"></span>
                </h2>
                <ul>
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/Baking_Soda_Cleanser_small.png" alt="" />
                        
                        <h3>منظّف بيكربونات<br /> الصودا للمسام</h3>
                        <p>ينظف بعمق و يقشر البشرة الجافة </p>
                        <a href="../baking-soda-for-combination-skin/baking-soda-pore-cleanser" id="details-baking-soda-pore-cleanser">تفاصيل ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-cleansing-scrub-small.png" alt="" />
                        
                        <h3>سكراب بيكربونات<br /> الصودا المنظف</h3>
                        <p>تنظيف بعمق من غير إفراط في الفرك</p>
                        <a href="../baking-soda-for-combination-skin/baking-soda-cleansing-scrub" id="A3">تفاصيل ></a>
                    </li>
                    
                </ul>
            </div>

            <div id="murtProds" class="prodList">
                <h2 class="pie roundedCorners murt"><span>لصاقات المسام</span>
                    <span class="arrow"></span>
                </h2>
                <ul> 
                    
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStrips.png" alt="" />
                        <!--<asp:Literal ID="litPoreStrips" runat="server" />-->
                        <br>
                        <h3> لصاقات التنظيف<br /> العميق للمسام </h3>
                        <p>تزيل الشوائب التي تسدّ المسام لأعمق تنظيف </p>
                        <a href="../pore-strips/pore-strips#regular" id="details-deep-cleansing-pore-strips">تفاصيل ></a>
                    </li>              
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.png" alt="" />
                        <!--<asp:Literal ID="litPoreStripsCombo" runat="server" />-->
                        <br>
                        <h3> لصاقات مزدوجة للتنظيف <br />العميق للمسام</h3>
                        <p>4 لصاقات للأنف و4 لصاقات للوجه</p>
                        <a href="../pore-strips/pore-strips#combo" id="details-deep-cleansing-pore-strips-combo">تفاصيل ></a>
                    </li>
                    
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltra.png" alt="" />
                        <!--<asp:Literal ID="litPoreStripsUltra" runat="server" />-->
                        <br>
                        <h3>لصاقات آلترا للتنظيف <br /> العميق للمسام</h3>
                        <p> مع خلاصة ويتش هازل  <br /> وزيت شجرة الشاي</p>
                        <a href="../pore-strips/pore-strips#ultra" id="details-deep-cleansing-pore-strips-ultra">تفاصيل ></a>
                    </li>   
                  
<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" />
                        <!--<asp:Literal ID="litCharcoalStrips" runat="server" />-->
                        <br>
                        <h3>لصاقات الفحم للتنظيف العميق للمسام</h3>
                        <p>تزيل الشوائب التي تسدّ المسام، وتستخرج الدهون الزائدة لأعمق تنظيف</p>
                        <a href="../pore-strips/charcoal-pore-strips" id="details-deep-cleansing-pore-strips-face">details ></a>
                    </li>
                </ul>
            </div>
            
             
            
            <div style="font-size:11px; line-height:10px; margin:20px 0;direction:rtl;">*تزيل لصاقات آلترا للتنظيف العميق للمسام من بيوريه® الشوائب العميقة التي تسدّ المسام مرتين أكثر من لصاقات التنظيف العميق للمسام بيوريه السابقة</div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
