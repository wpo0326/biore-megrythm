﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Charcoal.aspx.cs" Inherits="Biore2012.biore_facial_cleansing_products.Charcoal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Purify pores with our Charcoal Mask and Charcoal Face Wash. Try Deep Pore Charcoal Cleanser and Self Heating One Minute Mask from Bioré® Skincare." />
    <meta content="Charcoal Mask, Charcoal Face Wash, charcoal" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/charcoalLanding.css")
        .Render("~/css/combinednew_#.css")
    %>
    <meta name="viewport" content="width=device-width">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">
<script type="text/javascript" src="http://o2.eyereturn.com/?site=2687&amp;page=biore_charcoal_EN"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main" class="charcoalLanding">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
                <div class="charcoalHeaderSpacer"></div>
                <h1><img src="<%= VirtualPathUtility.ToAbsolute("~/images/charcoalLanding/charcoalHeader.png") %>" border="0" alt="" /></h1>
                
            <div id="charcoalProdWrap">
                <div id="charDescript">
                    <!--<div id="charPaint"></div>-->
                    <h2 class="archer-book">هل هذا معقول؟ <span class="charGrey archer-bold">الفحم؟</span></h2>
                    <p> نعم، الفحم! فمثل المغناطيس، يتشبّث الفحم بشوائب البشرة ويسحبها للحصول على أعمق تنظيف. تقدم لك بيوريه للعناية بالبشرة هذا المكوّن الرائد في 4 منتجات جديدة: <br /><br />
                        <b>جديد <span class="charGreeen">منظّف فحم للمسام العميقة</span></b><br>
						<b>جديدة <span class="charGreeen">صابونة فحم تخترق المسام</span></b><br>
						<b>جديد <span class="charGreeen">قناع سريع ذاتي التسخين</span></b><br>
						<b>جديد <span class="charGreeen">لصاقات فحم للتنظيف العميق للمسام</span></b></p>
                </div>
                <div id="charProducts"></div>
            </div>
            
            <div class="charList archer-bold">
                <span class="charGreeen">عند استخدام منتجات الفحم الجديدة معاً، ستنبهرين بنتائج مذهلة</span><br /><br />

				<span class="charbullet"></span> تزيل 24 بالمئة أكثر من الأوساخ والدهون*<br />
				<span style="font-size:14px; line-height:14px;">(*منظف فحم للمسام العميقة بيوريه + قناع سريع ذاتي التسخين بيوريه مقابل قناع سريع ذاتي التسخين بيوريه وحده)</span><br /><br />
                <span class="charbullet"></span> تزيل 60 بالمئة أكثر من الرؤوس السوداء الظاهرة على الأنف*<br />
                <span style="font-size:12px; line-height:14px;">(*منظف فحم للمسام العميقة بيوريه + قناع سريع ذاتي التسخين بيوريه + لصاقات فحم للتنظيف العميق للمسام بيوريه)</span><br /><br />
                <span class="charbullet"></span> تنظف بعمق وتقشر البشرة برفق لمسام أنظف مرتين ونصف أكثر<br />
                <span style="font-size:12px; line-height:14px;">(صابونة فحم تخترق المسام)</span><br /><br />
                <span class="charbullet"></span> تترك البشرة ناعمة ومنتعشة<br /><br />
                <span class="charbullet"></span> تنظّف بعمق كل مسامك البالغ عددها 200 ألف<br />
            </div>

            <!--<div id="charTryNowWrap">
                <div id="charTryProdOne">
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/charcoal-for-normal-or-oily-skin/deep-pore-charcoal-cleanser") %>"><div class="charTry">Try it Now</div></a>
                </div>
                <div id="charTryProdTwo">
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/charcoal-for-normal-or-oily-skin/self-heating-one-minute-mask") %>"><div class="charTry">Try it Now</div></a>
                </div>
            </div>-->
           
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
