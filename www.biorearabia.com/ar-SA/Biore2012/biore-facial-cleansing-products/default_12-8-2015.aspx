﻿<%@ Page Title="Free Your Pores – Get Clean, Healthy Skin – Browse All Products  | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.our_products.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Bior&eacute;&reg; Skincare products cleanse and gently exfoliate, giving you healthy, clean skin! See the complete Bior&eacute;&reg; Skincare product line." name="description" />
    <meta content="Clear Skin, Biore&reg; Products" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>
    <meta name="viewport" content="width=device-width">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="intro">
                <h1>منتجاتنا</h1>
                <div id="responseRule"></div>
                <p>منتجات بيوريه  للعناية بالبشرة تستهدف كل ما هو وراء أسباب مشاكل البشرة - المسام المسدودة. تأتي منتجات بيوريه الفعالة والمنظّفة للمسام، على شكل منظّف سائل، وصابونة، وقناع ولصاقات، لمساعدتك على التخلّص من الأوساخ التي تسدّ المسام! اذهبي من المدرسة إلى المكتبة، ومن المنزل إلى الشاطئ، ومن العمل إلى الرياضة ببشرة غير مسدودة وبمظهر صحي. لأنّك حين تنظفين المسام، تمنعين المشكلة من جذورها.</p>
            </div>

            <!--<div id="complexionClearingProds" class="prodList">
                <h2 class="pie roundedCorners complexionClearing">blemishes <span>are out of here</span>
                    <span></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishTreatingAstringentToner.png" alt="" />
                        <asp:Literal ID="litBlemishAstringent" runat="server" />
                        <h3>BLEMISH FIGHTING ASTRINGENT</h3>
                        <p>Help prevent breakouts with an oil-free toner.</p>
                        <a href="../blemishes-are-out-of-here/blemish-fighting-astringent-toner" id="details-blemish-fighting-astringent">details ></a>
                    </li>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishFightingIceCleanser.png" alt="" />
                        <asp:Literal ID="litBlemishIceCleanser" runat="server" />
                        <h3>BLEMISH FIGHTING<br />ICE CLEANSER</h3>
                        <p>With Salicylic acid, this formula cleans pores of dirt, oil, and makeup.</p>
                        <a href="../blemishes-are-out-of-here/blemish-fighting-ice-cleanser" id="details-blemish-fighting-ice-cleanser">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/acneClearingScrub.png" alt="" />
                        <asp:Literal ID="litAcneClearingScrub" runat="server" />
                        <h3>BLEMISH CLEARING<br />SCRUB</h3>
                        <p>Defeat deep-down dirt and oil in 2 days.</p>
                        <a href="../blemishes-are-out-of-here/acne-clearing-scrub" id="details-acne-clearing-scrub">details ></a>
                    </li>
                  
                </ul>
            </div>-->

            <div id="deepCleansingProds" class="prodList">
                <h2 class="pie roundedCorners deepCleansing">لا بشرة متسخة ودهنية بعد الآن
                    <span class="arrow"></span>
                </h2>
            	<!--<div class="intro">
                <p>Don't let dirt take over your pores.  Biore deep cleansing products target deep-down impurities to defend against daily buildup - like dirt, oil and make-up - for a truly deep clean.</p>
                </div>-->
                <ul>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CHARCOAL_100X235.png" alt="" />
                         <!--<asp:Literal ID="litDeepPoreCharcoalCleanser" runat="server" />-->
                         <br />
                        <h3> منظّف فحم <br />للمسام العميقة</h3>
                        <p>ينظّف بعمق مرتين أكثر و ينقي طبيعيا</p>
                        <a href="../dirty-and-oily-skin-no-more/deep-pore-charcoal-cleanser" id="A1">تفاصيل ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/SHM_100X235.png" alt="" />
                        <!--<asp:Literal ID="litSelfHeatingOneMinuteMask" runat="server" />-->
                        <br>
                        <h3>قناع سريع <br /> ذاتي التسخين</h3>
                        <p>يطهّر المسام مرتين ونصف أفضل*</p>
                        <a href="../dirty-and-oily-skin-no-more/self-heating-one-minute-mask" id="A2">تفاصيل ></a>
                    </li>
                     <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/poreUncloggingScrub.png" alt="" />
                        <asp:Literal ID="litPoreUncloggingScrub" runat="server" />
                        <h3>PORE UNCLOGGING <br />SCRUB</h3>
                        <p>Deep cleans pores & smoothes skin.</p>
                        <a href="../dirty-and-oily-skin-no-more/pore-unclogging-scrub" id="A3">details ></a>
                    </li>      
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/makeupRemovingTowelettes.png" alt="" />
                        <asp:Literal ID="litMakeUpTowelettes" runat="server" />
                        <h3>MAKE-UP <br />REMOVING WIPES</h3>
                        <p>Thoroughly cleans & removes make-up.</p>
                        <a href="../dirty-and-oily-skin-no-more/make-up-removing-towelettes" id="details-make-up-removing-towelettes">details ></a>
                    </li>
                    
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/dailyCleansingCloths.png" alt="" />
                        <asp:Literal ID="litDailyCleansingCloths" runat="server" />
                        <h3>DAILY DEEP PORE CLEANSING WIPES</h3>
                        <p>Deep cleans pores & exfoliates skin in one wipe.</p>
                        <a href="../dirty-and-oily-skin-no-more/daily-cleansing-cloths" id="details-daily-cleansing-cloths">details ></a>
                    </li>           
                                       
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/4In1DetoxifyingCleanser.png" alt="" />
                        <asp:Literal ID="litPoreFoamCleanser" runat="server" />
                        <h3>PORE DETOXIFYING <br />FOAM CLEANSER</h3>
                        <p>Cleanses, tones, stimulates & detoxifies.</p>
                        <a href="../dirty-and-oily-skin-no-more/pore-detoxifying-foam-cleanser" id="details-pore-detoxifying-foam-cleanser">details ></a>
                    </li>  
                        <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/nourishMoistureLotion.png" alt="" />
                         <asp:Literal ID="litNourishMoistureLotion" runat="server" />
                        <h3>NOURISH MOISTURE LOTION</h3>
                        <p>Hydrates pores for renewed, smooth skin.</p>
                        <a href="../dirty-and-oily-skin-no-more/nourish-moisture-lotion" id="details-nourish-moisture-lotion">details ></a>
                    </li> -->
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalBar.png" alt="" />
                         <!--<asp:Literal ID="litCharcoalBar" runat="server" />-->
                         <br>
                        <h3>صابونة فحم <br /> تخترق المسام</h3>
                        <p>تنظف بعمق وتقشر البشرة برفق لمسام أنظف مرتين ونصف أكثر*</p>
                        <a href="../dirty-and-oily-skin-no-more/pore-penetrating-charcoal-bar" id="A1">تفاصيل ></a>
                    </li> 
                </ul>
            </div>



            <div id="murtProds" class="prodList">
                <h2 class="pie roundedCorners murt">تخلّصي من <span> الرؤوس السوداء</span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/warmingAntiBlackheadCleanser.png" alt="" />
                        <asp:Literal ID="litBlackheadCleanser" runat="server" />
                        <h3>Warming Anti-<br />Blackhead Cleanser</h3>
                        <p>Heats up to open pores & target blackheads. </p>
                        <a href="../breakup-with-blackheads/warming-anti-blackhead-cleanser" id="details-deep-cleansing-blackhead-cleanser">details ></a>
                    </li>-->
                    
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStrips.png" alt="" />
                        <!--<asp:Literal ID="litPoreStrips" runat="server" />-->
                        <br>
                        <h3> لصاقات التنظيف<br /> العميق للمسام </h3>
                        <p>تزيل الشوائب التي تسدّ المسام لأعمق تنظيف </p>
                        <a href="../breakup-with-blackheads/pore-strips#regular" id="details-deep-cleansing-pore-strips">تفاصيل ></a>
                    </li>              
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.png" alt="" />
                        <!--<asp:Literal ID="litPoreStripsCombo" runat="server" />-->
                        <br>
                        <h3> لصاقات كومبو للتنظيف <br />العميق للمسام</h3>
                        <p>4 لصاقات للأنف و4 لصاقات للوجه</p>
                        <a href="../breakup-with-blackheads/pore-strips#combo" id="details-deep-cleansing-pore-strips-combo">تفاصيل ></a>
                    </li>
                    
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltra.png" alt="" />
                        <!--<asp:Literal ID="litPoreStripsUltra" runat="server" />-->
                        <br>
                        <h3>لصاقات آلترا للتنظيف <br /> العميق للمسام</h3>
                        <p> مع ويتش هازل <br /> وزيت شجرة الشاي</p>
                        <a href="../breakup-with-blackheads/pore-strips#ultra" id="details-deep-cleansing-pore-strips-ultra">تفاصيل ></a>
                    </li>   
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltraCombo.png" alt="" />
                        <asp:Literal ID="litPoreStripsFace" runat="server" />
                        <h3 class="combo">COMBO DEEP CLEANSING PORE STRIPS WITH ULTRA NOSE STRIPS</h3>
                        <p>Removes 2x more deep-down clogged pores.*</p>
                        <a href="../breakup-with-blackheads/pore-strips#ultracombo" id="details-deep-cleansing-pore-ultra-combo">details ></a>
                    </li>-->
                  
<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" />
                        <!--<asp:Literal ID="litCharcoalStrips" runat="server" />-->
                        <br>
                        <h3>لصاقات الفحم للتنظيف العميق للمسام</h3>
                        <p>تزيل الشوائب التي تسدّ المسام، وتستخرج الدهون الزائدة لأعمق تنظيف</p>
                        <a href="../breakup-with-blackheads/charcoal-pore-strips" id="details-deep-cleansing-pore-strips-face">details ></a>
                    </li>
                </ul>
            </div>
            
             <!--<div id="bigPoresProds" class="prodList">
                <h2 class="pie roundedCorners bigPores">hello visibly <span>smaller pores</span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreMinimizer.png" alt="" />
                        <asp:Literal ID="litCharcoalPoreMinimizer" runat="server"></asp:Literal>
                        <h3>Charcoal<br />
                      Pore Minimizer</h3>
                        <p>Gently exfoliates & deep cleans to instantly reduce the appearance of pores.	</p>
                        <a href="../hello-visibily-smaller-pores/charcoal-pore-minimizer" id="details-charcoal-pore-minimizer">details ></a>
                    </li>
                    
                </ul>
            </div>-->
            
            <div style="font-size:11px; line-height:10px; margin:20px 0;direction:rtl;">*تزيل لصاقات آلترا للتنظيف العميق للمسام من بيوريه® الشوائب العميقة التي تسدّ المسام مرتين أكثر من لصاقات التنظيف العميق للمسام بيوريه السابقة</div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
