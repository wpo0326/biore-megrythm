﻿<%@ Page Title="Über Bioré | Bioré" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.about_us._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Leer meer over Bioré® Huidverzorging " name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent" class="aboutBlueBG">
            <div class="centeringDiv">
                <div id="content" class="aboutBlueTxt">
                    <div class="titleh1">
                        <h1>من بيوريه</h1>
                        <div id="responseRule"></div>
                    </div>
                    <div class="about-info" dir="rtl">     
                        
                        <p><span>اكتشفي بيوريه خبيرة المسام الحرة</span></p>

                        <p>منتجات بيوريه للعناية بالبشرة تستهدف السبب الرئيسي لمشاكل البشرة - المسام المسدودة.</p> 
<p>منتجات بيوريه مع بيكربونات الصودا،المعروفة بقوة التنظيف العميق، والفحم، والمعروف بقوة امتصاصه الفعالة، إزالة الأوساخ والزيوت والزهوم من المسام للمساعدة في محاربة الرؤوس السوداء </p>  
<p>نحارب بذكاء
تأتي منتجات بيوريه الفعالة والمنظّفة للمسام، على شكل منظّف سائل جل، و بودرة ، و سكراب ولصاقات، لمساعدتك على التخلّص من الأوساخ  الموجودة في عمق المسام. لبشرة نظيفة و مسام غير مسدودة
</p>

                        <p><span>بيوريه - تخلصي من أوساخ المسام</span></p>
                                           
                       
                    </div>
                    <img src="../images/about/aboutBG.png" alt="" id="Img1" />                    
                </div>
            </div>
        </div>
    </div>
</asp:Content>

