﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KAOForms;

namespace KaoBrands.FormStuff
{
    public partial class ucFormUnsubscribe : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            OptinFormSuccess.Visible = false;
            OptinFormFailure.Visible = false;
            if (Request.QueryString["email"] != null && !Page.IsPostBack)
            {
                Email.Text = Server.UrlDecode(Request.QueryString["email"]);
            }

            if (Page.IsPostBack)
            {
                OptinForm.Visible = false;
                FormSubmitBLL SubmitDAO = new FormSubmitBLL();
                SubmitResultData ResultData = new SubmitResultData();

                //Submit Unsubscribe Request
                ResultData = SubmitDAO.submitUnsubscribeForm(Email.Text);

                // TODO: Update the following
                // CURRENTLY ResultData.StatusMsg is returning 9001 if the Email Address does not exist in ConsumerInfo or ExactTarget
                // So for now just mark it as Succss= true until we get better StatusMsg returned to differentiate 
                //    between a real Exception and just Email Address does not exist in ConsumerInfo or ExactTarget
                if (ResultData.StatusMsg != null && ResultData.StatusMsg.Equals("9001")) ResultData.Success = true;

                //Check to see if they answered the additional question and save to DB
                string unsubAddtlQ1 = whyRadio.Text;
                if (!String.IsNullOrEmpty(unsubAddtlQ1)) {
                    //Set Default Config Objects
                    DefaultValues oDefaultValues = DefaultFormDAO.GetDefaultValues();
                    int currentSiteID = oDefaultValues.CurrentSiteID;


                    int eventIDAddtlUnsubscribeQuestions = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["UnsubscribeAddtlQuestionsEventId"]);
                    string eventCode = System.Configuration.ConfigurationManager.AppSettings["UnsubscribeAddtlQuestionsEventCode"];
                    string eventOpenEnded = System.Configuration.ConfigurationManager.AppSettings["UnsubscribeAddtlQuestionsEventOpenEnded"];
                    int traitIDAddtlUnsubscribeQuestion = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["UnsubscribeAddtlQuestionsTraitId"]);;
                    string questionFormType = System.Configuration.ConfigurationManager.AppSettings["UnsubscribeAddtlQuestionsControlType"];


                    SiteData currentSiteData = SubmitDAO.getCurrenSiteData(currentSiteID, oDefaultValues.BrandSiteData);
                    MemberData mData = new MemberData();
                    //mData.FirstName = ucFormMemberInfo._txFirstName.Text;
                    //mData.LastName = ucFormMemberInfo._txLastName.Text;
                    mData.Email = Email.Text; //ucFormMemberInfo._txEmail.Text;
                    //mData.Gender = ucFormMemberInfo._ddGender.SelectedValue;
                    //mData.DOB = DOB;
                    //mData.Address1 = ucFormMemberInfo._txAddress1.Text;
                    //mData.Address2 = ucFormMemberInfo._txAddress2.Text;
                    //mData.Address3 = ucFormMemberInfo._txAddress3.Text;
                    //mData.City = ucFormMemberInfo._txCity.Text;
                    //mData.State = ucFormMemberInfo._ddState.SelectedValue;
                    //mData.PostalCode = ucFormMemberInfo._txPostalCode.Text;
                    //mData.Country = "United States";
                    //mData.Phone = ucFormMemberInfo._txPhone.Text;
                    //mData.QuestionComment = ucFormMemberInfo._txComment.Text;

                    // GET MEMBER ID
                    spStatus myStatus = new spStatus();
                    myStatus = SubmitDAO.submitMemberData(currentSiteID, false, eventIDAddtlUnsubscribeQuestions, eventCode, 0, 0, eventOpenEnded, currentSiteData, mData);
                    int MemberID = 0;
                    MemberID = myStatus.MemId;

                    //build an answerlist of all submitted custom questions and parameter values
                    List<UserAnswer> answerList = new List<UserAnswer>();
                    UserAnswer ua = new UserAnswer();
                    ua.traitID = traitIDAddtlUnsubscribeQuestion.ToString();
                    ua.value = unsubAddtlQ1;
                    ua.type = questionFormType;
                    answerList.Add(ua);

                    if (MemberID > 0) {
                        SubmitDAO.submitQuestionData(currentSiteID, eventIDAddtlUnsubscribeQuestions, MemberID, answerList);
                    }
                }

                if (ResultData.Success)
                {
                    OptinFormSuccess.Visible = true;
                }
                else
                {
                    OptinFormFailure.Visible = true;
                }
            }
        }
    }
}
