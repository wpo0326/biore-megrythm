﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.pore_care.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Pore Care 101 – Clear Blemishes & Clogged Pores | Bioré® Skincare" name="title" />
    <meta content="Clear Blemishes & Clogged Pores with a Pore Care Regimen from Bioré® Skincare." name="description" />
    <meta content="How to clean clogged pores, how to clean pores, clear blemishes, how to clear blemishes, blemish clearing" name="keywords" />

    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/poreCare.css")
        .Render("~/css/combinedprorecare_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="floodlightPixels" runat="server">
   <!-- header scripts here -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div class="centeringDiv">
                <div id="content">
                    <div class="titleh1">
                        <h1>PORE CARE <span>101</span></h1>
                        <div class="ruleBGa"></div>
                        <div class="responseRule"></div>

                    </div>
                    <div class="rowCont">
                        <div class="row1"> 
                            <div class="col1 colDesktop"><img src="../images/ourProducts/products/large/CHARCOAL_390X418.png" alt="" id="Img1" style="width:73%" /></div>
		                    <div class="col2"><h4>WHY CARE?</h4><h2>Maintaining clean, <br />healthy pores</h2><p>is important for sustaining healthy skin since the average adult has approximately 200,000 facial pores.</p></div>
                            <div class="col1 colMobile"><img src="../images/ourProducts/products/large/CHARCOAL_390X418.png" alt="" id="Img4" style="width:73%"/></div>    
		                </div>
                       <div class="row2">
                            <div class="col1"><h4>THE QUESTION:</h4><h2>Can clogged pores lead to blemishes?</h2><p><b>Really? Yes! </b>Dirt and oil collect around your pores, creating build-up that can clog the skin follicle.  If bacteria makes its way to the blockage and grows, this can cause inflamation and result in blemishes.  Swelling and redness may linger in the form of whiteheads, blackheads, and red inflamed patches of skin.</p></div>
		                    <div class="col2"><img src="../images/ourProducts/products/large/deepCleansingPoreStrips.png" style="width:73%" alt="" id="Img2" /></div>
		                </div>
                        <div class="row3">
                            <div class="col1 colDesktop"><img src="../images/fmas/FMA3BG.png" style="max-width:500px; width:100%" alt="" id="Img3" /></div>
		                    <div class="col2"><h4>THE SOLUTION:</h4><h2>Cleanse and Remove.</h2><p>Biore deep cleansing products target deep-down impurities to defend against daily buildup - like dirt, oil and make up - for a truly deep clean.</p></div>
                             <div class="col1 colMobile"><img src="../images/fmas/FMA3BG.png" style="max-width:500px; width:100%;" alt="" id="Img5" /></div>
		                </div>
                        <div style="clear:both;"></div>
                        <div id="Tips10">
                            <p class="largeNumber">10</p>
                            <p class="tipsFor green">TIPS FOR</p>
                            <p class="clearSkin">CLEAR SKIN</p>
                            <div class="ruleBG"></div>
                        </div>
		                <div class="bullets">	
                            <h3 class="green">• Keep a pore care regimen.</h3>
	                        <h3>• Clean skin gently with a mild cleanser.</h3>
	                        <h3 class="green">• Remove all dirt and makeup. </h3>
	                        <h3>• Wash twice a day, especially after exercising.</h3>
	                        <h3 class="green">• Avoid over-scrubbing or repeated skin washing.</h3>
	                        <h3>• Don’t squeeze, scratch, pick, or rub pimples. It can lead to infections and scarring. </h3>
	                        <h3 class="green">• Avoid touching your face with hands. </h3>
	                        <h3>• Avoid oily cosmetics and creams. </h3>
	                        <h3 class="green">• Use water-based formulas.</h3>
	                        <h3>• Stay out of the sun.  UV rays can make blemishes worse.  Seriously.</h3>
		                </div>
	                </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
    <%--<h1>pore care - scripts</h1>--%>
</asp:Content>
