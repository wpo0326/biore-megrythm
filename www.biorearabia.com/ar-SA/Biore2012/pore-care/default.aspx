﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.pore_care.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Pore Care 101 – Clear Blemishes & Clogged Pores | Bioré® Skincare" name="title" />
    <meta content="Clear Blemishes & Clogged Pores with a Pore Care Regimen from Bioré® Skincare." name="description" />
    <meta content="How to clean clogged pores, how to clean pores, clear blemishes, how to clear blemishes, blemish clearing" name="keywords" />

    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/poreCare.css")
        .Render("~/css/combinedprorecare_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="floodlightPixels" runat="server">
   <!-- header scripts here -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div class="centeringDiv">
                <div id="content">
                    <div class="titleh1">
                        <h1>معلومات أساسية حول العناية بالمسام </h1>
                        <div class="ruleBGa"></div>
                        <div class="responseRule"></div>

                    </div>
                    <div class="rowCont">
                        <div class="row1"> 
                            <div class="col1 colDesktop"><img src="../images/ourProducts/products/large/CHARCOAL_390X418.png" alt="" id="Img1" /></div>
                            <div class="col1 colMobile"><img src="../images/ourProducts/products/large/CHARCOAL_390X418.png" alt="" id="Img4"/></div>    
    	                    <div class="col2 RtoL"><h4>لمَ تُعتبر العناية بها أمراً مهماً؟</h4><h2>إنّ الحفاظ على مسام <br /> نظيفة وصحية</h2><p>هو أمر مهم للتنعّم ببشرة صحية، إذ أنّ وجه الشخص البالغ يحتوي على ما يناهز 200 ألف مسام.</p></div>
                            		                </div>
                       <div class="row2">
                             <div class="col2"><img src="../images/ourProducts/products/large/deepCleansingPoreStrips.png" alt="" id="Img2" /></div>

                            <div class="col1 RtoL"><h4>السؤال الذي يطرح نفسه:</h4><h2>هل يمكن أن يؤدي انسداد المسام<br /> إلى ظهور الشوائب؟</h2><p><b>هل هذا معقول؟ أجل! </b> فالأوساخ والدهون تتجمّع حول مسامك وتتراكم حتى تسدّ بصيلات البشرة.<br /> وإذا استطاعت البكتيريا اختراق الانسداد والنمو، فقد تسبّب الالتهاب وتؤدي إلى ظهور الشوائب. وقد تصاب بشرتك بالتورّم والاحمرار. </p></div>
		                   		                </div>
                        <div class="row3">
                            <div class="col1 colDesktop"><img src="../images/fmas/FMA3BG.png" alt="" id="Img3" /></div>
                             <div class="col1 colMobile"><img src="../images/fmas/FMA3BG.png" style="max-width:90%;alt="" id="Img5" /></div>
		
		                    <div class="col2 RtoL"><h4>الحلّ:</h4><h2>تنظيف المسام وإزالة الأوساخ.</h2><p>تستهدف منتجات بيوريه للتنظيف العميق الشوائب المتغلغلة في العمق لمنع تراكمها اليومي - مثل الأوساخ، الدهون والمكياج - للحصول على تنظيف عميق بامتياز.</p></div>
                                            </div>
                        <div style="clear:both;"></div>
                        <div id="Tips10">
                            <p class="largeNumber">10</p>
                            <p class="tipsFor green">نصائح</p>
                            <p class="clearSkin">لبشرة نظيفة</p>
                            <div class="ruleBG"></div>
                        </div>
		                <div class="bullets">	
                            <h3 class="green">•  إتّبعي نظاماً للعناية بالمسام.</h3>
	                        <h3>• نظّفي البشرة برفق باستخدام منظّف لطيف.</h3>
	                        <h3 class="green">• أزيلي كل الأوساخ والمكياج. </h3>
	                        <h3>• اغسلي وجهك مرتين يومياً، لا سيما بعد ممارسة الرياضة.</h3>
	                        <h3 class="green">•  تجنّبي الإفراط في الفرك أو الغسل المتكرر للبشرة.</h3>
	                        <h3>•  لا تعصري أو تحكّي أو تنقري أو تفركي البثور، لئلا تتسببي بظهور التهابات وندب. </h3>
	                        <h3 class="green">• تجنّبي لمس وجهك بيديك. </h3>
	                        <h3>•  تجنّبي مستحضرات التجميل والكريمات الدهنية. </h3>
	                        <h3 class="green">• استخدمي تركيبات مرتكزة على الماء.</h3>
	                        <h3>• ابتعدي عن أشعة الشمس. فالأشعة ما فوق البنفسجية تفاقم مظهر الشوائب. خذي الأمر على محمل الجدّ.</h3>
		                </div>
	                </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
    <%--<h1>pore care - scripts</h1>--%>
</asp:Content>
