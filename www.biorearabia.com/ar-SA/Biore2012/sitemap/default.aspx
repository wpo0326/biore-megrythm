﻿<%@ Page Title="Sitemap | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.sitemap._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1>خريطة الموقع</h1>
                 <div id="linkList">
                     <div id="prodLinks">
                         <ul class="col" id="col1">
                            <li><a href="../biore-facial-cleansing-products">منتجاتنا <span class="arrow">&rsaquo;</span></a></li>
                            <li>
                                <h2 class="pie deepCleansing"><span class="subNavHeadlines"><span class="greenBold">لا بشرة متسخة</span> ودهنية بعد الآ </span></h2>
                                <ul>
                                    <li><a href="../charcoal-for-normal-or-oily-skin/deep-pore-charcoal-cleanser">منظّف فحم للمسام العميقة</a></li>
                                    <li><a href="../charcoal-for-normal-or-oily-skin/self-heating-one-minute-mask">قناع سريع ذاتي التسخين</a></li>
                                    <li><a href="../charcoal-for-normal-or-oily-skin/pore-penetrating-charcoal-bar">صابونة فحم تخترق المسام</a></li>
                                </ul>
                                </li>
                        </ul>
                        <ul class="col" id="col2">
                            <li>
                               <h2 class="pie murt"><span class="subNavHeadlines">خلّصي من <span class="redBold">الرؤوس السوداء</span></span></h2>
                                <ul>
                                    <li><a href="../pore-strips/pore-strips#regular">لصاقات التنظيف العميق للمسام</a></li>
                                    <li><a href="../pore-strips/pore-strips#ultra">لصاقات آلترا للتنظيف العميق للمسام</a></li>
                                    <li><a href="../pore-strips/pore-strips#combo">لصاقات كومبو للتنظيف العميق للمسام</a></li>
                                    <li><a href="../pore-strips/charcoal-pore-strips">لصاقات الفحم للتنظيف العميق للمسام</a></li>
                                </ul>
                                <ul>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <ul class="col noPadding" id="col3">
                        <li>
                            <a href="../biore-facial-cleansing-products/charcoal.aspx">ما الجديد <span class="arrow">&rsaquo;</span></a>
                  
                        </li>
                        <!--<li><a href="../email-newsletter-sign-up">Sign Me Up <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../where-to-buy-biore">Where To Buy <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../biore-videos">Watch It Work <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../about-us">About Bior&eacute;<sup>&reg;</sup> Skincare <span class="arrow">&rsaquo;</span></a></li>-->
                        <li><a href="http://www.kaobrands.com/" target="_blank">Kao Brands Company <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../contact-us">اتصلي بنا <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../legal">Legal <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../privacy_policy">Privacy Policy <span class="arrow">&rsaquo;</span></a></li>
                     </ul>
                 </div>
                 <div id="photo"></div>
            </div>
        </div>
    </div>
</asp:Content>