﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Biore2012.facebook.permalinks.blemish_treating_astringent
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Status = "301 Moved Permanently";
            Response.AddHeader("Location", VirtualPathUtility.ToAbsolute("~/blemishes-are-out-of-here/blemish-treating-astringent-toner"));
        }
    }
}
