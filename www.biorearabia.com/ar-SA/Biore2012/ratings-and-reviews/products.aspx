﻿<%@ Page Title="Free Your Pores – Get Clean, Healthy Skin – Browse All Products  | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="products.aspx.cs" Inherits="Biore2012.ratings_and_reviews.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Bior&eacute;&reg; Skincare products cleanse and gently exfoliate, giving you healthy, clean skin! See the complete Bior&eacute;&reg; Skincare product line." name="description" />
    <meta content="Clear Skin, Biore&reg; Products" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>
    <meta name="viewport" content="width=device-width">
    <script type="text/javascript" src="//biore.ugc.bazaarvoice.com/static/7668redes-green-en_AU/bvapi.js"></script>
    <script type="text/javascript">$BV.configure("global", { submissionContainerUrl: "http://www.biore.com.au/FormConfig/Ratings-And-Reviews/Default.aspx" });</script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent" class="rnrPromo">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="intro">
                <h1>Our Products</h1>
                <div id="responseRule"></div>
                <p>Target the root of all skin problems &mdash; by stripping weekly and cleansing daily. Our powerful, pore-cleansing products come in liquid, foam, scrub, and strip forms to keep your skin clean and healthy-looking.</p>
            </div>

            <div id="complexionClearingProds" class="prodList">
                <h2 class="pie roundedCorners complexionClearing">acne's <span>outta here!<sup>&trade;</sup></span>
                    <span></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishFightingIceCleanser.png" alt="" />
                        <asp:Literal ID="litBlemishIceCleanser" runat="server" />
                        <h3>BLEMISH FIGHTING<br />ICE CLEANSER</h3>
                        <p>With Salicylic acid, this formula cleans pores of dirt, oil, and makeup.</p>
                        <a href="../blemishes-are-out-of-here/blemish-fighting-ice-cleanser" id="details-blemish-fighting-ice-cleanser" onclick="$BV.ui('rr', 'submit_review', {productId: 'blemish-fighting-ice-cleanser'});return false;">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/acneClearingScrub.png" alt="" />
                        <asp:Literal ID="litAcneClearingScrub" runat="server" />
                        <h3>ACNE CLEARING<br />SCRUB</h3>
                        <p>Defeat deep-down dirt and oil in 2 days.</p>
                        <a href="../blemishes-are-out-of-here/acne-clearing-scrub" id="details-acne-clearing-scrub" onclick="$BV.ui('rr', 'submit_review', {productId: 'acne-clearing-scrub'});return false;">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishTreatingAstringentToner.png" alt="" />
                        <asp:Literal ID="litBlemishAstringent" runat="server" />
                        <h3>BLEMISH FIGHTING ASTRINGENT</h3>
                        <p>Help prevent breakouts with an oil-free toner.</p>
                        <a href="../blemishes-are-out-of-here/blemish-fighting-astringent-toner" id="details-blemish-fighting-astringent" onclick="$BV.ui('rr', 'submit_review', {productId: 'blemish-fighting-astringent-tone'});return false;">details ></a>
                    </li>
                </ul>
            </div>

            <div id="deepCleansingProds" class="prodList">
                <h2 class="pie roundedCorners deepCleansing">don't be <span>dirty<sup>&trade;</sup></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CHARCOAL_100X235.png" alt="" />
                         <asp:Literal ID="litDeepPoreCharcoalCleanser" runat="server" />
                        <h3>Deep Pore<br />Charcoal Cleanser</h3>
                        <p>Charcoal formula acts as a magnet to draw out dirt and oil for a tingly clean.</p>
                        <a href="../charcoal-for-normal-or-oily-skin/deep-pore-charcoal-cleanser" id="A1" onclick="$BV.ui('rr', 'submit_review', {productId: 'deep-pore-charcoal-cleanser'});return false;">details ></a>
                    </li>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalBar.png" alt="" />
                         <asp:Literal ID="litCharcoalBar" runat="server" />
                        <h3>Pore Penetrating <br />Charcoal Bar</h3>
                        <p>Ideal for face and body.</p>
                        <a href="../charcoal-for-normal-or-oily-skin/charcoal-bar" id="A1"onclick="$BV.ui('rr', 'submit_review', {productId: 'charcoal-bar'});return false;">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/SHM_100X235.png" alt="" />
                        <asp:Literal ID="litSelfHeatingOneMinuteMask" runat="server" />
                        <h3>Self Heating<br />One Minute Mask</h3>
                        <p>Heats on contact and purifies pores in just one minute.</p>
                        <a href="../charcoal-for-normal-or-oily-skin/self-heating-one-minute-mask" id="A2" onclick="$BV.ui('rr', 'submit_review', {productId: 'self-heating-one-minute-mask'});return false;">details ></a>
                    </li>
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/4In1DetoxifyingCleanser.png" alt="" />
                        <asp:Literal ID="litPoreFoamCleanser" runat="server" />
                        <h3>Pore Revitalizing Foam Cleanser</h3>
                        <p>Clean, tone, and stimulate with our self-foaming formula.</p>
                        <a href="../charcoal-for-normal-or-oily-skin/pore-detoxifying-foam-cleanser" id="details-pore-detoxifying-foam-cleanser" onclick="$BV.ui('rr', 'submit_review', {productId: 'pore-detoxifying-foam-cleanser'});return false;">details ></a>
                    </li> --> 
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/combinationSkinBalancingCleanser.png" alt="" />
                        <asp:Literal ID="litComboSkinCleanser" runat="server" />
                        <h3>COMBINATION SKIN BALANCING CLEANSER</h3>
                        <p>Balance skin's moisture and address combination skin.</p>
                        <a href="../charcoal-for-normal-or-oily-skin/combination-skin-balancing-cleanser" id="details-combination-skin-balancing-cleanser" onclick="$BV.ui('rr', 'submit_review', {productId: 'combination-skin-balancing-cleanser'});return false;">details ></a>
                    </li>

                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/makeupRemovingTowelettes.png" alt="" />
                        <asp:Literal ID="litMakeUpTowelettes" runat="server" />
                        <h3>Daily Makeup Removing Towelettes</h3>
                        <p>Remove stubborn waterproof makeup and clean pores.</p>
                        <a href="../charcoal-for-normal-or-oily-skin/make-up-removing-towelettes" id="details-make-up-removing-towelettes" onclick="$BV.ui('rr', 'submit_review', {productId: 'make-up-removing-towelettes'});return false;">details ></a>
                    </li>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/poreUncloggingScrub.png" alt="" />
                        <asp:Literal ID="litPoreUncloggingScrub" runat="server" />
                        <h3>PORE UNCLOGGING SCRUB</h3>
                        <p>Smooths &amp; unclogs skin by targeting dirt and oil.</p>
                        <a href="../charcoal-for-normal-or-oily-skin/pore-unclogging-scrub" id="A3" onclick="$BV.ui('rr', 'submit_review', {productId: 'pore-unclogging-scrub'});return false;">details ></a>
                    </li>
                      
                </ul>
            </div>



            <div id="murtProds" class="prodList">
                <h2 class="pie roundedCorners murt">breakup with <span>blackheads<sup>&trade;</sup></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltra.png" alt="" />
                        <asp:Literal ID="litPoreStripsUltra" runat="server" />
                        <h3>Ultra Deep Cleansing Pore Strips</h3>
                        <p>Defeat blackheads and remove 2x more deep down dirt.*</p>
                        <a href="../pore-strips/pore-strips#ultra" id="details-deep-cleansing-pore-strips-ultra" onclick="$BV.ui('rr', 'submit_review', {productId: 'deep-cleansing-pore-strips-ultra'});return false;">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStrips.png" alt="" />
                        <asp:Literal ID="litPoreStrips" runat="server" />
                        <h3>DEEP CLEANSING<br />PORE STRIPS</h3>
                        <p>Unclog pores with magnet-like power. Instantly removes weeks' worth of gunk. </p>
                        <a href="../pore-strips/pore-strips#regular" id="details-deep-cleansing-pore-strips"  onclick="$BV.ui('rr', 'submit_review', {productId: 'deep-cleansing-pore-strips'});return false;">details ></a>
                    </li>
                    
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.png" alt="" />
                        <asp:Literal ID="litPoreStripsCombo" runat="server" />
                        <h3>Deep Cleansing Pore<br />Strips Combo</h3>
                        <p>Unclog pores with magnet-like power on your nose, chin, cheeks, and forehead.</p>
                        <a href="../pore-strips/pore-strips#combo" id="details-deep-cleansing-pore-strips-combo" onclick="$BV.ui('rr', 'submit_review', {productId: 'deep-cleansing-pore-strips-combo'});return false;">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsFace.png" alt="" />
                        <asp:Literal ID="litPoreStripsFace" runat="server" />
                        <h3>Deep Cleansing Pore<br />Strips for the Face</h3>
                        <p>Unclog pores on your chin, cheeks, and forehead.</p>
                        <a href="../pore-strips/deep-cleansing-pore-strips-face" id="details-deep-cleansing-pore-strips-face"onclick="$BV.ui('rr', 'submit_review', {productId: 'deep-cleansing-pore-strips-face'});return false;">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" />
                        <asp:Literal ID="litCharcoalStrips" runat="server" />
                        <h3>Deep Cleansing Charcoal<br />Pore Strips</h3>
                        <p>Unclog pores & see 3x less oil with a single use.</p>
                        <a href="../pore-strips/charcoal-pore-strips" id="charcoal-pore-strips"onclick="$BV.ui('rr', 'submit_review', {productId: 'charcoal-pore-strips'});return false;">details ></a>
                    </li>
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/warmingAntiBlackheadCleanser.png" alt="" />
                        <asp:Literal ID="litBlackheadCleanser" runat="server" />
                        <h3>Warming Anti-Blackhead Cleanser</h3>
                        <p>Destroy dirt & oil with a warming formula of micro-beads. </p>
                        <a href="../pore-strips/warming-anti-blackhead-cleanser" id="details-deep-cleansing-blackhead-cleanser" onclick="$BV.ui('rr', 'submit_review', {productId: 'deep-cleansing-blackhead-cleanser'});return false;">details ></a>
                    </li>-->

                </ul>
            </div>
            <div style="font-size:9px; line-height:10px; margin:20px 0;">* Bioré<sup>&reg;</sup> Ultra Deep Cleansing Pore Strips remove 2x more deep pore clogs than original Bioré®  Deep Cleansing Pore Strips</div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
