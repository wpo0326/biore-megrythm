﻿<%@ Page Title="Get a Clean Face | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Get healthy-looking, radiant skin with products from Bioré® Skincare" name="description" />
    <meta content="skincare, clean face" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
	.Add("~/css/UniversPro67.css")
        .Add("~/css/homepage.css")
        .Add("~/css/flexslider.css")
        .Render("~/CSS/combinedhome_#.css")
    %>
    <meta name="viewport" content="width=device-width">
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div class="flexslider"  id="theater" >
            <ul class="slides">
 <li id="theaterItem1" class="theaterHolder">
                <div class="fma1Product">
                    <div class="wrapperItem">
                        <h2 class="newest-headline">هو ذا أحدث منتجاتنا المحتوية على الفحم قد وصلت!</h2>
                        <a href="biore-facial-cleansing-products/Charcoal.aspx"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/fmas/fma1Product.png") %>" alt="Our newest charcoal products have arrived. Prepare your pores for de-gunk-ification. Find Out More." /></a>
                        <h2 class="prepeare-headline">حضّري مسامك للتنظيف <span> الشامل من الأوساخ</span></h2>
                        <a href="biore-facial-cleansing-products/Charcoal.aspx"  class="btnWin"><span></span> إكتشفي المزيد</a>
                    </div>
                </div>
               </li>
                
                <li id="theaterItem2" class="theaterHolder">
                    <div class="NTable">
                        <div class="NRow r1">
                            <div class="NCell c1">
                                <h1>معلومات أساسية حول العناية بالمسام </h1>
                                <div class="ruleBG"></div>
                                <div id="responseRule"></div>
                            </div>
                        </div>
                        <div class="NRow r2">
                            <div class="NCell c2"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/fmas/FMA3BG.png") %>" alt="" /></div>
                        </div>
                        <div class="NRow r3">
                            <div class="NCell c3"><img class="strikeOut" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/poreCAreStrike.gif") %>" alt="" />تعرّفي على كيفية مكافحة<img class="strikeOut" src="<%= VirtualPathUtility.ToAbsolute("~/images/home/poreCAreStrike.gif") %>" alt="" /></div>
                        </div>
                        <div class="NRow r4">
                            <div class="NCell c4">المسام المسدودة والرؤوس السوداء</div>
                        </div>
                         <div class="NRow r5">
                            <div class="NCell c5">للحصول على بشرة نظيفة بمظهر صحي كل يوم</div>
                        </div>
                        <div class="NRow r6">
                            <div class="NCell c6"><a href="<%= VirtualPathUtility.ToAbsolute("~/pore-care/") %>"><div class="fma3Face">البداية</div></a></div>
                            <br /><br /><br />
                        </div>
                    </div>

                </li>

            </ul>
            <div class="centeringDiv" id="pagerWrapper"><div id="pager"></div></div>
        </div>
        
    </div>
  
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <%=SquishIt.Framework.Bundle .JavaScript()
        .Add("~/js/jquery.flexslider.min.js")
        .Render("~/js/combinedhome_#.js")
    %>
</asp:Content>