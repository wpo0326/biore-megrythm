﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Biore2012.BLL;

namespace Biore2012
{
	public partial class Default : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e) {

            // Redirect to ensure no one hits the root of en-US
            if (Request.Url.ToString().Contains("biore-skincare") == false)
            {
                Response.Clear();
                Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", VirtualPathUtility.ToAbsolute("~/biore-skincare"));
            }

            BioreUtils bu = new BioreUtils();
            Site myMaster = (Site)this.Master;
            //bu.configureProveItLinks(myMaster.isMobile, proveItTheaterLink);
            //bu.configureProveItLinks(myMaster.isMobile, promo2Link);            

            myMaster.bodyClass += " home";

			//hlAuthorPartnershipLink.NavigateUrl = System.Configuration.ConfigurationManager.AppSettings["FBAuthorPartnershipPageLink"];
        }
	}
}
