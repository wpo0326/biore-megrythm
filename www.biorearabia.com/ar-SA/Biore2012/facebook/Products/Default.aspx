﻿<%@ Page Title="" Language="C#" MasterPageFile="~/facebook/FBContentPageMaster.Master"
	AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Biore2012.facebook.Products.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div id="fb-root">
	</div>

	<script type="text/javascript">
		var extJsHost_connect_facebook_net = (("https:" == document.location.protocol) ? "https://" : "http://");
		window.fbAsyncInit = function() {
			FB.init({
				appId: '<%= System.Configuration.ConfigurationManager.AppSettings["productsFBAppId"] %>', // App ID
				//channelURL : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
				status: true, // check login status
				cookie: true, // enable cookies to allow the server to access the session
				oauth: true, // enable OAuth 2.0
				xfbml: true  // parse XFBML
			});

			// Additional initialization code here
			FB.Canvas.setAutoGrow();
			FB.Canvas.scrollTo(0, 0);
		};

		// Load the SDK Asynchronously
		(function(d) {
			var js, id = 'facebook-jssdk'; if (d.getElementById(id)) { return; }
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = extJsHost_connect_facebook_net + "connect.facebook.net/en_US/all.js";
			d.getElementsByTagName('head')[0].appendChild(js);
		} (document));
	</script>

	<div class="products">
		<div>
			<h1>Bior&eacute; skincare Face Anything&trade;</h1>
			<a href="<%=System.Configuration.ConfigurationManager.AppSettings["path"] %>biore-skincare"
				target="_blank" id="visitUs">Visit us at www.biore.com/en-US/biore-skincare to learn
				more about our products.</a> <a href="<%=System.Configuration.ConfigurationManager.AppSettings["path"] %>past-biore-favorites/"
					target="_blank" id="updatedProds"><span>Can't find your favorite?</span> Check out
					our updated lineup <span>&raquo;</span></a>
		</div>
	</div>
</asp:Content>
