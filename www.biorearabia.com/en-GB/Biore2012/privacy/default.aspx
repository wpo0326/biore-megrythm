﻿<%@ Page Title="Privacy | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.privacy._default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>

    <style type="text/css">
        a {
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent" style="min-height: 730px">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <br />
                <br />
                <h1>Privacy Policy</h1>

                <p>At Kao, we respect your privacy and take reasonable measures to protect your privacy. This policy applies only to the web site on which it appears, and does not apply to any other Kao web sites or services.  By accessing or using this web site, you agree and consent to our use of your information as described in this policy. 
                  <br /><br /><h2>EFFECTIVE DATE</h2>

                <p>This privacy policy is effective and was last updated on 25 August 2015.

                  <br /><br /><h2>WHAT INFORMATION WE COLLECT ABOUT YOU</h2>
                    <br />
                <p>
                    <strong>Personally Identifiable Information</strong><br />
                    This web site is structured so that, in general, you can visit without revealing any personally identifiable information. Once you choose to provide us with such information, you may be assured that it will only be used in accordance with the principles set forth in this document.
                <br /><br />

                <p>On some Kao web pages, you can interact with entry forms for the purposes of contacting us, subscribing to communications, registering for an account, engaging in promotional events or other purposes. The types of personally identifiable information collected at these pages may include name, mailing address and email address. We may also ask you to voluntarily provide us with information regarding your personal or professional interests, demographics, experience with our products, and contact preferences.<br /><br /> 

<p>We do not collect any personally identifiable information from you through this web site unless you provide it to us. If you email us or answer questions we have placed on this web site, you are voluntarily releasing that information to us for use as set forth in this policy. Please do not submit personally identifiable information to us if you do not want us to have this information in our database. <br /><br />

<p><strong>Performance Information</strong><br />
Kao may record your interactions with our advertisements, our web sites, emails or other applications we provide using Clickstream Data and Cookies. “Clickstream Data” is a recording of what you click on while browsing this web site. This data can tell us the type of computer and browsing software you use and the address of the web site from which you linked to this web site. <br /><br />

<p>“Cookies” are small text files that are placed on your computer by a web site for the purpose of facilitating and enhancing your communication and interaction with that web site, remembering your preferences and collecting aggregate (i.e., not personally identifiable) information. Many web sites, including ours, use cookies for these purposes. <br /><br />

<p>The web site may also include cookies set by third parties, including Google, which 1) helps us measure the performance of the web site, 2) allows us to share relevant information and advertising with you based on your and other visitors’ past visits to this web site when you surf the web, and 3) helps us measure your interactions with any Kao advertising you see on other sites.  Finally, Kao will use data from Google’s Interest-based advertising or third-party audience data, such as age, gender and interests, together with our own data, to deliver relevant advertising to you based upon your demographic profile and interests.  Specifically, this web site has implemented the following Google Analytics features to support our advertising across the web: Remarketing (to show you ads on sites across the Internet), Google Display Network Impression Reporting, the DoubleClick Campaign Manager Integration, and Google Analytics Demographics and Interest Reporting. <br /><br /> 

<p>You can opt-out of Google Analytics for Display Advertising and customize Google Display Network ads by using the Ads Settings, or by using the Google Analytics opt-out Browser add-on.  You can find more information about Google Analytics here.  <br /><br />

<p>You may stop or restrict the placement of cookies on your computer or flush them from your browser by adjusting your web browser preferences and browser plug-in settings, in which case you may still use our web site, but it may interfere with some of its functionality. <br /><br />
 
<p><strong>Cookies on our Site</strong><br />
We're giving you this information as part of our initiative to comply with recent legislation, and to make sure we're honest and clear about your privacy when using out website. We know you'd expect nothing less from us, and please be assured that we're working on a number of other privacy and cookie-related improvements to the website.<br /><br />

<p><strong>Here's a list of the main cookies we use and what we use them for.</strong> <br /><br />

                <table border="0" style="width:100%"> 
                    <tr valign="top">
                        <td  style="width:15%">
                            <p class="western" align="left">
                                <b>cookie
			and status</b>
                            <br /><br />
                        </td>
                        <td style="width:15%">
                            <p class="western">
                                <b>cookie
			name</b>
                            <br /><br />
                        </td>
                        <td style="width:15%">
                            <p class="western"><b>party</b><br /><br />
                        </td>
                        <td style="width:auto">
                            <p class="western"><b>purpose</b><br /><br />
                        </td>
                    </tr>
                    <!--<tr valign="top">
                        <td>
                            <p class="western" style="margin-bottom: 0in">addthis<br /><br />
                            <p class="western">(optional)<br /><br />
                        </td>
                        <td>
                            <p class="western" style="margin-bottom: 0in">_atuvc<br /><br />
                            <p class="western" style="margin-bottom: 0in">xtc<br /><br />
                            <p class="western" style="margin-bottom: 0in">uid<br /><br />
                            <p class="western" style="margin-bottom: 0in">uvc<br /><br />
                            <p class="western" style="margin-bottom: 0in">bt2<br /><br />
                            <p class="western" style="margin-bottom: 0in">dt<br /><br />
                            <p class="western">loc<br /><br />
                        </td>
                        <td>
                            <p class="western" align="left">1st<br /><br />
                        </td>
                        <td>
                            <p class="western" align="left" style="margin-bottom: 0in">
                                addthis
			enables the social buttons on our product pages that let you share
			your favorites on twitter, facebook, google+ or by email.<br>
                                <br>
                                we
			also use addthis to measure how our visitors are using social
			media to share content from our site.<br>
                                <br>
                                the data collected
			is<br>
                                <br>
                            <br /><br />
                            <ul>
                                <li>
                                    <p class="western" align="left" style="margin-bottom: 0in">
                                        <b>anonymous</b>&nbsp;(which
				browser you're using, the date/time of your visit or click,
				hardware/software type and which pages you’ve viewed) or&nbsp;
                                    <br /><br />
                                    <li>
                                        <p class="western" align="left" style="margin-bottom: 0in">
                                            <b>pseudonymous</b>&nbsp;(your
				ip address).
                                        <br /><br />
                            </ul>
                            <p class="western" align="left">
                                <br>
                                aggregated
			and anonymous data can be shared with 3rd parties in the form of
			market place /industry vertical performance insight and
			benchmarking.<br>
                                <br>
                                to manage addthis cookies
			see&nbsp;<a class="western" href="http://www.addthis.com/privacy" target="_blank"><u>http://www.addthis.com/privacy</u></a>&nbsp;(opens
			in a new window – please note that we’re not responsible for
			the content of external websites).&nbsp;
                            <br /><br />
                        </td>
                    </tr>-->
                    <tr valign="top">
                        <td>
                            <p class="western" style="margin-bottom: 0in">
                                google
			analytics
                            <br /><br />
                            <p class="western">(optional)<br /><br />
                        </td>
                        <td>
                            <p class="western" style="margin-bottom: 0in">_utma<br /><br />
                            <p class="western" style="margin-bottom: 0in">_utmb<br /><br />
                            <p class="western" style="margin-bottom: 0in">_utmc<br /><br />
                            <p class="western">_utmz<br /><br />
                        </td>
                        <td>
                            <p class="western" align="left">1st<br /><br />
                        </td>
                        <td>
                            <p class="western" align="left" style="margin-bottom: 0in">
                                google
			analytics helps us see how visitors get to and navigate around our
			website, allowing us to constantly improve the site.<br>
                                <br />
                                the
			data collected is<br>
                                <br/ >
                            <br /><br />
                            <ul>
                                <li>
                                    <p class="western" align="left" style="margin-bottom: 0in">
                                        <b>anonymous</b>&nbsp;(which
				adverts you might have seen, which browser you’re using, the
				date/time of your visit, demographic information and which pages
				you viewed) or&nbsp;
                                    <br /><br /></li>
                                    <li>
                                        <p class="western" align="left" style="margin-bottom: 0in">
                                            <b>pseudonymous</b>&nbsp;(your
				ip address).&nbsp;
                                        <br /><br /></li>
                            </ul>
                            <p class="western" align="left">
                                <br>
                                aggregated
			and anonymous data can be shared with 3rd parties in the form of
			market place /industry vertical performance insight and
			benchmarking.<br>
                                <br>
                                to manage google analytics cookies
			see&nbsp;<a class="western" href="https://tools.google.com/dlpage/gaoptout/" target="_blank"><u>https://tools.google.com/dlpage/gaoptout/</u></a>&nbsp;(opens
			in a new window – please note that we’re not responsible for
			the content of external websites).
                            <br /><br />
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <p class="western" style="margin-bottom: 0in">
                                new
			relic
                            <br /><br />
                            <p class="western">(optional)<br /><br />
                        </td>
                        <td>
                            <p class="western">jsessionid<br /><br />
                        </td>
                        <td>
                            <p class="western" align="left">3rd<br /><br />
                        </td>
                        <td>
                            <p class="western" align="left" style="margin-bottom: 0in">
                                new
			relic enables performance monitoring on our sites to ensure you,
			the customer, have the best experience possible when engaging with
			our brand.
                            <br /><br />
                            <p class="western" align="left" style="margin-bottom: 0in">
                                <br>
                            <br /><br />
                            <p class="western" align="left" style="margin-bottom: 0in">
                                the
			data collected is<br>
                                <br>
                            <br /><br />
                            <ul>
                                <li>
                                    <p class="western" align="left" style="margin-bottom: 0in">
                                        <b>anonymous</b>&nbsp;(which
				adverts you might have seen, which browser you’re using, the
				date/time of your visit, demographic information and which pages
				you viewed) or&nbsp;
                                    <br /><br />
                                    <li>
                                        <p class="western" align="left" style="margin-bottom: 0in">
                                            <b>pseudonymous</b>&nbsp;(your
				ip address).&nbsp;
                                        <br /><br />
                            </ul>
                            <p class="western" align="left">
                                <br>
                                aggregated
			and anonymous data can be shared with 3rd parties in the form of
			market place /industry vertical performance insight and
			benchmarking.<br>
                                <br>
                            <br /><br />
                        </td>
                    </tr>
                    <!--<tr valign="top">
                        <td>
                            <p class="western" style="margin-bottom: 0in">doubleclick<br /><br />
                            <p class="western">(optional)<br /><br />
                        </td>
                        <td>
                            <p class="western">id<br /><br />
                        </td>
                        <td>
                            <p class="western" align="left">3rd<br /><br />
                        </td>
                        <td>
                            <p class="western" align="left" style="margin-bottom: 0in">
                                this
			cookie ensures that you can see kao brand offers on carefully
			selected third party websites after you have left the brand site.
			for more information or if you want to disable this cookie without
			removing other optional cookies, then please visit this link: 
			<u><a class="western" href="https://www.google.com/settings/ads/onweb#display_optout">https://www.google.com/settings/ads/onweb#display_optout</a></u>
                            <br /><br />
                            <p class="western" align="left">
                                <br>
                            <br /><br />
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <p class="western" style="margin-bottom: 0in">bazaarvoice<br /><br />
                            <p class="western">(optional)<br /><br />
                        </td>
                        <td>
                            <p class="western" style="margin-bottom: 0in">bvid<br /><br />
                            <p class="western">bvsid<br /><br />
                        </td>
                        <td>
                            <p class="western" align="left">3rd<br /><br />
                        </td>
                        <td>
                            <p class="western" align="left">
                                these
			cookies ensure that you can see kao brand product ratings and
			reviews that have been provided by consumers, such as yourself. 
			feedback is an important part of the buying process and we want
			you to have the most relevant information about our products.
                            <br /><br />
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <p class="western" align="left">
                                facebook
			social plugin
                            <br /><br />
                        </td>
                        <td>
                            <p class="western">
                                <br>
                            <br /><br />
                        </td>
                        <td>
                            <p class="western" align="left">3<sup>rd</sup><br /><br />
                        </td>
                        <td>
                            <p class="western" align="left" style="margin-bottom: 0in">
                                this
			facebook cookie lets you see what your friends have liked,
			commented on or shared.<br>
                                <br>
                                all the data collected is:&nbsp;
                            <br /><br />
                            <ul>
                                <li>
                                    <p class="western" align="left" style="margin-bottom: 0in">
                                        <b>anonymous</b>&nbsp;(which
				browser you're using, demographic information, how you've
				interacted with the site and which pages you've viewed) or
                                    <br /><br />
                                    <li>
                                        <p class="western" align="left" style="margin-bottom: 0in">
                                            <b>pseudonymous</b>&nbsp;(your
				ip address)&nbsp;
                                        <br /><br />
                            </ul>
                            <p class="western" align="left">
                                <br>
                                the
			data shared is dependent on your facebook privacy settings.<br>
                                <br>
                                to
			manage facebook cookies
			see:<br>
                                <a class="western" href="https://www.facebook.com/help/?page=176591669064814" target="_blank"><u>https://www.facebook.com/help/?page=176591669064814</u></a>&nbsp;
                            <br /><br />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <p class="western">
                                facebook
			connect
                            <br /><br />
                        </td>
                        <td valign="top">
                            <p class="western">
                                <br>
                            <br /><br />
                        </td>
                        <td valign="top">
                            <p class="western" align="left">3<sup>rd</sup><br /><br />
                        </td>
                        <td valign="top">
                            <p class="western" align="left" style="margin-bottom: 0in">
                                to
			make signing up and logging in easier, you can join us via your
			facebook account using facebook connect. this cookie remembers who
			you are so next time you come back you won't need to enter all
			your details. handy eh?<br>
                                <br>
                                all the data collected is:&nbsp;
                            <br /><br />
                            <ul>
                                <li>
                                    <p class="western" align="left" style="margin-bottom: 0in">
                                        <b>anonymous</b>&nbsp;(which
				browser you're using, demographic information, how you've
				interacted with the site and which pages you've viewed) or
                                    <br /><br />
                                    <li>
                                        <p class="western" align="left" style="margin-bottom: 0in">
                                            <b>pseudonymous</b>&nbsp;(your
				ip address)&nbsp;
                                        <br /><br />
                            </ul>
                            <p class="western" align="left">
                                <br>
                                the
			data shared is dependent on your facebook privacy settings.<br>
                                <br>
                                to
			manage facebook cookies
			see:<br>
                                <a class="western" href="https://www.facebook.com/help/?page=176591669064814" target="_blank"><u>https://www.facebook.com/help/?page=176591669064814</u></a>
                            <br /><br />
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <p class="western" style="margin-bottom: 0in">cookieacceptance<br /><br />
                            <p class="western">(necessary)<br /><br />
                        </td>
                        <td>
                            <p class="western">cookieacceptance<br /><br />
                        </td>
                        <td>
                            <p class="western" align="left">1st<br /><br />
                        </td>
                        <td>
                            <p class="western" align="left">
                                this
			cookie records if you have accepted the use of cookies on this
			site.  it does not contain any further information.  this cookie
			remains on your computer after you have left the site. 
                            <br /><br />
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <p class="western">
                                flash
			cookies
                            <br /><br />
                        </td>
                        <td>
                            <p class="western">
                                flash
			cookies
                            <br /><br />
                        </td>
                        <td>
                            <p class="western" align="left">3<sup>rd</sup><br /><br />
                        </td>
                        <td>
                            <p class="western" align="left">
                                this
			site uses flash to deliver video content to you.  to improve
			experience, flash cookies may be delivered to you for saving your
			preferences.  flash cookies are managed a bit differently than
			browser cookies. for information on how to manage these, visit
			<u><a class="western" href="http://www.adobe.com/products/flashplayer/security">www.adobe.com/products/flashplayer/security</a></u>
                                for details.  restricting the use of flash cookies may affect your
			experience with our brand.  
                            <br /><br />
                        </td>
                    </tr>-->
                </table>


 <p><strong>Kao “share” tools</strong><br />
If you take the opportunity to 'share' Kao content with friends through social networks – such as Facebook and Twitter - you may be sent cookies from these websites. We don't control the setting of these cookies, so we suggest you check the third-party websites for more information about their cookies and how to manage them.<br /><br />

<p><strong>Managing Cookies</strong><br />
If cookies aren't enabled on your computer, it will mean that some of the site may not function appropriately providing a poor experience. You will, however, still be able to view some content on our website if it's just a bit of research you're after.  If you are interested in managing cookies on your computer, then reference our <a href="/cookies">how to enable cookies guide here</a>.<br /><br />

<p><strong>Further information about cookies</strong><br />
<p>If you'd like to learn more about cookies in general and how to manage them, visit www.aboutcookies.org (opens in a new window – please note that we’re not responsible for the content of external websites). <br /><br /> 

<p><strong>Information from children</strong><br />
Kao has no intention of collecting personally identifiable information from children under the age of 13. Kao will maintain procedures to assure that personally identifiable information about children under the age of 13 is only collected with explicit consent of such child’s parent or guardian. If a child under the age of 13 has provided Kao with personally identifiable information without such consent, Kao asks that a parent or guardian of the child contact Kao using our <a href="../contact-us">Contact Us</a> page to inform us of that fact. Kao will use reasonable efforts to delete such information from our database. 

                  <br /><br /><h2>HOW WE USE YOUR INFORMATION</h2>
                Kao uses your information to understand your needs and provide better products and services. Kao may use data collected from you to personalise your web site experience, tailor future communications, and send you targeted offers as described below. Occasionally, Kao may use your personally identifiable information to contact you for market research or to provide you with marketing information we think would be of particular interest. We will always give you the opportunity to opt out of receiving such contacts.

Kao may share the personally identifiable information you provide with other Kao divisions or affiliates, provided that, such other divisions or affiliates have privacy practices that are similar to those set forth in this policy. 

Kao may permit its vendors and subcontractors to access your personally identifiable information, but they are only permitted to do so in connection with services they are performing for Kao. They are not authorized by Kao to use your personally identifiable information for their own benefit. Kao may disclose personally identifiable information as required by law or legal process. 

Kao may disclose personally identifiable information to investigate suspected fraud, harassment or other violations of any law, rule or regulation, or the terms or policies for the web site. 

In the event of a sale, merger, liquidation, dissolution, reorganization or acquisition of Kao, or a Kao business unit, information Kao has collected about you may be sold or otherwise transferred. However, this will only happen if the party acquiring the information agrees to use personally identifiable information in a manner which is substantially similar to the uses described in this policy. 

Sweepstakes, contests and other promotions may set forth additional uses of your personally identifiable information in connection with such promotions.

                  <br /><br /><h2>THIRD PARTY COLLECTION AND SHARING</h2>
                At times, personally identifiable information may be collected from you on behalf of Kao and a third party who is identified at the time of collection. This may include co-branded promotions.  In such instances, your personally identifiable information may be provided to both Kao and such third party. While Kao will use your personally identifiable information as set forth in this policy, such third party will use your personally identifiable information as set forth in their own privacy policy.  Such third parties may collect information regarding your activities over time and across different web sites.  Therefore, you should review such policies prior to providing your personally identifiable information. Kao is not responsible for the actions of such third parties. 


                  <br /><br /><h2>TARGETED CONTENT AND MESSAGING</h2>
                We believe that content, messages and advertising are more relevant and valuable to you when they are based upon your interests, needs and demographics. Therefore, we may deliver content, messages and advertising specifically to you that are based upon your prior activities on our web sites and information provided to us or gathered as described in this policy. For example, if you have previously expressed an interest in hair care products through your activities on our web site, we may deliver more information to you about hair care products than other products for which you have not expressed an interest or interacted with on the web site. While we may use this information to tailor what we deliver to you, we will still handle and secure your personally identifiable information as set forth in this policy.

Through the use of the Google services (described above), the cookies we place on this web site, and cookies placed by third parties on this web site or through other sites you visit on the internet, we may cause advertisements and content to appear on this web site and elsewhere on the internet based upon your activities over time and across different web sites.  In most instances, Kao is simply a content provider and does not directly possess such behavioral information about your online activities.  You are able to limit such targeted advertising by setting your browser to block third party cookies or by visiting www.aboutads.info/consumers to learn more about such advertising practices and to exercise options with respect to such practices at www.aboutads.info/choices.

We do not respond to or honor “do not track” (a/k/a DNT) signals or similar mechanisms transmitted by web browsers.

                  <br /><br /><h2>YOUR CHOICE</h2>
                Kao will not use or share the information provided to us online in ways unrelated to the ones described in this policy without letting you know and offering you a choice. 
LINKS TO OTHER SITES
This web site may contain links to other web sites and services.  This can include clicking “Buy Now” or a similar feature to take you to the web sites of various third party retailers where you can browse or purchase Kao products.  We do not control such third party web sites and services and are not responsible for their actions or privacy practices.  The existence of a link on our web site does not mean that we endorse or have vetted such third party web site or service.  You are encouraged to carefully review the policies and practices of every web site and service that you visit. 
                  <br /><br /><h2>SECURITY</h2>
                Kao is committed to taking reasonable steps to ensure the security of the information we collect. To prevent unauthorized access, maintain data accuracy, and ensure the appropriate use of personally identifiable information, we have put in place appropriate physical, electronic, and managerial procedures to safeguard and secure the personally identifiable information we collect online. If we provide you a direct means for maintaining your information in our database, such as through a “profile center” application, you are responsible for taking reasonable precautions to protect your login data, such ID and password. Kao will not be responsible for breach that results from a lost or stolen login.
                 
                  <br /><br /><h2>NON-CONFIDENTIAL</h2>
                Any communication or material you transmit to us by email or otherwise, including any data, questions, comments, suggestions, or the like is, and will be treated as, non-confidential and nonproprietary. Except to the extent expressly covered by this policy, anything you transmit or post may be used by us for any purpose, including but not limited to, reproduction, disclosure, transmission, publication, broadcast and posting. You expressly agree that we are free to use any ideas, concepts, know-how, or techniques contained in any communication you send to us without compensation and for any purpose whatsoever, including but not limited to, developing, manufacturing and marketing products and services using such information. Furthermore, you confirm that any media files you submit to us (eg. pictures, videos, etc) are your property and you have permission of anyone depicted in these files to transfer rights to Kao.
                 
                  <br /><br /><h2>ACCESS TO AND CORRECTION OF INFORMATION / OPT-OUT</h2>
                Kao will maintain reasonable procedures for individuals to gain access to their personally identifiable information and preferences. Kao will correct any information that is inaccurate or incomplete or allow you to change your individual consent level. You may do so by contacting us with the relevant details for your request. 

If you wish for us to stop using your personally identifiable information and delete it from our list of active users, please <a href="../contact-us">contact us</a> with the details necessary to complete your request. We will process and honor such requests within a reasonable period of time after receipt. However, even though we may remove your personally identifiable information from our list of active users, we are not responsible for removing your personally identifiable information from the lists of any third party who has been provided your information in accordance with this policy, such as a business partner. 

As described above, you are able to opt-out of certain targeted advertising by setting your browser to block third party cookies or by visiting www.aboutads.info/choices.

                  <br /><br /><h2>DATA RETENTION</h2>
                Except as described in the section above, we may retain and continue to use information collected under this privacy policy for as long as we have a business need for such information. 

                  <br /><br /><h2>COMMENT AND QUESTIONS</h2>
                If you have comments or questions about our privacy policy or your personally identifiable information, please use our <a href="../contact-us">Contact Us</a> page. 

                  <br /><br /><h2>UNITED STATES OF AMERICA</h2>
                This web site and our related databases are maintained in the United States of America. By using the web site, you freely and specifically give us your consent to collect and store, your information in the United States and to use your information as specified within this policy. 

                  <br /><br /><h2>CHANGES TO PRIVACY POLICY</h2>
                Kao will, and reserves the right to, modify and update this policy or its related business practices at any time at our discretion. Any such material changes will be posted here for reference. However, Kao will not make material changes to this policy, such as how it discloses personally identifiable information, without giving you a chance to opt-out of such differing uses. 
            </div>
        </div>
    </div>
</asp:Content>
