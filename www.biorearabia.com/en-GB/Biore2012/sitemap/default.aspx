﻿<%@ Page Title="Sitemap | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.sitemap._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1>Sitemap</h1>
                 <div id="linkList">
                     <div id="prodLinks">
                         <ul class="col" id="col1">
                            <li><a href="../biore-facial-cleansing-products">Our products <span class="arrow">&rsaquo;</span></a></li>
                            <li>
                                <h2 class="pie deepCleansing"><span class="subNavHeadlines"><span class="greenBold">dirty</span> & oily skin no more </span></h2>
                                <ul>
                                    <li><a href="../charcoal-for-normal-or-oily-skin/deep-pore-charcoal-cleanser">Deep Pore Charcoal Cleanser</a></li>
                                    <li><a href="../charcoal-for-normal-or-oily-skin/self-heating-one-minute-mask">Self-Heating One Minute Mask</a></li>
                                    <li><a href="../charcoal-for-normal-or-oily-skin/pore-unclogging-scrub">Pore Unclogging Scrub</a></li>
                                    <li><a href="../charcoal-for-normal-or-oily-skin/make-up-removing-towelettes">Make-up Removing Wipes</a></li>
                                    <li><a href="../charcoal-for-normal-or-oily-skin/daily-cleansing-cloths">Daily Deep Pore Cleansing Wipes</a></li>
                                    <li><a href="../charcoal-for-normal-or-oily-skin/pore-detoxifying-foam-cleanser">Pore Detoxifying Foam Cleanser</a></li>
                                    <li><a href="../charcoal-for-normal-or-oily-skin/nourish-moisture-lotion">Nourish Moisture Lotion</a></li>
                                    <li><a href="../charcoal-for-normal-or-oily-skin/pore-penetrating-charcoal-bar">Pore Penetrating Charcoal Bar</a></li>
                                </ul>
                            <h2 class="pie smallerPores"><span class="subNavHeadlines">hello visibily <span class="tealBold">smaller pores</span> </span></h2>
                                <ul>
                                    <li><a href="../hello-visibily-smaller-pores/charcoal-pore-minimizer">Charcoal Pore Minimiser</a></li>
                                    </ul>
                                    </li>
                        </ul>
                        <ul class="col" id="col2">
                            <li>
                                <h2 class="pie complexionClearing"><span class="subNavHeadlines"><span class="orangeBold">blemishes</span> are out of here</span></h2>
                                <ul>
                                    <li><a href="../blemishes-are-out-of-here/blemish-fighting-astringent-toner">Triple Action Toner</a></li>
                                    <li><a href="../blemishes-are-out-of-here/blemish-fighting-ice-cleanser">Blemish Fighting Ice Cleanser</a></li>
                                    <li><a href="../blemishes-are-out-of-here/acne-clearing-scrub">Blemish Clearing Scrub</a></li>
                                </ul>
                                <h2 class="pie murt"><span class="subNavHeadlines">Break up With <span class="redBold">Blackheads</span></span></h2>
                                <ul>
                                    <li><a href="../pore-strips/warming-anti-blackhead-cleanser">Warming Anti-Blackhead Cleanser</a></li>
                                    <li><a href="../pore-strips/pore-strips#regular">Deep Cleansing Pore Strips</a></li>
                                    <li><a href="../pore-strips/pore-strips#ultra">Ultra Deep Cleansing Pore Strips</a></li>
                                    <li><a href="../pore-strips/pore-strips#combo">Combo Deep Cleansing Pore Strips</a></li>
                                    <li><a href="../pore-strips/pore-strips#ultracombo">Combo Deep Cleansing Pore Strips with Ultra Nose Strips</a></li>
                                    <li><a href="../pore-strips/charcoal-pore-strips">Deep Cleansing Charcoal Pore Strips</a></li>
                                </ul>
                                <ul>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <ul class="col noPadding" id="col3">
                        <li>
                            <a href="../biore-facial-cleansing-products/charcoal.aspx">What's New <span class="arrow">&rsaquo;</span></a>
                  
                        </li>
                        <!--<li><a href="../email-newsletter-sign-up">Sign Me Up <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../where-to-buy-biore">Where To Buy <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../biore-videos">Watch It Work <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../about-us">About Bior&eacute;<sup>&reg;</sup> Skincare <span class="arrow">&rsaquo;</span></a></li>-->
                        <li><a href="http://www.kaobrands.com/" target="_blank">Kao Brands Company <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../contact-us">Contact Us <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../legal">Legal <span class="arrow">&rsaquo;</span></a></li>
                        <li><a href="../privacy_policy">Privacy Policy <span class="arrow">&rsaquo;</span></a></li>
                     </ul>
                 </div>
                 <div id="photo"></div>
            </div>
        </div>
    </div>
</asp:Content>