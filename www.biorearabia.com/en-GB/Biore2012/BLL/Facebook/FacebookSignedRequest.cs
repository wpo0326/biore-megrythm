﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Facebook
{
	public class FacebookSignedRequest
	{
		public string algorithm { get; set; }
		public string expires { get; set; }
		public int issued_at { get; set; }
		public string oauth_token { get; set; }
		public string user_id { get; set; }
		public string app_data { get; set; }
		public int profile_id { get; set; } // only for OLD FBML tabs
		public srPage page { get; set; }
		public srUser user { get; set; }
	}

	public class srUser
	{
		public string country { get; set; }
		public string locale { get; set; }
		public ageRange age { get; set; }
	}

	public class srPage
	{
		public string id { get; set; }
		public bool liked { get; set; }
		public bool admin { get; set; }
	}

	public class ageRange
	{
		public int min { get; set; }
		public int max { get; set; }
	}
}
