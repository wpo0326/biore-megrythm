﻿<%@ Page Title="Free Your Pores – Get Clean, Healthy Skin – Browse All Products  | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.our_products.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Bior&eacute;&reg; Skincare products cleanse and gently exfoliate, giving you healthy, clean skin! See the complete Bior&eacute;&reg; Skincare product line." name="description" />
    <meta content="Clear Skin, Biore&reg; Products" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>
    <meta name="viewport" content="width=device-width">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="intro">
                <h1>Our Products</h1>
                <div id="responseRule"></div>
                <p>Biore Skincare targets the cause of all skin problems - the clogged pore.  The powerful, pore-cleansing Biore products come in liquid cleanser, bar soap, masks and strip form to help you remove pore-clogging debris!  Go from school to the library, home to beach, and from work to working out, with skin that's unclogged and healthy-looking.  Because when you clean the pore, you clear the problem.</p>
            </div>

            <!--<div id="complexionClearingProds" class="prodList">
                <h2 class="pie roundedCorners complexionClearing">blemishes <span>are out of here</span>
                    <span></span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishTreatingAstringentToner.png" alt="" />
                        <asp:Literal ID="litBlemishAstringent" runat="server" />
                        <h3>BLEMISH FIGHTING ASTRINGENT</h3>
                        <p>Help prevent breakouts with an oil-free toner.</p>
                        <a href="../blemishes-are-out-of-here/blemish-fighting-astringent-toner" id="details-blemish-fighting-astringent">details ></a>
                    </li>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/blemishFightingIceCleanser.png" alt="" />
                        <asp:Literal ID="litBlemishIceCleanser" runat="server" />
                        <h3>BLEMISH FIGHTING<br />ICE CLEANSER</h3>
                        <p>With Salicylic acid, this formula cleans pores of dirt, oil, and makeup.</p>
                        <a href="../blemishes-are-out-of-here/blemish-fighting-ice-cleanser" id="details-blemish-fighting-ice-cleanser">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/acneClearingScrub.png" alt="" />
                        <asp:Literal ID="litAcneClearingScrub" runat="server" />
                        <h3>BLEMISH CLEARING<br />SCRUB</h3>
                        <p>Defeat deep-down dirt and oil in 2 days.</p>
                        <a href="../blemishes-are-out-of-here/acne-clearing-scrub" id="details-acne-clearing-scrub">details ></a>
                    </li>
                  
                </ul>
            </div>-->

            <div id="deepCleansingProds" class="prodList">
                <h2 class="pie roundedCorners deepCleansing"><span>dirty</span> & oily skin no more
                    <span class="arrow"></span>
                </h2>
            	<!--<div class="intro">
                <p>Don't let dirt take over your pores.  Biore deep cleansing products target deep-down impurities to defend against daily buildup - like dirt, oil and make-up - for a truly deep clean.</p>
                </div>-->
                <ul>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CHARCOAL_100X235.png" alt="" />
                         <!--<asp:Literal ID="litDeepPoreCharcoalCleanser" runat="server" />-->
                         <br />
                        <h3>Deep Pore<br />Charcoal Cleanser</h3>
                        <p>Deep cleans 2x better* & naturally purifies.</p>
                        <a href="../dirty-and-oily-skin-no-more/deep-pore-charcoal-cleanser" id="A1">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/SHM_100X235.png" alt="" />
                        <!--<asp:Literal ID="litSelfHeatingOneMinuteMask" runat="server" />-->
                        <br>
                        <h3>Self Heating<br />One Minute Mask</h3>
                        <p>Purifies pores 2.5x better*.</p>
                        <a href="../dirty-and-oily-skin-no-more/self-heating-one-minute-mask" id="A2">details ></a>
                    </li>
                     <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/poreUncloggingScrub.png" alt="" />
                        <asp:Literal ID="litPoreUncloggingScrub" runat="server" />
                        <h3>PORE UNCLOGGING <br />SCRUB</h3>
                        <p>Deep cleans pores & smoothes skin.</p>
                        <a href="../dirty-and-oily-skin-no-more/pore-unclogging-scrub" id="A3">details ></a>
                    </li>      
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/makeupRemovingTowelettes.png" alt="" />
                        <asp:Literal ID="litMakeUpTowelettes" runat="server" />
                        <h3>MAKE-UP <br />REMOVING WIPES</h3>
                        <p>Thoroughly cleans & removes make-up.</p>
                        <a href="../dirty-and-oily-skin-no-more/make-up-removing-towelettes" id="details-make-up-removing-towelettes">details ></a>
                    </li>
                    
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/dailyCleansingCloths.png" alt="" />
                        <asp:Literal ID="litDailyCleansingCloths" runat="server" />
                        <h3>DAILY DEEP PORE CLEANSING WIPES</h3>
                        <p>Deep cleans pores & exfoliates skin in one wipe.</p>
                        <a href="../dirty-and-oily-skin-no-more/daily-cleansing-cloths" id="details-daily-cleansing-cloths">details ></a>
                    </li>           
                                       
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/4In1DetoxifyingCleanser.png" alt="" />
                        <asp:Literal ID="litPoreFoamCleanser" runat="server" />
                        <h3>PORE DETOXIFYING <br />FOAM CLEANSER</h3>
                        <p>Cleanses, tones, stimulates & detoxifies.</p>
                        <a href="../dirty-and-oily-skin-no-more/pore-detoxifying-foam-cleanser" id="details-pore-detoxifying-foam-cleanser">details ></a>
                    </li>  
                        <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/nourishMoistureLotion.png" alt="" />
                         <asp:Literal ID="litNourishMoistureLotion" runat="server" />
                        <h3>NOURISH MOISTURE LOTION</h3>
                        <p>Hydrates pores for renewed, smooth skin.</p>
                        <a href="../dirty-and-oily-skin-no-more/nourish-moisture-lotion" id="details-nourish-moisture-lotion">details ></a>
                    </li> -->
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalBar.png" alt="" />
                         <!--<asp:Literal ID="litCharcoalBar" runat="server" />-->
                         <br>
                        <h3>Pore Penetrating <br />Charcoal Bar</h3>
                        <p>Deep cleans & gently exfoliates for 2.5x cleaner pores*.</p>
                        <a href="../dirty-and-oily-skin-no-more/pore-penetrating-charcoal-bar" id="A1">details ></a>
                    </li> 
                </ul>
            </div>



            <div id="murtProds" class="prodList">
                <h2 class="pie roundedCorners murt">break up with <span>blackheads</span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/warmingAntiBlackheadCleanser.png" alt="" />
                        <asp:Literal ID="litBlackheadCleanser" runat="server" />
                        <h3>Warming Anti-<br />Blackhead Cleanser</h3>
                        <p>Heats up to open pores & target blackheads. </p>
                        <a href="../breakup-with-blackheads/warming-anti-blackhead-cleanser" id="details-deep-cleansing-blackhead-cleanser">details ></a>
                    </li>-->
                    
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStrips.png" alt="" />
                        <!--<asp:Literal ID="litPoreStrips" runat="server" />-->
                        <br>
                        <h3>DEEP CLEANSING<br />PORE STRIPS</h3>
                        <p>Unclogs pores & achieves the deepest clean. </p>
                        <a href="../breakup-with-blackheads/pore-strips#regular" id="details-deep-cleansing-pore-strips">details ></a>
                    </li>              
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.png" alt="" />
                        <!--<asp:Literal ID="litPoreStripsCombo" runat="server" />-->
                        <br>
                        <h3>Combo Deep Cleansing<br /> Pore Strips</h3>
                        <p>4 nose strips & 4 face strips</p>
                        <a href="../breakup-with-blackheads/pore-strips#combo" id="details-deep-cleansing-pore-strips-combo">details ></a>
                    </li>
                    
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltra.png" alt="" />
                        <!--<asp:Literal ID="litPoreStripsUltra" runat="server" />-->
                        <br>
                        <h3>Ultra Deep Cleansing Pore Strips</h3>
                        <p>With Witch Hazel & <br />Tea Tree Oil.</p>
                        <a href="../breakup-with-blackheads/pore-strips#ultra" id="details-deep-cleansing-pore-strips-ultra">details ></a>
                    </li>   
                    <!--<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltraCombo.png" alt="" />
                        <asp:Literal ID="litPoreStripsFace" runat="server" />
                        <h3 class="combo">COMBO DEEP CLEANSING PORE STRIPS WITH ULTRA NOSE STRIPS</h3>
                        <p>Removes 2x more deep-down clogged pores.*</p>
                        <a href="../breakup-with-blackheads/pore-strips#ultracombo" id="details-deep-cleansing-pore-ultra-combo">details ></a>
                    </li>-->
                  
<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" />
                        <!--<asp:Literal ID="litCharcoalStrips" runat="server" />-->
                        <br>
                        <h3>Deep Cleansing Charcoal Pore Strips</h3>
                        <p>Unclogs pores & draws out excess oil for the deepest clean.</p>
                        <a href="../breakup-with-blackheads/charcoal-pore-strips" id="details-deep-cleansing-pore-strips-face">details ></a>
                    </li>
                </ul>
            </div>
            
             <!--<div id="bigPoresProds" class="prodList">
                <h2 class="pie roundedCorners bigPores">hello visibly <span>smaller pores</span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreMinimizer.png" alt="" />
                        <asp:Literal ID="litCharcoalPoreMinimizer" runat="server"></asp:Literal>
                        <h3>Charcoal<br />
                      Pore Minimizer</h3>
                        <p>Gently exfoliates & deep cleans to instantly reduce the appearance of pores.	</p>
                        <a href="../hello-visibily-smaller-pores/charcoal-pore-minimizer" id="details-charcoal-pore-minimizer">details ></a>
                    </li>
                    
                </ul>
            </div>-->
            
            <div style="font-size:9px; line-height:10px; margin:20px 0;">* Bioré<sup>&reg;</sup> Ultra Deep Cleansing Pore Strips remove 2x more deep pore clogs than original Bioré®  Deep Cleansing Pore Strips</div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
