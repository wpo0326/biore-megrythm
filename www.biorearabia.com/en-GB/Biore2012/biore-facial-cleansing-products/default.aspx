﻿<%@ Page Title="Free Your Pores – Get Clean, Healthy Skin – Browse All Products  | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.our_products.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Bior&eacute;&reg; Skincare products cleanse and gently exfoliate, giving you healthy, clean skin! See the complete Bior&eacute;&reg; Skincare product line." name="description" />
    <meta content="Clear Skin, Biore&reg; Products" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/ourProducts.css")
        .Render("~/css/combinedour_#.css")
    %>
    <meta name="viewport" content="width=device-width">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
            <div class="intro">
                <h1>Our Products</h1>
                <div id="responseRule"></div>
                <p>Target the root of skin problems — by stripping weekly and cleansing daily. Our powerful, pore-cleansing products come in liquid, foam, scrub, and strip forms to keep your skin clean and healthy-looking.</p>
            </div>

           
            <div id="deepCleansingProds" class="prodList">
                <h2 class="pie roundedCorners deepCleansing"><span>Charcoal</span> for oily/normal skin
                    <span class="arrow"></span>
                </h2>
            	<!--<div class="intro">
                <p>Don't let dirt take over your pores.  Biore deep cleansing products target deep-down impurities to defend against daily buildup - like dirt, oil and make-up - for a truly deep clean.</p>
                </div>-->
                <ul>
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/CHARCOAL_100X235.png" alt="" />
                         
                         <br />
                        <h3>Deep Pore<br />Charcoal Cleanser</h3>
                        <p>Deep cleans 2x better* & naturally purifies.</p>
                        <a href="../charcoal-for-normal-or-oily-skin/deep-pore-charcoal-cleanser" id="A1">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/SHM_100X235.png" alt="" />
                       
                        <br />
                        <h3>Self Heating<br />One Minute Mask</h3>
                        <p>Purifies pores 2.5x better*.</p>
                        <a href="../charcoal-for-normal-or-oily-skin/self-heating-one-minute-mask" id="A2">details ></a>
                    </li>
                     
                     <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalBar.png" alt="" />
                         <!--<asp:Literal ID="litCharcoalBar" runat="server" />-->
                         <br />
                        <h3>Pore Penetrating <br />Charcoal Bar</h3>
                        <p>Deep cleans & gently exfoliates for 2.5x cleaner pores*.<br />Great for face and body</p>
                        <a href="../charcoal-for-normal-or-oily-skin/pore-penetrating-charcoal-bar" id="A1">details ></a>
                    </li> 
                </ul>
            </div>

            <div id="bigPoresProds" class="prodList">
                <h2 class="pie roundedCorners bigPores"><span>Baking Soda</span> for Combination Skin
                    <span class="arrow"></span>
                </h2>
                <ul>
                	<li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/Baking_Soda_Cleanser_small.png" alt="" />
                        
                        <h3>Baking Soda<br />Pore Cleanser</h3>
                        <p>Deep cleans & exfoliates dry skin.</p>
                        <a href="../baking-soda-for-combination-skin/baking-soda-pore-cleanser" id="details-baking-soda-pore-cleanser">details ></a>
                    </li>
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/baking-soda-cleansing-scrub-small.png" alt="" />
                        
                        <h3>Baking Soda<br />Cleansing Scrub</h3>
                        <p>Deep clean without over-scrubbing.</p>
                        <a href="../baking-soda-for-combination-skin/baking-soda-cleansing-scrub" id="A3">details ></a>
                    </li>
                    
                </ul>
            </div>

            <div id="murtProds" class="prodList">
                <h2 class="pie roundedCorners murt"><span>Pore Strips</span>
                    <span class="arrow"></span>
                </h2>
                <ul>
                  
                    
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStrips.png" alt="" />
                        <!--<asp:Literal ID="litPoreStrips" runat="server" />-->
                        <br />
                        <h3>DEEP CLEANSING<br />PORE STRIPS</h3>
                        <p>Unclogs pores & achieves the deepest clean. </p>
                        <a href="../pore-strips/pore-strips#regular" id="details-deep-cleansing-pore-strips">details ></a>
                    </li>   
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsUltra.png" alt="" />
                        <!--<asp:Literal ID="litPoreStripsUltra" runat="server" />-->
                        <br />
                        <h3>Ultra Deep Cleansing Pore Strips</h3>
                        <p>With Witch Hazel & <br />Tea Tree Oil.</p>
                        <a href="../pore-strips/pore-strips#ultra" id="details-deep-cleansing-pore-strips-ultra">details ></a>
                    </li>              
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/deepCleansingPoreStripsCombo.png" alt="" />
                        <!--<asp:Literal ID="litPoreStripsCombo" runat="server" />-->
                        <br />
                        <h3>Combo Deep Cleansing<br /> Pore Strips</h3>
                        <p>4 nose strips & 4 face strips</p>
                        <a href="../pore-strips/pore-strips#combo" id="details-deep-cleansing-pore-strips-combo">details ></a>
                    </li>
                    
                  
                    <li class="productListSpacer">
                        <img src="../images/ourProducts/products/small/charcoalPoreStrips.png" alt="" />
                        <!--<asp:Literal ID="litCharcoalStrips" runat="server" />-->
                        <br />
                        <h3>Deep Cleansing Charcoal Pore Strips</h3>
                        <p>Unclogs pores & draws out excess oil for the deepest clean.</p>
                        <a href="../pore-strips/charcoal-pore-strips" id="details-deep-cleansing-pore-strips-face">details ></a>
                    </li>
                </ul>
            </div>
            
             
            
            <div style="font-size:9px; line-height:10px; margin:20px 0;">* Bioré<sup>&reg;</sup> Ultra Deep Cleansing Pore Strips remove 2x more deep pore clogs than original Bioré®  Deep Cleansing Pore Strips</div>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
