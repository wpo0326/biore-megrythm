

var over = false;
var N = document.all ? 0 : 1;
var X,Y;

var popupContainer = null;
var myContainer = null;
var animate = true;

if (N) {
    document.captureEvents(Event.MOUSEDOWN, Event.MOUSEMOVE, Event.MOUSEUP);
}

function QP_stopAnimation() {
    animate = false;
}

function QP_mouseDown(e) {
    if (over) {
	animate = false;
	popupContainer = document.getElementById("QuestionProPopupSurvey_" + config.surveyID);
	if (popupContainer) {
	    if (N) {
		X = e.layerX;
		Y = e.layerY;
		return false;
	    } else {
		popupContainer = popupContainer.style;
		X = event.offsetX;
		Y = event.offsetY;
	    }
	}
    }
}


function QP_mouseMove(e) {
    if (popupContainer) {
	if (N) {
	    popupContainer.style.left = (e.pageX - X);
	    popupContainer.style.top = (e.pageY - Y);
	} else {
	    popupContainer.pixelLeft = (event.clientX - X) + document.body.scrollLeft;
	    popupContainer.pixelTop = (event.clientY - Y) + document.body.scrollTop;
	    return false;
	}
    }
}

function QP_mouseUp() {
    popupContainer = null;
}

function QP_mouseOver() {
    over = true;
}

function QP_mouseOut() {
    over = false;
}


function QP_setCookie(name, value) {
  var expFromNow = 60*24*60*60*1000;  // Expires in 60 Days
  var exp = new Date(); 
  exp.setTime(exp.getTime() + expFromNow);
  document.cookie = (name + "=" + escape(value) + ";expires=" + exp.toGMTString() + ";path=/;");
}


function QP_getCookie(Name)  {
    var search = Name + "=";
    if (document.cookie.length > 0)  { 
	offset = document.cookie.indexOf(search);
	if (offset != -1) { 
	    offset += search.length;
	    end = document.cookie.indexOf(";", offset);
	    end = (end == -1 ? 
		   document.cookie.length :
		   offset +1);
	    return unescape(document.cookie.substring(offset, end));
	}    
    }
}

function showContainer() {

  if (myContainer) {
      if (config.animationMode == 0) {
        myContainer.style.display = 'block';
      } else if(config.animationMode == 1) {
        animateTop();
      } else {
        animateLeft();
      }
  }
}

function animateTop() {
  var target = parseInt(myContainer.style.top);
  myContainer.style.top = 0;
  myContainer.style.display = 'block';  
  setTimeout("moveDivVertical(" + target + ")", 100);
}


function animateLeft() {
  var target = parseInt(myContainer.style.left);
  myContainer.style.left = 0;
  myContainer.style.display = 'block';
  setTimeout("moveDivHorizontal(" + target + ")", 100);
}


function moveDivVertical(targetLocation) {
  var currentLocation = parseInt(myContainer.style.top);
  if (animate && (currentLocation < targetLocation)) {
    myContainer.style.top = (currentLocation + 5) + "px";
    setTimeout("moveDivVertical(" + targetLocation + ")", 100);
  }  
}

function moveDivHorizontal(targetLocation) {
  var currentLocation = parseInt(myContainer.style.left);
  
  if (animate && (currentLocation < targetLocation)) {
    myContainer.style.left = (currentLocation + 5) + "px";
    setTimeout("moveDivHorizontal(" + targetLocation + ")", 100);
  }  
}


function closeQPDiv() {
  if (myContainer) {
    myContainer.style.display = 'none';
  }
  myContainer = null;  
}


function openPreview(loc) {
  var winl = (screen.width-800)/2;
  var wint = (screen.height-640)/2;
  var myRemote = launch(loc,"FreeSurveys1","height=640,width=800,channelmode=0,dependent=0,directories=0,fullscreen=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,toolbar=0,top="+wint + ",left=" + winl,"Help");
  myRemote.focus();  
}

function launch(newURL, newName, newFeatures, orgName) {
    remote = open(newURL, newName, newFeatures);
    if (remote.opener == null)
	remote.opener = window;
    remote.opener.name = orgName;
    return remote;
}

function startSurvey() {
  closeQPDiv();
  openPreview(config.takeSurveyURL + '?id=' + config.surveyID + config.sCustom);
}


function QP_setCookieClose(value, days) {
  QP_setCookie("Survey_" + config.surveyID, value, days);
  closeQPDiv();
}

var skipCount = (config.skipCount && config.skipCount > 0 ?
		 (Math.random() * 100) % config.skipCount : 1);
skipCount = (config.skipCount && config.skipCount > 0 ?
	     (skipCount <= (config.skipCount / 2) ? 1 : 0) :
	     1);

if (QP_getCookie("Survey_" + config.surveyID) != "0" && skipCount == 1) {
    document.onmousedown = QP_mouseDown;
    document.onmousemove = QP_mouseMove;
    document.onmouseup = QP_mouseUp;    
    if (config.popupMode != 0) {
	myContainer = QP_addDiv("QuestionProPopupSurvey_" + config.surveyID);    
	myContainer.innerHTML = QP_getInnerContent();
	var styleText='display:none;padding:0px; position:absolute; z-index:999; width:250px; left:' +
	    config.windowPositionLeft + 'px;top:' + config.windowPositionTop + 'px';
	QP_setStyle(myContainer, styleText);
	myContainer.onmouseover=QP_stopAnimation;
	
	if (myContainer) {
	    if (config.invitationDelay == 0) {
		showContainer();
	    } else {
		setTimeout("showContainer()", config.invitationDelay * 1000);
	    }
	}
    } else {
	openPreview(config.takeSurveyURL + '?id=' + config.surveyID + config.sCustom);
    }
    QP_setCookie("Survey_" + config.surveyID, '0');
}

function QP_getInnerContent() {
    var s = '<div  style="border-style:solid;border-width:1px;">' +
	'<div style="font-family:Verdana,Arial,Geneva,helvetica,sans-serif; ' +
	'font-weight:bold; color:#000000; background-position:bottom; margin:0px 0px 0px 0px; padding: ' + 
	'3px; background-color:#C0C0C0;">' +
	'<div style="float:left; color:#000000">' +
  	  config.popupInvitationHeader +
	'</div>' +
	'<div align="right" style="color:#000000"><a href="javascript:closeQPDiv()" style="color:#000000">[X]</a></div>' +
	'</div>' +        
	'<div style="background-color:#ffffff; ' + 
	'background-repeat: repeat-y; background-position:top; margin:0px; padding: 5px 10px 5px 10px">' +
	'<div>' +
	config.popupMessage +
	'</div>' +    
	'<div>' +
	'<br/>' +
	(config.isRightToLeft  ? 
	 '<a href="javascript:startSurvey()">' +
	 '&raquo;' +
	 config.surveyStartMessage +
	 '</a>' +
	 '&nbsp;' +
	 '<a href="javascript:closeQPDiv()">No Thanks</a>' 
	 :
	 '<div align="center">' +
	 '<a href="javascript:closeQPDiv()">No Thanks</a>' +	
	 '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
	 '<b><a href="javascript:startSurvey()">' +
	 config.surveyStartMessage +
	 '&raquo;' +
	 '</b></a>' +
	 '</div>') +
	'</div>' +
    
	(config.showFooter ?
	 '<div style="border-bottom:1px dashed #cccccc;"><br/></div>' +
	 '<div align="right">' +
	 '<a href="' + config.home + '"><img border="0" src="' + config.home + 'images/smalllogo.gif"></a>' +
	 '</div>' 
	 : '') +
	'<div style="height:5px;">&nbsp;</div>' +    
	'</div>' +
	'<div style="background-color:#ffffff;' + 
	' background-repeat: no-repeat; background-position: top; height: 17px;"></div>' +    
	'</div>';
        return s;
} 


function QP_addDiv(divID) {
    var dialog = document.createElement('div');
    dialog.innerHTML = '<div id="' + divID + '"></div>';
    document.body.appendChild(dialog);
    return document.getElementById(divID);
}

function QP_setStyle(element, styleText) {
    element.setAttribute("style", styleText);
    element.style.cssText = styleText;
}


