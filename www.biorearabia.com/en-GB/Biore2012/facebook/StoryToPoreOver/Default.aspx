﻿<%@ Page Title="" Language="C#" MasterPageFile="~/facebook/FBContentPageMaster.Master"
	AutoEventWireup="True" CodeBehind="Default.aspx.cs" Inherits="Biore2012.facebook.StoryToPoreOver.Default" %>
<%@ Register TagPrefix="uc1" TagName="ucQuiz" Src="~/facebook/StoryToPoreOver/ucQuiz.ascx" %>
<%@ Reference Control="~/facebook/StoryToPoreOver/ucQuiz.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div id="fb-root">
	</div>
	<script type="text/javascript">
		var extJsHost_connect_facebook_net = (("https:" == document.location.protocol) ? "https://" : "http://");
		window.fbAsyncInit = function() {
			FB.init({
				appId: '<%= System.Configuration.ConfigurationManager.AppSettings["AuthorPartnershipFBAppId"] %>', // App ID
				//channelURL : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
				status: true, // check login status
				cookie: true, // enable cookies to allow the server to access the session
				oauth: true, // enable OAuth 2.0
				xfbml: true  // parse XFBML
			});

			// Additional initialization code here
			FB.Canvas.setAutoGrow();
			FB.Canvas.scrollTo(0, 0);
		};

		// Load the SDK Asynchronously
		(function(d) {
			var js, id = 'facebook-jssdk'; if (d.getElementById(id)) { return; }
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = extJsHost_connect_facebook_net + "connect.facebook.net/en_US/all.js";
			d.getElementsByTagName('head')[0].appendChild(js);
		} (document));
		
	</script>
	<!-- NOT LIKED -->
	<asp:Panel ID="panLikeOverlay" CssClass="likeOverlay" runat="server" Visible="true">
		<h1 class="sprite ir stpoBioreLogo">Biore</h1>
		<p class="sprite ir stpoLikeUsCtA">Like Us Now To Unlock Event Access!</p>
		<h2 class="sprite ir stpoLogo">A Story to Pore Over</h2>
		<p class="sprite ir stopLikeOverlayCopy">We've teamed up with our favorite chick-lit
			author to create a special short story for fans of Bior&eacute;&reg; Skincare. Join
			us as we reveal who she is, unlock access to the short story created for Bior&eacute;&reg;
			Skincare fans, and enter for a chance to win* great giveaways and contests!</p>
	</asp:Panel>
	
	<!-- LIKED -->
	<div class="userLikes">
		<a href="#fbShare" id="fbShare" class="fbShare ir">Share</a>
		<asp:PlaceHolder ID="phUserLikesPre10am" runat="server" Visible="true">
			<h1 class="sprite ir stpoBioreLogo">Biore</h1>
			<h2 class="sprite ir stpoLogo">A Story to Pore Over</h2>
			<div class="topSection">
				<div class="RSVPSec">
					<h3 class="sprite ir stpoRSVPHdr">Who Wrote Our Story to Pore Over? RSVP to Find Out!</h3>
					<p>Our favorite way to get our Pore Strip on? Curled up with a good book. That's why
						we've teamed up with our favorite author to create a special short story for fans
						of Bior&eacute;<sup>&reg;</sup> Skincare. We're revealing the Bior&eacute;<sup>&reg;</sup>
						Skincare mystery author and unlocking access to her story on <strong>November 1</strong>.
						To celebrate, we're giving away prizes, special offers, and exclusive content all
						day starting at <strong>10 AM EST</strong> that day.</p>
					<p>Don't miss out! Click <strong>Join Event</strong> now and we'll send you a reminder
						when the big day arrives.</p>
				</div>
				<div class="joinEventSec">
					<h4 class="sprite ir stpoEventSubHdr">Get a reminder for the big Facebook event on November
						1 starting at 10 AM EST.</h4>
					<a href="https://www.facebook.com/events/120029474815905/" target="_blank" class="sprite ir stpoEventBtn">Join Event</a>
				</div>
			</div>
			<div class="countdownSwatch">
				<p class="sprite ir stpoCountdownSwatchCopy">Find out who our Bior&eacute;&reg; Skincare
					secret author is in:</p>
				<div class="sprite stpoCountdownCalendar countdown">
					<span class="spriteCalDay ir daysLeft<%=daysLeft %>">
						<%=daysLeft %></span></div>
				<p class="sprite ir <asp:Literal ID="litDaysCopy" runat="server" Text="stpoCountdownDaysCopy" />">Days</p>
			</div>
			<div class="bottomSection">
				<div class="productBasket">
					<img src="images/stpoProductsSnap.jpg" alt="products in basket" />
				</div>
				<div class="sweepsEntry">
					<h3 class="sprite ir stpoSweepEntryHdr">All your favorites, one amazing chance to win!*</h3>
					<p>Get ready for our author reveal with skin that's ready for anything. Enter now and
						you could take home one of five Bior&eacute<sup>&reg;</sup> Skincare Gift Baskets
						featuring the new Bior&eacute<sup>&reg;</sup> Acne Clearing Scrub. It improves your
						complexion without damaging skin, so you can have clearer, healthier-looking skin
						in 2 days!</p>
					<a href="<%= System.Configuration.ConfigurationManager.AppSettings["path"] %>email-newsletter-sign-up/?promo=biore_us_201210_AthrPrtnPreLaunchSweeps63T2UY2&show=2"
						class="sprite ir stpoSweepsEntryBtn">Enter Now</a>
				</div>
			</div>
		</asp:PlaceHolder>
		<asp:PlaceHolder ID="phUserLikesTopContent10am" runat="server" Visible="false">
			<h1 class="sprite ir stpoBioreLogo">Biore</h1>
			<h2 class="sprite ir stpoLogo">A Story to Pore Over</h2>
			<div class="topSection">
				<div class="introCopy">
					<h3 class="spritePhase2 ir stpo10amHdr">Today is the day!</h3>
					<p>Let the celebration begin! We've teamed up with our favorite chick-lit author to
						create a special short story for fans of Bior&eacute;<sup>&reg;</sup> Skincare and
						today is the day we reveal who she is!</p>
					<p>We're unlocking special content all day including the big author announcement, access
						to her short story, and amazing chances to win* great giveaways and contests!</p>
				</div>
			</div>
		</asp:PlaceHolder>
		<asp:PlaceHolder ID="phUserLikesTopContent1p3p" runat="server" Visible="false">
			<img src="images/stpoAuthorPic.jpg" alt="Jane Green image" class="authorImg" />
			<h1 class="sprite ir stpoBioreLogo">Biore</h1>
			<h2 class="sprite ir stpoLogo">A Story to Pore Over</h2>
			<div class="topSection tabs1and3pm">
				<div class="introCopy">
					<h3 class="spritePhase2 ir stpo1and3pmHdr">The big moment has arrived! Jane Green is the Bior&eacute;&reg;
						Skincare secret author</h3>
					<p class="introPara">Download <strong><em>A Walk in the Park</em></strong>, Jane's short story written exclusively 
						for Bior&eacute;<sup>&reg;</sup> Skincare fans!</p>
					<p>British import Jane Green is the author of 13 bestselling novels dealing with real
						women and all the things life throws their way through her trademark wisdom, wit
						and warmth. Get her exclusive short story now.</p>
				</div>
			</div>
			<div class="downloadPanel">
				<a href="ebooks5d330e6h13a/AWalkInThePark.pdf" class="spritePhase2 ir stpoDownloadStoryBtn" target="_blank">Download the story PDF</a>
				<p>Or download in another format type: <a href="ebooks5d330e6h13a/AWalkInThePark.mobi" target="_blank">mobi</a>, <a href="ebooks5d330e6h13a/AWalkInThePark.epub" target="_blank">epub</a>.</p>
			</div>
		</asp:PlaceHolder>
		<asp:Panel ID="panTabContainer" CssClass="tabContainer" runat="server" Visible="false">
			<ul id="tabs" class="tabsHolder">
				<li class="spritePhase2 ir tabs10amInactive tab tab1000am <%= tab10amislocked %>"><span class="lock"></span><a href="#tabPanel1000am" class="ir">
					10AM</a></li>
				<li class="spritePhase2 ir tabs1pmInactive tab tab0100pm <%= tab1pmislocked %>"><span class="lock"></span><a href="#tabPanel0100pm">
					1PM</a></li>
				<li class="spritePhase2 ir tabs3pmInactive tab tab0300pm <%= tab3pmislocked %>"><span class="lock"></span><a href="#tabPanel0300pm">
					3PM</a></li>
			</ul>
			<div class="tabPanels">
				<div id="tabPanel1000am" class="tabPanel <%= tab10amislocked %> tabPanel1000am">
					<asp:Panel ID="pan10amGiveaway" CssClass="pan10amGiveaway" runat="server" Visible="false">
						<img src="images/stpo10amTabProduct.jpg" alt="Biore Acne Clearing Scrub" />
						<h4 class="spritePhase2 ir stpoTab10amSubHdr">GET BIOR&Eacute;&reg; ACNE CLEARING SCRUB BEFORE YOU CAN BUY IT</h4>
						<p>Act fast! If you're among the first 250 to enter, you'll win* a full size Bior&eacute;<sup>&reg;</sup> Acne Clearing Scrub! It continually improves complexion without damaging your skin, so scrub daily for clearer, healthier-looking skin&mdash;in just 2 days.</p>
						<a href="<%= System.Configuration.ConfigurationManager.AppSettings["path"] %>email-newsletter-sign-up/?promo=biore_us_201210_AthrPrtnLaunchDayAGiveaway9KWNM52&show=2" class="spritePhase2 ir stpo10amGetItNowBtn">Get It Now</a>
					</asp:Panel>
					<asp:Panel ID="pan10amGiveawayOver" CssClass="pan10amGiveawayOver" runat="server"
						Visible="false">
						<img src="images/stpo10amTabProduct.jpg" alt="Biore Acne Clearing Scrub" />
						<h4 class="spritePhase2 ir stpoTab10amThanksSubHdr">THANKS FOR CHECKING OUT THIS CHANCE TO WIN</h4>
						<p>250 Bior&eacute;<sup>&reg;</sup> Skincare fans were quick to snatch up our Bior&eacute;<sup>&reg;</sup> Acne Clearing Scrub, so this giveaway has ended. But no worries&mdash;we're giving away prizes, special offers, and more exciting giveaways all day so be sure to check back often!</p>
						<div class="polaroids"></div>
					</asp:Panel>
				</div>
				<div id="tabPanel0100pm" class="tabPanel <%= tab1pmislocked %> tabPanel0100pm">
					<uc1:ucQuiz ID="ucQuiz" runat="server" />
				</div>
				<div id="tabPanel0300pm" class="tabPanel <%= tab3pmislocked %> tabPanel0300pm">
					<asp:Panel ID="pan300Giveaway" CssClass="pan300Giveaway" runat="server" Visible="false">
						<h4 class="spritePhase2 ir stpoTab3pmSubHdr">Enter for the chance to win* Another Piece of My Heart!</h4>
						<p>Enter now for the chance to win* Jane's latest book, Another Piece of My Heart, in hardback. We're giving away 25 copies and one of them could go to you!</p>
						<a href="<%= System.Configuration.ConfigurationManager.AppSettings["path"] %>email-newsletter-sign-up/?promo=biore_us_201210_AthrPrtnLaunchDayBSweeps27SZG93&show=2" class="spritePhase2 ir stpoEnterNowBtn">Enter Now</a>
					</asp:Panel>
				</div>
			</div>
		</asp:Panel>
		<asp:PlaceHolder ID="phPostLaunchDayContent" runat="server" Visible="false">
			<img src="images/stpoAuthorPic.jpg" alt="Jane Green image" class="authorImg" />
			<h1 class="sprite ir stpoBioreLogo">Biore</h1>
			<h2 class="sprite ir stpoLogo">A Story to Pore Over</h2>
			<div class="topSection">
				<div class="introCopy">
					<h3 class="spriteVBC ir stpoVBCHdr">Win a spot in our virtual book club!</h3>
					<p class="introPara">What's better than 'poring' over a good book? Talking about it with friends&mdash;or even better, with the author herself!</p>
					<p>Five lucky winners get a live Q&amp;A session<br />with Jane, a copy of her exclusive short story,<br /><strong><em>A Walk in the Park</em></strong>, and other special surprises. Tell us why you want to be a part of this once-in-a-lifetime Book Club experience<br />and you could become one of our five members! <a href="#howDoesItWorkPopup" id="howDoesItWorkPopup">How Does It Work?</a></p>
					<a href="PostLaunchForm.aspx?promo=biore_us_201210_AthrPrtnLaunchDayCFinalSweeps5M2DBX67" class="spritePhase2 ir stpoEnterNowBtn">Enter Now</a>
				</div>
			</div>
			<div class="stpoDivider"></div>
			<div class="ebookDownload">
				<img src="images/stpoWalkInTheParkCover.jpg" alt="Book cover" class="bookCover" />
				<h3 class="spriteVBC ir stpoWalkInTheParkSubHdr">A Walk In The Park</h3>
				<p>Download <strong><em>A Walk in the Park</em></strong>, Jane's short story written exclusively for Bior&eacute;<sup>&reg;</sup> Skincare fans!</p>
				<p>British import Jane Green is the author of 13 bestselling novels dealing with real women and all the things life throws their way through her trademark wisdom, wit and warmth. Get her exclusive short story now.</p>
				<div class="downloadPanel">
					<a href="ebooks5d330e6h13a/AWalkInThePark.pdf" class="spriteVBC ir stpoVBCDownloadStoryBtn" target="_blank">Download the story PDF</a>
					<p>Or download in another format type: <a href="ebooks5d330e6h13a/AWalkInThePark.mobi" target="_blank">mobi</a>, <a href="ebooks5d330e6h13a/AWalkInThePark.epub" target="_blank">epub</a>.</p>
				</div>
			</div>
			<div class="stpoDivider"></div>
			<uc1:ucQuiz ID="ucQuizPostLaunch" runat="server" />
			<div class="howDoesItWorkPopup">
				<a href="#close" class="spriteVBC ir stpoHowItWorksPopupClose">Close</a>
				<h4 class="spriteVBC ir stpoHowItWorksHdr">How Does The Virtual Book Club Work?</h4>
				<p>Winners will receive a Book Club Bundle prior to the event containing a video camera to conference into the Book Club session, a copy of the short story, and other special surprises.</p>
				<p>To qualify, prospective members<br />must have a home computer<br />with video capability and be<br />available on <strong>December 5, 2012</strong><br />for program taping.</p>
				<p>Please see the <a href="/en-US/promotions/rules/A-Story-to-Pore-Over-Rules.aspx" target="_blank">official rules</a><br />for complete details.</p>
			</div>
			<div class="howDoesItWorkPopupOverlay"></div>
		</asp:PlaceHolder>
		<asp:PlaceHolder ID="phInterimContent" runat="server" Visible="false">
			<h1 class="sprite ir stpoBioreLogo">Biore</h1>
			<h2 class="sprite ir stpoLogo">A Story to Pore Over</h2>
			<div class="topSection">
				<div class="introCopy">
					<p>We're contacting our book club winners and conducting our virtual book club with Jane Green &ndash; our Bior&eacute;<sup>&reg;</sup> Skincare mystery author! Check back soon for book club footage and event photos.</p>
					<p>In the meantime, dive into <em><strong>A Walk in the Park</strong></em> &ndash; Jane's short story written exclusively for Bior&eacute;<sup>&reg;</sup> Skincare fans, take our quiz to find out what your next read should be, and even start your own book club!</p>
				</div>
			</div>
			<div class="stpoDivider"></div>
			<div class="ebookDownload">
				<img src="images/stpoWalkInTheParkCover.jpg" alt="Book cover" class="bookCover" />				
				<h3 class="spriteVBC ir stpoWalkInTheParkSubHdr">A Walk In The Park</h3>
				<p>Download <strong><em>A Walk in the Park</em></strong>, Jane's short story written exclusively for Bior&eacute;<sup>&reg;</sup> Skincare fans!</p>
				<p>British import Jane Green is the author of 13 bestselling novels dealing with real women and all the things life throws their way through her trademark wisdom, wit and warmth. Get her exclusive short story now.</p>
				<div class="downloadPanel">
					<a href="ebooks5d330e6h13a/AWalkInThePark.pdf" class="spriteVBC ir stpoVBCDownloadStoryBtn" target="_blank">Download the story PDF</a>
					<p>Or download in another format type: <a href="ebooks5d330e6h13a/AWalkInThePark.mobi" target="_blank">mobi</a>, <a href="ebooks5d330e6h13a/AWalkInThePark.epub" target="_blank">epub</a>.</p>
				</div>
			</div>
			<div class="stpoDivider"></div>
			<div class="behindTheScenes">
				<div class="videoContainer">
					<div class="videoHolder">
						<a class="videoLink vid1 defaultVid ir" rel="videoParams::384|288|55" href="video/Jane_and_Biore_H264_384x288_750kbps.mp4">Watch Video</a>
					</div>
					<ul>
						<li><a href="video/Jane_and_Biore_H264_384x288_750kbps.mp4" rel="videoParams::384|288|55" class="videoLink vid1 selected ir">Jane &amp; Us</a></li>
						<li><a href="video/Jane_On_skincare_H264_384x288_750kbps.mp4" rel="videoParams::384|288|82" class="videoLink vid2 ir">Jane On Skincare</a></li>
						<li><a href="video/Jane_on_Inspiration_H264_384x288_750kbps.mp4" rel="videoParams::384|288|137" class="videoLink vid3 ir">Jane On Inspiration</a></li>
						<li><a href="video/Jane_Getting_to_Know_H264_384x288_750kbps.mp4" rel="videoParams::384|288|229" class="videoLink vid4 ir">Getting To Know Jane</a></li>
					</ul>					
				</div>
				<h3 class="spriteInt ir stpoBehindTheScenes">Behind the Scenes</h3>
				<p>Jane Green sat down for an exclusive Q&amp;A session with Bior&eacute;<sup>&reg;</sup> Skincare. Get the inside scoop straight from the author.</p>
			</div>
			<div class="stpoDivider"></div>
			<div class="startBookClub">
				<img src="images/stpoStartBookClub.jpg" alt="Book club picture" />
				<h3 class="spriteInt ir stpoStartBookClubHdr">Start your own book club</h3>
				<p>Inspired by Jane's virtual book club? Now is the perfect time to start a book club of your very own. It's simple, easy, and fun. And hey, you always love a reason to 'pore' over more fabulous stories, right? Here are some tips to get you started!</p>
				<a href="#GetTips" id="getTips" class="spriteInt ir stpoGetTipsBtn">Get Tips</a>
			</div>
			<div class="stpoDivider"></div>
			<uc1:ucQuiz ID="ucQuizInterim" runat="server" />
			<div class="howDoesItWorkPopup">
				<a href="#close" class="spriteVBC ir stpoHowItWorksPopupClose">Close</a>
				<h4 class="spriteInt ir stpoClubTipsHdr">HOW TO START A BOOK CLUB</h4>

				<h5>Ready, Set - Think It Through!</h5>
				<p>Before you send an email inviting all your friends and family to join your book club, take a moment to think it all through. Where do you plan on meeting? What kind of books do you want to read? How often are you hoping to meet? The answers will provide added insight and help shape your book club right out of the gate.</p>
				
				<h5>Inviting Your Members</h5>
				<p>Now that you have an idea of your book club style, it's time to invite members. Don't be afraid to mix it up &mdash; think work friends, long-time pals and family members. Just be sure not to get carried away! About eight to ten members is just the right size. That way, your group is small enough to fit inside your home or around a table in a café and everyone's opinions are sure to be heard.</p>
				
				<h5>Set Up for Success</h5>
				<p>Before your first meeting begins, think about how you want the book club to run so that it's organized and enjoyable for everyone. Here are some things to keep in mind:</p>
				<ul>
					<li>Who will lead each meeting's discussion? It makes sense for you to suggest the first book (may we suggest Jane Green's short story, <em>A Walk in the Park</em>?!), but it's up to you to decide how it will be organized from there on out.</li>
					<li>Will the group keep a log of the books read and meeting notes? Or do you want to keep it more casual and flexibile?</li>
					<li>Where will meetings be held? Will the group travel to each member's home or a central meeting location like a restaurant or coffee shop? And how often will the group meet?</li>
					<li>Will refreshments be served and if so, who will provide them?</li>
				</ul>
				
				<h5>Keep the Momentum Going</h5>
				<p>A great way to keep your book club connected between meetings is to start a free online blog. You'll save time by eliminating the need for group emails that can easily get overwhelming for everyone. Plus, it's a great way to communicate with your book club members, make special announcements, and keep the momentum going.</p>
				
				<h5 class="btmCtA">Follow these tips and you'll be on your way to a successful book club. Have fun!</h5>
			</div>
			<div class="howDoesItWorkPopupOverlay"></div>
		</asp:PlaceHolder>
	</div>
	<asp:PlaceHolder ID="phDisclaimerPreLaunch" runat="server">
	<p class="disclaimer discPre">* NO PURCHASE NECESSARY. Open to legal residents of the
		50 United States (D.C.), 18 years or older. Sweepstakes ends 11/1/12 at 9:59 a.m.
		EST. To enter and for Official Rules, including prize descriptions, visit <a href="/en-US/promotions/rules/A-Story-to-Pore-Over-Rules.aspx"
			target="_blank">http://www.biore.com/en-US/promotions/rules/A-Story-to-Pore-Over-Rules.aspx</a>.
		Void where prohibited.</p>
	</asp:PlaceHolder>
	<asp:PlaceHolder ID="phDisclaimerLaunchDay" runat="server">
	<p class="disclaimer disctabPanel1000am">* NO PURCHASE NECESSARY. Open to legal residents of the
		50 United States (D.C.), 18 years or older. Promotion ends after the first 250 entrants
		register or at 11/1/12 at 2:59 p.m. EST, whichever occurs first. To enter and for
		Official Rules, including prize descriptions, visit <a href="/en-US/promotions/rules/A-Story-to-Pore-Over-Rules.aspx"
			target="_blank">http://www.biore.com/en-US/promotions/rules/A-Story-to-Pore-Over-Rules.aspx</a>.
		Void where prohibited.</p>
	<p class="disclaimer disctabPanel0300pm">* NO PURCHASE NECESSARY. Legal residents of the 50 United States (D.C.), 
		18 years or older and who did not purchase any equipment for purposes of entering the Promotion. 
		Sweepstakes ends 11/1/12 at 4:59 p.m. EST. To enter and for Official Rules, including prize descriptions, 
		visit <a href="/en-US/promotions/rules/A-Story-to-Pore-Over-Rules.aspx"
			target="_blank">http://www.biore.com/en-US/promotions/rules/A-Story-to-Pore-Over-Rules.aspx</a>.
		Void where prohibited.</p>
	</asp:PlaceHolder>
	<asp:PlaceHolder ID="phDisclaimerPostLaunch" runat="server">
	<p class="disclaimer discPost">* NO PURCHASE NECESSARY. Open to legal residents of the 50 United States (D.C.), 
		18 years or older. Contest ends 11/12/12. To enter and for Official Rules, including prize descriptions, 
		visit <a href="/en-US/promotions/rules/A-Story-to-Pore-Over-Rules.aspx"
			target="_blank">http://www.biore.com/en-US/promotions/rules/A-Story-to-Pore-Over-Rules.aspx</a>.
		Void where prohibited.</p>
	</asp:PlaceHolder>
	<p class="footerLinks"><a href="//www.kaobrands.com/privacy_policy.asp" target="_blank">
		View Privacy Policy</a>&nbsp;&nbsp;&nbsp; <a href="/en-US/promotions/rules/A-Story-to-Pore-Over-Rules.aspx"
			target="_blank">View All Rules</a></p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script type="text/javascript">
        if (typeof jQuery === 'undefined') {
            var source = '<%= VirtualPathUtility.ToAbsolute("~/facebook/StoryToPoreOver_phase2/js/jquery-1.8.2.min.js") %>';
            document.write(unescape("%3Cscript src='"+source+"' type='text/javascript'%3E%3C/script%3E"));
        }

        var FBVars = {
		  		fbAppId: '<%= System.Configuration.ConfigurationManager.AppSettings["AuthorPartnershipFBAppId"] %>',
		  		fbShareUrl: '<%= System.Configuration.ConfigurationManager.AppSettings["FBAuthorPartnershipPageLink"] %>',
				fbSharePicPath: '<%= System.Configuration.ConfigurationManager.AppSettings["FBAuthorPartnershipImageUrl"]%>',
				nameLine: 'Bioré® A Story to Pore (Strip) Over',
		  		baseURL: '<%= System.Configuration.ConfigurationManager.AppSettings["FBAuthorPartnershipBaseUrl"]%>',
		  		capt: '',
		  		desc: 'Bioré® Skincare has teamed up with their favorite author for an exclusive short story, plus giveaways, special offers and contests!'
		  	};

    </script>
	<%=SquishIt.Framework.Bundle .JavaScript()
		.Add("~/facebook/js/jquery.easytabs.js")
		.Add("~/js/swfobject.min.js")
		.Add("~/js/jquery.ENLvideo.js")
		.Add("~/facebook/js/stpoMain.js")
		.Render("~/facebook/js/stpoJS_#.js")
	%>
</asp:Content>
