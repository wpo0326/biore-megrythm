﻿<%@ Page Title="" Language="C#" MasterPageFile="~/facebook/FBContentPageMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Biore2012.facebook.Welcome.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div id="fb-root"></div>

	<script type="text/javascript">
		var extJsHost_connect_facebook_net = (("https:" == document.location.protocol) ? "https://" : "http://");
		window.fbAsyncInit = function() {
			FB.init({
				appId: '<%= System.Configuration.ConfigurationManager.AppSettings["welcomeFBAppId"] %>', // App ID
				//channelURL : '//WWW.YOUR_DOMAIN.COM/channel.html', // Channel File
				status: true, // check login status
				cookie: true, // enable cookies to allow the server to access the session
				oauth: true, // enable OAuth 2.0
				xfbml: true  // parse XFBML
			});

			// Additional initialization code here
			FB.Canvas.setAutoGrow();
			FB.Canvas.scrollTo(0, 0);
		};

		// Load the SDK Asynchronously
		(function(d) {
			var js, id = 'facebook-jssdk'; if (d.getElementById(id)) { return; }
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = extJsHost_connect_facebook_net + "connect.facebook.net/en_US/all.js";
			d.getElementsByTagName('head')[0].appendChild(js);
		} (document));
	</script>
	
	<div class="welcome">
		<asp:Panel ID="panLikeOverlay" CssClass="likeOverlay" runat="server" Visible="true">
			<h1>Click the Like Button for exclusive info and offers from Bior&eacute;<sup>&reg;</sup> Skincare</h1>
			<h2>Perks of liking Bior&eacute;<sup>&reg;</sup> skincare (besides getting clean skin you can count on)</h2>
			<dl>
				<dt id="exclusiveOffers">Exclusive Offers</dt>
				<dd>participate in promos only available to our Facebook fans like <span class="no-wrap">Prove It!&trade;</span> Reward Points—earn points towards free products.</dd>
				
				<dt id="askAway">Ask Away!</dt>
				<dd>The Bioré® Skincare team will answer your questions.</dd>
				
				<dt id="stayLoop">Stay in the loop</dt>
				<dd>Get updates on the latest news, promos and events.</dd>
				
				<dt id="talkFans">Talk to your fellow Biore® Skincare fans</dt>
				<dd>Share tips, ideas, and stories.</dd>
			</dl>
		</asp:Panel>
		<asp:Panel ID="panUserLikes" CssClass="userLikes" runat="server" Visible="false">
			<h1>Welcome Thanks for Joining Us</h1>
			<h2>Check out the latest promos, products and perks:</h2>
			<div id="proveIt">
				<h2>ProveIt!&trade; Reward Points</h2>
				<p>Join our rewards program to start earning points towards coupons, free products and more to come!</p>
				<a href="http://www.facebook.com/bioreskin?sk=app_205787372796203" target="_blank" class="linkBtn">Start Earning Now</a>
			</div>
			<div id="newLook">
				<h2>We've got a new look</h2>
				<p>See all the great changes we've made to our lineup on our <a href="<%=System.Configuration.ConfigurationManager.AppSettings["path"] %>clean-new-look" target="_blank">New Look</a> Facebook page and on our website.</p>
				<a href="<%=System.Configuration.ConfigurationManager.AppSettings["path"] %>biore-skincare" target="_blank" class="linkBtn">Take a Peek</a>
			</div>
			<div id="questions">
				<h2>Skincare Questions?</h2>
				<p>Feel free to ask questions on our wall and get answers from the Bior&eacute;<sup>&reg;</sup> Skincare Team and communicate with fellow fans.</p>
			</div>
		</asp:Panel>
	</div>
</asp:Content>
