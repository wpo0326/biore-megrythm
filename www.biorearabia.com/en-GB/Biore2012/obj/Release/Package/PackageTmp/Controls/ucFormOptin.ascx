﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="KaoBrands.FormStuff.ucFormOptin" Codebehind="ucFormOptin.ascx.cs" %>
<div class="OptinContainer">
    <div class="seperator png-fix"></div>
	<p class="privacy">
		Your privacy is important to us. You can trust that Kao USA Inc. will use personal information in accordance with our <a href="http://www.kaobrands.com/privacy_policy.asp"
			target="_blank">Privacy Policy</a>.
	</p>
	<div class="CurrentSiteOptinContainer">
	    <ul>
		    <li>
			    <asp:CheckBox Checked="false" ID="siteOptin" runat="server" />
			    <asp:Label ID="OptinLabel" runat="server" Text=""
				    AssociatedControlID="siteOptin" CssClass="siteOptinChkbox" />
			    <div class="ErrorContainer">
			        <asp:CustomValidator ID="siteOptinValidator" Display="Dynamic" runat="server" ErrorMessage="" 
				        ClientValidationFunction="optinJsValidate" OnServerValidate="optinValidate" SetFocusOnError="true"
				        EnableClientScript="false" CssClass="errormsg" Visible="true" />
				</div>
		    </li>
	    </ul>
	</div>
    
    <asp:Panel ID="PanelMultiOptin" CssClass="MultiBrandOptinContainer" runat="server">            
		<ul>            
			<li id="optinToAll">
				<asp:CheckBox ID="MultiOptin" runat="server" CssClass="multiOptin" />
				<asp:Label ID="MultiOptinLabel" runat="server" Text="Yes, I'd like to receive emails and newsletters from other great products from Kao USA Inc.:"
					AssociatedControlID="MultiOptin" />
			</li>			
			<li id="banListItem" runat="server">
				<asp:CheckBox ID="banoptin" runat="server" />
				<asp:Label ID="banoptinLabel" runat="server" Text="" AssociatedControlID="banoptin"
					CssClass="brandLabels" />
			</li>
			<li id="bioreListItem" runat="server">
				<asp:CheckBox ID="bioreoptin" runat="server" />
				<asp:Label ID="bioreoptinLabel" runat="server" Text="" AssociatedControlID="bioreoptin"
					CssClass="brandLabels" />
			</li>
			<li id="curelListItem" runat="server">
				<asp:CheckBox ID="cureloptin" runat="server" />
				<asp:Label ID="cureloptinLabel" runat="server" Text="" AssociatedControlID="cureloptin"
					CssClass="brandLabels" />
			</li>
			<li id="jergensListItem" runat="server">
				<asp:CheckBox ID="jergensoptin" runat="server" />
				<asp:Label ID="jergensoptinLabel" runat="server" Text="" AssociatedControlID="jergensoptin"
					CssClass="brandLabels" />
			</li>
			<li id="jfriedaListItem" runat="server">
				<asp:CheckBox ID="johnfriedaoptin" runat="server" />
				<asp:Label ID="johnfriedaoptinLabel" runat="server" Text="" AssociatedControlID="johnfriedaoptin"
					CssClass="brandLabels" />
			</li>
			<li>
				<p class="privDisclaimer">Before submitting your information, please view our <a
					href="http://www.kaobrands.com/privacy_policy.asp" target="_blank">Privacy Policy</a>.</p>
			</li>
		</ul>
	</asp:Panel>		
</div> 

<script type="text/javascript">    
var ban = document.getElementById("<%=banoptin.ClientID%>");
var curel = document.getElementById("<%=cureloptin.ClientID%>");
var jergens = document.getElementById("<%=jergensoptin.ClientID%>");
var biore = document.getElementById("<%=bioreoptin.ClientID%>");
var johnfrieda = document.getElementById("<%=johnfriedaoptin.ClientID%>");
var multioptin = document.getElementById("<%=MultiOptin.ClientID%>");
var optinArray = [ban, curel, jergens, biore, johnfrieda];

for (var i = 0; i < optinArray.length; i++) {
    if (optinArray[i]) {
        addEvent(optinArray[i], "click", setCheckbox, false); 
    }
}
   
addEvent(multioptin, "click", setAllCheckboxes, false);

function addEvent(elm, evType, fn, useCapture) {
    if (elm.addEventListener) {
        elm.addEventListener(evType, fn, useCapture);
        return true;
    }
    else if (elm.attachEvent) {
        var r = elm.attachEvent('on' + evType, fn);
        return r;
    }
    else { elm['on' + evType] = fn; }
}

function setCheckbox() {
    if (ban.checked || curel.checked || jergens.checked || biore.checked || johnfrieda) {
        multioptin.checked = true;
    }
    else { multioptin.checked = false; }
}

function optinJsValidate(sender, args) {
    document.getElementById("<%=siteOptin.ClientID%>").checked ? args.IsValid = true : args.IsValid = false;
}

function setAllCheckboxes() {
    if (multioptin.checked) {
        for (var i = 0; i < optinArray.length; i++) {
            if (optinArray[i]) { optinArray[i].checked = true; }
        }       
    } else {
        for (var i = 0; i < optinArray.length; i++) {
            if (optinArray[i]) { optinArray[i].checked = false; }
        }
    }
}
</script>