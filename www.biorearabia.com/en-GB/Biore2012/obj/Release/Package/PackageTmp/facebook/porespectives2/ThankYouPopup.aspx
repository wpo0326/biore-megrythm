﻿<%@ Page Title="" Language="C#" MasterPageFile="~/facebook/FBContentPageMaster.Master" AutoEventWireup="true" CodeBehind="ThankYouPopup.aspx.cs" Inherits="Biore2012.facebook.porespectives2.ThankYouPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<div class="pores">
		<asp:Panel ID="panThankYou" CssClass="thankYou" runat="server" Visible="true">
			<h1>Thank You!</h1>
			<p>Your Pore-spective has been successfully submitted.</p>
			<a href="#" id="closeBtn" class="closeBtn">Close</a>
		</asp:Panel>
		<asp:Panel ID="panCancelled" CssClass="cancelled" runat="server" Visible="false">
			<h1>We're Sorry</h1>
			<p>The Facebook Share was cancelled.  Please try again if you want to share your Pore-spective.</p>
			<a href="#" id="closeBtn" class="closeBtn">Close</a>
			<script>window.close();</script>
		</asp:Panel>
		<asp:Panel ID="panError" CssClass="error" runat="server" Visible="false">
			<h1>We're Sorry</h1>
			<p>There was an error.  Please try again later.</p>
			<a href="#" id="closeBtn" class="closeBtn">Close</a>
		</asp:Panel>
    </div>
	<%=SquishIt.Framework.Bundle.JavaScript()
		.Add("js/main.js")
		.Render("js/combinedpsty_#.js")
	%>
</asp:Content>
