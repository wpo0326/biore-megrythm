﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Charcoal.aspx.cs" Inherits="Biore2012.biore_facial_cleansing_products.Charcoal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta content="Purify pores with our Charcoal Mask and Charcoal Face Wash. Try Deep Pore Charcoal Cleanser and Self Heating One Minute Mask from Bioré® Skincare." />
    <meta content="Charcoal Mask, Charcoal Face Wash, charcoal" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/charcoalLanding.css")
        .Render("~/css/combinednew_#.css")
    %>
    <meta name="viewport" content="width=device-width">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">
<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: Biore Whats New Page
URL of the webpage where the tag is expected to be placed: http://www.biore.com/en-US/special-offers-and-skincare-tips/
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 02/01/2012
-->
<script type="text/javascript">
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    document.write('<iframe src="http://fls.doubleclick.net/activityi;src=1418070;type=biore797;cat=biore504;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="http://fls.doubleclick.net/activityi;src=1418070;type=biore797;cat=biore504;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>

<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="main" class="charcoalLanding">
    <div id="mainContent">
        <div id="shadow"></div>
        <div class="centeringDiv">
                <div class="charcoalHeaderSpacer"></div>
                <h1><img src="<%= VirtualPathUtility.ToAbsolute("~/images/charcoalLanding/charcoalHeader.png") %>" border="0" alt="" /></h1>
                
            <div id="charcoalProdWrap">
                <div id="charDescript">
                    <div id="charPaint"></div>
                    <h2 class="archer-book">Yes, <span class="charGrey archer-bold">CHARCOAL!</span></h2>
                    <p>Like a magnet, it draws out and traps impurities. And now, Bioré<sup>®</sup> Skincare brings you this revolutionary ingredient in 2 cleansers:<br /><br />
                        <b>NEW <span class="charGreeen">Deep Pore Charcoal Cleanser</span></b> and our <b><span class="charGreeen">Self Heating One Minute Mask.</span></b><br />   Both products draw out deep down dirt, oil, and impurities from pores. They purify skin and remove 2x more dirt and oil than a basic cleanser.</p>
                </div>
                <div id="charProducts"></div>
            </div>
            
            <div class="charList archer-bold">
                <span class="charGreeen">Benefits of Charcoal Mask and Charcoal Cleanser</span><br /><br />

                <span class="charbullet"></span>Deep cleans 40% more dirt & impurities than a basic cleanser.<br />
                <span class="charbullet"></span>Cleanser traps 2x more surface toxins than a basic cleanser. <br />
                <span class="charbullet"></span>Mask draws out 2.5x more surface toxins.<br />
                <span class="charbullet"></span>Leaves skin tingly-smooth.<br />
                <span class="charbullet"></span>Proven to purify pores instantly.<br />
                <span class="charbullet"></span>Deep cleans all 200,000 of your pores.<br />
            </div>

            <div id="charTryNowWrap">
                <div id="charTryProdOne">
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/charcoal-for-normal-or-oily-skin/deep-pore-charcoal-cleanser") %>"><div class="charTry">Try it Now</div></a>
                </div>
                <div id="charTryProdTwo">
                    <a href="<%= VirtualPathUtility.ToAbsolute("~/charcoal-for-normal-or-oily-skin/self-heating-one-minute-mask") %>"><div class="charTry">Try it Now</div></a>
                </div>
            </div>
           
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
