﻿<%@ Page Title="Where To Buy | Bior&eacute;&reg; Skincare" MasterPageFile="../Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.wheretobuy._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
	#storeLogos ul li { width:240px; height:120px; float:left; display:block; text-align:center;}
    #storeLogos ul li a {width:180px; height:100px; background-position:center left;text-indent:-9999px; display:block;}
	#storeLogos ul li a:hover { background-position:right;}
	</style>
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1>Where To Buy</h1>
                    <div id="storeLogos" class="logos">
                        <p>Biore skincare products are available to buy across the region.<br /><br /><br /></p>
                        <div class="content png-fix">
             
                <div style="float:left; width:32%; margin-right:1.33%">
                <h2><a href="#">UAE</a></h2> 
                <p>Abu Dhabi Coop Society<br>
                Al Azizia Panda United<br>
                Al Maya Trading Co LLC<br>
                Carrefour Group<br>
                EMKE &amp; Lulu Group<br>
                Megamart Hypermarket L.L.C<br>
                Safeer Group<br>
                Spinneys<br>
                T.Choithram &amp; Sons<br>
                Union Co-Op.Society</p><br>
                </div>

                <div style="float:left; width:32%; margin-right:1.33%">
				<h2><a href="#">Kuwait</a></h2>
                <p>Al Mirah<br>
                Carrefour<br>
                City Center<br>
                Geant<br>
                Gulf Mart<br>
                London Shopping Center<br>
                Lulu<br>
                The Sultan Center</p><br>
                </div>
                
                <!--<div style="float:left; width:32%; margin-right:1.33%">
                <h2><a href="#">Qatar</a></h2>
                <p>Al Meera<br>
                Carrefour<br>
                Family Food Center<br>
                Kabayan<br>
                Lulu Hypermarket<br>
                Lulu Hypermarkets<br>
                Safari Hypermarkets<br>
                Seashore Food Center<br>
                Supermarket</p>
                </div>-->

                <div style="float:left; width:32%; margin-right:1.33%">
				<h2><a href="#">Bahrain</a></h2>
                <p>In selected pharmacies and hypermarkets nationwide</p><br>
                </div>

           		<div class="clear"></div>
                
                <div style="float:left; width:32%; margin-right:1.33%">
                <h2><a href="#">Oman</a></h2>
                <p>In selected pharmacies and hypermarkets nationwide</p><br>
                </div>

                <!--<div style="float:left; width:32%; margin-right:1.33%">
				<h2><a href="#">Jordan</a></h2>
                <p>Carrefour<br>
                Cozmo<br>
                C-town<br>
                Food City<br>
                Miles<br>
                Plaza SS<br>
                Safeway<br>
                Yaser mall</p><br>
                </div>-->
                      
                <div style="float:left; width:32%; margin-right:1.33%">
                <h2><a href="#">KSA</a></h2>
                <p>Al Danoub<br>
                Al Othaim Markets<br>
                Al-Jazeera Super Stores<br>
                Al-Sarawat Stores<br>
                Bin Dawood<br>
                Carrefour<br>
                Farm 9<br>
                Farm Shopping Center<br>
                Panda-Aziziah<br>
                Star Markets<br>
                Tamimi</p>
                </div>
                
           		<div class="clear"></div>
                
                </div>        
                    </div>
                    <div style="clear:both; height:0px;"></div>
            </div>
        </div>
    </div>
</asp:Content>