﻿<%@ Page Title="Where To Buy | Bior&eacute;&reg; Skincare" MasterPageFile="~/Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.wheretobuy._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Explore the Bioré® Skincare website." name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/utilityPages.css")
        .Render("~/css/combinedutility_#.css")
    %>
     <style type="text/css">
	
	</style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="floodlightPixels" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                 <h1>Where To Buy</h1>
                    <div id="storeLogos" class="logos">
                        <p>Biore skincare products are available to buy across the region.<br /><br /><br /></p>
                        <div class="content png-fix">
             
                <div style="float:left; width:32%; margin-right:1.33%">
                <h2><a href="#">UAE</a></h2> 
                <p>Abu Dhabi Co-op Society<br />
                Hyper Panda<br />
                Al Maya Supermarket<br />
                Carrefour<br />	
                Lulu Hypermarket & Supermarket<br />
                Megamart Hypermarket<br />
                Safeer<br />
                Spinneys<br />
                T.Choithram &amp; Sons<br />
                Union Co-Op.Society<br />
                Boots Pharmacy<br />
                Bin Sina Pharmacy<br />
                Life Pharmacy<br />
                Al Ain Pharmacy</p><br />
                </div>

                <div style="float:left; width:32%; margin-right:1.33%">
                <h2><a href="#">KSA</a></h2>
                <p>Al Danoub<br />
                Al Othaim Markets<br />
                Al-Jazeera Super Stores<br />
                Al-Sarawat Stores<br />
                Bin Dawood<br />
                Carrefour<br />
                Farm Stores and Shopping Center<br />
                Panda-Aziziah<br />
                Star Markets<br />
                Tamimi<br />
                United Pharmacies <br />
                Nahdy Pharmacies<br />
                Dawaa pharmacies</p>
                </div>

                <div style="float:left; width:32%; margin-right:1.33%">
				<h2><a href="#">Bahrain</a></h2>
                <p>In selected pharmacies and hypermarkets nationwide</p><br />
                </div>
               
                
                <!--<div style="float:left; width:32%; margin-right:1.33%">
                <h2><a href="#">Qatar</a></h2>
                <p>Al Meera<br />
                Carrefour<br />
                Family Food Center<br />
                Kabayan<br />
                Lulu Hypermarket<br />
                Lulu Hypermarkets<br />
                Safari Hypermarkets<br />
                Seashore Food Center<br />
                Supermarket</p>
                </div>-->

           		<div class="clear"></div>

                <div style="float:left; width:32%; margin-right:1.33%">
				<h2><a href="#">Kuwait</a></h2>
                <p>Lulu Hypermarket<br />
                City Center Hypermarket<br />
                Gulf Mart Hypermarket<br />
                Geant Hypermarket<br />
                Carrefour Hypermarket<br />
                Saveco Hypermarket<br />
                The Sultan Center<br />
                Haras Al Watani COOP<br />
                Hateen COOP Society<br />
                Abu-Halifa COOP Society<br />
                Sharq COOP<br />
                Shamieh & Shuwaikh COOP<br />
                Faiha COOP<br />
                Nuzha COOP<br />
                Khaldiya COOP<br />
                Yarmouk COOP<br />
                Kaifan COOP<br />
                Adailiya COOP<br />
                Qadsiya COOP<br />
                Rowdah & Hawally COOP<br />
                Surra COOP<br />
                Shaab COOP<br />
                Salmiah COOP<br />
                Rumaithya COOP<br />
                Jabriya COOP<br />
                Daheiah COOP<br />
                Jahra COOP<br />
                Sulaibikhat & Doha COOP<br />
                Andalus & Riggae COOP<br />
                Firdous COOP<br />
                Ardiya COOP<br />
                Omariya & Rabya COOP<br />
                Farwania COOP<br />
                Khaitan COOP<br />
                Mishref COOP<br />
                Bayan COOP<br />
                Sabah Al-Salem COOP<br />
                Rikka COOP<br />
                Hadiya COOP<br />
                Sabahiya COOP<br />
                Fahaheel COOP<br />
                Mubarak Al Kabeer & Qurain COOP<br />
                Fintas COOP<br />
                Ali Sabah Al Salem COOP<br />
                Sabah Al Naser COOP<br />
                Daher COOP<br />
                Salwa COOP<br />
                Qortuba COOP<br />
                Jaber Al-Ali COOP<br />
                Adan & Qusoor COOP<br />
                Rihab COOP<br />
                Naseem COOP<br />
                Ahmadi COOP
                Zahra COOP
                Madina Saad Al Abdulla COOP<br />
                Abdulla Al Bubarak COOP<br />
                Qairwan COOP<br />
                Defence Ministry COOP<br />
                Ali Abdul Wahab & Sons Pharmacy<br />
                Fialaka Pharmacy<br />
                Al Qattan Pharmacy<br />
                Yiaco Pharmacy</p><br />
                </div>

                <div style="float:left; width:32%; margin-right:1.33%">
                <h2><a href="#">Oman</a></h2>
                <p>In selected pharmacies and hypermarkets nationwide</p><br />
                </div>

                <!--<div style="float:left; width:32%; margin-right:1.33%">
				<h2><a href="#">Jordan</a></h2>
                <p>Carrefour<br />
                Cozmo<br />
                C-town<br />
                Food City<br />
                Miles<br />
                Plaza SS<br />
                Safeway<br />
                Yaser mall</p><br />
                </div>-->
                      
           		<div class="clear"></div>
                
                </div>        
                    </div>
                    <div style="clear:both; height:0px;"></div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>