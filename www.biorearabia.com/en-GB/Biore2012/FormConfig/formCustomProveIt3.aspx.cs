﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Biore2012.BLL;
using KAOForms;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Configuration;

namespace Biore2012.FormConfig
{
    public partial class formCustomProveIt3 : System.Web.UI.Page
    {
        private KAOForms.FormConfig oFormConfigValues;
        private DefaultValues oDefaultValues;
        private KaoBrands.FormStuff.ucFormMemberInfo ucFormMemberInfo;

        private bool pastCustomMaxCapFlag = false;


        /****************************************************************************/
        /****************************************************************************/
        //TODO: THESE SHOULD NOT BE HARDCODED HERE - MOVE TO WEB.APPSETTINGS.CONFIG ????
        string mainSubmitFormUniquePromoID = "biore_us_201204_TriPackette_Chow_Samp"; // THIS IS EVENTID=400259 //TODO: THIS SHOULD NOT BE HARDCODED

        /*        IN WEB.CONFIG
            <!-- CUSTOM FORM biore_us_201204_TriPackette_Chow_Samp -->
            <add key="201204TriPacketteChowSampleFormCapEventID1" value="400260" /><!-- Sample -->
            <add key="201204TriPacketteChowSampleFormCapCount1" value="1600" />
            <add key="201204TriPacketteChowSampleFormCapEventID2" value="400261" /><!-- Prove It Codes -->
            <add key="201204TriPacketteChowSampleFormCapCount2" value="3200" /> */
        int customMaxCapCheckEventID1 = int.Parse(System.Configuration.ConfigurationManager.AppSettings["201204TriPacketteChowSampleFormCapEventID1"]);
        string customMaxCapMaxEntriesCount1 = System.Configuration.ConfigurationManager.AppSettings["201204TriPacketteChowSampleFormCapCount1"];// CAN NOT HAVE COMMA IN THIS STRING

        int customMaxCapCheckEventID2 = int.Parse(System.Configuration.ConfigurationManager.AppSettings["201204TriPacketteChowSampleFormCapEventID2"]);
        string customMaxCapMaxEntriesCount2 = System.Configuration.ConfigurationManager.AppSettings["201204TriPacketteChowSampleFormCapCount2"];// CAN NOT HAVE COMMA IN THIS STRING

        string customMaxCapReachedMessage = "<div class='ConfrimationContainer'><h2>We're sorry.</h2><p>Due to an overwhelming response, this offer is no longer available. We appreciate your interest. If you would like to be notified of future offers and promotions, <a href='/en-US/email-newsletter-sign-up'>sign up for our newsletter</a>.</p><p><img alt='Prove It reward points' src='/en-US/images/proveIt/proveItLogo.png'></p><p>To start earning points towards free coupons, products and more, join Prove It! Rewards Points on Facebook!</p><p><a target='_blank' href='https://www.facebook.com/bioreskin?sk=app_205787372796203'>Start Earning Now <span class='arrow'>&gt;</span></a></p></div>";

        int secondarySampleEventID = int.Parse(System.Configuration.ConfigurationManager.AppSettings["201204TriPacketteChowSampleFormCapEventID1"]);
        string secondarySampleEventCode = "biore_us_201204_TriPackette_Chow_Samp_GetsSamp"; //TODO: THIS SHOULD NOT BE HARDCODED
        string secondarySampleOpenEnded = "Biore TriPackette Sample April Chow Media- Qualified to get Sample";  //TODO: THIS SHOULD NOT BE HARDCODED

        int secondaryProveItCodeEventID = int.Parse(System.Configuration.ConfigurationManager.AppSettings["201204TriPacketteChowSampleFormCapEventID2"]);
        string secondaryProveItCodeEventCode = "biore_us_201204_TriPackette_Chow_Samp_GotPvItCd"; //TODO: THIS SHOULD NOT BE HARDCODED
        string secondaryProveItCodeOpenEnded = "Biore TriPackette Sample April Chow Media- Got ProveIt Code";  //TODO: THIS SHOULD NOT BE HARDCODED

        // THIS proveItCampaignId NEEDS TO BE UPDATED FOR EACH UNIQUE PROVEIT CAMPAIGN ID
        int proveItCampaignId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["201204TriPacketteChowSampleFormProveItCampaignId"]);

        string confirmMsgQualifiedForSampleAndHasProveItCode = "<div class='ConfrimationContainer'><h2>It's Time for a Spring Clean-You're Getting a Sample!</h2><p>Look for your FREE sample* of 3 Bior&eacute;<sup>&reg;</sup> Skincare products coming in the mail soon!</p><p>Try them all on us-then come back and tell us what you think.</p><p>In addition, you have earned a Bonus Code to start earning rewards. Join Prove It!&trade; Rewards Points on Facebook to earn points towards free samples, coupons and more!</p><p>Enter your exclusive Prove It!&trade; Rewards Points Bonus Code to earn even more!</p><p>Your Code: <span style='color:#CA0077'>[PLACEHOLDER_FOR_PROVEIT_CODE]</span></p><p>Once you have copied the code, log into the Prove It!&trade; Reward Points application and scroll down to the \"Enter Codes\" section. You can enter the code into the \"Product Code\" section on the left.</p><p><a href='http://www.facebook.com/bioreskin?sk=app_205787372796203' target'_proveItWindow'>Start Earning Now</a></p></div>";
        string confirmMsgQualifiedForSampleAndNoProveItCode = "<div class='ConfrimationContainer'><h2>It's Time for a Spring Clean-You're Getting a Sample!</h2><p>Look for your FREE sample* of 3 Bior&eacute;<sup>&reg;</sup> Skincare products coming in the mail soon!</p><p>Try them all on us-then come back and tell us what you think.</p></div>";
        string confirmMsgNotQualifiedForSampleButHasProveItCode = "<div class='ConfrimationContainer'><h2>Thanks!</h2><p>We're Sorry. You have not qualified for a sample—But use your Bonus Code to start earning rewards!</p><p>Join Prove It!&trade; Rewards Points on Facebook to earn points towards free samples, coupons and more! Or, if you're already a member, enter your exclusive Prove It!&trade; Rewards Points Bonus Code to earn even more!</p><p>Your Code: <span style='color:#CA0077'>[PLACEHOLDER_FOR_PROVEIT_CODE]</span></p><p>Once you have copied the code, log into the Prove It!&trade; Reward Points application and scroll down to the \"Enter Codes\" section. You can enter the code into the \"Product Code\" section on the left.</p><p><a href='http://www.facebook.com/bioreskin?sk=app_205787372796203' target'_proveItWindow'>Start Earning Now</a></p></div>";
        string confirmMsgNotQualifiedForSampleAndNoProveItCode = "<div class='ConfrimationContainer'><h2>Thanks!</h2><p>We're Sorry. You have not qualified for a sample. We appreciate your interest. If you would like to be notified of future offers and promotions, <a href='/en-US/email-newsletter-sign-up'>click here</a>.</p></div>";

        string confirmMsgQualifiedForSampleDisclaimer = "<p>*Please allow 6-8 weeks for delivery. Offer open to legal residents of all (50) U.S. States, including D.C. One offer per person, per household.</p><p>&nbsp;</p>";
        /****************************************************************************/
        /****************************************************************************/

        protected void Page_Load(object sender, EventArgs e)
        {
            /***********************************************/
            /* START - PART THAT WAS COPIED FROM form.aspx */
            Page.Header.DataBind();

            // Grab masterpage, set appropriate body classes
            Site myMaster = (Site)this.Master;
            myMaster.bodyClass += " forms";

            // hide floodlight pixels for all form pages
            myMaster.FindControl("floodlightPixels").Visible = false;

            if (Request.Url.ToString().Contains("email-newsletter-sign-up"))
            {
                myMaster.bodyClass += " signUp";
                // show floodlight pixels for sign me up page
                myMaster.FindControl("floodlightPixels").Visible = true;
            }
            else if (Request.Url.ToString().Contains("contact-us")) myMaster.bodyClass += " contactUs";


            // This turns on and off the Question Pro Include for this page...
            ((Panel)Page.Master.FindControl("panel_Question_Pro")).Visible = false;

            if (Request.QueryString["promo"] != null)
            {
                string promoID = Request.QueryString["promo"].ToString().ToLower();
                bool promoMatch = false; // LOOKS FOR PROMO MATCH TO DETERMINE IF ALLOW OR NOT
                //if (promoID.Contains("biore_us_201108_walmartcoup")) { promoMatch = true; }
                //if (promoID.Contains("biore_us_201108_newlooktargetendcap")) { promoMatch = true; }
                //if (promoID.Contains("biore_us_201111_freshfanporespectives")) { promoMatch = true; }

                if (!promoID.Equals(mainSubmitFormUniquePromoID.ToLower())) { promoMatch = true; }

                if (promoMatch)
                {
                    Response.Redirect("~/404/");
                }
            }
            /* END - PART THAT WAS COPIED FROM form.aspx */
            /***********************************************/


        }

        protected void doCustomMaxCapCheck()
        {

            /* CUSTOM CHECK FOR PAST MAX CAP - USES DIFFERENT EVENT THAN THE ONE PASSED IN BY promo */

            KAOForms.MaxEntries customMaxCapMaxEntries1 = new MaxEntries();
            customMaxCapMaxEntries1.CapCount = customMaxCapMaxEntriesCount1;
            customMaxCapMaxEntries1.isCapped = "true";

            KAOForms.MaxEntries customMaxCapMaxEntries2 = new MaxEntries();
            customMaxCapMaxEntries2.CapCount = customMaxCapMaxEntriesCount2;
            customMaxCapMaxEntries2.isCapped = "true";

            FormUtils fUtils = new FormUtils();

            //Check if Max Entries have been reached
            if (fUtils.MaxCapLimitExceeded(customMaxCapMaxEntries1, customMaxCapCheckEventID1))
            {
                //bool hasSecondaryID = fUtils.hasSecondaryID(oFormConfigValues.MaxEntriesData);
                //if (hasSecondaryID)
                //{
                //    //if the secondary ID does not match the currentID (to prevent accidental endless loop)
                //    if (oFormConfigValues.UniquePromoId.ToLower() != oFormConfigValues.MaxEntriesData.SecondaryPromoId.ToLower())
                //    {
                //        string url = fUtils.buildSecondaryIDUrl(Request, oFormConfigValues.MaxEntriesData.SecondaryPromoId);
                //        Response.Redirect(url);
                //        return;
                //    }
                //}
                //else
                //{
                pastCustomMaxCapFlag = true; // Page_LoadComplete will update the message
                return;
                //}
            }
            // DO SECOND CHECK FOR MAX CAP
            else if (fUtils.MaxCapLimitExceeded(customMaxCapMaxEntries2, customMaxCapCheckEventID2))
            {
                pastCustomMaxCapFlag = true; // Page_LoadComplete will update the message
                return;
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            /******* ADDING IN THIS CustomMaxCapCheck ***********/
            doCustomMaxCapCheck();

            if (pastCustomMaxCapFlag)
            {
                Literal descText = (Literal)ucForm.FindControl("LitDescriptionText");
                descText.Text = customMaxCapReachedMessage;

                // HIDE/DISABLE THE FORM PANEL
                Panel panelForm = (Panel)ucForm.FindControl("PanelForm");
                panelForm.Visible = false;
            }
            else if (Page.IsPostBack && Page.IsValid)
            {
                //check explicitly for "CUSTOM_REPLACE_CONFIRMATION_TEXT" - There may be a better way to do this
                Literal descText = (Literal)ucForm.FindControl("LitDescriptionText");
                if (!string.IsNullOrEmpty(descText.Text) && descText.Text.Contains("CUSTOM_REPLACE_CONFIRMATION_TEXT"))
                {
                    setCustomPostBackState();
                    return;
                }
                else
                {
                    // MUST HAVE BEEN AN ERROR SO LEAVE AS IS SINCE ucFormConfig WOULD HAVE TAKEN CARE OF IT
                }
            }
        }

        private void setCustomPostBackState()
        {
            /* TODO: MAKE THESE CHECKS MORE ROBUST BY CHECKING FOR NULLS */

            /*
             * NEED TO SHOW EITHER:
             * 1) QUALIFIED FOR SAMPLE        AND GETS PROVE IT CODE
             * 2) QUALIFIED FOR SAMPLE        AND NO PROVE IT CODE (NO MORE CODES LEFT OR OTHER PROVE IT CODE ERROR)
             * 3) NOT QUALIFIED FOR SAMPLE    BUT GETS PROVE IT CODE
             * 4) NOT QUALIFIED FOR SAMPLE    AND NO PROVE IT CODE (NO MORE CODES LEFT OR OTHER PROVE IT CODE ERROR)
             */

            FormLocalUtils sUtils = new FormLocalUtils();
            Literal descText = (Literal)ucForm.FindControl("LitDescriptionText");

            ucFormMemberInfo = (KaoBrands.FormStuff.ucFormMemberInfo)ucForm.FindControl("ucFormMemberInfo");
            string month = ((DropDownList)ucFormMemberInfo.FindControl("mm")).SelectedValue;
            string day = ((DropDownList)ucFormMemberInfo.FindControl("dd")).SelectedValue;
            string year = ((DropDownList)ucFormMemberInfo.FindControl("yyyy")).SelectedValue;
            string gender = ((DropDownList)ucFormMemberInfo.FindControl("Gender")).SelectedValue;

            Panel cQuestions = (Panel)ucForm.FindControl("QuestionPanel");
            string skintype = ((DropDownList)sUtils.findControlRecursive(cQuestions, "mq10")).SelectedValue;

            // CALCULATE AGE
            int age = -1;
            bool ageIsValid = false;
            string dob = month + "/" + day + "/" + year;
            try
            {
                DateTime DoB = DateTime.Parse(dob);
                age = calculateAgeInYears(DoB, DateTime.Now);
                ageIsValid = true;
            }
            catch
            {
                ageIsValid = false;
            }

            FormSubmitBLL FormSubmit = new FormSubmitBLL();
            //Set Default Config Objects
            oDefaultValues = DefaultFormDAO.GetDefaultValues();
            int currentSiteID = oDefaultValues.CurrentSiteID;

            oFormConfigValues = FormConfigDAO.GetFormByUniquePromoId(mainSubmitFormUniquePromoID);

            SiteData currentSiteData = FormSubmit.getCurrenSiteData(currentSiteID, oDefaultValues.BrandSiteData);
            MemberData memberData = getMemberInfo();

            bool qualifiedForSample = false;
            bool gettingProveItCode = false;
            spStatus myStatus = new spStatus();

            /*****************************************************************************/
            /* DO THEY QUALIFY FOR SAMPLE? Female 18-39yrs old and NON-Normal Skin Type  -- WAS -- Female 20-29yrs old and NON-Normal Skin Type */
            /*****************************************************************************/
            if (ageIsValid && age >= 18 && age < 39 && gender.ToLower().Equals("female") && !skintype.ToLower().Equals("normal"))
            {
                //Submit Event - secondarySampleEvent
                myStatus = FormSubmit.submitMemberData(currentSiteID, false, secondarySampleEventID, secondarySampleEventCode, 0, 0, secondarySampleOpenEnded, currentSiteData, memberData);
                //TODO: NEED TO CONFIRM STATUS WAS SUCCESSFUL
                qualifiedForSample = true;
            }

            /*******************************************/
            // NOW GET PROVE IT CODE FOR THIS USER
            /*******************************************/
            ProveItCode myProveItCode = Biore2012.BLL.ProveItCodeDAO.GetProveItCode(ucForm.memberId, proveItCampaignId);
            if (!myProveItCode.proveItCode.Equals("")) //TODO: BETTER WAY TO CHECK FOR BLANK STRING?
            {
                //Submit Event - secondarySampleEvent
                myStatus = FormSubmit.submitMemberData(currentSiteID, false, secondaryProveItCodeEventID, secondaryProveItCodeEventCode, 0, 0, secondaryProveItCodeOpenEnded, currentSiteData, memberData);
                //TODO: NEED TO CONFIRM STATUS WAS SUCCESSFUL //descText.Text += "<br />myProveItCode=" + myProveItCode.proveItCode + " " + myProveItCode.returnStatus + " " + myProveItCode.returnStatusMessage;
                gettingProveItCode = true;
            }


            /*
             * DETERMINE WHAT TO SHOW:
             * 1) QUALIFIED FOR SAMPLE        AND GETS PROVE IT CODE
             * 2) QUALIFIED FOR SAMPLE        AND NO PROVE IT CODE (NO MORE CODES LEFT OR OTHER PROVE IT CODE ERROR)
             * 3) NOT QUALIFIED FOR SAMPLE    BUT GETS PROVE IT CODE
             * 4) NOT QUALIFIED FOR SAMPLE    AND NO PROVE IT CODE (NO MORE CODES LEFT OR OTHER PROVE IT CODE ERROR)
             */
            if (qualifiedForSample && gettingProveItCode)
            {
                descText.Text = confirmMsgQualifiedForSampleAndHasProveItCode;
            }
            else if (qualifiedForSample && !gettingProveItCode)
            {
                descText.Text = confirmMsgQualifiedForSampleAndNoProveItCode;
            }
            else if (!qualifiedForSample && gettingProveItCode)
            {
                descText.Text = confirmMsgNotQualifiedForSampleButHasProveItCode;
            }
            else
            {
                descText.Text = confirmMsgNotQualifiedForSampleAndNoProveItCode;
            }

            if (gettingProveItCode)
            {
                /* PUT IN THIS SPECIFIC USER'S PROVEIT CODE */
                descText.Text = descText.Text.Replace("[PLACEHOLDER_FOR_PROVEIT_CODE]", myProveItCode.proveItCode);
            }

            if (qualifiedForSample)
            {
                /* PUT IN THIS SAMPLE DISCLAIMER COPY */
                Literal disclaimerText = (Literal)ucForm.FindControl("LitDisclaimer");
                disclaimerText.Text = confirmMsgQualifiedForSampleDisclaimer + disclaimerText.Text;
            }

        }

        /* THIS METHOD getMemberInfo() WAS COPIED FROM THE CONTROL ucFormConfig.ascx */
        private MemberData getMemberInfo()
        {
            string DOB = string.Empty;
            if (oFormConfigValues.OptionalFields.DOBVisible.ToLower() == "true")
            {
                DOB = ucFormMemberInfo._ddMonth.SelectedValue + "/" + ucFormMemberInfo._ddDay.SelectedValue + "/" + ucFormMemberInfo._ddYear.SelectedValue;

            }
            else
            {
                DOB = "empty";
            }
            MemberData mData = new MemberData();
            mData.FirstName = ucFormMemberInfo._txFirstName.Text;
            mData.LastName = ucFormMemberInfo._txLastName.Text;
            mData.Email = ucFormMemberInfo._txEmail.Text;
            mData.Gender = ucFormMemberInfo._ddGender.SelectedValue;
            mData.DOB = DOB;
            mData.Address1 = ucFormMemberInfo._txAddress1.Text;
            mData.Address2 = ucFormMemberInfo._txAddress2.Text;
            mData.Address3 = ucFormMemberInfo._txAddress3.Text;
            mData.City = ucFormMemberInfo._txCity.Text;
            mData.State = ucFormMemberInfo._ddState.SelectedValue;
            mData.PostalCode = ucFormMemberInfo._txPostalCode.Text;
            mData.Country = "United States";
            mData.Phone = ucFormMemberInfo._txPhone.Text;
            mData.QuestionComment = ucFormMemberInfo._txComment.Text;
            return mData;
        }

        /* THIS METHOD calculateAgeInYears(...) WAS COPIED FROM ucFormMemberInfo.ascx */
        protected int calculateAgeInYears(DateTime dateOfBirth, DateTime dateToCalculateAge)
        {
            int ageCalculation = -1;
            if (dateOfBirth != null && dateToCalculateAge != null)
            {
                int DoB_DayOfYear = dateOfBirth.DayOfYear;
                int DateNow_DayOfYear = dateToCalculateAge.DayOfYear;

                // Adjust DayOfYear values to allow for Leap Year comparison - e.g. make March 1st always Day 61 in this calculation
                if (!DateTime.IsLeapYear(dateOfBirth.Year) && dateOfBirth.Month > 2) DoB_DayOfYear++;
                if (!DateTime.IsLeapYear(dateToCalculateAge.Year) && dateOfBirth.Month > 2) DateNow_DayOfYear++;

                ageCalculation = dateToCalculateAge.Year - dateOfBirth.Year;
                if (DoB_DayOfYear > DateNow_DayOfYear)
                {
                    // Have not had their birthday yet
                    ageCalculation--;
                }

            }
            return ageCalculation;
        }
    }
}
