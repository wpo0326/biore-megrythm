﻿<%@ Page Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="Biore2012.FormConfig.ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%#SquishIt.Framework.Bundle .Css()
        .Add("~/css/formConfig.css")
        .Render("~/css/combinedform_#.css")
    %>
</asp:Content>

<asp:Content ID="floodlightPixelsContent" ContentPlaceHolderID="floodlightPixels" runat="server" Visible="false">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <div class="responsive contact">
                    <div id="contactFormWrap">
                        <div id="BrandImageContainer" class="png-fix">
                            <img class="png-fix" src="images/forms/contactUsPhoto.jpg" style="border-width: 0px;" />
                        </div>
                        <asp:Panel ID="ContactForm" runat="server">
                            <div id="ContactFormContainer">
                                <!-- Header -->
                                <div id="formHeader">
<h1 id="ctl00_ContentPlaceHolder1_ucForm_PageHeader">
                                        Contact Us
                                    </h1>
                                </div>

                                <!-- Description -->
                                <asp:Panel ID="DescriptionContainer" CssClass="DescriptionContainer png-fix" runat="server">
                                    <div class="content upperContent png-fix">
                                        

<p>Do you have a specific question or a comment?<br />
                                    We'd love to hear from you!</p>
                                    <br />


                                    </div>
                                    <!--<div class="bottom2 png-fix"></div>
                                    <div class="top2 png-fix"></div>
                                    <div class="content png-fix">
                                        <h2>ONLINE</h2>
                                        <p>Fill out the form below.</p>
                                    </div>-->

                                </asp:Panel>


                                <!-- From Fields -->
                                <asp:Panel ID="PanelForm" CssClass="FormContainer" runat="server">
                                    <p class="req png-fix"><em>Required*</em></p>
                                    <asp:Panel ID="PanelMemberInfo" CssClass="MemberInfoContainer png-fix" runat="server">
                                        <div class="NameWrapper">
                                            <div class="FirstNameContainer Question">
                                                <asp:Label ID="FNameLbl" runat="server" Text="First Name*" AssociatedControlID="FName"></asp:Label>
                                                <asp:TextBox ID="FName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="FNameValidator1" runat="server" Display="Dynamic"
                                                        ErrorMessage="Please enter your First Name." ControlToValidate="FName" EnableClientScript="true"
                                                        SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="ErrorContainer">
                                                    <asp:RegularExpressionValidator ID="FNameValidator2" runat="server" Display="Dynamic"
                                                        ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your First Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="FName" EnableClientScript="true"
                                                        SetFocusOnError="true" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                                                    ErrorMessage="Please limit the entry to 50 characters."
                                                    ValidationExpression="^[\s\S]{0,50}$" ControlToValidate="FName" SetFocusOnError="true"
                                                    CssClass="errormsg"></asp:RegularExpressionValidator>

                                            </div>
                                            <div class="LastNameContainer Question">
                                                <asp:Label ID="LNameLbl" runat="server" Text="Last Name*" AssociatedControlID="LName"></asp:Label>
                                                <asp:TextBox ID="LName" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="LNameValidator1" runat="server" Display="Dynamic"
                                                        ErrorMessage="Please enter your Last Name." ControlToValidate="LName" EnableClientScript="true"
                                                        SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="ErrorContainer">
                                                    <asp:RegularExpressionValidator ID="LNameValidator2" runat="server" Display="Dynamic"
                                                        ErrorMessage="The characters '>' and '<' are not permitted. Please re-enter your Last Name."
                                                        ValidationExpression="^[^<>]+$" ControlToValidate="LName" SetFocusOnError="true"
                                                        CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                                <div class="ErrorContainer">
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="Dynamic"
                                                        ErrorMessage="Please limit the entry to 50 characters."
                                                        ValidationExpression="^[\s\S]{0,50}$" ControlToValidate="LName" SetFocusOnError="true"
                                                        CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="EmailWrapper">
                                            <div class="EmailContainer Question">
                                                <asp:Label ID="EmailLbl" runat="server" Text="Email*" AssociatedControlID="Email"></asp:Label>
                                                <asp:TextBox ID="Email" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="EmailValidator1" runat="server" Display="Dynamic"
                                                        ErrorMessage="Please enter your Email Address." ControlToValidate="Email" EnableClientScript="true"
                                                        SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="ErrorContainer">
                                                    <asp:RegularExpressionValidator ID="EmailValidator2" runat="server" Display="Dynamic"
                                                        ErrorMessage="Please enter a valid Email Address." ValidationExpression="^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
                                                        ControlToValidate="Email" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                                <div class="ErrorContainer">
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Display="Dynamic"
                                                        ErrorMessage="Please limit the entry to 150 characters."
                                                        ValidationExpression="^[\s\S]{0,150}$" ControlToValidate="Email" SetFocusOnError="true"
                                                        CssClass="errormsg"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <asp:Panel ID="PanelConfirmEmail" CssClass="ConfirmEmailContainer Question" runat="server">
                                                <asp:Label ID="ConfirmEmailLbl" runat="server" Text="Confirm Email*" AssociatedControlID="Email"></asp:Label>
                                                <asp:TextBox ID="ConfirmEmail" runat="server" MaxLength="150" CssClass="inputTextBox"></asp:TextBox>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                                        ErrorMessage="Please confirm your Email Address." ControlToValidate="ConfirmEmail" EnableClientScript="true"
                                                        SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="ErrorContainer">
                                                    <asp:CompareValidator ID="CompareEmailValidator" runat="server"
                                                        ErrorMessage="The email values do not match" EnableClientScript="true"
                                                        ControlToCompare="Email" ControlToValidate="ConfirmEmail"
                                                        Display="Dynamic" CssClass="errormsg" SetFocusOnError="true"></asp:CompareValidator>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                        <div class="seperator png-fix"></div>


                                        <asp:Panel ID="PanelAddress" CssClass="AddressContainer" runat="server">
                                            <div class="AddressWrapper">
                                                <div class="Address1Container Question">
                                                    <asp:Label ID="Address1Lbl" runat="server" Text="Address 1*" AssociatedControlID="Address1"></asp:Label>
                                                    <asp:TextBox ID="Address1" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="Address1Validator1" runat="server" Display="Dynamic"
                                                            ErrorMessage="Please enter your Address." ControlToValidate="Address1" EnableClientScript="true"
                                                            SetFocusOnError="true" CssClass="errormsg" />
                                                    </div>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="Address1Validator2" runat="server" Display="Dynamic"
                                                            ErrorMessage="Please do not use special characters in the Address 1 field." ValidationExpression="^[^<>]+$"
                                                            ControlToValidate="Address1" EnableClientScript="true" SetFocusOnError="true"
                                                            CssClass="errormsg" />
                                                    </div>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" Display="Dynamic"
                                                            ErrorMessage="Please limit the entry to 50 characters."
                                                            ValidationExpression="^[\s\S]{0,50}$" ControlToValidate="Address1" SetFocusOnError="true"
                                                            CssClass="errormsg"></asp:RegularExpressionValidator>
                                                    </div>
                                                </div>
                                                <div class="Address2Container Question">
                                                    <asp:Label ID="Address2Lbl" runat="server" Text="Address 2" AssociatedControlID="Address2"></asp:Label>
                                                    <asp:TextBox ID="Address2" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="Address2Validator1" runat="server" Display="Dynamic"
                                                            ErrorMessage="Please do not use special characters in the Address 2 field." ValidationExpression="^[^<>]+$"
                                                            ControlToValidate="Address2" EnableClientScript="true" SetFocusOnError="true"
                                                            CssClass="errormsg" />
                                                    </div>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" Display="Dynamic"
                                                            ErrorMessage="Please limit the entry to 50 characters."
                                                            ValidationExpression="^[\s\S]{0,50}$" ControlToValidate="Address2" SetFocusOnError="true"
                                                            CssClass="errormsg"></asp:RegularExpressionValidator>
                                                    </div>
                                                </div>
                                                <div class="Address3Container Question">
                                                    <asp:Label ID="Address3Lbl" runat="server" Text="Address 3" AssociatedControlID="Address3"></asp:Label>
                                                    <asp:TextBox ID="Address3" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="Address3Validator1" runat="server" Display="Dynamic"
                                                            ErrorMessage="Please do not use special characters in the Address 3 field." ValidationExpression="^[^<>]+$"
                                                            ControlToValidate="Address3" EnableClientScript="true" SetFocusOnError="true"
                                                            CssClass="errormsg" />
                                                    </div>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" Display="Dynamic"
                                                            ErrorMessage="Please limit the entry to 50 characters."
                                                            ValidationExpression="^[\s\S]{0,50}$" ControlToValidate="Address3" SetFocusOnError="true"
                                                            CssClass="errormsg"></asp:RegularExpressionValidator>
                                                    </div>
                                                </div>
                                                <div class="CityContainer Question">
                                                    <asp:Label ID="CityLbl" runat="server" Text="City*" AssociatedControlID="City"></asp:Label>
                                                    <asp:TextBox ID="City" runat="server" MaxLength="50" CssClass="inputTextBox"></asp:TextBox>
                                                    <div class="ErrorContainer">
                                                        <asp:RequiredFieldValidator ID="CityValidator1" runat="server" Display="Dynamic"
                                                            ErrorMessage="Please enter your city." ControlToValidate="City" EnableClientScript="true"
                                                            SetFocusOnError="true" CssClass="errormsg" />
                                                    </div>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="CityValidator2" runat="server" Display="Dynamic"
                                                            ErrorMessage="Please do not use special characters in the City field." ValidationExpression="^[^<>]+$"
                                                            ControlToValidate="City" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg" />
                                                    </div>
                                                    <div class="ErrorContainer">
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" Display="Dynamic"
                                                            ErrorMessage="Please limit the entry to 50 characters."
                                                            ValidationExpression="^[\s\S]{0,50}$" ControlToValidate="City" SetFocusOnError="true"
                                                            CssClass="errormsg"></asp:RegularExpressionValidator>
                                                    </div>
                                                </div>
                                            </div>

                                        </asp:Panel>

                                        <asp:Panel ID="PanelPostalCode" CssClass="PostalCodeContainer Question" runat="server">
                                            <asp:Label ID="PostalCodeLbl" runat="server" Text="Postal Code*" AssociatedControlID="PostalCode"></asp:Label>
                                            <asp:TextBox ID="PostalCode" runat="server" MaxLength="20" CssClass="inputTextBox"></asp:TextBox>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="PostalCodeValidator1" runat="server" Display="Dynamic"
                                                    ErrorMessage="Please enter your Postal Code" ControlToValidate="PostalCode" EnableClientScript="true"
                                                    SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                                            </div>

                                        </asp:Panel>

                                        <asp:Panel ID="PanelCoutnry" CssClass="CountryContainer Question" runat="server">
                                            <div class="CountryContainer Question">
                                                <asp:Label ID="Label1" runat="server" Text="Country*" AssociatedControlID="Country"></asp:Label>
                                                <asp:DropDownList ID="Country" runat="server">
                                                    <asp:ListItem Value="">Select Country</asp:ListItem>
                                                    <asp:ListItem Value="BAHR">Bahrain</asp:ListItem>
                                                    <asp:ListItem Value="JORD">Jordan</asp:ListItem>
                                                    <asp:ListItem Value="KUWA">Kuwait</asp:ListItem>
                                                    <asp:ListItem Value="OMAN">Oman</asp:ListItem>
                                                    <asp:ListItem Value="QATA">Qatar</asp:ListItem>
                                                    <asp:ListItem Value="SAAR">Saudi Arabia</asp:ListItem>
                                                    <asp:ListItem Value="UAE">United Arab Emirates</asp:ListItem>

                                                </asp:DropDownList>
                                                <div class="ErrorContainer">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please select your Country."
                                                        ControlToValidate="Country" EnableClientScript="true" SetFocusOnError="true"
                                                        Display="Dynamic" CssClass="errormsg"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <div class="seperator png-fix"></div>


                                        <asp:Panel ID="PanelGender" CssClass="GenderContainer Question" runat="server">
                                            <asp:Label ID="GenderLbl" runat="server" Text="Gender" AssociatedControlID="Gender"></asp:Label>
                                            <asp:DropDownList ID="Gender" runat="server">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="Female">Female</asp:ListItem>
                                                <asp:ListItem Value="Male">Male</asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="GenderValidator" runat="server" Display="Dynamic"
                                                    ErrorMessage="Please enter your Gender." ControlToValidate="Gender" EnableClientScript="true"
                                                    SetFocusOnError="true" CssClass="errormsg"></asp:RequiredFieldValidator>
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel ID="PanelDOB" CssClass="BirthdayContainer Question" runat="server">
                                            <asp:Label ID="DOBLbl" runat="server" Text="Birthdate*" AssociatedControlID="yyyy"></asp:Label>
                                            <asp:DropDownList ID="yyyy" runat="server" CssClass="inputSelectBDay year"></asp:DropDownList>
                                            <asp:DropDownList ID="mm" runat="server" CssClass="inputSelectBDay month"></asp:DropDownList>
                                            <asp:DropDownList ID="dd" runat="server" CssClass="inputSelectBDay day"></asp:DropDownList>



                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="ddValidator2" runat="server" Display="Dynamic" ErrorMessage="Please enter your Birth Day."
                                                    ControlToValidate="dd" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg day"></asp:RequiredFieldValidator>
                                            </div>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="mmValidator1" runat="server" Display="Dynamic" ErrorMessage="Please enter your Birth Month."
                                                    ControlToValidate="mm" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg month"></asp:RequiredFieldValidator>
                                            </div>
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="yyyyValidator3" runat="server" Display="Dynamic" ErrorMessage="Please enter your Birth Year."
                                                    ControlToValidate="yyyy" EnableClientScript="true" SetFocusOnError="true" CssClass="errormsg year"></asp:RequiredFieldValidator>
                                            </div>
                                            <div class="ErrorContainer">
                                                <asp:CustomValidator ID="DOBValidator4" runat="server" Display="Dynamic" ErrorMessage="Please enter a valid date."
                                                    OnServerValidate="DOBValidate" EnableClientScript="false" CssClass="errormsg"></asp:CustomValidator>
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel ID="PanelPhone" CssClass="PhoneContainer Question" runat="server">
                                            <asp:Label ID="PhoneLbl" runat="server" Text="Phone" AssociatedControlID="Phone"></asp:Label>
                                            <asp:TextBox ID="Phone" MaxLength="50" runat="server" CssClass="inputTextBox" />
                                            <div class="ErrorContainer">
                                                <asp:RegularExpressionValidator ID="PhoneValidator1" runat="server" ErrorMessage="Your Phone is not required, but please do not use special characters in the Phone field."
                                                    ValidationExpression="^[^<>]+$" ControlToValidate="Phone" EnableClientScript="true"
                                                    SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                            </div>
                                            <div class="ErrorContainer">
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" Display="Dynamic"
                                                    ErrorMessage="Please limit the entry to 50 characters."
                                                    ValidationExpression="^[\s\S]{0,50}$" ControlToValidate="Phone" SetFocusOnError="true"
                                                    CssClass="errormsg"></asp:RegularExpressionValidator>
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel ID="PanelComment" CssClass="CommentContainer Question" runat="server">
                                            <asp:Label ID="QuestionCommentLbl" runat="server" Text="Question / Comment*"
                                                AssociatedControlID="QuestionComment"></asp:Label>
                                            <asp:TextBox ID="QuestionComment" runat="server" TextMode="MultiLine" Rows="4" Columns="5" />
                                            <div class="ErrorContainer">
                                                <asp:RequiredFieldValidator ID="QuestionCommentValidator" runat="server"
                                                    ErrorMessage="Please complete: Question / Comment." ControlToValidate="QuestionComment"
                                                    EnableClientScript="true" SetFocusOnError="true" Display="Dynamic" CssClass="errormsg questioncomment" />
                                            </div>
                                            <div class="ErrorContainer">
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" Display="Dynamic"
                                                    ErrorMessage="Please limit the entry to 5000 characters."
                                                    ValidationExpression="^[\s\S]{0,5000}$" ControlToValidate="QuestionComment" SetFocusOnError="true"
                                                    CssClass="errormsg"></asp:RegularExpressionValidator>
                                            </div>
                                            <div class="ErrorContainer">
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ErrorMessage="The characters '>' and '<' are not permitted."
                                                    ValidationExpression="^[^<>]+$" ControlToValidate="QuestionComment" EnableClientScript="true"
                                                    SetFocusOnError="true" Display="Dynamic" CssClass="errormsg" />
                                            </div>
                                        </asp:Panel>

                                    </asp:Panel>
                                    <div class="OptinContainer">
                                        <div class="seperator png-fix"></div>
                                        <p class="privacy">
                                            <span>Your privacy is important to us.</span> You can trust that we will
		                    use personal information in accordance with our <a href="http://www.kaobrands.com/privacy_policy.asp"
                                target="_blank">Privacy Policy</a>.
                                        </p>

                                        <div class="CurrentSiteOptinContainer" style="display: none;">
                                            <ul>
                                                <li>
                                                    <asp:CheckBox ID="jergensoptin" runat="server" />
                                                    <asp:Label ID="jergensoptinLabel" runat="server" Text="Yes, tell me about future Jergens<sup>&reg;</sup> product news and offerings." AssociatedControlID="jergensoptin" CssClass="siteOptinChkbox" />


                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <asp:HiddenField ID="hfHookID" runat="server" />
                                    <div id="submit-container" class="SubmitContainer png-fix">
                                        <asp:Button UseSubmitBehavior="false" ID="submit" Text="Submit" runat="server" CssClass="submit buttonLink png-fix" />
                                    </div>
                                </asp:Panel>

                                <div class="FormBottom png-fix"></div>

                                <!-- Disclaimer -->
                                <div id="DisclaimerContainer" class="png-fix">
                                    <asp:Literal ID="LitDisclaimer" runat="server"></asp:Literal>
                                </div>


                            </div>
                        </asp:Panel>
                        <asp:Panel ID="ContactFormSuccess" runat="server">
                            <h1>Contact Us</h1>
                            <div id="contactSuccess" style="padding: 10px 90px 0px 20px">
                                <h2>Thank You!</h2>
                                <p>
                                    Thank you for your contact. A member of our Consumer Relations team will be in contact
                        as soon as possible. If you would like to sign up to receive information about Jergens<sup>&reg;</sup> products, <a href="sign-up/optin.aspx">click here</a>.
                                </p>
                            </div>

                        </asp:Panel>
                        <asp:Panel ID="ContactFormFailure" runat="server">
                            <h1>Contact Us</h1>
                            <div id="contactFailure" style="padding: 10px 90px 0px 20px">
                                <h2>We're Sorry...</h2>
                                <p>
                                    There has been a problem with your form submission. Please <a href="javascript: back();">try again</a>.
                                </p>
                                <asp:Literal ID="litError" runat="server"></asp:Literal>
                            </div>

                        </asp:Panel>
                    </div>

                    <script type="text/javascript">

                    </script>

                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">
    <script type="text/javascript">
        $(function () {
            if ($(".signUp").length) {
                toggle("noneToggle1");
                toggle("noneToggle2");
                toggle("noneToggle3");
                toggle("noneToggle4");
            }
        });
        function toggle(className) {
            var checkToggle = $("." + className + " input:last");
            checkToggle.click(function () {
                if ($("#" + checkToggle.attr('id') + ":checked").val()) {
                    $("." + className + " input").each(function () {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).attr("checked", false);
                            $(this).attr('disabled', 'disabled');
                        }
                    });
                } else {
                    $("." + className + "  input").each(function () {
                        if ($(this).attr('id') != checkToggle.attr('id')) {
                            $(this).removeAttr('disabled');
                        }
                    });
                }
            });
        }
    </script>
</asp:Content>
