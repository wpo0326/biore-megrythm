﻿<%@ Page Title="About Bioré | Bioré" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.about_us._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Leer meer over Bioré® Huidverzorging " name="description" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/about.css")
        .Render("~/css/combinedabout_#.css")
    %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent" class="aboutBlueBG">
            <div class="centeringDiv">
                <div id="content" class="aboutBlueTxt">
                    <div class="titleh1">
                        <h1>About Bioré</h1>
                        <div id="responseRule"></div>
                    </div>
                    <div class="about-info">     
                        
                        <p><span>Discover Bioré – the expert for free pores!</span></p>

                        <p>Bioré skincare targets the root of skin problems – the clogged pore. Bioré products with baking soda, known for its deep cleansing power, and charcoal, known for its absorbing effect, remove dirt, oil and sebum from the pores to help fight blackheads.</p> 

<p>We fight smartly. Our Gels, Scrubs, Powder and Pore Strips clean deep into the pore to fight impurities at the source. For clean skin and free pores.</p>  


                        <p><span>Bioré – Free Your Pores.</span></p>
                                           
                       
                    </div>
                    <img src="../images/about/aboutBG.png" alt="" id="Img1" />                    
                </div>
            </div>
        </div>
    </div>
</asp:Content>

