﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Biore2012.buy_now
{
    public partial class BuyNow : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

#if DEBUG
        XmlDataSource1.DataFile = "/biore-facial-cleansing-products/xml/buynow.xml";
#endif

            XmlDataSource1.XPath = string.Format("Products/Product[ProductRefName='{0}']", Request.QueryString["RefName"]);
            XmlDataSource1.DataBind();
        }

        protected void OnItemDataBound(object sender, ListViewItemEventArgs e)
        {
#if DEBUG
            Image hlImage1 = (Image)e.Item.FindControl("Image1");

            string sImageUrl = hlImage1.ImageUrl.Replace("/en-US", "");

            hlImage1.ImageUrl = sImageUrl;
#endif
            return;
        }
         protected   void RepeaterOnItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {

#if DEBUG
            HyperLink hlRetailerLogo = (HyperLink)e.Item.FindControl("RetailerLogo");

            string sImageUrl = hlRetailerLogo.ImageUrl.Replace("/en-US", "");

            hlRetailerLogo.ImageUrl = sImageUrl;
#endif
            return;
        }
        
    }
}