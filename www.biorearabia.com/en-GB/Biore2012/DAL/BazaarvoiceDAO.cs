﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Caching;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Microsoft.Practices.EnterpriseLibrary.Logging;

//Added GetBVAPIFeed() method used to capture ratings and dynamically build into product grid pages - 06/06/12 Mike Taylor, CG Marketing Communications 
//Added GetRatingsByProductID method to generate ratings from XML loaded from GetBVAPIFeed() - 06/06/12 Mike Taylor, CG Marketing Communications
namespace Biore2012.DAL
{
    public class BazaarvoiceDAO
    {
        public static String GetSearchVoiceByProductRef(string ProductRefName)
        {
            string result = string.Empty;
            string fileName = ProductRefName + ".htm";

            //Let's see if its in cache
            string fileContent = (String)HttpContext.Current.Cache["SV" + fileName];
            if (String.IsNullOrEmpty(fileContent))
            {

                string fullpath = System.Configuration.ConfigurationManager.AppSettings["BVSVFilePath"] + fileName;
                //Check if product actually has accompanying searchvoice file
                if (!File.Exists(fullpath))
                {
                    result = "none";
                }
                else
                {
                    try
                    {
                        using (StreamReader sr = File.OpenText(fullpath))
                        {
                            String input;
                            StringBuilder sb = new StringBuilder();
                            while ((input = sr.ReadLine()) != null)
                            {
                                sb.Append(input);
                            }
                            result = sb.ToString();
                        }
                    }
                    catch (Exception e)
                    {
                        LogEntry myLog = new LogEntry();
                        myLog.Message = "Error reading the file contents in BazaarvoiceDAO.GetSearchVoiceByProductRef for " + fullpath + "\n" + e.Message;
                        Logger.Write(myLog);
                        myLog = null;
                        result = "none";
                    }
                    HttpContext.Current.Cache.Insert("SV" + fileName, result, new CacheDependency(fullpath));
                }
            }
            else  //It's in the cache
            {
                result = fileContent;
            }

            return result;
        }

        public static XmlDocument GetBVAPIFeed()
        {
            // build BV feed URL at runtime
            string url = System.Configuration.ConfigurationManager.AppSettings["BVApiFeedURL"];

            // Check if xmlFileName is in cache 
            string strXmlFileName = "../biore-facial-cleansing-products/xml/" + System.Configuration.ConfigurationManager.AppSettings["BVApiFeedFile"];
            XmlDocument BVXMLDoc = (XmlDocument)HttpRuntime.Cache["BVBV" + url];

            if (BVXMLDoc == null)
            {
                //HttpContext.Current.Response.Write("It's null.  The cache is Null");
                try
                {
                    BVXMLDoc = new XmlDocument();

                    WebRequest request = WebRequest.Create(url);
                    try
                    {
                        WebResponse response = request.GetResponse();
                        using (var sr = new System.IO.StreamReader(response.GetResponseStream()))
                        {
                            XDocument xmlDoc = new XDocument();
                            try
                            {
                                //write doc and save it to location
                                xmlDoc = XDocument.Parse(sr.ReadToEnd().Replace(" xmlns=\"http://www.bazaarvoice.com/xs/DataApiQuery/5.1\"", ""));
                                xmlDoc.Save(HttpContext.Current.Server.MapPath(strXmlFileName));
                            }
                            catch (Exception ex)
                            {
                                HttpContext.Current.Response.Write("first catch! " + ex);
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        HttpContext.Current.Response.Write("second catch! " + ex);
                    }


                    // Load into cache for future use 
                    //HttpContext.Current.Cache.Insert(xmlFileName, BVXMLDoc, new CacheDependency(xmlFileName)); 
                    HttpRuntime.Cache.Insert("BVBV" + strXmlFileName, BVXMLDoc, new CacheDependency(HttpContext.Current.Server.MapPath(strXmlFileName)));
                }
                catch (FileNotFoundException fex)
                {
                    // Document wasn't found 
                    BVXMLDoc = null;
                    HttpContext.Current.Response.Write("third catch! " + fex);
                }
            }

            return BVXMLDoc;
        }

        public static XmlDocument GetXMLDocument(string xmlFileName)
        {

            // Check if xmlFileName is in cache 
            XmlDocument XMLDoc = (XmlDocument)HttpRuntime.Cache[xmlFileName];

            if (XMLDoc == null)
            {
                //HttpContext.Current.Response.Write("This is null.");
                try
                {
                    XMLDoc = new XmlDocument();
                    //if (!File.Exists(xmlFileName))
                    //BuildXMLDocument(xmlFileName);

                    StreamReader srDoc = new StreamReader(HttpContext.Current.Server.MapPath(xmlFileName));
                    XMLDoc.Load(srDoc);
                    srDoc.Close();
                    srDoc.Dispose();

                    //XMLDoc.Load(HttpContext.Current.Server.MapPath(xmlFileName));

                    // Load into cache for future use 
                    //HttpContext.Current.Cache.Insert(xmlFileName, XMLDoc, new CacheDependency(xmlFileName)); 
                    HttpRuntime.Cache.Insert(xmlFileName, XMLDoc, new CacheDependency(HttpContext.Current.Server.MapPath(xmlFileName)));
                }
                catch (FileNotFoundException)
                {
                    // Document wasn't found 
                    XMLDoc = null;
                }
            }
            return XMLDoc;
        }

        public static String GetRatingsByProductID(string productID)
        {
            string strReturn = "";

            //load product xml
          
            XmlDocument xmlDoc = GetXMLDocument("../biore-facial-cleansing-products/xml/" + System.Configuration.ConfigurationManager.AppSettings["BVApiFeedFile"]);
           
            if (xmlDoc.HasChildNodes)
            {
                //string strNodeRoot = "DataApiResponse/Includes/Products/Product[@id = '" + productID + "']";

                string strNodeRoot = "DataApiResponse/Results/Product[@id = '" + productID + "']";

                //get the rating.
                double dblRating = 0;
                string strRatingGif = "";

                try
                {
                    dblRating = Convert.ToDouble(xmlDoc.SelectSingleNode(strNodeRoot + "/ReviewStatistics/AverageOverallRating").InnerText.ToString());
                }
                catch {}

                //determine what sort of star image to display based on rating.
                if (dblRating == 0)
                    strRatingGif = "rating_0_0.gif";
                else if (dblRating > 0 && dblRating <= 0.5)
                    strRatingGif="rating_0_25.gif";
                else if (dblRating >= 0.5 && dblRating < 0.75)
                    strRatingGif = "rating_0_5.gif";
                else if (dblRating >= 0.75 && dblRating < 1)
                    strRatingGif = "rating_0_75.gif";
                else if (dblRating == 1)
                    strRatingGif = "rating_1_0.gif";
                else if (dblRating > 1 && dblRating <= 1.5)
                    strRatingGif = "rating_1_25.gif";
                else if (dblRating >= 1.5 && dblRating < 1.75)
                    strRatingGif = "rating_1_5.gif";
                else if (dblRating >= 1.75 && dblRating < 2)
                    strRatingGif = "rating_1_75.gif";
                else if (dblRating == 2)
                    strRatingGif = "rating_2_0.gif";
                else if (dblRating > 2 && dblRating <= 2.5)
                    strRatingGif = "rating_2_25.gif";
                else if (dblRating >= 2.5 && dblRating < 2.75)
                    strRatingGif = "rating_2_5.gif";
                else if (dblRating >= 2.75 && dblRating < 3)
                    strRatingGif = "rating_2_75.gif";
                else if (dblRating == 3)
                    strRatingGif = "rating_3_0.gif";
                else if (dblRating > 3 && dblRating <= 3.5)
                    strRatingGif = "rating_3_25.gif";
                else if (dblRating >= 3.5 && dblRating < 3.75)
                    strRatingGif = "rating_3_5.gif";
                else if (dblRating >= 3.75 && dblRating < 4)
                    strRatingGif = "rating_3_75.gif";
                else if (dblRating == 4)
                    strRatingGif = "rating_4_0.gif";
                else if (dblRating > 4 && dblRating <= 4.5)  
                    strRatingGif = "rating_4_25.gif"; 
                else if (dblRating >= 4.5 && dblRating < 4.75) 
                    strRatingGif = "rating_4_5.gif";    
                else if (dblRating >= 4.75 && dblRating < 5)     
                    strRatingGif = "rating_4_75.gif";  
                else if (dblRating == 5)
                    strRatingGif = "rating_5_0.gif";
                

                strReturn = "<div style=\"position:relative;\"><img src=\"../images/ratings/" + strRatingGif + "\" alt=\"Product Rating\" width=\"115\" height=\"25\" class=\"starRating\" /></div>";
            }


            return strReturn;
        }
    }      
}
