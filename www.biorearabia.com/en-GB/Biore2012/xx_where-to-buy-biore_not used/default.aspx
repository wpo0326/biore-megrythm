﻿<%@ Page Title="Where To Buy | Bior&eacute;&reg; Skincare" Language="C#" MasterPageFile="../Site.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Biore2012.where_to_buy._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <meta content="Quickly find where to buy your favorite Bioré® products" name="description" />
    <meta content="where to buy Bioré products, find Bioré retailers" name="keywords" />
    <%=SquishIt.Framework.Bundle .Css()
        .Add("~/css/whereToBuy.css")
        .Render("~/css/combinedbuy_#.css")
    %>
    <link rel="stylesheet" type="text/css" href="/en-CA/css/wheretobuy.css">
    <script src="/en-CA/js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="/en-CA/js/popup.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main">
        <div id="mainContent">
            <div id="shadow"></div>
            <div class="centeringDiv">
                <h1>Where To Buy</h1>
                    <div id="storeLogos" class="logos">
                        <p>Shop for Bior&eacute;<sup>&reg;</sup> products at a retailer near you:<br /><br /><br /></p>
                <div id="logoHolder">
                    <asp:ListView runat="server" ID="lvRetailer" DataSourceID="XmlDataSource1" OnItemDataBound="lvRetailer_ItemDataBound">
						<LayoutTemplate>
							<ul id="retailLogos" style="width:100%; padding-top:20px; overflow:visible;">
								<div runat="server" id="itemPlaceholder"></div>
							</ul>
						</LayoutTemplate>

						<ItemTemplate>
							<li class="bubbleInfo">
								<asp:HyperLink runat="server" ID="hlRetailer" class="trigger" />
                                <div id="dpop" class="popup">
        		                	<div id="topleft" class="corner"></div>
        		                    <div id="top" class="top-bottom"></div>
        		                    <div id="topright" class="corner"></div>
                                    <div style="clear:both;"></div>
        		                    <div id="left" class="sides"></div>
        		                    <div id="right" class="sides"></div>
        		                    <div class="popup-contents">
                                    	<div><asp:HyperLink runat="server" ID="locatorLink" Target="_blank" Visible="false" style="text-indent:0px; margin:0px; padding:0px; height:25px; font-weight:normal">Store Locator</asp:HyperLink>
                                        </div>
                                        <div><asp:HyperLink runat="server" ID="onlineLink" Target="_blank" Visible="false" style="text-indent:0px; margin:0px; padding:0px; height:25px; font-weight:normal">Online Store</asp:HyperLink>
                                        </div>
                                    </div>
                                    <div style="clear:both;"></div>
        		                    <div class="corner" id="bottomleft"></div>
        		                    <div id="bottom" class="top-bottom"><img id="ImageBottom" runat="server" width="30" height="29" alt="popup tail" src="/en-US/images/wheretobuy/popup/bubble-tail2.png"/></div>
        		                    <div id="bottomright" class="corner"></div>
                                </div>
							</li>


						</ItemTemplate>
					</asp:ListView>

                    

				  <asp:XmlDataSource ID="XmlDataSource1" runat="server" 
                        DataFile="/en-CA/where-to-buy-biore/wheretobuy.xml" XPath="WTBLinks/Links"></asp:XmlDataSource>
                
                </div>


                <div id="wtbProductsAll"><img src="<%= VirtualPathUtility.ToAbsolute("~/images/whereToBuy/wtpProductsAll.png") %>" border="0" alt="" /></div>


            </div>
        </div>
    </div>
</asp:Content>

